﻿//*****************************************************************************
// Filename	: 	File_Config.h
// Created	:	2016/12/14 - 17:51
// Modified	:	2016/12/14 - 17:51
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef File_Config_h__
#define File_Config_h__

#pragma once

class CFile_Config
{
public:
	CFile_Config();
	~CFile_Config();

	// Model
	BOOL	Load_Recipe		(__in LPCTSTR szPath, __out CString& szRecipe);
	BOOL	Save_Recipe		(__in LPCTSTR szPath, __in LPCTSTR szRecipe);

	BOOL	Load_RecipeFile	(__in LPCTSTR szPath, __out CString& szRecipeFile);
	BOOL	Save_RecipeFile	(__in LPCTSTR szPath, __in LPCTSTR szRecipeFile);
	
	// EQP Model
	// Model
	// Model File

// 	BOOL	Load_CAN_Timeout(__in LPCTSTR szPath, __out ST_LGE_CAN_Timeout& stTimeout);
// 	BOOL	Save_CAN_Timeout(__in LPCTSTR szPath, __in ST_LGE_CAN_Timeout* pstTimeout);

};

#endif // File_Config_h__

