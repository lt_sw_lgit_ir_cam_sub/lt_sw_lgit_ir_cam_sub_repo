﻿//*****************************************************************************
// Filename	: 	Wnd_CurrentOp.h
// Created	:	2017/11/9 - 16:30
// Modified	:	2017/11/9 - 16:30
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_CurrentOp_h__
#define Wnd_CurrentOp_h__

#pragma once

#include "VGStatic.h"
#include "CommonFunction.h"
#include "Def_DataStruct_Cm.h"
#include "Def_T_Current.h"

enum enCurrentStatic
{
	Curr_Sleep,
	Curr_Operation,
	Curr_MaxEnum,
};

static LPCTSTR	g_szCurrentStatic[] =
{
	_T("Sleep Current (uA)"),
	_T("Operation Current (uA)"),
	NULL
};

//-----------------------------------------------------------------------------
// CWnd_CurrentOp
//-----------------------------------------------------------------------------
class CWnd_CurrentOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_CurrentOp)

public:
	CWnd_CurrentOp();
	virtual ~CWnd_CurrentOp();

protected:
	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow		(BOOL bShow, UINT nStatus);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);

	DECLARE_MESSAGE_MAP()

	CFont				m_font;

	CVGStatic			m_st_CapItem;
	CVGStatic			m_st_CapSpecMin;
	CVGStatic			m_st_CapSpecMax;

	CVGStatic			m_st_Item[Curr_MaxEnum];
	CMFCMaskedEdit		m_ed_SpecMin[Curr_MaxEnum];
	CMFCMaskedEdit		m_ed_SpecMax[Curr_MaxEnum];


public:

	void		Set_TestItemInfo	(__in ST_TestItemInfo* pstTestItemInfo);
	void		Set_TestItemInfo	(__in const ST_TestItemInfo* pstTestItemInfo, __in const ST_TI_Current* pstInTestItemOpt);

	void		Get_TestItemInfo	(__out ST_TestItemInfo& stOutTestItemInfo);
	void		Get_TestItemInfo	(__out ST_TestItemInfo& stOutTestItemInfo, __out ST_TI_Current& stOutTestItemOpt);

};

#endif // Wnd_CurrentOp_h__
