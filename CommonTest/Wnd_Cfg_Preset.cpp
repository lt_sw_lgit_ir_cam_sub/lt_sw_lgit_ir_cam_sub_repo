﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_Preset.cpp
// Created	:	2018/4/4 - 13:57
// Modified	:	2018/4/4 - 13:57
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
// Wnd_Cfg_Preset.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_Cfg_Preset.h"
#include "Def_WindowMessage_Cm.h"
#include "resource.h"

// CWnd_Cfg_Preset
#define IDC_CHK_ITEM_FST		1001
#define IDC_CHK_ITEM_LST		IDC_CHK_ITEM_FST + MAX_TESTSTEP_PRESET - 1
#define IDC_ST_ITEM_FST			2001
#define IDC_CB_ITEM_FST			3001
#define IDC_ED_ITEM_FST			4001
#define IDC_CB_USE_PRESET_MODE	5001

IMPLEMENT_DYNAMIC(CWnd_Cfg_Preset, CWnd)

CWnd_Cfg_Preset::CWnd_Cfg_Preset()
{
	VERIFY(m_font.CreateFont(
		30,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_Preset::~CWnd_Cfg_Preset()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_Preset, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_CHK_ITEM_FST, IDC_CHK_ITEM_LST, OnRangeChkCtrl)
END_MESSAGE_MAP()

// CWnd_Cfg_Preset 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_Cfg_Preset::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	// 스테레오 Calibration 전용
	m_st_UsePresetMode.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_UsePresetMode.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_UsePresetMode.SetFont_Gdip(L"Arial", 9.0F);
	m_st_UsePresetMode.Create(_T("Use Preset Mode"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_cb_UsePresetMode.Create(dwStyle | CBS_DROPDOWNLIST | WS_TABSTOP, rectDummy, this, IDC_CB_USE_PRESET_MODE);
	m_cb_UsePresetMode.SetFont(&m_font);
	m_cb_UsePresetMode.AddString(_T("Not Use"));
	m_cb_UsePresetMode.AddString(_T("Use"));
	m_cb_UsePresetMode.SetCurSel(0);

	CString szText;
	for (UINT nIdx = 0; nIdx < MAX_TESTSTEP_PRESET; nIdx++)
	{
		m_chk_Item[nIdx].Create(_T(""), dwStyle | BS_AUTOCHECKBOX, rectDummy, this, IDC_CHK_ITEM_FST + nIdx);
		m_chk_Item[nIdx].SetFont(&m_font);
		m_chk_Item[nIdx].SetMouseCursorHand();
		m_chk_Item[nIdx].SetImage(IDB_UNCHECKED_16);
		m_chk_Item[nIdx].SetCheckedImage(IDB_CHECKED_16);
		m_chk_Item[nIdx].SizeToContent();

		szText.Format(_T("Preset %02d"), nIdx + 1);
		m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdx].Create(szText, dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

		m_cb_Item[nIdx].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CB_ITEM_FST + nIdx);
		m_cb_Item[nIdx].SetFont(&m_font);

		m_ed_Item[nIdx].Create(WS_VISIBLE | WS_BORDER | ES_LEFT, rectDummy, this, IDC_ED_ITEM_FST + nIdx);
		m_ed_Item[nIdx].SetFont(&m_font);

		if (Sys_Stereo_Cal == m_InspectionType)
		{
			for (UINT nTestItemID = TI_Ste_Re_M_Focal_Len_X; nTestItemID <= TI_Ste_Re_StereoRMS; nTestItemID++)
			{
				m_cb_Item[nIdx].AddString(g_szTestItem_Ste_CAL[nTestItemID]);
			}

			if (nIdx <= TI_Ste_Re_StereoRMS)
			{
				m_cb_Item[nIdx].SetCurSel(nIdx);
			}
		}
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_Cfg_Preset::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iMargin		= 10;
	int iSpacing	= 5;

	int iLeft		= iMargin;
	int iTop		= iMargin;
	int iWidth		= cx - iMargin - iMargin;
	int iHeight		= cy - iMargin - iMargin;
	int iCtrlHeight = (iHeight - (iSpacing * (MAX_TESTSTEP_PRESET - 1))) / MAX_TESTSTEP_PRESET;
	iCtrlHeight		= __min(38, iCtrlHeight);
	
	int iChkWidth	= 40;
	int iStWidth	= 160;
	int iCbWidth	= 360;
	int iEdWidth	= 240;

	int iChkLeft	= iLeft;
	int iStLeft		= iChkLeft + iChkWidth + iSpacing;
	int iCbLeft		= iStLeft + iStWidth + iSpacing;
	int iEdLeft		= iCbLeft + iCbWidth + iSpacing;
	
	m_st_UsePresetMode.MoveWindow(iStLeft, iTop, iStWidth, iCtrlHeight);
	m_cb_UsePresetMode.MoveWindow(iCbLeft, iTop, iCbWidth, iCtrlHeight);
	iTop += iCtrlHeight + iCtrlHeight;

	for (UINT nIdx = 0; nIdx < m_nUsePresetCount; nIdx++)
	{
		m_chk_Item[nIdx].MoveWindow(iChkLeft, iTop, iChkWidth, iCtrlHeight);
		m_st_Item[nIdx].MoveWindow(iStLeft, iTop, iStWidth, iCtrlHeight);
		m_cb_Item[nIdx].MoveWindow(iCbLeft, iTop, iCbWidth, iCtrlHeight);
		m_ed_Item[nIdx].MoveWindow(iEdLeft, iTop, iEdWidth, iCtrlHeight);

		iTop += iCtrlHeight + iSpacing;
	}

	for (UINT nIdx = m_nUsePresetCount; nIdx < MAX_TESTSTEP_PRESET; nIdx++)
	{
		m_chk_Item[nIdx].MoveWindow(0, 0, 0, 0);
		m_st_Item[nIdx].MoveWindow(0, 0, 0, 0);
		m_cb_Item[nIdx].MoveWindow(0, 0, 0, 0);
		m_ed_Item[nIdx].MoveWindow(0, 0, 0, 0);
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_Preset::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnRangeChkCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_Cfg_Preset::OnRangeChkCtrl(UINT nID)
{
	UINT nIdx = nID - IDC_CHK_ITEM_FST;

	if (BST_CHECKED == m_chk_Item[nIdx].GetCheck())
	{
		//m_st_Item[nIdx].EnableWindow(TRUE);
		m_cb_Item[nIdx].EnableWindow(TRUE);
		m_ed_Item[nIdx].EnableWindow(TRUE);
	}
	else
	{
		//m_st_Item[nIdx].EnableWindow(FALSE);
		m_cb_Item[nIdx].EnableWindow(FALSE);
		m_ed_Item[nIdx].EnableWindow(FALSE);
	}
}

//=============================================================================
// Method		: SetSystemType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2018/4/5 - 15:43
// Desc.		:
//=============================================================================
void CWnd_Cfg_Preset::SetSystemType(__in enInsptrSysType nSysType)
{
	m_InspectionType = nSysType;
}

//=============================================================================
// Method		: Set_PresetInfo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_PresetStepInfo * pstPresetInfo
// Qualifier	:
// Last Update	: 2018/4/4 - 14:21
// Desc.		:
//=============================================================================
void CWnd_Cfg_Preset::Set_PresetInfo(__in const ST_PresetStepInfo* pstPresetInfo)
{
	if (NULL == pstPresetInfo)
	{
		return;
	}
	
	m_cb_UsePresetMode.SetCurSel(pstPresetInfo->bUsePresetMode);	

	UINT nCbIndex = 0;
	for (UINT nIdx = 0; nIdx < pstPresetInfo->nUsePresetFileCount; nIdx++)
	{
		if (Sys_Stereo_Cal == m_InspectionType)
		{
			nCbIndex = pstPresetInfo->stPresetLink[nIdx].nPresetType - TI_Ste_Re_M_Focal_Len_X;

			m_cb_Item[nIdx].SetCurSel(nCbIndex);
		}

		m_chk_Item[nIdx].SetCheck(BST_CHECKED);

		//m_st_Item[nIdx].EnableWindow(TRUE);
		m_cb_Item[nIdx].EnableWindow(TRUE);
		m_ed_Item[nIdx].EnableWindow(TRUE);
	}

	for (UINT nIdx = pstPresetInfo->nUsePresetFileCount; nIdx < MAX_TESTSTEP_PRESET; nIdx++)
	{
		m_chk_Item[nIdx].SetCheck(BST_UNCHECKED);

		//m_st_Item[nIdx].EnableWindow(FALSE);
		m_cb_Item[nIdx].EnableWindow(FALSE);
		m_ed_Item[nIdx].EnableWindow(FALSE);
	}
}

//=============================================================================
// Method		: Get_PresetInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_PresetStepInfo & stPresetInfo
// Qualifier	:
// Last Update	: 2018/4/4 - 14:21
// Desc.		:
//=============================================================================
void CWnd_Cfg_Preset::Get_PresetInfo(__out ST_PresetStepInfo& stPresetInfo)
{
	int iSel = m_cb_UsePresetMode.GetCurSel();
	stPresetInfo.bUsePresetMode = (0 == iSel) ? FALSE : TRUE;

	UINT nCbIndex = 0;
	UINT nPresetIndex = 0;
	for (UINT nIdx = 0; nIdx < MAX_TESTSTEP_PRESET; nIdx++)
	{
		if (Sys_Stereo_Cal == m_InspectionType)
		{
			if (BST_CHECKED == m_chk_Item[nIdx].GetCheck())
			{
				nCbIndex = m_cb_Item[nIdx].GetCurSel();

				stPresetInfo.stPresetLink[nPresetIndex].nPresetType =  nCbIndex + TI_Ste_Re_M_Focal_Len_X;

				++nPresetIndex;
			}
		}
	}

	stPresetInfo.nUsePresetFileCount = nPresetIndex;

	for (UINT nIdx = nPresetIndex; nIdx < MAX_TESTSTEP_PRESET; nIdx++)
	{
		stPresetInfo.stPresetLink[nIdx].Reset();
	}
}

//=============================================================================
// Method		: Set_UsePresetCount
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nUseCount
// Qualifier	:
// Last Update	: 2018/4/5 - 16:13
// Desc.		:
//=============================================================================
void CWnd_Cfg_Preset::Set_UsePresetCount(__in UINT nUseCount)
{
	m_nUsePresetCount = (nUseCount < MAX_TESTSTEP_PRESET) ? nUseCount : MAX_TESTSTEP_PRESET;
}

