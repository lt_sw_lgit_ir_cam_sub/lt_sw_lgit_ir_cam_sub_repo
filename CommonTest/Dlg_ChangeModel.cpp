﻿//*****************************************************************************
// Filename	: 	Dlg_ChangeModel.cpp
// Created	:	2017/2/27 - 17:45
// Modified	:	2017/2/27 - 17:45
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Dlg_ChangeModel.cpp : implementation file
//

#include "stdafx.h"
#include "Dlg_ChangeModel.h"
#include "afxdialogex.h"

#ifdef USE_CUSTOM_FUNCTION
	#include "CommonFunction.h"
#endif

#define IDC_ED_MODEL_BARCODE	1001
#define IDC_BN_CLEAR			1002
#define IDC_BN_SEARCH			1003
#define IDC_CB_MODEL			1004

// CDlg_ChangeModel dialog

IMPLEMENT_DYNAMIC(CDlg_ChangeModel, CDialogEx)

CDlg_ChangeModel::CDlg_ChangeModel(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlg_ChangeModel::IDD, pParent)
{
	VERIFY(m_font_Large.CreateFont(
		36,							// nHeight
		0,							// nWidth
		0,							// nEscapement
		0,							// nOrientation
		FW_BOLD,					// nWeight
		FALSE,						// bItalic
		FALSE,						// bUnderline
		0,							// cStrikeOut
		ANSI_CHARSET,				// nCharSet
		OUT_DEFAULT_PRECIS,			// nOutPrecision
		CLIP_DEFAULT_PRECIS,		// nClipPrecision
		ANTIALIASED_QUALITY,		// nQuality
		VARIABLE_PITCH,				// nPitchAndFamily
		_T("Arial")));		// lpszFacename

	VERIFY(m_font_Default.CreateFont(
		30,							// nHeight
		0,							// nWidth
		0,							// nEscapement
		0,							// nOrientation
		FW_BOLD,					// nWeight
		FALSE,						// bItalic
		FALSE,						// bUnderline
		0,							// cStrikeOut
		ANSI_CHARSET,				// nCharSet
		OUT_DEFAULT_PRECIS,			// nOutPrecision
		CLIP_DEFAULT_PRECIS,		// nClipPrecision
		ANTIALIASED_QUALITY,		// nQuality
		VARIABLE_PITCH,				// nPitchAndFamily
		_T("Arial")));		// lpszFacename

	m_szRecipePath	= _T("C:\\Recipe\\");
	m_szFileExt		= _T("luri");

	m_szAppName		= _T("Common");
	m_szKeyName		= _T("ModelCode");

	m_bFindModelFile	= FALSE;
}

CDlg_ChangeModel::~CDlg_ChangeModel()
{
}

void CDlg_ChangeModel::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDlg_ChangeModel, CDialogEx)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_BN_CLICKED(IDC_BN_CLEAR,			OnBnClickedBnClear)
	ON_BN_CLICKED(IDC_BN_SEARCH,		OnBnClickedBnSearch)
	ON_EN_CHANGE(IDC_ED_MODEL_BARCODE,	OnEnChangeEdModelBarcode)
	ON_CBN_SELENDOK	(IDC_CB_MODEL,		OnCbnSelEndCbModel)
	ON_BN_CLICKED(IDOK,					OnEnClickedBnOK)
END_MESSAGE_MAP()


// CDlg_ChangeModel message handlers
//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2017/1/10 - 10:13
// Desc.		:
//=============================================================================
BOOL CDlg_ChangeModel::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN)
	{
		//OnOK();

		if (m_ed_Model.GetFocus())
		{
			CString szText;
			m_ed_Model.GetWindowText(szText);
			if (FindModelByModelCode(szText))
			{

			}
			else
			{

			}
		}

		return TRUE;
	}
	else if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_ESCAPE)
	{
		// 여기에 ESC키 기능 작성       
		return TRUE;
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/10 - 10:13
// Desc.		:
//=============================================================================
int CDlg_ChangeModel::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

#ifdef USE_CUSTOM_FUNCTION
	m_st_Model.SetBackColor(RGB(0, 0, 0));
	m_st_Model.SetTextColor(Color::White, Color::White);
	m_st_Model.SetFont_Gdip(L"arial", 16.0F);
	m_st_Model_CB.SetBackColor(RGB(0, 0, 0));
	m_st_Model_CB.SetTextColor(Color::White, Color::White);
	m_st_Model_CB.SetFont_Gdip(L"arial", 16.0F);

	m_st_ModelFile.SetFont_Gdip(L"arial", 16.0F);
	m_st_Verify.SetFont_Gdip(L"arial", 16.0F);

	m_st_Model.Create(_T("Barcode"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_st_ModelFile.Create(_T("Selected Model File"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_st_Verify.Create(_T("Input Model Barcode"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_st_Model_CB.Create(_T("Model File"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
#else
	m_st_Model.Create(_T("Model Barcode"), dwStyle | WS_BORDER | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_st_ModelFile.Create(_T("Selected Model File"), dwStyle | WS_BORDER | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_st_Verify.Create(_T("Input Model Barcode"), dwStyle | WS_BORDER | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_st_Model_CB.Create(_T("Model File"), dwStyle | WS_BORDER | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
#endif
	
	m_ed_Model.Create(dwStyle | ES_CENTER | WS_BORDER, rectDummy, this, IDC_ED_MODEL_BARCODE);
	m_bn_Clear.Create(_T("Clear"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_CLEAR);
	m_bn_Search.Create(_T("Search"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_SEARCH);
	m_cb_Model.Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CB_MODEL);
	

	m_bn_OK.Create(_T("OK"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDOK);

	m_ed_Model.SetFont(&m_font_Large);
	m_cb_Model.SetFont(&m_font_Default);

	m_ed_Model.SetFocus();

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/10 - 10:13
// Desc.		:
//=============================================================================
void CDlg_ChangeModel::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	int iMagrin		= 10;
	int iSpacing	= 5;
	int iCateSpacing= 10;

	int iLeft		= iMagrin;
	int iTop		= iMagrin;
	int iWidth		= cx - iMagrin - iMagrin;
	int iHeight		= cy - iMagrin - iMagrin;
	int iCtrlWidth = 120;
	int iCtrlHeight = 40;
	int iCtrlHeight_MSG = 50;
	int iTempWidth = iWidth - iSpacing - iCtrlWidth;
	int iSubLeft = iLeft;

	m_st_Verify.MoveWindow(iLeft, iTop, iWidth, iCtrlHeight_MSG);

	iTop += iCtrlHeight_MSG + iSpacing;
	m_st_Model.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
	iLeft += iCtrlWidth + iSpacing;
	iTempWidth = iWidth - iSpacing - iCtrlWidth - iSpacing - iCtrlWidth;
	m_ed_Model.MoveWindow(iLeft, iTop, iTempWidth, iCtrlHeight);
	iLeft += iTempWidth + iSpacing;
	m_bn_Search.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);

	iLeft = iMagrin;
	iTop += iCtrlHeight + iSpacing;
	m_st_Model_CB.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
	iLeft += iCtrlWidth + iSpacing;
	iTempWidth = iWidth - iSpacing - iCtrlWidth;
	m_cb_Model.MoveWindow(iLeft, iTop, iTempWidth, iCtrlHeight);

	iLeft = cx - iMagrin - iCtrlWidth;
	iTop += iCtrlHeight + iSpacing;
	m_bn_Clear.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);

	iLeft = iMagrin;
	iTop += iCtrlHeight + iCateSpacing;
	m_st_ModelFile.MoveWindow(iLeft, iTop, iWidth, iCtrlHeight_MSG);
	

	iTop = cy - iMagrin - iCtrlHeight_MSG;
	m_bn_OK.MoveWindow(iLeft, iTop, iWidth, iCtrlHeight_MSG);
}

//=============================================================================
// Method		: OnGetMinMaxInfo
// Access		: protected  
// Returns		: void
// Parameter	: MINMAXINFO * lpMMI
// Qualifier	:
// Last Update	: 2017/1/11 - 11:51
// Desc.		:
//=============================================================================
void CDlg_ChangeModel::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMaxTrackSize.x = 620;
	lpMMI->ptMaxTrackSize.y = 400;

	lpMMI->ptMinTrackSize.x = 620;
	lpMMI->ptMinTrackSize.y = 400;

	CDialogEx::OnGetMinMaxInfo(lpMMI);
}

//=============================================================================
// Method		: OnInitDialog
// Access		: virtual protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/1/10 - 10:55
// Desc.		:
//=============================================================================
BOOL CDlg_ChangeModel::OnInitDialog()
{
	CDialogEx::OnInitDialog();


	// 모델 목록 업데이트
	m_szFileExt;
	m_fileWatch.SetWatchOption(m_szRecipePath, m_szFileExt);
	m_fileWatch.RefreshList();
	CStringList* strModelList = m_fileWatch.GetFileList();

	POSITION pos;

	for (pos = strModelList->GetHeadPosition(); pos != NULL;)
	{
		m_cb_Model.AddString(strModelList->GetNext(pos));
	}

	if (!m_szFindModelFile.IsEmpty())
	{
		int iSel = m_cb_Model.FindStringExact(0, m_szFindModelFile);

		if (0 <= iSel)
		{
			m_cb_Model.SetCurSel(iSel);
			//m_st_ModelFile.SetText(m_strSelectModel);
		}
	}


	m_szFindModelFile.Empty();

	m_ed_Model.SetFocus();

	return FALSE;
	//return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/11 - 13:00
// Desc.		:
//=============================================================================
BOOL CDlg_ChangeModel::PreCreateWindow(CREATESTRUCT& cs)
{
	ModifyStyle(0, WS_SIZEBOX);

	return CDialogEx::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnBnClickedBnClear
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/10 - 10:41
// Desc.		:
//=============================================================================
void CDlg_ChangeModel::OnBnClickedBnClear()
{
	m_ed_Model.SetWindowText(_T(""));

	m_ed_Model.SetFocus();
}

//=============================================================================
// Method		: OnBnClickedBnSearch
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/10 - 10:58
// Desc.		:
//=============================================================================
void CDlg_ChangeModel::OnBnClickedBnSearch()
{
	CString szBarcode;
	m_ed_Model.GetWindowText(szBarcode);

	if (FALSE == szBarcode.IsEmpty())
	{
		if (FindModelByModelCode(szBarcode))
		{

		}
		else
		{

		}
	}
	else
	{

	}
}

//=============================================================================
// Method		: OnEnChangeEdModelBarcode
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/10 - 11:40
// Desc.		:
//=============================================================================
void CDlg_ChangeModel::OnEnChangeEdModelBarcode()
{
	// /r/n
	CString szText;

	if (0 < m_ed_Model.GetWindowTextLength())
	{
		m_ed_Model.GetWindowText(szText);

		if (0 <= szText.Find(_T('\n')))
		{
			if (FindModelByModelCode(szText))
			{

			}
			else
			{

			}
		}
	}
}

//=============================================================================
// Method		: OnCbnSelEndCbModel
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/2 - 17:01
// Desc.		:
//=============================================================================
void CDlg_ChangeModel::OnCbnSelEndCbModel()
{
	int iSel = m_cb_Model.GetCurSel();

	if (0 <= iSel)
	{
		m_cb_Model.GetLBText(iSel, m_szFindModelFile);

		CString szValue;
		szValue.Format(_T("%s.%s"), m_szFindModelFile, m_szFileExt);
		m_st_ModelFile.SetText(szValue);

		m_bFindModelFile = TRUE;
	}
}

//=============================================================================
// Method		: OnEnClickedBnOK
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/11 - 13:17
// Desc.		:
//=============================================================================
void CDlg_ChangeModel::OnEnClickedBnOK()
{
	if (m_bFindModelFile)
	{
		OnOK();
	}
	else
	{
		OnCancel();
	}
}

//=============================================================================
// Method		: ReadModelCode
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out CString & szModelCode
// Qualifier	:
// Last Update	: 2017/1/10 - 10:13
// Desc.		:
//=============================================================================
BOOL CDlg_ChangeModel::ReadModelCode(__in LPCTSTR szPath, __out CString& szModelCode)
{
	szModelCode;
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	// 모델명
	if (0 < GetPrivateProfileString(m_szAppName, m_szKeyName, _T(""), inBuff, 255, szPath))
	{
		szModelCode = inBuff;
		return TRUE;
	}
	else
	{
		return FALSE;
	}
	

	return TRUE;
}

//=============================================================================
// Method		: FindModelByModelCode
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szCode
// Qualifier	:
// Last Update	: 2017/1/10 - 10:13
// Desc.		:
//=============================================================================
BOOL CDlg_ChangeModel::FindModelByModelCode(__in LPCTSTR szCode)
{
 	CString		strFindModel;
 	CString		szReadedCode;
 	CFileFind	finder;
	CString		szInputCode = szCode;

	szInputCode.MakeUpper();
 	m_bFindModelFile = FALSE;
 
 	CString strWildcard;
 	strWildcard.Format(_T("%s*.%s"), m_szRecipePath, m_szFileExt);
 
 	// start working for files
 	BOOL bWorking = finder.FindFile(strWildcard);
 
 	while (bWorking)
 	{
 		bWorking = finder.FindNextFile();
 
 		if (finder.IsDots())
 			continue;
 
 		if (finder.IsDirectory())
 			continue;
 
 		if (finder.IsArchived())
 		{
 			if (ReadModelCode(finder.GetFilePath(), szReadedCode))
 			{
 				szReadedCode.MakeUpper();
 				if (0 == szReadedCode.Compare(szInputCode))
 				{
 					strFindModel = finder.GetFileTitle();
					m_szFindModelFile = strFindModel;
 					m_bFindModelFile = TRUE;
 					break;
 				}
 			}
 			else
 			{
 				if (0 == szInputCode.GetLength())
 				{
 					m_bFindModelFile = TRUE;
 					break;
 				}
 			}
 		}
 	}
 
 	finder.Close();
 
 	if (m_bFindModelFile)
 	{
#ifdef USE_CUSTOM_FUNCTION
		m_st_Verify.SetBackColor(RGB(0, 255, 0));
#endif
		m_st_Verify.SetWindowText(_T("Successful change model"));

		strFindModel.Format(_T("%s.%s"), m_szFindModelFile, m_szFileExt);
		//m_st_ModelFile.SetWindowText(m_szFindModelFile);
#ifdef USE_CUSTOM_FUNCTION
		m_st_ModelFile.SetText(strFindModel);
#else
		m_st_ModelFile.SetWindowText(strFindModel);
#endif

#ifdef USE_CUSTOM_FUNCTION
		Delay(1000);
#else
		{
			MSG		msg;
			DWORD	endTick;

			endTick = GetTickCount() + 1000;

			while (GetTickCount() < endTick)
			{
				if (PeekMessage(&msg, NULL, 0, 0, TRUE))
				{
					TranslateMessage(&msg);
					DispatchMessage(&msg);
				}
			}
		}
#endif
		OnOK();
 		return TRUE;
 	}
 	else
 	{
#ifdef USE_CUSTOM_FUNCTION
		m_st_Verify.SetBackColor(RGB(255, 0, 0));
#endif
		m_st_Verify.SetWindowText(_T("Failed change model"));
		m_st_ModelFile.SetWindowText(_T("File not found"));

 		return FALSE;
 	}

	return TRUE;
}

//=============================================================================
// Method		: SetCurrentModel
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szRecipe
// Qualifier	:
// Last Update	: 2017/3/2 - 17:25
// Desc.		:
//=============================================================================
void CDlg_ChangeModel::SetCurrentModel(__in LPCTSTR szRecipe)
{
	m_szFindModelFile = szRecipe;
}

//=============================================================================
// Method		: InsertBarcode
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szBarcode
// Qualifier	:
// Last Update	: 2017/1/12 - 15:04
// Desc.		:
//=============================================================================
BOOL CDlg_ChangeModel::InsertBarcode(__in LPCTSTR szBarcode)
{
	CString szValue = szBarcode;

	szValue.Remove(_T('\r'));
	szValue.Remove(_T('\n'));
	m_ed_Model.SetWindowText(szValue);

	return FindModelByModelCode(szBarcode);
}

//=============================================================================
// Method		: GetModelFile
// Access		: public  
// Returns		: CString
// Qualifier	:
// Last Update	: 2017/1/10 - 11:53
// Desc.		:
//=============================================================================
CString CDlg_ChangeModel::GetModelFile()
{
	return m_szFindModelFile;
}
