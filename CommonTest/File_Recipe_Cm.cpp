//*****************************************************************************
// Filename	: 	File_Recipe_Cm.cpp
// Created	:	2017/9/25 - 21:03
// Modified	:	2017/9/25 - 21:03
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "File_Recipe_Cm.h"
#include "CommonFunction.h"
#include "tinyxml2.h"

#define		VerInfo_AppName				_T("VersionInfo")
#define		Recipe_AppName				_T("Recipe")
#define		Common_AppName				_T("Common")
#define		ModelCode_KeyName			_T("ModelCode")
#define		StepInfo_AppName			_T("StepInfo")
#define		ConsumInfo_AppName			_T("Consumables")

#define		CurrentOpt_AppName			_T("Current")
#define		VCSelOpt_AppName			_T("VCSel")

#define		WipIDOpt_AppName			_T("WipID")
#define		Cal2DOpt_AppName			_T("Cal2D")
#define		Cal3DDepthOpt_AppName		_T("Cal3Depth")
#define		Cal3DEvaluOpt_AppName		_T("Cal3DEvalu")

#define		AppName_2DCal_Para			_T("Para_2DCal")
#define		AppName_2DCal_Motor			_T("Motor_2DCal")
#define		AppName_2DCal_Delay			_T("Delay_2DCal")

#define		AppName_3D_Depth_Para		_T("Para_3D_Depth")
#define		AppName_3D_Eval_Para		_T("Para_3D_Eval")
#define		AppName_3D_Hand_Para		_T("Para_3D_Hand")
#define		AppName_3DCal_Motor			_T("Motor_3DCal")

#define		AppName_IQ_Para				_T("IQ")

#define		AppName_MES_Result			_T("MES_Result")


CFile_Recipe_Cm::CFile_Recipe_Cm()
{
}

CFile_Recipe_Cm::~CFile_Recipe_Cm()
{
}

BOOL CFile_Recipe_Cm::Save_XML_String(__in CString szFullPath, __in CString szXML)
{
	CFile File;
	CFileException e;
	if (!PathFileExists(szFullPath))
	{
		if (!File.Open(szFullPath, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}
	}
	else
	{
		if (!File.Open(szFullPath, CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}
	}

	CStringA szANSI;
	USES_CONVERSION;
	szANSI = CT2A(szXML.GetBuffer(0));

	File.SeekToEnd();
	//File.Write(szBuff.GetBuffer(0), szBuff.GetLength() * sizeof(TCHAR));
	File.Write(szANSI.GetBuffer(0), szANSI.GetLength());

	File.Flush();
	File.Close();

	return TRUE;
}

//=============================================================================
// Method		: SetSystemType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2017/11/9 - 21:41
// Desc.		:
//=============================================================================
void CFile_Recipe_Cm::SetSystemType(__in enInsptrSysType nSysType)
{
	m_InspectionType = nSysType;
}

//=============================================================================
// Method		: Load_RecipeFile
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_RecipeInfo_Base & stRecipeInfo
// Qualifier	:
// Last Update	: 2017/10/10 - 14:27
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Load_RecipeFile(__in LPCTSTR szPath, __out ST_RecipeInfo_Base& stRecipeInfo)
{
	if (NULL == szPath)
		return FALSE;

	// 파일이 존재하는가?
	if (!PathFileExists(szPath))
	{
		return FALSE;
	}

	TCHAR   inBuff[1024] = { 0, };
	CString	strKey;

	BOOL bReturn = TRUE;

	bReturn = Load_Common(szPath, stRecipeInfo);

	bReturn &= Load_StepInfo(szPath, stRecipeInfo.StepInfo);
	bReturn &= Load_TestItemInfo(szPath, stRecipeInfo.TestItemInfo);

	return bReturn;
}

//=============================================================================
// Method		: Save_RecipeFile
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_RecipeInfo_Base * pstRecipeInfo
// Qualifier	:
// Last Update	: 2017/10/10 - 14:27
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Save_RecipeFile(__in LPCTSTR szPath, __in const ST_RecipeInfo_Base* pstRecipeInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstRecipeInfo)
		return FALSE;

	::DeleteFile(szPath);

	CString strValue;
	CString strKey;

	BOOL bReturn = TRUE;

	bReturn = Save_Common(szPath, pstRecipeInfo);

	bReturn &= Save_StepInfo(szPath, &pstRecipeInfo->StepInfo);
	bReturn &= Save_TestItemInfo(szPath, &pstRecipeInfo->TestItemInfo);

	return bReturn;
}

//=============================================================================
// Method		: Load_Common
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_RecipeInfo_Base & stRecipeInfo
// Qualifier	:
// Last Update	: 2017/10/10 - 14:27
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Load_Common(__in LPCTSTR szPath, __out ST_RecipeInfo_Base& stRecipeInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString strValue;
	CString strKeyName;

	// 모델명
	GetPrivateProfileString(Common_AppName, ModelCode_KeyName, _T("Default"), inBuff, 255, szPath);
	stRecipeInfo.szModelCode = inBuff;

	// ModelType
	GetPrivateProfileString(Common_AppName, _T("ModelType"), _T("0"), inBuff, 255, szPath);
	stRecipeInfo.ModelType = (enModelType)_ttoi(inBuff);

	// Test Retry Count
	GetPrivateProfileString(Common_AppName, _T("TestRetryCount"), _T("0"), inBuff, 255, szPath);
	stRecipeInfo.nTestRetryCount = _ttoi(inBuff);

	// 소모품 설정 파일
	GetPrivateProfileString(Common_AppName, _T("ConsumablesFile"), _T(""), inBuff, 255, szPath);
	stRecipeInfo.szConsumablesFile = inBuff;

	// Exposure
	GetPrivateProfileString(Common_AppName, _T("Exposure"), _T("0"), inBuff, 255, szPath);
	stRecipeInfo.dExposure = _ttof(inBuff);

	// Voltage
	GetPrivateProfileString(Common_AppName, _T("Voltage"), _T("12.0"), inBuff, 255, szPath);
	stRecipeInfo.fVoltage = (float)_ttof(inBuff);

	GetPrivateProfileString(Common_AppName, _T("PixelSize"), _T("5.6"), inBuff, 255, szPath);
	stRecipeInfo.fPixelSize = (float)_ttof(inBuff);

	GetPrivateProfileString(Common_AppName, _T("FocalLength"), _T("1.74"), inBuff, 255, szPath);
	stRecipeInfo.fFocalLength = (float)_ttof(inBuff);

	GetPrivateProfileString(Common_AppName, _T("CamImageType"), _T("2"), inBuff, 255, szPath);
	stRecipeInfo.nCamImageType = _ttoi(inBuff);

	GetPrivateProfileString(Common_AppName, _T("LightDelay"), _T("1000"), inBuff, 255, szPath);
	stRecipeInfo.nLightDelay = _ttoi(inBuff);

	GetPrivateProfileString(Common_AppName, _T("ChangeDelay"), _T("1000"), inBuff, 255, szPath);
	stRecipeInfo.nChangeDelay = _ttoi(inBuff);

	// UseStereoCal_Flag
	GetPrivateProfileString(Common_AppName, _T("UseStereoCal_Flag"), _T("0"), inBuff, 255, szPath);
	stRecipeInfo.bUseStereoCal_Flag = _ttoi(inBuff);

	return TRUE;
}

//=============================================================================
// Method		: Save_Common
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_RecipeInfo_Base * pstRecipeInfo
// Qualifier	:
// Last Update	: 2017/10/10 - 14:27
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Save_Common(__in LPCTSTR szPath, __in const ST_RecipeInfo_Base* pstRecipeInfo)
{
	if (NULL == szPath)
		return FALSE;

	CString strValue;
	CString strKeyName;

	// Version, Format
	strValue.Format(_T("%s %s"), SYS_CUSTOMER, g_szInsptrSysType[pstRecipeInfo->nInspectionType]);
	WritePrivateProfileString(VerInfo_AppName, _T("Equipment"), strValue, szPath);
	strValue.Format(_T("%s (%s)"), GetVersionInfo(_T("ProductVersion")), GetVersionInfo(_T("FileVersion")));
	WritePrivateProfileString(VerInfo_AppName, _T("SWVersion"), strValue, szPath);

	// 검사기 명칭
	strValue = g_szInsptrSysType[pstRecipeInfo->nInspectionType];
	WritePrivateProfileString(VerInfo_AppName, _T("Inspection"), strValue, szPath);

	// 모델명
	strValue = pstRecipeInfo->szModelCode;
	WritePrivateProfileString(Common_AppName, ModelCode_KeyName, strValue, szPath);

	// ModelType
	strValue.Format(_T("%d"), pstRecipeInfo->ModelType);
	WritePrivateProfileString(Common_AppName, _T("ModelType"), strValue, szPath);

	// Test Retry Count
	strValue.Format(_T("%d"), pstRecipeInfo->nTestRetryCount);
	WritePrivateProfileString(Common_AppName, _T("TestRetryCount"), strValue, szPath);

	// 소모품 설정 파일
	strValue = pstRecipeInfo->szConsumablesFile;
	WritePrivateProfileString(Common_AppName, _T("ConsumablesFile"), strValue, szPath);

	// Exposure
	strValue.Format(_T("%.6f"), pstRecipeInfo->dExposure);
	WritePrivateProfileString(Common_AppName, _T("Exposure"), strValue, szPath);
	
	// Voltage
	strValue.Format(_T("%.1f"), pstRecipeInfo->fVoltage);
	WritePrivateProfileString(Common_AppName, _T("Voltage"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstRecipeInfo->fPixelSize);
	WritePrivateProfileString(Common_AppName, _T("PixelSize"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstRecipeInfo->fFocalLength);
	WritePrivateProfileString(Common_AppName, _T("FocalLength"), strValue, szPath);

	strValue.Format(_T("%d"), pstRecipeInfo->nCamImageType);
	WritePrivateProfileString(Common_AppName, _T("CamImageType"), strValue, szPath);

	strValue.Format(_T("%d"), pstRecipeInfo->nLightDelay);
	WritePrivateProfileString(Common_AppName, _T("LightDelay"), strValue, szPath);

	strValue.Format(_T("%d"), pstRecipeInfo->nChangeDelay);
	WritePrivateProfileString(Common_AppName, _T("ChangeDelay"), strValue, szPath);
	
	// UseStereoCal_Flag
	strValue.Format(_T("%d"), pstRecipeInfo->bUseStereoCal_Flag);
	WritePrivateProfileString(Common_AppName, _T("UseStereoCal_Flag"), strValue, szPath);

	return TRUE;
}

//=============================================================================
// Method		: Load_StepInfo
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_StepInfo & stStepInfo
// Qualifier	:
// Last Update	: 2017/9/25 - 21:08
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Load_StepInfo(__in LPCTSTR szPath, __out ST_StepInfo& stStepInfo)
{
	if (NULL == szPath)
		return FALSE;

	// 파일이 존재하는가?
	if (!PathFileExists(szPath))
	{
		return FALSE;
	}

	TCHAR   inBuff[255] = { 0, };

	CString strAppName;
	CString strKeyName;

	// 기존 스텝 정보 삭제
	stStepInfo.RemoveAll();

	GetPrivateProfileString(StepInfo_AppName, _T("ItemCount"), _T(""), inBuff, 255, szPath);
	int iItemCount = _ttoi(inBuff);

	for (INT nIdx = 0; nIdx < iItemCount; nIdx++)
	{
		strAppName.Format(_T("%s_%03d"), StepInfo_AppName, nIdx);

		ST_StepUnit stStepUnit;

		// Retry Count
		GetPrivateProfileString(strAppName, _T("RetryCnt"), _T("0"), inBuff, 255, szPath);
		stStepUnit.nRetryCnt = _ttoi(inBuff);

		// Delay
		GetPrivateProfileString(strAppName, _T("Delay"), _T("0"), inBuff, 255, szPath);
		stStepUnit.dwDelay = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("UseTest"), _T("0"), inBuff, 255, szPath);
		stStepUnit.bTest = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("TestItem"), _T(""), inBuff, 255, szPath);
		stStepUnit.nTestItem = _ttoi(inBuff);

		// Move Y
		GetPrivateProfileString(strAppName, _T("UseMoveY"), _T("0"), inBuff, 255, szPath);
		stStepUnit.bUseMoveY = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("MoveY"), _T("0"), inBuff, 255, szPath);
		if (stStepUnit.bUseMoveY)
		{
			stStepUnit.nMoveY = _ttoi(inBuff);
		}
		else
		{
			stStepUnit.nMoveY = 0;
		}

		// Move X
		GetPrivateProfileString(strAppName, _T("UseMoveX"), _T("0"), inBuff, 255, szPath);
		stStepUnit.bUseMoveX = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("MoveX"), _T("0"), inBuff, 255, szPath);
		if (stStepUnit.bUseMoveX)
		{
			stStepUnit.iMoveX = _ttoi(inBuff);
		}
		else
		{
			stStepUnit.iMoveX = 0;
		}
		
		// Chart Rotate
		GetPrivateProfileString(strAppName, _T("UseChartRot"), _T("0"), inBuff, 255, szPath);
		stStepUnit.bUseChart_Rot = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("ChartRotation"), _T("0"), inBuff, 255, szPath);
		if (stStepUnit.bUseChart_Rot)
		{
			stStepUnit.dChart_Rot = _ttof(inBuff);
		}
		else
		{
			stStepUnit.dChart_Rot = 0.0f;
		}
		
		// 차트 X
		GetPrivateProfileString(strAppName, _T("UseChart_Move_X"), _T("0"), inBuff, 255, szPath);
		stStepUnit.bUseChart_Move_X = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Chart_Move_X"), _T("0"), inBuff, 255, szPath);
		if (stStepUnit.bUseChart_Move_X)
		{
			stStepUnit.iChart_Move_X = _ttoi(inBuff);
		}
		else
		{
			stStepUnit.iChart_Move_X = 0;
		}

		// 차트 Z
		GetPrivateProfileString(strAppName, _T("UseChart_Move_Z"), _T("0"), inBuff, 255, szPath);
		stStepUnit.bUseChart_Move_Z = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Chart_Move_Z"), _T("0"), inBuff, 255, szPath);
		if (stStepUnit.bUseChart_Move_Z)
		{
			stStepUnit.iChart_Move_Z = _ttoi(inBuff);
		}
		else
		{
			stStepUnit.iChart_Move_Z = 0;
		}

		// 차트 Tilt X
		GetPrivateProfileString(strAppName, _T("UseChart_Tilt_X"), _T("0"), inBuff, 255, szPath);
		stStepUnit.bUseChart_Tilt_X = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Chart_Tilt_X"), _T("0"), inBuff, 255, szPath);
		if (stStepUnit.bUseChart_Tilt_X)
		{
			stStepUnit.iChart_Tilt_X = _ttoi(inBuff);
		}
		else
		{
			stStepUnit.iChart_Tilt_X = 0;
		}

		// 차트 Tilt Z
		GetPrivateProfileString(strAppName, _T("UseChart_Tilt_Z"), _T("0"), inBuff, 255, szPath);
		stStepUnit.bUseChart_Tilt_Z = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Chart_Tilt_Z"), _T("0"), inBuff, 255, szPath);
		if (stStepUnit.bUseChart_Tilt_Z)
		{
			stStepUnit.iChart_Tilt_Z = _ttoi(inBuff);
		}
		else
		{
			stStepUnit.iChart_Tilt_Z = 0;
		}

		// Alpha
		GetPrivateProfileString(strAppName, _T("Alpha"), _T("0"), inBuff, 255, szPath);
		stStepUnit.wAlpha = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Alpha_2nd"), _T("0"), inBuff, 255, szPath);
		stStepUnit.wAlpha_2nd = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Board_Ctrl"), _T("0"), inBuff, 255, szPath);
		stStepUnit.nBoardCtrl = (enBoardCtrl)_ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Test_Loop"), _T("0"), inBuff, 255, szPath);
		stStepUnit.nTestLoop = (enTestLoop)_ttoi(inBuff);

		stStepInfo.StepList.Add(stStepUnit);
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_StepInfo
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_StepInfo * pstStepInfo
// Qualifier	:
// Last Update	: 2017/9/25 - 21:22
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Save_StepInfo(__in LPCTSTR szPath, __in const ST_StepInfo* pstStepInfo)
{
	if (NULL == szPath)
		return FALSE;

	CString strValue;
	CString strAppName;
	CString strKeyName;

	INT_PTR iItemCount = pstStepInfo->StepList.GetCount();
	strValue.Format(_T("%d"), iItemCount);
	WritePrivateProfileString(StepInfo_AppName, _T("ItemCount"), strValue, szPath);
	
	for (INT_PTR nIdx = 0; nIdx < iItemCount; nIdx++)
	{
		strAppName.Format(_T("%s_%03d"), StepInfo_AppName, nIdx);

		// Retry Count
		strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].nRetryCnt);
		WritePrivateProfileString(strAppName, _T("RetryCnt"), strValue, szPath);

		// Delay
		strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].dwDelay);
		WritePrivateProfileString(strAppName, _T("Delay"), strValue, szPath);

		strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].bTest);
		WritePrivateProfileString(strAppName, _T("UseTest"), strValue, szPath);

		strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].nTestItem);
		WritePrivateProfileString(strAppName, _T("TestItem"), strValue, szPath);

		// Move Y
		strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].bUseMoveY);
		WritePrivateProfileString(strAppName, _T("UseMoveY"), strValue, szPath);

		if (pstStepInfo->StepList[nIdx].bUseMoveY)
		{
			strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].nMoveY);
		}
		else
		{
			strValue = _T("0");
		}
		WritePrivateProfileString(strAppName, _T("MoveY"), strValue, szPath);

		// Move X
		strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].bUseMoveX);
		WritePrivateProfileString(strAppName, _T("UseMoveX"), strValue, szPath);

		if (pstStepInfo->StepList[nIdx].bUseMoveX)
		{
			strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].iMoveX);
		}
		else
		{
			strValue = _T("0");
		}
		WritePrivateProfileString(strAppName, _T("MoveX"), strValue, szPath);

		// Chart Rotate
		strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].bUseChart_Rot);
		WritePrivateProfileString(strAppName, _T("UseChartRot"), strValue, szPath);

		if (pstStepInfo->StepList[nIdx].bUseChart_Rot)
		{
			strValue.Format(_T("%.2f"), pstStepInfo->StepList[nIdx].dChart_Rot);
		}
		else
		{
			strValue = _T("0.00");
		}
		WritePrivateProfileString(strAppName, _T("ChartRotation"), strValue, szPath);

		// 차트 X
		strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].bUseChart_Move_X);
		WritePrivateProfileString(strAppName, _T("UseChart_Move_X"), strValue, szPath);

		if (pstStepInfo->StepList[nIdx].bUseChart_Move_X)
		{
			strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].iChart_Move_X);
		}
		else
		{
			strValue = _T("0");
		}
		WritePrivateProfileString(strAppName, _T("Chart_Move_X"), strValue, szPath);

		// 차트 Z
		strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].bUseChart_Move_Z);
		WritePrivateProfileString(strAppName, _T("UseChart_Move_Z"), strValue, szPath);

		if (pstStepInfo->StepList[nIdx].bUseChart_Move_Z)
		{
			strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].iChart_Move_Z);
		}
		else
		{
			strValue = _T("0");
		}
		WritePrivateProfileString(strAppName, _T("Chart_Move_Z"), strValue, szPath);

		// 차트 Tilt X
		strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].bUseChart_Tilt_X);
		WritePrivateProfileString(strAppName, _T("UseChart_Tilt_X"), strValue, szPath);

		if (pstStepInfo->StepList[nIdx].bUseChart_Tilt_X)
		{
			strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].iChart_Tilt_X);
		}
		else
		{
			strValue = _T("0");
		}
		WritePrivateProfileString(strAppName, _T("Chart_Tilt_X"), strValue, szPath);

		// 차트 Tilt Z
		strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].bUseChart_Tilt_Z);
		WritePrivateProfileString(strAppName, _T("UseChart_Tilt_Z"), strValue, szPath);

		if (pstStepInfo->StepList[nIdx].bUseChart_Tilt_Z)
		{
			strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].iChart_Tilt_Z);
		}
		else
		{
			strValue = _T("0");
		}
		WritePrivateProfileString(strAppName, _T("Chart_Tilt_Z"), strValue, szPath);

		// Alpha
		strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].wAlpha);
		WritePrivateProfileString(strAppName, _T("Alpha"), strValue, szPath);

		strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].wAlpha_2nd);
		WritePrivateProfileString(strAppName, _T("Alpha_2nd"), strValue, szPath);

		strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].nBoardCtrl);
		WritePrivateProfileString(strAppName, _T("Board_Ctrl"), strValue, szPath);

		strValue.Format(_T("%d"), pstStepInfo->StepList[nIdx].nTestLoop);
		WritePrivateProfileString(strAppName, _T("Test_Loop"), strValue, szPath);
	}

	return TRUE;
}

//=============================================================================
// Method		: LoadXML_StepInfo
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_StepInfo & stStepInfo
// Qualifier	:
// Last Update	: 2018/3/27 - 15:08
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::LoadXML_StepInfo(__in LPCTSTR szPath, __out ST_StepInfo& stStepInfo)
{
	if (NULL == szPath)
		return FALSE;

	// 파일이 존재하는가?
	if (!PathFileExists(szPath))
	{
		return FALSE;
	}

	CStringA szTemp;
	tinyxml2::XMLDocument	ReadDoc;
	USES_CONVERSION;
	if (tinyxml2::XML_SUCCESS != ReadDoc.LoadFile(CT2A(szPath)))
	{
		TRACE("LoadXML_StepInfo : Failed to open [%s]\n", szPath);
	}

	tinyxml2::XMLDeclaration* pDecl = ReadDoc.ToDeclaration();

	tinyxml2::XMLElement* pRoot = ReadDoc.FirstChildElement("Luritech");

	if (pRoot)
	{
		pRoot->Attribute("Version");
		pRoot->Attribute("Customer");
		pRoot->Attribute("Equipment");

		tinyxml2::XMLElement* pStepInfo = pRoot->FirstChildElement("StepInfo");
		if (pStepInfo)
		{
			tinyxml2::XMLElement* pStepList = pStepInfo->FirstChildElement("StepList");
			if (pStepList)
			{
				// 스텝 정보 초기화
				stStepInfo.RemoveAll();

				// Step 개수
				INT_PTR iItemCount = pStepList->Int64Attribute("Count", 0);

				ST_StepUnit* pStep = NULL;
				for (INT_PTR nIdx = 0; nIdx < iItemCount; nIdx++)
				{
					// 스텝 1개 추가
					ST_StepUnit stStepUnit;
					stStepInfo.Step_Add(stStepUnit);

					// 추가된 스텝 포인터
					pStep = &stStepInfo.StepList.GetAt(nIdx);

					szTemp.Format("Step_%03d", nIdx + 1);
					tinyxml2::XMLElement* pStepItem = pStepList->FirstChildElement(szTemp.GetBuffer(0));
					if (pStepItem)
					{
						pStep->nRetryCnt	= pStepItem->IntAttribute("RetryCnt", 0);
						pStep->dwDelay		= (DWORD)pStepItem->Int64Attribute("Delay", 0);

						tinyxml2::XMLElement* pTest	= pStepItem->FirstChildElement("Test");
						if (pTest)
						{
							pStep->bTest		= pTest->IntAttribute("Use", 0);
							pStep->nTestItem	= pTest->IntAttribute("TestItem", 0);

							tinyxml2::XMLElement* pLoop	= pTest->FirstChildElement("Loop");
							if (pLoop)
							{
								pStep->nTestLoop = (enTestLoop)pLoop->IntAttribute("Use", 0);
							}
						}

						tinyxml2::XMLElement* pMotion = pStepItem->FirstChildElement("Motion");
						if (pMotion)
						{
							tinyxml2::XMLElement* pMoveY = pMotion->FirstChildElement("MoveY");
							if (pMoveY)
							{
								pStep->bUseMoveY	= pMoveY->IntAttribute("Use", 0);
								pStep->nMoveY		= pMoveY->IntAttribute("Value", 0);
							}

							tinyxml2::XMLElement* pMoveX = pMotion->FirstChildElement("MoveX");
							if (pMoveX)
							{
								pStep->bUseMoveX	= pMoveX->IntAttribute("Use", 0);
								pStep->iMoveX		= pMoveX->IntAttribute("Value", 0);
							}

							tinyxml2::XMLElement* pChartRotation = pMotion->FirstChildElement("ChartRotation");
							if (pChartRotation)
							{
								pStep->bUseChart_Rot	= pChartRotation->IntAttribute("Use", 0);
								pStep->dChart_Rot		= pChartRotation->DoubleAttribute("Value", 0.0f);
							}

							tinyxml2::XMLElement* pChart_Move_X	= pMotion->FirstChildElement("Chart_Move_X");
							if (pChart_Move_X)
							{
								pStep->bUseChart_Move_X	= pChart_Move_X->IntAttribute("Use", 0);
								pStep->iChart_Move_X	= pChart_Move_X->IntAttribute("Value", 0);
							}

							tinyxml2::XMLElement* pChart_Move_Z	= pMotion->FirstChildElement("Chart_Move_Z");
							if (pChart_Move_Z)
							{
								pStep->bUseChart_Move_Z	= pChart_Move_Z->IntAttribute("Use", 0);
								pStep->iChart_Move_Z	= pChart_Move_Z->IntAttribute("Value", 0);
							}

							tinyxml2::XMLElement* pChart_Tilt_X	= pMotion->FirstChildElement("Chart_Tilt_X");
							if (pChart_Tilt_X)
							{
								pStep->bUseChart_Tilt_X	= pChart_Tilt_X->IntAttribute("Use", 0);
								pStep->iChart_Tilt_X	= pChart_Tilt_X->IntAttribute("Value", 0);
							}

							tinyxml2::XMLElement* pChart_Tilt_Z	= pMotion->FirstChildElement("Chart_Tilt_Z");
							if (pChart_Tilt_Z)
							{
								pStep->bUseChart_Tilt_Z = pChart_Tilt_Z->IntAttribute("Use", 0);
								pStep->iChart_Tilt_Z	= pChart_Tilt_Z->IntAttribute("Value", 0);
							}
						}

						tinyxml2::XMLElement* pDeviceCtrl = pStepItem->FirstChildElement("DeviceCtrl");
						if (pDeviceCtrl)
						{
							tinyxml2::XMLElement* pAlpha = pDeviceCtrl->FirstChildElement("Alpha");
							if (pAlpha)
							{
								pStep->wAlpha		= pAlpha->IntAttribute("Value_1", 0);
								pStep->wAlpha_2nd	= pAlpha->IntAttribute("Value_2", 0);
							}

							tinyxml2::XMLElement* pBoard_Ctrl = pDeviceCtrl->FirstChildElement("Board_Ctrl");
							if (pBoard_Ctrl)
							{
								pStep->nBoardCtrl	= (enBoardCtrl)pBoard_Ctrl->IntAttribute("Code", 0);
							}
						}
					} // if (pStepItem)
				} // for()
			} // if (pStepList)
			else
			{
				return FALSE;
			}
		} // if (pStepInfo)
		else
		{
			return FALSE;
		}
	} // if (pRoot)
	else
	{
		return FALSE;
	}

	return TRUE;
}

//=============================================================================
// Method		: SaveXML_StepInfo
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_StepInfo * pstStepInfo
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2018/3/29 - 13:51
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::SaveXML_StepInfo(__in LPCTSTR szPath, __in const ST_StepInfo* pstStepInfo, __in enInsptrSysType nSysType)
{
	CStringA szTemp;

	tinyxml2::XMLDocument doc;
	tinyxml2::XMLDeclaration* decl = doc.NewDeclaration();
	doc.LinkEndChild(decl);

	USES_CONVERSION;
	tinyxml2::XMLElement* pRoot = doc.NewElement("Luritech");
	pRoot->SetAttribute("Version", "1.0");
	pRoot->SetAttribute("Customer", "LGIT");
	pRoot->SetAttribute("Equipment", CT2A(g_szInsptrSysType[nSysType])); 
	doc.LinkEndChild(pRoot);
	
	// 주석
	tinyxml2::XMLComment* pComment = doc.NewComment("Settings for Inspection Sequence");
	pRoot->LinkEndChild(pComment);

	tinyxml2::XMLElement* pStepInfo = doc.NewElement("StepInfo");
	tinyxml2::XMLElement* pStepList = doc.NewElement("StepList");
	INT_PTR iItemCount = pstStepInfo->StepList.GetCount();
	szTemp.Format("%d", iItemCount);
	pStepList->SetAttribute("Count", iItemCount);

	const ST_StepUnit* pStep = NULL;

	for (INT_PTR nIdx = 0; nIdx < iItemCount; nIdx++)
	{
		pStep = &pstStepInfo->StepList.GetAt(nIdx);

		szTemp.Format("Step_%03d", nIdx + 1);
		tinyxml2::XMLElement* pStepItem = doc.NewElement(szTemp.GetBuffer(0));
		pStepItem->SetAttribute("RetryCnt", pStep->nRetryCnt);
		pStepItem->SetAttribute("Delay", (UINT)pStep->dwDelay);

		// Test
		tinyxml2::XMLElement* pTest		= doc.NewElement("Test");
		pTest->SetAttribute("Use",		pStep->bTest);
		pTest->SetAttribute("TestItem", pStep->bTest ? pStep->nTestItem : 0);
		szTemp = pStep->bTest ? CT2A(GetTestItemName(nSysType, pStep->nTestItem).GetBuffer(0)) : "No Test";
		pTest->SetAttribute("Name",		szTemp.GetBuffer(0));
		
		tinyxml2::XMLElement* pLoop		= doc.NewElement("Loop");
		pLoop->SetAttribute("Use",	pStep->nTestLoop);		
		pTest->LinkEndChild(pLoop);
		pStepItem->LinkEndChild(pTest);

		// Motion	
		tinyxml2::XMLElement* pMotion	= doc.NewElement("Motion");
		tinyxml2::XMLElement* pMoveY			= doc.NewElement("MoveY");		
		tinyxml2::XMLElement* pMoveX			= doc.NewElement("MoveX");
		tinyxml2::XMLElement* pChartRotation	= doc.NewElement("ChartRotation");
		tinyxml2::XMLElement* pChart_Move_X		= doc.NewElement("Chart_Move_X");
		tinyxml2::XMLElement* pChart_Move_Z		= doc.NewElement("Chart_Move_Z");
		tinyxml2::XMLElement* pChart_Tilt_X		= doc.NewElement("Chart_Tilt_X");
		tinyxml2::XMLElement* pChart_Tilt_Z		= doc.NewElement("Chart_Tilt_Z");

		pMoveY->SetAttribute("Use",				pStep->bUseMoveY);
		pMoveY->SetAttribute("Value",			pStep->bUseMoveY ? pStep->nMoveY : 0);
		pMotion->LinkEndChild(pMoveY);

		pMoveX->SetAttribute("Use",				pStep->bUseMoveX);
		pMoveX->SetAttribute("Value",			pStep->bUseMoveX ? pStep->iMoveX : 0);
		pMotion->LinkEndChild(pMoveX);

		pChartRotation->SetAttribute("Use",		pStep->bUseChart_Rot);
		pChartRotation->SetAttribute("Value",	pStep->bUseChart_Rot ? pStep->dChart_Rot : 0);
		pMotion->LinkEndChild(pChartRotation);

		pChart_Move_X->SetAttribute("Use",		pStep->bUseChart_Move_X);
		pChart_Move_X->SetAttribute("Value",	pStep->bUseChart_Move_X ? pStep->iChart_Move_X : 0);
		pMotion->LinkEndChild(pChart_Move_X);

		pChart_Move_Z->SetAttribute("Use",		pStep->bUseChart_Move_Z);
		pChart_Move_Z->SetAttribute("Value",	pStep->bUseChart_Move_Z ? pStep->iChart_Move_Z : 0);
		pMotion->LinkEndChild(pChart_Move_Z);

		pChart_Tilt_X->SetAttribute("Use",		pStep->bUseChart_Tilt_X);
		pChart_Tilt_X->SetAttribute("Value",	pStep->bUseChart_Tilt_X ? pStep->iChart_Tilt_X : 0);
		pMotion->LinkEndChild(pChart_Tilt_X);

		pChart_Tilt_Z->SetAttribute("Use",		pStep->bUseChart_Tilt_Z);
		pChart_Tilt_Z->SetAttribute("Value",	pStep->bUseChart_Tilt_Z ? pStep->iChart_Tilt_Z : 0);
		pMotion->LinkEndChild(pChart_Tilt_Z);

		pStepItem->LinkEndChild(pMotion);

		// Device Control
		tinyxml2::XMLElement* pDevice	= doc.NewElement("DeviceCtrl");
		tinyxml2::XMLElement* pAlpha		= doc.NewElement("Alpha");
		tinyxml2::XMLElement* pBoard_Ctrl	= doc.NewElement("Board_Ctrl");

		pAlpha->SetAttribute("Value_1",		pStep->wAlpha);
		pAlpha->SetAttribute("Value_2",		pStep->wAlpha_2nd);
		pDevice->LinkEndChild(pAlpha);

		pBoard_Ctrl->SetAttribute("Code",	pStep->nBoardCtrl);
		pDevice->LinkEndChild(pBoard_Ctrl);

		pStepItem->LinkEndChild(pDevice);

		pStepList->LinkEndChild(pStepItem);
	}

	pStepInfo->LinkEndChild(pStepList);
	pRoot->LinkEndChild(pStepInfo);

	// 파일 저장
	CStringA szFileName;
	szFileName = CT2A(szPath);
	doc.SaveFile(szFileName.GetBuffer(0)); // writing document to a file

	return TRUE;
}

//=============================================================================
// Method		: SaveXML_StepInfo_Old
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_StepInfo * pstStepInfo
// Qualifier	:
// Last Update	: 2018/3/27 - 15:08
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::SaveXML_StepInfo_Old(__in LPCTSTR szPath, __in const ST_StepInfo* pstStepInfo)
{
	if (NULL == szPath)
		return FALSE;

	m_xml.SetDocFlags(CMarkup::MDF_TRIMWHITESPACE);
	m_xml.RemoveElem();
	m_xml.ResetPos();
	m_xml.FindElem();
	m_xml.IntoElem();

	CString szTemp;

	m_xml.AddElem(_T("Luritech"));
	m_xml.AddAttrib(_T("Version"), _T("1.0"));
	m_xml.AddAttrib(_T("Customer"), _T("LGIT"));
	m_xml.AddAttrib(_T("Equipment"), _T(""));
	m_xml.OutOfElem();

	m_xml.IntoElem();
	m_xml.AddElem(_T("StepInfo"));
	m_xml.IntoElem();

	m_xml.AddElem(_T("StepList"));
	INT_PTR iItemCount = pstStepInfo->StepList.GetCount();
	szTemp.Format(_T("%d"), iItemCount);
	m_xml.AddAttrib(_T("Count"), szTemp.GetBuffer(0));;
	m_xml.IntoElem();

	for (INT_PTR nIdx = 0; nIdx < iItemCount; nIdx++)
	{
		szTemp.Format(_T("Step_%03d"), nIdx);
		m_xml.AddElem(szTemp.GetBuffer(0));
		m_xml.IntoElem();

		// Retry Count
		szTemp.Format(_T("%d"), pstStepInfo->StepList[nIdx].nRetryCnt);
		m_xml.AddElem(_T("RetryCnt"), szTemp.GetBuffer(0));

		// Delay
		szTemp.Format(_T("%d"), pstStepInfo->StepList[nIdx].dwDelay);
		m_xml.AddElem(_T("Delay"), szTemp.GetBuffer(0));

		// Test 진행 여부
		szTemp.Format(_T("%d"), pstStepInfo->StepList[nIdx].bTest);
		m_xml.AddElem(_T("UseTest"), szTemp.GetBuffer(0));

		// Test 진행시 검사 항목명
		szTemp.Format(_T("%d"), pstStepInfo->StepList[nIdx].nTestItem);
		m_xml.AddElem(_T("TestItem"), szTemp.GetBuffer(0));

		// Move Test
		szTemp.Format(_T("%d"), pstStepInfo->StepList[nIdx].bUseMoveY);
		m_xml.AddElem(_T("UseMoveY"), szTemp.GetBuffer(0));

		if (pstStepInfo->StepList[nIdx].bUseMoveY)
		{
			szTemp.Format(_T("%d"), pstStepInfo->StepList[nIdx].nMoveY);
		}
		else
		{
			szTemp = _T("0");
		}
		m_xml.AddElem(_T("MoveY"), szTemp.GetBuffer(0));

		// Move Y
		szTemp.Format(_T("%d"), pstStepInfo->StepList[nIdx].bUseMoveY);
		m_xml.AddElem(_T("UseMoveY"), szTemp.GetBuffer(0));

		if (pstStepInfo->StepList[nIdx].bUseMoveY)
		{
			szTemp.Format(_T("%d"), pstStepInfo->StepList[nIdx].nMoveY);
		}
		else
		{
			szTemp = _T("0");
		}
		m_xml.AddElem(_T("MoveY"), szTemp.GetBuffer(0));

		// Move X
		szTemp.Format(_T("%d"), pstStepInfo->StepList[nIdx].bUseMoveX);
		m_xml.AddElem(_T("UseMoveX"), szTemp.GetBuffer(0));

		if (pstStepInfo->StepList[nIdx].bUseMoveX)
		{
			szTemp.Format(_T("%d"), pstStepInfo->StepList[nIdx].iMoveX);
		}
		else
		{
			szTemp = _T("0");
		}
		m_xml.AddElem(_T("MoveX"), szTemp.GetBuffer(0));

		// Chart Rotate
		szTemp.Format(_T("%d"), pstStepInfo->StepList[nIdx].bUseChart_Rot);
		m_xml.AddElem(_T("UseChartRot"), szTemp.GetBuffer(0));

		if (pstStepInfo->StepList[nIdx].bUseChart_Rot)
		{
			szTemp.Format(_T("%.2f"), pstStepInfo->StepList[nIdx].dChart_Rot);
		}
		else
		{
			szTemp = _T("0.00");
		}
		m_xml.AddElem(_T("ChartRotation"), szTemp.GetBuffer(0));

		// 차트 X
		szTemp.Format(_T("%d"), pstStepInfo->StepList[nIdx].bUseChart_Move_X);
		m_xml.AddElem(_T("UseChart_Move_X"), szTemp.GetBuffer(0));

		if (pstStepInfo->StepList[nIdx].bUseChart_Move_X)
		{
			szTemp.Format(_T("%d"), pstStepInfo->StepList[nIdx].iChart_Move_X);
		}
		else
		{
			szTemp = _T("0");
		}
		m_xml.AddElem(_T("Chart_Move_X"), szTemp.GetBuffer(0));

		// 차트 Z
		szTemp.Format(_T("%d"), pstStepInfo->StepList[nIdx].bUseChart_Move_Z);
		m_xml.AddElem(_T("UseChart_Move_Z"), szTemp.GetBuffer(0));

		if (pstStepInfo->StepList[nIdx].bUseChart_Move_Z)
		{
			szTemp.Format(_T("%d"), pstStepInfo->StepList[nIdx].iChart_Move_Z);
		}
		else
		{
			szTemp = _T("0");
		}
		m_xml.AddElem(_T("Chart_Move_Z"), szTemp.GetBuffer(0));

		// 차트 Tilt X
		szTemp.Format(_T("%d"), pstStepInfo->StepList[nIdx].bUseChart_Tilt_X);
		m_xml.AddElem(_T("UseChart_Tilt_X"), szTemp.GetBuffer(0));

		if (pstStepInfo->StepList[nIdx].bUseChart_Tilt_X)
		{
			szTemp.Format(_T("%d"), pstStepInfo->StepList[nIdx].iChart_Tilt_X);
		}
		else
		{
			szTemp = _T("0");
		}
		m_xml.AddElem(_T("Chart_Tilt_X"), szTemp.GetBuffer(0));

		// 차트 Tilt Z
		szTemp.Format(_T("%d"), pstStepInfo->StepList[nIdx].bUseChart_Tilt_Z);
		m_xml.AddElem(_T("UseChart_Tilt_Z"), szTemp.GetBuffer(0));

		if (pstStepInfo->StepList[nIdx].bUseChart_Tilt_Z)
		{
			szTemp.Format(_T("%d"), pstStepInfo->StepList[nIdx].iChart_Tilt_Z);
		}
		else
		{
			szTemp = _T("0");
		}
		m_xml.AddElem(_T("Chart_Tilt_Z"), szTemp.GetBuffer(0));

		// Alpha
		szTemp.Format(_T("%d"), pstStepInfo->StepList[nIdx].wAlpha);
		m_xml.AddElem(_T("Alpha"), szTemp.GetBuffer(0));

		szTemp.Format(_T("%d"), pstStepInfo->StepList[nIdx].wAlpha_2nd);
		m_xml.AddElem(_T("Alpha_2nd"), szTemp.GetBuffer(0));

		szTemp.Format(_T("%d"), pstStepInfo->StepList[nIdx].nBoardCtrl);
		m_xml.AddElem(_T("Board_Ctrl"), szTemp.GetBuffer(0));

		szTemp.Format(_T("%d"), pstStepInfo->StepList[nIdx].nTestLoop);
		m_xml.AddElem(_T("Test_Loop"), szTemp.GetBuffer(0));

		m_xml.OutOfElem(); // Step_000
	}

	m_xml.OutOfElem(); // StepList
	m_xml.OutOfElem(); // StepInfo
	m_xml.OutOfElem(); // Luritech

	m_xml.Save(szPath);

 	CString szXML = m_xml.GetSubDoc();
// 	Save_XML_String(szPath, szXML);

	return TRUE;
}

//=============================================================================
// Method		: Load_PresetInfo
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_PresetStepInfo & stPresetStepInfo
// Qualifier	:
// Last Update	: 2018/4/3 - 15:32
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Load_PresetInfo(__in LPCTSTR szPath, __out ST_PresetStepInfo& stPresetStepInfo)
{
	// Preset 정보
	TCHAR   inBuff[255] = { 0, };

	CString strKeyName;

	GetPrivateProfileString(_T("Preset"), _T("UsePresetMode"), _T("0"), inBuff, 255, szPath);
	stPresetStepInfo.bUsePresetMode = _ttoi(inBuff);

	GetPrivateProfileString(_T("Preset"), _T("UsePresetFileCount"), _T("0"), inBuff, 255, szPath);
	stPresetStepInfo.nUsePresetFileCount = _ttoi(inBuff);

	for (UINT nIdx = 0; nIdx < stPresetStepInfo.nUsePresetFileCount; nIdx++)
	{
		strKeyName.Format(_T("Alias_%02d"), nIdx);
 		GetPrivateProfileString(_T("Preset"), strKeyName, _T(""), inBuff, 255, szPath);
 		stPresetStepInfo.stPresetLink[nIdx].szAlias = inBuff;

		strKeyName.Format(_T("PresetType_%02d"), nIdx);
		GetPrivateProfileString(_T("Preset"), strKeyName, _T("0"), inBuff, 255, szPath);
		stPresetStepInfo.stPresetLink[nIdx].nPresetType = _ttoi(inBuff);

// 		strKeyName.Format(_T("StepInfoIndex_%02d"), nIdx);
// 		GetPrivateProfileString(_T("Preset"), strKeyName, _T("0"), inBuff, 255, szPath);
// 		stPresetStepInfo.stPresetLink[nIdx].nStepInfoIndex = _ttoi(inBuff);
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_PresetInfo
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_PresetStepInfo * pstPresetStepInfo
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2018/4/3 - 16:02
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Save_PresetInfo(__in LPCTSTR szPath, __in const ST_PresetStepInfo* pstPresetStepInfo, __in enInsptrSysType nSysType)
{
	CString strValue;
	CString strKeyName;

	strValue.Format(_T("%d"), pstPresetStepInfo->bUsePresetMode);
	WritePrivateProfileString(_T("Preset"), _T("UsePresetMode"), strValue, szPath);

	strValue.Format(_T("%d"), pstPresetStepInfo->nUsePresetFileCount);
	WritePrivateProfileString(_T("Preset"), _T("UsePresetFileCount"), strValue, szPath);

	for (INT_PTR nIdx = 0; nIdx < pstPresetStepInfo->nUsePresetFileCount; nIdx++)
	{
		strKeyName.Format(_T("Alias_%02d"), nIdx);
		strValue = pstPresetStepInfo->stPresetLink[nIdx].szAlias;
		WritePrivateProfileString(_T("Preset"), strKeyName, strValue, szPath);

		strKeyName.Format(_T("PresetType_%02d"), nIdx);
		strValue.Format(_T("%d"), pstPresetStepInfo->stPresetLink[nIdx].nPresetType);
		WritePrivateProfileString(_T("Preset"), strKeyName, strValue, szPath);

// 		strKeyName.Format(_T("StepInfoIndex_%02d"), nIdx);
// 		strValue.Format(_T("%d"), pstPresetStepInfo->stPresetLink[nIdx].nStepInfoIndex);
// 		WritePrivateProfileString(_T("Preset"), strKeyName, strValue, szPath);
	}

	return TRUE;
}

//=============================================================================
// Method		: LoadXML_PresetStepInfo
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_PresetStepInfo & stPresetStepInfo
// Qualifier	:
// Last Update	: 2018/4/3 - 15:04
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::LoadXML_PresetStepInfo(__in LPCTSTR szPath, __out ST_PresetStepInfo& stPresetStepInfo)
{
	// Preset_01.xml ~ Preset_10.xml
	CString		szXMLFullPath;

	// Preset Step 정보
	for (UINT nIdx = 0; nIdx < MAX_TESTSTEP_PRESET; nIdx++)
	{
		szXMLFullPath.Format(_T("%s\\Preset_%02d.xml"), szPath, nIdx + 1);

		if (PathFileExists(szXMLFullPath.GetBuffer(0)))
		{
			// 파일이 있으면 Load
			if (FALSE == LoadXML_StepInfo(szXMLFullPath.GetBuffer(0), stPresetStepInfo.stStepInfo[nIdx]))
			{
				TRACE(_T("LoadXML_StepInfo Failed : %d\n"), nIdx);
			}
		}
		else
		{
			// 파일이 없으면 초기화
			stPresetStepInfo.stStepInfo[nIdx].RemoveAll();

			continue;
		}
	}

	return TRUE;
}

//=============================================================================
// Method		: SaveXML_PresetStepInfo
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_PresetStepInfo * pstPresetStepInfo
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2018/4/3 - 15:04
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::SaveXML_PresetStepInfo(__in LPCTSTR szPath, __in const ST_PresetStepInfo* pstPresetStepInfo, __in enInsptrSysType nSysType)
{
	// Preset_01.xml ~ Preset_10.xml
	CString		szXMLFullPath;

	// Preset Step 정보
	for (UINT nIdx = 0; nIdx < MAX_TESTSTEP_PRESET; nIdx++)
	{
		szXMLFullPath.Format(_T("%s\\Preset_%02d.xml"), szPath, nIdx + 1);

		if (PathFileExists(szXMLFullPath.GetBuffer(0)))
		{
			::DeleteFile(szXMLFullPath.GetBuffer(0));
		}

		// 파일 저장
		if (FALSE == SaveXML_StepInfo(szXMLFullPath.GetBuffer(0), &pstPresetStepInfo->stStepInfo[nIdx], nSysType))
		{
			TRACE(_T("SaveXML_StepInfo Failed : %d\n"), nIdx);
		}
	}

	return TRUE;
}

//=============================================================================
// Method		: Load_ConsumInfoFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_ConsumablesInfo & stConsumInfo
// Qualifier	:
// Last Update	: 2016/3/18 - 10:13
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Load_ConsumInfoFile(__in LPCTSTR szPath, __out ST_ConsumablesInfo& stConsumInfo)
{
	if (NULL == szPath)
		return FALSE;

	// 파일이 존재하는가?
	if (!PathFileExists(szPath))
	{
		return FALSE;
	}

	TCHAR   inBuff[80] = { 0, };

	CString strKey;

	// 소모품 항목 갯수
// 	GetPrivateProfileString(ConsumInfo_AppName, _T("Item_Count"), _T("1"), inBuff, 80, szPath);
// 	stConsumInfo.ItemCount = _ttoi(inBuff);

	for (UINT nIdx = 0; nIdx < stConsumInfo.ItemCount; nIdx++)
	{
		// Max Count
		strKey.Format(_T("Count_Max_%d"), nIdx);
		GetPrivateProfileString(ConsumInfo_AppName, strKey, _T("50000"), inBuff, 80, szPath);
		stConsumInfo.Item[nIdx].dwCount_Max = _ttoi(inBuff);

		// Channel Count	
		strKey.Format(_T("Count_%d"), nIdx);
		GetPrivateProfileString(ConsumInfo_AppName, strKey, _T("0"), inBuff, 80, szPath);
		stConsumInfo.Item[nIdx].dwCount = _ttoi(inBuff);
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_ConsumInfoFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_ConsumablesInfo * pstConsumInfo
// Qualifier	:
// Last Update	: 2016/3/18 - 10:14
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Save_ConsumInfoFile(__in LPCTSTR szPath, __in const ST_ConsumablesInfo* pstConsumInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConsumInfo)
		return FALSE;

	::DeleteFile(szPath);

	CString strValue;
	CString strKey;

	// 소모품 항목 갯수
// 	strValue.Format(_T("%d"), pstConsumInfo->ItemCount);
// 	WritePrivateProfileString(ConsumInfo_AppName, _T("Item_Count"), strValue, szPath);

	for (UINT nIdx = 0; nIdx < pstConsumInfo->ItemCount; nIdx++)
	{
		// Max Count
		strKey.Format(_T("Count_Max_%d"), nIdx);
		strValue.Format(_T("%d"), pstConsumInfo->Item[nIdx].dwCount_Max);
		WritePrivateProfileString(ConsumInfo_AppName, strKey, strValue, szPath);

		// Channel Count
		strKey.Format(_T("Count_%d"), nIdx);
		strValue.Format(_T("%d"), pstConsumInfo->Item[nIdx].dwCount);
		WritePrivateProfileString(ConsumInfo_AppName, strKey, strValue, szPath);
	}

	return TRUE;
}

//=============================================================================
// Method		: Load_ConsumInfo
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in UINT nItemIdx
// Parameter	: __out DWORD & dwCount
// Qualifier	:
// Last Update	: 2017/1/6 - 10:58
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Load_ConsumInfo(__in LPCTSTR szPath, __in UINT nItemIdx, __out DWORD& dwCount)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[80] = { 0, };
	CString strValue;
	CString strKey;

	// Channel Count
	strKey.Format(_T("Count_%d"), nItemIdx);
	strValue.Format(_T("%d"), dwCount);
	GetPrivateProfileString(ConsumInfo_AppName, strKey, _T("0"), inBuff, 80, szPath);
	dwCount = _ttoi(inBuff);

	return TRUE;
}

//=============================================================================
// Method		: Save_ConsumInfo
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in UINT nItemIdx
// Parameter	: __in DWORD dwCount
// Qualifier	:
// Last Update	: 2017/1/6 - 10:58
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Save_ConsumInfo(__in LPCTSTR szPath, __in UINT nItemIdx, __in DWORD dwCount)
{
	if (NULL == szPath)
		return FALSE;

	CString strValue;
	CString strKey;

	// Channel Count
	strKey.Format(_T("Count_%d"), nItemIdx);
	strValue.Format(_T("%d"), dwCount);
	WritePrivateProfileString(ConsumInfo_AppName, strKey, strValue, szPath);

	return TRUE;
}

//=============================================================================
// Method		: Load_TestItemInfo
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_TestItemInfo & stOutTestItemInfo
// Qualifier	:
// Last Update	: 2017/11/9 - 19:44
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Load_TestItemInfo(__in LPCTSTR szPath, __out ST_TestItemInfo& stOutTestItemInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[128] = { 0, };
	CString strValue;
	CString strApp;
	CString strKey;

	UINT nCount = (UINT)stOutTestItemInfo.GetCount();

	GetPrivateProfileString(_T("TestItem_Info"), _T("ItemCount"), _T("0"), inBuff, 128, szPath);
	UINT nItemCount = _ttoi(inBuff);

	if (nCount < nItemCount)
	{
		// 에러?
		TRACE(_T("Error : CFile_Recipe_Cm::Load_TestItemInfo() -> nCount < nItemCount\n"));
		nItemCount = nCount;
	}

	for (UINT nIdx = 0; nIdx < nItemCount; nIdx++)
	{
		if (NULL != stOutTestItemInfo.GetTestItem(nIdx))
		{
			ST_TestItemSpec* pstTestItem = stOutTestItemInfo.GetTestItem(nIdx);

			strApp.Format(_T("TestItem_%03d"), nIdx);

	// 		GetPrivateProfileString(strApp, _T("ItemID"), _T("0"), inBuff, 128, szPath);
	// 		pstTestItem->nItemID = _ttoi(inBuff);

	// 		GetPrivateProfileString(strApp, _T("Name"), _T("0"), inBuff, 128, szPath);
	// 		pstTestItem->szName = inBuff;

	// 		GetPrivateProfileString(strApp, _T("vt"), _T("0"), inBuff, 128, szPath);
	// 		pstTestItem->vt = _ttoi(inBuff);
	// 		pstTestItem->Spec_Min.ChangeType(pstTestItem->vt);
	// 		pstTestItem->Spec_Max.ChangeType(pstTestItem->vt);

// 			GetPrivateProfileString(strApp, _T("ResultCount"), _T("1"), inBuff, 128, szPath);
// 			pstTestItem->nResultCount = _ttoi(inBuff);

			GetPrivateProfileString(strApp, _T("UseMinMaxSpec"), _T("1"), inBuff, 128, szPath);
			pstTestItem->bUseMinMaxSpec = _ttoi(inBuff);

// 			GetPrivateProfileString(strApp, _T("UseMultiSpec"), _T("0"), inBuff, 128, szPath);
// 			pstTestItem->bUseMultiSpec = _ttoi(inBuff);

			for (UINT nArIdx = 0; nArIdx < pstTestItem->nResultCount; nArIdx++)
			{
				// ** ASSERT(nArIdx < pstTestItem->Spec.GetCount());
				ASSERT(nArIdx < pstTestItem->nResultCount);
				// ** if (pstTestItem->Spec.GetCount() <= nArIdx)
				if (pstTestItem->nResultCount <= nArIdx)
				{
					break;	// 에러 상황임
				}

				strKey.Format(_T("UseMinSpec_%03d"), nArIdx);
				GetPrivateProfileString(strApp, strKey, _T("1"), inBuff, 128, szPath);
				pstTestItem->Spec[nArIdx].bUseSpecMin = _ttoi(inBuff);

				strKey.Format(_T("UseMaxSpec_%03d"), nArIdx);
				GetPrivateProfileString(strApp, strKey, _T("1"), inBuff, 128, szPath);
				pstTestItem->Spec[nArIdx].bUseSpecMax = _ttoi(inBuff);

				strKey.Format(_T("Spec_Min_%03d"), nArIdx);
				GetPrivateProfileString(strApp, strKey,	_T("0"), inBuff, 128, szPath);
				if ((VT_I2 == pstTestItem->vt) || (VT_I4 == pstTestItem->vt) || ((VT_I1 <= pstTestItem->vt) && (pstTestItem->vt <= VT_UINT)))
				{
					pstTestItem->Spec[nArIdx].Spec_Min.intVal = _ttoi(inBuff);
				}
				else if ((VT_R4 == pstTestItem->vt) || (VT_R8 == pstTestItem->vt))
				{
					pstTestItem->Spec[nArIdx].Spec_Min.dblVal = _ttof(inBuff);
				}

				strKey.Format(_T("Spec_Max_%03d"), nArIdx);
				GetPrivateProfileString(strApp, strKey,	_T("0"), inBuff, 128, szPath);
				if ((VT_I2 == pstTestItem->vt) || (VT_I4 == pstTestItem->vt) || ((VT_I1 <= pstTestItem->vt) && (pstTestItem->vt <= VT_UINT)))
				{
					pstTestItem->Spec[nArIdx].Spec_Max.intVal = _ttoi(inBuff);
				}
				else if ((VT_R4 == pstTestItem->vt) || (VT_R8 == pstTestItem->vt))
				{
					pstTestItem->Spec[nArIdx].Spec_Max.dblVal = _ttof(inBuff);
				}
			}

			//stOutTestItemInfo.Set_Spec(nIdx, pstTestItem);
		}
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_TestItemInfo
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_TestItemInfo * pstTestItemInfo
// Qualifier	:
// Last Update	: 2017/11/9 - 21:24
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Save_TestItemInfo(__in LPCTSTR szPath, __in const ST_TestItemInfo* pstTestItemInfo)
{
	if (NULL == szPath)
		return FALSE;

	CString strValue;
	CString strApp;
	CString strKey;

	const ST_TestItemSpec* pstTestItem = NULL;

	UINT nCount = (UINT)pstTestItemInfo->GetCount();

	strValue.Format(_T("%d"), nCount);
	WritePrivateProfileString(_T("TestItem_Info"), _T("ItemCount"), strValue, szPath);

	for (UINT nIdx = 0; nIdx < nCount; nIdx++)
	{
		pstTestItem = &pstTestItemInfo->TestItemList[nIdx];

		strApp.Format(_T("TestItem_%03d"), nIdx);

// 		strValue.Format(_T("%d"), pstTestItem->nItemID);
// 		WritePrivateProfileString(strApp, _T("ItemID"), strValue, szPath);

// 		strValue = pstTestItem->szName;
// 		WritePrivateProfileString(strApp, _T("Name"), strValue, szPath);

// 		strValue.Format(_T("%d"), pstTestItem->vt);
// 		WritePrivateProfileString(strApp, _T("vt"), strValue, szPath);

// 		strValue.Format(_T("%d"), pstTestItem->nResultCount);
// 		WritePrivateProfileString(strApp, _T("ResultCount"), strValue, szPath);

		strValue.Format(_T("%d"), pstTestItem->bUseMinMaxSpec);
		WritePrivateProfileString(strApp, _T("UseMinMaxSpec"), strValue, szPath);

// 		strValue.Format(_T("%d"), pstTestItem->bUseMultiSpec);
// 		WritePrivateProfileString(strApp, _T("UseMultiSpec"), strValue, szPath);

		for (UINT nArIdx = 0; nArIdx < pstTestItem->nResultCount; nArIdx++)
		{
			strKey.Format(_T("UseMinSpec_%03d"), nArIdx);
			strValue.Format(_T("%d"), pstTestItem->Spec[nArIdx].bUseSpecMin);
			WritePrivateProfileString(strApp, strKey, strValue, szPath);

			strKey.Format(_T("UseMaxSpec_%03d"), nArIdx);
			strValue.Format(_T("%d"), pstTestItem->Spec[nArIdx].bUseSpecMax);
			WritePrivateProfileString(strApp, strKey, strValue, szPath);

			strKey.Format(_T("Spec_Min_%03d"), nArIdx);
			if ((VT_I2 == pstTestItem->vt) || (VT_I4 == pstTestItem->vt) || ((VT_I1 <= pstTestItem->vt) && (pstTestItem->vt <= VT_UINT)))
			{
				strValue.Format(_T("%d"), pstTestItem->Spec[nArIdx].Spec_Min.intVal);
			}
			else if ((VT_R4 == pstTestItem->vt) || (VT_R8 == pstTestItem->vt))
			{
				strValue.Format(_T("%.04f"), pstTestItem->Spec[nArIdx].Spec_Min.dblVal);
			}
			WritePrivateProfileString(strApp, strKey, strValue, szPath);

			strKey.Format(_T("Spec_Max_%03d"), nArIdx);
			if ((VT_I2 == pstTestItem->vt) || (VT_I4 == pstTestItem->vt) || ((VT_I1 <= pstTestItem->vt) && (pstTestItem->vt <= VT_UINT)))
			{
				strValue.Format(_T("%d"), pstTestItem->Spec[nArIdx].Spec_Max.intVal);
			}
			else if ((VT_R4 == pstTestItem->vt) || (VT_R8 == pstTestItem->vt))
			{
				strValue.Format(_T("%.04f"), pstTestItem->Spec[nArIdx].Spec_Max.dblVal);
			}
			WritePrivateProfileString(strApp, strKey, strValue, szPath);
		}
	}

	return TRUE;
}

//=============================================================================
// Method		: Load_CurrentFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_RecipeInfo_Base & stRecipeInfo
// Qualifier	:
// Last Update	: 2017/12/3 - 13:38
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Load_CurrentFile(__in LPCTSTR szPath, __out ST_RecipeInfo_Base& stRecipeInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	return TRUE;
}

//=============================================================================
// Method		: Save_CurrentFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_RecipeInfo_Base * pstRecipeInfo
// Qualifier	:
// Last Update	: 2017/12/3 - 13:48
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Save_CurrentFile(__in LPCTSTR szPath, __in const ST_RecipeInfo_Base* pstRecipeInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstRecipeInfo)
		return FALSE;

	CString strValue;

	CString	strAppName;
	strAppName = CurrentOpt_AppName;

// 	strValue.Format(_T("%.1f"), pstRecipeInfo->TestItemOpt.stCurrent.stCurrentOpt.dbOffset);
// 	WritePrivateProfileString(strAppName, _T("Offset"), strValue, szPath);
// 
// 	strValue.Format(_T("%d"), pstRecipeInfo->TestItemOpt.stCurrent.stCurrentOpt.nMinCurrent);
// 	WritePrivateProfileString(strAppName, _T("MinCurrent"), strValue, szPath);
// 
// 	strValue.Format(_T("%d"), pstRecipeInfo->TestItemOpt.stCurrent.stCurrentOpt.nMaxCurrent);
// 	WritePrivateProfileString(strAppName, _T("MaxCurrent"), strValue, szPath);

	return TRUE;
}


//=============================================================================
// Method		: Load_VCSelFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_RecipeInfo_Base & stRecipeInfo
// Qualifier	:
// Last Update	: 2017/12/18 - 9:31
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Load_VCSelFile(__in LPCTSTR szPath, __out ST_RecipeInfo_Base& stRecipeInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName;

	strAppName = VCSelOpt_AppName;

// 	GetPrivateProfileString(strAppName, _T("MinSpec"), _T("0"), inBuff, 80, szPath);
// 	stRecipeInfo.TestItemOpt.stVcsel.stVCSELOption.fMinSpec = (float)_ttof(inBuff);
// 
// 	GetPrivateProfileString(strAppName, _T("MaxSpec"), _T("0"), inBuff, 80, szPath);
// 	stRecipeInfo.TestItemOpt.stVcsel.stVCSELOption.fMaxSpec = (float)_ttof(inBuff);
// 
// 	GetPrivateProfileString(strAppName, _T("MinSpec_Enable"), _T("0"), inBuff, 80, szPath);
// 	stRecipeInfo.TestItemOpt.stVcsel.stVCSELOption.bMinSpec_Enable = _ttoi(inBuff);
// 
// 	GetPrivateProfileString(strAppName, _T("MaxSpec_Enable"), _T("0"), inBuff, 80, szPath);
// 	stRecipeInfo.TestItemOpt.stVcsel.stVCSELOption.bMaxSpec_Enable = _ttoi(inBuff);

	return TRUE;
}

//=============================================================================
// Method		: Save_VCSelFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_RecipeInfo_Base * pstRecipeInfo
// Qualifier	:
// Last Update	: 2017/12/18 - 9:31
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Save_VCSelFile(__in LPCTSTR szPath, __in const ST_RecipeInfo_Base* pstRecipeInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstRecipeInfo)
		return FALSE;

	CString strValue;
	CString	strAppName;

	strAppName = VCSelOpt_AppName;

// 	strValue.Format(_T("%.2f"), pstRecipeInfo->TestItemOpt.stVcsel.stVCSELOption.fMinSpec);
// 	WritePrivateProfileString(strAppName, _T("MinSpec"), strValue, szPath);
// 
// 	strValue.Format(_T("%.2f"), pstRecipeInfo->TestItemOpt.stVcsel.stVCSELOption.fMaxSpec);
// 	WritePrivateProfileString(strAppName, _T("MaxSpec"), strValue, szPath);
// 
// 	strValue.Format(_T("%d"), pstRecipeInfo->TestItemOpt.stVcsel.stVCSELOption.bMinSpec_Enable);
// 	WritePrivateProfileString(strAppName, _T("MinSpec_Enable"), strValue, szPath);
// 
// 	strValue.Format(_T("%d"), pstRecipeInfo->TestItemOpt.stVcsel.stVCSELOption.bMaxSpec_Enable);
// 	WritePrivateProfileString(strAppName, _T("MaxSpec_Enable"), strValue, szPath);

	return TRUE;
}

//=============================================================================
// Method		: Load_WipIDFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_Foc_Info & stWipID
// Qualifier	:
// Last Update	: 2017/11/5 - 14:59
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Load_Focusing_Info(__in LPCTSTR szPath, __out ST_Foc_Info& stWipID)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };
	CString	strAppName;

	return TRUE;
}

//=============================================================================
// Method		: Save_WipIDFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_Foc_Info * pstWipID
// Qualifier	:
// Last Update	: 2017/11/5 - 14:59
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Save_Focusing_Info(__in LPCTSTR szPath, __in const ST_Foc_Info* pstWipID)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstWipID)
		return FALSE;

	return TRUE;
}

//=============================================================================
// Method		: Load_2DCal_Info
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_2DCal_Info & stOut2DCal_Info
// Qualifier	:
// Last Update	: 2017/11/14 - 23:18
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Load_2DCal_Info(__in LPCTSTR szPath, __out ST_2DCal_Info& stOut2DCal_Info)
{
	if (NULL == szPath)
		return FALSE;

	BOOL bReturn = TRUE;

	bReturn &= Load_2DCal_Para(szPath, stOut2DCal_Info.Parameter);

	return TRUE;
}

//=============================================================================
// Method		: Save_2DCal_Info
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_2DCal_Info * pst2DCal_Info
// Qualifier	:
// Last Update	: 2017/11/14 - 23:18
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Save_2DCal_Info(__in LPCTSTR szPath, __in const ST_2DCal_Info* pst2DCal_Info)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pst2DCal_Info)
		return FALSE;

	BOOL bReturn = TRUE;

	bReturn &= Save_2DCal_Para(szPath, &pst2DCal_Info->Parameter);

	return TRUE;
}

//=============================================================================
// Method		: Load_2DCal_Para
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_2DCal_Para & stOut2DCal_Para
// Qualifier	:
// Last Update	: 2018/2/7 - 19:51
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Load_2DCal_Para(__in LPCTSTR szPath, __out ST_2DCal_Para& stOut2DCal_Para)
{
	if (NULL == szPath)
		return FALSE;

	BOOL bReturn = TRUE;
	TCHAR   inBuff[255] = { 0, };
	CString strKeyName;

// 	stOut2DCal_Para.IntrParam.nImgWidth						= 640;
	GetPrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_nImgWidth], _T("640"), inBuff, 255, szPath);
	stOut2DCal_Para.IntrParam.nImgWidth = _ttoi(inBuff);

// 	stOut2DCal_Para.IntrParam.nImgHeight					= 480;
	GetPrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_nImgHeight], _T("480"), inBuff, 255, szPath);
	stOut2DCal_Para.IntrParam.nImgHeight = _ttoi(inBuff);

// 	stOut2DCal_Para.IntrParam.nNumofCornerX					= 4;
	GetPrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_nNumofCornerX], _T("4"), inBuff, 255, szPath);
	stOut2DCal_Para.IntrParam.nNumofCornerX = _ttoi(inBuff);

// 	stOut2DCal_Para.IntrParam.nNumofCornerY					= 6;
	GetPrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_nNumofCornerY], _T("6"), inBuff, 255, szPath);
	stOut2DCal_Para.IntrParam.nNumofCornerY = _ttoi(inBuff);

// 	stOut2DCal_Para.IntrParam.fPatternSizeX					= 60.f;
	GetPrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_fPatternSizeX], _T("60.0"), inBuff, 255, szPath);
	stOut2DCal_Para.IntrParam.fPatternSizeX = (float)_ttof(inBuff);

// 	stOut2DCal_Para.IntrParam.fPatternSizeY					= 60.f;
	GetPrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_fPatternSizeY], _T("60.0"), inBuff, 255, szPath);
	stOut2DCal_Para.IntrParam.fPatternSizeY = (float)_ttof(inBuff);

// 	stOut2DCal_Para.IntrParam.fRepThres						= 1.f;
	GetPrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_fRepThres], _T("1.0"), inBuff, 255, szPath);
	stOut2DCal_Para.IntrParam.fRepThres = (float)_ttof(inBuff);

// 	stOut2DCal_Para.IntrParam.nMinNumImages					= 13;
	GetPrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_nMinNumImages], _T("13"), inBuff, 255, szPath);
	stOut2DCal_Para.IntrParam.nMinNumImages = _ttoi(inBuff);

// 	stOut2DCal_Para.IntrParam.nMinNumValidPnts				= 300;
	GetPrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_nMinNumValidPnts], _T("300"), inBuff, 255, szPath);
	stOut2DCal_Para.IntrParam.nMinNumValidPnts = _ttoi(inBuff);

// 	stOut2DCal_Para.IntrParam.fMinOriginOffset				= -4.f;
	GetPrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_fMinOriginOffset], _T("-4.0"), inBuff, 255, szPath);
	stOut2DCal_Para.IntrParam.fMinOriginOffset = (float)_ttof(inBuff);

// 	stOut2DCal_Para.IntrParam.fMaxOriginOffset				= 4.f;
	GetPrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_fMaxOriginOffset], _T("4.0"), inBuff, 255, szPath);
	stOut2DCal_Para.IntrParam.fMaxOriginOffset =(float) _ttof(inBuff);

// 	stOut2DCal_Para.IntrParam.fMinFocalLength				= 311.f;
	GetPrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_fMinFocalLength], _T("311.0"), inBuff, 255, szPath);
	stOut2DCal_Para.IntrParam.fMinFocalLength = (float)_ttof(inBuff);

// 	stOut2DCal_Para.IntrParam.fMaxFocalLength				= 314.f;
	GetPrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_fMaxFocalLength], _T("314.0"), inBuff, 255, szPath);
	stOut2DCal_Para.IntrParam.fMaxFocalLength = (float)_ttof(inBuff);

//  stOut2DCal_Para.IntrParam.stCalibInfo_default.arfKK[0]	= 310.f;
	GetPrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_arfKK_0], _T("310.0"), inBuff, 255, szPath);
	stOut2DCal_Para.IntrParam.stCalibInfo_default.arfKK[0] = (float)_ttof(inBuff);

//  stOut2DCal_Para.IntrParam.stCalibInfo_default.arfKK[1]	= 310.f;
	GetPrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_arfKK_1], _T("310.0"), inBuff, 255, szPath);
	stOut2DCal_Para.IntrParam.stCalibInfo_default.arfKK[1] = (float)_ttof(inBuff);

//  stOut2DCal_Para.IntrParam.stCalibInfo_default.arfKK[2]	= 320.f;
	GetPrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_arfKK_2], _T("320.0"), inBuff, 255, szPath);
	stOut2DCal_Para.IntrParam.stCalibInfo_default.arfKK[2] = (float)_ttof(inBuff);

//  stOut2DCal_Para.IntrParam.stCalibInfo_default.arfKK[3]	= 240.f;
	GetPrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_arfKK_3], _T("240.0"), inBuff, 255, szPath);
	stOut2DCal_Para.IntrParam.stCalibInfo_default.arfKK[3] = (float)_ttof(inBuff);

//  stOut2DCal_Para.IntrParam.stCalibInfo_default.arfKc[0]	= -0.2f;
	GetPrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_arfKc_0], _T("-0.2"), inBuff, 255, szPath);
	stOut2DCal_Para.IntrParam.stCalibInfo_default.arfKc[0] = (float)_ttof(inBuff);

//  stOut2DCal_Para.IntrParam.stCalibInfo_default.arfKc[1]	= 0.045f;
	GetPrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_arfKc_1], _T("0.045"), inBuff, 255, szPath);
	stOut2DCal_Para.IntrParam.stCalibInfo_default.arfKc[1] = (float)_ttof(inBuff);

//  stOut2DCal_Para.IntrParam.stCalibInfo_default.arfKc[2]	= 0.f;
	GetPrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_arfKc_2], _T("0.0"), inBuff, 255, szPath);
	stOut2DCal_Para.IntrParam.stCalibInfo_default.arfKc[2] = (float)_ttof(inBuff);

//  stOut2DCal_Para.IntrParam.stCalibInfo_default.arfKc[3]	= 0.f;
	GetPrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_arfKc_3], _T("0.0"), inBuff, 255, szPath);
	stOut2DCal_Para.IntrParam.stCalibInfo_default.arfKc[3] = (float)_ttof(inBuff);

//  stOut2DCal_Para.IntrParam.stCalibInfo_default.arfKc[4]	= -0.0045f;
	GetPrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_arfKc_4], _T("-0.0045"), inBuff, 255, szPath);
	stOut2DCal_Para.IntrParam.stCalibInfo_default.arfKc[4] = (float)_ttof(inBuff);

//  stOut2DCal_Para.IntrParam.stCalibInfo_default.fR2Max	= 4.4f;
	GetPrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_fR2Max], _T("4.4"), inBuff, 255, szPath);
	stOut2DCal_Para.IntrParam.stCalibInfo_default.fR2Max = (float)_ttof(inBuff);

// 	stOut2DCal_Para.IntrParam.fVersion						= 1.0f;
	GetPrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_fVersion], _T("1.0"), inBuff, 255, szPath);
	stOut2DCal_Para.IntrParam.fVersion = (float)_ttof(inBuff);

	for (UINT nPos = 0; nPos < MAX_2DCAL_ROI; nPos++)
	{
		strKeyName.Format(_T("ROI_%03d_U"),nPos);
		GetPrivateProfileString(AppName_2DCal_Para, strKeyName, _T("0"), inBuff, 255, szPath);
		stOut2DCal_Para.ROI[nPos].nU = _ttoi(inBuff);

		strKeyName.Format(_T("ROI_%03d_V"), nPos);
		GetPrivateProfileString(AppName_2DCal_Para, strKeyName, _T("0"), inBuff, 255, szPath);
		stOut2DCal_Para.ROI[nPos].nV = _ttoi(inBuff);

		strKeyName.Format(_T("ROI_%03d_W"), nPos);
		GetPrivateProfileString(AppName_2DCal_Para, strKeyName, _T("0"), inBuff, 255, szPath);
		stOut2DCal_Para.ROI[nPos].nW = _ttoi(inBuff);

		strKeyName.Format(_T("ROI_%03d_H"), nPos);
		GetPrivateProfileString(AppName_2DCal_Para, strKeyName, _T("0"), inBuff, 255, szPath);
		stOut2DCal_Para.ROI[nPos].nH = _ttoi(inBuff);
	}
	

	// * BPF Version (16 Byte) : 0x6E
	GetPrivateProfileString(AppName_2DCal_Para, _T("BPF_Version"), _T(""), inBuff, 255, szPath);
	stOut2DCal_Para.szBPFVer = inBuff;
	
	// * Gate Driver Version (16 Byte) : 0x7E
	GetPrivateProfileString(AppName_2DCal_Para, _T("Gate_Driver_Version"), _T(""), inBuff, 255, szPath);
	stOut2DCal_Para.szGateDriverVer = inBuff;


	GetPrivateProfileString(AppName_2DCal_Para, _T("Temperature"), _T("0"), inBuff, 255, szPath);
	stOut2DCal_Para.bTemperature = _ttoi(inBuff);

	return bReturn;
}

//=============================================================================
// Method		: Save_2DCal_Para
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_2DCal_Para * pst2DCal_Para
// Qualifier	:
// Last Update	: 2017/11/11 - 14:22
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Save_2DCal_Para(__in LPCTSTR szPath, __in const ST_2DCal_Para* pst2DCal_Para)
{
	if (NULL == szPath)
		return FALSE;

	BOOL bReturn = TRUE;
	CString strValue;
	CString strKeyName;

	// 	pst2DCal_Para->IntrParam.nImgWidth						= 640;
	strValue.Format(_T("%d"), pst2DCal_Para->IntrParam.nImgWidth);
	WritePrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_nImgWidth], strValue, szPath);

	// 	pst2DCal_Para->IntrParam.nImgHeight						= 480;
	strValue.Format(_T("%d"), pst2DCal_Para->IntrParam.nImgHeight);
	WritePrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_nImgHeight], strValue, szPath);

	// 	pst2DCal_Para->IntrParam.nNumofCornerX					= 4;
	strValue.Format(_T("%d"), pst2DCal_Para->IntrParam.nNumofCornerX);
	WritePrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_nNumofCornerX], strValue, szPath);

	// 	pst2DCal_Para->IntrParam.nNumofCornerY					= 6;
	strValue.Format(_T("%d"), pst2DCal_Para->IntrParam.nNumofCornerY);
	WritePrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_nNumofCornerY], strValue, szPath);

	// 	pst2DCal_Para->IntrParam.fPatternSizeX					= 60.f;
	strValue.Format(_T("%.5f"), pst2DCal_Para->IntrParam.fPatternSizeX);
	WritePrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_fPatternSizeX], strValue, szPath);

	// 	pst2DCal_Para->IntrParam.fPatternSizeY					= 60.f;
	strValue.Format(_T("%.5f"), pst2DCal_Para->IntrParam.fPatternSizeY);
	WritePrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_fPatternSizeY], strValue, szPath);

	// 	pst2DCal_Para->IntrParam.fRepThres						= 1.f;
	strValue.Format(_T("%.5f"), pst2DCal_Para->IntrParam.fRepThres);
	WritePrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_fRepThres], strValue, szPath);

	// 	pst2DCal_Para->IntrParam.nMinNumImages					= 13;
	strValue.Format(_T("%d"), pst2DCal_Para->IntrParam.nMinNumImages);
	WritePrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_nMinNumImages], strValue, szPath);

	// 	pst2DCal_Para->IntrParam.nMinNumValidPnts				= 300;
	strValue.Format(_T("%d"), pst2DCal_Para->IntrParam.nMinNumValidPnts);
	WritePrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_nMinNumValidPnts], strValue, szPath);

	// 	pst2DCal_Para->IntrParam.fMinOriginOffset				= -4.f;
	strValue.Format(_T("%.5f"), pst2DCal_Para->IntrParam.fMinOriginOffset);
	WritePrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_fMinOriginOffset], strValue, szPath);

	// 	pst2DCal_Para->IntrParam.fMaxOriginOffset				= 4.f;
	strValue.Format(_T("%.5f"), pst2DCal_Para->IntrParam.fMaxOriginOffset);
	WritePrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_fMaxOriginOffset], strValue, szPath);

	// 	pst2DCal_Para->IntrParam.fMinFocalLength				= 311.f;
	strValue.Format(_T("%.5f"), pst2DCal_Para->IntrParam.fMinFocalLength);
	WritePrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_fMinFocalLength], strValue, szPath);

	// 	pst2DCal_Para->IntrParam.fMaxFocalLength				= 314.f;
	strValue.Format(_T("%.5f"), pst2DCal_Para->IntrParam.fMaxFocalLength);
	WritePrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_fMaxFocalLength], strValue, szPath);

	//  pst2DCal_Para->IntrParam.stCalibInfo_default.arfKK[0]	= 310.f;
	strValue.Format(_T("%.5f"), pst2DCal_Para->IntrParam.stCalibInfo_default.arfKK[0]);
	WritePrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_arfKK_0], strValue, szPath);

	//  pst2DCal_Para->IntrParam.stCalibInfo_default.arfKK[1]	= 310.f;
	strValue.Format(_T("%.5f"), pst2DCal_Para->IntrParam.stCalibInfo_default.arfKK[1]);
	WritePrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_arfKK_1], strValue, szPath);

	//  pst2DCal_Para->IntrParam.stCalibInfo_default.arfKK[2]	= 320.f;
	strValue.Format(_T("%.5f"), pst2DCal_Para->IntrParam.stCalibInfo_default.arfKK[2]);
	WritePrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_arfKK_2], strValue, szPath);

	//  pst2DCal_Para->IntrParam.stCalibInfo_default.arfKK[3]	= 240.f;
	strValue.Format(_T("%.5f"), pst2DCal_Para->IntrParam.stCalibInfo_default.arfKK[3]);
	WritePrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_arfKK_3], strValue, szPath);

	//  pst2DCal_Para->IntrParam.stCalibInfo_default.arfKc[0]	= -0.2f;
	strValue.Format(_T("%.5f"), pst2DCal_Para->IntrParam.stCalibInfo_default.arfKc[0]);
	WritePrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_arfKc_0], strValue, szPath);

	//  pst2DCal_Para->IntrParam.stCalibInfo_default.arfKc[1]	= 0.045f;
	strValue.Format(_T("%.5f"), pst2DCal_Para->IntrParam.stCalibInfo_default.arfKc[1]);
	WritePrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_arfKc_1], strValue, szPath);

	//  pst2DCal_Para->IntrParam.stCalibInfo_default.arfKc[2]	= 0.f;
	strValue.Format(_T("%.5f"), pst2DCal_Para->IntrParam.stCalibInfo_default.arfKc[2]);
	WritePrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_arfKc_2], strValue, szPath);

	//  pst2DCal_Para->IntrParam.stCalibInfo_default.arfKc[3]	= 0.f;
	strValue.Format(_T("%.5f"), pst2DCal_Para->IntrParam.stCalibInfo_default.arfKc[3]);
	WritePrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_arfKc_3], strValue, szPath);

	//  pst2DCal_Para->IntrParam.stCalibInfo_default.arfKc[4]	= -0.0045f;
	strValue.Format(_T("%.5f"), pst2DCal_Para->IntrParam.stCalibInfo_default.arfKc[4]);
	WritePrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_arfKc_4], strValue, szPath);

	//  pst2DCal_Para->IntrParam.stCalibInfo_default.fR2Max		= 4.4f;
	strValue.Format(_T("%.5f"), pst2DCal_Para->IntrParam.stCalibInfo_default.fR2Max);
	WritePrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_fR2Max], strValue, szPath);

	// 	pst2DCal_Para->IntrParam.fVersion						= 1.0f;
	strValue.Format(_T("%.5f"), pst2DCal_Para->IntrParam.fVersion);
	WritePrivateProfileString(AppName_2DCal_Para, g_Param_2DCal[Param_2DCal_fVersion], strValue, szPath);

	for (UINT nPos = 0; nPos < MAX_2DCAL_ROI; nPos++)
	{
		strKeyName.Format(_T("ROI_%03d_U"), nPos);
		strValue.Format(_T("%d"), pst2DCal_Para->ROI[nPos].nU);
		WritePrivateProfileString(AppName_2DCal_Para, strKeyName, strValue, szPath);

		strKeyName.Format(_T("ROI_%03d_V"), nPos);
		strValue.Format(_T("%d"), pst2DCal_Para->ROI[nPos].nV);
		WritePrivateProfileString(AppName_2DCal_Para, strKeyName, strValue, szPath);

		strKeyName.Format(_T("ROI_%03d_W"), nPos);
		strValue.Format(_T("%d"), pst2DCal_Para->ROI[nPos].nW);
		WritePrivateProfileString(AppName_2DCal_Para, strKeyName, strValue, szPath);

		strKeyName.Format(_T("ROI_%03d_H"), nPos);
		strValue.Format(_T("%d"), pst2DCal_Para->ROI[nPos].nH);
		WritePrivateProfileString(AppName_2DCal_Para, strKeyName, strValue, szPath);
	}
	
	// * BPF Version (16 Byte) : 0x6E
	strValue = pst2DCal_Para->szBPFVer;
	WritePrivateProfileString(AppName_2DCal_Para, _T("BPF_Version"), strValue, szPath);

	// * Gate Driver Version (16 Byte) : 0x7E
	strValue = pst2DCal_Para->szGateDriverVer;
	WritePrivateProfileString(AppName_2DCal_Para, _T("Gate_Driver_Version"), strValue, szPath);

	strValue.Format(_T("%d"), pst2DCal_Para->bTemperature);
	WritePrivateProfileString(AppName_2DCal_Para, _T("Temperature"), strValue, szPath);

	return bReturn;
}

//=============================================================================
// Method		: Load_3DCal_Info
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_3DCal_Info & stOut3DCal_Info
// Qualifier	:
// Last Update	: 2017/11/12 - 20:33
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Load_3DCal_Info(__in LPCTSTR szPath, __out ST_3DCal_Info& stOut3DCal_Info)
{
	if (NULL == szPath)
		return FALSE;

	BOOL bReturn = TRUE;

	bReturn &= Load_3DCal_Depth_Para(szPath, stOut3DCal_Info.Depth_Param);
	bReturn &= Load_3DCal_Eval_Para(szPath, stOut3DCal_Info.Eval_Param);

	return TRUE;
}

//=============================================================================
// Method		: Save_3DCal_Info
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_3DCal_Info * pst3DCal_Info
// Qualifier	:
// Last Update	: 2017/11/12 - 20:33
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Save_3DCal_Info(__in LPCTSTR szPath, __in const ST_3DCal_Info* pst3DCal_Info)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pst3DCal_Info)
		return FALSE;

	BOOL bReturn = TRUE;

	bReturn &= Save_3DCal_Depth_Para(szPath, &pst3DCal_Info->Depth_Param);
	bReturn &= Save_3DCal_Eval_Para(szPath, &pst3DCal_Info->Eval_Param);

	return TRUE;
}

//=============================================================================
// Method		: Load_3DCal_Depth_Para
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_3DCal_Depth_Para & stOut3D_Depth_Para
// Qualifier	:
// Last Update	: 2017/11/12 - 20:22
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Load_3DCal_Depth_Para(__in LPCTSTR szPath, __out ST_3DCal_Depth_Para& stOut3D_Depth_Para)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };
	CString strKeyName;

	GetPrivateProfileString(AppName_3D_Depth_Para, _T("ImgWidth"),			_T("640"),	inBuff, 255, szPath);
	stOut3D_Depth_Para.nImgWidth		= _ttoi(inBuff);

	GetPrivateProfileString(AppName_3D_Depth_Para, _T("ImgHeight"),			_T("480"),	inBuff, 255, szPath);
	stOut3D_Depth_Para.nImgHeight		= _ttoi(inBuff);

	GetPrivateProfileString(AppName_3D_Depth_Para, _T("SamplingStep"),		_T("0"),	inBuff, 255, szPath);
	stOut3D_Depth_Para.nSamplingStep	= _ttoi(inBuff);

	GetPrivateProfileString(AppName_3D_Depth_Para, _T("NumofCornerX"),		_T("4"),	inBuff, 255, szPath);
	stOut3D_Depth_Para.nNumofCornerX	= _ttoi(inBuff);

	GetPrivateProfileString(AppName_3D_Depth_Para, _T("NumofCornerY"),		_T("6"),	inBuff, 255, szPath);
	stOut3D_Depth_Para.nNumofCornerY	= _ttoi(inBuff);

	GetPrivateProfileString(AppName_3D_Depth_Para, _T("ChessMarginX"),		_T("700"),	inBuff, 255, szPath);
	stOut3D_Depth_Para.fChessMarginX	= (float)_ttof(inBuff);

	GetPrivateProfileString(AppName_3D_Depth_Para, _T("ChessMarginY"),		_T("350"),	inBuff, 255, szPath);
	stOut3D_Depth_Para.fChessMarginY	= (float)_ttof(inBuff);

	GetPrivateProfileString(AppName_3D_Depth_Para, _T("PatternSizeX"),		_T("60"),	inBuff, 255, szPath);
	stOut3D_Depth_Para.fPatternSizeX	= (float)_ttof(inBuff);

	GetPrivateProfileString(AppName_3D_Depth_Para, _T("PatternSizeY"),		_T("60"),	inBuff, 255, szPath);
	stOut3D_Depth_Para.fPatternSizeY = (float)_ttof(inBuff);

	GetPrivateProfileString(AppName_3D_Depth_Para, _T("IrThres"),			_T("16"),	inBuff, 255, szPath);
	stOut3D_Depth_Para.nIrThres = _ttoi(inBuff);

	GetPrivateProfileString(AppName_3D_Depth_Para, _T("StdThres"),			_T("17"),	inBuff, 255, szPath);
	stOut3D_Depth_Para.fStdThres = (float)_ttof(inBuff);

	GetPrivateProfileString(AppName_3D_Depth_Para, _T("UseDepthFilter"),	_T("0"),	inBuff, 255, szPath);
	stOut3D_Depth_Para.nUseDepthFilter	= _ttoi(inBuff);	

	for (UINT nIdx = 0; nIdx < MAX_3DCAL_ROI; nIdx++)
	{
		strKeyName.Format(_T("ROI_%03d_U"), nIdx);
		GetPrivateProfileString(AppName_3D_Depth_Para, strKeyName, _T("0"), inBuff, 255, szPath);
		stOut3D_Depth_Para.ROI[nIdx].nU = _ttoi(inBuff);

		strKeyName.Format(_T("ROI_%03d_V"), nIdx);
		GetPrivateProfileString(AppName_3D_Depth_Para, strKeyName, _T("0"), inBuff, 255, szPath);
		stOut3D_Depth_Para.ROI[nIdx].nV = _ttoi(inBuff);

		strKeyName.Format(_T("ROI_%03d_W"), nIdx);
		GetPrivateProfileString(AppName_3D_Depth_Para, strKeyName, _T("0"), inBuff, 255, szPath);
		stOut3D_Depth_Para.ROI[nIdx].nW = _ttoi(inBuff);

		strKeyName.Format(_T("ROI_%03d_H"), nIdx);
		GetPrivateProfileString(AppName_3D_Depth_Para, strKeyName, _T("0"), inBuff, 255, szPath);
		stOut3D_Depth_Para.ROI[nIdx].nH = _ttoi(inBuff);
	}

	for (UINT nIdx = 0; nIdx < MAX_3DCAL_Re_DistCoef; nIdx++)
	{
		strKeyName.Format(_T("DistCoef_%03d"), nIdx);
		GetPrivateProfileString(AppName_3D_Depth_Para, strKeyName, _T("0"), inBuff, 255, szPath);
		stOut3D_Depth_Para.arfCoefTemp[nIdx] = (float)_ttof(inBuff);
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_3DCal_Depth_Para
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_3DCal_Depth_Para * pst3D_Depth_Para
// Qualifier	:
// Last Update	: 2017/11/12 - 20:22
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Save_3DCal_Depth_Para(__in LPCTSTR szPath, __in const ST_3DCal_Depth_Para* pst3D_Depth_Para)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pst3D_Depth_Para)
		return FALSE;

	CString strKeyName;
	CString strValue;

	strValue.Format(_T("%d"), pst3D_Depth_Para->nImgWidth);
	WritePrivateProfileString(AppName_3D_Depth_Para, _T("ImgWidth"),		strValue, szPath);

	strValue.Format(_T("%d"), pst3D_Depth_Para->nImgHeight);
	WritePrivateProfileString(AppName_3D_Depth_Para, _T("ImgHeight"),		strValue, szPath);

	strValue.Format(_T("%d"), pst3D_Depth_Para->nSamplingStep);
	WritePrivateProfileString(AppName_3D_Depth_Para, _T("SamplingStep"),	strValue, szPath);

	strValue.Format(_T("%d"), pst3D_Depth_Para->nNumofCornerX);
	WritePrivateProfileString(AppName_3D_Depth_Para, _T("NumofCornerX"),	strValue, szPath);

	strValue.Format(_T("%d"), pst3D_Depth_Para->nNumofCornerY);
	WritePrivateProfileString(AppName_3D_Depth_Para, _T("NumofCornerY"),	strValue, szPath);

	strValue.Format(_T("%.4f"), pst3D_Depth_Para->fChessMarginX);
	WritePrivateProfileString(AppName_3D_Depth_Para, _T("ChessMarginX"),	strValue, szPath);

	strValue.Format(_T("%.4f"), pst3D_Depth_Para->fChessMarginY);
	WritePrivateProfileString(AppName_3D_Depth_Para, _T("ChessMarginY"),	strValue, szPath);

	strValue.Format(_T("%.4f"), pst3D_Depth_Para->fPatternSizeX);
	WritePrivateProfileString(AppName_3D_Depth_Para, _T("PatternSizeX"),	strValue, szPath);

	strValue.Format(_T("%.4f"), pst3D_Depth_Para->fPatternSizeY);
	WritePrivateProfileString(AppName_3D_Depth_Para, _T("PatternSizeY"),	strValue, szPath);

	strValue.Format(_T("%d"), pst3D_Depth_Para->nIrThres);
	WritePrivateProfileString(AppName_3D_Depth_Para, _T("IrThres"),			strValue, szPath);

	strValue.Format(_T("%.4f"), pst3D_Depth_Para->fStdThres);
	WritePrivateProfileString(AppName_3D_Depth_Para, _T("StdThres"),		strValue, szPath);

	strValue.Format(_T("%d"), pst3D_Depth_Para->nUseDepthFilter);
	WritePrivateProfileString(AppName_3D_Depth_Para, _T("UseDepthFilter"),	strValue, szPath);

	for (UINT nIdx = 0; nIdx < MAX_3DCAL_Re_DistCoef; nIdx++)
	{
		strKeyName.Format(_T("DistCoef_%03d"), nIdx);
		strValue.Format(_T("%e"), pst3D_Depth_Para->arfCoefTemp[nIdx]);
		WritePrivateProfileString(AppName_3D_Depth_Para, strKeyName, strValue, szPath);
	}

	for (UINT nIdx = 0; nIdx < MAX_3DCAL_ROI; nIdx++)
	{
		strKeyName.Format(_T("ROI_%03d_U"), nIdx);
		strValue.Format(_T("%d"), pst3D_Depth_Para->ROI[nIdx].nU);
		WritePrivateProfileString(AppName_3D_Depth_Para, strKeyName, strValue, szPath);

		strKeyName.Format(_T("ROI_%03d_V"), nIdx);
		strValue.Format(_T("%d"), pst3D_Depth_Para->ROI[nIdx].nV);
		WritePrivateProfileString(AppName_3D_Depth_Para, strKeyName, strValue, szPath);

		strKeyName.Format(_T("ROI_%03d_W"), nIdx);
		strValue.Format(_T("%d"), pst3D_Depth_Para->ROI[nIdx].nW);
		WritePrivateProfileString(AppName_3D_Depth_Para, strKeyName, strValue, szPath);

		strKeyName.Format(_T("ROI_%03d_H"), nIdx);
		strValue.Format(_T("%d"), pst3D_Depth_Para->ROI[nIdx].nH);
		WritePrivateProfileString(AppName_3D_Depth_Para, strKeyName, strValue, szPath);
	}

	return TRUE;
}

//=============================================================================
// Method		: Load_3DCal_Eval_Para
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_3DCal_Eval_Para & stOut3D_Eval_Para
// Qualifier	:
// Last Update	: 2017/11/12 - 20:28
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Load_3DCal_Eval_Para(__in LPCTSTR szPath, __out ST_3DCal_Eval_Para& stOut3D_Eval_Para)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };
	CString strKeyName;

	GetPrivateProfileString(AppName_3D_Eval_Para, _T("ImgWidth"),			_T("640"), inBuff, 255, szPath);
	stOut3D_Eval_Para.nImgWidth			= _ttoi(inBuff);

	GetPrivateProfileString(AppName_3D_Eval_Para, _T("ImgHeight"),			_T("480"), inBuff, 255, szPath);
	stOut3D_Eval_Para.nImgHeight = _ttoi(inBuff);

	GetPrivateProfileString(AppName_3D_Eval_Para, _T("NumofCornerX"),		_T("0"), inBuff, 255, szPath);
	stOut3D_Eval_Para.nNumofCornerX = _ttoi(inBuff);

	GetPrivateProfileString(AppName_3D_Eval_Para, _T("NumofCornerY"),		_T("0"), inBuff, 255, szPath);
	stOut3D_Eval_Para.nNumofCornerY = _ttoi(inBuff);

	GetPrivateProfileString(AppName_3D_Eval_Para, _T("PatternSizeX"),		_T("0"), inBuff, 255, szPath);
	stOut3D_Eval_Para.fPatternSizeX = (float)_ttof(inBuff);

	GetPrivateProfileString(AppName_3D_Eval_Para, _T("PatternSizeY"),		_T("0"), inBuff, 255, szPath);
	stOut3D_Eval_Para.fPatternSizeY = (float)_ttof(inBuff);

	GetPrivateProfileString(AppName_3D_Eval_Para, _T("ChessMarginX"),		_T("0"), inBuff, 255, szPath);
	stOut3D_Eval_Para.fChessMarginX = (float)_ttof(inBuff);

	GetPrivateProfileString(AppName_3D_Eval_Para, _T("ChessMarginY"),		_T("0"), inBuff, 255, szPath);
	stOut3D_Eval_Para.fChessMarginY = (float)_ttof(inBuff);

	GetPrivateProfileString(AppName_3D_Eval_Para, _T("UseDepthFileter"),	_T("0"), inBuff, 255, szPath);
	stOut3D_Eval_Para.nUseDepthFileter = _ttoi(inBuff);

	GetPrivateProfileString(AppName_3D_Eval_Para, _T("IrThres"),			_T("0"), inBuff, 255, szPath);
	stOut3D_Eval_Para.nIrThres = _ttoi(inBuff);

	for (UINT nIdx = 0; nIdx < MAX_EVAL_ExternalGT; nIdx++)
	{
		strKeyName.Format(_T("ExternalGT_%03d"), nIdx);

		GetPrivateProfileString(AppName_3D_Eval_Para, strKeyName,			_T("0"), inBuff, 255, szPath);
		stOut3D_Eval_Para.fExternalGT[nIdx] = (float)_ttof(inBuff);
	}
	
	GetPrivateProfileString(AppName_3D_Eval_Para, _T("PatchSizeU"),			_T("0"), inBuff, 255, szPath);
	stOut3D_Eval_Para.nPatchSizeU = _ttoi(inBuff);

	GetPrivateProfileString(AppName_3D_Eval_Para, _T("PatchSizeV"),			_T("0"), inBuff, 255, szPath);
	stOut3D_Eval_Para.nPatchSizeV = _ttoi(inBuff);

	GetPrivateProfileString(AppName_3D_Eval_Para, _T("PatchStepU"),			_T("0"), inBuff, 255, szPath);
	stOut3D_Eval_Para.nPatchStepU = _ttoi(inBuff);

	GetPrivateProfileString(AppName_3D_Eval_Para, _T("PatchStepV"),			_T("0"), inBuff, 255, szPath);
	stOut3D_Eval_Para.nPatchStepV = _ttoi(inBuff);

	GetPrivateProfileString(AppName_3D_Eval_Para, _T("ShadeROIH"),			_T("0"), inBuff, 255, szPath);
	stOut3D_Eval_Para.nShadeROIH = _ttoi(inBuff);

	GetPrivateProfileString(AppName_3D_Eval_Para, _T("ShadeROIV"),			_T("0"), inBuff, 255, szPath);
	stOut3D_Eval_Para.nShadeROIV = _ttoi(inBuff);

	GetPrivateProfileString(AppName_3D_Eval_Para, _T("ShadeROID"),			_T("0"), inBuff, 255, szPath);
	stOut3D_Eval_Para.nShadeROID		= _ttoi(inBuff);

	GetPrivateProfileString(AppName_3D_Eval_Para, _T("ShadeIndex"),			_T("1"), inBuff, 255, szPath);
	stOut3D_Eval_Para.nShadeIndex = _ttoi(inBuff);

	for (UINT nIdx = 0; nIdx < MAX_TI_3DCAL_Eval_ROI; nIdx++)
	{
		strKeyName.Format(_T("ROI_%03d_U"), nIdx);
		GetPrivateProfileString(AppName_3D_Eval_Para, strKeyName, _T("0"), inBuff, 255, szPath);
		stOut3D_Eval_Para.ROI[nIdx].nU = _ttoi(inBuff);

		strKeyName.Format(_T("ROI_%03d_V"), nIdx);
		GetPrivateProfileString(AppName_3D_Eval_Para, strKeyName, _T("0"), inBuff, 255, szPath);
		stOut3D_Eval_Para.ROI[nIdx].nV = _ttoi(inBuff);

		strKeyName.Format(_T("ROI_%03d_W"), nIdx);
		GetPrivateProfileString(AppName_3D_Eval_Para, strKeyName, _T("0"), inBuff, 255, szPath);
		stOut3D_Eval_Para.ROI[nIdx].nW = _ttoi(inBuff);

		strKeyName.Format(_T("ROI_%03d_H"), nIdx);
		GetPrivateProfileString(AppName_3D_Eval_Para, strKeyName, _T("0"), inBuff, 255, szPath);
		stOut3D_Eval_Para.ROI[nIdx].nH = _ttoi(inBuff);
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_3DCal_Eval_Para
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_3DCal_Eval_Para * pst3D_Eval_Para
// Qualifier	:
// Last Update	: 2017/11/12 - 20:30
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Save_3DCal_Eval_Para(__in LPCTSTR szPath, __in const ST_3DCal_Eval_Para* pst3D_Eval_Para)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pst3D_Eval_Para)
		return FALSE;

	CString strKeyName;
	CString strValue;
	
	strValue.Format(_T("%d"), pst3D_Eval_Para->nImgWidth);
	WritePrivateProfileString(AppName_3D_Eval_Para, _T("ImgWidth"),		strValue, szPath);

	strValue.Format(_T("%d"), pst3D_Eval_Para->nImgHeight);
	WritePrivateProfileString(AppName_3D_Eval_Para, _T("ImgHeight"),		strValue, szPath);

	strValue.Format(_T("%d"), pst3D_Eval_Para->nNumofCornerX);
	WritePrivateProfileString(AppName_3D_Eval_Para, _T("NumofCornerX"),	strValue, szPath);
	
	strValue.Format(_T("%d"), pst3D_Eval_Para->nNumofCornerY);
	WritePrivateProfileString(AppName_3D_Eval_Para, _T("NumofCornerY"), strValue, szPath);
	
	strValue.Format(_T("%.4f"), pst3D_Eval_Para->fPatternSizeX);
	WritePrivateProfileString(AppName_3D_Eval_Para, _T("PatternSizeX"), strValue, szPath);
	
	strValue.Format(_T("%.4f"), pst3D_Eval_Para->fPatternSizeY);
	WritePrivateProfileString(AppName_3D_Eval_Para, _T("PatternSizeY"), strValue, szPath);
	
	strValue.Format(_T("%.4f"), pst3D_Eval_Para->fChessMarginX);
	WritePrivateProfileString(AppName_3D_Eval_Para, _T("ChessMarginX"), strValue, szPath);
	
	strValue.Format(_T("%.4f"), pst3D_Eval_Para->fChessMarginY);
	WritePrivateProfileString(AppName_3D_Eval_Para, _T("ChessMarginY"), strValue, szPath);
	
	strValue.Format(_T("%d"), pst3D_Eval_Para->nUseDepthFileter);
	WritePrivateProfileString(AppName_3D_Eval_Para, _T("UseDepthFileter"), strValue, szPath);
	
	strValue.Format(_T("%d"), pst3D_Eval_Para->nIrThres);
	WritePrivateProfileString(AppName_3D_Eval_Para, _T("IrThres"), strValue, szPath);
	
	for (UINT nIdx = 0; nIdx < MAX_EVAL_ExternalGT; nIdx++)
	{
		strKeyName.Format(_T("ExternalGT_%03d"), nIdx);

		strValue.Format(_T("%.2f"), pst3D_Eval_Para->fExternalGT[nIdx]);
		WritePrivateProfileString(AppName_3D_Eval_Para, strKeyName, strValue, szPath);
	}

	strValue.Format(_T("%d"), pst3D_Eval_Para->nPatchSizeU);
	WritePrivateProfileString(AppName_3D_Eval_Para, _T("PatchSizeU"), strValue, szPath);
	
	strValue.Format(_T("%d"), pst3D_Eval_Para->nPatchSizeV);
	WritePrivateProfileString(AppName_3D_Eval_Para, _T("PatchSizeV"), strValue, szPath);
	
	strValue.Format(_T("%d"), pst3D_Eval_Para->nPatchStepU);
	WritePrivateProfileString(AppName_3D_Eval_Para, _T("PatchStepU"), strValue, szPath);
	
	strValue.Format(_T("%d"), pst3D_Eval_Para->nPatchStepV);
	WritePrivateProfileString(AppName_3D_Eval_Para, _T("PatchStepV"), strValue, szPath);
	
	strValue.Format(_T("%d"), pst3D_Eval_Para->nShadeROIH);
	WritePrivateProfileString(AppName_3D_Eval_Para, _T("ShadeROIH"), strValue, szPath);
	
	strValue.Format(_T("%d"), pst3D_Eval_Para->nShadeROIV);
	WritePrivateProfileString(AppName_3D_Eval_Para, _T("ShadeROIV"), strValue, szPath);

	strValue.Format(_T("%d"), pst3D_Eval_Para->nShadeIndex);
	WritePrivateProfileString(AppName_3D_Eval_Para, _T("ShadeIndex"), strValue, szPath);
	
	strValue.Format(_T("%d"), pst3D_Eval_Para->nShadeROID);
	WritePrivateProfileString(AppName_3D_Eval_Para, _T("ShadeROID"), strValue, szPath);

	for (UINT nIdx = 0; nIdx < MAX_TI_3DCAL_Eval_ROI; nIdx++)
	{
		strKeyName.Format(_T("ROI_%03d_U"), nIdx);
		strValue.Format(_T("%d"), pst3D_Eval_Para->ROI[nIdx].nU);
		WritePrivateProfileString(AppName_3D_Eval_Para, strKeyName, strValue, szPath);
		
		strKeyName.Format(_T("ROI_%03d_V"), nIdx);
		strValue.Format(_T("%d"), pst3D_Eval_Para->ROI[nIdx].nV);
		WritePrivateProfileString(AppName_3D_Eval_Para, strKeyName, strValue, szPath);

		strKeyName.Format(_T("ROI_%03d_W"), nIdx);
		strValue.Format(_T("%d"), pst3D_Eval_Para->ROI[nIdx].nW);
		WritePrivateProfileString(AppName_3D_Eval_Para, strKeyName, strValue, szPath);

		strKeyName.Format(_T("ROI_%03d_H"), nIdx);
		strValue.Format(_T("%d"), pst3D_Eval_Para->ROI[nIdx].nH);
		WritePrivateProfileString(AppName_3D_Eval_Para, strKeyName, strValue, szPath);
	}

	return TRUE;
}

//=============================================================================
// Method		: Load_MES_Info
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_MES_Result_Info & stMES_ResultInfo
// Qualifier	:
// Last Update	: 2017/12/6 - 17:50
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Load_MES_Info(__in LPCTSTR szPath, __out ST_MES_Result_Info& stMES_ResultInfo)
{
	if (NULL == szPath)
		return FALSE;

	BOOL bReturn = TRUE;
	TCHAR   inBuff[255] = { 0, };
	CString strKeyName;

// 	for (UINT nIdx = 0; nIdx < stMES_ResultInfo.ItemList.GetCount(); nIdx++)
// 	{
// 		strKeyName.Format(_T("Item_%03d"), nIdx);
// 
// 		GetPrivateProfileString(AppName_MES_Result, strKeyName, _T("1"), inBuff, 255, szPath);
// 		stMES_ResultInfo.ItemList[nIdx].bUse = _ttoi(inBuff);
// 	}

	return TRUE;
}

//=============================================================================
// Method		: Save_MES_Info
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_MES_Result_Info * pstMES_ResultInfo
// Qualifier	:
// Last Update	: 2017/12/6 - 17:50
// Desc.		:
//=============================================================================
BOOL CFile_Recipe_Cm::Save_MES_Info(__in LPCTSTR szPath, __in const ST_MES_Result_Info* pstMES_ResultInfo)
{
	if (NULL == szPath)
		return FALSE;

	BOOL bReturn = TRUE;
	CString strValue;
	CString strKeyName;

// 	for (UINT nIdx = 0; nIdx < pstMES_ResultInfo->ItemList.GetCount(); nIdx++)
// 	{
// 		strKeyName.Format(_T("Item_%03d"), nIdx);
// 
// 		strValue.Format(_T("%d"), pstMES_ResultInfo->ItemList[nIdx].bUse);
// 		WritePrivateProfileString(AppName_MES_Result, strKeyName, strValue, szPath);
// 	}

	return TRUE;
}
