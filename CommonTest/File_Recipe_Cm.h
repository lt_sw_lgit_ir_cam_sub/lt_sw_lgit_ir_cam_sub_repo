//*****************************************************************************
// Filename	: 	File_Recipe_Cm.h
// Created	:	2017/9/25 - 21:02
// Modified	:	2017/9/25 - 21:02
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef File_Recipe_Cm_h__
#define File_Recipe_Cm_h__

#pragma once

#include "Markup.h"

#include "Def_TestItem_Cm.h"
#include "Def_DataStruct_Cm.h"

#include "Def_T_Foc.h"
#include "Def_T_2DCal.h"
#include "Def_T_3DCal.h"
#include "Def_T_Current.h"


//-----------------------------------------------------------------------------
// CFile_Recipe
//-----------------------------------------------------------------------------
class CFile_Recipe_Cm
{
public:
	CFile_Recipe_Cm();
	~CFile_Recipe_Cm();

protected:

	CMarkup				m_xml;

	// 검사기 설정
	enInsptrSysType		m_InspectionType = enInsptrSysType::Sys_Focusing;

	// 최종 파일 저장
	BOOL			Save_XML_String		(__in CString szFullPath, __in CString szXML);

public:

	// 검사기 종류 설정
	void			SetSystemType		(__in enInsptrSysType nSysType);

	// 레시피 설정값
	virtual BOOL	Load_RecipeFile		(__in LPCTSTR szPath, __out ST_RecipeInfo_Base& stRecipeInfo);
	virtual BOOL	Save_RecipeFile		(__in LPCTSTR szPath, __in const ST_RecipeInfo_Base* pstRecipeInfo);

	// 공통 사항
	virtual BOOL	Load_Common			(__in LPCTSTR szPath, __out ST_RecipeInfo_Base& stRecipeInfo);
	virtual BOOL	Save_Common			(__in LPCTSTR szPath, __in const ST_RecipeInfo_Base* pstRecipeInfo);

	// 검사 스텝 정보
	BOOL	Load_StepInfo				(__in LPCTSTR szPath, __out ST_StepInfo& stStepInfo);
	BOOL	Save_StepInfo				(__in LPCTSTR szPath, __in const ST_StepInfo* pstStepInfo);

	BOOL	LoadXML_StepInfo			(__in LPCTSTR szPath, __out ST_StepInfo& stStepInfo);
	BOOL	SaveXML_StepInfo			(__in LPCTSTR szPath, __in const ST_StepInfo* pstStepInfo, __in enInsptrSysType nSysType);
	BOOL	SaveXML_StepInfo_Old		(__in LPCTSTR szPath, __in const ST_StepInfo* pstStepInfo);

	BOOL	Load_PresetInfo				(__in LPCTSTR szPath, __out ST_PresetStepInfo& stPresetStepInfo);
	BOOL	Save_PresetInfo				(__in LPCTSTR szPath, __in const ST_PresetStepInfo* pstPresetStepInfo, __in enInsptrSysType nSysType);
	BOOL	LoadXML_PresetStepInfo		(__in LPCTSTR szPath, __out ST_PresetStepInfo& stPresetStepInfo);
	BOOL	SaveXML_PresetStepInfo		(__in LPCTSTR szPath, __in const ST_PresetStepInfo* pstPresetStepInfo, __in enInsptrSysType nSysType);

	// 소모품 사용 정보
	BOOL	Load_ConsumInfoFile			(__in LPCTSTR szPath, __out ST_ConsumablesInfo& stConsumInfo);
	BOOL	Save_ConsumInfoFile			(__in LPCTSTR szPath, __in const ST_ConsumablesInfo* pstConsumInfo);
	BOOL	Load_ConsumInfo				(__in LPCTSTR szPath, __in UINT nItemIdx, __out DWORD& dwCount);
	BOOL	Save_ConsumInfo				(__in LPCTSTR szPath, __in UINT nItemIdx, __in DWORD dwCount);	

	// 검사 스펙
	BOOL	Load_TestItemInfo			(__in LPCTSTR szPath, __out ST_TestItemInfo& stOutTestItemInfo);
	BOOL	Save_TestItemInfo			(__in LPCTSTR szPath, __in const ST_TestItemInfo* pstTestItemInfo);
	
	// 화질 검사 ---------------------------------------------------------------

 	// 전류
 	BOOL	Load_CurrentFile			(__in LPCTSTR szPath, __out ST_RecipeInfo_Base& stRecipeInfo);
	BOOL	Save_CurrentFile			(__in LPCTSTR szPath, __in const ST_RecipeInfo_Base* pstRecipeInfo);
	
	// VCSel
	BOOL	Load_VCSelFile				(__in LPCTSTR szPath, __out ST_RecipeInfo_Base& stRecipeInfo);
	BOOL	Save_VCSelFile				(__in LPCTSTR szPath, __in const ST_RecipeInfo_Base* pstRecipeInfo);

	// Focusing -----------------------------------------------------------------
	BOOL	Load_Focusing_Info			(__in LPCTSTR szPath, __out ST_Foc_Info& stWipID);
	BOOL	Save_Focusing_Info			(__in LPCTSTR szPath, __in const ST_Foc_Info* pstWipID);

	// 2D Calibration ---------------------------------------------------------
	BOOL	Load_2DCal_Info				(__in LPCTSTR szPath, __out ST_2DCal_Info& stOut2DCal_Info);
	BOOL	Save_2DCal_Info				(__in LPCTSTR szPath, __in const ST_2DCal_Info* pst2DCal_Info);

	BOOL	Load_2DCal_Para				(__in LPCTSTR szPath, __out ST_2DCal_Para& stOut2DCal_Para);
	BOOL	Save_2DCal_Para				(__in LPCTSTR szPath, __in const ST_2DCal_Para* pst2DCal_Para);
	

	// 3D Calibration ---------------------------------------------------------
	BOOL	Load_3DCal_Info				(__in LPCTSTR szPath, __out ST_3DCal_Info& stOut3DCal_Info);
	BOOL	Save_3DCal_Info				(__in LPCTSTR szPath, __in const ST_3DCal_Info* pst3DCal_Info);

	BOOL	Load_3DCal_Depth_Para		(__in LPCTSTR szPath, __out ST_3DCal_Depth_Para& stOut3D_Depth_Para);
	BOOL	Save_3DCal_Depth_Para		(__in LPCTSTR szPath, __in const ST_3DCal_Depth_Para* pst3D_Depth_Para);

	BOOL	Load_3DCal_Eval_Para		(__in LPCTSTR szPath, __out ST_3DCal_Eval_Para& stOut3D_Eval_Para);
	BOOL	Save_3DCal_Eval_Para		(__in LPCTSTR szPath, __in const ST_3DCal_Eval_Para* pst3D_Eval_Para);

	// MES Selection ---------------------------------------------------------
	BOOL	Load_MES_Info				(__in LPCTSTR szPath, __out ST_MES_Result_Info& stMES_ResultInfo);
	BOOL	Save_MES_Info				(__in LPCTSTR szPath, __in const ST_MES_Result_Info* pstMES_ResultInfo);

};

#endif // File_Recipe_h__
