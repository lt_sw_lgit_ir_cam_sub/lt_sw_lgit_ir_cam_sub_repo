﻿// List_CurrentRt.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_CurrentRt.h"

// CList_CurrentRt
IMPLEMENT_DYNAMIC(CList_CurrentRt, CListCtrl)

CList_CurrentRt::CList_CurrentRt()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
}

CList_CurrentRt::~CList_CurrentRt()
{
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CList_CurrentRt, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

// CList_CurrentRt 메시지 처리기입니다.
int CList_CurrentRt::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);
	
	InitHeader();
	this->GetHeaderCtrl()->EnableWindow(FALSE);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_CurrentRt::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iColWidth[CuRt_MaxCol] = { 0, };
	int iColDivide = 0;
	int iUnitWidth = 0;
	int iMisc = 0;

	for (int nCol = 0; nCol < CuRt_MaxCol; nCol++)
	{
		iColDivide += iHeaderWidth_CurrentRt[nCol];
	}

	CRect rectClient;
	GetClientRect(rectClient);

	for (int nCol = 0; nCol < CuRt_MaxCol; nCol++)
	{
		iUnitWidth = (rectClient.Width() * iHeaderWidth_CurrentRt[nCol]) / iColDivide;
		iMisc += iUnitWidth;
		SetColumnWidth(nCol, iUnitWidth);
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_CurrentRt::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/20 - 14:21
// Desc.		:
//=============================================================================
void CList_CurrentRt::InitHeader()
{
	for (int nCol = 0; nCol < CuRt_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_CurrentRt[nCol], iListAglin_CurrentRt[nCol], iHeaderWidth_CurrentRt[nCol]);
	}

	for (int nCol = 0; nCol < CuRt_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_CurrentRt[nCol]);
	}

	for (int nCol = 0; nCol < CuRt_ItemNum; nCol++)
	{
		CString strValue;

		InsertItem(nCol, _T(""));

		strValue.Format(_T("%s"), g_lpszItem_CurrentRt[nCol]);
		SetItemText(nCol, CuRt_Object, strValue);
	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_TI_Current * pstCurrent
// Qualifier	:
// Last Update	: 2017/10/21 - 12:38
// Desc.		:
//=============================================================================
void CList_CurrentRt::SetRectRow(__in const ST_Current_Data *pstCurrent)
{
	DeleteAllItems();

// 	for (int nCol = 0; nCol < CuRt_ItemNum; nCol++)
// 	{
// 		CString strValue;
// 
// 		InsertItem(nCol, _T(""));
// 
// 		strValue.Format(_T("%s"), g_lpszItem_CurrentRt[nCol]);
// 		SetItemText(nCol, CuRt_Object, strValue);
// 		
// 		switch (nCol)
// 		{
// 		case CuRt_Current:
// 			strValue.Format(_T("%d"), pstCurrent->nCurrent);
// 			SetItemText(nCol, CuRt_Channel, strValue);
// 			break;
// 
// 		case CuRt_Result:
// 			if (TER_Pass == pstCurrent->nResult)
// 				strValue = _T("PASS");
// 			else
// 				strValue = _T("FAIL");
// 			SetItemText(nCol, CuRt_Channel, strValue);
// 			break;
// 		default:
// 			break;
// 		}
// 	}
}

//=============================================================================
// Method		: SetRectClear
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/21 - 14:23
// Desc.		:
//=============================================================================
void CList_CurrentRt::SetRectClear()
{
	DeleteAllItems();

	for (int nCol = 0; nCol < CuRt_ItemNum; nCol++)
	{
		CString strValue;

		InsertItem(nCol, _T(""));

		strValue.Format(_T("%s"), g_lpszItem_CurrentRt[nCol]);
		SetItemText(nCol, CuRt_Object, strValue);
	}
}
