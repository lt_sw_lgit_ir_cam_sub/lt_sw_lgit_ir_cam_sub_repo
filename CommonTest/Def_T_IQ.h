//*****************************************************************************
// Filename	: 	Def_T_IQ.h
// Created	:	2017/11/13 - 11:51
// Modified	:	2017/11/13 - 11:51
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_T_IQ_h__
#define Def_T_IQ_h__

#include <afxwin.h>

#include "Def_T_ECurrent.h"
#include "Def_T_OpticalCenter.h"
#include "Def_T_Distortion.h"
#include "Def_T_Dynamic.h"
#include "Def_T_Rotate.h"
#include "Def_T_SFR.h"
#include "Def_T_FOV.h"
#include "Def_T_DefectPixel.h"
#include "Def_T_Shading.h"
#include "Def_T_SNR_Light.h"
#include "Def_T_Particle.h"
#include "Def_T_Particle_Entry.h"
#include "Def_T_Intensity.h"
#include "Def_T_HotPixel.h"
#include "Def_T_3D_Depth.h"
#include "Def_T_FPN.h"
#include "Def_T_EEPROM_Verify.h"
#include "Def_T_TemperatureSensor.h"

#pragma pack(push,1)


typedef enum enSpec_IQ
{
	Spec_IQ_TI01_Result = 0,
	Spec_IQ_TI02_Result = 0,
	Spec_IQ_TI03_Result = 0,
	Spec_IQ_TI04_Result = 0,
	Spec_IQ_TI05_Result = 0,
	Spec_IQ_MaxNum
};

static LPCTSTR	g_szSpec_IQ[] =
{
	_T(""),
	_T(""),
	NULL
};

typedef enum enSpec_Pat
{
	Spec_Pat_TI01_Result = 0,
	Spec_Pat_TI02_Result = 0,
	Spec_Pat_TI03_Result = 0,
	Spec_Pat_TI04_Result = 0,
	Spec_Pat_TI05_Result = 0,
	Spec_Pat_MaxNum
};

static LPCTSTR	g_szSpec_Pat[] =
{
	_T(""),
	_T(""),
	NULL
};

typedef struct _tag_IQ_Result
{
	ST_ECurrent_Data		stECurrentData;
	ST_OpticalCenter_Data	stOpticalCenterData;
	ST_Rotate_Data			stRotateData;
	ST_Distortion_Data		stDistortionData;
	ST_FOV_Data				stFovData;
	ST_Dynamic_Data			stDynamicData;
	ST_DefectPixel_Data		stDefectPixelData;
	//ST_SFR_Data				stSFRData;
	ST_SFR_Data				stSFRData[SFR_TestNum_MaxEnum];
	ST_SNR_Light_Data		stSNR_LightData;
	ST_Intensity_Data		stIntensityData;
	ST_Shading_Data			stShadingData;
	ST_Particle_Data		stParticleData;
	ST_HotPixel_Data		stHotPixelData;
	ST_3D_Depth_Data		st3D_DepthData;
	ST_FPN_Data				stFPNData;
	ST_EEPROM_Verify_Data	stEEPROM_VerifyData;
	ST_TemperatureSensor_Data	stTemperatureSensorData;
	ST_Particle_Entry_Data	stParticle_EntryData;

	CString szTestFileName;

	_tag_IQ_Result()
	{
		Reset();
	};

	_tag_IQ_Result& operator= ( _tag_IQ_Result& ref)
	{

		for (int t = 0; t < SFR_TestNum_MaxEnum; t++){
			stSFRData[t] = ref.stSFRData[t];
		}
		stECurrentData = ref.stECurrentData;
		stOpticalCenterData = ref.stOpticalCenterData;
		stRotateData = ref.stRotateData;
		stDistortionData = ref.stDistortionData;
		stFovData = ref.stFovData;
		stDynamicData = ref.stDynamicData;
		stDefectPixelData = ref.stDefectPixelData;
		//stSFRData = ref.stSFRData;
		stSNR_LightData = ref.stSNR_LightData;
		stIntensityData = ref.stIntensityData;
		stShadingData = ref.stShadingData;
		stParticleData = ref.stParticleData;
		stHotPixelData = ref.stHotPixelData;
		st3D_DepthData = ref.st3D_DepthData;
		stFPNData = ref.stFPNData;
		stEEPROM_VerifyData = ref.stEEPROM_VerifyData;
		stTemperatureSensorData = ref.stTemperatureSensorData;
		
		szTestFileName = ref.szTestFileName;

		return *this;
	};

	void Reset()
	{
		for (int t = 0; t < SFR_TestNum_MaxEnum; t++){
			stSFRData[t].Reset();
		}

		stECurrentData.Reset();											
		stOpticalCenterData.Reset();
		stRotateData.Reset();
		stDistortionData.Reset();
		stFovData.Reset();
		stDynamicData.Reset();
		stDefectPixelData.Reset();
		//stSFRData.Reset();
		stSNR_LightData.Reset();
		stIntensityData.Reset();
		stShadingData.Reset();
		stParticleData.Reset();
		stHotPixelData.Reset();
		st3D_DepthData.Reset();
		stFPNData.Reset();
		stEEPROM_VerifyData.Reset();
		szTestFileName.Empty();
		stTemperatureSensorData.Reset();
	};

}ST_IQ_Result, *PST_IQ_Result;

typedef struct _tag_IQ_Spec
{
	_tag_IQ_Spec()
	{
		Reset();
	};

	_tag_IQ_Spec& operator= (const _tag_IQ_Spec& ref)
	{

		return *this;
	};

	void Reset()
	{
		
	};

}ST_IQ_Spec, *PST_IQ_Spec;

typedef struct _tag_IQ_Info
{
	// �Ķ����
	ST_ECurrent_Opt			stECurrentOpt;
	ST_OpticalCenter_Opt	stOpticalCenterOpt;
	ST_Rotate_Opt			stRotateOpt;
	ST_Distortion_Opt		stDistortionOpt;
	ST_FOV_Opt				stFovOpt;
	ST_Dynamic_Opt			stDynamicOpt;
	ST_DefectPixel_Opt		stDefectPixelOpt;
	//ST_SFR_Opt				stSFROpt;
	ST_SFR_Opt				stSFROpt[SFR_TestNum_MaxEnum];
	ST_SNR_Light_Opt		stSNR_LightOpt;
	ST_Intensity_Opt		stIntensityOpt;
	ST_Shading_Opt			stShadingOpt;
	ST_Particle_Opt			stParticleOpt;
	ST_HotPixel_Opt			stHotPixelOpt;
	ST_3D_Depth_Opt			st3D_DepthOpt;
	ST_FPN_Opt		stFPNOpt;
	ST_EEPROM_Verify_Opt	stEEPROM_VerifyOpt;
	ST_TemperatureSensor_Opt	stTemperatureSensorOpt;
	ST_Particle_Entry_Opt	stParticle_EntryOpt;

	_tag_IQ_Info()
	{
		Reset();
	};

	_tag_IQ_Info& operator= (const _tag_IQ_Info& ref)
	{
		return *this;
	};

	void Reset()
	{
		
	};

}ST_IQ_Info, *PST_IQ_Info;




#pragma pack(pop)

#endif // Def_T_IQ_h__
