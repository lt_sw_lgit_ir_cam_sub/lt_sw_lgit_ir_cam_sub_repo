﻿//*****************************************************************************
// Filename	: 	Grid_TactTime.h
// Created	:	2016/11/14 - 17:59
// Modified	:	2016/11/14 - 17:59
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Grid_TactTime_h__
#define Grid_TactTime_h__

#pragma once

#include "Grid_Base.h"
#include "Def_DataStruct_Cm.h"

class CGrid_TactTime : public CGrid_Base
{
public:
	CGrid_TactTime();
	virtual ~CGrid_TactTime();

protected:
	virtual void	OnSetup			();
	virtual int		OnHint			(int col, long row, int section, CString *string);
	virtual void	OnGetCell		(int col, long row, CUGCell *cell);
	virtual void	OnDrawFocusRect	(CDC *dc, RECT *rect);

	// 그리드 외형 및 내부 문자열을 채운다.
	virtual void	DrawGridOutline	();

	// 셀 갯수 가변에 따른 다시 그리기 위한 함수
	virtual void	CalGridOutline	();

	// 헤더를 초기화
	void			InitHeader		();

	CFont		m_font_Header;
	CFont		m_font_Data;

	UINT		m_nSocketCTCount;

public:

	void		SetUseSocketCTCount	(__in UINT nCount);
	void		SetTactTime			(__in const ST_CycleTime* pstCycleTime);

};

#endif // Grid_TactTime_h__

