//*****************************************************************************
// Filename	: 	MES_LGIT.h
// Created	:	2018/3/1 - 14:48
// Modified	:	2018/3/1 - 14:48
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef MES_LGIT_h__
#define MES_LGIT_h__

#pragma once

#include "FileMES_Base.hpp"

//=============================================================================
// CFileMES_LGIT
//=============================================================================
class CFileMES_LGIT : public CFileMES_Base
{
public:
	CFileMES_LGIT();
	virtual ~CFileMES_LGIT();

protected:
	
	virtual CString		Make_Dataz				();

	virtual CString		Make_Header				();

	virtual CString		Get_MESFileData			();

	virtual BOOL		Get_MESFileData			(__out LPBYTE lpbyOutDATA, __out DWORD& dwOutDataSize);

	virtual BOOL		SaveMESFile_Unicode		();
	virtual BOOL		SaveMESFile_ANSI		();
	
	// Check Test Item Fail Judge
	virtual void		Remake_TestItem_Judge	();

public:

	virtual void		Reset_ItemData			();

	virtual BOOL		Add_ItemData			(__in LPCTSTR szName, __in LPCTSTR szValue, __in BOOL bPass = TRUE);

	
	
	virtual BOOL		SaveMESFile				(__in LPCTSTR szInBarcode, __in UINT nInRetryCnt, __in BOOL bInJudgment, __in const SYSTEMTIME* pstTime);
	
};

#endif // MES_LGIT_h__
