
#ifndef Wnd_Cfg_3DCal_Evalu_h__
#define Wnd_Cfg_3DCal_Evalu_h__

// CWnd_Cfg_3D_Eval
#pragma once

#include "Def_T_3DCal.h"
#include "VGStatic.h"
#include "List_3DCal_Eval_ROI.h"

class CWnd_Cfg_3D_Eval : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Cfg_3D_Eval)

public:
	CWnd_Cfg_3D_Eval();
	virtual ~CWnd_Cfg_3D_Eval();

protected:
	afx_msg int		OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow(CREATESTRUCT& cs);

	DECLARE_MESSAGE_MAP()
	CFont				m_font;

	// 세팅 파라미터 옵션
	CVGStatic			m_st_Param_Eval[Param_3D_Eval_MaxNum];
	CMFCMaskedEdit		m_ed_Param_Eval[Param_3D_Eval_MaxNum];

	// ROI 영역
	CList_3DCal_Eval_ROI	m_lst_Eval_ROI;

public:

	void	Set_3DCalInfo(__in const ST_3DCal_Op* st2DCalOp);
	void	Get_3DCalInfo(__out ST_3DCal_Op& pst2DCalOp);

	void	Set_TestItemInfo(__in ST_TestItemInfo* pstTestItemInfo);
	void	Get_TestItemInfo(__out ST_TestItemInfo& stOutTestItemInfo);

	void	Set_3DCal_Para(__in ST_3DCal_Eval_Para* pst3DCal_Para);
	void	Get_3DCal_Para(__out ST_3DCal_Eval_Para& stOut3DCal_Para);


};

#endif // Wnd_Cfg_3DCal_Evalu_h__