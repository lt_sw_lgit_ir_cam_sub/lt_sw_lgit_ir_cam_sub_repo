// Wnd_Cfg_3DCal_Evalu.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_Cfg_3DCal_Evalu.h"
// CWnd_Cfg_3D_Eval
#define IDC_EDIT_PARAM	2000
#define IDC_EDIT_SPEC	2001
#define IDC_EDIT_ROI	2002

#define IDC_LST_ROI		4000


IMPLEMENT_DYNAMIC(CWnd_Cfg_3D_Eval, CWnd)

CWnd_Cfg_3D_Eval::CWnd_Cfg_3D_Eval()
{
	VERIFY(m_font.CreateFont(
		24,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_3D_Eval::~CWnd_Cfg_3D_Eval()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_3D_Eval, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

// CWnd_Cfg_3D_Eval 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/11/10 - 17:25
// Desc.		:
//=============================================================================
int CWnd_Cfg_3D_Eval::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	// 세팅 파라미터
	for (UINT nIdx = 0; nIdx < Param_3D_Eval_MaxNum; nIdx++)
	{
		m_st_Param_Eval[nIdx].SetTextAlignment(StringAlignmentNear);
		m_st_Param_Eval[nIdx].SetFont_Gdip(L"Arial", 9.0F, FontStyleRegular);
		m_st_Param_Eval[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Param_Eval[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Param_Eval[nIdx].Create(g_Param_3D_Eval[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

		m_ed_Param_Eval[nIdx].Create(dwStyle | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EDIT_PARAM + nIdx);
		m_ed_Param_Eval[nIdx].SetFont(&m_font);
		m_ed_Param_Eval[nIdx].SetValidChars(_T("0123456789.-"));
	}

	// ROI
	m_lst_Eval_ROI.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LST_ROI);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/11/10 - 17:25
// Desc.		:
//=============================================================================
void CWnd_Cfg_3D_Eval::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMargin = 10;
	int iSpacing = 5;

	int iLeft = iMargin;
	int iTop = iMargin;
	int iWidth = cx - iMargin - iMargin;
	int iHeight = cy - iMargin - iMargin;

	int iCtrlHeight = 36;
	int iTempWidth = (iWidth - (iSpacing * 3)) / 4;
	int iNameWidth = iTempWidth;

	int iLeftEdit = iLeft + iNameWidth + iSpacing;
	int iLeftSub = iLeftEdit + iTempWidth + iSpacing;
	int iLeftEditSub = iLeftSub + iNameWidth + iSpacing;

	int iListWidth = iWidth;
	int iListHeight = 150;

	// 세팅 파라미터
	for (UINT nIdx = 0; nIdx < Param_3D_Eval_MaxNum; nIdx++)
	{
		if (0 == (nIdx % 2))
		{
			m_st_Param_Eval[nIdx].MoveWindow(iLeft, iTop, iNameWidth, iCtrlHeight);
			m_ed_Param_Eval[nIdx].MoveWindow(iLeftEdit, iTop, iTempWidth, iCtrlHeight);
		}
		else
		{
			m_st_Param_Eval[nIdx].MoveWindow(iLeftSub, iTop, iNameWidth, iCtrlHeight);
			m_ed_Param_Eval[nIdx].MoveWindow(iLeftEditSub, iTop, iTempWidth, iCtrlHeight);
			iTop += iCtrlHeight + iSpacing;
		}
	}

	// ROI
	m_lst_Eval_ROI.MoveWindow(iLeft, iTop, iListWidth, iListHeight);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/5/29 - 19:39
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_3D_Eval::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: Set_3DCalInfo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_3DCal_Op * st3DCalOp
// Qualifier	:
// Last Update	: 2017/11/11 - 16:57
// Desc.		:
//=============================================================================
void CWnd_Cfg_3D_Eval::Set_3DCalInfo(__in const ST_3DCal_Op* st3DCalOp)
{

}

//=============================================================================
// Method		: Get_3DCalInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_3DCal_Op & pst3DCalOp
// Qualifier	:
// Last Update	: 2017/11/11 - 16:57
// Desc.		:
//=============================================================================
void CWnd_Cfg_3D_Eval::Get_3DCalInfo(__out ST_3DCal_Op& pst3DCalOp)
{

}

//=============================================================================
// Method		: Set_TestItemInfo
// Access		: public  
// Returns		: void
// Parameter	: __in ST_TestItemInfo * pstTestItemInfo
// Qualifier	:
// Last Update	: 2017/11/10 - 20:34
// Desc.		:
//=============================================================================
void CWnd_Cfg_3D_Eval::Set_TestItemInfo(__in ST_TestItemInfo* pstTestItemInfo)
{

}

//=============================================================================
// Method		: Get_TestItemInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_TestItemInfo & stOutTestItemInfo
// Qualifier	:
// Last Update	: 2017/11/11 - 16:58
// Desc.		:
//=============================================================================
void CWnd_Cfg_3D_Eval::Get_TestItemInfo(__out ST_TestItemInfo& stOutTestItemInfo)
{

}

//=============================================================================
// Method		: Set_3DCal_Para
// Access		: public  
// Returns		: void
// Parameter	: __in ST_3DCal_Eval_Para * pst3DCal_Para
// Qualifier	:
// Last Update	: 2017/11/12 - 17:05
// Desc.		:
//=============================================================================
void CWnd_Cfg_3D_Eval::Set_3DCal_Para(__in ST_3DCal_Eval_Para* pst3DCal_Para)
{
	CString szText;

	// 3D Cal Evaluation ------------------------------------------------------

	szText.Format(_T("%d"), pst3DCal_Para->nImgWidth);
	m_ed_Param_Eval[Param_3D_Eval_ImgWidth].SetWindowText(szText);

	szText.Format(_T("%d"), pst3DCal_Para->nImgHeight);
	m_ed_Param_Eval[Param_3D_Eval_ImgHeight].SetWindowText(szText);

	szText.Format(_T("%d"), pst3DCal_Para->nNumofCornerX);
	m_ed_Param_Eval[Param_3D_Eval_NumofCornerX].SetWindowText(szText);

	szText.Format(_T("%d"), pst3DCal_Para->nNumofCornerY);
	m_ed_Param_Eval[Param_3D_Eval_NumofCornerY].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_Para->fPatternSizeX);
	m_ed_Param_Eval[Param_3D_Eval_PatternSizeX].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_Para->fPatternSizeY);
	m_ed_Param_Eval[Param_3D_Eval_PatternSizeY].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_Para->fChessMarginX);
	m_ed_Param_Eval[Param_3D_Eval_ChessMarginX].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_Para->fChessMarginY);
	m_ed_Param_Eval[Param_3D_Eval_ChessMarginY].SetWindowText(szText);

	szText.Format(_T("%d"), pst3DCal_Para->nUseDepthFileter);
	m_ed_Param_Eval[Param_3D_Eval_UseDepthFileter].SetWindowText(szText);

	szText.Format(_T("%d"), pst3DCal_Para->nIrThres);
	m_ed_Param_Eval[Param_3D_Eval_IrThres].SetWindowText(szText);

	szText.Format(_T("%.2f"), pst3DCal_Para->fExternalGT[0]);	//MAX_EVAL_ExternalGT
	m_ed_Param_Eval[Param_3D_Eval_ExternalGT_0].SetWindowText(szText);

	szText.Format(_T("%.2f"), pst3DCal_Para->fExternalGT[1]);
	m_ed_Param_Eval[Param_3D_Eval_ExternalGT_1].SetWindowText(szText);

	szText.Format(_T("%.2f"), pst3DCal_Para->fExternalGT[2]);
	m_ed_Param_Eval[Param_3D_Eval_ExternalGT_2].SetWindowText(szText);

	szText.Format(_T("%d"), pst3DCal_Para->nPatchSizeU);
	m_ed_Param_Eval[Param_3D_Eval_PatchSizeU].SetWindowText(szText);

	szText.Format(_T("%d"), pst3DCal_Para->nPatchSizeV);
	m_ed_Param_Eval[Param_3D_Eval_PatchSizeV].SetWindowText(szText);

	szText.Format(_T("%d"), pst3DCal_Para->nPatchStepU);
	m_ed_Param_Eval[Param_3D_Eval_PatchStepU].SetWindowText(szText);

	szText.Format(_T("%d"), pst3DCal_Para->nPatchStepV);
	m_ed_Param_Eval[Param_3D_Eval_PatchStepV].SetWindowText(szText);

	szText.Format(_T("%d"), pst3DCal_Para->nShadeROIH);
	m_ed_Param_Eval[Param_3D_Eval_ShadeROIH].SetWindowText(szText);

	szText.Format(_T("%d"), pst3DCal_Para->nShadeROIV);
	m_ed_Param_Eval[Param_3D_Eval_ShadeROIV].SetWindowText(szText);

	szText.Format(_T("%d"), pst3DCal_Para->nShadeROID);
	m_ed_Param_Eval[Param_3D_Eval_ShadeROID].SetWindowText(szText);

	m_lst_Eval_ROI.InsertFullData(pst3DCal_Para);
}

//=============================================================================
// Method		: Get_3DCal_Para
// Access		: public  
// Returns		: void
// Parameter	: __out ST_3DCal_Eval_Para & stOut3DCal_Para
// Qualifier	:
// Last Update	: 2017/11/12 - 17:05
// Desc.		:
//=============================================================================
void CWnd_Cfg_3D_Eval::Get_3DCal_Para(__out ST_3DCal_Eval_Para& stOut3DCal_Para)
{
	CString szText;

	// 3D Cal Evaluation ------------------------------------------------------
	m_ed_Param_Eval[Param_3D_Eval_ImgWidth].GetWindowText(szText);
	stOut3DCal_Para.nImgWidth = _ttoi(szText.GetBuffer(0));

	m_ed_Param_Eval[Param_3D_Eval_ImgHeight].GetWindowText(szText);
	stOut3DCal_Para.nImgHeight = _ttoi(szText.GetBuffer(0));

	m_ed_Param_Eval[Param_3D_Eval_NumofCornerX].GetWindowText(szText);
	stOut3DCal_Para.nNumofCornerX = _ttoi(szText.GetBuffer(0));

	m_ed_Param_Eval[Param_3D_Eval_NumofCornerY].GetWindowText(szText);
	stOut3DCal_Para.nNumofCornerY = _ttoi(szText.GetBuffer(0));

	m_ed_Param_Eval[Param_3D_Eval_PatternSizeX].GetWindowText(szText);
	stOut3DCal_Para.fPatternSizeX = (float)_ttof(szText.GetBuffer(0));

	m_ed_Param_Eval[Param_3D_Eval_PatternSizeY].GetWindowText(szText);
	stOut3DCal_Para.fPatternSizeY = (float)_ttof(szText.GetBuffer(0));

	m_ed_Param_Eval[Param_3D_Eval_ChessMarginX].GetWindowText(szText);
	stOut3DCal_Para.fChessMarginX = (float)_ttof(szText.GetBuffer(0));

	m_ed_Param_Eval[Param_3D_Eval_ChessMarginY].GetWindowText(szText);
	stOut3DCal_Para.fChessMarginY = (float)_ttof(szText.GetBuffer(0));

	m_ed_Param_Eval[Param_3D_Eval_UseDepthFileter].GetWindowText(szText);
	stOut3DCal_Para.nUseDepthFileter = _ttoi(szText.GetBuffer(0));

	m_ed_Param_Eval[Param_3D_Eval_IrThres].GetWindowText(szText);
	stOut3DCal_Para.nIrThres = _ttoi(szText.GetBuffer(0));

	m_ed_Param_Eval[Param_3D_Eval_ExternalGT_0].GetWindowText(szText);
	stOut3DCal_Para.fExternalGT[0] = (float)_ttof(szText.GetBuffer(0));

	m_ed_Param_Eval[Param_3D_Eval_ExternalGT_1].GetWindowText(szText);
	stOut3DCal_Para.fExternalGT[1] = (float)_ttof(szText.GetBuffer(0));

	m_ed_Param_Eval[Param_3D_Eval_ExternalGT_2].GetWindowText(szText);
	stOut3DCal_Para.fExternalGT[2] = (float)_ttof(szText.GetBuffer(0));

	m_ed_Param_Eval[Param_3D_Eval_PatchSizeU].GetWindowText(szText);
	stOut3DCal_Para.nPatchSizeU = _ttoi(szText.GetBuffer(0));

	m_ed_Param_Eval[Param_3D_Eval_PatchSizeV].GetWindowText(szText);
	stOut3DCal_Para.nPatchSizeV = _ttoi(szText.GetBuffer(0));

	m_ed_Param_Eval[Param_3D_Eval_PatchStepU].GetWindowText(szText);
	stOut3DCal_Para.nPatchStepU = _ttoi(szText.GetBuffer(0));

	m_ed_Param_Eval[Param_3D_Eval_PatchStepV].GetWindowText(szText);
	stOut3DCal_Para.nPatchStepV = _ttoi(szText.GetBuffer(0));

	m_ed_Param_Eval[Param_3D_Eval_ShadeROIH].GetWindowText(szText);
	stOut3DCal_Para.nShadeROIH = _ttoi(szText.GetBuffer(0));

	m_ed_Param_Eval[Param_3D_Eval_ShadeROIV].GetWindowText(szText);
	stOut3DCal_Para.nShadeROIV = _ttoi(szText.GetBuffer(0));

	m_ed_Param_Eval[Param_3D_Eval_ShadeROID].GetWindowText(szText);
	stOut3DCal_Para.nShadeROID = _ttoi(szText.GetBuffer(0));

	m_lst_Eval_ROI.GetCellData(stOut3DCal_Para);
}

