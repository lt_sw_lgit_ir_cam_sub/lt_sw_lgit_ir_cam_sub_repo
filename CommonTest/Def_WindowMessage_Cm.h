﻿//*****************************************************************************
// Filename	: Def_WindowMessage_Cm.h
// Created	: 2012/1/16
// Modified	: 2016/08/17
//
// Author	: PiRing
//	
// Purpose	: 윈도우 메세지 정의 (0x0400 ~ 0x7FFF)
//*****************************************************************************
#ifndef Def_WindowMessage_Cm_h__
#define Def_WindowMessage_Cm_h__

//-------------------------------------------------------------------
// 프로그램 운영
//-------------------------------------------------------------------
#define		WM_ALIVE_PROCESS		(WM_USER + 108)

#define		WM_LOGMSG				WM_USER + 1		// 로그
#define		WM_OPTION				WM_USER + 2		// 옵션 창 열기
#define		WM_OPTION_CHANGED		WM_USER + 3		// 옵션이 변경됨
#define		WM_LOGMSG_DEV			WM_USER + 4		// 주변기기 로그 처리용
#define		WM_LOGMSG_PCB_CAM		WM_USER + 5		// 카메라 보드 로그 메세지
#define		WM_LOGMSG_PCB_LIGHT		WM_USER + 6		// 보드 로그 메세지
#define		WM_LOGMSG_POWERSUPPLY	WM_USER + 7		// 파워 서플라이 로그 메세지
#define		WM_LOGMSG_PLC			WM_USER + 8		// PLC 로그 메세지
#define		WM_LOGMSG_MES			WM_USER + 9		// MES 로그 메세지

#define		WM_LOAD_COMPLETE		WM_USER + 10	// 프로그램 로딩 끝
#define		WM_TEST_FUNCTION		WM_USER + 11	// 테스트 함수 실행
#define		WM_SYSTEM_MODE			WM_USER + 12	// 프로그램 운용모드(wparam - 운용모드[관리자, LOT모드, MES모드] lParam - Type[LotStart, LotStop])

#define		WM_LOGMSG_TESTER		WM_USER + 13	// 테스터 로그 메세지
#define		WM_LOGMSG_TORQUE		WM_USER + 14	// 토크 드라이버

//-------------------------------------------------------------------
// 주변 기기 통신
//-------------------------------------------------------------------
#define		WM_COMM_STATUS_SERVER	WM_USER + 20	// Handler 클라와의 통신 상태

#define		WM_RECV_HANDLER			WM_USER + 25	// 핸들러로부터 메세지 수신됨

#define		WM_RECV_LIGHT_BRD_ACK	WM_USER + 30	// 광원 보드로부터 데이터 수신
#define		WM_RECV_BARCODE			WM_USER + 31	// BCR로부터 데이터 수신
#define		WM_RECV_PALLET_ID		WM_USER + 32	// BCR로부터 Pallet ID 데이터 수신
#define		WM_RECV_VIDEO			WM_USER + 33	// 카메라 영상 데이터가 영상보드로부터 수신됨
#define		WM_CAMERA_STATUS		WM_USER + 34	// 카메라 영상 데이터가 영상보드로부터 수신됨

#define		WM_CAMERA_SELECT		WM_USER + 35	// 화면에 보여줄 Camera 선택
#define		WM_CAMERA_CHG_STATUS	WM_USER + 36	// 카메라 영상 상태가 변경됨
#define		WM_CAMERA_RECV_VIDEO	WM_USER + 37	// 카메라 영상 데이터가 영상보드로부터 수신됨

#define		WM_PLC_RECV_DATA		WM_USER + 38	// PLC 수신 데이터	
#define		WM_PLC_COMM_STATUS		WM_USER + 39	// PLC 통신 상태
#define		WM_RECV_PLC_BIT_DI		WM_USER + 40	// PLC DI 신호 수신 : Bit 단위
#define		WM_RECV_PLC_FST_READ	WM_USER + 41	// PLC DI 신호 수신 : 처음 읽음
#define		WM_RECV_DIO_BIT			WM_USER + 42	// DIO 신호 수신
#define		WM_RECV_DIO_FST_READ	WM_USER + 43	// DIO 신호 수신 : 처음 읽음



//-------------------------------------------------------------------
// 계측기 장비 제어
//-------------------------------------------------------------------
#define		WM_EXE_START			WM_USER + 50

#define		WM_CHANGE_VIEW			WM_USER + 51	// 윈도우 전환
#define		WM_PERMISSION_MODE		WM_USER + 52	// 관리자 모드 설정
#define		WM_TEST_START			WM_USER + 53	// 검사 시작
#define		WM_TEST_STOP			WM_USER + 54	// 검사 중지(검사 실패, 오류)
#define		WM_TEST_COMPLETED		WM_USER + 55	// 검사 완료, 제품 배출
#define		WM_TEST_INIT			WM_USER + 56	// 테스트 초기화

#define		WM_FILE_RECIPE			WM_USER + 57	// 설정된 모델 폴더의 파일들이 변경,추가,삭제 되었음을 알림
#define		WM_CHANGED_MODEL		WM_USER + 58	// 모델 파일의 데이터가 변경 되었음을 알림
#define		WM_REFESH_MODEL			WM_USER + 59	// 모델 파일 리스트 갱신
#define		WM_CHANGED_MODEL_TYPE	WM_USER + 60	// 모델 타입 변경 (콤보 박스)

#define		WM_MANUAL_DEV_CTRL		WM_USER + 61	// 주변장치 수동제어
#define		WM_INCREASE_POGO_CNT	WM_USER + 62	// 포고 카운트 증가 (A~D)
#define		WM_UPDATE_POGO_CNT		WM_USER + 63	// 포고 카운트의 설정 데이터가 변경됨
#define		WM_MANUAL_BARCODE		WM_USER + 64	// 수동 바코드 입력
#define		WM_MANUAL_PALLETID		WM_USER + 65	// 수동 Pallet ID 입력

#define		WM_MES_COMM_STATUS		WM_USER + 80	// MES 통신 상태
#define		WM_MES_ONLINE_MODE		WM_USER + 81	// MES Online/Offline 모드 변경
#define		WM_MES_RECV_BARCODE		WM_USER + 82	// MES Barcode 정보 수신

#define		WM_CONSUMABLES_RESET	WM_USER + 83	// 소모품 카운트 리셋

#define		WM_EQP_INIT				WM_USER + 84	// 설비 초기화 (원점 수행)
#define		WM_EQP_OPER_MODE		WM_USER + 85	// 설비 생산 모드 변경

#define		WM_UI_ERR_RESET			WM_USER + 98	// 에러 초기화
#define		WM_EXE_END				WM_USER + 99
//-------------------------------------------------------------------
//
//-------------------------------------------------------------------
#define		WM_MOTOR_ETC_CONTROL	WM_USER + 201
#define		WM_LOGMSG_MOTOR_CAM		WM_USER + 202
#define		WM_UPDATE_MOTOR_PARAM	WM_USER + 203	// 모터 셋팅 값이 변경됨
#define		WM_MOTOR_ORIGIN			WM_USER + 204	// UI 상에서 사용되는 Alarm Clear Message [wparam - 제어 해당 축]
#define		WM_MOTOR_STOP			WM_USER + 205	// UI 상에서 사용되는 Alarm Clear Message [wparam - 제어 해당 축]
#define		WM_MOTOR_ALARM_CLEAR	WM_USER	+ 206	// UI 상에서 사용되는 Alarm Clear Message [wparam - 제어 해당 축]
#define		WM_MOTOR_MANUAL			WM_USER	+ 207	// UI 상에서 사용되는 메뉴얼 제어 Message [wparam - 제어 해당 축, lparam - 제어방법]
#define		WM_MOTOR_MOVE_POS		WM_USER	+ 208	// UI 상에서 사용되는 메뉴얼 제어 Message [wparam - 제어 해당 축, lparam - 제어 Pulse]
#define		WM_MEASURE_MANUAL		WM_USER + 209	// UI 상에서 사용되는 메뉴얼 제어 Message [wparam - 테스트(Site ID 또는 ParaID), lparam - 메뉴얼 측정 종류]
#define		WM_DRYRUN				WM_USER + 210	// DRY RUN 모드

#define		WM_SELECT_OVERLAY		WM_USER + 211	// PIC 상태 변환
#define		WM_SELECT_ITEM_VIEW		WM_USER + 212	// 선택된 테스트 아이템 VIEW
#define		WM_SELECT_ITEM_TEST		WM_USER + 213	// 선택된 테스트 아이템 TEST

#define		WM_MANUAL_ONEITEM_TEST			WM_USER + 214	// 선택된 테스트 아이템 TEST

#endif // Def_WindowMessage_Cm_h__
