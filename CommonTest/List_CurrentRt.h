﻿#ifndef List_CurrentRt_h__
#define List_CurrentRt_h__

#pragma once

#include "Def_DataStruct_Cm.h"
#include "Def_T_Current.h"

typedef enum enListNumCurrentRt
{
	CuRt_Object = 0,
	CuRt_Channel,
	CuRt_MaxCol,
};

static LPCTSTR	g_lpszHeader_CurrentRt[] =
{
	_T(""),
	_T("Channel"),
	NULL
};

typedef enum enListItemNum_CurrentRt
{
	CuRt_Current = 0,
	CuRt_Result,
	CuRt_ItemNum,
};

static LPCTSTR	g_lpszItem_CurrentRt[] =
{
	_T("Current"),
	_T("Result"),
	NULL
};

const int	iListAglin_CurrentRt[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_CurrentRt[] =
{
	95,
	95,
	95,
};
// List_CurrentInfo

class CList_CurrentRt : public CListCtrl
{
	DECLARE_DYNAMIC(CList_CurrentRt)

public:
	CList_CurrentRt();
	virtual ~CList_CurrentRt();

	void SetRectRow		(__in const ST_Current_Data *pstCurrent);
	void SetRectClear	();
	
protected:

	DECLARE_MESSAGE_MAP()

	CFont	m_Font;

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	
	void InitHeader();

};

#endif // List_CurrentInfo_h__
