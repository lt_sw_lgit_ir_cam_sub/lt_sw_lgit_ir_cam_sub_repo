
// CWnd_Cfg_3D_Depth
#ifndef Wnd_Cfg_3DCal_h__
#define Wnd_Cfg_3DCal_h__

#pragma once

#include "VGStatic.h"
#include "Def_T_3DCal.h"
#include "List_3DCal_ROI.h"

class CWnd_Cfg_3D_Depth : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Cfg_3D_Depth)

public:
	CWnd_Cfg_3D_Depth();
	virtual ~CWnd_Cfg_3D_Depth();	

protected:
	afx_msg int		OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow(CREATESTRUCT& cs);

	afx_msg void	OnRangeBtnCtrl(__in UINT nID);

	DECLARE_MESSAGE_MAP()
	CFont				m_font;

	// 세팅 파라미터 옵션
	CVGStatic			m_st_Param_Depth[Param_3D_Depth_MaxNum];
	CMFCMaskedEdit		m_ed_Param_Depth[Param_3D_Depth_MaxNum];

	// ROI 영역
	CList_3DCal_ROI		m_lst_3DCalROI;

public:

	void	Set_3DCalInfo(__in const ST_3DCal_Op* st2DCalOp);
	void	Get_3DCalInfo(__out ST_3DCal_Op& pst2DCalOp);

	void	Set_TestItemInfo(__in ST_TestItemInfo* pstTestItemInfo);
	void	Get_TestItemInfo(__out ST_TestItemInfo& stOutTestItemInfo);

	void	Set_3DCal_Para(__in ST_3DCal_Depth_Para* pst3DCal_Para);
	void	Get_3DCal_Para(__out ST_3DCal_Depth_Para& stOut3DCal_Para);


};

#endif // Wnd_Cfg_3DCal_h__

