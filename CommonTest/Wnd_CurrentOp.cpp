﻿//*****************************************************************************
// Filename	: 	Wnd_CurrentOp.cpp
// Created	:	2017/11/10 - 10:42
// Modified	:	2017/11/10 - 10:42
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_CurrentOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_CurrentOp.h"
#include "Def_WindowMessage_Cm.h"

// CWnd_CurrentOp
typedef enum CurrentOptID
{
	IDC_ED_SPEC_MIN  = 1001,
	IDC_ED_SPEC_MAX  = 2001,
};

IMPLEMENT_DYNAMIC(CWnd_CurrentOp, CWnd)

CWnd_CurrentOp::CWnd_CurrentOp()
{
	VERIFY(m_font.CreateFont(
		32,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_CurrentOp::~CWnd_CurrentOp()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_CurrentOp, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()

// CWnd_CurrentOp 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_CurrentOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_st_CapItem.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_CapItem.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_CapItem.SetFont_Gdip(L"Arial", 9.0F);
	m_st_CapItem.Create(_T("Item"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_CapSpecMin.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_CapSpecMin.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_CapSpecMin.SetFont_Gdip(L"Arial", 9.0F);
	m_st_CapSpecMin.Create(_T("Spec Min"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_CapSpecMax.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_CapSpecMax.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_CapSpecMax.SetFont_Gdip(L"Arial", 9.0F);
	m_st_CapSpecMax.Create(_T("Spec Max"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);


	for (UINT nIdx = 0; nIdx < Curr_MaxEnum; nIdx++)
	{
		m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_Black);
		m_st_Item[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdx].Create(g_szCurrentStatic[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

		m_ed_SpecMin[nIdx].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_SPEC_MIN + nIdx);
		m_ed_SpecMin[nIdx].SetWindowText(_T("0"));
		m_ed_SpecMin[nIdx].SetValidChars(_T("0123456789.-"));

		m_ed_SpecMax[nIdx].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_SPEC_MAX + nIdx);
		m_ed_SpecMax[nIdx].SetWindowText(_T("0"));
		m_ed_SpecMax[nIdx].SetValidChars(_T("0123456789.-"));

		m_ed_SpecMin[nIdx].SetFont(&m_font);
		m_ed_SpecMax[nIdx].SetFont(&m_font);
	}
		
	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_CurrentOp::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iMargin		= 10;
	int iSpacing	= 5;
	int iCateSpacing= 5;
	int iLeft		= iMargin;
	int iTop		= iMargin;
	int iWidth		= cx - iMargin - iMargin;
	int iHeight		= cy - iMargin - iMargin;

	int iCtrlWidth	= 240;
	int	iCtrlHeight	= 40;
	int iLeft_2nd	= iLeft + iCtrlWidth + iSpacing;
	int iLeft_3rd	= iLeft_2nd + iCtrlWidth + iSpacing;

	m_st_CapItem.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
	m_st_CapSpecMin.MoveWindow(iLeft_2nd, iTop, iCtrlWidth, iCtrlHeight);
	m_st_CapSpecMax.MoveWindow(iLeft_3rd, iTop, iCtrlWidth, iCtrlHeight);

	iTop += iCtrlHeight + iCateSpacing;
	for (UINT nIdx = 0; nIdx < Curr_MaxEnum; nIdx++)
	{
		m_st_Item[nIdx].MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
		m_ed_SpecMin[nIdx].MoveWindow(iLeft_2nd, iTop, iCtrlWidth, iCtrlHeight);
		m_ed_SpecMax[nIdx].MoveWindow(iLeft_3rd, iTop, iCtrlWidth, iCtrlHeight);

		iTop += iCtrlHeight + iCateSpacing;
	}

}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_CurrentOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_CurrentOp::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (TRUE == bShow)
	{
		GetOwner()->SendMessage(WM_SELECT_ITEM_VIEW, 0, 0);
	}
}

//=============================================================================
// Method		: Set_TestItemInfo
// Access		: public  
// Returns		: void
// Parameter	: __in ST_TestItemInfo * pstTestItemInfo
// Qualifier	:
// Last Update	: 2017/11/9 - 18:46
// Desc.		:
//=============================================================================
void CWnd_CurrentOp::Set_TestItemInfo(__in ST_TestItemInfo* pstTestItemInfo)
{
// 	ASSERT(NULL != pstTestItemInfo);
// 
// 	CString szText;
// 
// 	ST_TestItemSpec* pSpec = pstTestItemInfo->GetTestItem(enTestItem_Focusing::TI_Foc_SleepCurrent);
// 	ASSERT(NULL != pSpec);
// 
// 	szText.Format(_T("%d"), pSpec->Spec[0].Spec_Min.intVal);
// 	m_ed_SpecMin[Curr_Sleep].SetWindowText(szText);
// 
// 	szText.Format(_T("%d"), pSpec->Spec[0].Spec_Max.intVal);
// 	m_ed_SpecMax[Curr_Sleep].SetWindowText(szText);
// 
// 	pSpec = pstTestItemInfo->GetTestItem(enTestItem_Focusing::TI_Foc_OperCurrent);
// 
// 	szText.Format(_T("%d"), pSpec->Spec[0].Spec_Min.intVal);
// 	m_ed_SpecMin[Curr_Operation].SetWindowText(szText);
// 
// 	szText.Format(_T("%d"), pSpec->Spec[0].Spec_Max.intVal);
// 	m_ed_SpecMax[Curr_Operation].SetWindowText(szText);
}

void CWnd_CurrentOp::Set_TestItemInfo(__in const ST_TestItemInfo* pstTestItemInfo, __in const ST_TI_Current* pstInTestItemOpt)
{

}

//=============================================================================
// Method		: Get_TestItemInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_TestItemInfo & stOutTestItemInfo
// Qualifier	:
// Last Update	: 2017/11/9 - 18:46
// Desc.		:
//=============================================================================
void CWnd_CurrentOp::Get_TestItemInfo(__out ST_TestItemInfo& stOutTestItemInfo)
{
	CString szText;

// 	m_ed_SpecMin[Curr_Sleep].GetWindowText(szText);
// 	stOutTestItemInfo.TestItemList[enTestItem_Focusing::TI_Foc_SleepCurrent].Spec[0].Spec_Min.intVal = _ttol(szText.GetBuffer(0));
// 	
// 	m_ed_SpecMax[Curr_Sleep].GetWindowText(szText);
// 	stOutTestItemInfo.TestItemList[enTestItem_Focusing::TI_Foc_SleepCurrent].Spec[0].Spec_Max.intVal = _ttol(szText.GetBuffer(0));
// 
// 	
// 	m_ed_SpecMin[Curr_Operation].GetWindowText(szText);
// 	stOutTestItemInfo.TestItemList[enTestItem_Focusing::TI_Foc_OperCurrent].Spec[0].Spec_Min.intVal = _ttol(szText.GetBuffer(0));
// 
// 	m_ed_SpecMax[Curr_Operation].GetWindowText(szText);
// 	stOutTestItemInfo.TestItemList[enTestItem_Focusing::TI_Foc_OperCurrent].Spec[0].Spec_Max.intVal = _ttol(szText.GetBuffer(0));

}

void CWnd_CurrentOp::Get_TestItemInfo(__out ST_TestItemInfo& stOutTestItemInfo, __out ST_TI_Current& stOutTestItemOpt)
{

}
