﻿//*****************************************************************************
// Filename	: 	Dlg_OperMode.h
// Created	:	2016/11/6 - 19:05
// Modified	:	2016/11/6 - 19:05
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Dlg_OperMode_h__
#define Dlg_OperMode_h__

#pragma once

#include "resource.h"
#include "Def_Enum_Cm.h"
#include "VGStatic.h"
#include "afxwin.h"
#include "Reg_Management.h"


//-----------------------------------------------------------------------------
// CDlg_OperMode dialog
//-----------------------------------------------------------------------------
class CDlg_OperMode : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_OperMode)

public:
	CDlg_OperMode(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlg_OperMode();

// Dialog Data
	enum { IDD = IDD_DLG_OPERATE_MODE };

protected:
	virtual void	DoDataExchange				(CDataExchange* pDX);    // DDX/DDV support
	afx_msg int		OnCreate					(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize						(UINT nType, int cx, int cy);
	virtual BOOL	PreTranslateMessage			(MSG* pMsg);
	virtual BOOL	OnInitDialog				();

	afx_msg void	OnEnChangeEdPassword		();
	afx_msg void	OnCommandRangeRbOperMode	(UINT nID);
	virtual void	OnOK();
	virtual void	OnCancel();


	DECLARE_MESSAGE_MAP()

	CFont			m_font_Large;
	CFont			m_font_Default;

	CVGStatic			m_st_Title;

	CMFCButton			m_rb_OperMode[OpMode_MaxEnum];

	CVGStatic			m_st_Password;
	CEdit				m_ed_Password;

	CButton				m_bn_OK;
	CButton				m_bn_Cancel;

	CReg_Management		m_regManagement;
	enOperateMode		m_nOperMode				= enOperateMode::OpMode_Production;	
	BOOL				m_bUseMode[OpMode_MaxEnum];

	CArray<UINT, UINT>	m_arOperModez;

	void		Load_OperMode			();
	void		Save_OperMode			();

	BOOL		CheckPassword			();

public:

	void		Set_OperateMode			(__in enOperateMode nOnlineMode);

	enOperateMode	Get_OperateMode()
	{
		return m_nOperMode;
	};

	void		Add_OperateMode			(__in enOperateMode nOnlineMode);
	void		Reset_OperateMode		();

	// 검사기 종류 설정
	void		SetSystemType			(__in enInsptrSysType nSysType);
};

#endif // Dlg_OperMode_h__
