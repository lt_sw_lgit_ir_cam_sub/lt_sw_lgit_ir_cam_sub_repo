// List_3DCal_Eval_ROI.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_3DCal_Eval_ROI.h"
#include "Def_WindowMessage_Cm.h"

#define IDC_EDT_CELLEDIT		5001

// CList_3DCal_Eval_ROI
IMPLEMENT_DYNAMIC(CList_3DCal_Eval_ROI, CListCtrl)

CList_3DCal_Eval_ROI::CList_3DCal_Eval_ROI()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);

	m_nEditCol = 0;
	m_nEditRow = 0;
}

CList_3DCal_Eval_ROI::~CList_3DCal_Eval_ROI()
{
	m_Font.DeleteObject();
}
BEGIN_MESSAGE_MAP(CList_3DCal_Eval_ROI, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(NM_CLICK, &CList_3DCal_Eval_ROI::OnNMClick)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CList_3DCal_Eval_ROI::OnNMDblclk)
	ON_EN_KILLFOCUS(IDC_EDT_CELLEDIT, &CList_3DCal_Eval_ROI::OnEnKillFocusECpOpellEdit)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_3DCal_Eval_ROI 메시지 처리기입니다.
int CList_3DCal_Eval_ROI::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);
	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER /*| LVS_EX_CHECKBOXES*/);

	InitHeader();
	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER | ES_NUMBER, CRect(0, 0, 0, 0), this, IDC_EDT_CELLEDIT);
	this->GetHeaderCtrl()->EnableWindow(FALSE);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_3DCal_Eval_ROI::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iColWidth[ROI_3D_Eval_MaxCol] = { 0, };
	int iColDivide = 0;
	int iUnitWidth = 0;
	int iMisc = 0;

	for (int nCol = 0; nCol < ROI_3D_Eval_MaxCol; nCol++)
	{
		iColDivide += iHeaderWidth_3DCal_Eval_ROI[nCol];
	}

	CRect rectClient;
	GetClientRect(rectClient);

	for (int nCol = 0; nCol < ROI_3D_Eval_MaxCol; nCol++)
	{
		iUnitWidth = (rectClient.Width() * iHeaderWidth_3DCal_Eval_ROI[nCol]) / iColDivide;
		iMisc += iUnitWidth;
		SetColumnWidth(nCol, iUnitWidth);
	}

	iUnitWidth = ((rectClient.Width() * iHeaderWidth_3DCal_Eval_ROI[ROI_3D_Eval_Object]) / iColDivide) + (rectClient.Width() - iMisc);
	SetColumnWidth(ROI_3D_Eval_Object, iUnitWidth);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_3DCal_Eval_ROI::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | /*LVS_EDITLABELS | */WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_3DCal_Eval_ROI::InitHeader()
{
	for (int nCol = 0; nCol <ROI_3D_Eval_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_3DCal_Eval_ROI[nCol], iListAglin_3DCal_Eval_ROI[nCol], iHeaderWidth_3DCal_Eval_ROI[nCol]);
	}

	for (int nCol = 0; nCol <ROI_3D_Eval_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_3DCal_Eval_ROI[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Parameter	: __in ST_3DCal_Eval_Para * pst3DCal_Eval_Para
// Qualifier	:
// Last Update	: 2017/11/11 - 18:03
// Desc.		:
//=============================================================================
void CList_3DCal_Eval_ROI::InsertFullData(__in ST_3DCal_Eval_Para* pst3DCal_Eval_Para)
{
	DeleteAllItems();

	for (UINT nIdx = 0; nIdx <ROI_3D_Eval_ItemNum; nIdx++)
	{
		m_st3DCal_Eval_Para.ROI[nIdx] = pst3DCal_Eval_Para->ROI[nIdx];

		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/6/26 - 14:26
// Desc.		:
//=============================================================================
void CList_3DCal_Eval_ROI::SetRectRow(UINT nRow)
{
	CString strValue;

	strValue.Format(_T("%d"), nRow);
	SetItemText(nRow,ROI_3D_Eval_Object, strValue);

	strValue.Format(_T("%d"), m_st3DCal_Eval_Para.ROI[nRow].nU);
	SetItemText(nRow,ROI_3D_Eval_U, strValue);

	strValue.Format(_T("%d"), m_st3DCal_Eval_Para.ROI[nRow].nV);
	SetItemText(nRow,ROI_3D_Eval_V, strValue);

	strValue.Format(_T("%d"), m_st3DCal_Eval_Para.ROI[nRow].nW);
	SetItemText(nRow,ROI_3D_Eval_W, strValue);

	strValue.Format(_T("%d"), m_st3DCal_Eval_Para.ROI[nRow].nH);
	SetItemText(nRow,ROI_3D_Eval_H, strValue);
}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_3DCal_Eval_ROI::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_3DCal_Eval_ROI::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem <ROI_3D_Eval_MaxCol && pNMItemActivate->iSubItem > 0)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);

			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

			m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
			m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
			m_ed_CellEdit.SetFocus();
		}
	}

	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusECpOpellEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
void CList_3DCal_Eval_ROI::OnEnKillFocusECpOpellEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText));

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
BOOL CList_3DCal_Eval_ROI::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	if (iValue < 0)
		iValue = 0;

	switch (nCol)
	{
	case ROI_3D_Eval_U:
		m_st3DCal_Eval_Para.ROI[nRow].nU = iValue;
		break;
	case ROI_3D_Eval_V:
		m_st3DCal_Eval_Para.ROI[nRow].nV = iValue;
		break;
	case ROI_3D_Eval_W:
		m_st3DCal_Eval_Para.ROI[nRow].nW = iValue;
		break;
	case ROI_3D_Eval_H:
		m_st3DCal_Eval_Para.ROI[nRow].nH = iValue;
		break;
	default:
		break;
	}

	CString strValue;

	switch (nCol)
	{
	case ROI_3D_Eval_U:
		strValue.Format(_T("%d"), m_st3DCal_Eval_Para.ROI[nRow].nU);
		break;
	case ROI_3D_Eval_V:
		strValue.Format(_T("%d"), m_st3DCal_Eval_Para.ROI[nRow].nV);
		break;
	case ROI_3D_Eval_W:
		strValue.Format(_T("%d"), m_st3DCal_Eval_Para.ROI[nRow].nW);
		break;
	case ROI_3D_Eval_H:
		strValue.Format(_T("%d"), m_st3DCal_Eval_Para.ROI[nRow].nH);
		break;
	default:
		break;
	}

	m_ed_CellEdit.SetWindowText(strValue);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: GetCellData
// Access		: protected  
// Returns		: void
// Parameter	: __out ST_3DCal_Eval_Para & stOut3DCal_Eval_Para
// Qualifier	:
// Last Update	: 2017/11/11 - 17:44
// Desc.		:
//=============================================================================
void CList_3DCal_Eval_ROI::GetCellData(__out ST_3DCal_Eval_Para& stOut3DCal_Eval_Para)
{
	for (UINT nIdx = 0; nIdx <ROI_3D_Eval_ItemNum; nIdx++)
	{
		stOut3DCal_Eval_Para.ROI[nIdx] = m_st3DCal_Eval_Para.ROI[nIdx];
	}
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_3DCal_Eval_ROI::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue = _ttoi(strText);

		if (0 < zDelta)
		{
			iValue = iValue + ((zDelta / 120));
		}
		else
		{
			if (0 < iValue)
			{
				iValue = iValue + ((zDelta / 120));
			}
		}

		if (iValue < 0)
			iValue = 0;

		if (iValue > 1000)
			iValue = 1000;

		UpdateCellData(m_nEditRow, m_nEditCol, iValue);
	}

	return CListCtrl::OnMouseWheel(nFlags, zDelta, pt);
}
