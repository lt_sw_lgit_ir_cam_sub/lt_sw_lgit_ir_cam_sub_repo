//*****************************************************************************
// Filename	: 	Def_T_Foc.h
// Created	:	2018/3/4 - 10:29
// Modified	:	2018/3/4 - 10:29
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_T_Foc_h__
#define Def_T_Foc_h__

#include <afxwin.h>

#include "Def_T_ECurrent.h"
#include "Def_T_OpticalCenter.h"
#include "Def_T_Rotate.h"
#include "Def_T_SFR.h"
#include "Def_T_DefectPixel.h"
#include "Def_T_Particle.h"
#include "Def_T_ActiveAlign.h"
#include "Def_T_Torque.h"
#include "Def_T_Focus.h"

#pragma pack(push,1)

typedef enum enSpec_Foc
{
	
	Spec_Foc_Item_01,
	Spec_Foc_Item_02,
	Spec_Foc_Item_03,
	Spec_Foc_Item_04,
	Spec_Foc_Item_05,
	Spec_Foc_Item_06,
	Spec_Foc_Item_07,
	Spec_Foc_Item_08,
	Spec_Foc_Item_09,
	Spec_Foc_Item_10,
	Spec_Foc_Item_11,
	Spec_Foc_Item_12,
	Spec_Foc_MaxNum
};

static LPCTSTR	g_szSpecFoc[] =
{
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	NULL
};

typedef struct _tag_Foc_Result
{
	ST_ECurrent_Data		stECurrentData;
	ST_OpticalCenter_Data	stOpticalCenterData;
	ST_Rotate_Data			stRotateData;
	ST_DefectPixel_Data		stDefectPixelData;
	ST_SFR_Data				stSFRData;
	ST_Particle_Data		stParticleData;
	ST_ActiveAlign_Data		stActiveAlignData;
	ST_Torque_Data			stTorqueData;
	ST_Focus_Data			stFocusData;

	CString					szTestFileName;

	_tag_Foc_Result()
	{
		Reset();
	};

	_tag_Foc_Result& operator= (const _tag_Foc_Result& ref)
	{
		return *this;
	};

	void Reset()
	{
		stECurrentData.Reset();
		stOpticalCenterData.Reset();
		stRotateData.Reset();
		stDefectPixelData.Reset();
		stSFRData.Reset();
		stParticleData.Reset();
		stTorqueData.Reset();
		stActiveAlignData.Reset();
		stFocusData.Reset();
		szTestFileName.Empty();
	};

}ST_Foc_Result, *PST_Foc_Result;

typedef struct _tag_Foc_Spec
{
	_tag_Foc_Spec()
	{
		Reset();
	};

	_tag_Foc_Spec& operator= (const _tag_Foc_Spec& ref)
	{

		return *this;
	};

	void Reset()
	{

	};

}ST_Foc_Spec, *PST_Foc_Spec;

typedef struct _tag_Foc_Info
{
	// �Ķ����
	ST_ECurrent_Opt			stECurrentOpt;
	ST_OpticalCenter_Opt	stOpticalCenterOpt;
	ST_Rotate_Opt			stRotateOpt;
	ST_DefectPixel_Opt		stDefectPixelOpt;
	ST_SFR_Opt				stSFROpt;
	ST_Particle_Opt			stParticleOpt;
	ST_ActiveAlign_Opt		stActiveAlignOpt;
	ST_Torque_Opt			stTorqueOpt;
	ST_Focus_Opt			stFocusOpt;
	
	_tag_Foc_Info()
	{
		Reset();
	};

	_tag_Foc_Info& operator= (const _tag_Foc_Info& ref)
	{

		return *this;
	};

	void Reset()
	{

	};

}ST_Foc_Info, *PST_Foc_Info;

#pragma pack(pop)

#endif // Def_T_Foc_h__
