﻿#ifndef Wnd_DelayOp_h__
#define Wnd_DelayOp_h__

#pragma once

#include "VGStatic.h"
#include "CommonFunction.h"
#include "List_Delay.h"
#include "Def_DataStruct_Cm.h"

// CWnd_Cfg_Delay

enum enDelayStatic
{
	STI_DLY_MAX = 1,
};

static LPCTSTR	g_szDelayStatic[] =
{
	NULL
};

enum enDelayButton
{
	BTN_DLY_MAX = 1,
};

static LPCTSTR	g_szDelayButton[] =
{
	NULL
};

enum enDelayComobox
{
	CMB_DLY_MAX = 1,
};

enum enDelayEdit
{
	EDT_DLY_MAX = 1,
};

class CWnd_Cfg_Delay : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Cfg_Delay)

public:
	CWnd_Cfg_Delay();
	virtual ~CWnd_Cfg_Delay();

protected:
	DECLARE_MESSAGE_MAP()

	ST_RecipeInfo_Base	*m_pstRecipeInfo;
	CList_Delay		    m_List;

	CFont				m_font;

	CVGStatic			m_st_Item[STI_DLY_MAX];
	CButton				m_bn_Item[BTN_DLY_MAX];
	CComboBox			m_cb_Item[CMB_DLY_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_DLY_MAX];

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void	SetPtr_RecipeInfo (ST_RecipeInfo_Base* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstRecipeInfo = pstRecipeInfo;
	};

	void Set_RecipeInfo		();
	void Get_RecipeInfo		();
};
#endif // Wnd_DelayOp_h__
