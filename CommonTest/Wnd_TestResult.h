﻿#ifndef Wnd_TestResult_h__
#define Wnd_TestResult_h__

#pragma once

#include "VGStatic.h"
#include "CommonFunction.h"
#include "Def_DataStruct_Cm.h"
// #include "List_CenterPointRt.h"
// #include "List_CurrentRt.h"
// #include "List_DistortionRt.h"
// #include "List_DynamicRt.h"
// #include "List_RotateRt.h"
// #include "List_SFRRt.h"
// #include "List_SNR_BWRt.h"
// #include "List_SNR_IQRt.h"
// #include "List_TiltRt.h"
// #include "List_FOVRt.h"
// #include "List_DefectPixelRt.h"
// #include "List_ShadingRt.h"
// #include "List_SNR_LightRt.h"
// #include "List_FPNRt.h"

// CWnd_TestResult
enum enTestResultStatic
{
	STI_TR_Title,
	STI_TR_MAX,
};

static LPCTSTR	g_szTestResultStatic[] =
{
	_T("TEST RESULT"),
	NULL
};

enum enTestResultButton
{
	BTN_TR_MAX = 1,
};

static LPCTSTR	g_szTestResultButton[] =
{
	NULL
};

enum enTestResultComobox
{
	CMB_TR_MAX = 1,
};

static LPCTSTR	g_szTestResultCommoBox[] =
{
	NULL
};

enum enTestResultEdit
{
	EDT_TR_MAX = 1,
};

class CWnd_TestResult : public CWnd
{
	DECLARE_DYNAMIC(CWnd_TestResult)

public:
	CWnd_TestResult();
	virtual ~CWnd_TestResult();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	enInsptrSysType			m_InspectionType;

	CFont					m_font;
	
	CVGStatic				m_st_default;
	CVGStatic				m_st_Item[STI_TR_MAX];
	CMFCButton				m_bn_Item[BTN_TR_MAX];
	CComboBox				m_cb_Item[CMB_TR_MAX];
	CMFCMaskedEdit			m_ed_Item[EDT_TR_MAX];

// 	CList_CenterPointRt		m_List_CenterPoint;
// 	CList_CurrentRt			m_List_Current;
// 	CList_DistortionRt		m_List_Distortion;
// 	CList_DynamicRt			m_List_Dynamic;
// 	CList_RotateRt			m_List_Rotate;
// 	CList_SFRRt				m_List_SFR;
// 	CList_FOVRt				m_List_FOV;
// 	CList_SNR_BWRt			m_List_SNR_BW;
// 	CList_SNR_IQRt			m_List_SNR_IQ;
// 	CList_TiltRt			m_List_Tilt;
// 
// 	CList_DefectPixelRt		m_List_DefectPixel;
// 	CList_ShadingRt	m_List_Shading;
// 	CList_SNR_LightRt		m_List_SNR_Light;
// 	CList_FPNRt				m_List_FPN;

	void	SetShowWindowResult (int iItem);
	void	MoveWindow_Result	(int x, int y, int nWidth, int nHeight);

public:

	// 검사기 종류 설정
	void	SetSystemType (__in enInsptrSysType nSysType)
	{
		m_InspectionType = nSysType;
	};

	void SetTestItem		(__in int iTestItem);
	void SetUpdateResult	(__in const ST_RecipeInfo_Base* pstRecipeInfo, __in int iTestItem);
	void SetUpdateClear		();

};
#endif // Wnd_TestResult_h__
