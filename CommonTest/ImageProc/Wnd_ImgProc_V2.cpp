﻿// Wnd_ImgProc_V2.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_ImgProc_V2.h"


// CWnd_ImgProc_V2

IMPLEMENT_DYNAMIC(CWnd_ImgProc_V2, CWnd_BaseView)

CWnd_ImgProc_V2::CWnd_ImgProc_V2()
{
	m_lpbyRGB = new BYTE[MAX_IMAGE_BUFFER]; // 최대 4K 해상도 메모리 버퍼

	ZeroMemory(&m_BmpInfo, sizeof(m_BmpInfo));

	m_BmpInfo.bmiHeader.biSize			= sizeof(BITMAPINFOHEADER);
	m_BmpInfo.bmiHeader.biWidth			= 1920;
	m_BmpInfo.bmiHeader.biHeight		= 1200;
	m_BmpInfo.bmiHeader.biPlanes		= 1;
	m_BmpInfo.bmiHeader.biBitCount		= 24;
	m_BmpInfo.bmiHeader.biCompression	= BI_RGB;
	m_BmpInfo.bmiHeader.biSizeImage		= MAX_IMAGE_BUFFER;
}

CWnd_ImgProc_V2::~CWnd_ImgProc_V2()
{
	if (NULL != m_lpbyRGB)
		delete[] m_lpbyRGB;
}


BEGIN_MESSAGE_MAP(CWnd_ImgProc_V2, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_PAINT()
END_MESSAGE_MAP()


// CWnd_ImgProc_V2 message handlers
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/5/22 - 16:08
// Desc.		:
//=============================================================================
int CWnd_ImgProc_V2::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/5/22 - 16:08
// Desc.		:
//=============================================================================
void CWnd_ImgProc_V2::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
}

//=============================================================================
// Method		: OnPaint
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/22 - 16:08
// Desc.		:
//=============================================================================
void CWnd_ImgProc_V2::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	
	if (m_bRender)
		DrawVideo(&dc);
	else
		DrawEmptyRect(&dc);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/5/22 - 16:08
// Desc.		:
//=============================================================================
BOOL CWnd_ImgProc_V2::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Add your specialized code here and/or call the base class

	return CWnd_BaseView::PreCreateWindow(cs);
}

//=============================================================================
// Method		: SetUseFPS
// Access		: public  
// Returns		: void
// Parameter	: __in BOOL bUse
// Qualifier	:
// Last Update	: 2018/2/5 - 12:32
// Desc.		:
//=============================================================================
void CWnd_ImgProc_V2::SetUseFPS(__in BOOL bUse)
{
	m_bUseFPS = bUse;
}

//=============================================================================
// Method		: SetUseOverlay
// Access		: public  
// Returns		: void
// Parameter	: __in BOOL bUse
// Qualifier	:
// Last Update	: 2018/2/5 - 12:32
// Desc.		:
//=============================================================================
void CWnd_ImgProc_V2::SetUseOverlay(__in BOOL bUse /*= TRUE*/)
{
	m_bUseOverlay = bUse;
}

//=============================================================================
// Method		: SetUseCrosshair
// Access		: public  
// Returns		: void
// Parameter	: __in BOOL bUse
// Qualifier	:
// Last Update	: 2018/2/5 - 12:32
// Desc.		:
//=============================================================================
void CWnd_ImgProc_V2::SetUseCrosshair(__in BOOL bUse /*= TRUE*/)
{
	m_bUseCrosshair = bUse;
}

//=============================================================================
// Method		: AddOverlayRect
// Access		: public  
// Returns		: void
// Parameter	: RECT rect
// Qualifier	:
// Last Update	: 2018/1/13 - 13:47
// Desc.		:
//=============================================================================
void CWnd_ImgProc_V2::AddOverlayRect(RECT rect)
{
	m_arrRect.Add(rect);

	BOOL bUse = TRUE;
	m_arrUseRect.Add(bUse);
}

//=============================================================================
// Method		: ResetOverlayRect
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/1/13 - 13:47
// Desc.		:
//=============================================================================
void CWnd_ImgProc_V2::ResetOverlayRect()
{
	m_arrRect.RemoveAll();
	m_arrUseRect.RemoveAll();
}

//=============================================================================
// Method		: DrawEmptyRect
// Access		: public  
// Returns		: void
// Parameter	: CDC * pDC
// Qualifier	:
// Last Update	: 2018/1/13 - 13:47
// Desc.		:
//=============================================================================
void CWnd_ImgProc_V2::DrawEmptyRect(CDC* pDC)
{
// 	CBrush NewBrush;
// 	NewBrush.CreateStockObject(BLACK_BRUSH);
// 	CBrush* pOldBrush = pDC->SelectObject(&NewBrush);
// 
// 	pDC->SetBkMode(TRANSPARENT);
// 
// 	CRect rectClient;
// 	GetClientRect(rectClient);
// 	pDC->Rectangle(rectClient);
// 	pDC->SelectObject(pOldBrush);

	CBrush NewBrush;
	NewBrush.CreateStockObject(BLACK_BRUSH);
	CBrush* pOldBrush = pDC->SelectObject(&NewBrush);

	pDC->SetBkMode(TRANSPARENT);

	LOGBRUSH lb;
	lb.lbStyle = BS_SOLID;
	lb.lbColor = RGB(0, 0, 0);
	CPen NewPen;
	NewPen.CreatePen(PS_GEOMETRIC | PS_SOLID | PS_ENDCAP_FLAT, 1, &lb);
	CPen* pOldPen = pDC->SelectObject(&NewPen);

	CRect rectClient;
	GetClientRect(rectClient);
	pDC->Rectangle(rectClient); // 478

	CFont font;
	font.CreatePointFont(130, _T("Arial"));
	pDC->SelectObject(&font);
	pDC->SetTextColor(RGB(255, 0, 0));
	pDC->SetTextAlign(TA_LEFT | TA_BASELINE);
	CString strText = _T("No Signal");
	pDC->TextOut(20, 30, strText, strText.GetLength());
	font.DeleteObject();

	pDC->SelectObject(pOldBrush);
	pDC->SelectObject(pOldPen);
}

//=============================================================================
// Method		: DrawOverlayRect
// Access		: public  
// Returns		: void
// Parameter	: CDC * pDC
// Parameter	: LONG lDestWidth
// Parameter	: LONG lDestHeight
// Qualifier	:
// Last Update	: 2018/1/13 - 13:48
// Desc.		:
//=============================================================================
void CWnd_ImgProc_V2::DrawOverlayRect(CDC* pDC, LONG lDestWidth, LONG lDestHeight)
{
	if (m_arrRect.IsEmpty())
		return;

	LOGBRUSH lb;
	lb.lbStyle = BS_SOLID;
	lb.lbColor = RGB(255, 0, 0);

	CPen NewPen;
	NewPen.CreatePen(PS_GEOMETRIC | PS_SOLID | PS_ENDCAP_FLAT, 1, &lb);
	CPen* pOldPen = pDC->SelectObject(&NewPen);
	CBrush NewBrush;
	NewBrush.CreateStockObject(NULL_BRUSH);
	CBrush* pOldBrush = pDC->SelectObject(&NewBrush);

	pDC->SetBkMode(TRANSPARENT);

	CRect rectOv;
	CRect rectClient;
	GetClientRect(rectClient);

	for (int iCnt = 0; iCnt < m_arrRect.GetCount(); iCnt++)
	{
		if (iCnt < m_arrUseRect.GetCount())
		{
			if (m_arrUseRect.GetAt(iCnt))
			{
				rectOv = m_arrRect.GetAt(iCnt);

				rectOv.left		= (rectOv.left * rectClient.Width()) / lDestWidth;
				rectOv.top		= (rectOv.top * rectClient.Height()) / lDestHeight;
				rectOv.right	= (rectOv.right * rectClient.Width()) / lDestWidth;
				rectOv.bottom	= (rectOv.bottom * rectClient.Height()) / lDestHeight;

				pDC->Rectangle(rectOv);
			}
		}
	}

	pDC->SelectObject(pOldBrush);
	pDC->SelectObject(pOldPen);
}

//=============================================================================
// Method		: DrawCrosshair
// Access		: public  
// Returns		: void
// Parameter	: CDC * pDC
// Qualifier	:
// Last Update	: 2018/1/13 - 13:48
// Desc.		:
//=============================================================================
void CWnd_ImgProc_V2::DrawCrosshair(CDC* pDC)
{
	LOGBRUSH lb;
	lb.lbStyle = BS_SOLID;
	lb.lbColor = RGB(255, 0, 0);

	CPen NewPen;
	NewPen.CreatePen(PS_GEOMETRIC | PS_SOLID | PS_ENDCAP_FLAT, 1, &lb);
	CPen* pOldPen = pDC->SelectObject(&NewPen);
	CBrush NewBrush;
	NewBrush.CreateStockObject(NULL_BRUSH);
	CBrush* pOldBrush = pDC->SelectObject(&NewBrush);

	pDC->SetBkMode(TRANSPARENT);

	CRect rectOv;
	CRect rectClient;
	GetClientRect(rectClient);

	pDC->MoveTo(rectClient.CenterPoint().x, 0);
	pDC->LineTo(rectClient.CenterPoint().x, rectClient.bottom);

	pDC->MoveTo(0, rectClient.CenterPoint().y);
	pDC->LineTo(rectClient.right, rectClient.CenterPoint().y );

	pDC->SelectObject(pOldBrush);
	pDC->SelectObject(pOldPen);
}

//=============================================================================
// Method		: DrawVideo
// Access		: public  
// Returns		: void
// Parameter	: CDC * pDC
// Qualifier	:
// Last Update	: 2018/1/13 - 13:48
// Desc.		:
//=============================================================================
void CWnd_ImgProc_V2::DrawVideo(CDC *pDC)
{
	if (pDC == NULL)
		pDC = this->GetDC();

	// 메모리 DC 선언
	CDC memDC;
	CBitmap *pOldBitmap, bitmap;
	CRect rect;
	GetClientRect(&rect);

	// 화면 DC와 호환되는 메모리 DC 객체를 생성
	memDC.CreateCompatibleDC(pDC);

	// 마찬가지로 화면 DC와 호환되는 Bitmap 생성
	bitmap.CreateCompatibleBitmap(pDC, rect.Width(), rect.Height());

	pOldBitmap = memDC.SelectObject(&bitmap);
	memDC.PatBlt(0, 0, rect.Width(), rect.Height(), WHITENESS); // 흰색으로

	::SetStretchBltMode(memDC, COLORONCOLOR);
	
	if (m_bRender)
		DrawRGB(&memDC);
	else
		DrawEmptyRect(&memDC);

// 	if (m_bUseCrosshair)
// 		DrawCrosshair(&memDC);

	if (m_bUseOverlay)
		DrawOverlayRect(&memDC, m_BmpInfo.bmiHeader.biWidth, m_BmpInfo.bmiHeader.biHeight);

	// 메모리 DC를 화면 DC에 고속 복사
	pDC->BitBlt(0, 0, rect.Width(), rect.Height(), &memDC, 0, 0, SRCCOPY);

	memDC.SelectObject(pOldBitmap);
	memDC.DeleteDC();
	bitmap.DeleteObject();

	if (pDC == NULL)
	{
		this->ReleaseDC(pDC);
	}
}

//=============================================================================
// Method		: DrawRGB
// Access		: public  
// Returns		: void
// Parameter	: CDC * pDC
// Qualifier	:
// Last Update	: 2018/1/13 - 13:48
// Desc.		:
//=============================================================================
void CWnd_ImgProc_V2::DrawRGB(CDC *pDC)
{
	if (m_bSignal)
	{
		RECT	rc;
		GetClientRect(&rc);
		rc.right -= rc.left;
		rc.bottom -= rc.top;
		rc.top = rc.bottom - 1;

		::StretchDIBits(*pDC, rc.left, rc.top, rc.right, rc.bottom * (-1),
			0, 0, m_BmpInfo.bmiHeader.biWidth, m_BmpInfo.bmiHeader.biHeight,
			m_lpbyRGB, &m_BmpInfo, DIB_RGB_COLORS, SRCCOPY);

		// FPS 출력 한다면
		if (m_bUseFPS)
		{
			LOGBRUSH lb;
			lb.lbStyle = BS_SOLID;
			lb.lbColor = RGB(0, 0, 0);
			CPen NewPen;
			NewPen.CreatePen(PS_GEOMETRIC | PS_SOLID | PS_ENDCAP_FLAT, 1, &lb);
			CPen* pOldPen = pDC->SelectObject(&NewPen);

			CFont font;
			CString strText;
			font.CreatePointFont(130, _T("Arial"));
			pDC->SelectObject(&font);
			pDC->SetTextColor(RGB(0, 0, 255));
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextAlign(TA_LEFT | TA_BASELINE);

			strText.Format(_T("%d fps"), m_dwFrameRate);
			pDC->TextOut(20, 60, strText, strText.GetLength());
			font.DeleteObject();

			pDC->SelectObject(pOldPen);
		}
	}
	else
	{
		CBrush NewBrush;
		NewBrush.CreateStockObject(BLACK_BRUSH);
		CBrush* pOldBrush = pDC->SelectObject(&NewBrush);

		pDC->SetBkMode(TRANSPARENT);

		LOGBRUSH lb;
		lb.lbStyle = BS_SOLID;
		lb.lbColor = RGB(0, 0, 0);
		CPen NewPen;
		NewPen.CreatePen(PS_GEOMETRIC | PS_SOLID | PS_ENDCAP_FLAT, 1, &lb);
		CPen* pOldPen = pDC->SelectObject(&NewPen);

		CRect rectClient;
		GetClientRect(rectClient);
		pDC->Rectangle(rectClient); // 478

		CFont font;
		font.CreatePointFont(130, _T("Arial"));
		pDC->SelectObject(&font);
		pDC->SetTextColor(RGB(255, 0, 0));
		pDC->SetTextAlign(TA_LEFT | TA_BASELINE);
		CString strText = _T("No Signal");
		pDC->TextOut(20, 30, strText, strText.GetLength());
		font.DeleteObject();

		pDC->SelectObject(pOldBrush);
		pDC->SelectObject(pOldPen);
	}
}

//=============================================================================
// Method		: Render
// Access		: public  
// Returns		: void
// Parameter	: LPBYTE lpVideo
// Parameter	: DWORD dwWidth
// Parameter	: DWORD dwHeight
// Qualifier	:
// Last Update	: 2018/1/13 - 13:48
// Desc.		:
//=============================================================================
void CWnd_ImgProc_V2::Render(LPBYTE lpVideo, DWORD dwWidth, DWORD dwHeight)
{
	if (NULL == lpVideo)
		return;

	ZeroMemory(&m_BmpInfo, sizeof(m_BmpInfo));

	m_BmpInfo.bmiHeader.biSize			= sizeof(BITMAPINFOHEADER);
	m_BmpInfo.bmiHeader.biWidth			= dwWidth;
	m_BmpInfo.bmiHeader.biHeight		= dwHeight;
	m_BmpInfo.bmiHeader.biPlanes		= 1;
	m_BmpInfo.bmiHeader.biBitCount		= 24;
	m_BmpInfo.bmiHeader.biCompression	= BI_RGB;
	m_BmpInfo.bmiHeader.biSizeImage		= dwWidth * dwHeight * 3;

	// 복사 ?
	//m_lpbyRGB = lpVideo;	
	memcpy_s(m_lpbyRGB, dwWidth * dwHeight * 3, lpVideo, dwWidth * dwHeight * 3);
	
	m_dwRGBSize = m_BmpInfo.bmiHeader.biSizeImage;

	if (!m_bRender)
	{
		m_bRender = TRUE;
	}

	m_bSignal = TRUE;

	Invalidate();
}

//=============================================================================
// Method		: Render_Immediately
// Access		: public  
// Returns		: void
// Parameter	: LPBYTE lpVideo
// Parameter	: DWORD dwWidth
// Parameter	: DWORD dwHeight
// Qualifier	:
// Last Update	: 2018/1/13 - 13:48
// Desc.		:
//=============================================================================
void CWnd_ImgProc_V2::Render_Immediately(LPBYTE lpVideo, DWORD dwWidth, DWORD dwHeight)
{
	if (NULL == lpVideo)
		return;

	//ZeroMemory(&m_BmpInfo, sizeof(m_BmpInfo));

	m_BmpInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	m_BmpInfo.bmiHeader.biWidth = dwWidth;
	m_BmpInfo.bmiHeader.biHeight = dwHeight;
	m_BmpInfo.bmiHeader.biPlanes = 1;
	m_BmpInfo.bmiHeader.biBitCount = 24;
	m_BmpInfo.bmiHeader.biCompression = BI_RGB;
	m_BmpInfo.bmiHeader.biSizeImage = dwWidth * dwHeight * 3;

	CDC* pDC = this->GetDC();

	// 메모리 DC 선언
	CDC memDC;
	CBitmap *pOldBitmap, bitmap;
	CRect rect;
	GetClientRect(&rect);

	// 화면 DC와 호환되는 메모리 DC 객체를 생성
	memDC.CreateCompatibleDC(pDC);

	// 마찬가지로 화면 DC와 호환되는 Bitmap 생성
	bitmap.CreateCompatibleBitmap(pDC, rect.Width(), rect.Height());

	pOldBitmap = memDC.SelectObject(&bitmap);

	::SetStretchBltMode(memDC, COLORONCOLOR);

	RECT	rc;
	GetClientRect(&rc);
	rc.right -= rc.left;
	rc.bottom -= rc.top;
	rc.top = rc.bottom - 1;

	::StretchDIBits(memDC, rc.left, rc.top, rc.right, rc.bottom * (-1),
		0, 0, m_BmpInfo.bmiHeader.biWidth, m_BmpInfo.bmiHeader.biHeight,
		lpVideo, &m_BmpInfo, DIB_RGB_COLORS, SRCCOPY);

	// 메모리 DC를 화면 DC에 고속 복사
	pDC->BitBlt(0, 0, rect.Width(), rect.Height(), &memDC, 0, 0, SRCCOPY);

	memDC.SelectObject(pOldBitmap);
	memDC.DeleteDC();
	bitmap.DeleteObject();

	if (pDC != NULL)
	{
		this->ReleaseDC(pDC);
	}

	m_bSignal = TRUE;
}

//=============================================================================
// Method		: Render_Empty
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/27 - 18:04
// Desc.		:
//=============================================================================
void CWnd_ImgProc_V2::Render_Empty()
{
	m_dwFrameRate = 0;

	CDC* pDC = this->GetDC();
	
	memset (m_lpbyRGB, 0, MAX_IMAGE_BUFFER);

	m_bSignal = FALSE;

	if (pDC != NULL)
	{
		DrawEmptyRect(pDC);

		this->ReleaseDC(pDC);
	}
}

//=============================================================================
// Method		: SetUseOverlayRect
// Access		: public  
// Returns		: void
// Parameter	: INT_PTR nIndex
// Parameter	: BOOL bUse
// Qualifier	:
// Last Update	: 2017/8/27 - 18:46
// Desc.		:
//=============================================================================
void CWnd_ImgProc_V2::SetUseOverlayRect(INT_PTR nIndex, BOOL bUse)
{
	if (nIndex < m_arrUseRect.GetCount())
	{
		m_arrUseRect.ElementAt(nIndex) = bUse;
	}

	Invalidate();
}

//=============================================================================
// Method		: SetFPS
// Access		: public  
// Returns		: void
// Parameter	: __in DWORD dwFrameRate
// Qualifier	:
// Last Update	: 2018/1/13 - 20:19
// Desc.		:
//=============================================================================
void CWnd_ImgProc_V2::SetFPS(__in DWORD dwFrameRate)
{
	m_dwFrameRate = dwFrameRate;
}
