//*****************************************************************************
// Filename	: 	Wnd_Chart.h
// Created	:	2017/3/15 - 11:39
// Modified	:	2017/3/15 - 11:39
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Chart_h__
#define Wnd_Chart_h__

#pragma once

// 차트 종류
typedef enum enChartType
{
	Chart_Blank,
	Chart_Circle,
	Chart_GridChess,	
	Chart_Crosshair,
	Chart_Shapes,
};

// 차트에 사용되는 도형 형태
typedef enum enShapeType
{
	Shape_Ellipse,
	Shape_Rectangle,
	Shape_Polygon,
};

#define	MAX_SHAPE_POLYGON	10
// 차트에 사용되는 도형 정의
typedef struct _tag_ChartRect
{
	CRect		Rect;
	COLORREF	Color;

	enShapeType	ShapeType;
	
	POINT		Polygon[MAX_SHAPE_POLYGON];
	UINT		nPolyCount;
}ST_ChartRect, *PST_ChartRect;

// 체스판 모양의 차트 데이터
typedef struct _tag_ChartGridChess
{
	//CRect		rtPosition;
	UINT		nGridWidth;
	UINT		nGridHeight;
	COLORREF	clrFirst;
	COLORREF	clrSecond;
}ST_ChartGridChess, *PST_ChartGridChess;

// 십자선 모양의 차트 데이터
typedef struct _tag_ChartCrosshair
{
	float		fCenterLineThickness; 
	COLORREF	clrCenterLine;

 	float		fLineThickness;
 	COLORREF	clrLine;
}ST_ChartCrosshair, *PST_ChartCrosshair;


#define		USE_GDI_PLUS	// Gdiplus  사용여부 (Gdi / Gdiplus)

//-----------------------------------------------------------------------------
// CWnd_Chart
//-----------------------------------------------------------------------------
class CWnd_Chart : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Chart)

public:
	CWnd_Chart();
	virtual ~CWnd_Chart();

protected:
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnPaint			();
	afx_msg BOOL	OnEraseBkgnd	(CDC* pDC);
	
	DECLARE_MESSAGE_MAP()

	// 도형 그리기
	void			DrawBlank				(__in CDC* pDC);
	void			DrawBlank				(__in Gdiplus::Graphics& graphics);
	void			Draw_Ellipse			(__in CDC* pDC, __in CRect rect, __in COLORREF clrBack = RGB(0, 0, 0));
	void			Draw_Ellipse			(__in Gdiplus::Graphics& graphics, __in Gdiplus::Rect rect, __in Gdiplus::Color clrBack = Color::Black);
	void			Draw_Rectangle			(__in CDC* pDC, __in CRect rect, __in COLORREF clrBack = RGB(0, 0, 0));
	void			Draw_Rectangle			(__in Gdiplus::Graphics& graphics, __in Gdiplus::Rect rect, __in Gdiplus::Color clrBack = Color::Black);

	// 차트 그리기
	virtual void	DrawChartCrosshair		(__in CDC* pDC);
	virtual void	DrawChartCrosshair		(__in Gdiplus::Graphics& graphics);
	virtual void	DrawChartCricle			(__in CDC* pDC);
	virtual void	DrawChartCricle			(__in Gdiplus::Graphics& graphics);
	virtual void	DrawChartShape			(__in CDC* pDC);
	virtual void	DrawChartShape			(__in Gdiplus::Graphics& graphics);
	virtual void	DrawChartGridChess		(__in CDC* pDC);
	virtual void	DrawChartGridChess		(__in Gdiplus::Graphics& graphics);

	// 차트 데이터
	CArray<ST_ChartRect, ST_ChartRect&>		m_Item_Circle;
	CArray<ST_ChartRect, ST_ChartRect&>		m_Item_Shapes;
	ST_ChartGridChess						m_Item_GridChess;
	ST_ChartCrosshair						m_Item_Crosshair;

	enChartType			m_nChartType;

public:

	// 차트 종료 선택
	void	SetChartType				(__in enChartType nChartType);
	// 그리드 체스판 형태의 차트
	void	SetChart_GridChess			(/*__in CRect rtPosition, */__in UINT nGridWidth, __in UINT nGridHeight, __in COLORREF bFstColor = RGB(0, 0, 0), __in COLORREF b2ndColor = RGB(255, 255, 255));
	// 십자 모양의 차트
	void	SetChart_Crosshair			(__in float fCenterLineThickness, __in COLORREF clrCenterLine, __in float fLineThickness, __in COLORREF clrLine);

	// 원으로 구성된 차트 그리기
	void	RemoveAll_ChartCircle		();
	void	Add_ChartCircle				(__in POINT ptCenter, __in UINT nRadius, __in COLORREF clrBack = RGB(0, 0, 0));
	void	Add_ChartCircle				(__in CRect rectCircle, __in COLORREF clrBack = RGB(0, 0, 0));
	void	Delete_ChartCircle			(__in UINT nIdex);
	void	Modify_ChartCircle			(__in UINT nIdex, __in POINT ptCenter, __in UINT nRadius, __in COLORREF clrBack = RGB(0, 0, 0));
	void	Modify_ChartCircle			(__in UINT nIdex, __in CRect rectCircle, __in COLORREF clrBack = RGB(0, 0, 0));

	// 여러 모양의 차트 구성용
	void	RemoveAll_ChartShape		();
	void	Add_ChartShape				(__in enShapeType Shape, __in POINT ptCenter, __in UINT nWidth, __in UINT nHeight, __in COLORREF clrBack = RGB(0, 0, 0));
	void	Add_ChartShape				(__in enShapeType Shape, __in CRect rectRectangle, __in COLORREF clrBack = RGB(0, 0, 0));
	void	Delete_ChartShape			(__in UINT nIdex);
	void	Modify_ChartShape			(__in UINT nIdex, __in enShapeType Shape, __in POINT ptCenter,  __in UINT nWidth, __in UINT nHeight, __in COLORREF clrBack = RGB(0, 0, 0));
	void	Modify_ChartShape			(__in UINT nIdex, __in enShapeType Shape, __in CRect rectRectangle, __in COLORREF clrBack = RGB(0, 0, 0));

	// 차트 다시 그리기
	virtual void	UpdateChart			();

};

#endif // Wnd_Chart_h__


