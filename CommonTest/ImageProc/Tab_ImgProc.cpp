//*****************************************************************************
// Filename	: 	Tab_ImgProc.cpp
// Created	:	2018/2/8 - 11:08
// Modified	:	2018/2/8 - 11:08
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
// Tab_ImgProc.cpp : implementation file
//

#include "stdafx.h"
#include "Tab_ImgProc.h"


// CTab_ImgProc

IMPLEMENT_DYNAMIC(CTab_ImgProc, CMFCTabCtrl)

CTab_ImgProc::CTab_ImgProc()
{

}

CTab_ImgProc::~CTab_ImgProc()
{
}


BEGIN_MESSAGE_MAP(CTab_ImgProc, CMFCTabCtrl)
	ON_WM_CREATE()
END_MESSAGE_MAP()

// CTab_ImgProc message handlers
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2018/2/8 - 11:08
// Desc.		:
//=============================================================================
int CTab_ImgProc::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMFCTabCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	UINT	nWndID = 10;

	m_wnd_LiveImage.Create(NULL, _T(""), dwStyle | WS_BORDER, rectDummy, this, nWndID++);;

	for (UINT nIdx = 0; nIdx < MAX_IMAGE_WINDOW; nIdx++)
	{
		m_arwnd_ImageProc[nIdx].Create(NULL, _T(""), dwStyle | WS_BORDER, rectDummy, this, nWndID++);;
	}

	if (m_bUseLive)
	{
		AddTab(&m_wnd_LiveImage, _T("Live"), 0, FALSE);
		SetActiveTab(0);
	}

	Add_ImageTab (_T("1"));
	Add_ImageTab (_T("2"));
	Add_ImageTab (_T("3"));
	Add_ImageTab (_T("4"));

	return 1;
}

//=============================================================================
// Method		: Set_UseLiveImage
// Access		: public  
// Returns		: void
// Parameter	: __in BOOL bUseLive
// Qualifier	:
// Last Update	: 2018/2/14 - 14:40
// Desc.		:
//=============================================================================
void CTab_ImgProc::Set_UseLiveImage(__in BOOL bUseLive)
{
	if (m_bUseLive != bUseLive)
	{
		if (GetSafeHwnd()) // 윈도우가 생성된 이후 사용여부 변경
		{
			CStringArray szarLabel;
			CString szLabel;

			int iStartOffset = m_bUseLive ? 1 : 0;
			int iCount = GetTabsNum();

			if (0 < iCount)
			{
				// 탭의 이름 구하기
				for (INT nIdx = iStartOffset; nIdx < iCount; nIdx++)
				{
					GetTabLabel(nIdx, szLabel);
					szarLabel.Add(szLabel);
				}

				// Tab 삭제
				for (INT nIdx = 0; nIdx < iCount; nIdx++)
				{
					RemoveTab(nIdx, FALSE);
				}
				//RemoveAllTabs();
			}
			
			// 미사용 -> 사용
			if (bUseLive)
			{
				AddTab(&m_wnd_LiveImage, _T("Live"), 0, FALSE);
			}

			// 탭에 다시 추가
			iStartOffset = GetTabsNum();
			for (UINT nIdx = 0; nIdx < szarLabel.GetCount(); nIdx++)
			{
				this->AddTab(&m_arwnd_ImageProc[nIdx], szarLabel.GetAt(nIdx), iStartOffset++, FALSE);
			}

			if (0 < GetTabsNum())
			{
				SetActiveTab(0);
			}
		}

		m_bUseLive = bUseLive;
	}
}

//=============================================================================
// Method		: Add_ImageTab
// Access		: virtual public  
// Returns		: int
// Parameter	: __in LPCTSTR lpszTabLabel
// Qualifier	:
// Last Update	: 2018/2/14 - 14:40
// Desc.		:
//=============================================================================
int CTab_ImgProc::Add_ImageTab(__in LPCTSTR lpszTabLabel)
{
	++m_nWindowCount;

	int iWndIndex = m_nWindowCount - 1;
	if (m_bUseLive)
	{
		++iWndIndex; // Live Image 고정 1개
	}

	if (iWndIndex < MAX_IMAGE_WINDOW)
	{
		AddTab(&m_arwnd_ImageProc[iWndIndex], lpszTabLabel, iWndIndex, FALSE);
		
		if (FALSE == m_bUseLive) // Live Image 고정
		{
			SetActiveTab(iWndIndex);
		}
	}

	return iWndIndex;
}

//=============================================================================
// Method		: Remove_ImageTab
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/14 - 14:40
// Desc.		:
//=============================================================================
void CTab_ImgProc::Remove_ImageTab()
{
	if (0 < m_nWindowCount)
	{
		for (UINT nIdx = 0; nIdx < m_nWindowCount; nIdx++)
		{
			m_arwnd_ImageProc[nIdx].Render_Empty();

			if (m_bUseLive)
			{
				RemoveTab(nIdx + 1); // Live Image 고정 1개
			}
			else
			{
				RemoveTab(nIdx);
			}
		}

		//RemoveAllTabs();

		m_nWindowCount = 0;
	}
}

//=============================================================================
// Method		: ShowVideo
// Access		: public  
// Returns		: void
// Parameter	: __in UINT iWndIndex
// Parameter	: __in LPBYTE lpVideo
// Parameter	: __in DWORD dwWidth
// Parameter	: __in DWORD dwHeight
// Qualifier	:
// Last Update	: 2018/2/14 - 14:40
// Desc.		:
//=============================================================================
void CTab_ImgProc::ShowVideo(__in UINT iWndIndex, __in LPBYTE lpVideo, __in DWORD dwWidth, __in DWORD dwHeight)
{
	if (iWndIndex < MAX_IMAGE_WINDOW)
	{
		m_arwnd_ImageProc[iWndIndex].Render(lpVideo, dwWidth, dwHeight);
	}
}

//=============================================================================
// Method		: NoSignal_Ch
// Access		: public  
// Returns		: void
// Parameter	: __in UINT iWndIndex
// Qualifier	:
// Last Update	: 2018/2/14 - 14:40
// Desc.		:
//=============================================================================
void CTab_ImgProc::NoSignal_Ch(__in UINT iWndIndex)
{
	if (iWndIndex < MAX_IMAGE_WINDOW)
	{
		m_arwnd_ImageProc[iWndIndex].Render_Empty();
	}
}

//=============================================================================
// Method		: ShowVideo_Live
// Access		: public  
// Returns		: void
// Parameter	: __in LPBYTE lpVideo
// Parameter	: __in DWORD dwWidth
// Parameter	: __in DWORD dwHeight
// Qualifier	:
// Last Update	: 2018/2/14 - 14:47
// Desc.		:
//=============================================================================
void CTab_ImgProc::ShowVideo_Live(__in LPBYTE lpVideo, __in DWORD dwWidth, __in DWORD dwHeight)
{
	if (m_bUseLive)
	{
		m_wnd_LiveImage.Render(lpVideo, dwWidth, dwHeight);
	}
}

//=============================================================================
// Method		: NoSignal_Live
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/14 - 14:47
// Desc.		:
//=============================================================================
void CTab_ImgProc::NoSignal_Live()
{
	if (m_bUseLive)
	{
		m_wnd_LiveImage.Render_Empty();
	}
}

