﻿#ifndef Def_T_Current_h__
#define Def_T_Current_h__

#include <afxwin.h>
#include "Def_Test_Cm.h"

#pragma pack(push,1)

typedef enum enCurrent
{
	Current_1st = 0,
	Current_2nd,
	Current_Max,
};

typedef struct _tag_Current_Opt
{
	double dbOffset;

	_tag_Current_Opt()
	{
		dbOffset = 0.0;
	};

	_tag_Current_Opt& operator= (_tag_Current_Opt& ref)
	{
		dbOffset = ref.dbOffset;
		
		return *this;
	};

}ST_Current_Opt, *PST_Current_Opt;

typedef struct _tag_Current_Data
{
	UINT nResult;
	
	double dbCurrent[Current_Max];

	_tag_Current_Data()
	{
		Reset();
	};

	void Reset()
	{
		nResult  = TR_Pass;

		for (UINT nIdx = 0; nIdx < Current_Max; nIdx++)
		{
			dbCurrent[nIdx] = 0.0;
		}

	};

	_tag_Current_Data& operator= (_tag_Current_Data& ref)
	{
		nResult  = ref.nResult;

		for (UINT nIdx = 0; nIdx < Current_Max; nIdx++)
		{
			dbCurrent[nIdx] = ref.dbCurrent[nIdx];
		}

		return *this;
	};

}ST_Current_Data, *PST_Current_Data;

typedef struct _tag_TI_Current
{
	// 테스트 항목
	CString	szrItem;

	// 검사 기준 데이터
	ST_Current_Opt	stCurrentOpt;

	// 측정 데이터
	ST_Current_Data stCurrentData;

	_tag_TI_Current()
	{
		szrItem = _T("Current");
	};

	void Reset()
	{
		stCurrentData.Reset();
	};

	_tag_TI_Current& operator= (_tag_TI_Current& ref)
	{
		stCurrentOpt = ref.stCurrentOpt;
		stCurrentData = ref.stCurrentData;

		return *this;
	};

}ST_TI_Current, *PST_TI_Current;

#pragma pack(pop)

#endif // Def_T_Current_h__