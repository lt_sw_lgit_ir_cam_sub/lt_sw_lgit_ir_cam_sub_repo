//*****************************************************************************
// Filename	: 	Def_EEPROM_Cm.h
// Created	:	2018/2/9 - 15:26
// Modified	:	2018/2/9 - 15:26
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#ifndef Def_EEPROM_Cm_h__
#define Def_EEPROM_Cm_h__

#include "Def_Enum_Cm.h"


// enModelType::Model_OMS_Entry
// enModelType::Model_OMS_Front
// enModelType::Model_MRA2
// enModelType::Model_IKC

#define		EEPROM_SlaveAddr_OMS_Entry		0xAC	// 0x56 << 1
#define		EEPROM_SlaveAddr_OMS_Front		0xFF	// 0x56 << 1
#define		EEPROM_SlaveAddr_MRA2			0xFF	// 0x56 << 1
#define		EEPROM_SlaveAddr_IKC			0xA0	// 0x50 << 1


typedef struct _tag_EEPROM_Item
{
	BOOL		bUse;
	UINT		nItemID;
	LPCTSTR		szItemName;
	
	DWORD		Address;
	WORD		DataLength;
}ST_EEPROM_Item, *PST_EEPROM_Item;

typedef enum enEEPROM_Item_ID
{
	E2RI_SN,
	E2RI_ADDI_ID,
	E2RI_Sensor_ID,
	E2RI_FW_Ver,
	E2RI_OC_X,
	E2RI_OC_Y,
	E2RI_2D_CAL_Param,
	E2RI_VCSEL_PWM,
	E2RI_Checksum_1,
	E2RI_Checksum_2,
	E2RI_Mode,
};

static ST_EEPROM_Item g_E2ROM_Entry[] =
{
	{ TRUE,		E2RI_SN,			_T(""), 0x00, 16 },
	{ TRUE,		E2RI_ADDI_ID,		_T(""), 0x10, 16 },
	{ TRUE,		E2RI_Sensor_ID,		_T(""), 0x20, 16 },
	{ FALSE,	E2RI_FW_Ver,		_T(""), 0x30, 16 },
	{ TRUE,		E2RI_OC_X,			_T(""), 0x40, 4 },
	{ TRUE,		E2RI_OC_Y,			_T(""), 0x44, 4 },
	{ FALSE,	E2RI_2D_CAL_Param,	_T(""), 0x48, 36 },
	{ FALSE,	E2RI_VCSEL_PWM,		_T(""), 0x6C, 1 },
	{ TRUE,		E2RI_Checksum_1,	_T(""), 0xFA, 2 },
	{ FALSE,	E2RI_Checksum_2,	_T(""), 0xFC, 2 },
	{ FALSE,	E2RI_Mode,			_T(""), 0xFE, 2 },
};

static ST_EEPROM_Item g_E2ROM_Front[] =
{
	{ TRUE,		E2RI_SN,			_T(""), 0x00, 16 },
	{ TRUE,		E2RI_ADDI_ID,		_T(""), 0x10, 16 },
	{ TRUE,		E2RI_Sensor_ID,		_T(""), 0x20, 16 },
	{ TRUE,		E2RI_FW_Ver,		_T(""), 0x30, 16 },
	{ TRUE,		E2RI_OC_X,			_T(""), 0x40, 4 },
	{ TRUE,		E2RI_OC_Y,			_T(""), 0x44, 4 },
	{ TRUE,		E2RI_2D_CAL_Param,	_T(""), 0x48, 36 },
	{ TRUE,		E2RI_VCSEL_PWM,		_T(""), 0x6C, 1 },
	{ TRUE,		E2RI_Checksum_1,	_T(""), 0xFA, 2 },
	{ TRUE,		E2RI_Checksum_2,	_T(""), 0xFC, 2 },
	{ TRUE,		E2RI_Mode,			_T(""), 0xFE, 2 },
};


#endif // Def_EEPROM_Cm_h__
