//*****************************************************************************
// Filename	: 	Sequence_Mgr_Base.h
// Created	:	2018/1/17 - 16:09
// Modified	:	2018/1/17 - 16:09
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#ifndef Sequence_Mgr_h__
#define Sequence_Mgr_h__

#pragma once

#include <afxwin.h>

//-----------------------------------------------------------------------------
// CSequence_Mgr_Base
//-----------------------------------------------------------------------------
class CSequence_Mgr_Base
{
public:
	CSequence_Mgr_Base();
	~CSequence_Mgr_Base();


	// pure virtual function
	virtual long	OnMotion_MoveAxis		(__in UINT nAxisIndex, __in double dPulse) const = 0;
	virtual long	OnIO_SetStatus			(__in UINT nDO_Offset, __in BOOL bSet) const = 0;	
	virtual long	OnComm_PCB				(__in UINT nPCBIndex, __in DWORD dwAckTimeout) const = 0;

	// �˻� �׸�
	virtual long	OnProc_TestItem			(__in UINT nTestItemID) const = 0;

	virtual long	OnProc_JigGroup			(__in UINT nJigGroupID) const = 0;
	virtual long	OnProc_TestGroup		(__in UINT nTestGroupID) const = 0;

	



};

#endif // Sequence_Mgr_h__
