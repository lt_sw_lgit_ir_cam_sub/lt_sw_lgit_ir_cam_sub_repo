//*****************************************************************************
// Filename	: 	Def_Seq_JIG.h
// Created	:	2018/1/17 - 15:31
// Modified	:	2018/1/17 - 15:31
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#ifndef Def_Seq_JIG_h__
#define Def_Seq_JIG_h__


// Motor
// I/O
// I/O - MCU
// PLC

// ----- Motor ------
// Alias 

typedef enum enLTSeqMotorType
{
	SM_Type_Pulse,	// Pulse 값
	SM_Type_Meter,	// Meter 법 단위
	SM_Type_Angle,	// 회전 각도
};

typedef struct _tag_MotorCtrl
{
	short		shMoveType;

	double		dPulse;
	long		lMilimeter;
	double		dAngle;

}ST_MotorCtrl, *PST_MotorCtrl;

typedef struct _tag_Seq_MotorAxis
{
	CString			szAxisName;	// 축 이름
	BOOL			bEnable;	// 사용/미사용
	UINT			nAxisID;	// 축 구분
	
	ST_MotorCtrl	AxisControl;

}ST_Seq_MotorAxis, *PST_Seq_MotorAxis;

// I/O -> Set/ Clear
typedef struct _tag_Seq_IO_Offset
{
	CString			szOffsetName;
	BOOL			bEnable;	// 사용/미사용

	short			shIOType;	// DIO / IO MCU

	BOOL			bBitStatus;	// Set / Clear
	UINT			nOffset;	// DO Offset

}ST_Seq_IO, *PST_Seq_IO;


//BOOL	bEnable; bDisable

#endif // Def_Seq_JIG_h__
