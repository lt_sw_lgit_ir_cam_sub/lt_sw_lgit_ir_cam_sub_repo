//*****************************************************************************
// Filename	: 	ADLink_DASK.h
// Created	:	2018/1/4 - 11:22
// Modified	:	2018/1/4 - 11:22
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#ifndef ADLINK_DASK_h__
#define ADLINK_DASK_h__

#pragma once

#include <afxwin.h>


#include "Def_ADLink_DASK.h"

//-----------------------------------------------------------------------------
// ADLink PCIS-DASK
//-----------------------------------------------------------------------------
class CADLink_DASK
{
public:
	CADLink_DASK();
	~CADLink_DASK();

};

#endif // ADLINK_DASK_h__

