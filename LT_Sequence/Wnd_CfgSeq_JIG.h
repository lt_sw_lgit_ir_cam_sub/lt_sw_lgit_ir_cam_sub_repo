//*****************************************************************************
// Filename	: 	Wnd_CfgSeq_JIG.h
// Created	:	2018/1/17 - 15:34
// Modified	:	2018/1/17 - 15:34
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#ifndef Wnd_CfgSeq_JIG_h__
#define Wnd_CfgSeq_JIG_h__

#pragma once
#include "afxwin.h"

#include "Def_Seq_JIG.h"

//-----------------------------------------------------------------------------
// CWnd_CfgSeq_JIG
//-----------------------------------------------------------------------------
class CWnd_CfgSeq_JIG :	public CWnd
{
public:
	CWnd_CfgSeq_JIG();
	virtual ~CWnd_CfgSeq_JIG();



};

#endif // Wnd_CfgSeq_JIG_h__
