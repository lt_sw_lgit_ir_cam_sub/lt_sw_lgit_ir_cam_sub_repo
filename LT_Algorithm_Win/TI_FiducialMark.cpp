﻿//*****************************************************************************
// Filename	: 	TI_FiducialMark.cpp
// Created	:	2018/2/11 - 15:46
// Modified	:	2018/2/11 - 15:46
//
// Author	:	Luritech
//	
// Purpose	:	
//****************************************************************************
//#include "stdafx.h"
#include "TI_FiducialMark.h"
#include <math.h>

CTI_FiducialMark::CTI_FiducialMark()
{
	
}

CTI_FiducialMark::~CTI_FiducialMark()
{

}

//=============================================================================
// Method		: FiducialMark_Test
// Access		: public  
// Returns		: int
// Parameter	: IN LPWORD pImageBuf
// Parameter	: IN UINT dwWidth
// Parameter	: IN UINT dwHeight
// Parameter	: IN ST_ROI stROI
// Parameter	: IN BYTE byMarkColor
// Parameter	: IN BYTE byBrightness
// Parameter	: OUT POINT & ptOUT_Center
// Qualifier	:
// Last Update	: 2018/2/11 - 15:45
// Desc.		:
//=============================================================================
int CTI_FiducialMark::FiducialMark_Test(IN LPWORD pImageBuf, IN UINT dwWidth, IN UINT dwHeight, IN ST_ROI stROI, IN BYTE byMarkColor, IN BYTE byBrightness, OUT POINT& ptOUT_Center)
{
	if (nullptr == pImageBuf)
		return -1;

	POINT ptCenter;

	// 초기값
	ptCenter.x = -1;
	ptCenter.y = -1;

	IplImage *OriginImage = cvCreateImage(cvSize(stROI.sWidth, stROI.sHeight), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(stROI.sWidth, stROI.sHeight), IPL_DEPTH_8U, 1);

	WORD wData16Bit = 0;
	BYTE byData8Bit = 0;

	cvSetZero(OriginImage);

	for (int y = stROI.sTop; y < stROI.sTop + stROI.sHeight; y++)
	{
		for (int x = stROI.sLeft; x < stROI.sLeft + stROI.sWidth; x++)
		{
			wData16Bit = *(pImageBuf + y * dwWidth + x);

			wData16Bit = wData16Bit & 0xF000 ? 0x0FFF : wData16Bit;
			byData8Bit = (wData16Bit >> 4) & 0x00FF;
		
			if (byBrightness >= 0)
				byData8Bit = (byData8Bit << byBrightness) >= 0xFF ? 0xFF : (byData8Bit << byBrightness);
			else
				byData8Bit = (byData8Bit >> (-1 * byBrightness)) <= 0x00 ? 0x00 : (byData8Bit >> (-1 * byBrightness));

			OriginImage->imageData[(y - stROI.sTop) * OriginImage->widthStep + (x - stROI.sLeft)] = byData8Bit;
		}
	}


	bool bIsCornerDarkArea = false;
	int Offset_X = 1, Offset_Y = 1;

	if (stROI.sLeft + stROI.sWidth < 80)
	{
		if (stROI.sTop + stROI.sHeight < 80)
		{
			bIsCornerDarkArea = true;
			Offset_X *= -1;  Offset_Y *= -1;
		}
		if (stROI.sTop > dwHeight - 80)
		{
			bIsCornerDarkArea = true;
			Offset_X *= -1;  Offset_Y *= 1;
		}
	}

	if (stROI.sLeft > dwWidth - 80)
	{
		if (stROI.sTop + stROI.sHeight < 80)
		{
			bIsCornerDarkArea = true;
			Offset_X *= 1;  Offset_Y *= -1;
		}
		if (stROI.sTop > dwHeight - 80)
		{
			bIsCornerDarkArea = true;
			Offset_X *= 1;  Offset_Y *= 1;
		}
	}

	cvAdaptiveThreshold(OriginImage, DilateImage, 255, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 15, 3.0);

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvRect *rectArray = new CvRect[2];

	int iCounter = 0;
	CvRect Rect;
	CvSeq *contour = 0;

	if (bIsCornerDarkArea)
	{ 
		// 네 모서리 코너다크 부분(어두운 부분)의 Mark 찾기 인 경우.....
		cvDilate(DilateImage, DilateImage);
		cvDilate(DilateImage, DilateImage);
		cvNot(DilateImage, DilateImage);

		cvFindContours(DilateImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

		int MaxRectSize = 0;

		double Rate;
		for (; contour != 0; contour = contour->h_next)
		{
			Rect = cvContourBoundingRect(contour, 1);
			Rate = (double)Rect.width / (double)Rect.height;

			// ROI 사이즈 비율 변경 1.5 -> 2.0
			if ((Rect.width > 4) && (Rect.width <= (stROI.sWidth / 2)) && Rect.height > 4 && (Rect.height <= (stROI.sHeight / 2)) && Rate > 0.5 && Rate < 2.0)
			{
				int RectSize = Rect.width + Rect.height;
				int CenterX = Rect.x + Rect.width / 2;
				int CenterY = Rect.y + Rect.height / 2;

				if (iCounter == 0)
				{
					MaxRectSize = RectSize;
					ptCenter.x = CenterX;
					ptCenter.y = CenterY;
					iCounter++;
				}
				else if ((RectSize >= MaxRectSize - 4) && (Offset_X*(ptCenter.x - CenterX)) > 0 && (Offset_Y*(ptCenter.y - CenterY)) > 0)
				{
					MaxRectSize = RectSize;
					ptCenter.x = CenterX;
					ptCenter.y = CenterY;
					iCounter++;
				}
			}
		}

		if (iCounter == 0)
		{
			ptCenter.x = -1;
			ptCenter.y = -1;

			cvReleaseMemStorage(&contour_storage);
			cvReleaseImage(&DilateImage);
			cvReleaseImage(&OriginImage);

			delete[] rectArray;
			
			ptOUT_Center = ptCenter;

			return 1;
		}

		ptCenter.x += Offset_X * 6;
		ptCenter.y += Offset_Y * 6;

	}
	else
	{
		if (byMarkColor == MarkCol_Black)
		{
			cvNot(DilateImage, DilateImage);
		}
	
		cvErode(DilateImage, DilateImage);

		IplImage *RGBResultImage = cvCreateImage(cvSize(stROI.sWidth, stROI.sHeight), IPL_DEPTH_8U, 3);

		cvFindContours(DilateImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

		double Rate;
		double old_dist = 99999.0;
		double old_dist2 = 99999.0;
		for (; contour != 0; contour = contour->h_next)
		{
			Rect = cvContourBoundingRect(contour, 1);
			Rate = (double)Rect.width / (double)Rect.height;
		
			if ((Rect.width > 4) && (Rect.width <= (stROI.sWidth / 2)) && Rect.height > 4 && (Rect.height <= (stROI.sHeight / 2)) && Rate > 0.5 && Rate < 2.5)
			{
				double distance = GetDistance(Rect.x + Rect.width / 2, Rect.y + Rect.height / 2, stROI.sWidth / 2, stROI.sHeight / 2);

				if (distance < old_dist || distance < old_dist2)
				{
					if (distance >= old_dist && distance < old_dist2)
					{ // 두 거리 사이의 거리가 들어오면 
						rectArray[1] = Rect;		//두번째 거리에 있는 Rect 에 입력
						old_dist2 = distance;
					}
					else // 최소 거리가 들어오면 
					{
						rectArray[1] = rectArray[0];// 가지고 있던 Rect 저장 ( 두번째로 가까운 Rect )
						rectArray[0] = Rect;		// 새로운 Rect 저장		 ( 제일 가까운 Rect )
						old_dist2 = old_dist;
						old_dist = distance;
					}

					iCounter++;
				}
			}
		}

		if (iCounter < 2)
		{
			ptCenter.x = -1;
			ptCenter.y = -1;

			cvReleaseMemStorage(&contour_storage);
			cvReleaseImage(&DilateImage);
			cvReleaseImage(&OriginImage);
			delete[]rectArray;

			ptOUT_Center = ptCenter;

			return 1;
		}

		ptCenter.x = (int)((double)(rectArray[0].x + (rectArray[0].x + rectArray[0].width) + rectArray[1].x + (rectArray[1].x + rectArray[1].width)) / 4.0);
		ptCenter.y = (int)((double)(rectArray[0].y + (rectArray[0].y + rectArray[0].height) + rectArray[1].y + (rectArray[1].y + rectArray[1].height)) / 4.0);
	}

	ptCenter.x += stROI.sLeft;
	ptCenter.y += stROI.sTop;

	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&DilateImage);
	cvReleaseImage(&OriginImage);

	delete[]rectArray;

	ptOUT_Center = ptCenter;

	return 1;
}
