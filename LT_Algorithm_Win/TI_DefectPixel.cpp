//*****************************************************************************
// Filename	: 	TI_DefectPixel.cpp
// Created	:	2018/2/10 - 17:26
// Modified	:	2018/2/10 - 17:26
//
// Author	:	Luritech
//	
// Purpose	:	
//****************************************************************************
//#include "stdafx.h"
#include "TI_DefectPixel.h"
#include <math.h>

CTI_DefectPixel::CTI_DefectPixel()
{
	
}

CTI_DefectPixel::~CTI_DefectPixel()
{

}

enum { VeryHot, Hot, VeryBright, Bright, VeryDark, Dark, RowCol,Cluster, NONE };

//=============================================================================
// Method		: DPDetection
// Access		: public  
// Returns		: BOOL
// Parameter	: IN IplImage * pImageBuf
// Parameter	: OUT stDPList & DPList
// Parameter	: IN int MODE
// Parameter	: IN int VHP_Th
// Parameter	: IN int HP_Th
// Parameter	: IN int VBP_Th
// Parameter	: IN int BP_Th
// Parameter	: IN int VDP_Th
// Parameter	: IN int DP_Th
// Parameter	: IN int Max_Value
// Parameter	: IN int Block_Size
// Qualifier	:
// Last Update	: 2018/2/27 - 18:46
// Desc.		:
//=============================================================================
BOOL CTI_DefectPixel::DPDetection(IN IplImage* pImageBuf, OUT stDPList& DPList, IN int MODE, IN int VHP_Th, IN int HP_Th, IN int VBP_Th, IN int BP_Th, IN int VDP_Th, IN int DP_Th, IN int RC_Th, IN int Max_Value, IN int Block_Size)
{
	if (pImageBuf == NULL)
	{
		TRACE(_T("InputImage 없음"));
		return FALSE;
	}

	IplImage* srcImage = NULL;
	if (pImageBuf->depth > 8)
		srcImage = cvCreateImage(cvGetSize(pImageBuf), IPL_DEPTH_16U, 1);
	else if (pImageBuf->depth <= 8)
		srcImage = cvCreateImage(cvGetSize(pImageBuf), IPL_DEPTH_8U, 1);


	if (pImageBuf->nChannels == 3)
		cvCvtColor(pImageBuf, srcImage, CV_RGB2GRAY);
	else
		cvCopy(pImageBuf, srcImage);

	int Illumination_MODE = MODE; // 0 이면 암실 환경, 1 광원 환경

	INT32 Sum = 0;
	int _c = 0;

	int Block_Sizes = Block_Size; // 파라미터로 들어갈것 무조건 홀수일것 -> 단일 Pixel 1개 포함

	if ((Block_Sizes % 2) == 0) // 만약 짝수일때 +1 해줌 ^^
		Block_Sizes++;

	// Type 1, 2 : 암실 환경 Hot Pixel
	if (Illumination_MODE == 0)
	{
		CvScalar Tmp;
		for (int j = 0; j < srcImage->height; j++)
		{
			for (int i = 0; i < srcImage->width; i++)
			{
				Tmp = cvGet2D(srcImage, j, i);

				Sum += (int)Tmp.val[0];
				_c++;
			}
		}
		int AVG = 0;
		if (_c)
			AVG = Sum / _c;
		else
		{
			TRACE(_T("Err : 0으로 나눌수 없습니다."));
			AVG = 0;
		}

		int GetDiff = 0;

		// VHP, HP 탐색
		for (int j = 0; j < srcImage->height; j++)
		{
			for (int i = 0; i < srcImage->width; i++)
			{
				Tmp = cvGet2D(srcImage, j, i);
				GetDiff = (int)Tmp.val[0] - AVG;
				if (GetDiff >= HP_Th)
				{
					if (DPList.DefectPixe_Cnt <MAX_PARTICLE_COUNT)
					{
						if (GetDiff >= VHP_Th)
							DPList.DP_List[DPList.DefectPixe_Cnt].nType = VeryHot;
						else
							DPList.DP_List[DPList.DefectPixe_Cnt].nType = Hot;

						DPList.DP_List[DPList.DefectPixe_Cnt].x = i;
						DPList.DP_List[DPList.DefectPixe_Cnt].y = j;
						DPList.DP_List[DPList.DefectPixe_Cnt].W = 1;
						DPList.DP_List[DPList.DefectPixe_Cnt].H = 1;
						DPList.DP_List[DPList.DefectPixe_Cnt].dbLSBValue = GetDiff;
						DPList.DefectPixe_Cnt++;
					}
					else{
						break;
					}
				
				}

			}
		}
	}
	else if (Illumination_MODE == 1)
	{
		int GapOfSide = (int)(Block_Sizes / 2);

		int Startx = 0;
		int Starty = 0;
		int Endx = 0;
		int Endy = 0;

		CvScalar Tmp_A;
		CvScalar Tmp_A_Next;
		for (int j = 0; j < srcImage->height; j++)
		{
			for (int i = 0; i < srcImage->width; i++)
			{
				Tmp_A = cvGet2D(srcImage, j, i);

				Sum += (int)Tmp_A.val[0];
				_c++;
			}
		}
		int AVG = 0;
		if (_c)
			AVG = Sum / _c;
		else
		{
			TRACE(_T("Err : 0으로 나눌수 없습니다."));
			AVG = 0;
		}

		int GetDiff = 0;

		//CvPoint Dead_Candidate[MAX_PARTICLE_COUNT];
		CvPoint *Dead_Candidate;
		Dead_Candidate = new CvPoint[MAX_PARTICLE_COUNT];

		int Candidate_Cut = Max_Value / 50;
		int _c = 0;
		// Dead Pixel 후보군 탐색
		for (int j = 0; j < srcImage->height; j++)
		{
			for (int i = 0; i < srcImage->width - 1; i++)
			{
				if (_c < MAX_PARTICLE_COUNT)
				{
					Tmp_A = cvGet2D(srcImage, j, i);
					Tmp_A_Next = cvGet2D(srcImage, j, i + 1);

					if (abs(Tmp_A.val[0] - Tmp_A_Next.val[0]) >= Candidate_Cut)
					{
						Dead_Candidate[_c].x = i;
						Dead_Candidate[_c].y = j;
						_c++;
					}
				}

			}
		}

		//후보군의 주변영역 포함 실제 밝기률 산출 
		for (int i = 0; i < _c; i++)
		{
			if (MAX_PARTICLE_COUNT < i)
			{
				break;
			}
			Startx = Dead_Candidate[i].x - GapOfSide;
			Endx = Dead_Candidate[i].x + GapOfSide;
			Starty = Dead_Candidate[i].y - GapOfSide;
			Endy = Dead_Candidate[i].y + GapOfSide;

			//사이드 여백 구간일 경우 보정
			if (!(Dead_Candidate[i].x >= GapOfSide && Dead_Candidate[i].x <= srcImage->width - 1 - GapOfSide && Dead_Candidate[i].y >= GapOfSide && Dead_Candidate[i].y <= srcImage->height - 1 - GapOfSide))
			{
				// x 보정
				if (Startx < 0)
				{
					Startx = 0;
					Endx = GapOfSide * 2;
				}
				if (Endx >= srcImage->width)
				{
					Startx = srcImage->width - 1 - (GapOfSide * 2);
					Endx = Startx + Block_Sizes - 1;
				}

				// y 보정
				if (Starty < 0)
				{
					Starty = 0;
					Endy = GapOfSide * 2;
				}
				if (Endy >= srcImage->height)
				{
					Starty = srcImage->height - 1 - (GapOfSide * 2);
					Endy = Starty + Block_Sizes;
				}

			}

			int Block_mean = 0;
			int Bcount = 0;
			// Block 단위 Mean 추출
			for (int Bx = Startx; Bx <= Endx; Bx++)
			{
				for (int By = Starty; By <= Endy; By++)
				{
					if (Bx >= 0 && Bx < srcImage->width && By >= 0 && By < srcImage->height)
					{
						if (!(Bx == Dead_Candidate[i].x && By == Dead_Candidate[i].y))
						{
							CvScalar Tmp = cvGet2D(srcImage, By, Bx);
							Block_mean += (int)(Tmp.val[0]);
							Bcount++;
						}
					}
					else
					{
						int a = 0;
						a = 1;
					}
				}
			}
			int mean_b = 0;

			if (Bcount)
				mean_b = (int)(Block_mean / Bcount);
			else
			{
				mean_b = 0;
				TRACE(_T("Err : 0으로 나눌수 없습니다."));
			}
			CvScalar B_Pixel = cvGet2D(srcImage, Dead_Candidate[i].y, Dead_Candidate[i].x);

			double Bright_Percent = (B_Pixel.val[0] - mean_b) / mean_b * 100;

			if (abs(Bright_Percent) >= BP_Th)
			{

				// 밝은놈 구분
				if (DPList.DefectPixe_Cnt < MAX_PARTICLE_COUNT)
				{

					if (Bright_Percent >= BP_Th)
					{
						if (Bright_Percent >= VBP_Th)
							DPList.DP_List[DPList.DefectPixe_Cnt].nType = VeryBright;
						else
							DPList.DP_List[DPList.DefectPixe_Cnt].nType = Bright;
					}
					// 어두운놈 구분
					if (Bright_Percent <= DP_Th)
					{
						if (Bright_Percent <= VDP_Th)
							DPList.DP_List[DPList.DefectPixe_Cnt].nType = VeryDark;
						else
							DPList.DP_List[DPList.DefectPixe_Cnt].nType = Dark;
					}

					DPList.DP_List[DPList.DefectPixe_Cnt].x = Dead_Candidate[i].x;
					DPList.DP_List[DPList.DefectPixe_Cnt].y = Dead_Candidate[i].y;
					DPList.DP_List[DPList.DefectPixe_Cnt].W = 1;
					DPList.DP_List[DPList.DefectPixe_Cnt].H = 1;
					DPList.DP_List[DPList.DefectPixe_Cnt].dbDeviationRate = Bright_Percent;
					DPList.DefectPixe_Cnt++;
				}
				else{
					break;
				}
			}
		}

		delete[] Dead_Candidate;

	}
	// 중복된 녀석은 제거 한다
	double Tmp_Dis_Same = 0.0;

	for (int i = 0; i < DPList.DefectPixe_Cnt; i++)
	{
		for (int j = i + 1; j < DPList.DefectPixe_Cnt; j++)
		{
			int x1 = DPList.DP_List[i].x;
			int y1 = DPList.DP_List[i].y;
			int x2 = DPList.DP_List[j].x;
			int y2 = DPList.DP_List[j].y;
			Tmp_Dis_Same = GetDistance(x1, y1, x2, y2);
			
			if (Tmp_Dis_Same == 0)
			{
				DPList.DefectPixe_Cnt--;
				for (int t = j; t < DPList.DefectPixe_Cnt; t++)
				{
					DPList.DP_List[t].x = DPList.DP_List[t+1].x;
					DPList.DP_List[t].y = DPList.DP_List[t+1].y;
					DPList.DP_List[t].W = DPList.DP_List[t+1].W;
					DPList.DP_List[t].H = DPList.DP_List[t+1].H;
					DPList.DP_List[t].dbDeviationRate = DPList.DP_List[t + 1].dbDeviationRate;
					DPList.DP_List[t].nType = DPList.DP_List[t + 1].nType;
				}

			}
		}
	}

	// 찾은 놈들 중에 달라붙어있는 녀석이 있으면 둘다 Cluster로 구분한다.
	double Tmp_Dis = 0.0;
	for (int i = 0; i < DPList.DefectPixe_Cnt; i++)
	{
		for (int j = i + 1; j < DPList.DefectPixe_Cnt; j++)
		{
			int x1 = DPList.DP_List[i].x;
			int y1 = DPList.DP_List[i].y;
			int x2 = DPList.DP_List[j].x;
			int y2 = DPList.DP_List[j].y;
			Tmp_Dis = GetDistance(x1, y1, x2, y2);

			if (Tmp_Dis <= 1 && Tmp_Dis > 0)
			{
				DPList.DP_List[i].nType = Cluster;
				DPList.DP_List[j].nType = Cluster;
			}
		}
	}

	// Row, col 줄무늬 검출 추가
	CvScalar Tmp;

	double means = 0.0;

	int* Row_Means = new int[srcImage->height];

	for (int j = 0; j < srcImage->height; j++)
	{
		means = 0;
		for (int i = 0; i < srcImage->width; i++)
		{
			Tmp = cvGet2D(srcImage, j, i);
			means += Tmp.val[0];

		}
		Row_Means[j] = means / (double)srcImage->width;

	}

	double Prev_means	= 0;
	double Now_means	= 0;
	double Next_means	= 0;

	double Percent_Prev = 0;
	double Percent_Next = 0;

	// 튀는 값 찾기
	for (int i = 1; i < srcImage->height-1; i++)
	{
		Prev_means	= Row_Means[i - 1];
		Now_means	= Row_Means[i];
		Next_means	= Row_Means[i + 1];

		Percent_Prev = (Now_means / Prev_means * 100) - 100;
		Percent_Next = (Next_means / Now_means * 100) - 100;

		if (abs(Percent_Prev) > RC_Th || abs(Percent_Next) > RC_Th)
		{
			if (DPList.DefectPixe_Cnt < MAX_PARTICLE_COUNT)
			{
				DPList.DP_List[DPList.DefectPixe_Cnt].x = 0;
				DPList.DP_List[DPList.DefectPixe_Cnt].y = i - 10;
				DPList.DP_List[DPList.DefectPixe_Cnt].W = srcImage->width;
				DPList.DP_List[DPList.DefectPixe_Cnt].H = 20;
				DPList.DP_List[DPList.DefectPixe_Cnt].nType = RowCol;
				if (abs(Percent_Prev) > RC_Th)
					DPList.DP_List[DPList.DefectPixe_Cnt].dbDeviationRate = Percent_Prev;
				else if (abs(Percent_Next) > RC_Th)
					DPList.DP_List[DPList.DefectPixe_Cnt].dbDeviationRate = Percent_Next;
				i += 5;

				DPList.DefectPixe_Cnt++;
			}
			else{
				break;
			}
	
		}
	}


	//Col
	int* Col_Means = new int[srcImage->width];

	for (int i = 0; i < srcImage->width; i++)
	{
		means = 0;
		for (int j = 0; j < srcImage->height; j++)
		{
			Tmp = cvGet2D(srcImage, j, i);
			means += Tmp.val[0];

		}
		Col_Means[i] = means / (double)srcImage->height;
	}

	// 튀는 값 찾기
	for (int i = 1; i < srcImage->width - 1; i++)
	{
		Prev_means = Col_Means[i - 1];
		Now_means = Col_Means[i];
		Next_means = Col_Means[i + 1];

		Percent_Prev = (Now_means / Prev_means * 100) - 100;
		Percent_Next = (Next_means / Now_means * 100) - 100;

		if (abs(Percent_Prev) > RC_Th || abs(Percent_Next) > RC_Th)
		{
			if (DPList.DefectPixe_Cnt < MAX_PARTICLE_COUNT)
			{
				DPList.DP_List[DPList.DefectPixe_Cnt].x = i -10;
				DPList.DP_List[DPList.DefectPixe_Cnt].y = 0;
				DPList.DP_List[DPList.DefectPixe_Cnt].W = 20;
				DPList.DP_List[DPList.DefectPixe_Cnt].H = srcImage->height;
				DPList.DP_List[DPList.DefectPixe_Cnt].nType = RowCol;
				if (abs(Percent_Prev) > RC_Th)
					DPList.DP_List[DPList.DefectPixe_Cnt].dbDeviationRate = Percent_Prev;
				else if (abs(Percent_Next) > RC_Th)
					DPList.DP_List[DPList.DefectPixe_Cnt].dbDeviationRate = Percent_Next;
				i += 5;

				DPList.DefectPixe_Cnt++;
			}
			else{
				break;
			}

		}
	}






	delete[]Col_Means;
	delete[]Row_Means;






	cvReleaseImage(&srcImage);

	return TRUE;
}

//=============================================================================
// Method		: GetDistance
// Access		: public  
// Returns		: double
// Parameter	: __in int iSrcX
// Parameter	: __in int iSrcY
// Parameter	: __in int iDstX
// Parameter	: __in int iDstY
// Qualifier	:
// Last Update	: 2018/2/27 - 19:58
// Desc.		:
//=============================================================================
double CTI_DefectPixel::GetDistance(__in int iSrcX, __in int iSrcY, __in int iDstX, __in int iDstY)
{
	return sqrt((double)((iDstX - iSrcX) * (iDstX - iSrcX) + (iDstY - iSrcY) * (iDstY - iSrcY)));

}
