﻿//*****************************************************************************
// Filename	: 	TI_SFR.cpp
// Created	:	2018/2/11 - 17:21
// Modified	:	2018/2/11 - 17:21
//
// Author	:	Luritech
//	
// Purpose	:	
//****************************************************************************
// #include "stdafx.h"
#include "TI_SFR.h"
#include <math.h>


CTI_SFR::CTI_SFR()
{
	
}

CTI_SFR::~CTI_SFR()
{

}

//=============================================================================
// Method		: MulMatrix
// Access		: protected  
// Returns		: void
// Parameter	: double * * A
// Parameter	: double * * B
// Parameter	: double * * C
// Parameter	: unsigned int Row
// Parameter	: unsigned int Col
// Parameter	: unsigned int n
// Parameter	: unsigned int Mode
// Qualifier	:
// Last Update	: 2017/11/3 - 12:31
// Desc.		:
//=============================================================================
void CTI_SFR::MulMatrix(double **A, double **B, double **C, unsigned int Row, unsigned int Col, unsigned int n, unsigned int Mode)
{
	////  A[Row][n] x B[n][Col] = [Row x n] x [n x Col] = [ Row x Col ] = C[Row][Col]   ///////
	////  Mode :  [ Mode 0 : A x B , Mode 1 : A' x B, Mode 2 : A x B' ] 
	unsigned int i, j, k;

	for (i = 0; i < Row; i++){
		for (j = 0; j < Col; j++){
			C[i][j] = 0;
			for (k = 0; k < n; k++){
				if (Mode == 0)	     C[i][j] += A[i][k] * B[k][j];   // Mode 0 : A  x B
				else if (Mode == 1) C[i][j] += A[k][i] * B[k][j];   // Mode 1 : A' x B
				else if (Mode == 2) C[i][j] += A[i][k] * B[j][k];   // Mode 2 : A  x B' 
			}
		}
	}
}

//=============================================================================
// Method		: pInverse
// Access		: protected  
// Returns		: void
// Parameter	: double * * A
// Parameter	: unsigned int Row
// Parameter	: unsigned int Col
// Parameter	: double * * RtnMat
// Qualifier	:
// Last Update	: 2017/11/3 - 12:31
// Desc.		:
//=============================================================================
void CTI_SFR::pInverse(double **A, unsigned int Row, unsigned int Col, double **RtnMat)
{

	unsigned int i;//, j;
	double **AxA, **InvAxA;

	AxA = new double *[Col];
	for (i = 0; i < Col; i++)
		AxA[i] = new double[Col];

	InvAxA = new double *[Col];
	for (i = 0; i < Col; i++)
		InvAxA[i] = new double[Col];

	MulMatrix(A, A, AxA, Col, Col, Row, 1);  // Mode 1 : A' * B

	Inverse(AxA, Col, InvAxA);

	MulMatrix(InvAxA, A, RtnMat, Col, Row, Col, 2);

	for (i = 0; i < Col; i++)
		delete[]InvAxA[i];
	delete[]InvAxA;

	for (i = 0; i < Col; i++)
		delete[]AxA[i];

	delete[] AxA;
}

//=============================================================================
// Method		: Inverse
// Access		: protected  
// Returns		: void
// Parameter	: double * * dataMat
// Parameter	: unsigned int n
// Parameter	: double * * MatRtn
// Qualifier	:
// Last Update	: 2017/11/3 - 12:32
// Desc.		:
//=============================================================================
void CTI_SFR::Inverse(double **dataMat, unsigned int n, double **MatRtn)
{
	unsigned int i, j;
	double *dataA;

	CvMat matA;
	CvMat *pMatB = cvCreateMat(n, n, CV_64F);

	dataA = new double[n*n];

	for (i = 0; i < n; i++)
		for (j = 0; j < n; j++)
			dataA[i*n + j] = dataMat[i][j];

	matA = cvMat(n, n, CV_64F, dataA);

	cvInvert(&matA, pMatB);
	//	PrintMat(&matA, "pMatA = Source");    // Mat print

	for (i = 0; i < n; i++)
		for (j = 0; j < n; j++)
			MatRtn[i][j] = cvGetReal2D(pMatB, i, j);  // A = U * W * VT  ,  MatRtn : VT

	cvReleaseMat(&pMatB);
	delete[]dataA;
}

//=============================================================================
// Method		: GetSFRValue
// Access		: protected  
// Returns		: double
// Parameter	: IN WORD * pBuf
// Parameter	: IN int iWidth
// Parameter	: IN int iHeight
// Parameter	: IN double dbLinePair
// Qualifier	:
// Last Update	: 2018/2/11 - 17:18
// Desc.		:
//=============================================================================
#if 0
double CTI_SFR::GetSFRValue(IN WORD *pBuf, IN int iWidth, IN int iHeight, IN double dbLinePair)
{
	int iImgWidth = iWidth;
	int iImgHeight = iHeight;

	double tleft = 0;
	double tright = 0;

	double fil1[2] = { 0.5, -0.5 };
	double fil2[3] = { 0.5, 0, -0.5 };
	double dbResultSFR;

	//rotatev2가 들어가야 함.

	for (int y = 0; y < iImgHeight; y++)
	{
		for (int x = 0; x < 5; x++)
		{
			tleft += (int)pBuf[y * iImgWidth + x];
		}

		for (int x = iImgWidth - 6; x<iImgWidth; x++)
		{
			tright += (int)pBuf[y * iImgWidth + x];
		}
	}

	if (tleft > tright)
	{
		fil1[0] = -0.5;
		fil1[1] = 0.5;
		fil2[0] = -0.5;
		fil2[1] = 0;
		fil2[2] = 0.5;
	}

	if (tleft + tright == 0.0)
	{
		//	printf("Zero divsion!\n");
		return 0;
	}
	else
	{
		double test = fabs((tleft - tright) / (tleft + tright));

		if (test < 0.2)
		{
			//		printf("Edge contrast is less that 20%\n");
			return 0;
		}
	}

	double n = iImgWidth;
	double mid = (iImgWidth + 1) / 2.0;	// MatLab SFRmat3 에서는 +1을 해줌. #Lucas수정1

	/////////////////////////// ahamming start
	double wid1 = mid - 1;		// // MatLab SFRmat3 에서는 -1을 해줌. #Lucas수정2
	double wid2 = n - mid;
	double wid;
	if (wid1 > wid2)
		wid = wid1;
	else
		wid = wid2;

	double arg = 0;

	double *win1 = new double[iImgWidth];

	for (int i = 0; i < n; i++)
	{
		arg = i - mid + 1;	// MatLab은 1 base임으로, 0 base 시 1을 더해줌. #Lucas수정3
		win1[i] = 0.54 + 0.46*cos((CV_PI*arg / wid));
	}
	/////////////////////////// ahamming end // 여기까지 sfrmat3와 동일 결과.

	//******************************deriv1**********************************************//
	// todo: #Lucas deriv1의 결과 값이 Matlab과 다름. 디버깅이 필요한데 소스가 달라 복잡함.
	int d_n = 2;	//필터열갯수..
	int m = iImgWidth + d_n - 1;
	double sum = 0;
	double *Deriv_c = new double[iImgHeight * m];
	double *c = new double[iImgWidth * iImgHeight];

	for (int k = 0; k < iImgWidth * iImgHeight; k++)
		c[k] = 0.0;

	for (int y = 0; y<iImgHeight; y++)
	{
		for (int k = 0; k<iImgWidth; k++)
		{
			sum = 0;

			for (int d_j = d_n - 1; d_j>-1; d_j--)
			{
				if ((k - d_j) > -1 && (k - d_j) < iImgWidth)
					sum += (double)(pBuf[y * iImgWidth + (k - d_j)]) * fil1[d_j];
			}
			Deriv_c[y * iImgWidth + k] = sum;
		}
	}

	for (int y = 0; y < iImgHeight; y++)
	{
		for (int k = (d_n - 1); k < iImgWidth; k++)
			c[y * iImgWidth + k] = Deriv_c[y * iImgWidth + k];
		c[y * iImgWidth + d_n - 2] = Deriv_c[y * iImgWidth + d_n - 1];
	}

	delete[]Deriv_c;
	Deriv_c = NULL;
	//************************************************************************************//
	double *loc = new double[iImgHeight];
	double loc_v = 0, total_v = 0;

	for (int y = 0; y < iImgHeight; y++)
	{
		loc_v = 0;
		total_v = 0;
		for (int x = 0; x < iImgWidth; x++)
		{
			loc_v += ((double)c[y * iImgWidth + x] * win1[x])*((double)x + 1);	//수정 //MATLAB과 최대한 mid position을 맞춰주자..
			total_v += (double)c[y * iImgWidth + x] * win1[x];
		}

		if (total_v == 0 || total_v < 0.0001)
			loc[y] = 0;
		else
			loc[y] = loc_v / total_v - 0.5;
	}

	delete[]win1;
	//*****************************************************************************//	QR-DECOMP

	//*****************************************************************************//
	int rM = iImgHeight;
	int cN = 2;

	int i, j, rm, cn;
	int count = 0;

	rm = rM;
	cn = cN;

	double **A = new double *[rM];
	double **pinvA = new double *[cN];

	for (i = 0; i < rM; i++)
		A[i] = new double[cN];

	for (i = 0; i < rM; i++)
	{
		A[i][0] = count;
		A[i][1] = 1.0;
		count++;
	}

	for (i = 0; i < cN; i++)
		pinvA[i] = new double[rM];

	pInverse(A, rM, cN, pinvA);

	double **B = new double *[rM];
	for (i = 0; i < rM; i++)
		B[i] = new double[1];

	for (i = 0; i < rM; i++)
		B[i][0] = loc[i];

	double **C = new double *[cN];
	for (i = 0; i < cN; i++)
		C[i] = new double[1];

	MulMatrix(pinvA, B, C, cN, 1, rM, 0);

	//	printf("MulMatrix C[0][0]: %10.4f\n", C[0][0]);
	//	printf("MulMatrix C[1][0]: %10.4f\n", C[1][0]);

	double *place = new double[iImgHeight];
	double *win2 = new double[iImgWidth];

	for (j = 0; j<iImgHeight; j++)
	{
		//**********hamming Window***************************//
		place[j] = C[1][0] + C[0][0] * (double)(j + 1);

		n = iImgWidth;
		mid = place[j];
		wid1 = mid - 1;
		wid2 = n - mid;

		if (wid1 > wid2)
			wid = wid1;
		else
			wid = wid2;

		arg = 0;

		for (i = 0; i < n; i++)
		{
			arg = (i + 1) - mid;
			win2[i] = cos(CV_PI*arg / wid);
		}

		for (i = 0; i < n; i++)
			win2[i] = 0.54 + 0.46*win2[i];

		loc_v = 0;
		total_v = 0;

		for (int x = 0; x < iImgWidth; x++)
			total_v += c[j * iImgWidth + x] * win2[x];

		for (int x = 0; x < iImgWidth; x++)
		{
			loc_v += (c[j * iImgWidth + x] * win2[x])*(x + 1); // 수정
		}

		if (total_v == 0 || total_v < 0.0001)
			loc[j] = 0;
		else
			loc[j] = loc_v / total_v - 0.5;
	}

	delete[]win2;
	delete[]place;
	delete[]c;

	for (i = 0; i < rM; i++)
	{
		delete[]A[i];
		delete[]B[i];
	}
	delete[]A;
	delete[]B;

	for (i = 0; i < cN; i++)
		delete[]pinvA[i];
	delete[]pinvA;

	//====================centeroid fitting==============// Hamming Window를 통한 값으로 라인 fitting을 한번 더 해준다..(여기서 slope 구함)
	rM = iImgHeight;
	cN = 2;

	//	int i, j, rm, cn;
	count = 0;

	rm = rM;
	cn = cN;

	A = new double *[rM];
	pinvA = new double *[cN];

	for (i = 0; i < rM; i++)
		A[i] = new double[cN];

	for (i = 0; i < rM; i++)
	{
		A[i][0] = count;
		A[i][1] = 1.0;
		count++;
	}

	for (i = 0; i < cN; i++)
		pinvA[i] = new double[rM];

	pInverse(A, rM, cN, pinvA);

	B = new double *[rM];
	for (i = 0; i < rM; i++)
		B[i] = new double[1];

	for (i = 0; i < rM; i++)
		B[i][0] = loc[i];

	MulMatrix(pinvA, B, C, cN, 1, rM, 0);

	for (i = 0; i < rM; i++)
	{
		delete[]A[i];
		delete[]B[i];
	}
	delete[]A;
	delete[]B;

	for (i = 0; i < cN; i++)
		delete[]pinvA[i];
	delete[]pinvA;
	//===================================================================================================//

	int nbin = 4;
	double nn = iImgWidth * nbin;
	double nn2 = nn / 2 + 1;
	double *freq = new double[(int)nn];
	double del = 1.0;

	for (i = 0; i < (int)nn; i++)
		freq[i] = (double)nbin*((double)i) / (del*nn);

	double freqlim = 1.0;
	double nn2out = (nn2*freqlim / 2);
	double *win = new double[nbin * iImgWidth];

	//**********hamming Window***************************//	
	n = nbin * iImgWidth;
	mid = (nbin*iImgWidth + 1) / 2.0;
	wid1 = mid - 1;
	wid2 = n - mid;

	if (wid1 > wid2)
		wid = wid1;
	else
		wid = wid2;

	arg = 0;

	for (i = 0; i < n; i++)
	{
		arg = (double)(i + 1) - mid;
		win[i] = cos((CV_PI*arg / wid));
	}

	for (i = 0; i < n; i++)
		win[i] = 0.54 + 0.46*win[i];

	//sfrmat3로 수정===============================================================//

	double fac = 4.0;
	double inProject_nn = iImgWidth * fac;
	double slope = C[0][0];

	double vslope = slope;

	slope = 1.0 / slope;

	double slope_deg = 180.0 * atan(abs(vslope)) / CV_PI;

	if (slope_deg < 1.5)
	{
		delete[]freq;
		delete[]win;
		return 0.0;
		//resultSFR = 0.0;
		//return resultSFR;//너무 급경사를 가진 Edge라서 제대로 측정할 수 없음. //예외 처리 구문을 넣어야 할듯..return 등등
	}

	double del2 = 0;
	double delfac;

	delfac = cos(atan(vslope));
	del = del * delfac;
	del2 = del / nbin;

	//	double offset = 0;
	double offset = fac * (0.0 - (((double)iImgHeight - 1.0) / slope));

	del = abs(cvRound(offset));
	if (offset > 0) offset = 0.0;

	double *dcorr = new double[(int)nn2];
	double temp_m = 3.0;	//length of difference filter
	double scale = 1;

	for (i = 0; i < nn2; i++)
		dcorr[i] = 1.0;

	temp_m = temp_m - 1;

	for (i = 1; i<nn2; i++)
	{
		dcorr[i] = abs((CV_PI*(double)(i + 1)*temp_m / (2.0*(nn2 + 1))) / sin(CV_PI * (double)(i + 1)*temp_m / (2.0*(nn2 + 1))));	//필터를 이용하여 낮은 응답 신호 부분을 증폭시키려는 의도인듯....;;
		dcorr[i] = 1.0 + scale * (dcorr[i] - 1.0);																	//scale로 weight를 조정하는듯..default 1

		if (dcorr[i] > 10)
			dcorr[i] = 10.0;
	}

	//============================================================================//
	//sfrmat3 : PER ISO Algorithm
	iImgHeight = cvRound((double)(cvFloor(iImgHeight * fabs(C[0][0]))) / fabs(C[0][0]));	//수직방향으로 1 Cycle이 이뤄지게하기 위함인듯..

	for (i = 0; i < cN; i++)
		delete[]C[i];

	delete[]C;
	delete[]loc;

	double **barray = new double *[2];
	for (i = 0; i < 2; i++)
		barray[i] = new double[(int)inProject_nn + (int)del + 200];

	for (i = 0; i < 2; i++)
	for (j = 0; j < ((int)inProject_nn + (int)del + 100); j++)
		barray[i][j] = 0;

	for (int x = 0; x < iImgWidth; x++)
	{
		for (int y = 0; y < iImgHeight; y++)
		{
			double ttt = ((double)x - (double)y / slope)*fac;
			int ling = cvCeil(ttt) - (int)offset;
			barray[0][ling]++;
			double kk = barray[1][ling];
			barray[1][ling] = kk + pBuf[y * iImgWidth + x];
		}
	}

	double *point = new double[(int)inProject_nn];
	int start = cvRound(del*0.5);
	double nz = 0;
	int status = 1;

	for (i = start; i < start + (int)inProject_nn - 1; i++)
	{
		if (barray[0][i] == 0)
		{
			nz = nz + 1;
			status = 0;
			if (i == 0)
				barray[0][i] = barray[0][i + 1];
			else
				barray[0][i] = (barray[0][i - 1] + barray[0][i + 1]) / 2.0;
		}
	}

	if (status == 0)
	{
		printf(" Zero count(s) found during projection binning. The edge \n");
		printf(" angle may be large, or you may need more lines of data. \n");
		printf(" Execution will continue, but see Users Guide for info. \n");
	}

	for (i = -1; i < (int)inProject_nn - 1; i++)
	{
		if (barray[0][i + start] != 0){
			if (barray[0][(i + 1) + start] == 0){
				point[(i + 1)] = 0;
			}
			else
				point[(i + 1)] = barray[1][(i + 1) + start] / barray[0][(i + 1) + start];
		}
		else
			point[(i + 1)] = 0;
	}

	for (i = 0; i < 2; i++)
		delete[]barray[i];

	delete[]barray;

	//******************************deriv1**********************************************//
	m = (int)inProject_nn;
	d_n = 3;	//필터열갯수..
	sum = 0;
	double *pointDeriv = new double[(int)inProject_nn];
	Deriv_c = new double[(int)inProject_nn + d_n - 1];

	for (int k = 0; k<(int)inProject_nn; k++)
		pointDeriv[k] = 0.0;

	for (int k = 0; k<m + d_n - 1; k++)
	{
		sum = 0;

		for (int d_j = d_n - 1; d_j>-1; d_j--)
		{
			if ((k - d_j) > -1 && (k - d_j) < m)
				sum += point[k - d_j] * fil2[d_j];
		}

		Deriv_c[k] = sum;
	}

	for (int k = (d_n - 1); k < (int)inProject_nn; k++)		//MATLAB은 배열이 1base이기 때문에 다시 정리해준다..
		pointDeriv[k] = Deriv_c[k];
	pointDeriv[d_n - 2] = Deriv_c[d_n - 1];
	//************************************************************************************//

	delete[]point;
	delete[]Deriv_c;

	//*********************centroid*************************************//
	loc_v = 0, total_v = 0;

	loc_v = 0;
	total_v = 0;
	for (int x = 0; x < (int)inProject_nn; x++)
	{
		loc_v += pointDeriv[x] * x;
		total_v += pointDeriv[x];
	}

	// 마이너스 처리 수정해야 하나?  // : piring
	if (total_v == 0 || total_v < 0.0001)
	{
		delete[]freq;
		delete[]win;
		delete[]pointDeriv;

		return 0.0;
	}
	else
	{
		loc_v = loc_v / total_v;
	}

	//**************************cent****************************//
	double *temp = new double[(int)inProject_nn];
	for (i = 0; i<(int)inProject_nn; i++)
		temp[i] = 0.0;

	mid = cvRound((inProject_nn + 1.0) / 2.0);

	int cent_del = cvRound(cvRound(loc_v) - mid);

	if (cent_del > 0)
	{
		for (i = 0; i < inProject_nn - cent_del; i++)
			temp[i] = pointDeriv[i + cent_del];
	}
	else if (cent_del < 1)
	{
		for (i = -cent_del; i < inProject_nn; i++)
			temp[i] = pointDeriv[i + cent_del];
	}
	else
	{
		for (i = 0; i < inProject_nn; i++)
			temp[i] = pointDeriv[i];
	}

	for (i = 0; i < (int)inProject_nn; i++)
		temp[i] = win[i] * temp[i];

	delete[]win;
	delete[]pointDeriv;

	CvMat *sourceFFT = cvCreateMat((int)inProject_nn * 2, 1, CV_64FC1);
	CvMat *resultFFT = cvCreateMat((int)inProject_nn * 2, 1, CV_64FC1);
	CvMat *ConveredResultFFT = cvCreateMat((int)inProject_nn * 2, 1, CV_64FC1);

	for (i = 0; i < (int)inProject_nn; i++)
	{
		sourceFFT->data.db[i * 2] = temp[i];
		sourceFFT->data.db[i * 2 + 1] = 0;
	}

	delete[]temp;

	cvDFT(sourceFFT, resultFFT, CV_DXT_FORWARD, 0);

	for (i = 0; i < (int)inProject_nn; i++)
	{
		if (i != 0)
		{
			ConveredResultFFT->data.db[i] = sqrt(pow(resultFFT->data.db[i * 2 - 1], 2) + pow(resultFFT->data.db[i * 2], 2));
		}
		else
		{
			ConveredResultFFT->data.db[i] = fabs(resultFFT->data.db[i]);
		}
	}
	double *Resultmtf = new double[(int)inProject_nn];
	int count_j = 0;

	memset(Resultmtf, 0, sizeof(Resultmtf));

	for (int i = 0; i < (int)inProject_nn / 2; i++)
	{
		//Resultmtf[count_j] = fabs(ConveredResultFFT->data.db[i]) / fabs(ConveredResultFFT->data.db[0]); // 수정 : piring -> 0으로 나눗셈이 진행되는 문제가 있음
		Resultmtf[count_j] = (0.0f != ConveredResultFFT->data.db[0]) ? (fabs(ConveredResultFFT->data.db[i]) / fabs(ConveredResultFFT->data.db[0])) : 0;
		Resultmtf[count_j] = Resultmtf[count_j] * dcorr[i];
		count_j++;
	}

	delete[]dcorr;	//correct weight 배열 해제

	cvReleaseMat(&sourceFFT);
	cvReleaseMat(&resultFFT);
	cvReleaseMat(&ConveredResultFFT);

	double nPixel = 1000.0 / m_fSFR_PixelSize;

	// cypx를 결과로 하는 방식
	// 0.5 의 근사치 위치의 값을 양쪽 값을 이용하여 계산하는 방식
	double	dbBefore, dbNext;
	int		nBefore, nNext;

	dbBefore = dbNext = 1.0;
	nBefore = nNext = 0;
	for (int i = 0; i < (int)inProject_nn; i++)
	{
		nNext = i;
		dbNext = Resultmtf[i];
		//		printf("Result[%d] Freq.:MTF--- %f,%f\n", i, freq[i], dbNext);
		if (dbNext < dbLinePair / 100.0)
			break;
		dbBefore = dbNext;
		nBefore = nNext;
	}

	double dbBeforeFreq = freq[nBefore];
	double dbNextFreq = freq[nNext];

	// 그래프가 반비례 형태임으로 아래 형태 계산이 더 근접한 계산
	//dbResultSFR = dbNextFreq - (dbLinePair / 100.0 - dbNext) / (dbBefore - dbNext) * (dbNextFreq - dbBeforeFreq); // 수정 : piring -> 0으로 나눗셈이 진행되는 문제가 있음
	dbResultSFR = (dbBefore != dbNext) ? (dbNextFreq - (dbLinePair / 100.0 - dbNext) / (dbBefore - dbNext) * (dbNextFreq - dbBeforeFreq)) : dbNextFreq;

	//printf("Result mtf range p[%d,%d] - value: [%f,%f] / [%f,%f] --> result SFR: %f \n"
	//	, nBefore, nNext, dbBefore, dbNext, dbBeforeFreq, dbNextFreq, resultSFR);

	if (dbResultSFR > 0.5)	// 왜? 0.5보다 크면 안 됨으로
		dbResultSFR = 0.0;
	//////////////////////// 끝

	// cymm를 결과로 하는 방식
	dbResultSFR = (dbResultSFR / m_fSFR_PixelSize) * 1000.0;

	delete[]freq;
	delete[]Resultmtf;

	return dbResultSFR;
}
#else

double CTI_SFR::GetSFRValue(WORD *pBuf,  int iWidth, int iHeight, double dbPixelSize, double dbLinePair, int imode)
{
	int imgWidth = iWidth;
	int imgHeight = iHeight;

	double tleft = 0;
	double tright = 0;

	double fil1[2] = { 0.5, -0.5 };
	double fil2[3] = { 0.5, 0, -0.5 };
	double dbResultSFR;


	//rotatev2가 들어가야 함.

	for (int y = 0; y < imgHeight; y++)
	{
		for (int x = 0; x < 3; x++)
		{
			tleft += (int)pBuf[y * imgWidth + x];
		}

		for (int x = imgWidth - 3; x < imgWidth; x++)
		{
			tright += (int)pBuf[y * imgWidth + x];
		}
	}

	if (tleft > tright)
	{
		fil1[0] = -0.5;
		fil1[1] = 0.5;
		fil2[0] = -0.5;
		fil2[1] = 0;
		fil2[2] = 0.5;
	}

	if (tleft + tright == 0.0)
	{
		//	printf("Zero divsion!\n");
		return 0;
	}
	else
	{
		double test = fabs((tleft - tright) / (tleft + tright));

		if (test < 0.2)
		{
			//		printf("Edge contrast is less that 20%\n");
			return 0;
		}
	}

	double n = imgWidth;
	double mid = (imgWidth + 1) / 2.0;	// MatLab SFRmat3 에서는 +1을 해줌. #Lucas수정1

	/////////////////////////// ahamming start
	double wid1 = mid - 1;		// // MatLab SFRmat3 에서는 -1을 해줌. #Lucas수정2
	double wid2 = n - mid;
	double wid;
	if (wid1 > wid2)
		wid = wid1;
	else
		wid = wid2;

	double arg = 0;

	double *win1 = new double[imgWidth];

	for (int i = 0; i < n; i++)
	{
		arg = i - mid + 1;	// MatLab은 1 base임으로, 0 base 시 1을 더해줌. #Lucas수정3
		win1[i] = 0.54 + 0.46*cos((CV_PI*arg / wid));
	}
	/////////////////////////// ahamming end // 여기까지 sfrmat3와 동일 결과.

	//******************************deriv1**********************************************//
	// todo: #Lucas deriv1의 결과 값이 Matlab과 다름. 디버깅이 필요한데 소스가 달라 복잡함.
	int d_n = 2;	//필터열갯수..
	int m = imgWidth + d_n - 1;
	double sum = 0;
	double *Deriv_c = new double[imgHeight * m];
	double *c = new double[imgWidth * imgHeight];

	for (int k = 0; k < imgWidth * imgHeight; k++)
		c[k] = 0.0;

	for (int y = 0; y<imgHeight; y++)
	{
		for (int k = 0; k < imgWidth; k++)
		{
			sum = 0;

			for (int d_j = d_n - 1; d_j > -1; d_j--)
			{
				if ((k - d_j) > -1 && (k - d_j) < imgWidth)
					sum += (double)(pBuf[y * imgWidth + (k - d_j)]) * fil1[d_j];
			}
			Deriv_c[y * imgWidth + k] = sum;
		}
	}

	for (int y = 0; y < imgHeight; y++)
	{
		for (int k = (d_n - 1); k < imgWidth; k++)
			c[y * imgWidth + k] = Deriv_c[y * imgWidth + k];
		c[y * imgWidth + d_n - 2] = Deriv_c[y * imgWidth + d_n - 1];
	}

	delete[]Deriv_c;
	Deriv_c = NULL;
	//************************************************************************************//
	double *loc = new double[imgHeight];
	double loc_v = 0, total_v = 0;

	for (int y = 0; y < imgHeight; y++)
	{
		loc_v = 0;
		total_v = 0;
		for (int x = 0; x < imgWidth; x++)
		{
			loc_v += ((double)c[y * imgWidth + x] * win1[x])*((double)x + 1);	//수정 //MATLAB과 최대한 mid position을 맞춰주자..
			total_v += (double)c[y * imgWidth + x] * win1[x];
		}

		if (total_v == 0 || total_v < 0.0001)
			loc[y] = 0;
		else
			loc[y] = loc_v / total_v - 0.5;
	}

	delete[]win1;
	//*****************************************************************************//	QR-DECOMP

	//*****************************************************************************//
	int rM = imgHeight;
	int cN = 2;

	int i, j, rm, cn;
	int count = 0;

	rm = rM;
	cn = cN;

	double **A = new double *[rM];
	double **pinvA = new double *[cN];

	for (i = 0; i < rM; i++)
		A[i] = new double[cN];

	for (i = 0; i < rM; i++)
	{
		A[i][0] = count;
		A[i][1] = 1.0;
		count++;
	}

	for (i = 0; i < cN; i++)
		pinvA[i] = new double[rM];

	pInverse(A, rM, cN, pinvA);

	double **B = new double *[rM];
	for (i = 0; i < rM; i++)
		B[i] = new double[1];

	for (i = 0; i < rM; i++)
		B[i][0] = loc[i];

	double **C = new double *[cN];
	for (i = 0; i < cN; i++)
		C[i] = new double[1];

	MulMatrix(pinvA, B, C, cN, 1, rM, 0);

	//	printf("MulMatrix C[0][0]: %10.4f\n", C[0][0]);
	//	printf("MulMatrix C[1][0]: %10.4f\n", C[1][0]);

	double *place = new double[imgHeight];
	double *win2 = new double[imgWidth];

	for (j = 0; j<imgHeight; j++)
	{
		//**********hamming Window***************************//
		place[j] = C[1][0] + C[0][0] * (double)(j + 1);

		n = imgWidth;
		mid = place[j];
		wid1 = mid - 1;
		wid2 = n - mid;

		if (wid1 > wid2)
			wid = wid1;
		else
			wid = wid2;

		arg = 0;

		for (i = 0; i < n; i++)
		{
			arg = (i + 1) - mid;
			win2[i] = cos(CV_PI*arg / wid);
		}

		for (i = 0; i < n; i++)
			win2[i] = 0.54 + 0.46*win2[i];

		loc_v = 0;
		total_v = 0;

		for (int x = 0; x < imgWidth; x++)
			total_v += c[j * imgWidth + x] * win2[x];

		for (int x = 0; x < imgWidth; x++)
		{
			loc_v += (c[j * imgWidth + x] * win2[x])*(x + 1); // 수정
		}

		if (total_v == 0 || total_v < 0.0001)
			loc[j] = 0;
		else
			loc[j] = loc_v / total_v - 0.5;
	}

	delete[]win2;
	delete[]place;
	delete[]c;

	for (i = 0; i < rM; i++)
	{
		delete[]A[i];
		delete[]B[i];
	}
	delete[]A;
	delete[]B;

	for (i = 0; i < cN; i++)
		delete[]pinvA[i];
	delete[]pinvA;

	//====================centeroid fitting==============// Hamming Window를 통한 값으로 라인 fitting을 한번 더 해준다..(여기서 slope 구함)
	rM = imgHeight;
	cN = 2;

	//	int i, j, rm, cn;
	count = 0;

	rm = rM;
	cn = cN;

	A = new double *[rM];
	pinvA = new double *[cN];

	for (i = 0; i < rM; i++)
		A[i] = new double[cN];

	for (i = 0; i < rM; i++)
	{
		A[i][0] = count;
		A[i][1] = 1.0;
		count++;
	}

	for (i = 0; i < cN; i++)
		pinvA[i] = new double[rM];

	pInverse(A, rM, cN, pinvA);

	B = new double *[rM];
	for (i = 0; i < rM; i++)
		B[i] = new double[1];

	for (i = 0; i < rM; i++)
		B[i][0] = loc[i];

	MulMatrix(pinvA, B, C, cN, 1, rM, 0);

	for (i = 0; i < rM; i++)
	{
		delete[]A[i];
		delete[]B[i];
	}
	delete[]A;
	delete[]B;

	for (i = 0; i < cN; i++)
		delete[]pinvA[i];
	delete[]pinvA;

	//===================================================================================================//
	//여기까진 OK!
	int nbin = 4;
	double nn = imgWidth * nbin;
	double nn2 = nn / 2 + 1;
	double *freq = new double[(int)nn];
	double del = 1.0;

	for (i = 0; i < (int)nn; i++)
		freq[i] = (double)nbin*((double)i) / (del*nn);

	double freqlim = 1.0;
	double nn2out = (nn2*freqlim / 2);
	double *win = new double[nbin * imgWidth];

	//**********hamming Window***************************//	
	n = nbin * imgWidth;
	mid = (nbin*imgWidth + 1) / 2.0;
	wid1 = mid - 1;
	wid2 = n - mid;

	if (wid1 > wid2)
		wid = wid1;
	else
		wid = wid2;

	arg = 0;

	for (i = 0; i < n; i++)
	{
		arg = (double)(i + 1) - mid;
		win[i] = cos((CV_PI*arg / wid));
	}

	for (i = 0; i < n; i++)
		win[i] = 0.54 + 0.46*win[i];

	//sfrmat3로 수정===============================================================//

	double fac = 4.0;
	double inProject_nn = imgWidth * fac;
	double slope = C[0][0];

	double vslope = slope;

	slope = 1.0 / slope;

	double slope_deg = 180.0 * atan(abs(vslope)) / CV_PI;

	if (slope_deg < 3.5)
	{
		delete[]freq;
		delete[]win;

		for (int i = 0; i < cN; i++)
			delete[] C[i];
		delete[]C;

		delete[]loc;
		
		return (100.0 + slope_deg);
		//resultSFR = 0.0;
		//return resultSFR;//너무 급경사를 가진 Edge라서 제대로 측정할 수 없음. //예외 처리 구문을 넣어야 할듯..return 등등
	}

	double del2 = 0;
	double delfac;

	delfac = cos(atan(vslope));
	del = del * delfac;
	del2 = del / nbin;

	//===========================================================================2018-02-20 주석처리하고!!
	/*	//	double offset = 0;
	double offset = fac * (0.0 - (((double)imgHeight - 1.0) / slope));

	del = abs(cvRound(offset));
	if (offset > 0) offset = 0.0;*/
	//============================================================================

	double *dcorr = new double[(int)nn2];
	double temp_m = 3.0;	//length of difference filter
	double scale = 1;

	for (i = 0; i < nn2; i++)
		dcorr[i] = 1.0;

	temp_m = temp_m - 1;

	for (i = 1; i<nn2; i++)
	{
		dcorr[i] = abs((CV_PI*(double)(i + 1)*temp_m / (2.0*(nn2 + 1))) / sin(CV_PI * (double)(i + 1)*temp_m / (2.0*(nn2 + 1))));	//필터를 이용하여 낮은 응답 신호 부분을 증폭시키려는 의도인듯....;;
		dcorr[i] = 1.0 + scale * (dcorr[i] - 1.0);																	//scale로 weight를 조정하는듯..default 1

		if (dcorr[i] > 10)
			dcorr[i] = 10.0;
	}

	//============================================================================//
	//sfrmat3 : PER ISO Algorithm
	imgHeight = cvRound((double)(cvFloor((double)imgHeight * fabs(C[0][0]))) / fabs(C[0][0]));	//수직방향으로 1 Cycle이 이뤄지게하기 위함인듯..

	for (i = 0; i < cN; i++)
		delete[]C[i];

	delete[]C;
	delete[]loc;

	//==========================================================================2018-02-20 여기로!!!
	//	double offset = 0;
	double offset = fac * (0.0 - (((double)imgHeight) / slope));

	del = abs(cvRound(offset));
	if (offset > 0) offset = 0.0;
	//============================================================================

	double **barray = new double *[2];
	for (i = 0; i < 2; i++)
		barray[i] = new double[(int)inProject_nn + (int)del + 100];

	for (i = 0; i < 2; i++)
	for (j = 0; j < ((int)inProject_nn + (int)del + 100); j++)
		barray[i][j] = 0;

	int tmp_x, tmp_y;

	for (tmp_x = 0; tmp_x < imgWidth; tmp_x++)
	{
		for (tmp_y = 0; tmp_y < imgHeight; tmp_y++)
		{
			double ttt = ((double)(tmp_x)-((double)tmp_y / slope))*fac;
			double ling = cvCeil(ttt) - (double)offset;
			barray[0][(int)ling] = barray[0][(int)ling] + 1;
			barray[1][(int)ling] = barray[1][(int)ling] + (double)pBuf[tmp_y * imgWidth + tmp_x];
		}
	}

	double *point = new double[(int)inProject_nn];
	int start = cvRound(del*0.5);
	double nz = 0;
	int status = 1;

	for (i = start; i < start + (int)inProject_nn - 1; i++)
	{
		if (barray[0][i] == 0)
		{
			nz = nz + 1;
			status = 0;
			if (i == 0)
				barray[0][i] = barray[0][i + 1];
			else
				barray[0][i] = (barray[0][i - 1] + barray[0][i + 1]) / 2.0;
		}
	}

	if (status != 0)
	{
		printf(" Zero count(s) found during projection binning. The edge \n");
		printf(" angle may be large, or you may need more lines of data. \n");
		printf(" Execution will continue, but see Users Guide for info. \n");
	}

	for (i = -1; i < (int)inProject_nn - 1; i++)
	{
		if (barray[0][i + start] != 0)
		{
			if (barray[0][(i + 1) + start] == 0)
			{
				point[(i + 1)] = 0;
			}
			else
				point[(i + 1)] = barray[1][(i + 1) + start] / barray[0][(i + 1) + start];
		}
		else
			point[(i + 1)] = 0;
	}

	for (i = 0; i < 2; i++)
		delete[]barray[i];
	delete[]barray;

	//******************************deriv1**********************************************//
	m = (int)inProject_nn;
	d_n = 3;	//필터열갯수..
	sum = 0;
	double *pointDeriv = new double[(int)inProject_nn];
	Deriv_c = new double[(int)inProject_nn + d_n - 1];

	for (int k = 0; k<(int)inProject_nn; k++)
		pointDeriv[k] = 0.0;

	for (int k = 0; k<m + d_n - 1; k++)
	{
		sum = 0;

		for (int d_j = d_n - 1; d_j > -1; d_j--)
		{
			if ((k - d_j) > -1 && (k - d_j) < m)
				sum += point[k - d_j] * fil2[d_j];
		}

		Deriv_c[k] = sum;
	}

	for (int k = (d_n - 1); k < (int)inProject_nn; k++)		//MATLAB은 배열이 1base이기 때문에 다시 정리해준다..
		pointDeriv[k] = Deriv_c[k];
	pointDeriv[d_n - 2] = Deriv_c[d_n - 1];
	//************************************************************************************//

	delete[]point;
	delete[]Deriv_c;

	//*********************centroid*************************************//
	loc_v = 0, total_v = 0;

	loc_v = 0;
	total_v = 0;
	for (int x = 0; x < (int)inProject_nn; x++)
	{
		loc_v += pointDeriv[x] * x;
		total_v += pointDeriv[x];
	}

	if (total_v == 0 || total_v < 0.0001)
	{
		delete[]freq;
		delete[]win;
		delete[]pointDeriv;
		return 0.0;
	}
	else
	{
		loc_v = loc_v / total_v;
	}

	//**************************cent****************************//
	double *temp = new double[(int)inProject_nn];
	for (i = 0; i<(int)inProject_nn; i++)
		temp[i] = 0.0;

	mid = cvRound((inProject_nn + 1.0) / 2.0);

	int cent_del = cvRound(cvRound(loc_v) - mid);

	if (cent_del > 0)
	{
		for (i = 0; i < inProject_nn - cent_del; i++)
			temp[i] = pointDeriv[i + cent_del];
	}
	else if (cent_del < 1)
	{
		for (i = -cent_del; i < inProject_nn; i++)
			temp[i] = pointDeriv[i + cent_del];
	}
	else
	{
		for (i = 0; i < inProject_nn; i++)
			temp[i] = pointDeriv[i];
	}

	for (i = 0; i < (int)inProject_nn; i++)
		temp[i] = win[i] * temp[i];

	delete[]win;
	delete[]pointDeriv;

	CvMat *sourceFFT = cvCreateMat((int)inProject_nn * 2, 1, CV_64FC1);
	CvMat *resultFFT = cvCreateMat((int)inProject_nn * 2, 1, CV_64FC1);
	CvMat *ConveredResultFFT = cvCreateMat((int)inProject_nn * 2, 1, CV_64FC1);

	for (i = 0; i < (int)inProject_nn; i++)
	{
		sourceFFT->data.db[i * 2] = temp[i];
		sourceFFT->data.db[i * 2 + 1] = 0;
	}

	delete[]temp;

	cvDFT(sourceFFT, resultFFT, CV_DXT_FORWARD, 0);

	for (i = 0; i < (int)inProject_nn; i++)
	{
		if (i != 0)
		{
			ConveredResultFFT->data.db[i] = sqrt(pow(resultFFT->data.db[i * 2 - 1], 2) + pow(resultFFT->data.db[i * 2], 2));
		}
		else
		{
			ConveredResultFFT->data.db[i] = fabs(resultFFT->data.db[i]);
		}
	}
	double *Resultmtf = new double[(int)inProject_nn];
	int count_j = 0;

	memset(Resultmtf, 0, sizeof(Resultmtf));

	for (int i = 0; i < (int)inProject_nn / 2; i++)
	{
		//Resultmtf[count_j] = fabs(ConveredResultFFT->data.db[i]) / fabs(ConveredResultFFT->data.db[0]); //-> 0으로 나눗셈이 진행되는 문제가 있음
		Resultmtf[count_j] = (0.0f != ConveredResultFFT->data.db[0]) ? (fabs(ConveredResultFFT->data.db[i]) / fabs(ConveredResultFFT->data.db[0])) : 0;
		Resultmtf[count_j] = Resultmtf[count_j] * dcorr[i];
		count_j++;
	}

	delete[]dcorr;	//correct weight 배열 해제

	cvReleaseMat(&sourceFFT);
	cvReleaseMat(&resultFFT);
	cvReleaseMat(&ConveredResultFFT);


	double nPixel = 1000.0 / dbPixelSize;
	double CyclePerPixel = dbLinePair / nPixel;

	enum SFRMODE
	{
		MTFResult,
		CyclePerPixelResult,
		CyclePerMMResult
	};

	//int SFRResultMODE = MTFResult; 기존
	int SFRResultMODE = imode;


	// MTF를 결과로 하는 방식
	if (SFRResultMODE == MTFResult)
	{
		int half_sampling_index = -1;
		double data, old_data = 1.0;

		for (int q = 0; q<inProject_nn / 2; q++)
		{
			data = fabs(freq[q] - CyclePerPixel);

			if (old_data > data)
			{
				old_data = data;
				half_sampling_index = q;
			}
		}

		if (Resultmtf[half_sampling_index] < 0 || half_sampling_index == -1)
			dbResultSFR = 0.0;
		else
			dbResultSFR = Resultmtf[half_sampling_index];
	}
	else if (SFRResultMODE == CyclePerPixelResult || SFRResultMODE == CyclePerMMResult)
	{
		// cypx를 결과로 하는 방식
		// 0.5 의 근사치 위치의 값을 양쪽 값을 이용하여 계산하는 방식
		double	dbBefore, dbNext;
		int		nBefore, nNext;

		dbBefore = dbNext = 1.0;
		nBefore = nNext = 0;
		for (int i = 0; i < (int)inProject_nn; i++)
		{
			nNext = i;
			dbNext = Resultmtf[i];
			TRACE(_T("Result[%d] Freq.:MTF--- %f,%f\n"), i, freq[i], dbNext);
			if (dbNext < CyclePerPixel)
				break;
			dbBefore = dbNext;
			nBefore = nNext;
		}

		double dbBeforeFreq = freq[nBefore];
		double dbNextFreq = freq[nNext];

		// 그래프가 반비례 형태임으로 아래 형태 계산이 더 근접한 계산
		dbResultSFR = dbNextFreq - (CyclePerPixel - dbNext) / (dbBefore - dbNext) * (dbNextFreq - dbBeforeFreq);

		if (SFRResultMODE == CyclePerMMResult)
		{
			dbResultSFR = (dbResultSFR / dbPixelSize) * 1000;
		}
	}

	delete[]freq;
	delete[]Resultmtf;

	return dbResultSFR;
}
#endif

//=============================================================================
// Method		: Get_ROI_SFR_16bit
// Access		: protected  
// Returns		: double
// Parameter	: IN LPWORD pImageBuf
// Parameter	: IN UINT dwWidth
// Parameter	: IN UINT dwHeight
// Parameter	: IN ST_ROI stROI
// Parameter	: IN double dbLinPair
// Qualifier	:
// Last Update	: 2018/2/11 - 17:20
// Desc.		:
//=============================================================================
double CTI_SFR::Get_ROI_SFR_16bit(IN LPWORD pImageBuf, IN UINT dwWidth, IN UINT dwHeight, IN ST_ROI stROI, double dbPixelSize, IN double dbLinPair)//사용 안함
{
	//?
	/*if (stROI.sWidth > stROI.sHeight)
	{
		for (int X = stROI.sLeft, CntX = 0; X < stROI.sLeft + stROI.sWidth; X++, CntX++)
		{
			for (int Y = stROI.sTop, CntY = 0; Y < stROI.sTop + stROI.sHeight; Y++, CntY++)
			{
				pImageBuf[(CntX * stROI.sHeight) + CntY] = pImageBuf[Y * dwWidth + X];
			}
		}

		return GetSFRValue(pImageBuf, stROI.sHeight, stROI.sWidth, dbPixelSize, dbLinPair);
	}
	else
	{
		for (int Y = stROI.sTop, CntY = 0; Y < stROI.sTop + stROI.sHeight; Y++, CntY++)
		{
			for (int X = stROI.sLeft, CntX = 0; X < stROI.sLeft + stROI.sWidth; X++, CntX++)
			{
				pImageBuf[(CntY * stROI.sWidth) + CntX] = pImageBuf[Y * dwWidth + X];
			}
		}

		return GetSFRValue(pImageBuf, stROI.sWidth, stROI.sHeight, dbPixelSize, dbLinPair);
	}*/
	return 0;
}

//=============================================================================
// Method		: SFR_Test
// Access		: public  
// Returns		: int
// Parameter	: IN LPWORD pImageBuf
// Parameter	: IN UINT dwWidth
// Parameter	: IN UINT dwHeight
// Parameter	: IN ST_ROI stROI
// Parameter	: IN double dbLinPair
// Parameter	: OUT double & dbResultSFR
// Qualifier	:
// Last Update	: 2018/2/11 - 17:13
// Desc.		:
//=============================================================================사용안함
int CTI_SFR::SFR_Test(IN LPWORD pImageBuf, IN UINT dwWidth, IN UINT dwHeight, IN ST_ROI stROI, IN double dbLinPair, double dbPixelSize, OUT double& dbResultSFR)
{
	if (nullptr == pImageBuf)
		return -1;

	double dbValue = 0.0;

	//// 1차
	//dbValue = Get_ROI_SFR_16bit(pImageBuf, dwWidth, dwHeight, stROI, dbPixelSize,dbLinPair);

	//// 2차 <- 1픽셀 이동
	//if (0.0 == dbValue)
	//{
	//	if (1 <= stROI.sLeft)	// <- 1픽셀 이동 가능하면..
	//	{
	//		stROI.sLeft = stROI.sLeft - 1;

	//		dbValue = Get_ROI_SFR_16bit(pImageBuf, dwWidth, dwHeight, stROI, dbPixelSize, dbLinPair);

	//		stROI.sLeft = stROI.sLeft + 1;
	//	}

	//	// 3차 -> 1픽셀 이동
	//	if (0.0 == dbValue)
	//	{
	//		if ((stROI.sLeft + stROI.sWidth) < dwWidth)	// -> 1픽셀 이동 가능하면..
	//		{
	//			stROI.sLeft = stROI.sLeft + 1;

	//			dbValue = Get_ROI_SFR_16bit(pImageBuf, dwWidth, dwHeight, stROI, dbPixelSize, dbLinPair);

	//			stROI.sLeft = stROI.sLeft - 1;
	//		}

	//		// 4차 up 1픽셀 이동
	//		if (0.0 == dbValue)
	//		{
	//			if (1 <= stROI.sTop)	// up 1픽셀 이동 가능하면..
	//			{
	//				stROI.sTop = stROI.sTop - 1;

	//				dbValue = Get_ROI_SFR_16bit(pImageBuf, dwWidth, dwHeight, stROI, dbPixelSize, dbLinPair);

	//				stROI.sTop = stROI.sTop + 1;
	//			}

	//			// 5차 down 1픽셀 이동
	//			if (0.0 == dbValue)
	//			{
	//				if ((stROI.sTop + stROI.sHeight) < dwHeight)	// down 1픽셀 이동 가능하면..
	//				{
	//					stROI.sTop = stROI.sTop + 1;

	//					dbValue = Get_ROI_SFR_16bit(pImageBuf, dwWidth, dwHeight, stROI, dbPixelSize, dbLinPair);

	//					stROI.sTop = stROI.sTop - 1;
	//				}
	//			}
	//		}
	//	}
	//}

	//dbResultSFR = dbValue;

	return 1;
}

//=============================================================================
// Method		: SFR_Test
// Access		: public  
// Returns		: int
// Parameter	: IN LPWORD pImageBuf
// Parameter	: int nImgW
// Parameter	: int nImgH
// Parameter	: CRect ROI
// Parameter	: IN double dbLinPair
// Parameter	: double dbPixelSize
// Parameter	: OUT double & dbResultSFR
// Parameter	: int imode
// Qualifier	:
// Last Update	: 2018/10/19 - 13:21
// Desc.		:
//=============================================================================
int CTI_SFR::SFR_Test(IN LPWORD pImageBuf, int nImgW, int nImgH, CRect ROI, IN double dbLinPair, double dbPixelSize, OUT double& dbResultSFR, int imode)
{
 	if (nullptr == pImageBuf)
 		return -1;
 
 	double dbValue = 0.0;
  
	dbValue = Get_ROI_SFR_16bit(pImageBuf, nImgW, nImgH, ROI, dbPixelSize, dbLinPair, imode);
  
 	dbResultSFR = dbValue;

	return 1;
}

//=============================================================================
// Method		: Get_ROI_SFR_16bit
// Access		: protected  
// Returns		: double
// Parameter	: IN LPWORD pImageBuf
// Parameter	: int nImgW
// Parameter	: int nImgH
// Parameter	: IN CRect ROI
// Parameter	: double dbPixelSize
// Parameter	: IN double dbLinPair
// Parameter	: int imode
// Qualifier	:
// Last Update	: 2018/10/19 - 13:21
// Desc.		:
//=============================================================================
double CTI_SFR::Get_ROI_SFR_16bit(IN LPWORD pImageBuf, int nImgW, int nImgH, IN CRect ROI,  double dbPixelSize, IN double dbLinPair, int imode)
{
	LPWORD pTemp;
	pTemp = new WORD[(ROI.Width())* (ROI.Height())];
	double dSFRValue = 0;
	if (ROI.Width() > ROI.Height()) 
	{
		for (int X = ROI.left, CntX = 0; X <ROI.right; X++, CntX++)
		{
			for (int Y = ROI.top, CntY = 0; Y < ROI.bottom; Y++, CntY++)
			{
				pTemp[(CntX * ROI.Height()) + CntY] = pImageBuf[Y * nImgW + X];
			}
		}	
		
		dSFRValue = GetSFRValue(pTemp, ROI.Height(), ROI.Width(), dbPixelSize, dbLinPair, imode);

	}
	else
	{
		for (int Y = ROI.top, CntY = 0; Y < ROI.bottom; Y++, CntY++)
		{
			for (int X = ROI.left, CntX = 0; X <ROI.right; X++, CntX++)
			{
				pTemp[(CntY * ROI.Width()) + CntX] = pImageBuf[Y * nImgW + X];
			}
		}
		dSFRValue = GetSFRValue(pTemp, ROI.Width(), ROI.Height(), dbPixelSize, dbLinPair, imode);
	}

	delete []pTemp;
	return dSFRValue;
}