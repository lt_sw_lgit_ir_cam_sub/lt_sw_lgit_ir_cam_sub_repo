﻿//*****************************************************************************
// Filename	: 	TI_FPN.h
// Created	:	2018/2/10 - 17:10
// Modified	:	2018/2/10 - 17:10
//
// Author	:	Luritech
//	
// Purpose	:	
//****************************************************************************
#ifndef TI_FPN_h__
#define TI_FPN_h__

#pragma once

#include <windows.h>
#include "Def_Algorithm_Common.h"

//=============================================================================
// CTI_FPN
//=============================================================================
class CTI_FPN
{
public:
	CTI_FPN();
	virtual ~CTI_FPN();

protected:
	

public:

	//=============================================================================
	// Method		: FPN_Test
	// Access		: public  
	// Returns		: int
	// Parameter	: IN unsigned short * pImageBuf
	// Parameter	: IN unsigned int dwWidth
	// Parameter	: IN unsigned int dwHeight
	// Parameter	: IN float fThresholdX
	// Parameter	: IN float fThresholdY
	// Parameter	: OUT unsigned short & sFailCountX
	// Parameter	: OUT unsigned short & sFailCountY
	// Qualifier	:
	// Last Update	: 2018/2/10 - 17:09
	// Desc.		:
	//=============================================================================
	void FPN_Test(IN unsigned short* pImageBuf, IN unsigned int dwWidth, IN unsigned int dwHeight, IN CRect rtRoI, IN float fThresholdX, IN float fThresholdY, OUT int& sFailCountX, OUT  int& sFailCountY);

};

#endif // TI_FPN_h__
