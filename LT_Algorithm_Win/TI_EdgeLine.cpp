﻿//*****************************************************************************
// Filename	: 	TI_EdgeLine.cpp
// Created	:	2018/2/11 - 15:36
// Modified	:	2018/2/11 - 15:36
//
// Author	:	Luritech
//	
// Purpose	:	
//****************************************************************************
//#include "stdafx.h"
#include "TI_EdgeLine.h"
#include <math.h>

CTI_EdgeLine::CTI_EdgeLine()
{
	
}

CTI_EdgeLine::~CTI_EdgeLine()
{

}

//=============================================================================
// Method		: EdgeLine_Test
// Access		: public  
// Returns		: int
// Parameter	: IN LPWORD pImageBuf
// Parameter	: IN UINT dwWidth
// Parameter	: IN UINT dwHeight
// Parameter	: IN ST_ROI stROI
// Parameter	: IN int iCenterX
// Parameter	: IN int iCenterY
// Parameter	: IN BYTE byDetectColor
// Parameter	: IN BYTE byBrightness
// Parameter	: IN BYTE byThreshold
// Parameter	: OUT POINT & ptOUT_Center
// Qualifier	:
// Last Update	: 2018/2/11 - 15:34
// Desc.		:
//=============================================================================
int CTI_EdgeLine::EdgeLine_Test(IN LPWORD pImageBuf, IN UINT dwWidth, IN UINT dwHeight, IN ST_ROI stROI, IN int iCenterX, IN int iCenterY, IN BYTE byDetectColor, IN BYTE byBrightness, IN BYTE byThreshold, OUT POINT& ptOUT_Center)
{
	if (nullptr == pImageBuf)
		return -1;

	POINT ptCenter;

	// 초기값
	ptCenter.x = 0;
	ptCenter.y = 0;

	IplImage *OriginImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 1);
	IplImage *ThresholdImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 1);

	WORD wData16Bit = 0;
	BYTE byData8Bit = 0;

	cvSetZero(OriginImage);

	for (UINT y = 0; y < dwHeight; y++)
	{
		for (UINT x = 0; x < dwWidth; x++)
		{
			wData16Bit = *(pImageBuf + y * dwWidth + x);

			wData16Bit = wData16Bit & 0xF000 ? 0x0FFF : wData16Bit;
			byData8Bit = (wData16Bit >> 4) & 0x00FF;

			if (byBrightness >= 0)
				byData8Bit = (byData8Bit << byBrightness) >= 0xFF ? 0xFF : (byData8Bit << byBrightness);
			else
				byData8Bit = (byData8Bit >> (-1 * byBrightness)) <= 0x00 ? 0x00 : (byData8Bit >> (-1 * byBrightness));


			OriginImage->imageData[y * OriginImage->widthStep + x] = byData8Bit;
		}
	}

	cvThreshold(OriginImage, ThresholdImage, byThreshold, 255, CV_THRESH_BINARY);

	//변화가 있는 영역
	std::vector<int> vChange_Pos;
	vChange_Pos.clear();
	int iTempData = -1;
	int iImageData = 0;
	int iOri_Data = 0;

	switch (byDetectColor)
	{
	case Line_LR_WB:
	{
		for (int x = stROI.sLeft; x < stROI.sLeft + stROI.sWidth; x++)
		{
			iOri_Data = ThresholdImage->imageData[iCenterY*ThresholdImage->widthStep + x];
			iImageData = abs(iOri_Data);
			if (iImageData != iTempData)
			{
				vChange_Pos.push_back(x);

				iTempData = iImageData;
			}
		}

		//Array에 담은 것을 역으로 Searching
		int nChangeCnt = (int)vChange_Pos.size();

		iTempData = 1;

		if (nChangeCnt > 0)
		{
			int nData_X = vChange_Pos.at(nChangeCnt - 1);

			for (int t = nData_X - 2; t < nData_X + 2; t++)
			{
				if (t < (int)dwWidth)
				{
					iOri_Data = ThresholdImage->imageData[iCenterY*ThresholdImage->widthStep + t];
					iImageData = abs(iOri_Data);
					if (t == nData_X - 2 && iImageData == 0)// 첫번째 데이터가 검은색이면 Searching Fail
					{
						break;
					}

					if (iImageData != iTempData)
					{
						ptCenter.x = t;
						ptCenter.y = iCenterY;
						break;
					}
				}
			}
		}
	}
		break;

	case Line_LR_BW:
	{
		for (int x = stROI.sLeft + stROI.sWidth - 1; x > stROI.sLeft; x--)
		{
			iOri_Data = ThresholdImage->imageData[iCenterY * ThresholdImage->widthStep + x];
			iImageData = abs(iOri_Data);
			if (iImageData != iTempData)
			{
				vChange_Pos.push_back(x);

				iTempData = iImageData;
			}
		}

		//Array에 담은 것을 역으로 Searching
		int nChangeCnt = (int)vChange_Pos.size();

		iTempData = 1;

		if (nChangeCnt > 0)
		{
			int nData_X = vChange_Pos.at(nChangeCnt - 1);

			for (int t = nData_X + 2; t > nData_X - 2; t--)
			{
				if (t >= 0)
				{
					iOri_Data = ThresholdImage->imageData[iCenterY*ThresholdImage->widthStep + t];
					iImageData = abs(iOri_Data);
					if ((t == nData_X + 2 && iImageData == 0))// 첫번째 데이터가 검은색이면 Searching Fail
					{
						break;
					}

					if (iImageData != iTempData)
					{
						ptCenter.x = t;
						ptCenter.y = iCenterY;
						break;
					}
				}
			}
		}
	}
		break;

	case Line_TB_BW:
	{
		for (int y = stROI.sTop; y < stROI.sTop + stROI.sHeight; y++)
		{
			iOri_Data = ThresholdImage->imageData[y*ThresholdImage->widthStep + iCenterX];
			iImageData = abs(iOri_Data);
			if (iImageData != iTempData)
			{
				vChange_Pos.push_back(y);

				iTempData = iImageData;
			}
		}

		//Array에 담은 것을 역으로 Searching
		int nChangeCnt = (int)vChange_Pos.size();

		iTempData = 0;

		if (nChangeCnt > 0)
		{
			int nData_Y = vChange_Pos.at(nChangeCnt - 1);

			for (int t = nData_Y - 2; t < nData_Y + 2; t++)
			{
				if (t < dwHeight)
				{
					iOri_Data = ThresholdImage->imageData[t*ThresholdImage->widthStep + iCenterX];
					iImageData = abs(iOri_Data);
					if ((t == nData_Y - 2 && iImageData == 1))// 첫번째 데이터가 흰색이 경우에만.
					{
						break;
					}

					if (iImageData != iTempData)
					{
						ptCenter.x = iCenterX;
						ptCenter.y = t;
						break;
					}
				}
			}
		}
	}
		break;

	case Line_TB_WB:
	{
		for (int y = stROI.sTop + stROI.sWidth - 1; y > stROI.sTop; y--)
		{
			iOri_Data = ThresholdImage->imageData[y*ThresholdImage->widthStep + iCenterX];
			iImageData = abs(iOri_Data);
			if (iImageData != iTempData)
			{
				vChange_Pos.push_back(y);

				iTempData = iImageData;
			}
		}

		//Array에 담은 것을 역으로 Searching
		int nChangeCnt = (int)vChange_Pos.size();

		iTempData = 0;

		if (nChangeCnt > 0)
		{
			int nData_Y = vChange_Pos.at(nChangeCnt - 1);

			for (int t = nData_Y + 2; t > nData_Y - 2; t--)
			{
				if (t >= 0)
				{
					iOri_Data = ThresholdImage->imageData[t*ThresholdImage->widthStep + iCenterX];
					iImageData = abs(iOri_Data);
					if ((t == nData_Y + 2 && iImageData == 1))// 첫번째 데이터가 흰색이 경우에만.
					{
						break;
					}

					if (iImageData != iTempData)
					{
						ptCenter.x = iCenterX;
						ptCenter.y = t;
						break;
					}
				}
			}
		}
	}
		break;
	}

	cvReleaseImage(&ThresholdImage);
	cvReleaseImage(&OriginImage);

	ptOUT_Center = ptCenter;
	
	return 1;
}
