﻿// *****************************************************************************
//  Filename	: 	TI_Particle.h
//  Created	:	2018/2/11 - 15:47
//  Modified	:	2018/2/11 - 15:47
// 
//  Author	:	Luritech
// 	
//  Purpose	:	
// ****************************************************************************
#ifndef TI_Particle_Entry_h__
#define TI_Particle_Entry_h__

#pragma once

#include <windows.h>
#include "cv.h"
#include "highgui.h"
#include "Def_Algorithm_Common.h"

// -----------------------------------------------------------------------------
//  Particle
// -----------------------------------------------------------------------------

#define		MAX_Particle_Entry_ResultROI_Count		50	// Max는 60개 고정



typedef enum enParticle_Entry_ROI
{
	Particle_Entry_ROI_Image = 0,
	Particle_Entry_ROI_Side,
	Particle_Entry_ROI_Center,
	Particle_Entry_ROI_Max,
};

typedef enum enParticle_Entry__Al_Type
{
	Pr_Type_Entry_Stain = 0,
	Pr_Type_Entry_Blemish,
	Pr_Type_Entry_Max,
};

typedef enum enPaticle_EntryRegion
{
	ROI_Pr_Entry_Side = 0,
	ROI_Pr_Entry_Middle,
	ROI_Pr_Entry_Center,
	ROI_Pr_Entry_Max,
};

typedef struct _tag_CAN_Particle_Entry_Opt
{
	ST_ROI			stROI[ROI_Pr_Entry_Max];
	bool			bEllipse[ROI_Pr_Entry_Max];				// 영역 모양(사각 or 원)

	float			fThreConcentration[ROI_Pr_Entry_Max];
	float			fThreSize[ROI_Pr_Entry_Max];

	float			fSensitivity;								// 민감도

	_tag_CAN_Particle_Entry_Opt()
	{
		fSensitivity = 0.0f;

		for (int i = 0; i < ROI_Pr_Entry_Max; i++)
		{
			bEllipse[i] = true;
			fThreConcentration[i] = 0.0f;
			fThreSize[i] = 0.0f;
		}
	};

}ST_CAN_Particle_Entry_Opt;

typedef struct _tag_CAN_Particle_Entry_Result
{
	WORD			wResultCode;

	BYTE			byRoiCnt;
	ST_ROI			stROI[MAX_Particle_Entry_ResultROI_Count];			// 발견한 오브젝트
	BYTE			byType[MAX_Particle_Entry_ResultROI_Count];			// 발견한 오브젝트의 타입(Stain, Blemish)
	float			fConcentration[MAX_Particle_Entry_ResultROI_Count];	// 농도

	_tag_CAN_Particle_Entry_Result()
	{
		byRoiCnt = 0;

		for (UINT nIdx = 0; nIdx < MAX_Particle_Entry_ResultROI_Count; nIdx++)
		{
			byType[nIdx] = Pr_Type_Entry_Max;
			fConcentration[nIdx] = 0.0f;
		}
	};

}ST_CAN_Particle_Entry_Result;

// =============================================================================
//  CTI_Particle_Entry
// =============================================================================
class CTI_Particle_Entry
{
public:
	CTI_Particle_Entry();
	virtual ~CTI_Particle_Entry();

protected:

	UINT	m_nMax_ParticleCount	= 20;	//#define MAX_PARTICLE_COUNT	20


	//int			m_iCounter;
	//CvRect*		m_pContour;
	//char**		m_Area;	//char	m_Area[CAM_IMAGE_WIDTH][CAM_IMAGE_HEIGHT];

	void	AssignMem_Area		(IN DWORD dwSizeX, IN DWORD dwSizeY);
	void	ReleaseMem_Area		();


	double	GetDistance			(int x1, int y1, int x2, int y2);
	double	EllipseDistanceSum	(int XC, int YC, double A, double B, int X1, int Y1);

public:

	//=============================================================================
	// Method		: SetTestArea
	// Access		: public  
	// Returns		: void
	// Parameter	: IN UINT dwWidth
	// Parameter	: IN UINT dwHeight
	// Parameter	: IN ST_ROI stROI_Side
	// Parameter	: IN ST_ROI stROI_Middle
	// Parameter	: IN ST_ROI stROI_Center
	// Parameter	: IN bool bEllipse_Side
	// Parameter	: IN bool bEllipse_Middle
	// Parameter	: IN bool bEllipse_Center
	// Parameter	: OUT char * * ppchOUT_Area
	// Qualifier	:
	// Last Update	: 2018/2/12 - 11:44
	// Desc.		:
	//=============================================================================
	void	SetTestArea		(IN UINT dwWidth, IN UINT dwHeight, IN ST_ROI stROI_Side, IN ST_ROI stROI_Middle, IN ST_ROI stROI_Center, IN bool bEllipse_Side, IN bool bEllipse_Middle, IN bool bEllipse_Center, OUT char** ppchOUT_Area);
	//void	SetTestArea		(IN UINT dwWidth, IN UINT dwHeight, ST_CAN_Particle_Opt& ParticleOpt);
	
	//void	Lump_Detection(IN LPWORD pImageBuf, IN UINT dwWidth, IN UINT dwHeight, IN float fSensitivity, IN float* fThreSize, OUT int& iOUT_Counter, OUT CvRect* prtOUT_Contour, OUT char** ppchOUT_Area);
	void	Lump_Detection(IN LPWORD pImageBuf, IN UINT dwWidth, IN UINT dwHeight, IN float fSensitivity, IN float* fThreSize, OUT int& iOUT_Counter, OUT char** ppchOUT_Area);
	void	Lump_Detection(IN LPWORD pImageBuf, IN UINT dwWidth, IN UINT dwHeight, IN ST_CAN_Particle_Entry_Opt& ParticleOpt);
	

	void	ParticleCluster	(IN int nParticleCount, IN ST_ROI* pROI, IN float* pConcentration, OUT bool* pbOUT_Judge, OUT LPBYTE lpbyOUT_ParticleType);
	//void	ParticleCluster	(ST_ROI* pROI, float* pConcentration, int nParticleCount, bool* pPassIndex, ST_CAN_Particle_Result &ParticleResult);	


	void	Particle_Test(IN LPWORD pImageBuf, IN UINT dwWidth, IN UINT dwHeight, IN ST_CAN_Particle_Entry_Opt ParticleOpt, OUT ST_CAN_Particle_Entry_Result &ParticleResult);
	
	//전역으로 씀
	CvRect m_rtParticle[2000];

};

#endif // TI_Particle_h__