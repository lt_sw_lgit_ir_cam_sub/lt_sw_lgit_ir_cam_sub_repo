#include "stdafx.h"
#include "TI_T3D_Depth.h"

CTI_T3D_Depth::CTI_T3D_Depth()
{
//	wdImageBuf = new WORD[CAM_IMAGE_WIDTH * CAM_IMAGE_HEIGHT];
}

CTI_T3D_Depth::~CTI_T3D_Depth()
{
// 	if (NULL != wdImageBuf)
// 	{
// 		delete[] wdImageBuf;
// 		wdImageBuf = NULL;
// 	}
}

//=============================================================================
// Method		: T3D_Depth_Test
// Access		: public  
// Returns		: void
// Parameter	: __in float fTreshold
// Parameter	: __out int & iIndexCount
// Parameter	: __out WORD * sPointX
// Parameter	: __out WORD * sPointY
// Parameter	: __out float * fConcentration
// Parameter	: WORD* wdImageBuf
// Qualifier	:
// Last Update	: 2017/12/19 - 13:28
// Desc.		:
//=============================================================================
void CTI_T3D_Depth::T3D_Depth_Test(__in LPWORD pImageBuf, __in int iWidth, __in int iHeight, __out double* dbAveData, __out double *dbstdevData)
{
	unsigned long count = 0;

	double Sum1 = 0, Sum2 = 0;
	double data = 0;

	for (int y = 0; y < iHeight; y++)
	{
		for (int x = 0; x < iHeight; x++)
		{
			data = (double)pImageBuf[y*iWidth + x];
			Sum1 += data;
			Sum2 += data*data;
			count++;
		}
	}

	double Mean = 0;
	double mean2 = 0;
	double StdDev = 0;

	if (count > 0)
	{
		Mean = Sum1 / (double)count;

		mean2 = Sum2 / (double)count;

		StdDev = sqrt(mean2 - Mean*Mean);
	}

	*dbAveData = Mean;
	*dbstdevData = StdDev;

}
