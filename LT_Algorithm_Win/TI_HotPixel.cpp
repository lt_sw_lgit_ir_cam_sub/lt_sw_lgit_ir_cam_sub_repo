#include "stdafx.h"
#include "TI_HotPixel.h"

#define STEP_PIXEL			1	// Step Pixel수
#define SIZE_SCAN_BLOCK		4	// Block Size
#define HOTPIXEL_INDX_MAX	50	

#define SCALE_INVALID		0
#define SCALE_WIDTH			320	// 영상	사이즈 / 2
#define SCALE_HEIGHT		240	// 영상	사이즈 / 2

CTI_HotPixel::CTI_HotPixel()
{
	m_wScalerBuf = new WORD[SCALE_WIDTH	* SCALE_HEIGHT];
}

CTI_HotPixel::~CTI_HotPixel()
{
	if (NULL != m_wScalerBuf)
	{
		delete[] m_wScalerBuf;
		m_wScalerBuf = NULL;
	}
}

//=============================================================================
// Method		: HotPixel_Test
// Access		: public  
// Returns		: void
// Parameter	: __in float fTreshold
// Parameter	: __out int & iIndexCount
// Parameter	: __out WORD * sPointX
// Parameter	: __out WORD * sPointY
// Parameter	: __out float * fConcentration
// Parameter	: WORD* wdImageBuf
// Qualifier	:
// Last Update	: 2017/12/19 - 13:28
// Desc.		:
//=============================================================================
void CTI_HotPixel::HotPixel_Test(__in LPWORD pImageBuf, __in int iWidth, __in int iHeight, __in float fTreshold, __out int& iIndexCount, __out WORD* sPointX, __out WORD* sPointY, __out float* fConcentration)
{
	// 이미지 전환 360, 240
	HotPixel_Scaler(pImageBuf);
	
	int nStart_X = 0;
	int nStart_Y = 1;
	int nEnd_X = SCALE_WIDTH;
	int nEnd_Y = SCALE_HEIGHT;

	unsigned long int *iIntegral = new unsigned long int[(SCALE_WIDTH + 1) * (SCALE_HEIGHT + 1)];

	// 테이블 생성
	for (int y = 0; y < nEnd_Y; y++)
	{
		for (int x = 0; x < nEnd_X; x++)
		{
			if (x == 0 && y == 0)
			{
				iIntegral[0] = 0;
				iIntegral[(y + 1) * (SCALE_WIDTH + 1) + (x + 1)] = m_wScalerBuf[y * SCALE_WIDTH + x];
			}
			else
			{
				if (x == 0)
				{
					iIntegral[(y + 1) * (SCALE_WIDTH + 1) + 1] = iIntegral[y * (SCALE_WIDTH + 1) + 1] + m_wScalerBuf[y * SCALE_WIDTH];
					iIntegral[(y + 1) * (SCALE_WIDTH + 1)] = 0;
				}

				if (y == 0)
				{
					iIntegral[(SCALE_WIDTH + 1) + (x + 1)] = iIntegral[(SCALE_WIDTH + 1) + x] + m_wScalerBuf[x];
					iIntegral[x + 1] = 0;
				}
			}

			if (x > 0 && y > 0)
			{
				iIntegral[(y + 1) * (SCALE_WIDTH + 1) + (x + 1)] = iIntegral[(y + 1) * (SCALE_WIDTH + 1) + x] + iIntegral[y * (SCALE_WIDTH + 1) + (x + 1)]
					- iIntegral[y * (SCALE_WIDTH + 1) + x] + m_wScalerBuf[y * SCALE_WIDTH + x];
			}
		}
	}

	iIntegral[1] = iIntegral[SCALE_WIDTH + 1] = 0;

	double dbSum_Outter = 0.0, dbSum_Inner = 0.0;
	double dbMean_Inner = 0.0, dbMean_Outter = 0.0;

	int nInner = SIZE_SCAN_BLOCK * SIZE_SCAN_BLOCK;
	int nOutter = nInner * 8;
	int nBlock = SIZE_SCAN_BLOCK;
	bool IsNearDetect = false;
	double dbDistance = 9999.0;
	double dbValue = 0.0;
	double dbDifference;

	for (int y = nStart_Y; y < (nEnd_Y - nBlock * 3) && (iIndexCount < HOTPIXEL_INDX_MAX); y += STEP_PIXEL)
	{
		for (int x = nStart_X; (x < nEnd_X - nBlock * 3) && (iIndexCount < HOTPIXEL_INDX_MAX); x += STEP_PIXEL)
		{
			// 중앙 합
			dbSum_Inner = (double)(iIntegral[(y + nBlock * 2)*(SCALE_WIDTH + 1) + (x + nBlock * 2)] + iIntegral[(y + nBlock)*(SCALE_WIDTH + 1) + (x + nBlock)] - iIntegral[(y + nBlock)*(SCALE_WIDTH + 1) + (x + nBlock * 2)] - iIntegral[(y + nBlock * 2)*(SCALE_WIDTH + 1) + (x + nBlock)]);
			// 중앙 평균
			dbMean_Inner = dbSum_Inner / (double)nInner;

			// 외곽 합
			dbSum_Outter = (double)(iIntegral[(y + nBlock * 3)*(SCALE_WIDTH + 1) + (x + nBlock * 3)] + iIntegral[(y)*(SCALE_WIDTH + 1) + (x)]
				- iIntegral[(y)*(SCALE_WIDTH + 1) + (x + nBlock * 3)] - iIntegral[(y + nBlock * 3)*(SCALE_WIDTH + 1) + (x)] - dbSum_Inner);
			// 외곽 평균
			dbMean_Outter = dbSum_Outter / (double)nOutter;

			// 차이
			dbDifference = dbMean_Outter - dbMean_Inner;

			// Hot Pixel일 때만 한다.
			// Dead Pixel은 하지 않음
			dbDifference *= -1.0;

			// 농도 계산
			dbValue = dbDifference / dbMean_Outter * 100.0;

			if (dbValue > fTreshold && iIndexCount < HOTPIXEL_INDX_MAX)
			{
				IsNearDetect = false;

				// 				for (int i = 0; i < iIndexCount; i++)
				// 				{
				// 					dbDistance = sqrt((double)(sPointX[i] - (x + nBlock))*(sPointX[i] - (x + nBlock)) +
				// 						(double)(sPointY[i] - (y + nBlock))*(sPointY[i] - (y + nBlock)));
				// 
				// 					if (dbDistance < (double)nBlock * 1.5)
				// 					{
				// 						IsNearDetect = true;
				// 						break;
				// 					}
				//				}

				if (IsNearDetect == false)
				{
					// 농도
					fConcentration[iIndexCount] = (float)dbValue;

					// 검색 영역의 평균 값
					sPointX[iIndexCount] = ((x + nBlock) + (x + nBlock * 2)) / 2;
					sPointY[iIndexCount] = ((y + nBlock) + (y + nBlock * 2)) / 2;

					// 갯수
					iIndexCount++;

					// HOTPIXEL_INDX_MAX 이상 된다면 더이상 찾지 않고 검색을 종료한다
					if (iIndexCount >= HOTPIXEL_INDX_MAX)
					{
						delete[] iIntegral;
						return;
					}
				}
			}
		}
	}

	delete[] iIntegral;
}

//=============================================================================
// Method		: HotPixel_Scaler
// Access		: protected  
// Returns		: void
// Parameter	: __in WORD * wImageBuf
// Qualifier	:
// Last Update	: 2018/8/13 - 20:34
// Desc.		:
//=============================================================================
void CTI_HotPixel::HotPixel_Scaler(__in WORD *wImageBuf)
{
	for (int y = 0; y < 240; y++)
	{
		for (int x = 0; x < 320; x++)
		{
			int cnt = 0;
			int sum = 0;
			int ind = (y * 2 * 640) + (x * 2);
			int out_ind = (y * 320) + x;

			cnt += ((wImageBuf[ind]) != SCALE_INVALID);
			cnt += ((wImageBuf[ind + 1]) != SCALE_INVALID);
			cnt += ((wImageBuf[ind + 640]) != SCALE_INVALID);
			cnt += ((wImageBuf[ind + 641]) != SCALE_INVALID);


			if (wImageBuf[ind] == SCALE_INVALID)
				sum += (cnt == 3) ? ((wImageBuf[ind + 1] + wImageBuf[ind + 640] + 1) >> 1) : 0;
			else
				sum += (wImageBuf[ind]);

			if (wImageBuf[ind + 1] == SCALE_INVALID)
				sum += (cnt == 3) ? (wImageBuf[ind] + wImageBuf[ind + 641] + 1) >> 1 : 0;
			else
				sum += wImageBuf[ind + 1];

			if (wImageBuf[ind + 640] == SCALE_INVALID)
				sum += (cnt == 3) ? (wImageBuf[ind] + wImageBuf[ind + 641] + 1) >> 1 : 0;
			else
				sum += wImageBuf[ind + 640];

			if (wImageBuf[ind + 641] == SCALE_INVALID)
				sum += (cnt == 3) ? (wImageBuf[ind + 1] + wImageBuf[ind + 640] + 1) >> 1 : 0;
			else
				sum += wImageBuf[ind + 641];

			switch (cnt)
			{
			case 0:
				m_wScalerBuf[out_ind] = 0;
				break;
			case 1:
				m_wScalerBuf[out_ind] = sum;
				break;
			case 2:
				m_wScalerBuf[out_ind] = (sum + 1) >> 1;
				break;
			case 3:
			case 4:
				m_wScalerBuf[out_ind] = (sum + 2) >> 2;
				break;
			}
		}
	}
}
