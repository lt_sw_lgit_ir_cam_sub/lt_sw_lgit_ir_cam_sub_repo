//*****************************************************************************
// Filename	: 	CVA_HotPixel.h
// Created	:	2017/12/19 - 15:13
// Modified	:	2017/12/19 - 15:13
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef CVA_HotPixel_h__
#define CVA_HotPixel_h__

#pragma once
#include <windows.h>

#define MAX_HOT_PIXEL_DETECT_CNT	50
typedef struct _tag_HotPixelUnit
{
	unsigned short	wPosX;
	unsigned short	wPosY;
	float			fConcentration;

	_tag_HotPixelUnit()
	{
		Reset();
	};

	void Reset()
	{
		wPosX = 0;
		wPosY = 0;
		fConcentration = 0.0f;
	};
}ST_HotPixelUnit, *PST_HotPixelUnit;

typedef struct _tag_HotPixelInfo
{
	byte				byDetectCount;		// Max 50

	ST_HotPixelUnit		stDataz[MAX_HOT_PIXEL_DETECT_CNT];

	_tag_HotPixelInfo()
	{
		byDetectCount = 0;

		memset(stDataz, 0, sizeof(ST_HotPixelUnit)* MAX_HOT_PIXEL_DETECT_CNT);
	};

}ST_HotPixelInfo, *PST_HotPixelInfo;

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
class CHotPixeRect
{
	long			left;
	long			top;
	long			right;
	long			bottom;

	unsigned int	nDetectSize;
	RECT			SearchRect;

	CHotPixeRect()
	{
		left		= 0;
		top			= 0;
		right		= 0;
		bottom		= 0;

		nDetectSize	= 2;

		memset(&SearchRect, 0, sizeof(RECT));
	};

	CHotPixeRect& operator= (CHotPixeRect& ref)
	{
		left		= ref.left;
		top			= ref.top;
		right		= ref.right;
		bottom		= ref.bottom;
		nDetectSize	= ref.nDetectSize;

		memcpy(&SearchRect, &ref.SearchRect, sizeof(RECT));
	};

	// 검색할 전체 영역 크기
	void Set_SearchRect(__in unsigned int nWidth, __in unsigned int nHeight)
	{
		SearchRect.left		= 0;
		SearchRect.top		= 0;
		SearchRect.right	= nWidth;
		SearchRect.bottom	= nHeight;
	};

	void Set_DetectPixelSize(__in unsigned int nSize)
	{
		nDetectSize = nSize;

		Move_Origin();
	};

	void Move_Origin()
	{
		left	= 0;
		top		= 0;
		right	= left + (nDetectSize * 3) - 1;
		bottom	= top + (nDetectSize * 3) - 1;
	};

	BOOL Move_Right(__in int nPixelCount = 1)
	{
		if ((right + nPixelCount) <= SearchRect.right)
		{
			left	+= nPixelCount;
			right	+= nPixelCount;

			return TRUE;
		}
		else
		{
			return FALSE;
		}
	};

	BOOL Move_Left(__in int nPixelCount = 1)
	{
		if (SearchRect.left <= (right - nPixelCount))
		{
			left	-= nPixelCount;
			right	-= nPixelCount;

			return TRUE;
		}
		else
		{
			return FALSE;
		}
	};

	BOOL Move_Up(__in int nPixelCount = 1)
	{
		if (SearchRect.top <= (top - nPixelCount))
		{
			top		-= nPixelCount;
			bottom	-= nPixelCount;

			return TRUE;
		}
		else
		{
			return FALSE;
		}
	};

	BOOL Move_Bottom(__in int nPixelCount = 1)
	{
		if ((bottom + nPixelCount) <= SearchRect.bottom)
		{
			top		+= nPixelCount;
			bottom	+= nPixelCount;

			return TRUE;
		}
		else
		{
			return FALSE;
		}
	};

	// 이미지 설정
	void	Set_Image			(__in const unsigned short** pInImage, __in unsigned long dwImageSize, __in unsigned int iWidth, __in unsigned int iHeight);

	// 주변 농도, 가운데 농도 및 차이 배분율
	long	Get_Concentration	(__out float& fInnerConcent, __out float& fOutterConect, __out float& fDifferentRate);

};

//-----------------------------------------------------------------------------
// CVA_HotPixel
//-----------------------------------------------------------------------------
class CVA_HotPixel
{
public:
	CVA_HotPixel();
	~CVA_HotPixel();

	void	Detect_HotPixel		(__in const char* pInImage, __in unsigned long dwImageSize, __in unsigned int iWidth, __in unsigned int iHeight, __in float fTreshold, __out ST_HotPixelInfo& stOutResult);

};

#endif // CVA_HotPixel_h__
