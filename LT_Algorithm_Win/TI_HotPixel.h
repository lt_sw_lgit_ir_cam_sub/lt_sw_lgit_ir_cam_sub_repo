#pragma once

#include "cv.h"
#include "highgui.h"

class CTI_HotPixel
{
public:
	CTI_HotPixel();
	virtual ~CTI_HotPixel();

	void	HotPixel_Test(__in LPWORD pImageBuf, __in int iWidth, __in int iHeight, __in float fTreshold, __out int& iIndexCount, __out WORD* sPointX, __out WORD* sPointY, __out float* fConcentration);

protected:

	WORD	*m_wScalerBuf;

	void	HotPixel_Scaler(__in WORD *wImageBuf);
};

