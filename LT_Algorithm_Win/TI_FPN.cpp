﻿//*****************************************************************************
// Filename	: 	TI_FPN.cpp
// Created	:	2018/2/10 - 17:11
// Modified	:	2018/2/10 - 17:11
//
// Author	:	Luritech
//	
// Purpose	:	
//****************************************************************************
#include "stdafx.h"
#include "TI_FPN.h"

CTI_FPN::CTI_FPN()
{

}

CTI_FPN::~CTI_FPN()
{

}

//=============================================================================
// Method		: FPN_Test
// Access		: public  
// Returns		: int
// Parameter	: IN unsigned short * pImageBuf
// Parameter	: IN unsigned int dwWidth
// Parameter	: IN unsigned int dwHeight
// Parameter	: IN float fThresholdX
// Parameter	: IN float fThresholdY
// Parameter	: OUT unsigned short & sFailCountX
// Parameter	: OUT unsigned short & sFailCountY
// Qualifier	:
// Last Update	: 2018/2/10 - 17:10
// Desc.		:
//=============================================================================
void CTI_FPN::FPN_Test(IN unsigned short* pImageBuf, unsigned int dwWidth, unsigned int dwHeight, IN CRect rtRoI, IN float fThresholdX, IN float fThresholdY, OUT int& sFailCountX, OUT  int& sFailCountY)
{

	int iStart_X = rtRoI.left;
	int iStart_Y = rtRoI.top;
	unsigned int iImgWidth = rtRoI.Width();
	unsigned int iImgHeight = rtRoI.Height();

	double* pResultValue_X = new double[iImgWidth];
	double* pResultValue_Y = new double[iImgHeight];

	double	*dbSum_X = new double[iImgWidth];
	double	*dbSum_Y = new double[iImgHeight];

	bool* pPassLineX = new bool[iImgWidth];
	bool* pPassLineY = new bool[iImgHeight];

	// 초기화
	for (unsigned int i = 0; i < iImgWidth; i++)
	{
		dbSum_X[i] = 0.0;
		pPassLineX[i] = true;
	}
	for (unsigned int i = 0; i < iImgHeight; i++)
	{
		dbSum_Y[i] = 0.0;
		pPassLineY[i] = true;
	}

	int nCntX = 0;
	int nCntY = 0;

	unsigned int nLimitWidth = iStart_X + (iImgWidth);
	unsigned int nLimitHeight = iStart_Y + (iImgHeight);

	if (nLimitWidth > dwWidth)
		nLimitWidth = dwWidth;
	if (nLimitHeight > dwHeight)
		nLimitHeight = dwHeight;

	//////////////////// 전체 이미지중 설정된 영역(in_rectInspectionArea)에 대해 (각 Line별) FPN 값을 구함.////
	for (int X = iStart_X; X < nLimitWidth; X++)
	{
		nCntY = 0;
		for (int Y = iStart_Y; Y < nLimitHeight; Y++)
		{
			dbSum_X[nCntX] += pImageBuf[Y * dwWidth + X]; //편균 밝기 구하기 위해, 우선 각 Line의 밝기값(세로줄) 누적합(Sum)하는 중..
			dbSum_Y[nCntY] += pImageBuf[Y * dwWidth + X]; //편균 밝기 구하기 위해, 우선 각 Line의 밝기값(가로줄) 누적합(Sum)하는 중..
			nCntY++;
		}
		nCntX++;
	}

	sFailCountX = 0;
	sFailCountY = 0;

	//각 세로 Line 들에 대해 Sum으로 부터 평균 밝기 값을 각각 구함 
	for (int i = 0; i < iImgWidth; i++)
	{
		pResultValue_X[i] = dbSum_X[i] / (double)nCntY;
		if (fThresholdX < pResultValue_X[i])
		{
			sFailCountX++;
			pPassLineX[i] = false;
		}
	}

	//각 가로 Line들에 대해 Sum으로 부터 평균 밝기 값을 각각 구함 
	for (int i = 0; i < iImgHeight; i++)
	{
		pResultValue_Y[i] = dbSum_Y[i] / (double)nCntX;
		if (fThresholdY < pResultValue_Y[i])
		{
			sFailCountY++;
			pPassLineY[i] = false;
		}
	}

	delete[] pResultValue_X;
	delete[] pResultValue_Y;
	delete[] dbSum_X;
	delete[] dbSum_Y;
	delete[] pPassLineX;
	delete[] pPassLineY;

}
