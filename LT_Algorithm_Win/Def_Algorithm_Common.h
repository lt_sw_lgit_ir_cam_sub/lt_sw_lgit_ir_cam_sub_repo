//*****************************************************************************
// Filename	: 	Def_Algorithm_Common.h
// Created	:	2018/2/10 - 16:11
// Modified	:	2018/2/10 - 16:11
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#ifndef Def_Algorithm_Common_h__
#define Def_Algorithm_Common_h__

#pragma once

#include <math.h>

typedef enum enMark_Color
{
	MarkCol_White = 0,
	MarkCol_Black,
	MarkCol_Max,
};

typedef enum enInspectionAlgoritm
{
	Inspection_Init = 0,				// 초기값
	Inspection_SFR,						// SFR
	Inspection_FiducialMark,
	Inspection_EdgeLine,
	Inspection_SNR,

	// 이물
	Inspection_Particle,				// 이물검사(Stain, Blemish, Dead Pixel)
	Inspection_Shading,			// 광량비
	Inspection_SNR_Light,				// 이물 SNR
	Inspection_FPN,						// FPN
	Inspection_DefectPixel,				// Hot Pixel
	Inspection_Max,
};

// 광량비, 이물 SNR의 영역 나누는 갯수
typedef enum enSectionNum
{
	SECTION_NUM_X = 21,
	SECTION_NUM_Y = 21,
};

// Header
typedef struct _tag_CAN_Header
{
	// 알고리즘 종류(INSPECTION_ALGORITHM)
	unsigned char	iTestItem;

	_tag_CAN_Header()
	{
		iTestItem = Inspection_Init;
	};

}ST_CAN_Header;

typedef struct _tag_CAN_ROI
{
	unsigned short	sLeft;
	unsigned short	sTop;
	unsigned short	sWidth;
	unsigned short	sHeight;

	_tag_CAN_ROI()
	{
		sLeft = 0;
		sTop = 0;
		sWidth = 0;
		sHeight = 0;
	};

}ST_ROI;

#endif // Def_Algorithm_Common_h__
