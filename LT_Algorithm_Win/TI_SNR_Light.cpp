﻿//*****************************************************************************
// Filename	: 	TI_SNR_Light.cpp
// Created	:	2018/2/11 - 15:15
// Modified	:	2018/2/11 - 15:15
//
// Author	:	Luritech
//	
// Purpose	:	
//****************************************************************************
#include "stdafx.h"
#include "TI_SNR_Light.h"
#include <math.h>


CTI_SNR_Light::CTI_SNR_Light()
{
	
}

CTI_SNR_Light::~CTI_SNR_Light()
{
	
}

//=============================================================================
// Method		: MakeSection
// Access		: protected  
// Returns		: void
// Parameter	: IN unsigned int dwWidth
// Parameter	: IN unsigned int dwHeight
// Parameter	: OUT ST_ROI * pstOUT_ROI
// Parameter	: IN unsigned int nSectionCnt_X
// Parameter	: IN unsigned int nSectionCnt_Y
// Qualifier	:
// Last Update	: 2018/2/11 - 15:02
// Desc.		:
//=============================================================================
void CTI_SNR_Light::MakeSection(IN unsigned int dwWidth, IN unsigned int dwHeight, OUT ST_ROI* pstOUT_ROI, IN unsigned int nSectionCnt_X /*= SECTION_NUM_X*/, IN unsigned int nSectionCnt_Y /*= SECTION_NUM_Y*/)
{
	// 섹션 사이즈
	int nSectionSizeX = dwWidth / (nSectionCnt_X - 1);
	int nSectionSizeY = dwHeight / (nSectionCnt_Y - 1);
	
	// 위치값을 누적하여 계산
	int nPosX = 0, nPosY = 0;
	int nIndex = 0;

	for (int iIdx_Y = 0; iIdx_Y < (int)nSectionCnt_Y; iIdx_Y++)
	{
		nPosX = 0;

		// 좌표 : 가로 줄
		if (iIdx_Y == 0)
			nPosY = 0;
		else if (iIdx_Y != 1)
			nPosY += nSectionSizeY;
		else
			nPosY += nSectionSizeY / 2;

		for (int iIdx_X = 0; iIdx_X < (int)nSectionCnt_X; iIdx_X++)
		{
			nIndex = iIdx_Y * nSectionCnt_X + iIdx_X;

			// 좌표 : 세로 줄
			if (iIdx_X == 0)
				nPosX = 0;
			else if (iIdx_X != 1)
				nPosX += nSectionSizeX;
			else
				nPosX += nSectionSizeX / 2;

			// 사이즈 : 세로 줄
			if (iIdx_X == 0 || iIdx_X == nSectionCnt_X - 1)
				pstOUT_ROI[nIndex].sWidth = nSectionSizeX / 2;
			else
				pstOUT_ROI[nIndex].sWidth = nSectionSizeX;
			
			// 사이즈 : 가로 줄
			if (iIdx_Y == 0 || iIdx_Y == nSectionCnt_Y - 1)
				pstOUT_ROI[nIndex].sHeight = nSectionSizeY / 2;
			else
				pstOUT_ROI[nIndex].sHeight = nSectionSizeY;

			pstOUT_ROI[nIndex].sTop = nPosY;
			pstOUT_ROI[nIndex].sLeft = nPosX;
		}
	}
}

//=============================================================================
// Method		: SetROI
// Access		: protected  
// Returns		: UINT
// Parameter	: IN unsigned int dwWidth
// Parameter	: IN unsigned int dwHeight
// Parameter	: IN unsigned int nROI_Count
// Parameter	: IN const bool * pbIN_ROI_UseFlag
// Parameter	: OUT ST_ROI * pstOUT_ROI
// Parameter	: IN unsigned int nSectionCnt_X
// Parameter	: IN unsigned int nSectionCnt_Y
// Qualifier	:
// Last Update	: 2018/2/11 - 15:16
// Desc.		:
//=============================================================================
UINT CTI_SNR_Light::SetROI(IN unsigned int dwWidth, IN unsigned int dwHeight, IN unsigned int nROI_Count, IN const bool* pbIN_ROI_UseFlag, OUT ST_ROI* pstOUT_ROI, IN unsigned int nSectionCnt_X /*= SECTION_NUM_X*/, IN unsigned int nSectionCnt_Y /*= SECTION_NUM_Y*/)
{
	ST_ROI* pstAllROI = new ST_ROI[nSectionCnt_X * nSectionCnt_Y];

	MakeSection(dwWidth, dwHeight, pstAllROI, nSectionCnt_X, nSectionCnt_Y);

	UINT nMax_ROI_Count = min(nROI_Count, m_nSNR_Light_MaxCount);

	int iCount = 0;
// 
// 	for (UINT nIdx = 0; nIdx < m_nSNR_Light_MaxCount; nIdx++)
// 	{
// 		if (pbIN_ROI_UseFlag[nIdx] == true)
// 		{
// 			pstOUT_ROI[iCount] = pstAllROI[g_nSNR_Light[nIdx]];
// 			iCount++;
// 		}
// 	}


	delete[] pstAllROI;

	return nMax_ROI_Count;
}

//=============================================================================
// Method		: SNR_Light_Test
// Access		: public  
// Returns		: int
// Parameter	: IN unsigned short * pImageBuf
// Parameter	: IN unsigned int dwWidth
// Parameter	: IN unsigned int dwHeight
// Parameter	: IN unsigned int nIN_ROI_Count
// Parameter	: IN const bool * pbIN_ROI_UseFlag
// Parameter	: OUT unsigned short * arwOUT_Signal
// Parameter	: OUT float * arfOUT_Noise
// Qualifier	:
// Last Update	: 2018/2/11 - 15:13
// Desc.		:
//=============================================================================

int CTI_SNR_Light::SNR_Light_Test(IN unsigned short* pImageBuf, IN int dwWidth, IN int dwHeight, IN int nIN_ROI_Count, IN const bool* pbIN_ROI_UseFlag, OUT short* arwOUT_Signal, OUT float* arfOUT_Noise)
{
	if (nullptr == pImageBuf)
		return 0;

	int nMax_ROI_Count = min(nIN_ROI_Count, (int)m_nSNR_Light_MaxCount);

	ST_ROI* pstROI = new ST_ROI[nIN_ROI_Count];

	int iROI_Count	= SetROI(dwWidth, dwHeight, nMax_ROI_Count, pbIN_ROI_UseFlag, pstROI, m_nSectionCount_X, m_nSectionCount_Y);

	double dbData	= 0.0;
	double dbVal	= 0.0;
	int iCount		= 0;

	for (int iIdx_ROI = 0; iIdx_ROI < iROI_Count; iIdx_ROI++)
	{
		dbData = 0.0;
		dbVal = 0.0;

		// 사용하는 항목만 계산
		if (pbIN_ROI_UseFlag[iIdx_ROI] == true)
		{
				if (iCount >= iROI_Count)
				{
					iCount = iROI_Count;
				}

				for (int y = pstROI[iCount].sTop; y < pstROI[iCount].sTop + pstROI[iCount].sHeight; y++)
				{
					for (int x = pstROI[iCount].sLeft; x < pstROI[iCount].sLeft + pstROI[iCount].sWidth; x++)
					{
						dbData += pImageBuf[y * dwWidth + x];
					}
				}

				arwOUT_Signal[iIdx_ROI] = (short)(dbData / (double)(pstROI[iCount].sWidth * pstROI[iCount].sHeight));

				dbData = 0.0;

				for (int y = pstROI[iCount].sTop; y < pstROI[iCount].sTop + pstROI[iCount].sHeight; y++)
				{
					for (int x = pstROI[iCount].sLeft; x < pstROI[iCount].sLeft + pstROI[iCount].sWidth; x++)
					{
						dbData = abs(arwOUT_Signal[iIdx_ROI] - pImageBuf[y * dwWidth + x]);
						dbVal += dbData * dbData;
					}
				}

				arfOUT_Noise[iIdx_ROI] = (float)(sqrt(dbVal / (double)(pstROI[iCount].sWidth * pstROI[iCount].sHeight)));

				iCount++;
			}
	}

	delete[] pstROI;

	return 1;
}



int CTI_SNR_Light::SNR_Light_Test_New(IN unsigned short* pImageBuf, IN int dwWidth, IN int dwHeight, IN const CRect* pTestRect, IN const bool* pbIN_ROI_UseFlag, OUT short* arwOUT_Signal, OUT float* arfOUT_Noise)
{
	if (nullptr == pImageBuf)
		return 0;

	
	double dbData = 0.0;
	double dbVal = 0.0;
	int iCount = 0;

	for (int iIdx_ROI = 0; iIdx_ROI < MAX_SNRLight_ROI_Count; iIdx_ROI++)
	{
		dbData = 0.0;
		dbVal = 0.0;

		// 사용하는 항목만 계산
		if (pbIN_ROI_UseFlag[iIdx_ROI] == true)
		{
		
			for (int y = pTestRect[iCount].top; y <pTestRect[iCount].top + pTestRect[iCount].Height(); y++)
			{
				for (int x = pTestRect[iCount].left; x < pTestRect[iCount].left + pTestRect[iCount].Width(); x++)
				{
					dbData += pImageBuf[y * dwWidth + x];
				}
			}

			arwOUT_Signal[iIdx_ROI] = (short)(dbData / (double)(pTestRect[iCount].Width() * pTestRect[iCount].Height()));

			dbData = 0.0;

			for (int y = pTestRect[iCount].top; y < pTestRect[iCount].top + pTestRect[iCount].Height(); y++)
			{
				for (int x = pTestRect[iCount].left; x < pTestRect[iCount].left + pTestRect[iCount].Width(); x++)
				{
					dbData = abs(arwOUT_Signal[iIdx_ROI] - pImageBuf[y * dwWidth + x]);
					dbVal += dbData * dbData;
				}
			}

			arfOUT_Noise[iIdx_ROI] = (float)(sqrt(dbVal / (double)(pTestRect[iCount].Width() * pTestRect[iCount].Height())));

			iCount++;
		}
	}


	return 1;
}


