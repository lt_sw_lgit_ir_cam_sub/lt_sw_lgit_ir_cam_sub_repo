﻿//*****************************************************************************
// Filename	: 	TI_SFR.h
// Created	:	2018/2/11 - 17:17
// Modified	:	2018/2/11 - 17:17
//
// Author	:	Luritech
//	
// Purpose	:	
//****************************************************************************
#ifndef TI_SFR_h__
#define TI_SFR_h__

#pragma once

#include <afxwin.h>
#include "Def_Algorithm_Common.h"
#include "cv.h"
#include "highgui.h"

//-----------------------------------------------------------------------------
// SFR
//-----------------------------------------------------------------------------

#define		MAX_SFR_ROI_Count		30

//=============================================================================
// CTI_SFR
//=============================================================================
class CTI_SFR
{
public:
	CTI_SFR();
	~CTI_SFR();

protected:
	
	void	MulMatrix			(double **A, double **B, double **C, unsigned int Row, unsigned int Col, unsigned int n, unsigned int Mode);
	void	pInverse			(double **A, unsigned int Row, unsigned int Col, double **RtnMat);
	void	Inverse				(double **dataMat, unsigned int n, double **MatRtn);
	
	//double	GetSFRValue(WORD *pBuf, int iWidth, int iHeight, double dbPixelSize, double dbLinePair); 기존
	double	GetSFRValue(WORD *pBuf, int iWidth, int iHeight, double dbPixelSize, double dbLinePair, int imode);


	double	Get_ROI_SFR_16bit(IN LPWORD pImageBuf, IN UINT dwWidth, IN UINT dwHeight, IN ST_ROI stROI, double dbPixelSize, IN double dbLinPair);

	//double Get_ROI_SFR_16bit(IN LPWORD pImageBuf, int nImgW, int nImgH, IN CRect ROI, double dbPixelSize, IN double dbLinPair); 기존
	double Get_ROI_SFR_16bit(IN LPWORD pImageBuf, int nImgW, int nImgH, IN CRect ROI, double dbPixelSize, IN double dbLinPair,int imode);
public:

	int		SFR_Test(IN LPWORD pImageBuf, IN UINT dwWidth, IN UINT dwHeight, IN ST_ROI stROI, IN double dbLinPair, double dbPixelSize, OUT double& dbResultSFR);

	int		SFR_Test(IN LPWORD pImageBuf, int nImgW, int nImgH, CRect ROIrect, IN double dbLinPair, double dbPixelSize, OUT double& dbResultSFR , int imode);
};

#endif // TI_SFR_h__