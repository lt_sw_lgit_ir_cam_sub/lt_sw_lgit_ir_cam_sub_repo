﻿//*****************************************************************************
// Filename	: 	TI_EdgeLine.h
// Created	:	2018/2/11 - 15:38
// Modified	:	2018/2/11 - 15:38
//
// Author	:	Luritech
//	
// Purpose	:	
//****************************************************************************
#ifndef TI_EdgeLine_h__
#define TI_EdgeLine_h__

#pragma once

#include <afxwin.h>
#include "cv.h"
#include "highgui.h"
#include "Def_Algorithm_Common.h"

//-----------------------------------------------------------------------------
// EdgeLine
//-----------------------------------------------------------------------------

#define		MAX_EdgeLine_ROI_Count		10

typedef enum enEdgeLind_TYPE
{
	Line_LR_WB,
	Line_LR_BW,
	Line_TB_WB,
	Line_TB_BW,
};

//=============================================================================
// CTI_EdgeLine
//=============================================================================
class CTI_EdgeLine
{
public:
	CTI_EdgeLine();
	virtual ~CTI_EdgeLine();

protected:

public:

	//=============================================================================
	// Method		: EdgeLine_Test
	// Access		: public  
	// Returns		: int
	// Parameter	: IN LPWORD pImageBuf
	// Parameter	: IN UINT dwWidth
	// Parameter	: IN UINT dwHeight
	// Parameter	: IN ST_ROI stROI
	// Parameter	: IN int iCenterX
	// Parameter	: IN int iCenterY
	// Parameter	: IN BYTE byDetectColor
	// Parameter	: IN BYTE byBrightness
	// Parameter	: IN BYTE byThreshold
	// Parameter	: OUT POINT & ptOUT_Center
	// Qualifier	:
	// Last Update	: 2018/2/11 - 15:37
	// Desc.		:
	//=============================================================================
	int		EdgeLine_Test	(IN LPWORD pImageBuf, IN UINT dwWidth, IN UINT dwHeight, IN ST_ROI stROI, IN int iCenterX, IN int iCenterY, IN BYTE byDetectColor, IN BYTE byBrightness, IN BYTE byThreshold, OUT POINT& ptOUT_Center);
};

#endif // TI_EdgeLine_h__
