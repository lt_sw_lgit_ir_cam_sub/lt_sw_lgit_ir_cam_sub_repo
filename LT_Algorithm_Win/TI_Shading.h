﻿//*****************************************************************************
// Filename	: 	TI_Shading.h
// Created	:	2018/2/10 - 17:51
// Modified	:	2018/2/10 - 17:51
//
// Author	:	Luritech
//	
// Purpose	:	
//****************************************************************************
#ifndef TI_Shading_h__
#define TI_Shading_h__

#pragma once

#include <windows.h>
#include "Def_Algorithm_Common.h"

//-----------------------------------------------------------------------------
// Relative Illumination
//-----------------------------------------------------------------------------

#define		Shading_CenterPositon		36
#define		MAX_Shading_ROI_Count		73	// Max는 73개 고정

//=============================================================================
// CTI_Shading
//=============================================================================
class CTI_Shading
{
public:
	CTI_Shading();
	virtual ~CTI_Shading();

protected:

	void	MakeSection		(IN unsigned int dwWidth, IN unsigned int dwHeight, OUT ST_ROI* pstOUT_ROI, IN unsigned int nSectionCnt_X = SECTION_NUM_X, IN unsigned int nSectionCnt_Y = SECTION_NUM_Y);
	UINT	SetROI			(IN unsigned int dwWidth, IN unsigned int dwHeight, IN unsigned int nROI_Count, IN const bool* pbIN_ROI_UseFlag, OUT ST_ROI* pstOUT_ROI, IN unsigned int nSectionCnt_X = SECTION_NUM_X, IN unsigned int nSectionCnt_Y = SECTION_NUM_Y);		

	UINT	m_nSectionCount_X		= SECTION_NUM_X;
	UINT	m_nSectionCount_Y		= SECTION_NUM_Y;
	UINT	m_nShading_MaxCount		= MAX_Shading_ROI_Count;

public:

	void	Set_SectionCount(IN unsigned int nSectionCnt_X, IN unsigned int nSectionCnt_Y)
	{
		m_nSectionCount_X = nSectionCnt_X;
		m_nSectionCount_Y = nSectionCnt_Y;
	};

	void	Set_Shading_MaxCount(IN unsigned int nIn_ShadingCount)
	{
		m_nShading_MaxCount = nIn_ShadingCount;
	};
	
	//=============================================================================
	// Method		: Shading_Test
	// Access		: public  
	// Returns		: int
	// Parameter	: IN unsigned short * pImageBuf
	// Parameter	: IN unsigned int dwWidth
	// Parameter	: IN unsigned int dwHeight
	// Parameter	: IN unsigned int nIN_ROI_Count
	// Parameter	: IN const bool * pbIN_ROI_UseFlag
	// Parameter	: OUT unsigned short * arwOUT_Signal
	// Parameter	: OUT float * arfOUT_Noise
	// Qualifier	:
	// Last Update	: 2018/2/11 - 15:05
	// Desc.		:
	//=============================================================================
	int		Shading_Test	(IN unsigned short* pImageBuf, IN int dwWidth, IN int dwHeight, IN int nIN_ROI_Count, IN const bool* pbIN_ROI_UseFlag, OUT short* arwOUT_Signal, OUT float* arfOUT_Noise);

	int Shading_Test_New(IN unsigned short* pImageBuf, IN int dwWidth, IN int dwHeight, IN const CRect* pTestRect, IN const bool* pbIN_ROI_UseFlag, OUT short* arwOUT_Signal, OUT float* arfOUT_Noise);
};

#endif // TI_Shading_h__
