﻿//*****************************************************************************
// Filename	: 	TI_SNR.cpp
// Created	:	2018/2/10 - 17:14
// Modified	:	2018/2/10 - 17:14
//
// Author	:	Luritech
//	
// Purpose	:	
//****************************************************************************
//#include "stdafx.h"
#include "TI_SNR.h"
#include <math.h>

CTI_SNR::CTI_SNR()
{

}

CTI_SNR::~CTI_SNR()
{

}

//=============================================================================
// Method		: SNR_Test
// Access		: public  
// Returns		: int
// Parameter	: IN LPWORD pImageBuf
// Parameter	: IN UINT dwWidth
// Parameter	: IN UINT dwHeight
// Parameter	: IN ST_ROI stROI
// Parameter	: OUT short & wSignal
// Parameter	: OUT float & fNoise
// Qualifier	:
// Last Update	: 2018/2/27 - 18:58
// Desc.		:
//=============================================================================
int CTI_SNR::SNR_Test(IN LPWORD pImageBuf, IN UINT dwWidth, IN UINT dwHeight, IN ST_ROI stROI, OUT float& wSignal, OUT float& fNoise)
{

	if (nullptr == pImageBuf)
		return -1;

	double dbData = 0.0;
	double dbVal = 0.0;

	for (int y = stROI.sTop; y < stROI.sTop + stROI.sHeight; y++)
	{
		for (int x = stROI.sLeft; x < stROI.sLeft + stROI.sWidth; x++)
		{
			dbData += pImageBuf[y * dwWidth + x];
		}
	}

	wSignal = (short)(dbData / (double)(stROI.sWidth * stROI.sHeight));

	dbData = 0.0;

	for (int y = stROI.sTop; y < stROI.sTop + stROI.sHeight; y++)
	{
		for (int x = stROI.sLeft; x < stROI.sLeft + stROI.sWidth; x++)
		{
			dbData = abs(wSignal - pImageBuf[y * dwWidth + x]);
			dbVal += dbData * dbData;
		}
	}

	fNoise = (float)(sqrt(dbVal / (double)(stROI.sWidth * stROI.sHeight)));

	return 1;
}

//=============================================================================
// Method		: SNR_Test
// Access		: public  
// Returns		: int
// Parameter	: IN LPWORD pImageBuf
// Parameter	: IN UINT dwWidth
// Parameter	: IN UINT dwHeight
// Parameter	: IN ST_ROI stROI
// Parameter	: OUT short & wSignal
// Parameter	: OUT float & fNoise
// Qualifier	:
// Last Update	: 2018/5/2 - 10:15
// Desc.		:
//=============================================================================
int CTI_SNR::SNR_Test(IN LPWORD pImageBuf, IN UINT dwWidth, IN UINT dwHeight, IN ST_ROI stROI, OUT short& wSignal, OUT float& fNoise)
{

	if (nullptr == pImageBuf)
		return -1;

	double dbData = 0.0;
	double dbVal = 0.0;

	for (int y = stROI.sTop; y < stROI.sTop + stROI.sHeight; y++)
	{
		for (int x = stROI.sLeft; x < stROI.sLeft + stROI.sWidth; x++)
		{
			dbData += pImageBuf[y * dwWidth + x];
		}
	}

	wSignal = (short)(dbData / (double)(stROI.sWidth * stROI.sHeight));

	dbData = 0.0;

	for (int y = stROI.sTop; y < stROI.sTop + stROI.sHeight; y++)
	{
		for (int x = stROI.sLeft; x < stROI.sLeft + stROI.sWidth; x++)
		{
			dbData = abs(wSignal - pImageBuf[y * dwWidth + x]);
			dbVal += dbData * dbData;
		}
	}

	fNoise = (float)(sqrt(dbVal / (double)(stROI.sWidth * stROI.sHeight)));

	return 1;
}
