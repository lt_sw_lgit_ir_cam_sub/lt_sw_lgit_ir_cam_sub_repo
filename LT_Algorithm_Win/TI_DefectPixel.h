//*****************************************************************************
// Filename	: 	TI_DefectPixel.h
// Created	:	2018/2/10 - 17:26
// Modified	:	2018/2/10 - 17:26
//
// Author	:	Luritech
//	
// Purpose	:	
//****************************************************************************
#ifndef TI_DefectPixel_h__
#define TI_DefectPixel_h__

#pragma once
#include <afxwin.h>
#include "Def_Algorithm_Common.h"

//OpenCV 헤더 참조
#include <opencv2/core/core.hpp>  
#include <opencv2/highgui/highgui.hpp>  
#include <opencv2/imgproc/imgproc_c.h>

#define MAX_PARTICLE_COUNT 2000

// 이물에 필요한 간이 구조체
typedef struct _tag_DefectPixelERRData
{
	UINT x;
	UINT y;
	UINT W; // Cluster 불량을 위한 크기 저장
	UINT H;
	double dbLSBValue; // 전체 평균과의 차이 ___ VHP,HP 전용
	double dbDeviationRate; // 11 x 11 블럭 내부에서 평균과의 비교율 ___ VBP, BP, VDP, DP 전용

	/*
	Type 1 : Very Hot Pixel __ dbLSBValue > 1200->밝기
	Type 2 : Hot Pixel __ dbLSBValue > 600->밝기
	Type 3 : Very Bright Pixel __ dbDeviationRate > 50%
	Type 4 : Bright Pixel __ dbDeviationRate > 25%
	Type 5 : Very Dark Pixel __ dbDeviationRate < 50%
	Type 6 : Dark Pixel __ dbDeviationRate < 25%
	Type 7 : Cluster __ 2 Pixel 이상의 큰 Hot, Dead Pixel

	*/
	int nType;


	_tag_DefectPixelERRData()
	{
		x = 0;
		y = 0;
		W = 0;
		H = 0;
		dbLSBValue = 0.0;
		dbDeviationRate = 0.0;
		nType = 0;
	}

	void Reset()
	{
		x = 0;
		y = 0;
		W = 0;
		H = 0;
		dbLSBValue = 0.0;
		dbDeviationRate = 0.0;
		nType = 0;
	};

	_tag_DefectPixelERRData& operator= (_tag_DefectPixelERRData& ref)
	{
		x = ref.x;
		y = ref.y;
		W = ref.W;
		H = ref.H;
		dbLSBValue = ref.dbLSBValue;
		dbDeviationRate = ref.dbDeviationRate;
		nType = 0;

		return *this;
	};

}stDefectPixel;

typedef struct _tag_DPList
{
	stDefectPixel DP_List[MAX_PARTICLE_COUNT];

	int DefectPixe_Cnt;

	_tag_DPList()
	{
		Reset();
	};
	void release(){
// 		if (DP_List != NULL)
// 		{
// 			delete[]DP_List;
// 			DP_List = NULL;
// 		}
	};

	void Reset()
	{
// 		if (DP_List != NULL)
// 		{
// 			delete[]DP_List;
// 			DP_List = NULL;
// 		}
// 
// 		DP_List = new stDefectPixel[MAX_PARTICLE_COUNT];

		for (int i = 0; i < MAX_PARTICLE_COUNT; i++)
			DP_List[i].Reset();

		DefectPixe_Cnt = 0;

	};

	_tag_DPList& operator= (_tag_DPList& ref)
	{
		//if (ref.DP_List != NULL)
		{
			for (int i = 0; i < MAX_PARTICLE_COUNT; i++)
				DP_List[i] = ref.DP_List[i];
		}
	

		DefectPixe_Cnt = ref.DefectPixe_Cnt;
	};

}stDPList;


//=============================================================================
// CTI_DefectPixel
//=============================================================================
class CTI_DefectPixel
{
public:
	CTI_DefectPixel();
	virtual ~CTI_DefectPixel();


	/*
	1. InputImage  ....... 16Bit
	2. Defect Pixel 구조체
	3. MODE : 현재 광원 조건 / 0 이면 OFF, 1이면 ON
	4. ~ Thr  : Hot, Dead Pixel의 종류를 구분하기위한 Threshold stDPList Type 변수 참고
	5. 단위 블럭의 크기 ex) 11 x 11 Pixel 블럭 => 11, 15 x 15 Pixel 블럭 => 15  .........  홀수로만 입력
	*/

	BOOL DPDetection(IN IplImage* pImageBuf, OUT stDPList& DPList, IN int MODE, IN int VHP_Th, IN int HP_Th, IN int VBP_Th, IN int BP_Th, IN int VDP_Th, IN int DP_Th, IN int RC_Th, IN int Max_Value, IN int Block_Size);

	double  GetDistance(__in int iSrcX, __in int iSrcY, __in int iDstX, __in int iDstY);

};

#endif // TI_DefectPixel_h__
