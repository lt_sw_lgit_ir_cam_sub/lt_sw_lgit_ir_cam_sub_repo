﻿//*****************************************************************************
// Filename	: 	TI_SNR.h
// Created	:	2018/2/10 - 17:14
// Modified	:	2018/2/10 - 17:14
//
// Author	:	Luritech
//	
// Purpose	:	
//****************************************************************************
#ifndef TI_SNR_h__
#define TI_SNR_h__

#pragma once

#include <windows.h>
#include "Def_Algorithm_Common.h"

//-----------------------------------------------------------------------------
// SNR
//-----------------------------------------------------------------------------

#define MAX_SNR_ROI_Count		10

//=============================================================================
// CTI_SNR
//=============================================================================
class CTI_SNR
{
public:
	CTI_SNR();
	virtual ~CTI_SNR();

protected:


public:

	//=============================================================================
	// Method		: SNR_Test
	// Access		: public  
	// Returns		: int
	// Parameter	: IN LPWORD pImageBuf
	// Parameter	: IN UINT dwWidth
	// Parameter	: IN UINT dwHeight
	// Parameter	: IN ST_ROI stROI
	// Parameter	: OUT WORD & wSignal
	// Parameter	: OUT float & fNoise
	// Qualifier	:
	// Last Update	: 2018/2/10 - 18:13
	// Desc.		:
	//=============================================================================
	int SNR_Test(IN LPWORD pImageBuf, IN UINT dwWidth, IN UINT dwHeight, IN ST_ROI stROI, OUT float& wSignal, OUT float& fNoise);
	int SNR_Test(IN LPWORD pImageBuf, IN UINT dwWidth, IN UINT dwHeight, IN ST_ROI stROI, OUT short& wSignal, OUT float& fNoise);

};

#endif // TI_SNR_h__
