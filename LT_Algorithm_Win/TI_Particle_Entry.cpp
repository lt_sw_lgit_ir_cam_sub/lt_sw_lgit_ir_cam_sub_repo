﻿// *****************************************************************************
//  Filename	: 	TI_Particle_Entry.cpp
//  Created	:	2018/2/11 - 16:17
//  Modified	:	2018/2/11 - 16:17
// 
//  Author	:	Luritech
// 	
//  Purpose	:	
// ****************************************************************************
#include "stdafx.h"
#include "TI_Particle_Entry.h"


CTI_Particle_Entry::CTI_Particle_Entry()
{
	
}

CTI_Particle_Entry::~CTI_Particle_Entry()
{

}

//=============================================================================
// Method		: GetDistance
// Access		: protected  
// Returns		: double
// Parameter	: int x1
// Parameter	: int y1
// Parameter	: int x2
// Parameter	: int y2
// Qualifier	:
// Last Update	: 2018/2/11 - 16:18
// Desc.		:
//=============================================================================
double CTI_Particle_Entry::GetDistance(int x1, int y1, int x2, int y2)
{
	double result;

	result = sqrt((double)((x2 - x1)*(x2 - x1)) + ((y2 - y1)*(y2 - y1)));

	return result;
}

//=============================================================================
// Method		: EllipseDistanceSum
// Access		: protected  
// Returns		: double
// Parameter	: int XC
// Parameter	: int YC
// Parameter	: double A
// Parameter	: double B
// Parameter	: int X1
// Parameter	: int Y1
// Qualifier	:
// Last Update	: 2018/2/11 - 16:18
// Desc.		:
//=============================================================================
double CTI_Particle_Entry::EllipseDistanceSum(int XC, int YC, double A, double B, int X1, int Y1)
{
	double DistX, DistY, x, y;

	x = (double)(X1 - XC);
	y = (double)(Y1 - YC);
	DistX = (x*x) / (double)((A*A));
	DistY = (y*y) / (double)((B*B));

	double Dist = DistX + DistY;

	return Dist;
}

//=============================================================================
// Method		: SetTestArea
// Access		: public  
// Returns		: void
// Parameter	: IN UINT dwWidth
// Parameter	: IN UINT dwHeight
// Parameter	: IN ST_ROI stROI_Side
// Parameter	: IN ST_ROI stROI_Middle
// Parameter	: IN ST_ROI stROI_Center
// Parameter	: IN bool bEllipse_Side
// Parameter	: IN bool bEllipse_Middle
// Parameter	: IN bool bEllipse_Center
// Parameter	: OUT char * * ppchOUT_Area
// Qualifier	:
// Last Update	: 2018/2/11 - 16:36
// Desc.		:
//=============================================================================
void CTI_Particle_Entry::SetTestArea(IN UINT dwWidth, IN UINT dwHeight, IN ST_ROI stROI_Side, IN ST_ROI stROI_Middle, IN ST_ROI stROI_Center, IN bool bEllipse_Side, IN bool bEllipse_Middle, IN bool bEllipse_Center, OUT char** ppchOUT_Area)
{
	IplImage *pFieldImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);

	//최초 배경 
	for (UINT y = 0; y < dwHeight; y++)
	{
		for (UINT x = 0; x < dwWidth; x++)
		{
			ppchOUT_Area[y][x] = 0;
			pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 0] = (char)255;
			pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 1] = (char)255;
			pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 2] = (char)255;
		}
	}

	UINT Startx = stROI_Side.sLeft;
	UINT Endx = stROI_Side.sLeft + stROI_Side.sWidth;
	UINT Starty = stROI_Side.sTop;
	UINT Endy = stROI_Side.sTop + stROI_Side.sHeight;

	// Side
	for (UINT y = Starty; y < Endy; y++)
	{
		for (UINT x = Startx; x < Endx; x++)
		{
			if (bEllipse_Side == TRUE)
			{
				if (EllipseDistanceSum(stROI_Side.sLeft + stROI_Side.sWidth / 2, stROI_Side.sTop + stROI_Side.sHeight / 2, ((double)stROI_Side.sWidth / 2.0), ((double)stROI_Side.sHeight / 2.0), x, y) <= 1)
				{
					ppchOUT_Area[y][x] = 3;
					pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 0] = (char)0;
					pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 1] = (char)0;
					pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 2] = (char)255;
				}
			}
			else
			{
				ppchOUT_Area[y][x] = 3;
				pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 0] = (char)0;
				pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 1] = (char)0;
				pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 2] = (char)255;
			}
		}
	}

	Startx = stROI_Middle.sLeft;
	Endx = stROI_Middle.sLeft + stROI_Middle.sWidth;
	Starty = stROI_Middle.sTop;
	Endy = stROI_Middle.sTop + stROI_Middle.sHeight;

	// Middle
	for (UINT y = Starty; y < Endy; y++)
	{
		for (UINT x = Startx; x < Endx; x++)
		{
			if (bEllipse_Middle == TRUE)
			{
				if (EllipseDistanceSum(stROI_Middle.sLeft + stROI_Middle.sWidth / 2, stROI_Middle.sTop + stROI_Middle.sHeight / 2, ((double)stROI_Middle.sWidth / 2.0), ((double)stROI_Middle.sHeight / 2.0), x, y) <= 1)
				{
					ppchOUT_Area[y][x] = 2;
					pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 0] = (char)0;
					pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 1] = (char)255;
					pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 2] = (char)0;
				}
			}
			else
			{
				ppchOUT_Area[y][x] = 2;
				pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 0] = (char)0;
				pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 1] = (char)255;
				pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 2] = (char)0;
			}
		}
	}

	Startx = stROI_Center.sLeft;
	Endx = stROI_Center.sLeft + stROI_Center.sWidth;
	Starty = stROI_Center.sTop;
	Endy = stROI_Center.sTop + stROI_Center.sHeight;

	// Center
	for (UINT y = Starty; y < Endy; y++)
	{
		for (UINT x = Startx; x < Endx; x++)
		{
			if (bEllipse_Center == TRUE)
			{
				if (EllipseDistanceSum(stROI_Center.sLeft + stROI_Center.sWidth / 2, stROI_Center.sTop + stROI_Center.sHeight / 2, ((double)stROI_Center.sWidth / 2.0), ((double)stROI_Center.sHeight / 2.0), x, y) <= 1)
				{
					ppchOUT_Area[y][x] = 1;
					pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 0] = (char)255;
					pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 1] = (char)0;
					pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 2] = (char)0;
				}
			}
			else
			{
				ppchOUT_Area[y][x] = 1;
				pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 0] = (char)255;
				pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 1] = (char)0;
				pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 2] = (char)0;
			}
		}
	}






	cvReleaseImage(&pFieldImage);
}

//=============================================================================
// Method		: Lump_Detection
// Access		: public  
// Returns		: void
// Parameter	: IN LPWORD pImageBuf
// Parameter	: IN UINT dwWidth
// Parameter	: IN UINT dwHeight
// Parameter	: IN float fSensitivity
// Parameter	: IN float * fThreSize
// Parameter	: OUT int & iOUT_Counter
// Parameter	: OUT CvRect * prtOUT_Contour
// Parameter	: OUT char * * ppchOUT_Area
// Qualifier	:
// Last Update	: 2018/2/12 - 11:56
// Desc.		:
//=============================================================================
void CTI_Particle_Entry::Lump_Detection(IN LPWORD pImageBuf, IN UINT dwWidth, IN UINT dwHeight, IN float fSensitivity, IN float* fThreSize, OUT int& iOUT_Counter, /*OUT CvRect* prtOUT_Contour,*/ OUT char** ppchOUT_Area)
{
// 	float fThreSize[ROI_Pr_Max] = {0,};
// 	fThreSize[ROI_Pr_Side]		= fThreSize_Side;
// 	fThreSize[ROI_Pr_Middle]	= fThreSize_Middle;
// 	fThreSize[ROI_Pr_Center]	= fThreSize_Center;

	IplImage *OriginImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 1);
	IplImage *RGBResultImage = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);

	WORD wData16Bit = 0;
	BYTE byData8Bit = 0;

	for (int y = 0; y < dwHeight; y++)
	{
		for (int x = 0; x < dwWidth; x++)
		{
			wData16Bit = *(pImageBuf + y * dwWidth + x);

			wData16Bit = wData16Bit & 0xF000 ? 0x0FFF : wData16Bit;
			byData8Bit = (wData16Bit >> 4) & 0x00FF;

			//if (byBrightness >= 0)
			//	byData8Bit = (byData8Bit << byBrightness) >= 0xFF ? 0xFF : (byData8Bit << byBrightness);
			//else
			//	byData8Bit = (byData8Bit >> (-1 * byBrightness)) <= 0x00 ? 0x00 : (byData8Bit >> (-1 * byBrightness));

			OriginImage->imageData[y * OriginImage->widthStep + x] = byData8Bit;
		}
	}

	cvAdaptiveThreshold(OriginImage, DilateImage, 255, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 51/*31:홀수*/, fSensitivity/*2*/);
	cvNot(DilateImage, DilateImage);

	cvErode(DilateImage, DilateImage);
	cvDilate(DilateImage, DilateImage);

	cvCvtColor(DilateImage, RGBResultImage, CV_GRAY2BGR);

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;

	cvFindContours(DilateImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	temp_contour = contour;


	for (; temp_contour != 0; temp_contour = temp_contour->h_next)
	{
		iOUT_Counter++;
	}

	//prtOUT_Contour = new CvRect[iOUT_Counter];

	double area = 0, arcCount = 0;
	iOUT_Counter = 0;
	double old_dist = 999999;

	CvRect	rect;
	POINT	ptCurrPt;

	for (; contour != 0; contour = contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		// 기존엔 2번 호출되고 있었음
		//rect = cvContourBoundingRect(contour, 1);
		rect = cvContourBoundingRect(contour, 1);

		int center_x, center_y;
		center_x = rect.x + rect.width / 2;
		center_y = rect.y + rect.height / 2;
		cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(255, 0, 0), 1, 8);

		BOOL Rate_Part = FALSE;
		double  Rate = 0;

		if (rect.width > rect.height)
			Rate = (double)rect.width / rect.height;
		else
			Rate = (double)rect.height / rect.width;

		if (Rate >= 9)
			Rate_Part = TRUE;

		//Size -> 
		if (rect.width < (dwWidth / 4) && rect.height < (dwHeight / 4))
		{
			int Center_X = rect.x + rect.width / 2;
			int Center_Y = rect.y + rect.height / 2;

			for (int i = 0; i < ROI_Pr_Entry_Max; i++)
			{
				// [Input] 멍 크기
				if (rect.width > fThreSize[i] && rect.height > fThreSize[i] && ppchOUT_Area[Center_Y][Center_X] == i + 1 && Rate_Part == FALSE && iOUT_Counter < MAX_Particle_Entry_ResultROI_Count)
				{
					m_rtParticle[iOUT_Counter] = rect;

					iOUT_Counter++;

					double dist = GetDistance(center_x, center_y, dwWidth / 2, dwHeight / 2);

					if (old_dist > dist)
					{
						ptCurrPt.x = center_x - (dwWidth / 2);
						ptCurrPt.y = center_y - (dwHeight / 2);

						ptCurrPt.x = ptCurrPt.x / 2;
						ptCurrPt.y = ptCurrPt.y / 2;

						old_dist = dist;
					}

					cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(0, 255, 0), 1, 8);
				}
			}
		}
	}



	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&OriginImage);
	cvReleaseImage(&DilateImage);
	cvReleaseImage(&RGBResultImage);
}

//=============================================================================
// Method		: ParticleCluster
// Access		: public  
// Returns		: void
// Parameter	: IN int nParticleCount
// Parameter	: IN ST_ROI * pROI
// Parameter	: IN float * pConcentration
// Parameter	: OUT bool * pbOUT_Judge
// Parameter	: OUT LPBYTE lpbyOUT_ParticleType
// Qualifier	:
// Last Update	: 2018/2/12 - 12:29
// Desc.		:
//=============================================================================
void CTI_Particle_Entry::ParticleCluster(IN int nParticleCount, IN ST_ROI* pROI, IN float* pConcentration, OUT bool* pbOUT_Judge, OUT LPBYTE lpbyOUT_ParticleType)
{
	int		nArea = 0;
	double dbStainThresold = 20.0;
	double dbDeadPixelThresold = 70.0;

	// 타입 별 갯수 - 전체 분류용
	int nNumOfEveryCluster[Pr_Type_Entry_Max] = { 0, };
	// 타입 종류
	int* nType = new int[nParticleCount];

	for (int i = 0; i < nParticleCount; i++)
	{
		// 초기화
		nType[i] = 0;

		// 예외처리
		if (pConcentration[i] > 100)
			pConcentration[i] = 100;
		else if (pConcentration[i] < 0)
			pConcentration[i] = 0;

		nArea = pROI[i].sWidth * pROI[i].sHeight;

		// Blemish
		nType[i] = Pr_Type_Entry_Blemish;

		// Stain
		if (pConcentration[i] > dbStainThresold)
		{
			nType[i] = Pr_Type_Entry_Stain;
			nNumOfEveryCluster[Pr_Type_Entry_Stain]++;
		}
	}

	nNumOfEveryCluster[Pr_Type_Entry_Blemish] = nParticleCount - nNumOfEveryCluster[Pr_Type_Entry_Stain];

	if (nNumOfEveryCluster[Pr_Type_Entry_Blemish] < 0)
		nNumOfEveryCluster[Pr_Type_Entry_Blemish] = 0;

	// 타입 별 갯수 - 갯수 제한용
	int nTypeCount[Pr_Type_Entry_Max] = { 0, };

	// 타입 별 최대 갯수 제한
	int nCount = 0;
	for (int i = 0; i < nParticleCount; i++)
	{
		if (nType[i] == Pr_Type_Entry_Stain && nTypeCount[Pr_Type_Entry_Stain]++ >= m_nMax_ParticleCount)
			continue;
		if (nType[i] == Pr_Type_Entry_Blemish && nTypeCount[Pr_Type_Entry_Blemish]++ >= m_nMax_ParticleCount)
			continue;

		pbOUT_Judge[i] = false;
		lpbyOUT_ParticleType[nCount++] = nType[i];
	}

	delete[] nType;
}

//=============================================================================
// Method		: Particle_Test
// Access		: public  
// Returns		: void
// Parameter	: IN LPWORD pImageBuf
// Parameter	: IN UINT dwWidth
// Parameter	: IN UINT dwHeight
// Parameter	: IN ST_CAN_Particle_Opt ParticleOpt
// Parameter	: IN ST_CAN_Particle_Result & ParticleResult
// Qualifier	:
// Last Update	: 2018/2/12 - 10:20
// Desc.		:
//=============================================================================
void CTI_Particle_Entry::Particle_Test(IN LPWORD pImageBuf, IN UINT dwWidth, IN UINT dwHeight, IN ST_CAN_Particle_Entry_Opt ParticleOpt, OUT ST_CAN_Particle_Entry_Result &ParticleResult)
{
	char** ppchArea = nullptr;

	if ((nullptr == ppchArea) && (0 != dwWidth * dwHeight))
	{
		ppchArea = new char*[dwHeight];
		ppchArea[0] = new char[dwWidth * dwHeight];
		for (DWORD dwIdx = 1; dwIdx < dwHeight; dwIdx++)
		{
			ppchArea[dwIdx] = ppchArea[dwIdx - 1] + dwWidth;
		}
	}

	float** pfData			= NULL;
	ST_ROI* pstROI			= NULL;
	float*	pfConcentration	= NULL;
	int		nGroup			= 0;
	long	nCount			= 0;
	int		nResultCount	= 0;
	int		iCount			= 0;

	// 검사 영역 구분
	SetTestArea(dwWidth, dwHeight, ParticleOpt.stROI[ROI_Pr_Entry_Side], ParticleOpt.stROI[ROI_Pr_Entry_Middle], ParticleOpt.stROI[ROI_Pr_Entry_Center], ParticleOpt.bEllipse[ROI_Pr_Entry_Side], ParticleOpt.bEllipse[ROI_Pr_Entry_Middle], ParticleOpt.bEllipse[ROI_Pr_Entry_Center], ppchArea);

	// 이물 검출
	//Lump_Detection(pImageBuf, dwWidth, dwHeight, ParticleOpt.fSensitivity, ParticleOpt.fThreSize, iCount, prtContour, ppchArea);
	Lump_Detection(pImageBuf, dwWidth, dwHeight, ParticleOpt.fSensitivity, ParticleOpt.fThreSize, iCount, ppchArea); // prtContour => m_rtParticle 전역 변수로 바꿈

	// 선언
	pstROI = new ST_ROI[iCount];
	pfConcentration = new float[iCount];

	for (int i = 0; i < iCount; i++)
	{
		pstROI[i].sLeft		= m_rtParticle[i].x;
		pstROI[i].sTop		= m_rtParticle[i].y;
		pstROI[i].sWidth	= m_rtParticle[i].width;
		pstROI[i].sHeight	= m_rtParticle[i].height;

		nCount++;
	}
	
	pfData = new float*[dwHeight];

	for (int i = 0; i < dwHeight; i++)
	{
		pfData[i] = new float[dwWidth];
	}

	for (int lopy = 0; lopy < dwHeight; lopy++)
	{
		for (int lopx = 0; lopx < dwWidth; lopx++)
		{
			pfData[lopy][lopx] = (float)pImageBuf[lopy * dwWidth + lopx];
		}
	}

	///신규 농도 추가 1005
	nResultCount = nGroup = iCount;

	int Mid_x = 0;
	int Mid_y = 0;

	for (int lop = 0; lop < nGroup; lop++)
	{
		float centerSum = 0; int centerCount = 0; double center;
		float side1_Sum = 0; int side1_Count = 0; double side;
		float side2_Sum = 0; int side2_Count = 0;

		int size_x = pstROI[lop].sWidth;
		int size_y = pstROI[lop].sHeight;

		for (int y = pstROI[lop].sTop - size_y; y <= pstROI[lop].sTop + pstROI[lop].sHeight + size_y; y++)
		{
			for (int x = pstROI[lop].sLeft - size_x; x <= pstROI[lop].sLeft + pstROI[lop].sWidth + size_x; x++)
			{
				if (y > 0 && y < dwHeight && x > 0 && x < dwWidth)
				{
					side1_Sum += pfData[y][x];
					side1_Count++;
				}
			}
		}

		for (int y = pstROI[lop].sTop; y <= pstROI[lop].sTop + pstROI[lop].sHeight; y++)
		{
			for (int x = pstROI[lop].sLeft; x <= pstROI[lop].sLeft + pstROI[lop].sWidth; x++)
			{
				if (y > 0 && y < dwHeight && x > 0 && x < dwWidth)
				{
					side2_Sum += pfData[y][x];
					side2_Count++;
				}
			}
		}

		for (int y = pstROI[lop].sTop; y <= pstROI[lop].sTop + pstROI[lop].sHeight; y++)
		{
			for (int x = pstROI[lop].sLeft; x <= pstROI[lop].sLeft + pstROI[lop].sWidth; x++)
			{
				if (y > 0 && y < dwHeight && x > 0 && x < dwWidth)
				{
					centerSum += pfData[y][x];
					centerCount++;
				}
			}
		}

		center = (double)centerSum / (double)centerCount;
		side = (double)(side1_Sum - side2_Sum) / (double)(side1_Count - side2_Count);

		pfConcentration[lop] = (float)(100 - (center / side) * 100);

		if (pfConcentration[lop] > 100)
			pfConcentration[lop] = 100;
		else if (pfConcentration[lop] < 0)
			pfConcentration[lop] = 0;
	}

	for (UINT nSize = 0; nSize < dwHeight; nSize++)
		free(pfData[nSize]);

	free(pfData);

	bool bBreakCheck = false;

	// [Input] 멍 농도
	for (int lop = 0; lop < nGroup; lop++)
	{
		for (int lop3 = 0; lop3 < nResultCount; lop3++)
		{
			int Center_X = pstROI[lop3].sLeft + (pstROI[lop3].sWidth) / 2;
			int Center_Y = pstROI[lop3].sTop + (pstROI[lop3].sHeight) / 2;

			bBreakCheck = false;

			// 영역 별 체크
			for (int i = 0; i < ROI_Pr_Entry_Max; i++)
			{
				if (pfConcentration[lop3] <= ParticleOpt.fThreConcentration[i] && ppchArea[Center_X][Center_Y] == i + 1 && pfConcentration[lop3] >= 0)
				{
					for (int lop2 = lop3; lop2 < nResultCount; lop2++)
					{
						pstROI[lop2].sLeft = pstROI[lop2 + 1].sLeft;
						pstROI[lop2].sTop = pstROI[lop2 + 1].sTop;
						pstROI[lop2].sWidth = pstROI[lop2 + 1].sWidth;
						pstROI[lop2].sHeight = pstROI[lop2 + 1].sHeight;

						pfConcentration[lop2] = pfConcentration[lop2 + 1];
					}

					nResultCount--;
					bBreakCheck = true;
					break;
				}
			}

			if (bBreakCheck == true)
				break;
		}
	}

	//bool* bPass = new bool[nResultCount];
	//for (int i = 0; i < nResultCount; i++)
	//	bPass[i] = true;

	// 타입 분류
	//ParticleCluster	(nResultCount, pstROI, pfConcentration, bPass, ParticleResult.byType);

	// 최종 결과 카피
	for (int i = 0; i < nResultCount; i++)
	{
		/*if (bPass[i] == true)
			continue;*/

		ParticleResult.stROI[i] = pstROI[i];
		ParticleResult.fConcentration[i] = pfConcentration[i];

		ParticleResult.byRoiCnt++;
	}

	//delete[] bPass;
	delete[] pstROI;
	delete[] pfConcentration;


	if (nullptr != ppchArea)
	{
		delete[] ppchArea[0];
		delete[] ppchArea;

		ppchArea = nullptr;
	}

}
