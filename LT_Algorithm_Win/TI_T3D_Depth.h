#pragma once

#include "cv.h"
#include "highgui.h"

class CTI_T3D_Depth
{
public:
	CTI_T3D_Depth();
	virtual ~CTI_T3D_Depth();

	void	T3D_Depth_Test(__in LPWORD pImageBuf, __in int iWidth, __in int iHeight, __out double* dbAveData, __out double *dbstdevData);

protected:

//	WORD *m_wImageBuf;
};

