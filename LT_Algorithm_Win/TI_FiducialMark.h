﻿//*****************************************************************************
// Filename	: 	TI_FiducialMark.h
// Created	:	2018/2/11 - 15:45
// Modified	:	2018/2/11 - 15:45
//
// Author	:	Luritech
//	
// Purpose	:	
//****************************************************************************
#ifndef TI_FiducialMark_h__
#define TI_FiducialMark_h__

#pragma once

#include <windows.h>
#include "cv.h"
#include "highgui.h"
#include "Def_Algorithm_Common.h"

//-----------------------------------------------------------------------------
// FiducialMark
//-----------------------------------------------------------------------------
#define		MAX_FiducialMark_ROI_Count		11

//=============================================================================
// CTI_FiducialMark
//=============================================================================
class CTI_FiducialMark
{
public:
	CTI_FiducialMark();
	virtual ~CTI_FiducialMark();

protected:

public:

	//=============================================================================
	// Method		: FiducialMark_Test
	// Access		: public  
	// Returns		: int
	// Parameter	: IN LPWORD pImageBuf
	// Parameter	: IN UINT dwWidth
	// Parameter	: IN UINT dwHeight
	// Parameter	: IN ST_ROI stROI
	// Parameter	: IN BYTE byMarkColor
	// Parameter	: IN BYTE byBrightness
	// Parameter	: OUT POINT & ptOUT_Center
	// Qualifier	:
	// Last Update	: 2018/2/11 - 15:45
	// Desc.		:
	//=============================================================================
	int		FiducialMark_Test	(IN LPWORD pImageBuf, IN UINT dwWidth, IN UINT dwHeight, IN ST_ROI stROI, IN BYTE byMarkColor, IN BYTE byBrightness, OUT POINT& ptOUT_Center);

	double  GetDistance(__in int iSrcX, __in int iSrcY, __in int iDstX, __in int iDstY)
	{
		return sqrt((double)((iDstX - iSrcX) * (iDstX - iSrcX) + (iDstY - iSrcY) * (iDstY - iSrcY)));
	};
};

#endif // TI_FiducialMark_h__
