// #include "stdafx.h"
#include "VA_HotPixel.h"


CVA_HotPixel::CVA_HotPixel()
{
}


CVA_HotPixel::~CVA_HotPixel()
{
}


//=============================================================================
// Method		: Detect_HotPixel
// Access		: public  
// Returns		: void
// Parameter	: __in char * pInImage
// Parameter	: __in unsigned long dwImageSize
// Parameter	: __in unsigned int iWidth
// Parameter	: __in unsigned int iHeight
// Parameter	: __in float fTreshold
// Parameter	: __out ST_HotPixelInfo & stOutResult
// Qualifier	:
// Last Update	: 2017/12/19 - 15:15
// Desc.		:
//=============================================================================
void CVA_HotPixel::Detect_HotPixel(__in const char* pInImage, __in unsigned long dwImageSize, __in unsigned int iWidth, __in unsigned int iHeight, __in float fTreshold, __out ST_HotPixelInfo& stOutResult)
{
	stOutResult.byDetectCount;
	stOutResult.stDataz[0].wPosX;
	stOutResult.stDataz[0].wPosY;
	stOutResult.stDataz[0].fConcentration;

	unsigned short** pDetectImage	= NULL;
	unsigned short** iIntegral		= NULL;

	if (NULL == pDetectImage)
	{
		pDetectImage = new unsigned short*[iHeight];
		pDetectImage[0] = new unsigned short[iWidth * iHeight];
		for (DWORD dwIdx = 1; dwIdx < iHeight; dwIdx++)
		{
			pDetectImage[dwIdx] = pDetectImage[dwIdx - 1] + iWidth;
		}
	}

	RECT rectROI;	// 6 x 6
	rectROI.left;
	rectROI.top;
	rectROI.right;
	rectROI.bottom;






	if (NULL != pDetectImage)
	{
		delete[] pDetectImage[0];
		delete[] pDetectImage;

		pDetectImage = NULL;
	}
}
