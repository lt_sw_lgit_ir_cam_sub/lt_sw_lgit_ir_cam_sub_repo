﻿//*****************************************************************************
// Filename	: 	TI_SNR_Light.h
// Created	:	2018/2/11 - 15:15
// Modified	:	2018/2/11 - 15:15
//
// Author	:	Luritech
//	
// Purpose	:	
//****************************************************************************
#ifndef TI_SNR_Light_h__
#define TI_SNR_Light_h__

#pragma once

#include <windows.h>
#include "Def_Algorithm_Common.h"

//-----------------------------------------------------------------------------
// SNR Particle
//-----------------------------------------------------------------------------

#define MAX_SNRLight_ROI_Count		56	// Max는 56개 고정

//=============================================================================
// CTI_SNR_Light
//=============================================================================
class CTI_SNR_Light
{
public:
	CTI_SNR_Light();
	virtual ~CTI_SNR_Light();

protected:

	void	MakeSection		(IN unsigned int dwWidth, IN unsigned int dwHeight, OUT ST_ROI* pstOUT_ROI, IN unsigned int nSectionCnt_X = SECTION_NUM_X, IN unsigned int nSectionCnt_Y = SECTION_NUM_Y);
	UINT	SetROI			(IN unsigned int dwWidth, IN unsigned int dwHeight, IN unsigned int nROI_Count, IN const bool* pbIN_ROI_UseFlag, OUT ST_ROI* pstOUT_ROI, IN unsigned int nSectionCnt_X = SECTION_NUM_X, IN unsigned int nSectionCnt_Y = SECTION_NUM_Y);		

	UINT	m_nSectionCount_X			= SECTION_NUM_X;
	UINT	m_nSectionCount_Y			= SECTION_NUM_Y;
	UINT	m_nSNR_Light_MaxCount		= MAX_SNRLight_ROI_Count;

	void	SetROI			(ST_ROI* stROI, bool* bUseIndex);
	void	MakeSection		(ST_ROI* pROI);

public:

	void	Set_SectionCount(IN unsigned int nSectionCnt_X, IN unsigned int nSectionCnt_Y)
	{
		m_nSectionCount_X = nSectionCnt_X;
		m_nSectionCount_Y = nSectionCnt_Y;
	};

	void	Set_SNR_Light_MaxCount(IN unsigned int nIn_SNR_LightCount)
	{
		m_nSNR_Light_MaxCount = nIn_SNR_LightCount;
	};

	int		SNR_Light_Test	(IN unsigned short* pImageBuf, IN int dwWidth, IN int dwHeight, IN int nIN_ROI_Count, IN const bool* pbIN_ROI_UseFlag, OUT short* arwOUT_Signal, OUT float* arfOUT_Noise);

	int SNR_Light_Test_New(IN unsigned short* pImageBuf, IN int dwWidth, IN int dwHeight, IN const CRect* pTestRect, IN const bool* pbIN_ROI_UseFlag, OUT short* arwOUT_Signal, OUT float* arfOUT_Noise);
};

#endif // TI_SNR_Light_h__
