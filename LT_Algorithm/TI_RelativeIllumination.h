#pragma once
#include "TI_BaseAlgorithm.h"

class CTI_RelativeIllumination : CTI_BaseAlgorithm
{
private:
	// 계산 결과값 저장
	int			m_iSignal[RelativeIllumination_Idx_Max];
	double		m_dbNoise[RelativeIllumination_Idx_Max];

	ST_CAN_ROI		m_stROI[RelativeIllumination_Idx_Max];

	// Input 데이터
	ST_CAN_RelativeIllumination_Opt		m_stData;

	// Output 데이터
	ST_CAN_RelativeIllumination_Result	m_stResult;

private:
	void MakeSection(ST_CAN_ROI* pROI);
	void SetROI(bool* pUseIndex);

	void SignalnNoiseGen(ST_CAN_ROI stROI, unsigned short &sSignal, float& fNoise);

public:
	virtual void						Initialize(WORD* pGRAYScanBuf, int iWidth, int iHeight);
	ST_CAN_RelativeIllumination_Result	Inspection(ST_CAN_RelativeIllumination_Opt stData);
};
