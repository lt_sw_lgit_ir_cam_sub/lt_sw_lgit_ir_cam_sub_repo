#pragma once
#include "TI_BaseAlgorithm.h"

class CTI_FPN : CTI_BaseAlgorithm
{
private:
	// Input 데이터
	ST_CAN_FPN_Opt		m_stData;

	// Output 데이터
	ST_CAN_FPN_Result	m_stResult;

private:
	
public:
	virtual void		Initialize(WORD* pGRAYScanBuf, int iWidth, int iHeight);
	ST_CAN_FPN_Result	Inspection(ST_CAN_FPN_Opt stData);

	void	FPNGen(float fThresholdX, float fThresholdY, unsigned short& sFailCountX, unsigned short& sFailCountY);
};
