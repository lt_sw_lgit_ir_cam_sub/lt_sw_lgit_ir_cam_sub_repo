#include "LT_EOL_Func.h"

#include "LT_Algorithm.h"

#ifndef CAM_IMAGE_WIDTH
#define CAM_IMAGE_WIDTH		640
#endif

#ifndef CAM_IMAGE_HEIGHT
#define CAM_IMAGE_HEIGHT	480
#endif
#include <fstream>
LT_ERROR EOL_Algorithm::Initialize(int nWidth, int nHeight)
{
#ifdef DEBUG_PRINT_LOG
	printf("[Start] EOL_Algorithm::Initialize() : %d, %d\n", nWidth, nHeight);
#endif
	// 결과값
	LT_ERROR result = RC_Unknown_Code;

	// Default Init
	m_nWidth = 0;
	m_nHeight = 0;

	// 이미지 Width 값이 다름
	if (nWidth != CAM_IMAGE_WIDTH)
		result = RC_ImageSizeWidth_Wrong;
	// 이미지 Height 값이 다름
	else if (nHeight != CAM_IMAGE_HEIGHT)
		result = RC_ImageSizeHeight_Wrong;
	else
	{
		m_nWidth = nWidth;
		m_nHeight = nHeight;

		result = RC_OK;
	}

#ifdef DEBUG_PRINT_LOG
	printf("[End] EOL_Algorithm::Initialize() : %d\n", result);
#endif
	return result;
}

// pIR : IR camera capture data Memory pointer
// pDepth : Depth camera capture data Memory pointer
// pMSG : 검사 구분, 항목, ROI등을 포함한 CAN 을 통한 전송되는 MSG 전체
// resultLen : resultMSG length
// pResultMSG : 검사 결과 data
LT_ERROR EOL_Algorithm::Inspection(BYTE *pIR, BYTE *pDepth, BYTE *pMSG, int &resultLen, BYTE *pResultMSG)
{
#ifdef DEBUG_PRINT_LOG
	printf("[Start] EOL_Algorithm::Inspection() : \n    pIR : %d, pMSG : %d, resultLen : %d, pResultMSG : %d\n",
		pIR != NULL ? 1 : 0,
		pMSG != NULL ? 1 : 0,
		resultLen,
		pResultMSG != NULL ? 1 : 0);
#endif
	// 결과값
	LT_ERROR result = RC_Unknown_Code;

	// 검사 항목 종류
	ST_CAN_Header msgHeader;

	// 예외처리
	// 이미지 버퍼가 NULL
	if (pIR == NULL)
		result = RC_ImageBuf_NULL;
	// Input MSG 버퍼가 NULL
	else if (pMSG == NULL)
		result = RC_MsgBuf_NULL;
	// 결과 버퍼를 담을 메모리가 NULL
	else if (pResultMSG == NULL)
		result = RC_ResultBuf_NULL;
	// 이미지 Width 값이 다름
	else if (m_nWidth != CAM_IMAGE_WIDTH)
		result = RC_ImageSizeWidth_Wrong;
	// 이미지 Height 값이 다름
	else if (m_nHeight != CAM_IMAGE_HEIGHT)
		result = RC_ImageSizeHeight_Wrong;
	else
	{
		// 전체 메시지에서 검사 항목 종류 데이터만 복사
		memcpy(&msgHeader, pMSG, sizeof(ST_CAN_Header));

#ifdef DEBUG_PRINT_LOG

		/// 저장 
		FILE* pFile;
		pFile = fopen("./CaputreImage.raw", "wb");
		if (pFile == NULL)
			printf("Image Capture Fail\n");
		else
		{
			// 이물 이미지 관련 저장
			if (msgHeader.iTestItem == Inspection_Particle || msgHeader.iTestItem == Inspection_RelativeIllumination ||
				msgHeader.iTestItem == Inspection_SNR_Particle || msgHeader.iTestItem == Inspection_FPN ||
				msgHeader.iTestItem == Inspection_DefectPixel)
			{
				printf(" ** [pDepth] Image Capture OK\n");
				fwrite(pDepth, CAM_IMAGE_HEIGHT * CAM_IMAGE_WIDTH * 2, 1, pFile);
			}
			// 차트 이미지 관련 저장
			else
			{
				printf(" ** [pIR] Image Capture OK\n");
				fwrite(pIR, CAM_IMAGE_HEIGHT * CAM_IMAGE_WIDTH * 2, 1, pFile);
			}
			fclose(pFile);
		}

		printf("[Value] EOL_Algorithm::Inspection() : TestItem : %d\n", msgHeader.iTestItem);

		//  이물검사 시작 확인용 로그
		if (msgHeader.iTestItem == Inspection_Particle || msgHeader.iTestItem == Inspection_RelativeIllumination ||
			msgHeader.iTestItem == Inspection_SNR_Particle || msgHeader.iTestItem == Inspection_FPN ||
			msgHeader.iTestItem == Inspection_DefectPixel)
		{
			printf(" *****************************************************************************\n");
			printf(" ****************** Particle Part Test [Start] : TestItem(%d) ****************\n", msgHeader.iTestItem);
			printf(" *****************************************************************************\n");
		}
#endif
		// 알고리즘 객체 생성
		CLT_Algorithm* pAlg = NULL;

		// 알고리즘 선택
		if (msgHeader.iTestItem == Inspection_FiducialMark)
			pAlg = new CLT_FiducialMark();
		else if (msgHeader.iTestItem == Inspection_EdgeLine)
			pAlg = new CLT_EdgeLine();
		else if (msgHeader.iTestItem == Inspection_SNR)
			pAlg = new CLT_SNR();
		else if (msgHeader.iTestItem == Inspection_SFR)
			pAlg = new CLT_SFR();
		// 이물
		else if (msgHeader.iTestItem == Inspection_Particle)
			pAlg = new CLT_Particle();
		else if (msgHeader.iTestItem == Inspection_RelativeIllumination)
			pAlg = new CLT_RelativeIllumination();
		else if (msgHeader.iTestItem == Inspection_SNR_Particle)
			pAlg = new CLT_SNR_Particle();
		else if (msgHeader.iTestItem == Inspection_FPN)
			pAlg = new CLT_FPN();
		else if (msgHeader.iTestItem == Inspection_DefectPixel)
			pAlg = new CLT_DefectPixel();

		// 알고리즘 호출
		if (pAlg != NULL)
		{
			pAlg->Initialize(m_nWidth, m_nHeight);
			result = pAlg->Execute(pIR, pDepth, pMSG, resultLen, pResultMSG);

#ifdef DEBUG_PRINT_LOG
			
			if (msgHeader.iTestItem == Inspection_SFR)
			{
				ST_CAN_SFR_Result* resultRL = (ST_CAN_SFR_Result*)pResultMSG;
				for (int i = 0; i < resultRL->byRoiCnt; i++)
				{
					printf("[Value SFR] %3d )) Degree : %.2f // Cnt : %d\n",
						i, resultRL->fValue[i], resultRL->byRoiCnt);
				}
			}
			else if (msgHeader.iTestItem == Inspection_FiducialMark)
			{
				ST_CAN_FiducialMark_Result* resultRL = (ST_CAN_FiducialMark_Result*)pResultMSG;
				for (int i = 0; i < resultRL->byRoiCnt; i++)
				{
					printf("[Value FM] %3d )) X : %3d, Y : %3d // Cnt : %d\n",
						i, resultRL->sMarkCenterX[i], resultRL->sMarkCenterY[i], resultRL->byRoiCnt);
				}
			}
			else if (msgHeader.iTestItem == Inspection_EdgeLine)
			{
				ST_CAN_EdgeLine_Result* resultRL = (ST_CAN_EdgeLine_Result*)pResultMSG;
				for (int i = 0; i < resultRL->byRoiCnt; i++)
				{
					printf("[Value EL] %3d )) X : %3d, Y : %3d // Cnt : %d\n",
						i, resultRL->sLineCenterX[i], resultRL->sLineCenterX[i], resultRL->byRoiCnt);
				}
			}
			else if (msgHeader.iTestItem == Inspection_SNR)
			{
				ST_CAN_SNR_Result* resultRL = (ST_CAN_SNR_Result*)pResultMSG;
				for (int i = 0; i < resultRL->byRoiCnt; i++)
				{
					printf("[Value SNR] %3d )) Signal : %3d, Noise : %.2f // Cnt : %d\n",
						i, resultRL->sSignal[i], resultRL->fNoise[i], resultRL->byRoiCnt);
				}
			}
			else if (msgHeader.iTestItem == Inspection_Particle)
			{
				ST_CAN_Particle_Result* resultRL = (ST_CAN_Particle_Result*)pResultMSG;
				for (int i = 0; i < resultRL->byRoiCnt; i++)
				{
					printf("[Value Particle]] %3d )) ROI[%d](L_%d, T_%d, W_%d, H_%d), byType : %d, fConcentration : %f\n",
						i, i, resultRL->stROI[i].sLeft, resultRL->stROI[i].sTop, resultRL->stROI[i].sWidth,
						resultRL->stROI[i].sHeight, resultRL->byType[i], resultRL->fConcentration[i]);
				}
			}
			else if (msgHeader.iTestItem == Inspection_RelativeIllumination)
			{
				ST_CAN_RelativeIllumination_Result* resultRL = (ST_CAN_RelativeIllumination_Result*)pResultMSG;
				for (int i = 0; i < RelativeIllumination_Idx_Max; i++)
				{
					printf("[Value - RL] %3d )) Signal : %5d, Noise : %.2f\n",
						i, resultRL->sSignal[i], resultRL->fNoise[i]);
				}
			}
			else if (msgHeader.iTestItem == Inspection_SNR_Particle)
			{
				ST_CAN_SNR_Particle_Result* resultRL = (ST_CAN_SNR_Particle_Result*)pResultMSG;
				for (int iRoiCnt = 0; iRoiCnt < SNR_Particle_Idx_Max; iRoiCnt++)
				{
					printf("[Value - SNR_Particle] )) Signal[%d] %d, Noise[%d] : %.2f\n",
						iRoiCnt, resultRL->sSignal[iRoiCnt], iRoiCnt, resultRL->fNoise[iRoiCnt]);
				}
			}
			else if (msgHeader.iTestItem == Inspection_FPN)
			{
				ST_CAN_FPN_Result* resultRL = (ST_CAN_FPN_Result*)pResultMSG;
				printf("Value FPN] )) FailCount X : %d, FailCount Y : %d\n",
					resultRL->sFailCountX, resultRL->sFailCountY);
			}
			else if (msgHeader.iTestItem == Inspection_DefectPixel)
			{
				ST_CAN_DefectPixel_Result* resultRL = (ST_CAN_DefectPixel_Result*)pResultMSG;
				for (int i = 0; i < resultRL->byIndexCnt; i++)
				{
					printf("[Value DefectPixel] %3d )) [%d] X : %d, Y : %d, Conen : %.2f\n",
						i, i, resultRL->sPointX[i], resultRL->sPointY[i], resultRL->fConcentration[i]);
				}
			}

			//  이물검사 종료 확인용 로그 - 안찍히면 중간에 뻗은 것임
			if (msgHeader.iTestItem == Inspection_Particle || msgHeader.iTestItem == Inspection_RelativeIllumination ||
				msgHeader.iTestItem == Inspection_SNR_Particle || msgHeader.iTestItem == Inspection_FPN ||
				msgHeader.iTestItem == Inspection_DefectPixel)
			{
				printf(" *****************************************************************************\n");
				printf(" ****************** Particle Part Test [End] : TestItem(%d) ****************\n", msgHeader.iTestItem);
				printf(" *****************************************************************************\n");
			}
#endif

			SAFE_DELETE(pAlg);
		}
		else
		{
			result = RC_Algorithm_NoneSelect;
			printf("Log : Execute() Fail");
		}
	}

#ifdef DEBUG_PRINT_LOG
	printf("[End] EOL_Algorithm::Inspection() : resultLen : %d\n",
		resultLen);
#endif
	return result;
}

LT_ERROR EOL_Algorithm::Finalize()
{
	return RC_OK;
}
