//*****************************************************************************
// Filename	: Def_Communication.h
// Created	: 2017/10/14
// Modified	: 2012/10/17 - 14:41
//
// Author	: SSM
//	
// Purpose	: 
//*****************************************************************************
#ifndef Def_Communication_h__
#define Def_Communication_h__

//#define DEBUG_PRINT_LOG

// 알고리즘 종류

#pragma pack (push,1)
typedef enum enInspectionAlgoritm
{
	Inspection_Init = 0,				// 초기값
	Inspection_SFR,						// SFR
	Inspection_FiducialMark,
	Inspection_EdgeLine,
	Inspection_SNR,

	// 이물
	Inspection_Particle,				// 이물검사(Stain, Blemish, Dead Pixel)
	Inspection_RelativeIllumination,	// 광량비
	Inspection_SNR_Particle,			// 이물 SNR
	Inspection_FPN,						// FPN
	Inspection_DefectPixel,				// Hot Pixel

	Inspection_Max,
};

typedef enum enResultCode
{
	RC_Unknown_Code = 0,		// 코드 미설정
	RC_OK,						// 이상 없음

	RC_ImageBuf_NULL,			// 이미지 버퍼가 NULL
	RC_ImageSizeWidth_Wrong,	// 이미지 Width 값이 다름
	RC_ImageSizeHeight_Wrong,	// 이미지 Height 값이 다름

	RC_MsgBuf_NULL,				// Input MSG 버퍼가 NULL
	RC_Algorithm_NoneSelect,	// 알고리즘 항목 선택이 안됨
	RC_Item_Count_Zero,			// ROI나 검사 항목의 갯수 지정이 안됨
	RC_FindMark_Fail,			// 피두셜 마크를 찾지 못함

	RC_ResultBuf_NULL,			// 결과 버퍼를 담을 메모리가 NULL
};

// FiducialMark
typedef enum enFiducialMark_ROI
{
	Mark_ROI_Index_FST = 0,
	Mark_ROI_Index_LST = 10,
	Mark_ROI_Max,
};

typedef enum enFiducialMark_TYPE
{
	Mark_C_BLOCK = 0,
	Mark_C_WHITE,
};

// SNR 
typedef enum enSNR_ROI
{
	SNR_ROI_Index_FST = 0,
	SNR_ROI_Index_LST = 9,
	SNR_ROI_Max,
};

// Edge Line
typedef enum enEdgeLine_ROI
{
	Line_ROI_Index_FST = 0,
	Line_ROI_Index_LST = 9,
	Line_ROI_Max,
};

typedef enum enEdgeLind_TYPE
{
	Line_LR_WB,
	Line_LR_BW,
	Line_TB_WB,
	Line_TB_BW,
};

// SFR
typedef enum enSFR_ROI
{
	SFR_ROI_Index_FST = 0,
	SFR_ROI_Index_LST = 29,
	SFR_ROI_Max,
};

// DynamicRange
typedef enum enDynamic_ROI
{
	Dynamic_ROI_White = 0,
	Dynamic_ROI_Gray,
	Dynamic_ROI_Black,
	Dynamic_ROI_Max,
};

typedef enum enDynamic_INDEX
{
	Dynamic_IDX_1ST = 0,
	Dynamic_IDX_2ND,
	Dynamic_IDX_Max,
};

// 화질검사 SNR
typedef enum enIQSNR_Index
{
	IQSNR_First = 0,
	IQSNR_MaxEnum,
};

// Particle
typedef enum enParticle_ROI
{
	Particle_ROI_Image = 0,
	Particle_ROI_Side,
	Particle_ROI_Center,
	Particle_ROI_Max,
};

typedef enum enParticle_Idx_ROI
{
	Particle_Idx_FST = 0,
	Particle_Idx_LST = 59,
	Particle_Idx_Max,		// Max는 60개 고정
};

typedef enum enParticle_Type
{
	Particle_Type_None = -1,
	Particle_Type_Stain,
	Particle_Type_Blemish,
	Particle_Type_DeadPixel,
	Particle_Type_Max,
};

// Relative Illumination
typedef enum enRelativeIllumination_Idx_ROI
{
	RelativeIllumination_Idx_FST = 0,
	RelativeIllumination_Idx_LST = 72,
	RelativeIllumination_Idx_Max,		// Max는 73개 고정
};

// SNR_Particle
typedef enum enSNR_Particle_Idx_ROI
{
	SNR_Particle_Idx_FST = 0,
	SNR_Particle_Idx_LST = 55,
	SNR_Particle_Idx_Max,		// Max는 56개 고정
};

// 광량비, 이물 SNR의 영역 나누는 갯수
typedef enum enSectionNum
{
	SECTION_NUM_X = 21,
	SECTION_NUM_Y = 21,
};

// Hot Pixel
typedef enum enDefectPixel_Idx_ROI
{
	DefectPixel_Idx_FST = 0,
	DefectPixel_Idx_LST = 49,
	DefectPixel_Idx_Max,		// Max는 50개 고정
};

// Header
typedef struct _tag_CAN_Header
{
	// 알고리즘 종류(INSPECTION_ALGORITHM)
	unsigned char	iTestItem;

	_tag_CAN_Header()
	{
		iTestItem = Inspection_Init;
	};

}ST_CAN_Header;

// ROI
typedef struct _tag_CAN_ROI
{
	unsigned short	sLeft;
	unsigned short	sTop;
	unsigned short	sWidth;
	unsigned short	sHeight;

	_tag_CAN_ROI()
	{
		sLeft	= 0;
		sTop	= 0;
		sWidth	= 0;
		sHeight = 0;
	};

}ST_CAN_ROI;

//-----------------------------------------------------------------------------
// SFR, Opt : PC -> CAMERA , Result : CAMERA -> PC
//-----------------------------------------------------------------------------
typedef struct _tag_CAN_SFR_Opt
{
	ST_CAN_Header	stHeader;

	// 사용하는 ROI 수량
	BYTE byRoiCnt;

	ST_CAN_ROI	stROI[SFR_ROI_Max];

	float	fLinePair[SFR_ROI_Max];

	_tag_CAN_SFR_Opt()
	{
		stHeader.iTestItem = Inspection_SFR;

		for (int iIdx = 0; iIdx < SFR_ROI_Max; iIdx++)
		{
			fLinePair[iIdx] = 0.0f;
		}

		byRoiCnt = 0;
	};

}ST_CAN_SFR_Opt;

typedef struct _tag_CAN_SFR_Result
{
	ST_CAN_Header	stHeader;
	WORD			wResultCode;

	// 사용하는 ROI 수량
	BYTE byRoiCnt;

	float fValue[SFR_ROI_Max];

	_tag_CAN_SFR_Result()
	{
		stHeader.iTestItem = Inspection_SFR;
		wResultCode = RC_Unknown_Code;

		for (int iIdx = 0; iIdx < SFR_ROI_Max; iIdx++)
		{
			fValue[iIdx] = 0.0f;
		}

		byRoiCnt = 0;
	};

}ST_CAN_SFR_Result;

//-----------------------------------------------------------------------------
// FiducialMark, Opt : PC -> CAMERA , Result : CAMERA -> PC
//-----------------------------------------------------------------------------
typedef struct _tag_CAN_FiducialMark_Opt
{
	ST_CAN_Header	stHeader;

	BYTE			byRoiCnt;

	ST_CAN_ROI		stROI[Mark_ROI_Max];

	BYTE			byMarkColor[Mark_ROI_Max];
	BYTE			byBrightness[Mark_ROI_Max];

	_tag_CAN_FiducialMark_Opt()
	{
		stHeader.iTestItem = Inspection_FiducialMark;

		byRoiCnt = 0;

		for (int iIdx = 0; iIdx < Mark_ROI_Max; iIdx++)
		{
			byMarkColor[iIdx] = Mark_C_BLOCK;
			byBrightness[iIdx] = 0;
		}
	};

}ST_CAN_FiducialMark_Opt;

typedef struct _tag_CAN_FiducialMark_Result
{
	ST_CAN_Header	stHeader;
	WORD			wResultCode;

	BYTE			byRoiCnt;

	unsigned short  sMarkCenterX[Mark_ROI_Max];
	unsigned short  sMarkCenterY[Mark_ROI_Max];

	_tag_CAN_FiducialMark_Result()
	{
		stHeader.iTestItem = Inspection_FiducialMark;
		wResultCode = RC_Unknown_Code;

		for (int iIdx = 0; iIdx < Mark_ROI_Max; iIdx++)
		{
			sMarkCenterX[iIdx] = 0;
			sMarkCenterY[iIdx] = 0;
		}
	};

}ST_CAN_FiducialMark_Result;

//-----------------------------------------------------------------------------
// EdgeLine, Opt : PC -> CAMERA , Result : CAMERA -> PC
//-----------------------------------------------------------------------------
typedef struct _tag_CAN_EdgeLine_Opt
{
	ST_CAN_Header	stHeader;

	BYTE			byRoiCnt;

	ST_CAN_ROI		stROI[Line_ROI_Max];

	BYTE			byDetectColor[Line_ROI_Max];
	BYTE			byThreshold[Line_ROI_Max];
	BYTE			byBrightness[Line_ROI_Max];

	unsigned short  sCenterX[Line_ROI_Max];
	unsigned short  sCenterY[Line_ROI_Max];

	_tag_CAN_EdgeLine_Opt()
	{
		stHeader.iTestItem = Inspection_EdgeLine;

		byRoiCnt = 0;

		for (int iIdx = 0; iIdx < Line_ROI_Max; iIdx++)
		{
			sCenterX[iIdx] = 0;
			sCenterY[iIdx] = 0;
			byDetectColor[iIdx] = Line_LR_WB;
			byBrightness[iIdx] = 0;
			byThreshold[iIdx] = 0;
		}
	};

}ST_CAN_EdgeLine_Opt;

typedef struct _tag_CAN_EdgeLine_Result
{
	ST_CAN_Header	stHeader;
	WORD			wResultCode;

	BYTE			byRoiCnt;

	unsigned short  sLineCenterX[Line_ROI_Max];
	unsigned short  sLineCenterY[Line_ROI_Max];

	_tag_CAN_EdgeLine_Result()
	{
		stHeader.iTestItem = Inspection_EdgeLine;
		wResultCode = RC_Unknown_Code;

		for (int iIdx = 0; iIdx < Line_ROI_Max; iIdx++)
		{
			sLineCenterX[iIdx] = 0;
			sLineCenterY[iIdx] = 0;
		}
	};

}ST_CAN_EdgeLine_Result;

//-----------------------------------------------------------------------------
// SNR, Opt : PC -> CAMERA , Result : CAMERA -> PC
//-----------------------------------------------------------------------------
typedef struct _tag_CAN_SNR_Opt
{
	ST_CAN_Header	stHeader;

	BYTE			byRoiCnt;

	ST_CAN_ROI		stROI[SNR_ROI_Max];

	_tag_CAN_SNR_Opt()
	{
		stHeader.iTestItem = Inspection_SNR;
		byRoiCnt = 0;
	};

}ST_CAN_SNR_Opt;

typedef struct _tag_CAN_SNR_Result
{
	ST_CAN_Header	stHeader;
	WORD			wResultCode;

	BYTE			byRoiCnt;

	unsigned short	sSignal[SNR_ROI_Max];
	float			fNoise[SNR_ROI_Max];

	_tag_CAN_SNR_Result()
	{
		stHeader.iTestItem = Inspection_SNR;
		wResultCode = RC_Unknown_Code;

		for (int iIdx = 0; iIdx < SNR_ROI_Max; iIdx++)
		{
			sSignal[iIdx] = 0;
			fNoise[iIdx] = 0.0;
		}
	};

}ST_CAN_SNR_Result;

//-----------------------------------------------------------------------------
// Particle, Opt : PC -> CAMERA , Result : CAMERA -> PC
//-----------------------------------------------------------------------------
typedef struct _tag_CAN_Particle_Opt
{
	ST_CAN_Header stHeader;

	ST_CAN_ROI	stROI[Particle_ROI_Max];
	bool		bEllipse[Particle_ROI_Max];				// 영역 모양(사각 or 원)

	float		fThreDark[Particle_ROI_Max];			// 흑점 Threshold
	float		fThreConcentration[Particle_ROI_Max];	// 멍농도 Threshold
	float		fThreSize[Particle_ROI_Max];			// 멍크기 Threshold

	float		fSensitivity;							// 민감도

	_tag_CAN_Particle_Opt()
	{
		stHeader.iTestItem = Inspection_Particle;

		fSensitivity = 0.0f;

		for (int i = 0; i < Particle_ROI_Max; i++)
		{
			bEllipse[i] = true;

			fThreDark[i]			= 0.0f;
			fThreConcentration[i]	= 0.0f;
			fThreSize[i]			= 0.0f;
		}
	};

}ST_CAN_Particle_Opt;

typedef struct _tag_CAN_Particle_Result
{
	ST_CAN_Header	stHeader;
	WORD			wResultCode;

	BYTE			byRoiCnt;
	ST_CAN_ROI		stROI[Particle_Idx_Max];			// 발견한 오브젝트
	BYTE			byType[Particle_Idx_Max];			// 발견한 오브젝트의 타입(Stain, Blemish, Dead Pixel)
	float			fConcentration[Particle_Idx_Max];	// 농도

	_tag_CAN_Particle_Result()
	{
		stHeader.iTestItem	= Inspection_Particle;
		wResultCode			= RC_Unknown_Code;

		byRoiCnt = 0;

		for (int i = 0; i < Particle_Idx_Max; i++)
		{
			byType[i] = Particle_Type_None;
			fConcentration[i] = 0.0f;
		}
	};

}ST_CAN_Particle_Result;

//-----------------------------------------------------------------------------
// Relative Illumination, Opt : PC -> CAMERA , Result : CAMERA -> PC
//-----------------------------------------------------------------------------
typedef struct _tag_CAN_RelativeIllumination_Opt
{
	ST_CAN_Header stHeader;

	bool	bRoiIndex[RelativeIllumination_Idx_Max];

	_tag_CAN_RelativeIllumination_Opt()
	{
		stHeader.iTestItem = Inspection_RelativeIllumination;

		for (int i = 0; i < RelativeIllumination_Idx_Max; i++)
		{
			bRoiIndex[i] = false;
		}
	};

}ST_CAN_RelativeIllumination_Opt;

typedef struct _tag_CAN_RelativeIllumination_Result
{
	ST_CAN_Header	stHeader;
	WORD			wResultCode;

	unsigned short	sSignal[RelativeIllumination_Idx_Max];
	float			fNoise[RelativeIllumination_Idx_Max];

	_tag_CAN_RelativeIllumination_Result()
	{
		stHeader.iTestItem = Inspection_RelativeIllumination;
		wResultCode = RC_Unknown_Code;
		
		for (int i = 0; i < RelativeIllumination_Idx_Max; i++)
		{
			sSignal[i] = 0;
			fNoise[i] = 0.0f;
		}
	};

}ST_CAN_RelativeIllumination_Result;

//-----------------------------------------------------------------------------
// SNR Particle, Opt : PC -> CAMERA , Result : CAMERA -> PC
//-----------------------------------------------------------------------------
typedef struct _tag_CAN_SNR_Particle_Opt
{
	ST_CAN_Header stHeader;

	bool	bRoiIndex[SNR_Particle_Idx_Max];

	_tag_CAN_SNR_Particle_Opt()
	{
		stHeader.iTestItem = Inspection_SNR_Particle;

		for (int i = 0; i < SNR_Particle_Idx_Max; i++)
		{
			bRoiIndex[i] = false;
		}
	};

}ST_CAN_SNR_Particle_Opt;

typedef struct _tag_CAN_SNR_Particle_Result
{
	ST_CAN_Header	stHeader;
	WORD			wResultCode;

	unsigned short	sSignal[SNR_Particle_Idx_Max];
	float			fNoise[SNR_Particle_Idx_Max];

	_tag_CAN_SNR_Particle_Result()
	{
		stHeader.iTestItem = Inspection_SNR_Particle;
		wResultCode = RC_Unknown_Code;

		for (int i = 0; i < SNR_Particle_Idx_Max; i++)
		{
			sSignal[i] = 0;
			fNoise[i] = 0.0f;
		}
	};

}ST_CAN_SNR_Particle_Result;

//-----------------------------------------------------------------------------
// FPN, Opt : PC -> CAMERA , Result : CAMERA -> PC
//-----------------------------------------------------------------------------
typedef struct _tag_CAN_FPN_Opt
{
	ST_CAN_Header stHeader;

	float	fThresholdX;		// X Fail 기준 Threshold
	float	fThresholdY;		// Y Fail 기준 Threshold

	_tag_CAN_FPN_Opt()
	{
		stHeader.iTestItem = Inspection_FPN;

		fThresholdX = 0.0f;
		fThresholdY = 0.0f;
	};

}ST_CAN_FPN_Opt;

typedef struct _tag_CAN_FPN_Result
{
	ST_CAN_Header	stHeader;
	WORD			wResultCode;

	unsigned short	sFailCountX;	// Fail X Line 수량
	unsigned short	sFailCountY;	// Fail Y Line 수량

	_tag_CAN_FPN_Result()
	{
		stHeader.iTestItem = Inspection_FPN;
		wResultCode = RC_Unknown_Code;

		sFailCountX = 0;
		sFailCountY = 0;
	};

}ST_CAN_FPN_Result;

//-----------------------------------------------------------------------------
// Hot Pixel, Opt : PC -> CAMERA , Result : CAMERA -> PC
//-----------------------------------------------------------------------------
typedef struct _tag_CAN_DefectPixel_Opt
{
	ST_CAN_Header stHeader;

	float	fThreshold;			// 기준 Threshold

	_tag_CAN_DefectPixel_Opt()
	{
		stHeader.iTestItem = Inspection_DefectPixel;

		fThreshold = 0.0f;
	};

}ST_CAN_DefectPixel_Opt;

typedef struct _tag_CAN_DefectPixel_Result
{
	ST_CAN_Header	stHeader;
	WORD			wResultCode;

	BYTE			byIndexCnt;							// 찾은 수량
	unsigned short	sPointX[DefectPixel_Idx_Max];			// Hot Pixel의 X좌표
	unsigned short	sPointY[DefectPixel_Idx_Max];			// Hot Pixel의 Y좌표
	float			fConcentration[DefectPixel_Idx_Max];	// 농도

	_tag_CAN_DefectPixel_Result()
	{
		stHeader.iTestItem = Inspection_DefectPixel;
		wResultCode = RC_Unknown_Code;

		byIndexCnt = 0;
		for (int i = 0; i < DefectPixel_Idx_Max; i++)
		{
			sPointX[i] = 0;
			sPointY[i] = 0;
			fConcentration[i] = 0.0f;
		}
	};

}ST_CAN_DefectPixel_Result;

//-----------------------------------------------------------------------------
// TestItem, Opt : PC -> CAMERA , Result : CAMERA -> PC
//-----------------------------------------------------------------------------
typedef struct _tag_CAN_TestItem_Opt
{
	// 검사 항목 수
	WORD wTestCnt;

	// TEST 
	ST_CAN_SFR_Opt					stSFROpt;

}ST_CAN_TestItem_Opt;

typedef struct _tag_CAN_TestItem_Result
{
	// 검사 항목 수
	WORD wTestCnt;

	// TEST 
	ST_CAN_SFR_Result					stSFRResult;

}ST_CAN_TestItem_Result;
#pragma pack(pop)
#endif
