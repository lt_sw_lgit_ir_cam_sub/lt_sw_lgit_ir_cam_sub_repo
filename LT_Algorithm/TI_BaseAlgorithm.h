﻿#pragma once
#include "Def_Type.h"
#include "Def_Communication.h"

#include "opencv2/opencv.hpp"
#include "cv.h"

class CTI_BaseAlgorithm
{
protected:
	// 이미지 버퍼
	WORD*		m_pGRAYScanBuf;

	// 이미지 해상도
	int			m_iWidth;
	int			m_iHeight;


protected:
	bool			CheckROI(ST_CAN_ROI stROI);

public:
	virtual void	Initialize(WORD* pGRAYScanBuf, int iWidth, int iHeight);
};

