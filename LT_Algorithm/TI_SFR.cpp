#include "TI_SFR.h"

void CTI_SFR::Initialize(WORD* pGRAYScanBuf, int iWidth, int iHeight)
{
#ifdef DEBUG_PRINT_LOG
	printf("[Start] CTI_SFR::Initialize() : pGRAYScanBuf : %d, iWidth : %d, iHeight : %d\n",
		pGRAYScanBuf != NULL ? 1 : 0, iWidth, iHeight);
#endif
	m_pGRAYScanBuf = pGRAYScanBuf;

	m_iWidth = iWidth;
	m_iHeight = iHeight;

#ifdef DEBUG_PRINT_LOG
	printf("[End] CTI_SFR::Initialize()\n");
#endif
}

ST_CAN_SFR_Result CTI_SFR::Inspection(ST_CAN_SFR_Opt stData)
{
	//-------------------------------------------------------Data셋팅
	m_stData = stData;
	m_stResult.byRoiCnt = stData.byRoiCnt;
	m_stResult.stHeader.iTestItem = stData.stHeader.iTestItem;

#ifdef DEBUG_PRINT_LOG
	printf("[Start] CTI_SFR::Inspection()\n");
	for (int i = 0; i < m_stData.byRoiCnt; i++)
	{
		if (CheckROI(m_stData.stROI[i]) == false)
			continue;

		printf("[Info] CTI_SFR::Inspection() : Cnt : %d, ROI[%d](L_%d, T_%d, W_%d, H_%d), LinePair : %.2f\n",
			m_stData.byRoiCnt, i, m_stData.stROI[i].sLeft, m_stData.stROI[i].sTop, m_stData.stROI[i].sWidth,
			m_stData.stROI[i].sHeight, m_stData.fLinePair[i]);
	}
#endif

	if (stData.byRoiCnt <= 0)
	{
		m_stResult.wResultCode = RC_Item_Count_Zero;
		return m_stResult;
	}

	//-------------------------------------------------------TEST진행
	SFR_Gen_16bit();

#ifdef DEBUG_PRINT_LOG
	for (int i = 0; i < m_stData.byRoiCnt; i++)
	{
		printf("[Run] CTI_SFR::Inspection() : m_stResult.fDegree[%d] %.2f\n",
			i, m_stResult.fValue[i]);
	}
#endif

	//-------------------------------------------------------TEST결과
	m_stResult.wResultCode = RC_OK;

	/*
	// 결과
	for (int i = 0; i < MAX_ROI_SFR; i++){
		if (SFR_DATA[i].bEnable == TRUE){

			CString str_temp;
			str_temp.Format(_T("%0.4f"), m_dResultData[i]);

			// 결과 출력
			printf("SRF result [%d]: %.4f\n", i, m_dResultData[i]);

			//double temp_Result = 0.0;
			//temp_Result = atof(LPSTR(LPCTSTR(str_temp)));
		}
	}*/

#ifdef DEBUG_PRINT_LOG
	printf("[End] CTI_SFR::Inspection()\n");
#endif

	return m_stResult;
}

void CTI_SFR::Get_16bit_ROI(int iROI_Index, int iStartX, int iStartY, int iImgWidth, int iImgHeight, WORD* pTemp)
{
	int iLimitWidth = 0;
	int iLimitHeight = 0;

	iLimitWidth = iStartX + iImgWidth;
	iLimitHeight = iStartY + iImgHeight;

	if (iImgWidth > iImgHeight)
	{
		for (int X = iStartX, CntX = 0; X < iLimitWidth; X++, CntX++)
		{
			for (int Y = iStartY, CntY = 0; Y < iLimitHeight; Y++, CntY++)
			{
				pTemp[(CntX * iImgHeight) + CntY] = m_pGRAYScanBuf[Y * m_iWidth + X];
			}
		}
		m_stResult.fValue[iROI_Index] = (float)GetSFRValue_16bit(pTemp, iImgHeight, iImgWidth, m_stData.fLinePair[iROI_Index]);//Ver.3
	}
	else
	{
		for (int Y = iStartY, CntY = 0; Y < iLimitHeight; Y++, CntY++)
		{
			for (int X = iStartX, CntX = 0; X < iLimitWidth; X++, CntX++)
			{
				pTemp[(CntY * iImgWidth) + CntX] = m_pGRAYScanBuf[Y * m_iWidth + X];
			}
		}
		m_stResult.fValue[iROI_Index] = (float)GetSFRValue_16bit(pTemp, iImgWidth, iImgHeight, m_stData.fLinePair[iROI_Index]);//Ver.3
	}
}

void	CTI_SFR::SFR_Gen_16bit()
{
	WORD	*pTemp = NULL;
	
	int iStartX = 0;
	int iStartY = 0;
	int iImgWidth = 0;
	int iImgHeight = 0;

	int iLimitWidth = 0;
	int iLimitHeight = 0;
	
	for (int i = 0; i < SFR_ROI_Max; i++)
	{
		if (CheckROI(m_stData.stROI[i]) == false)
			continue;

		iStartX = m_stData.stROI[i].sLeft;
		iStartY = m_stData.stROI[i].sTop;
		iImgWidth = m_stData.stROI[i].sWidth;
		iImgHeight = m_stData.stROI[i].sHeight;

		iLimitWidth = iStartX + iImgWidth;
		iLimitHeight = iStartY + iImgHeight;

		// 해상도 예외 처리
		if (iStartX < 0 || iStartY < 0 ||
			iLimitWidth > m_iWidth || 
			iLimitHeight > m_iHeight)
			continue;

		pTemp = new WORD[iImgWidth * iImgHeight];


// 수정 : piring -> SFR 값이 0.0이 나오면 1픽셀씩 ROI를 이동하며 재 측정한다.
// 		if (iImgWidth > iImgHeight)
// 		{
//  			for (int X = iStartX, CntX = 0; X < iLimitWidth; X++, CntX++)
//  			{
//  				for (int Y = iStartY, CntY = 0; Y < iLimitHeight; Y++, CntY++)
//  				{
//  					pTemp[(CntX * iImgHeight) + CntY] = m_pGRAYScanBuf[Y * m_iWidth + X];
//  				}
//  			}
//  			m_stResult.fValue[i] = (float)GetSFRValue_16bit(pTemp, iImgHeight, iImgWidth, m_stData.fLinePair[i]);//Ver.3
// 		}
// 		else
// 		{
//  			for (int Y = iStartY, CntY = 0; Y < iLimitHeight; Y++, CntY++)
//  			{
//  				for (int X = iStartX, CntX = 0; X < iLimitWidth; X++, CntX++)
//  				{
//  					pTemp[(CntY * iImgWidth) + CntX] = m_pGRAYScanBuf[Y * m_iWidth + X];
//  				}
//  			}
//  			m_stResult.fValue[i] = (float)GetSFRValue_16bit(pTemp, iImgWidth, iImgHeight, m_stData.fLinePair[i]);//Ver.3
// 		}

		// 1차
		Get_16bit_ROI(i, iStartX, iStartY, iImgWidth, iImgHeight, pTemp);

		// 2차 <- 1픽셀 이동
		if (0.0 == m_stResult.fValue[i])
		{
			if (1 <= iStartX)	// <- 1픽셀 이동 가능하면..
			{
				iStartX = iStartX - 1;

				Get_16bit_ROI(i, iStartX, iStartY, iImgWidth, iImgHeight, pTemp);

				iStartX = iStartX + 1;
			}

			// 3차 -> 1픽셀 이동
			if (0.0 == m_stResult.fValue[i])
			{
				if ((iStartX + iImgWidth) < m_iWidth)	// -> 1픽셀 이동 가능하면..
				{
					iStartX = iStartX + 1;

					Get_16bit_ROI(i, iStartX, iStartY, iImgWidth, iImgHeight, pTemp);

					iStartX = iStartX - 1;
				}

				// 4차 up 1픽셀 이동
				if (0.0 == m_stResult.fValue[i])
				{
					if (1 <= iStartY)	// up 1픽셀 이동 가능하면..
					{
						iStartY = iStartY - 1;

						Get_16bit_ROI(i, iStartX, iStartY, iImgWidth, iImgHeight, pTemp);

						iStartY = iStartY + 1;
					}

					// 5차 down 1픽셀 이동
					if (0.0 == m_stResult.fValue[i])
					{
						if ((iStartY + iImgHeight) < m_iHeight)	// down 1픽셀 이동 가능하면..
						{
							iStartY = iStartY + 1;

							Get_16bit_ROI(i, iStartX, iStartY, iImgWidth, iImgHeight, pTemp);

							iStartY = iStartY - 1;
						}
					}
				}
			}
		}

		SAFE_DELETE_ARRAY(pTemp);
	}
}

double CTI_SFR::GetSFRValue_16bit(WORD *pBuf, int iWidth, int iHeight, double dbLinePair)
{
	int iImgWidth = iWidth;
	int iImgHeight = iHeight;

	double tleft = 0;
	double tright = 0;

	double fil1[2] = { 0.5, -0.5 };
	double fil2[3] = { 0.5, 0, -0.5 };
	double dbResultSFR;

	//rotatev2가 들어가야 함.

	for (int y = 0; y < iImgHeight; y++)
	{
		for (int x = 0; x < 5; x++)
		{
			tleft += (int)pBuf[y * iImgWidth + x];
		}

		for (int x = iImgWidth - 6; x<iImgWidth; x++)
		{
			tright += (int)pBuf[y * iImgWidth + x];
		}
	}

	if (tleft > tright)
	{
		fil1[0] = -0.5;
		fil1[1] = 0.5;
		fil2[0] = -0.5;
		fil2[1] = 0;
		fil2[2] = 0.5;
	}

	if (tleft + tright == 0.0)
	{
		//	printf("Zero divsion!\n");
		return 0;
	}
	else
	{
		double test = fabs((tleft - tright) / (tleft + tright));

		if (test < 0.2)
		{
			//		printf("Edge contrast is less that 20%\n");
			return 0;
		}
	}

	double n = iImgWidth;
	double mid = (iImgWidth + 1) / 2.0;	// MatLab SFRmat3 에서는 +1을 해줌. #Lucas수정1

	/////////////////////////// ahamming start
	double wid1 = mid - 1;		// // MatLab SFRmat3 에서는 -1을 해줌. #Lucas수정2
	double wid2 = n - mid;
	double wid;
	if (wid1 > wid2)
		wid = wid1;
	else
		wid = wid2;

	double arg = 0;

	double *win1 = new double[iImgWidth];

	for (int i = 0; i < n; i++)
	{
		arg = i - mid + 1;	// MatLab은 1 base임으로, 0 base 시 1을 더해줌. #Lucas수정3
		win1[i] = 0.54 + 0.46*cos((M_PI*arg / wid));
	}
	/////////////////////////// ahamming end // 여기까지 sfrmat3와 동일 결과.

	//******************************deriv1**********************************************//
	// todo: #Lucas deriv1의 결과 값이 Matlab과 다름. 디버깅이 필요한데 소스가 달라 복잡함.
	int d_n = 2;	//필터열갯수..
	int m = iImgWidth + d_n - 1;
	double sum = 0;
	double *Deriv_c = new double[iImgHeight * m];
	double *c = new double[iImgWidth * iImgHeight];

	for (int k = 0; k < iImgWidth * iImgHeight; k++)
		c[k] = 0.0;

	for (int y = 0; y<iImgHeight; y++)
	{
		for (int k = 0; k<iImgWidth; k++)
		{
			sum = 0;

			for (int d_j = d_n - 1; d_j>-1; d_j--)
			{
				if ((k - d_j) > -1 && (k - d_j) < iImgWidth)
					sum += (double)(pBuf[y * iImgWidth + (k - d_j)]) * fil1[d_j];
			}
			Deriv_c[y * iImgWidth + k] = sum;
		}
	}

	for (int y = 0; y < iImgHeight; y++)
	{
		for (int k = (d_n - 1); k < iImgWidth; k++)
			c[y * iImgWidth + k] = Deriv_c[y * iImgWidth + k];
		c[y * iImgWidth + d_n - 2] = Deriv_c[y * iImgWidth + d_n - 1];
	}

	delete[]Deriv_c;
	Deriv_c = NULL;
	//************************************************************************************//
	double *loc = new double[iImgHeight];
	double loc_v = 0, total_v = 0;

	for (int y = 0; y < iImgHeight; y++)
	{
		loc_v = 0;
		total_v = 0;
		for (int x = 0; x < iImgWidth; x++)
		{
			loc_v += ((double)c[y * iImgWidth + x] * win1[x])*((double)x + 1);	//수정 //MATLAB과 최대한 mid position을 맞춰주자..
			total_v += (double)c[y * iImgWidth + x] * win1[x];
		}

		if (total_v == 0 || total_v < 0.0001)
			loc[y] = 0;
		else
			loc[y] = loc_v / total_v - 0.5;
	}

	delete[]win1;
	//*****************************************************************************//	QR-DECOMP

	//*****************************************************************************//
	int rM = iImgHeight;
	int cN = 2;

	int i, j, rm, cn;
	int count = 0;

	rm = rM;
	cn = cN;

	double **A = new double *[rM];
	double **pinvA = new double *[cN];

	for (i = 0; i < rM; i++)
		A[i] = new double[cN];

	for (i = 0; i < rM; i++)
	{
		A[i][0] = count;
		A[i][1] = 1.0;
		count++;
	}

	for (i = 0; i < cN; i++)
		pinvA[i] = new double[rM];

	pInverse(A, rM, cN, pinvA);

	double **B = new double *[rM];
	for (i = 0; i < rM; i++)
		B[i] = new double[1];

	for (i = 0; i < rM; i++)
		B[i][0] = loc[i];

	double **C = new double *[cN];
	for (i = 0; i < cN; i++)
		C[i] = new double[1];

	MulMatrix(pinvA, B, C, cN, 1, rM, 0);

	//	printf("MulMatrix C[0][0]: %10.4f\n", C[0][0]);
	//	printf("MulMatrix C[1][0]: %10.4f\n", C[1][0]);
	
	double *place = new double[iImgHeight];
	double *win2 = new double[iImgWidth];

	for (j = 0; j<iImgHeight; j++)
	{
		//**********hamming Window***************************//
		place[j] = C[1][0] + C[0][0] * (double)(j + 1);

		n = iImgWidth;
		mid = place[j];
		wid1 = mid - 1;
		wid2 = n - mid;

		if (wid1 > wid2)
			wid = wid1;
		else
			wid = wid2;

		arg = 0;

		for (i = 0; i < n; i++)
		{
			arg = (i + 1) - mid;
			win2[i] = cos(M_PI*arg / wid);
		}

		for (i = 0; i < n; i++)
			win2[i] = 0.54 + 0.46*win2[i];

		loc_v = 0;
		total_v = 0;

		for (int x = 0; x<iImgWidth; x++)
			total_v += c[j * iImgWidth + x] * win2[x];

		for (int x = 0; x < iImgWidth; x++)
		{
			loc_v += (c[j * iImgWidth + x] * win2[x])*(x + 1); // 수정
		}

		if (total_v == 0 || total_v < 0.0001)
			loc[j] = 0;
		else
			loc[j] = loc_v / total_v - 0.5;
	}

	delete[]win2;
	delete[]place;
	delete[]c;

	for (i = 0; i < rM; i++)
	{
		delete[]A[i];
		delete[]B[i];
	}
	delete[]A;
	delete[]B;

	for (i = 0; i < cN; i++)
		delete[]pinvA[i];
	delete[]pinvA;

	//====================centeroid fitting==============// Hamming Window를 통한 값으로 라인 fitting을 한번 더 해준다..(여기서 slope 구함)
	rM = iImgHeight;
	cN = 2;

	//	int i, j, rm, cn;
	count = 0;

	rm = rM;
	cn = cN;

	A = new double *[rM];
	pinvA = new double *[cN];

	for (i = 0; i < rM; i++)
		A[i] = new double[cN];

	for (i = 0; i < rM; i++)
	{
		A[i][0] = count;
		A[i][1] = 1.0;
		count++;
	}

	for (i = 0; i < cN; i++)
		pinvA[i] = new double[rM];

	pInverse(A, rM, cN, pinvA);

	B = new double *[rM];
	for (i = 0; i < rM; i++)
		B[i] = new double[1];

	for (i = 0; i < rM; i++)
		B[i][0] = loc[i];

	MulMatrix(pinvA, B, C, cN, 1, rM, 0);

	for (i = 0; i < rM; i++)
	{
		delete[]A[i];
		delete[]B[i];
	}
	delete[]A;
	delete[]B;

	for (i = 0; i < cN; i++)
		delete[]pinvA[i];
	delete[]pinvA;
	//===================================================================================================//

	int nbin = 4;
	double nn = iImgWidth * nbin;
	double nn2 = nn / 2 + 1;
	double *freq = new double[(int)nn];
	double del = 1.0;

	for (i = 0; i < (int)nn; i++)
		freq[i] = (double)nbin*((double)i) / (del*nn);

	double freqlim = 1.0;
	double nn2out = (nn2*freqlim / 2);
	double *win = new double[nbin * iImgWidth];

	//**********hamming Window***************************//	
	n = nbin * iImgWidth;
	mid = (nbin*iImgWidth + 1) / 2.0;
	wid1 = mid - 1;
	wid2 = n - mid;
	
	if (wid1 > wid2)
		wid = wid1;
	else
		wid = wid2;

	arg = 0;

	for (i = 0; i < n; i++)
	{
		arg = (double)(i + 1) - mid;
		win[i] = cos((M_PI*arg / wid));
	}

	for (i = 0; i < n; i++)
		win[i] = 0.54 + 0.46*win[i];

	//sfrmat3로 수정===============================================================//

	double fac = 4.0;
	double inProject_nn = iImgWidth * fac;
	double slope = C[0][0];

	double vslope = slope;

	slope = 1.0 / slope;

	double slope_deg = 180.0 * atan(abs(vslope)) / M_PI;

	if (slope_deg < 1.5)
	{
		delete[]freq;
		delete[]win;
		return 0.0;
		//resultSFR = 0.0;
		//return resultSFR;//너무 급경사를 가진 Edge라서 제대로 측정할 수 없음. //예외 처리 구문을 넣어야 할듯..return 등등
	}

	double del2 = 0;
	double delfac;

	delfac = cos(atan(vslope));
	del = del * delfac;
	del2 = del / nbin;

	//	double offset = 0;
	double offset = fac * (0.0 - (((double)iImgHeight - 1.0) / slope));

	del = abs(cvRound(offset));
	if (offset > 0) offset = 0.0;

	double *dcorr = new double[(int)nn2];
	double temp_m = 3.0;	//length of difference filter
	double scale = 1;

	for (i = 0; i<nn2; i++)
		dcorr[i] = 1.0;

	temp_m = temp_m - 1;

	for (i = 1; i<nn2; i++)
	{
		dcorr[i] = abs((M_PI*(double)(i + 1)*temp_m / (2.0*(nn2 + 1))) / sin(M_PI * (double)(i + 1)*temp_m / (2.0*(nn2 + 1))));	//필터를 이용하여 낮은 응답 신호 부분을 증폭시키려는 의도인듯....;;
		dcorr[i] = 1.0 + scale * (dcorr[i] - 1.0);																	//scale로 weight를 조정하는듯..default 1

		if (dcorr[i] > 10)
			dcorr[i] = 10.0;
	}

	//============================================================================//
	//sfrmat3 : PER ISO Algorithm
	iImgHeight = (int)(cvRound(cvFloor(iImgHeight * fabs(C[0][0]))) / fabs(C[0][0]));	//수직방향으로 1 Cycle이 이뤄지게하기 위함인듯..

	for (i = 0; i < cN; i++)
		delete[]C[i];

	delete[]C;
	delete[]loc;

	double **barray = new double *[2];
	for (i = 0; i < 2; i++)
		barray[i] = new double[(int)inProject_nn + (int)del + 200];

	for (i = 0; i < 2; i++)
		for (j = 0; j < ((int)inProject_nn + (int)del + 100); j++)
			barray[i][j] = 0;

	for (int x = 0; x < iImgWidth; x++)
	{
		for (int y = 0; y < iImgHeight; y++)
		{
			double ttt = ((double)x - (double)y / slope)*fac;
			int ling = (int)(cvCeil(ttt) - offset);
			barray[0][ling]++;
			double kk = barray[1][ling];
			barray[1][ling] = kk + pBuf[y * iImgWidth + x];
		}
	}

	double *point = new double[(int)inProject_nn];
	int start = cvRound(del*0.5);
	double nz = 0;
	int status = 1;

	for (i = start; i < start + (int)inProject_nn - 1; i++)
	{
		if (barray[0][i] == 0)
		{
			nz = nz + 1;
			status = 0;
			if (i == 0)
				barray[0][i] = barray[0][i + 1];
			else
				barray[0][i] = (barray[0][i - 1] + barray[0][i + 1]) / 2.0;
		}
	}

	if (status == 0)
	{
		printf(" Zero count(s) found during projection binning. The edge \n");
		printf(" angle may be large, or you may need more lines of data. \n");
		printf(" Execution will continue, but see Users Guide for info. \n");
	}

	for (i = -1; i < (int)inProject_nn - 1; i++)
	{
		if (barray[0][i + start] != 0){
			if (barray[0][(i + 1) + start] == 0){
				point[(i + 1)] = 0;
			}
			else
				point[(i + 1)] = barray[1][(i + 1) + start] / barray[0][(i + 1) + start];
		}
		else
			point[(i + 1)] = 0;
	}

	for (i = 0; i < 2; i++)
		delete[]barray[i];

	delete[]barray;

	//******************************deriv1**********************************************//
	m = (int)inProject_nn;
	d_n = 3;	//필터열갯수..
	sum = 0;
	double *pointDeriv = new double[(int)inProject_nn];
	Deriv_c = new double[(int)inProject_nn + d_n - 1];

	for (int k = 0; k<(int)inProject_nn; k++)
		pointDeriv[k] = 0.0;

	for (int k = 0; k<m + d_n - 1; k++)
	{
		sum = 0;

		for (int d_j = d_n - 1; d_j>-1; d_j--)
		{
			if ((k - d_j) > -1 && (k - d_j) < m)
				sum += point[k - d_j] * fil2[d_j];
		}

		Deriv_c[k] = sum;
	}

	for (int k = (d_n - 1); k < (int)inProject_nn; k++)		//MATLAB은 배열이 1base이기 때문에 다시 정리해준다..
		pointDeriv[k] = Deriv_c[k];
	pointDeriv[d_n - 2] = Deriv_c[d_n - 1];
	//************************************************************************************//

	delete[]point;
	delete[]Deriv_c;

	//*********************centroid*************************************//
	loc_v = 0, total_v = 0;

	loc_v = 0;
	total_v = 0;
	for (int x = 0; x < (int)inProject_nn; x++)
	{
		loc_v += pointDeriv[x] * x;
		total_v += pointDeriv[x];
	}

	if (total_v == 0 || total_v < 0.0001){
		delete[]freq;
		delete[]win;
		delete[]pointDeriv;
		return 0.0;
	}
	else{
		loc_v = loc_v / total_v;
	}

	//**************************cent****************************//
	double *temp = new double[(int)inProject_nn];
	for (i = 0; i<(int)inProject_nn; i++)
		temp[i] = 0.0;

	mid = cvRound((inProject_nn + 1.0) / 2.0);

	int cent_del = cvRound(cvRound(loc_v) - mid);

	if (cent_del > 0)
	{
		for (i = 0; i < inProject_nn - cent_del; i++)
			temp[i] = pointDeriv[i + cent_del];
	}
	else if (cent_del < 1)
	{
		for (i = -cent_del; i < inProject_nn; i++)
			temp[i] = pointDeriv[i + cent_del];
	}
	else
	{
		for (i = 0; i < inProject_nn; i++)
			temp[i] = pointDeriv[i];
	}

	for (i = 0; i < (int)inProject_nn; i++)
		temp[i] = win[i] * temp[i];

	delete[]win;
	delete[]pointDeriv;

	CvMat *sourceFFT = cvCreateMat((int)inProject_nn * 2, 1, CV_64FC1);
	CvMat *resultFFT = cvCreateMat((int)inProject_nn * 2, 1, CV_64FC1);
	CvMat *ConveredResultFFT = cvCreateMat((int)inProject_nn * 2, 1, CV_64FC1);

	for (i = 0; i < (int)inProject_nn; i++)
	{
		sourceFFT->data.db[i * 2] = temp[i];
		sourceFFT->data.db[i * 2 + 1] = 0;
	}

	delete[]temp;

	cvDFT(sourceFFT, resultFFT, CV_DXT_FORWARD, 0);

	for (i = 0; i < (int)inProject_nn; i++)
	{
		if (i != 0)
		{
			ConveredResultFFT->data.db[i] = sqrt(pow(resultFFT->data.db[i * 2 - 1], 2) + pow(resultFFT->data.db[i * 2], 2));
		}
		else
		{
			ConveredResultFFT->data.db[i] = fabs(resultFFT->data.db[i]);
		}
	}
	double *Resultmtf = new double[(int)inProject_nn];
	int count_j = 0;

	memset(Resultmtf, 0, sizeof(Resultmtf));

	for (int i = 0; i < (int)inProject_nn / 2; i++)
	{
		//Resultmtf[count_j] = fabs(ConveredResultFFT->data.db[i]) / fabs(ConveredResultFFT->data.db[0]); // 수정 : piring -> 0으로 나눗셈이 진행되는 문제가 있음
		Resultmtf[count_j] = (0.0f != ConveredResultFFT->data.db[0]) ? (fabs(ConveredResultFFT->data.db[i]) / fabs(ConveredResultFFT->data.db[0])) : 0;
		Resultmtf[count_j] = Resultmtf[count_j] * dcorr[i];
		count_j++;
	}

	delete[]dcorr;	//correct weight 배열 해제

	cvReleaseMat(&sourceFFT);
	cvReleaseMat(&resultFFT);
	cvReleaseMat(&ConveredResultFFT);
	
	double nPixel = 1000.0 / SFR_PIXEL_SIZE;

	// cypx를 결과로 하는 방식
	// 0.5 의 근사치 위치의 값을 양쪽 값을 이용하여 계산하는 방식
	double	dbBefore, dbNext;
	int		nBefore, nNext;

	dbBefore = dbNext = 1.0;
	nBefore = nNext = 0;
	for (int i = 0; i < (int)inProject_nn; i++)
	{
		nNext = i;
		dbNext = Resultmtf[i];
		//		printf("Result[%d] Freq.:MTF--- %f,%f\n", i, freq[i], dbNext);
		if (dbNext < dbLinePair / 100.0)
			break;
		dbBefore = dbNext;
		nBefore = nNext;
	}

	double dbBeforeFreq = freq[nBefore];
	double dbNextFreq = freq[nNext];

	// 그래프가 반비례 형태임으로 아래 형태 계산이 더 근접한 계산	  
	//dbResultSFR = dbNextFreq - (dbLinePair / 100.0 - dbNext) / (dbBefore - dbNext) * (dbNextFreq - dbBeforeFreq); // 수정 : piring -> 0으로 나눗셈이 진행되는 문제가 있음
	dbResultSFR = (dbBefore != dbNext) ? (dbNextFreq - (dbLinePair / 100.0 - dbNext) / (dbBefore - dbNext) * (dbNextFreq - dbBeforeFreq)) : dbNextFreq;

	//printf("Result mtf range p[%d,%d] - value: [%f,%f] / [%f,%f] --> result SFR: %f \n"
	//	, nBefore, nNext, dbBefore, dbNext, dbBeforeFreq, dbNextFreq, resultSFR);

	if (dbResultSFR > 0.5)	// 왜? 0.5보다 크면 안 됨으로
		dbResultSFR = 0.0;
	//////////////////////// 끝

	// cymm를 결과로 하는 방식
	dbResultSFR = (dbResultSFR / SFR_PIXEL_SIZE) * 1000.0;

	delete[]freq;
	delete[]Resultmtf;

	return dbResultSFR;
}

//void CTI_SFR::SearchROIArea_16bit(WORD *GRAYScanBuf, int tempWidth)
//{
//	IplImage *OriginImage_org = cvCreateImage(cvSize(tempWidth, m_nHeight), IPL_DEPTH_8U, 1);
//	IplImage *OriginImage = cvCreateImage(cvSize(tempWidth, m_nHeight), IPL_DEPTH_8U, 1);
//	IplImage *CannyImage = cvCreateImage(cvSize(tempWidth, m_nHeight), IPL_DEPTH_8U, 1);
//	IplImage *DilateImage = cvCreateImage(cvSize(tempWidth, m_nHeight), IPL_DEPTH_8U, 1);
//	IplImage *SmoothImage = cvCreateImage(cvSize(tempWidth, m_nHeight), IPL_DEPTH_8U, 1);
//	IplImage *RGBResultImage = cvCreateImage(cvSize(tempWidth, m_nHeight), IPL_DEPTH_8U, 3);
//	IplImage *temp_PatternImage = cvCreateImage(cvSize(tempWidth, m_nHeight), IPL_DEPTH_8U, 1);
//
//	BYTE *pBase = (BYTE*)GRAYScanBuf;
//	BYTE nHigh, nLow;
//
//	for (int y = 0; y < m_nHeight; y++)
//	{
//		for (int x = 0; x < tempWidth; x++)
//		{
//			//B = IN_RGB[y*(tempWidth * 3) + x * 3];
//			//G = IN_RGB[y*(tempWidth * 3) + x * 3 + 1];
//			//R = IN_RGB[y*(tempWidth * 3) + x * 3 + 2];
//
//			//Sum_Y = (BYTE)((0.29900*R) + (0.58700*G) + (0.11400*B));
//
//			//OriginImage->imageData[y*OriginImage->widthStep + x] = Sum_Y;
//
//
//			// GRAYScanBuf 가 12 bit일경우 >> 4 bit 
//			//			OriginImage->imageData[y*OriginImage->widthStep + x] = GRAYScanBuf[y*tempWidth + x];
//			//OriginImage_org->imageData[y*OriginImage->widthStep + x] = GRAYScanBuf[y*tempWidth + x];
//			//OriginImage->imageData[y*OriginImage->widthStep + x] = (BYTE)((GRAYScanBuf[y*tempWidth + x] >> 4) & 0x00FF);
//
//
//			nLow = *(pBase + y * tempWidth * 2 + x * 2);
//			nHigh = *(pBase + y * tempWidth * 2 + x * 2 + 1);
//
//			// GRAYScanBuf 가 12 bit일경우 >> 4 bit 
//			// 그런데, 실상은 high BYTE max가 16, 따라서 3비트 쉬프트
//			OriginImage_org->imageData[y*tempWidth + x] = ((nHigh << 3) & 0xF0) | ((nLow >> 5) & 0x0F);
//			OriginImage->imageData[y*tempWidth + x] = ((nHigh << 3) & 0xF0) | ((nLow >> 5) & 0x0F);
//		}
//	}
//
//	// 영역 확장
//	cvDilate(OriginImage, OriginImage);
//
//	// 이진화 함수
//	//		threshold 초기값과 무관하게 입력 영상에 대해 내부적으로 threshold 값을 구하고 
//	//		이에 따라 선택된 픽셀들에 Max_Value 값을 준다.
//	cvThreshold(OriginImage, DilateImage, 0, 255, CV_THRESH_OTSU);
//	// 이미지 반전
//	cvNot(DilateImage, DilateImage);
//
//	cvCopyImage(DilateImage, temp_PatternImage);
//
//	// 색상 변경 (GRAY -> BGR)
//	cvCvtColor(DilateImage, RGBResultImage, CV_GRAY2BGR);
//
//	CvMemStorage* contour_storage = cvCreateMemStorage(0);
//	CvSeq *contour = 0;
//	CvSeq *temp_contour = 0;
//
//	// 이미지에서 Contour를 찾은 후 갯수를 리턴
//	cvFindContours(DilateImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
//
//	temp_contour = contour;
//
//	int counter = 0;
//
//	for (; temp_contour != 0; temp_contour = temp_contour->h_next)
//		counter++;
//
//	CvRect *rectArray = new CvRect[counter];
//	//	double *areaArray = new double[counter];
//	CvRect rect;
//	double area = 0, arcCount = 0;
//	counter = 0;
//	double old_dist = 999999;
//
//	for (; contour != 0; contour = contour->h_next)
//	{
//		// 면적
//		area = cvContourArea(contour, CV_WHOLE_SEQ);
//		// 길이
//		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);
//
//		// contour의 사각 영역
//		rect = cvContourBoundingRect(contour, 1);
//		double circularity = (4.0*M_PI*area) / (arcCount*arcCount);
//
//		rect = cvContourBoundingRect(contour, 1);
//
//		int center_x, center_y;
//		center_x = rect.x + rect.width / 2;
//		center_y = rect.y + rect.height / 2;
//
//		int i = 0;
//
//		if (rect.width < (tempWidth / 4) && rect.height < (m_nHeight / 4))
//		{
//			double dRatio = (double)rect.width / (double)rect.height * 100.0;
//			if ((rect.width > 30 || rect.height > 30) && dRatio <120)
//			{
//				rectArray[counter] = rect;
//				//						areaArray[counter] = area;
//
//				counter++;
//
//				double dist = GetDistance(center_x, center_y, m_nWidth / 2, m_nHeight / 2);
//
//				if (old_dist > dist)
//				{
//					m_currPt.x = center_x - (m_nWidth / 2);
//					m_currPt.y = center_y - (m_nHeight / 2);
//
//					m_currPt.x = m_currPt.x / 2;
//					m_currPt.y = m_currPt.y / 2;
//
//					old_dist = dist;
//				}
//
//				cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(0, 255, 0), 1, 8);
//			}
//		}
//	}
//
//	int rect_cnt = 0;
//
//	if (counter > 0 && abs(m_currPt.x) < 50 && abs(m_currPt.y) < 50)
//	{
//		for (int q = 0; q < 50; q++)
//		{
//			double old_dist = 9999999;
//			double WHRatio_Thr, HWRatio_Thr;
//			double areaRatio_Thr;
//
//			int shortest_index = -1;
//
//			if (SFR_DATA[q].bEnable == TRUE)
//			{
//				shortest_index = -1;
//
//				for (int k = 0; k < counter; k++)
//				{
//					rect = rectArray[k];
//					double WHRatio = (double)rect.width / (double)rect.height;
//					double HWRatio = (double)rect.height / (double)rect.width;
//					int White_area_cnt = 0;
//					unsigned char pixel_v = 0;
//
//					int curr_rect_center_x = rect.x + rect.width / 2;
//					int curr_rect_center_y = rect.y + rect.height / 2;
//					int roi_x = INIT_SFR_DATA[q].tROI.nLeft + INIT_SFR_DATA[q].tROI.nWidth / 2;
//					int roi_y = INIT_SFR_DATA[q].tROI.nTop + INIT_SFR_DATA[q].tROI.nHeight / 2;
//
//					double dist = GetDistance(curr_rect_center_x, curr_rect_center_y, roi_x, roi_y);
//
//					if (old_dist > dist && dist < 300)
//					{
//						shortest_index = k;
//						old_dist = dist;
//					}
//				}
//				if (shortest_index != -1)
//				{
//					if (SFR_DATA[q].nType == 0)
//					{
//						if (rectArray[shortest_index].x - INIT_SFR_DATA[q].tROI.nWidth / 2 < 0)
//							SFR_DATA[q].tROI.nLeft = 0;
//						else
//						{
//							int newPos = (rectArray[shortest_index].x - INIT_SFR_DATA[q].tROI.nWidth / 2);
//							int dev_x = abs(INIT_SFR_DATA[q].tROI.nLeft - newPos);
//
//							if (dev_x < 100)
//							{
//								SFR_DATA[q].tROI.nLeft = (rectArray[shortest_index].x - INIT_SFR_DATA[q].tROI.nWidth / 3); //VW버전
//							}
//						}
//
//						if ((rectArray[shortest_index].y + rectArray[shortest_index].height / 2) - INIT_SFR_DATA[q].tROI.nHeight / 2 < 0)
//							SFR_DATA[q].tROI.nTop = 0;
//						else
//						{
//							int newPos = (rectArray[shortest_index].y + rectArray[shortest_index].height / 2) - INIT_SFR_DATA[q].tROI.nHeight / 2;
//							int dev_y = abs(INIT_SFR_DATA[q].tROI.nTop - newPos);
//
//							if (dev_y < 100){
//
//								if (q >= 4){
//									SFR_DATA[q].tROI.nTop = (rectArray[shortest_index].y + rectArray[shortest_index].height / 2) - INIT_SFR_DATA[q].tROI.nHeight / 2.0; //원복
//								}
//								else
//								{
//									SFR_DATA[q].tROI.nTop = (rectArray[shortest_index].y + rectArray[shortest_index].height / 2) - INIT_SFR_DATA[q].tROI.nHeight / 2; //VW버전
//								}
//							}
//						}
//					}
//					else if (SFR_DATA[q].nType == 1)
//					{
//						if (rectArray[shortest_index].x + rectArray[shortest_index].width - INIT_SFR_DATA[q].tROI.nWidth / 2 < 0)
//							SFR_DATA[q].tROI.nLeft = 0;
//						else
//						{
//							int newPos = rectArray[shortest_index].x + rectArray[shortest_index].width - INIT_SFR_DATA[q].tROI.nWidth / 2;
//							int dev_x = abs(INIT_SFR_DATA[q].tROI.nLeft - newPos);
//
//							if (dev_x < 100)
//							{
//								SFR_DATA[q].tROI.nLeft = rectArray[shortest_index].x + rectArray[shortest_index].width - INIT_SFR_DATA[q].tROI.nWidth / 1.5; //원복
//							}
//						}
//
//						if ((rectArray[shortest_index].y + rectArray[shortest_index].height / 2) - INIT_SFR_DATA[q].tROI.nHeight / 2 < 0)
//							SFR_DATA[q].tROI.nTop = 0;
//						else
//						{
//							int newPos = (rectArray[shortest_index].y + rectArray[shortest_index].height / 2) - INIT_SFR_DATA[q].tROI.nHeight / 2;
//							int dev_y = abs(INIT_SFR_DATA[q].tROI.nTop - newPos);
//
//							if (dev_y < 100)
//							{
//								SFR_DATA[q].tROI.nTop = (rectArray[shortest_index].y + rectArray[shortest_index].height / 2) - INIT_SFR_DATA[q].tROI.nHeight / 2; //VW 버전
//							}
//						}
//					}
//					else if (SFR_DATA[q].nType == 2)
//					{
//						if (rectArray[shortest_index].x + rectArray[shortest_index].width / 2 - INIT_SFR_DATA[q].tROI.nWidth / 2 < 0)
//							SFR_DATA[q].tROI.nLeft = 0;
//						else
//						{
//							int newPos = rectArray[shortest_index].x + rectArray[shortest_index].width / 2 - INIT_SFR_DATA[q].tROI.nWidth / 2;
//							int dev_x = abs(INIT_SFR_DATA[q].tROI.nLeft - newPos);
//
//							if (dev_x < 100)
//							{
//								SFR_DATA[q].tROI.nLeft = rectArray[shortest_index].x + rectArray[shortest_index].width / 2 - INIT_SFR_DATA[q].tROI.nWidth / 2; //VW버전
//							}
//						}
//
//						if ((rectArray[shortest_index].y) - INIT_SFR_DATA[q].tROI.nHeight / 2 < 0)
//							SFR_DATA[q].tROI.nTop = 0;
//						else
//						{
//							int newPos = (rectArray[shortest_index].y) - INIT_SFR_DATA[q].tROI.nHeight / 2;
//							int dev_y = abs(INIT_SFR_DATA[q].tROI.nTop - newPos);
//
//							if (dev_y < 100)
//								SFR_DATA[q].tROI.nTop = (rectArray[shortest_index].y) - INIT_SFR_DATA[q].tROI.nHeight / 3;
//						}
//					}
//					else if (SFR_DATA[q].nType == 3)
//					{
//						if (q == 8)
//						{
//							int t = 0;
//						}
//						if (rectArray[shortest_index].x + rectArray[shortest_index].width / 2 - INIT_SFR_DATA[q].tROI.nWidth / 2 < 0)
//							SFR_DATA[q].tROI.nLeft = 0;
//						else
//						{
//							int newPos = rectArray[shortest_index].x + rectArray[shortest_index].width / 2 - INIT_SFR_DATA[q].tROI.nWidth / 2;
//							int dev_x = abs(INIT_SFR_DATA[q].tROI.nLeft - newPos);
//
//							if (dev_x < 100)
//							{
//								SFR_DATA[q].tROI.nLeft = rectArray[shortest_index].x + rectArray[shortest_index].width / 2 - INIT_SFR_DATA[q].tROI.nWidth / 2; //VW버전
//							}
//						}
//
//						if ((rectArray[shortest_index].y + rectArray[shortest_index].height) - INIT_SFR_DATA[q].tROI.nHeight / 2 < 0)
//							SFR_DATA[q].tROI.nTop = 0;
//						else
//						{
//							int newPos = (rectArray[shortest_index].y + rectArray[shortest_index].height) - INIT_SFR_DATA[q].tROI.nHeight / 2;
//							int dev_y = abs(INIT_SFR_DATA[q].tROI.nTop - newPos);
//
//							if (dev_y < 100)
//							{
//								SFR_DATA[q].tROI.nTop = (rectArray[shortest_index].y + rectArray[shortest_index].height) - INIT_SFR_DATA[q].tROI.nHeight / 1.5; //원복
//							}
//						}
//					}
//				}
//			}
//		}
//	}
//
//	cvReleaseMemStorage(&contour_storage);
//	cvReleaseImage(&OriginImage);
//	cvReleaseImage(&CannyImage);
//	cvReleaseImage(&DilateImage);
//	cvReleaseImage(&SmoothImage);
//	cvReleaseImage(&RGBResultImage);
//	cvReleaseImage(&temp_PatternImage);
//
//	delete[]rectArray;
//}
//
//void CTI_SFR::SearchROIArea(LPBYTE IN_RGB, int tempWidth)
//{
//	IplImage *OriginImage = cvCreateImage(cvSize(tempWidth, m_nHeight), IPL_DEPTH_8U, 1);
//	IplImage *CannyImage = cvCreateImage(cvSize(tempWidth, m_nHeight), IPL_DEPTH_8U, 1);
//	IplImage *DilateImage = cvCreateImage(cvSize(tempWidth, m_nHeight), IPL_DEPTH_8U, 1);
//	IplImage *SmoothImage = cvCreateImage(cvSize(tempWidth, m_nHeight), IPL_DEPTH_8U, 1);
//	IplImage *RGBResultImage = cvCreateImage(cvSize(tempWidth, m_nHeight), IPL_DEPTH_8U, 3);
//	IplImage *temp_PatternImage = cvCreateImage(cvSize(tempWidth, m_nHeight), IPL_DEPTH_8U, 1);
//
//	//IplImage *tempOriginImage = cvCreateImage(cvSize(tempWidth, m_nHeight), IPL_DEPTH_8U, 3);
//
//	BYTE R, G, B;
//	double Sum_Y;
//
//	for (int y = 0; y<m_nHeight; y++)
//	{
//		for (int x = 0; x<tempWidth; x++)
//		{
//			B = IN_RGB[y*(tempWidth * 3) + x * 3];
//			G = IN_RGB[y*(tempWidth * 3) + x * 3 + 1];
//			R = IN_RGB[y*(tempWidth * 3) + x * 3 + 2];
//
//			// 			tempOriginImage->imageData[y * tempOriginImage->widthStep + x*4 ] = B;
//			// 			tempOriginImage->imageData[y * tempOriginImage->widthStep + x*4 +1] = G;
//			// 			tempOriginImage->imageData[y * tempOriginImage->widthStep + x*4 +2] = R;
//
//			Sum_Y = (BYTE)((0.29900*R) + (0.58700*G) + (0.11400*B));
//
//			//	int value = (R+G+B)/3;
//
//			OriginImage->imageData[y*OriginImage->widthStep + x] = Sum_Y;
//		}
//	}
//
//	//	cvSaveImage("D:\\ORIGIANLCAMIMAGE.bmp", OriginImage);
//	//	cvReleaseImage(&tempOriginImage);
//	//	OriginImage = cvLoadImage("C:\\asdasd.bmp", 0);
//
//	//	Capture_Gray_SAVE("C:\\RGBIMAGE", IN_RGB, 720, 480);	
//	//	cvSaveImage("C:\\Default_Image.bmp", OriginImage);
//
//	//	BOOL WideCamMode = GetWideCamCheckFunc(RGBOrgImage);
//
//	//	cvSmooth(OriginImage, SmoothImage, CV_GAUSSIAN, 3, 3, 1, 1);	
//
//	//	cvCanny(SmoothImage, CannyImage, 0, 100);
//
//	cvDilate(OriginImage, OriginImage);
//
//
//	cvThreshold(OriginImage, DilateImage, 0, 255, CV_THRESH_OTSU);
//	cvNot(DilateImage, DilateImage);
//	//	cvCanny(DilateImage, DilateImage, 0, 255);
//
//	//	cvSaveImage("D:\\DilateImage.bmp", DilateImage);
//
//	cvCopyImage(DilateImage, temp_PatternImage);
//
//	cvCvtColor(DilateImage, RGBResultImage, CV_GRAY2BGR);
//
//	CvMemStorage* contour_storage = cvCreateMemStorage(0);
//	CvSeq *contour = 0;
//	CvSeq *temp_contour = 0;
//
//	cvFindContours(DilateImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
//
//	temp_contour = contour;
//
//	int counter = 0;
//
//	for (; temp_contour != 0; temp_contour = temp_contour->h_next)
//		counter++;
//
//	CvRect *rectArray = new CvRect[counter];
//	//	double *areaArray = new double[counter];
//	CvRect rect;
//	double area = 0, arcCount = 0;
//	counter = 0;
//	double old_dist = 999999;
//
//	for (; contour != 0; contour = contour->h_next)
//	{
//		area = cvContourArea(contour, CV_WHOLE_SEQ);
//		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);
//
//		rect = cvContourBoundingRect(contour, 1);
//		double circularity = (4.0*M_PI*area) / (arcCount*arcCount);
//
//		rect = cvContourBoundingRect(contour, 1);
//
//		int center_x, center_y;
//		center_x = rect.x + rect.width / 2;
//		center_y = rect.y + rect.height / 2;
//
//		BYTE R, G, B;
//		B = IN_RGB[center_y*(tempWidth * 3) + center_x * 3];
//		G = IN_RGB[center_y*(tempWidth * 3) + center_x * 3 + 1];
//		R = IN_RGB[center_y*(tempWidth * 3) + center_x * 3 + 2];
//
//		if (rect.width < (tempWidth / 4) && rect.height < (m_nHeight / 4))
//		{
//			double dRatio = (double)rect.width / (double)rect.height * 100.0;
//			if ((rect.width > 30 || rect.height > 30) && dRatio <120)
//			{
//				rectArray[counter] = rect;
//				//						areaArray[counter] = area;
//
//				counter++;
//
//				double dist = GetDistance(center_x, center_y, m_nWidth / 2, m_nHeight / 2);
//
//				if (old_dist > dist)
//				{
//					m_currPt.x = center_x - (m_nWidth / 2);
//					m_currPt.y = center_y - (m_nHeight / 2);
//
//					m_currPt.x = m_currPt.x / 2;
//					m_currPt.y = m_currPt.y / 2;
//
//					old_dist = dist;
//				}
//
//				cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(0, 255, 0), 1, 8);
//			}
//		}
//	}
//
//	int rect_cnt = 0;
//
//	if (counter > 0 && abs(m_currPt.x) < 50 && abs(m_currPt.y) < 50)
//	{
//		for (int q = 0; q < 50; q++)
//		{
//			double old_dist = 9999999;
//			double WHRatio_Thr, HWRatio_Thr;
//			double areaRatio_Thr;
//
//			int shortest_index = -1;
//
//			if (SFR_DATA[q].bEnable == TRUE)
//			{
//				shortest_index = -1;
//
//				for (int k = 0; k<counter; k++)
//				{
//					rect = rectArray[k];
//					double WHRatio = (double)rect.width / (double)rect.height;
//					double HWRatio = (double)rect.height / (double)rect.width;
//					int White_area_cnt = 0;
//					unsigned char pixel_v = 0;
//
//					int curr_rect_center_x = rect.x + rect.width / 2;
//					int curr_rect_center_y = rect.y + rect.height / 2;
//					int roi_x = INIT_SFR_DATA[q].tROI.nLeft + INIT_SFR_DATA[q].tROI.nWidth / 2;
//					int roi_y = INIT_SFR_DATA[q].tROI.nTop + INIT_SFR_DATA[q].tROI.nHeight / 2;
//
//					double dist = GetDistance(curr_rect_center_x, curr_rect_center_y, roi_x, roi_y);
//
//					if (old_dist > dist && dist < 300)
//					{
//						shortest_index = k;
//						old_dist = dist;
//					}
//				}
//				if (shortest_index != -1)
//				{
//					if (SFR_DATA[q].nType == 0)
//					{
//						if (rectArray[shortest_index].x - INIT_SFR_DATA[q].tROI.nWidth / 2 < 0)
//							SFR_DATA[q].tROI.nLeft = 0;
//						else
//						{
//							int newPos = (rectArray[shortest_index].x - INIT_SFR_DATA[q].tROI.nWidth / 2);
//							int dev_x = abs(INIT_SFR_DATA[q].tROI.nLeft - newPos);
//
//							if (dev_x < 100)
//							{
//								SFR_DATA[q].tROI.nLeft = (rectArray[shortest_index].x - INIT_SFR_DATA[q].tROI.nWidth / 3); //VW버전
//							}
//						}
//
//						if ((rectArray[shortest_index].y + rectArray[shortest_index].height / 2) - INIT_SFR_DATA[q].tROI.nHeight / 2 < 0)
//							SFR_DATA[q].tROI.nTop = 0;
//						else
//						{
//							int newPos = (rectArray[shortest_index].y + rectArray[shortest_index].height / 2) - INIT_SFR_DATA[q].tROI.nHeight / 2;
//							int dev_y = abs(INIT_SFR_DATA[q].tROI.nTop - newPos);
//
//							if (dev_y < 100){
//
//								if (q >= 4){
//									SFR_DATA[q].tROI.nTop = (rectArray[shortest_index].y + rectArray[shortest_index].height / 2) - INIT_SFR_DATA[q].tROI.nHeight / 2.0; //원복
//								}
//								else
//								{
//									SFR_DATA[q].tROI.nTop = (rectArray[shortest_index].y + rectArray[shortest_index].height / 2) - INIT_SFR_DATA[q].tROI.nHeight / 2; //VW버전
//								}
//							}
//						}
//					}
//					else if (SFR_DATA[q].nType == 1)
//					{
//						if (rectArray[shortest_index].x + rectArray[shortest_index].width - INIT_SFR_DATA[q].tROI.nWidth / 2 < 0)
//							SFR_DATA[q].tROI.nLeft = 0;
//						else
//						{
//							int newPos = rectArray[shortest_index].x + rectArray[shortest_index].width - INIT_SFR_DATA[q].tROI.nWidth / 2;
//							int dev_x = abs(INIT_SFR_DATA[q].tROI.nLeft - newPos);
//
//							if (dev_x < 100)
//							{
//								SFR_DATA[q].tROI.nLeft = rectArray[shortest_index].x + rectArray[shortest_index].width - INIT_SFR_DATA[q].tROI.nWidth / 1.5; //원복
//							}
//						}
//
//						if ((rectArray[shortest_index].y + rectArray[shortest_index].height / 2) - INIT_SFR_DATA[q].tROI.nHeight / 2 < 0)
//							SFR_DATA[q].tROI.nTop = 0;
//						else
//						{
//							int newPos = (rectArray[shortest_index].y + rectArray[shortest_index].height / 2) - INIT_SFR_DATA[q].tROI.nHeight / 2;
//							int dev_y = abs(INIT_SFR_DATA[q].tROI.nTop - newPos);
//
//							if (dev_y < 100)
//							{
//								SFR_DATA[q].tROI.nTop = (rectArray[shortest_index].y + rectArray[shortest_index].height / 2) - INIT_SFR_DATA[q].tROI.nHeight / 2; //VW 버전
//							}
//						}
//					}
//					else if (SFR_DATA[q].nType == 2)
//					{
//						if (rectArray[shortest_index].x + rectArray[shortest_index].width / 2 - INIT_SFR_DATA[q].tROI.nWidth / 2 < 0)
//							SFR_DATA[q].tROI.nLeft = 0;
//						else
//						{
//							int newPos = rectArray[shortest_index].x + rectArray[shortest_index].width / 2 - INIT_SFR_DATA[q].tROI.nWidth / 2;
//							int dev_x = abs(INIT_SFR_DATA[q].tROI.nLeft - newPos);
//
//							if (dev_x < 100)
//							{
//								SFR_DATA[q].tROI.nLeft = rectArray[shortest_index].x + rectArray[shortest_index].width / 2 - INIT_SFR_DATA[q].tROI.nWidth / 2; //VW버전
//							}
//						}
//
//						if ((rectArray[shortest_index].y) - INIT_SFR_DATA[q].tROI.nHeight / 2 < 0)
//							SFR_DATA[q].tROI.nTop = 0;
//						else
//						{
//							int newPos = (rectArray[shortest_index].y) - INIT_SFR_DATA[q].tROI.nHeight / 2;
//							int dev_y = abs(INIT_SFR_DATA[q].tROI.nTop - newPos);
//
//							if (dev_y < 100)
//								SFR_DATA[q].tROI.nTop = (rectArray[shortest_index].y) - INIT_SFR_DATA[q].tROI.nHeight / 3;
//						}
//					}
//					else if (SFR_DATA[q].nType == 3)
//					{
//						if (q == 8)
//						{
//							int t = 0;
//						}
//						if (rectArray[shortest_index].x + rectArray[shortest_index].width / 2 - INIT_SFR_DATA[q].tROI.nWidth / 2 < 0)
//							SFR_DATA[q].tROI.nLeft = 0;
//						else
//						{
//							int newPos = rectArray[shortest_index].x + rectArray[shortest_index].width / 2 - INIT_SFR_DATA[q].tROI.nWidth / 2;
//							int dev_x = abs(INIT_SFR_DATA[q].tROI.nLeft - newPos);
//
//							if (dev_x < 100)
//							{
//								SFR_DATA[q].tROI.nLeft = rectArray[shortest_index].x + rectArray[shortest_index].width / 2 - INIT_SFR_DATA[q].tROI.nWidth / 2; //VW버전
//							}
//						}
//
//						if ((rectArray[shortest_index].y + rectArray[shortest_index].height) - INIT_SFR_DATA[q].tROI.nHeight / 2 < 0)
//							SFR_DATA[q].tROI.nTop = 0;
//						else
//						{
//							int newPos = (rectArray[shortest_index].y + rectArray[shortest_index].height) - INIT_SFR_DATA[q].tROI.nHeight / 2;
//							int dev_y = abs(INIT_SFR_DATA[q].tROI.nTop - newPos);
//
//							if (dev_y < 100)
//							{
//								SFR_DATA[q].tROI.nTop = (rectArray[shortest_index].y + rectArray[shortest_index].height) - INIT_SFR_DATA[q].tROI.nHeight / 1.5; //원복
//							}
//						}
//					}
//				}
//			}
//		}
//	}
//	
//	cvReleaseMemStorage(&contour_storage);
//	cvReleaseImage(&OriginImage);
//	cvReleaseImage(&CannyImage);
//	cvReleaseImage(&DilateImage);
//	cvReleaseImage(&SmoothImage);
//	cvReleaseImage(&RGBResultImage);
//	cvReleaseImage(&temp_PatternImage);
//
//	delete[]rectArray;
//}

void CTI_SFR::MulMatrix(double **A, double **B, double **C, unsigned int Row, unsigned int Col, unsigned int n, unsigned int Mode)
{
	////  A[Row][n] x B[n][Col] = [Row x n] x [n x Col] = [ Row x Col ] = C[Row][Col]   ///////
	////  Mode :  [ Mode 0 : A x B , Mode 1 : A' x B, Mode 2 : A x B' ] 
	unsigned int i, j, k;

	for (i = 0; i < Row; i++){
		for (j = 0; j < Col; j++){
			C[i][j] = 0;
			for (k = 0; k < n; k++){
				if (Mode == 0)	     C[i][j] += A[i][k] * B[k][j];   // Mode 0 : A  x B
				else if (Mode == 1) C[i][j] += A[k][i] * B[k][j];   // Mode 1 : A' x B
				else if (Mode == 2) C[i][j] += A[i][k] * B[j][k];   // Mode 2 : A  x B' 
			}
		}
	}
}

void CTI_SFR::pInverse(double **A, unsigned int Row, unsigned int Col, double **RtnMat)
{
	unsigned int i;//, j;
	double **AxA, **InvAxA;

	AxA = new double *[Col];
	for (i = 0; i < Col; i++)
		AxA[i] = new double[Col];

	InvAxA = new double *[Col];
	for (i = 0; i < Col; i++)
		InvAxA[i] = new double[Col];

	MulMatrix(A, A, AxA, Col, Col, Row, 1);  // Mode 1 : A' * B

	Inverse(AxA, Col, InvAxA);

	MulMatrix(InvAxA, A, RtnMat, Col, Row, Col, 2);

	for (i = 0; i < Col; i++)
		delete[]InvAxA[i];
	delete[]InvAxA;

	for (i = 0; i < Col; i++)
		delete[]AxA[i];

	delete[] AxA;
}

void CTI_SFR::Inverse(double **dataMat, unsigned int n, double **MatRtn)
{
	unsigned int i, j;
	double *dataA;

	CvMat matA;
	CvMat *pMatB = cvCreateMat(n, n, CV_64F);

	dataA = new double[n*n];

	for (i = 0; i < n; i++)
	for (j = 0; j < n; j++)
		dataA[i*n + j] = dataMat[i][j];

	matA = cvMat(n, n, CV_64F, dataA);

	cvInvert(&matA, pMatB);
	//	PrintMat(&matA, "pMatA = Source");    // Mat print

	for (i = 0; i < n; i++)
	for (j = 0; j < n; j++)
		MatRtn[i][j] = cvGetReal2D(pMatB, i, j);  // A = U * W * VT  ,  MatRtn : VT

	cvReleaseMat(&pMatB);
	delete[]dataA;
}