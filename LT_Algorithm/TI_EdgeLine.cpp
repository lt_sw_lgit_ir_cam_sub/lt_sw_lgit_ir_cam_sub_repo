﻿#include "TI_EdgeLine.h"

#define FIDUCIAL_MARK_SIZE 20

void CTI_EdgeLine::Initialize(WORD* pGRAYScanBuf, int iWidth, int iHeight)
{
#ifdef DEBUG_PRINT_LOG
	printf("[Start] CTI_EdgeLine::Initialize() : pGRAYScanBuf : %d, iWidth : %d, iHeight : %d\n",
		pGRAYScanBuf != NULL ? 1 : 0, iWidth, iHeight);
#endif
	m_pGRAYScanBuf = pGRAYScanBuf;

	m_iWidth = iWidth;
	m_iHeight = iHeight;

#ifdef DEBUG_PRINT_LOG
	printf("[End] CTI_EdgeLine::Initialize()\n");
#endif
}

ST_CAN_EdgeLine_Result CTI_EdgeLine::Inspection(ST_CAN_EdgeLine_Opt stData)
{
	// ROI INPUT
	m_stData = stData;
	m_stResult.stHeader.iTestItem = stData.stHeader.iTestItem;
	m_stResult.byRoiCnt = m_stData.byRoiCnt;

#ifdef DEBUG_PRINT_LOG
	printf("[Start] CTI_EdgeLine::Inspection()\n");
	for (int i = 0; i < m_stData.byRoiCnt; i++)
	{
		if (CheckROI(m_stData.stROI[i]) == false)
			continue;

		printf("[Info] CTI_EdgeLine::Inspection() : Cnt : %d, ROI[%d](L_%d, T_%d, W_%d, H_%d), Color : %d, Thre : %d, Brightness : %d, CenterX : %d, CenterY : %d\n",
			m_stData.byRoiCnt, i, m_stData.stROI[i].sLeft, m_stData.stROI[i].sTop, m_stData.stROI[i].sWidth,
			m_stData.stROI[i].sHeight, m_stData.byDetectColor[i], m_stData.byThreshold[i], m_stData.byBrightness[i],
			m_stData.sCenterX[i], m_stData.sCenterY[i]);
	}
#endif

	// TEST
	CPoint ptResult;

	for (int i = 0; i < m_stResult.byRoiCnt; i++)
	{
		if (CheckROI(m_stData.stROI[i]) == false)
			continue;

		ptResult = EdgeLine_Test(m_stData.stROI[i], m_stData.sCenterX[i], m_stData.sCenterY[i],
			m_stData.byDetectColor[i], m_stData.byBrightness[i], m_stData.byThreshold[i]);
		m_stResult.sLineCenterX[i] = (unsigned short)ptResult.x;
		m_stResult.sLineCenterY[i] = (unsigned short)ptResult.y;

#ifdef DEBUG_PRINT_LOG
		printf("[Run] CTI_EdgeLine::Inspection() : m_stResult.sLineCenterX[%d] : %d, m_stResult.sLineCenterY[%d] : %d\n",
			i, m_stResult.sLineCenterX[i], i, m_stResult.sLineCenterY[i]);
#endif
	}

	// Result
	m_stResult.wResultCode = RC_OK;

#ifdef DEBUG_PRINT_LOG
	printf("[End] CTI_EdgeLine::Inspection()\n");
#endif

	return m_stResult;
}

//=============================================================================
// Method		: EdgeLine_Test
// Access		: public  
// Returns		: CPoint
// Parameter	: ST_CAN_ROI stROI
// Parameter	: int iCenterX
// Parameter	: int iCenterY
// Parameter	: BYTE byDetectColor
// Parameter	: BYTE byBrightness
// Parameter	: BYTE byThreshold
// Qualifier	:
// Last Update	: 2017/11/5 - 13:21
// Desc.		:
//=============================================================================
CPoint CTI_EdgeLine::EdgeLine_Test(ST_CAN_ROI stROI, int iCenterX, int iCenterY, BYTE byDetectColor, BYTE byBrightness, BYTE byThreshold)
{
	CPoint ptCenter;

	// 초기값
	ptCenter.x = 0;
	ptCenter.y = 0;

	IplImage *OriginImage	 = cvCreateImage(cvSize(m_iWidth, m_iHeight), IPL_DEPTH_8U, 1);
	IplImage *ThresholdImage = cvCreateImage(cvSize(m_iWidth, m_iHeight), IPL_DEPTH_8U, 1);

	WORD wData16Bit;
	BYTE byData8Bit;

	cvSetZero(OriginImage);

	for (int y = 0; y < m_iHeight; y++)
	{
		for (int x = 0; x < m_iWidth; x++)
		{
			wData16Bit = *(m_pGRAYScanBuf + y * m_iWidth + x);

			wData16Bit = wData16Bit & 0xF000 ? 0x0FFF : wData16Bit;
			byData8Bit = (wData16Bit >> 4) & 0x00FF;

			if (byBrightness >= 0)
				byData8Bit = (byData8Bit << byBrightness) >= 0xFF ? 0xFF : (byData8Bit << byBrightness);
			else
				byData8Bit = (byData8Bit >> (-1 * byBrightness)) <= 0x00 ? 0x00 : (byData8Bit >> (-1 * byBrightness));


			OriginImage->imageData[y * OriginImage->widthStep + x] = byData8Bit;
		}
	}

	cvThreshold(OriginImage, ThresholdImage, byThreshold, 255, CV_THRESH_BINARY);

	//변화가 있는 영역
	std::vector<int> vChange_Pos;
	vChange_Pos.clear();
	//CArray<int, int&> nArray_Change_Pos;
	int iTempData  = -1;
	int iImageData = 0;
	int iOri_Data  = 0;

	switch (byDetectColor)
	{
	case Line_LR_WB:
	{
		for (int x = stROI.sLeft; x < stROI.sLeft + stROI.sWidth; x++)
		{
			iOri_Data = ThresholdImage->imageData[iCenterY*ThresholdImage->widthStep + x];
			iImageData = abs(iOri_Data);
			if (iImageData != iTempData)
			{
				vChange_Pos.push_back(x);
				//nArray_Change_Pos.Add(x);

				iTempData = iImageData;
			}
		}

		//Array에 담은 것을 역으로 Searching
		int nChangeCnt = (int)vChange_Pos.size();
		//int nChangeCnt = nArray_Change_Pos.GetCount();

		iTempData = 1;

		if (nChangeCnt > 0)
		{
			int nData_X = vChange_Pos.at(nChangeCnt - 1);
			//int nData_X = nArray_Change_Pos.GetAt(nChangeCnt - 1);

			for (int t = nData_X - 2; t < nData_X + 2; t++)
			{
				if (t < m_iWidth)
				{
					iOri_Data = ThresholdImage->imageData[iCenterY*ThresholdImage->widthStep + t];
					iImageData = abs(iOri_Data);
					if (t == nData_X - 2 && iImageData == 0)// 첫번째 데이터가 검은색이면 Searching Fail
					{
						break;
					}

					if (iImageData != iTempData)
					{
						ptCenter.x = t;
						ptCenter.y = iCenterY;
						break;
					}
				}
			}
		}
	}
	break;
	case Line_LR_BW:
	{
		for (int x = stROI.sLeft + stROI.sWidth - 1; x > stROI.sLeft; x--)
		{
			iOri_Data = ThresholdImage->imageData[iCenterY * ThresholdImage->widthStep + x];
			iImageData = abs(iOri_Data);
			if (iImageData != iTempData)
			{
				vChange_Pos.push_back(x);
				//nArray_Change_Pos.Add(x);

				iTempData = iImageData;
			}
		}

		//Array에 담은 것을 역으로 Searching
		int nChangeCnt = (int)vChange_Pos.size();
		//int nChangeCnt = nArray_Change_Pos.GetCount();

		iTempData = 1;

		if (nChangeCnt > 0)
		{
			int nData_X = vChange_Pos.at(nChangeCnt - 1);
			//int nData_X = nArray_Change_Pos.GetAt(nChangeCnt - 1);

			for (int t = nData_X + 2; t > nData_X - 2; t--)
			{
				if (t >= 0)
				{
					iOri_Data = ThresholdImage->imageData[iCenterY*ThresholdImage->widthStep + t];
					iImageData = abs(iOri_Data);
					if ((t == nData_X + 2 && iImageData == 0))// 첫번째 데이터가 검은색이면 Searching Fail
					{
						break;
					}

					if (iImageData != iTempData)
					{
						ptCenter.x = t;
						ptCenter.y = iCenterY;
						break;
					}
				}
			}
		}
	}
		break;
	case Line_TB_BW:
	{
		for (int y = stROI.sTop; y < stROI.sTop + stROI.sHeight; y++)
		{
			iOri_Data = ThresholdImage->imageData[y*ThresholdImage->widthStep + iCenterX];
			iImageData = abs(iOri_Data);
			if (iImageData != iTempData)
			{
				vChange_Pos.push_back(y);
				//nArray_Change_Pos.Add(y);

				iTempData = iImageData;
			}
		}

		//Array에 담은 것을 역으로 Searching
		int nChangeCnt = (int)vChange_Pos.size();
		//int nChangeCnt = nArray_Change_Pos.GetCount();

		iTempData = 0;

		if (nChangeCnt > 0)
		{
			int nData_Y = vChange_Pos.at(nChangeCnt - 1);
			//int nData_Y = nArray_Change_Pos.GetAt(nChangeCnt - 1);

			for (int t = nData_Y - 2; t < nData_Y + 2; t++)
			{
				if (t < m_iHeight)
				{
					iOri_Data = ThresholdImage->imageData[t*ThresholdImage->widthStep + iCenterX];
					iImageData = abs(iOri_Data);
					if ((t == nData_Y - 2 && iImageData == 1))// 첫번째 데이터가 흰색이 경우에만.
					{
						break;
					}

					if (iImageData != iTempData)
					{
						ptCenter.x = iCenterX;
						ptCenter.y = t;
						break;
					}
				}
			}
		}

		break;
	}
	case Line_TB_WB:
	{
		for (int y = stROI.sTop + stROI.sWidth - 1; y > stROI.sTop; y--)
		{
			iOri_Data = ThresholdImage->imageData[y*ThresholdImage->widthStep + iCenterX];
			iImageData = abs(iOri_Data);
			if (iImageData != iTempData)
			{
				vChange_Pos.push_back(y);
				//nArray_Change_Pos.Add(y);

				iTempData = iImageData;
			}
		}

		//Array에 담은 것을 역으로 Searching
		int nChangeCnt = (int)vChange_Pos.size();
		//int nChangeCnt = nArray_Change_Pos.GetCount();

		iTempData = 0;

		if (nChangeCnt > 0)
		{
			int nData_Y = vChange_Pos.at(nChangeCnt - 1);
			//int nData_Y = nArray_Change_Pos.GetAt(nChangeCnt - 1);

			for (int t = nData_Y + 2; t > nData_Y - 2; t--)
			{
				if (t >= 0)
				{
					iOri_Data = ThresholdImage->imageData[t*ThresholdImage->widthStep + iCenterX];
					iImageData = abs(iOri_Data);
					if ((t == nData_Y + 2 && iImageData == 1))// 첫번째 데이터가 흰색이 경우에만.
					{
						break;
					}

					if (iImageData != iTempData)
					{
						ptCenter.x = iCenterX;
						ptCenter.y = t;
						break;
					}
				}
			}
		}

	}
		break;
	}

	cvReleaseImage(&ThresholdImage);
	cvReleaseImage(&OriginImage);
	
	return ptCenter;
}

//=============================================================================
// Method		: GetDistance
// Access		: public  
// Returns		: double
// Parameter	: int ix1
// Parameter	: int iy1
// Parameter	: int ix2
// Parameter	: int iy2
// Qualifier	:
// Last Update	: 2017/1/12 - 17:23
// Desc.		:
//=============================================================================
double CTI_EdgeLine::GetDistance(int ix1, int iy1, int ix2, int iy2)
{
	double result;

	result = sqrt((double)((ix2 - ix1)*(ix2 - ix1)) + ((iy2 - iy1)*(iy2 - iy1)));

	return result;
}
