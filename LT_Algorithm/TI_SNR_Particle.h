#pragma once
#include "TI_BaseAlgorithm.h"

class CTI_SNR_Particle : CTI_BaseAlgorithm
{
private:
	// 계산 결과값 저장
	int			m_iSignal[SNR_Particle_Idx_Max];
	double		m_dbNoise[SNR_Particle_Idx_Max];

	ST_CAN_ROI		m_stROI[SNR_Particle_Idx_Max];

	// Input 데이터
	ST_CAN_SNR_Particle_Opt		m_stData;

	// Output 데이터
	ST_CAN_SNR_Particle_Result	m_stResult;

private:
	void MakeSection(ST_CAN_ROI* pROI);
	void SetROI(bool* pUseIndex);

public:
	virtual void				Initialize(WORD* pGRAYScanBuf, int iWidth, int iHeight);
	ST_CAN_SNR_Particle_Result	Inspection(ST_CAN_SNR_Particle_Opt stData);

	void SignalnNoiseGen(ST_CAN_ROI stROI, unsigned short &sSignal, float& fNoise);
};
