#include "TI_RelativeIllumination.h"

void CTI_RelativeIllumination::Initialize(WORD* pGRAYScanBuf, int iWidth, int iHeight)
{
#ifdef DEBUG_PRINT_LOG
	printf("[Start] CTI_RelativeIllumination::Initialize() : pGRAYScanBuf : %d, iWidth : %d, iHeight : %d\n",
		pGRAYScanBuf != NULL ? 1 : 0, iWidth, iHeight);
#endif
	m_pGRAYScanBuf = pGRAYScanBuf;

	m_iWidth = iWidth;
	m_iHeight = iHeight;

#ifdef DEBUG_PRINT_LOG
	printf("[End] CTI_RelativeIllumination::Initialize()\n");
#endif
}

ST_CAN_RelativeIllumination_Result CTI_RelativeIllumination::Inspection(ST_CAN_RelativeIllumination_Opt stData)
{
#ifdef DEBUG_PRINT_LOG
	printf("[Start] CTI_RelativeIllumination::Inspection()\n");
	for (int i = 0; i < RelativeIllumination_Idx_Max; i++)
	{
		printf("[Info] CTI_RelativeIllumination::Inspection() : bRoiIndex[%d] : %d\n",
			i, stData.bRoiIndex[i]);
	}
#endif
	// ROI INPUT
	m_stData = stData;
	m_stResult.stHeader.iTestItem = stData.stHeader.iTestItem;

	// TEST
	// 검사 영역 설정
	SetROI(m_stData.bRoiIndex);

	// Signal, Noise 계산
	for (int iRoiCnt = 0; iRoiCnt < RelativeIllumination_Idx_Max; iRoiCnt++)
	{
		// 사용하는 항목만 계산
		if (m_stData.bRoiIndex[iRoiCnt] == true)
			SignalnNoiseGen(m_stROI[iRoiCnt], m_stResult.sSignal[iRoiCnt], m_stResult.fNoise[iRoiCnt]);
		// 사용하지 않는 항목은 0으로 설정
		else
		{
			m_stResult.sSignal[iRoiCnt] = 0;
			m_stResult.fNoise[iRoiCnt] = 0.0f;
		}
#ifdef DEBUG_PRINT_LOG
		printf("[Run] CTI_RelativeIllumination::Inspection() : Signal[%d] %d, Noise[%d] : %.2f\n",
			iRoiCnt, m_stResult.sSignal[iRoiCnt], iRoiCnt, m_stResult.fNoise[iRoiCnt]);
#endif
	}

	// Result
	m_stResult.wResultCode = RC_OK;

#ifdef DEBUG_PRINT_LOG
	printf("[End] CTI_RelativeIllumination::Inspection()\n");
#endif

	return m_stResult;
}

void CTI_RelativeIllumination::SignalnNoiseGen(ST_CAN_ROI stROI, unsigned short &sSignal, float& fNoise)
{
	double dbData = 0.0;
	double dbVal = 0.0;

	for (int y = stROI.sTop; y < stROI.sTop + stROI.sHeight; y++)
	{
		for (int x = stROI.sLeft; x < stROI.sLeft + stROI.sWidth; x++)
		{
			dbData += m_pGRAYScanBuf[y * m_iWidth + x];
		}
	}

	sSignal = (short)(dbData / (double)(stROI.sWidth * stROI.sHeight));
	dbData = 0.0;

	for (int y = stROI.sTop; y < stROI.sTop + stROI.sHeight; y++)
	{
		for (int x = stROI.sLeft; x < stROI.sLeft + stROI.sWidth; x++)
		{
			dbData = abs(sSignal - m_pGRAYScanBuf[y * m_iWidth + x]);
			dbVal += dbData * dbData;
		}
	}

	fNoise = (float)(sqrt(dbVal / (double)(stROI.sWidth * stROI.sHeight)));
}

void CTI_RelativeIllumination::SetROI(bool* pUseIndex)
{
	ST_CAN_ROI stAllROI[SECTION_NUM_X * SECTION_NUM_Y];
	MakeSection(stAllROI);

	int nRelativeIllumination[] = { 22, 31, 40, 44, 52, 60, 66, 73, 80, 88, 94, 100, 110,
		115, 120, 132, 136, 140, 154, 157, 160, 176, 178, 180, 198,
		199, 200, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220,
		221, 222, 223, 224, 225, 226, 227, 228, 229, 240, 241, 242,
		260, 262, 264, 280, 283, 286, 300, 304, 308, 320, 325, 330,
		340, 346, 352, 360, 367, 374, 380, 388, 396, 400, 409, 418 };

	for (int i = 0; i < RelativeIllumination_Idx_Max; i++)
	{
		if (pUseIndex[i] == true)
			m_stROI[i] = stAllROI[nRelativeIllumination[i]];

		//printf("[%3d, %3d] Left : %3d, Top : %3d, Width : %3d, Height : %3d\n", i, nRelativeIllumination[i],
		//	m_stROI[i].wLeft, m_stROI[i].wTop,
		//	m_stROI[i].wWidth, m_stROI[i].wHeight);
	}
}

void CTI_RelativeIllumination::MakeSection(ST_CAN_ROI* pROI)
{
	// 섹션 사이즈
	int nSectionSizeX = m_iWidth / (SECTION_NUM_X - 1);
	int nSectionSizeY = m_iHeight / (SECTION_NUM_Y - 1);
	// 위치값을 누적하여 계산
	int nPosX = 0, nPosY = 0;

	int nIndex = 0;

	for (int i = 0; i < SECTION_NUM_Y; i++)
	{
		nPosX = 0;

		// 좌표 : 가로 줄
		if (i == 0)
			nPosY = 0;
		else if (i != 1)
			nPosY += nSectionSizeY;
		else
			nPosY += nSectionSizeY / 2;

		for (int j = 0; j < SECTION_NUM_X; j++)
		{
			nIndex = i * SECTION_NUM_X + j;

			// 좌표 : 세로 줄
			if (j == 0)
				nPosX = 0;
			else if (j != 1)
				nPosX += nSectionSizeX;
			else
				nPosX += nSectionSizeX / 2;

			// 사이즈 : 세로 줄
			if (j == 0 || j == SECTION_NUM_X - 1)
				pROI[nIndex].sWidth = nSectionSizeX / 2;
			else
				pROI[nIndex].sWidth = nSectionSizeX;
			// 사이즈 : 가로 줄
			if (i == 0 || i == SECTION_NUM_Y - 1)
				pROI[nIndex].sHeight = nSectionSizeY / 2;
			else
				pROI[nIndex].sHeight = nSectionSizeY;

			pROI[nIndex].sTop = nPosY;
			pROI[nIndex].sLeft = nPosX;

		//	printf("[%2d, %2d] Left : %3d, Top : %3d, Width : %3d, Height : %3d\n", i, j,
		//		pROI[nIndex].wLeft, pROI[nIndex].wTop, pROI[nIndex].wWidth, pROI[nIndex].wHeight);
		}
	}
}