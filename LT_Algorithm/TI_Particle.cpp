#include "TI_Particle.h"

// 이물 종류 별 최대 개수
#define MAX_PARTICLE_COUNT	20

void CTI_Particle::Initialize(WORD* pGRAYScanBuf, int iWidth, int iHeight)
{
	m_pGRAYScanBuf = pGRAYScanBuf;

	m_iWidth = iWidth;
	m_iHeight = iHeight;

	m_nCounter = 0;
}

ST_CAN_Particle_Result CTI_Particle::Inspection(ST_CAN_Particle_Opt stData)
{
#ifdef DEBUG_PRINT_LOG
	printf("[Start] CTI_Particle::Inspection()\n");
	for (int i = 0; i < Particle_ROI_Max; i++)
	{
		printf("[Info] CTI_Particle::Inspection() : ROI[%d](L_%d, T_%d, W_%d, H_%d), bEllipse : %d, fThreDark : %f, fThreConcentration : %f, fThreSize : %f\n",
			i, stData.stROI[i].sLeft, stData.stROI[i].sTop, stData.stROI[i].sWidth,
			stData.stROI[i].sHeight, stData.bEllipse[i], stData.fThreDark[i],
			stData.fThreConcentration[i], stData.fThreSize[i]);
	}
#endif
	// ROI INPUT
	m_stData = stData;
	m_stResult.stHeader.iTestItem = stData.stHeader.iTestItem;

	m_Area = new char*[m_iWidth];
	for (int i = 0; i < m_iWidth; i++)
		m_Area[i] = new char[m_iHeight];

	// TEST
	ParticleGen_16bit();

#ifdef DEBUG_PRINT_LOG
	for (int i = 0; i < m_stResult.byRoiCnt; i++)
	{
		printf("[Run] CTI_Particle::Inspection() : ROI[%d](L_%d, T_%d, W_%d, H_%d), byType : %d, fConcentration : %f\n",
			i, m_stResult.stROI[i].sLeft, m_stResult.stROI[i].sTop, m_stResult.stROI[i].sWidth,
			m_stResult.stROI[i].sHeight, m_stResult.byType[i], m_stResult.fConcentration[i]);
	}
#endif

	for (int i = 0; i < m_iWidth; i++)
		SAFE_DELETE_ARRAY(m_Area[i]);
	SAFE_DELETE_ARRAY(m_Area);

	// Result
	m_stResult.wResultCode = RC_OK;

#ifdef DEBUG_PRINT_LOG
	printf("[End] CTI_Particle::Inspection()\n");
#endif

	return m_stResult;
}

void CTI_Particle::ParticleGen_16bit()
{
	float** Data;
	ST_CAN_ROI* stROI;
	float* fConcentration;

	int i;

	int nGroup = 0;
	long nCount = 0;

	int nResultCount = 0;

	// 검사 영역 구분
	SetTestArea(m_stData.stROI);

	// 이물 검출
	Lump_Detection();

	// 선언
	stROI = new ST_CAN_ROI[m_nCounter];
	fConcentration = new float[m_nCounter];

	for (i = 0; i < m_nCounter; i++)
	{
		stROI[i].sLeft = m_pContour[i].x;
		stROI[i].sTop = m_pContour[i].y;
		stROI[i].sWidth = m_pContour[i].width;
		stROI[i].sHeight = m_pContour[i].height;

		nCount++;
	}
	delete[] m_pContour;

	Data = new float*[m_iHeight];
	for (i = 0; i < m_iHeight; i++)
		Data[i] = new float[m_iWidth];
	for (int lopy = 0; lopy < m_iHeight; lopy++)
	{
		for (int lopx = 0; lopx < m_iWidth; lopx++)
		{
			Data[lopy][lopx] = (float)m_pGRAYScanBuf[lopy * m_iWidth + lopx];
		}
	}

	///신규 농도 추가 1005
	nResultCount = nGroup = m_nCounter;

	int Mid_x = 0;
	int Mid_y = 0;

	for (int lop = 0; lop < nGroup; lop++)
	{
		int centerSum = 0, centerCount = 0; double center;
		int side1_Sum = 0, side1_Count = 0; double side;
		int side2_Sum = 0, side2_Count = 0;

		int size_x = stROI[lop].sWidth;
		int size_y = stROI[lop].sHeight;

		for (int y = stROI[lop].sTop - size_y; y <= stROI[lop].sTop + stROI[lop].sHeight + size_y; y++)
		{
			for (int x = stROI[lop].sLeft - size_x; x <= stROI[lop].sLeft + stROI[lop].sWidth + size_x; x++)
			{
				if (y > 0 && y < m_iHeight && x > 0 && x < m_iWidth)
				{
					side1_Sum += (int)Data[y][x];
					side1_Count++;
				}
			}
		}

		for (int y = stROI[lop].sTop; y <= stROI[lop].sTop + stROI[lop].sHeight; y++)
		{
			for (int x = stROI[lop].sLeft; x <= stROI[lop].sLeft + stROI[lop].sWidth; x++)
			{
				if (y > 0 && y < m_iHeight && x > 0 && x < m_iWidth)
				{
					side2_Sum += (int)Data[y][x];
					side2_Count++;
				}
			}
		}

		for (int y = stROI[lop].sTop; y <= stROI[lop].sTop + stROI[lop].sHeight; y++)
		{
			for (int x = stROI[lop].sLeft; x <= stROI[lop].sLeft + stROI[lop].sWidth; x++)
			{
				if (y > 0 && y < m_iHeight && x > 0 && x < m_iWidth)
				{
					centerSum += (int)Data[y][x];
					centerCount++;
				}
			}
		}

		center = (double)centerSum / (double)centerCount;
		side = (double)(side1_Sum - side2_Sum) / (double)(side1_Count - side2_Count);

		fConcentration[lop] = (float)(100 - (center / side) * 100);

		if (fConcentration[lop] > 100)
			fConcentration[lop] = 100;
		else if (fConcentration[lop] < 0)
			fConcentration[lop] = 0;
	}

	for (i = 0; i < m_iHeight; i++)
		SAFE_DELETE_ARRAY(Data[i]);
	SAFE_DELETE_ARRAY(Data);


	bool bBreakCheck = false;

	// [Input] 멍 농도
	for (int lop = 0; lop < nGroup; lop++)
	{
		for (int lop3 = 0; lop3 < nResultCount; lop3++)
		{
			int Center_X = stROI[lop3].sLeft + (stROI[lop3].sWidth) / 2;
			int Center_Y = stROI[lop3].sTop + (stROI[lop3].sHeight) / 2;

			bBreakCheck = false;

			// 영역 별 체크
			for (i = 0; i < Particle_ROI_Max; i++)
			{
				if (fConcentration[lop3] <= m_stData.fThreConcentration[i] && m_Area[Center_X][Center_Y] == i + 1 && fConcentration[lop3] >= 0)
				{
					for (int lop2 = lop3; lop2 < nResultCount; lop2++)
					{
						stROI[lop2].sLeft = stROI[lop2 + 1].sLeft;
						stROI[lop2].sTop = stROI[lop2 + 1].sTop;
						stROI[lop2].sWidth = stROI[lop2 + 1].sWidth;
						stROI[lop2].sHeight = stROI[lop2 + 1].sHeight;

						fConcentration[lop2] = fConcentration[lop2 + 1];
					}

					nResultCount--;
					bBreakCheck = true;
					break;
				}
			}

			if (bBreakCheck == true)
				break;
		}
	}

	bool* bPass = new bool[nResultCount];
	for (int i = 0; i < nResultCount; i++)
		bPass[i] = true;

	// 타입 분류
	ParticleCluster(stROI, fConcentration, nResultCount, bPass);

	// 최종 결과 카피
	for (i = 0; i < nResultCount; i++)
	{
		if (bPass[i] == true)
			continue;

		m_stResult.stROI[i] = stROI[i];
		m_stResult.fConcentration[i] = fConcentration[i];

		m_stResult.byRoiCnt++;
	}
	
	SAFE_DELETE_ARRAY(bPass);
	SAFE_DELETE_ARRAY(stROI);
	SAFE_DELETE_ARRAY(fConcentration);
}

void CTI_Particle::ParticleCluster(ST_CAN_ROI* pROI, float* pConcentration, int nParticleCount, bool* pPassIndex)
{
	int nArea = 0;
	double dbStainThresold = 20.0;
	double dbDeadPixelThresold = 70.0;
	
	// 타입 별 갯수 - 전체 분류용
	int nNumOfEveryCluster[Particle_Type_Max] = { 0, };
	// 타입 종류
	int* nType = new int[nParticleCount];

	for (int i = 0; i < nParticleCount; i++)
	{
		// 초기화
		nType[i] = 0;

		// 예외처리
		if (pConcentration[i] > 100)
			pConcentration[i] = 100;
		else if (pConcentration[i] < 0)
			pConcentration[i] = 0;

		nArea = pROI[i].sWidth * pROI[i].sHeight;

		// Blemish
		nType[i] = Particle_Type_Blemish;

		// Stain
		if (pConcentration[i] > dbStainThresold)
		{
			nType[i] = Particle_Type_Stain;
			nNumOfEveryCluster[Particle_Type_Stain]++;
		}

		// Dead Pixel
		if (nArea < 5 && pConcentration[i] > dbDeadPixelThresold)
		{
			if (nNumOfEveryCluster[Particle_Type_Stain] >= MAX_PARTICLE_COUNT)
				continue;

			nType[i] = Particle_Type_DeadPixel;
			nNumOfEveryCluster[Particle_Type_DeadPixel]++;
		}
	}

	nNumOfEveryCluster[Particle_Type_Blemish] = nParticleCount - nNumOfEveryCluster[Particle_Type_Stain] - nNumOfEveryCluster[Particle_Type_DeadPixel];
	
	if (nNumOfEveryCluster[Particle_Type_Blemish] < 0)
		nNumOfEveryCluster[Particle_Type_Blemish] = 0;

	// 타입 별 갯수 - 갯수 제한용
	int nTypeCount[Particle_Type_Max] = { 0, };

	// 타입 별 최대 갯수 제한
	int nCount = 0;
	for (int i = 0; i < nParticleCount; i++)
	{
		if (nType[i] == Particle_Type_Stain && nTypeCount[Particle_Type_Stain]++ >= MAX_PARTICLE_COUNT)
			continue;
		if (nType[i] == Particle_Type_Blemish && nTypeCount[Particle_Type_Blemish]++ >= MAX_PARTICLE_COUNT)
			continue;
		if (nType[i] == Particle_Type_DeadPixel && nTypeCount[Particle_Type_DeadPixel]++ >= MAX_PARTICLE_COUNT)
			continue;
		
		pPassIndex[i] = false;
		m_stResult.byType[nCount++] = nType[i];
	}

	SAFE_DELETE_ARRAY(nType);
}

void CTI_Particle::Lump_Detection()
{
	IplImage *OriginImage = cvCreateImage(cvSize(m_iWidth, m_iHeight), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(m_iWidth, m_iHeight), IPL_DEPTH_8U, 1);
	IplImage *RGBResultImage = cvCreateImage(cvSize(m_iWidth, m_iHeight), IPL_DEPTH_8U, 3);

	WORD wData16Bit;
	BYTE byData8Bit;
	
	for (int y = 0; y < m_iHeight; y++)
	{
		for (int x = 0; x < m_iWidth; x++)
		{
			wData16Bit = *(m_pGRAYScanBuf + y * m_iWidth + x);

			wData16Bit = wData16Bit & 0xF000 ? 0x0FFF : wData16Bit;
			byData8Bit = (wData16Bit >> 4) & 0x00FF;

			//if (byBrightness >= 0)
			//	byData8Bit = (byData8Bit << byBrightness) >= 0xFF ? 0xFF : (byData8Bit << byBrightness);
			//else
			//	byData8Bit = (byData8Bit >> (-1 * byBrightness)) <= 0x00 ? 0x00 : (byData8Bit >> (-1 * byBrightness));

			OriginImage->imageData[y * OriginImage->widthStep + x] = byData8Bit;
		}
	}

	cvAdaptiveThreshold(OriginImage, DilateImage, 255, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 21/*31:홀수*/, m_stData.fSensitivity/*2*/);
	cvNot(DilateImage, DilateImage);

	cvCvtColor(DilateImage, RGBResultImage, CV_GRAY2BGR);

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;

	cvFindContours(DilateImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	temp_contour = contour;


	for (; temp_contour != 0; temp_contour = temp_contour->h_next)
	{
		m_nCounter++;
	}

	m_pContour = new CvRect[m_nCounter];

	double area = 0, arcCount = 0;
	m_nCounter = 0;
	double old_dist = 999999;

	CvRect	rect;
	CPoint	ptCurrPt;

	for (; contour != 0; contour = contour->h_next)
	{
		area = cvContourArea(contour, CV_WHOLE_SEQ);
		arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);

		// 기존엔 2번 호출되고 있었음
		//rect = cvContourBoundingRect(contour, 1);
		rect = cvContourBoundingRect(contour, 1);

		int center_x, center_y;
		center_x = rect.x + rect.width / 2;
		center_y = rect.y + rect.height / 2;
		cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(255, 0, 0), 1, 8);

		BOOL Rate_Part = FALSE;
		double  Rate = 0;

		if (rect.width > rect.height)
			Rate = (double)rect.width / rect.height;
		else
			Rate = (double)rect.height / rect.width;

		if (Rate >= 9)
			Rate_Part = TRUE;

		//Size -> 
		if (rect.width < (m_iWidth / 4) && rect.height < (m_iHeight / 4))
		{
			int Center_X = rect.x + rect.width / 2;
			int Center_Y = rect.y + rect.height / 2;

			for (int i = 0; i < Particle_ROI_Max; i++)
			{
				// [Input] 멍 크기
				if (rect.width > m_stData.fThreSize[i] && rect.height > m_stData.fThreSize[i] && m_Area[Center_X][Center_Y] == i + 1 && Rate_Part == FALSE)
				{
					m_pContour[m_nCounter] = rect;

					m_nCounter++;

					double dist = GetDistance(center_x, center_y, m_iWidth / 2, m_iHeight / 2);

					if (old_dist > dist)
					{
						ptCurrPt.x = center_x - (m_iWidth / 2);
						ptCurrPt.y = center_y - (m_iHeight / 2);

						ptCurrPt.x = ptCurrPt.x / 2;
						ptCurrPt.y = ptCurrPt.y / 2;

						old_dist = dist;
					}

					cvRectangle(RGBResultImage, cvPoint(rect.x, rect.y), cvPoint(rect.x + rect.width, rect.y + rect.height), CV_RGB(0, 255, 0), 1, 8);
				}
			}
		}
	}

	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&OriginImage);
	cvReleaseImage(&DilateImage);
	cvReleaseImage(&RGBResultImage);
}

void CTI_Particle::SetTestArea(ST_CAN_ROI* pArea)
{
	IplImage *pFieldImage = cvCreateImage(cvSize(m_iWidth, m_iHeight), IPL_DEPTH_8U, 3);

	for (int y = 0; y < m_iHeight; y++)
	{
		for (int x = 0; x < m_iWidth; x++)
		{
			m_Area[x][y] = 0;
			pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 0] = (char)255;
			pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 1] = (char)255;
			pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 2] = (char)255;

			////////////// 최외각 라인에 안 들어온
			if ((pArea[0].sTop <= y) && (pArea[0].sTop + pArea[0].sHeight >= y))
			{
				if ((pArea[0].sLeft <= x) && (pArea[0].sLeft + pArea[0].sWidth >= x))
				{
					////////////////외각 MIDDLE 라인에 안 들어온
					if ((pArea[1].sTop <= y) && (pArea[1].sTop + pArea[1].sHeight >= y))
					{
						if ((pArea[1].sLeft <= x) && (pArea[1].sLeft + pArea[1].sWidth >= x))
						{
							//////////////////////////////  MIDDLE_RECT의 사각, 타원에 따라 m_Area 설정 :: 영역 안 = 2, 영역 밖 = 3  //////////////////////////////////////////////////
							if (m_stData.bEllipse[1] == true)
							{ // m_Ellipse -> ROI 영역 체크박스를 타원으로 했을 때.
								if (EllipseDistanceSum(pArea[1].sLeft + pArea[1].sWidth / 2, pArea[1].sTop + pArea[1].sHeight / 2,
									((double)pArea[1].sWidth / 2.0), ((double)pArea[1].sHeight / 2.0), x, y) <= 1)
								{//X, Y 가 타원 안에 들어온다면
									m_Area[x][y] = 2; // B영역
									pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 0] = 0;
									pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 1] = (char)255;
									pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 2] = (char)255;
								}
								else
								{
									m_Area[x][y] = 3; // C영역
									pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 0] = 0;
									pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 1] = (char)255;
									pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 2] = 0;

								}
							}// ROI 영역이 사각형일때
							else
							{
								m_Area[x][y] = 2; // B영역
								pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 0] = 0;
								pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 1] = (char)255;
								pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 2] = (char)255;
							}
							///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
							////////////////CENTER 라인에 들어온다면
							if ((pArea[2].sTop <= y) && (pArea[2].sTop + pArea[2].sHeight >= y)){
								if ((pArea[2].sLeft <= x) && (pArea[2].sLeft + pArea[2].sWidth >= x)){
									//////////////////////////////  R_RECT의 사각, 타원에 따라 m_Area 설정 :: 영역 안 = 1, 영역 밖 = 2  //////////////////////////////////////////////////
									if (m_stData.bEllipse[2] == true)
									{ // m_Ellipse -> ROI 영역 체크박스를 타원으로 했을 때.
										if (EllipseDistanceSum(pArea[2].sLeft + pArea[2].sWidth / 2, pArea[2].sTop + pArea[2].sHeight / 2,
											((double)pArea[2].sWidth / 2.0), ((double)pArea[2].sHeight / 2.0), x, y) <= 1)
										{//X, Y 가 타원 안에 들어온다면
											m_Area[x][y] = 1; // A영역
											pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 0] = 100;
											pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 1] = 0;
											pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 2] = 0;
										}
										else
										{
											m_Area[x][y] = 2; // B영역
											pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 0] = 0;
											pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 1] = (char)255;
											pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 2] = (char)255;

										}
									}// ROI 영역이 사각형일때
									else
									{
										m_Area[x][y] = 1; // A영역
										pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 0] = 100;
										pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 1] = 0;
										pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 2] = 0;
									}
									///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
								}
							}
						}
						else
						{
							m_Area[x][y] = 3;//C영역
							pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 0] = 0;
							pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 1] = (char)255;
							pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 2] = 0;
						}
					}
					else
					{
						m_Area[x][y] = 3;//C영역
						pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 0] = 0;
						pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 1] = (char)255;
						pFieldImage->imageData[y * pFieldImage->widthStep + x * 3 + 2] = 0;
					}
				}
			}
		}
	}
	cvReleaseImage(&pFieldImage);
}

double CTI_Particle::GetDistance(int x1, int y1, int x2, int y2)
{
	double result;

	result = sqrt((double)((x2 - x1)*(x2 - x1)) + ((y2 - y1)*(y2 - y1)));

	return result;
}

double CTI_Particle::EllipseDistanceSum(int XC, int YC, double A, double B, int X1, int Y1)
{
	double DistX, DistY, x, y;

	x = (double)(X1 - XC);
	y = (double)(Y1 - YC);
	DistX = (x*x) / (double)((A*A));
	DistY = (y*y) / (double)((B*B));

	double Dist = DistX + DistY;

	return Dist;
}