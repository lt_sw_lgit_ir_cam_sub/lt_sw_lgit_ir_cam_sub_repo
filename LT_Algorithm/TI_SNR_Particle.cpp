#include "TI_SNR_Particle.h"

void CTI_SNR_Particle::Initialize(WORD* pGRAYScanBuf, int iWidth, int iHeight)
{
#ifdef DEBUG_PRINT_LOG
	printf("[Start] CTI_SNR_Particle::Initialize() : pGRAYScanBuf : %d, iWidth : %d, iHeight : %d\n",
		pGRAYScanBuf != NULL ? 1 : 0, iWidth, iHeight);
#endif
	m_pGRAYScanBuf = pGRAYScanBuf;

	m_iWidth = iWidth;
	m_iHeight = iHeight;

#ifdef DEBUG_PRINT_LOG
	printf("[End] CTI_SNR_Particle::Initialize()\n");
#endif
}

ST_CAN_SNR_Particle_Result CTI_SNR_Particle::Inspection(ST_CAN_SNR_Particle_Opt stData)
{
	// ROI INPUT
	m_stData = stData;
	m_stResult.stHeader.iTestItem = stData.stHeader.iTestItem;

#ifdef DEBUG_PRINT_LOG
	printf("[Start] CTI_SNR_Particle::Inspection()\n");
	for (int i = 0; i < RelativeIllumination_Idx_Max; i++)
	{
		printf("[Info] CTI_SNR_Particle::Inspection() : bRoiIndex[%d] : %d\n",
			i, stData.bRoiIndex[i]);
	}
#endif

	// TEST
	// 검사 영역 설정
	SetROI(m_stData.bRoiIndex);

	// Signal, Noise 계산
	for (int iRoiCnt = 0; iRoiCnt < SNR_Particle_Idx_Max; iRoiCnt++)
	{
		// 사용하는 항목만 계산
		if (m_stData.bRoiIndex[iRoiCnt] == true)
			SignalnNoiseGen(m_stROI[iRoiCnt], m_stResult.sSignal[iRoiCnt], m_stResult.fNoise[iRoiCnt]);
		// 사용하지 않는 항목은 0으로 설정
		else
		{
			m_stResult.sSignal[iRoiCnt]	= 0;
			m_stResult.fNoise[iRoiCnt]	= 0.0f;
		}

		// 결과 값이 0보다 작은 경우 0으로 초기화
		if (m_stResult.sSignal[iRoiCnt] < 0)
			m_stResult.sSignal[iRoiCnt] = 0;
		if (m_stResult.fNoise[iRoiCnt] < 0.0f)
			m_stResult.fNoise[iRoiCnt] = 0.0f;

#ifdef DEBUG_PRINT_LOG
		printf("[Run] CTI_SNR_Particle::Inspection() : Signal[%d] %d, Noise[%d] : %.2f\n",
			iRoiCnt, m_stResult.sSignal[iRoiCnt], iRoiCnt, m_stResult.fNoise[iRoiCnt]);
#endif
	}

	// Result
	m_stResult.wResultCode = RC_OK;

#ifdef DEBUG_PRINT_LOG
	printf("[End] CTI_RelativeIllumination::Inspection()\n");
#endif

	return m_stResult;
}

void CTI_SNR_Particle::SignalnNoiseGen(ST_CAN_ROI stROI, unsigned short &sSignal, float& fNoise)
{
	double dbData = 0.0;
	double dbVal = 0.0;

	for (int y = stROI.sTop; y < stROI.sTop + stROI.sHeight; y++)
	{
		for (int x = stROI.sLeft; x < stROI.sLeft + stROI.sWidth; x++)
		{
			dbData += m_pGRAYScanBuf[y * m_iWidth + x];
		}
	}

	sSignal = (short)(dbData / (double)(stROI.sWidth * stROI.sHeight));
	dbData = 0.0;

	for (int y = stROI.sTop; y < stROI.sTop + stROI.sHeight; y++)
	{
		for (int x = stROI.sLeft; x < stROI.sLeft + stROI.sWidth; x++)
		{
			dbData = abs(sSignal - m_pGRAYScanBuf[y * m_iWidth + x]);
			dbVal += dbData * dbData;
		}
	}

	fNoise = (float)(sqrt(dbVal / (double)(stROI.sWidth * stROI.sHeight)));
}

void CTI_SNR_Particle::SetROI(bool* pUseIndex)
{
	ST_CAN_ROI stAllROI[SECTION_NUM_X * SECTION_NUM_Y];
	MakeSection(stAllROI);

	int nSNR_Particle[] = { 29, 30, 31, 32, 33, 46, 47, 48, 49, 55, 56, 57, 58,
		66, 80, 86, 102, 107, 123, 128, 144, 148, 166, 169, 187, 190, 208, 211, 229,
		232, 250, 253, 271, 274, 292, 296, 312, 317, 333, 338, 354, 360, 374,
		382, 383, 384, 385, 391, 392, 393, 394, 407, 408, 409, 410, 411 };

	for (int i = 0; i < SNR_Particle_Idx_Max; i++)
	{
		if (pUseIndex[i] == true)
			m_stROI[i] = stAllROI[nSNR_Particle[i]];

		//printf("[%3d, %3d] Left : %3d, Top : %3d, Width : %3d, Height : %3d\n", i, nRelativeIllumination[i],
		//	m_stROI[i].wLeft, m_stROI[i].wTop,
		//	m_stROI[i].wWidth, m_stROI[i].wHeight);
	}
}

void CTI_SNR_Particle::MakeSection(ST_CAN_ROI* pROI)
{
	// 섹션 사이즈
	int nSectionSizeX = m_iWidth / (SECTION_NUM_X - 1);
	int nSectionSizeY = m_iHeight / (SECTION_NUM_Y - 1);
	// 위치값을 누적하여 계산
	int nPosX = 0, nPosY = 0;

	int nIndex = 0;

	for (int i = 0; i < SECTION_NUM_Y; i++)
	{
		nPosX = 0;

		// 좌표 : 가로 줄
		if (i == 0)
			nPosY = 0;
		else if (i != 1)
			nPosY += nSectionSizeY;
		else
			nPosY += nSectionSizeY / 2;

		for (int j = 0; j < SECTION_NUM_X; j++)
		{
			nIndex = i * SECTION_NUM_X + j;

			// 좌표 : 세로 줄
			if (j == 0)
				nPosX = 0;
			else if (j != 1)
				nPosX += nSectionSizeX;
			else
				nPosX += nSectionSizeX / 2;

			// 사이즈 : 세로 줄
			if (j == 0 || j == SECTION_NUM_X - 1)
				pROI[nIndex].sWidth = nSectionSizeX / 2;
			else
				pROI[nIndex].sWidth = nSectionSizeX;
			// 사이즈 : 가로 줄
			if (i == 0 || i == SECTION_NUM_Y - 1)
				pROI[nIndex].sHeight = nSectionSizeY / 2;
			else
				pROI[nIndex].sHeight = nSectionSizeY;

			pROI[nIndex].sTop = nPosY;
			pROI[nIndex].sLeft = nPosX;

		//	printf("[%2d, %2d] Left : %3d, Top : %3d, Width : %3d, Height : %3d\n", i, j,
		//		pROI[nIndex].wLeft, pROI[nIndex].wTop, pROI[nIndex].wWidth, pROI[nIndex].wHeight);
		}
	}
}