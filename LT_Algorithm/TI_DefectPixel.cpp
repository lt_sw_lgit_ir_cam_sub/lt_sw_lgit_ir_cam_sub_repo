#include "TI_DefectPixel.h"

void CTI_DefectPixel::Initialize(WORD* pGRAYScanBuf, int iWidth, int iHeight)
{
#ifdef DEBUG_PRINT_LOG
	printf("[Start] CTI_DefectPixel::Initialize() : pGRAYScanBuf : %d, iWidth : %d, iHeight : %d\n",
		pGRAYScanBuf != NULL ? 1 : 0, iWidth, iHeight);
#endif
	m_pGRAYScanBuf = pGRAYScanBuf;

	m_iWidth = iWidth;
	m_iHeight = iHeight;

#ifdef DEBUG_PRINT_LOG
	printf("[End] CTI_DefectPixel::Initialize()\n");
#endif
}

ST_CAN_DefectPixel_Result CTI_DefectPixel::Inspection(ST_CAN_DefectPixel_Opt stData)
{
	// ROI INPUT
	m_stData = stData;
	m_stResult.stHeader.iTestItem = stData.stHeader.iTestItem;

#ifdef DEBUG_PRINT_LOG
	printf("[Start] CTI_DefectPixel::Inspection()\n");
	printf("[Info] CTI_DefectPixel::Inspection() : Thre : %.2f\n", m_stData.fThreshold);
#endif

	// TEST
	Particle_DefectPixel_16bit(m_stData.fThreshold, m_stResult.byIndexCnt,
		m_stResult.sPointX, m_stResult.sPointY, m_stResult.fConcentration);

#ifdef DEBUG_PRINT_LOG
	for (int i = 0; i < m_stResult.byIndexCnt; i++)
	{
		printf("[Run] CTI_DefectPixel::Inspection() : [%d] X : %d, Y : %d, Conen : %.2f\n",
			i, m_stResult.sPointX[i], m_stResult.sPointY[i], m_stResult.fConcentration[i]);
	}
#endif
	
	// Result
	m_stResult.wResultCode = RC_OK;

#ifdef DEBUG_PRINT_LOG
	printf("[End] CTI_DefectPixel::Inspection()\n");
#endif

	return m_stResult;
}

void CTI_DefectPixel::Particle_DefectPixel_16bit(float fTreshold, BYTE& byIndexCount, WORD* sPointX, WORD* sPointY, float* fConcentration)
{
	int nStart_X	= 0;
	int nStart_Y	= 0;
	int nEnd_X		= m_iWidth;
	int nEnd_Y		= m_iHeight;
	
	int x, y;

	unsigned long int *nIntegal = new unsigned long int[(m_iWidth + 1)*(m_iHeight + 1)];

	// 테이블 생성
	for (y = 0; y < nEnd_Y; y++)
	{
		for (x = 0; x < nEnd_X; x++)
		{
			if (x == 0 && y == 0)
			{
				nIntegal[0] = 0;
				nIntegal[(y + 1) * (m_iWidth + 1) + (x + 1)] = m_pGRAYScanBuf[y * m_iWidth + x];
			}
			else
			{
				if (x == 0)
				{
					nIntegal[(y + 1) * (m_iWidth + 1) + 1] = nIntegal[y * (m_iWidth + 1) + 1] + m_pGRAYScanBuf[y * m_iWidth];
					nIntegal[(y + 1) * (m_iWidth + 1)] = 0;
				}

				if (y == 0)
				{
					nIntegal[(m_iWidth + 1) + (x + 1)] = nIntegal[(m_iWidth + 1) + x] + m_pGRAYScanBuf[x];
					nIntegal[x + 1] = 0;
				}
			}

			if (x > 0 && y > 0)
			{
				nIntegal[(y + 1) * (m_iWidth + 1) + (x + 1)] = nIntegal[(y + 1) * (m_iWidth + 1) + x] + nIntegal[y * (m_iWidth + 1) + (x + 1)]
																- nIntegal[y * (m_iWidth + 1) + x] + m_pGRAYScanBuf[y * m_iWidth + x];
			}
		}
	}

	nIntegal[1] = nIntegal[m_iWidth + 1] = 0;

	/////////////////////Integral확인용 ////////////////////////////////////
#if 0
	unsigned long int aa[480 + 1][640 + 1] = { 1, };
	unsigned long int data[480][640] = { 1, };

	for (y = 0; y < nEnd_Y + 1; y++)
	{
		for (x = 0; x < nEnd_X + 1; x++)
		{
			aa[y][x] = nIntegal[y*(nEnd_X + 1) + x];
		}
	}
	for (y = 0; y < nEnd_Y; y++)
	{
		for (x = 0; x < nEnd_X; x++)
		{
			data[y][x] = m_pGRAYScanBuf[y * m_iWidth + x];
		}
	}
#endif
	////////////////////////////////////////////////////////
	double dbSum_Outter = 0.0, dbSum_Inner = 0.0;
	double dbMean_Inner = 0.0, dbMean_Outter = 0.0;

	int nInner = SIZE_SCAN_BLOCK * SIZE_SCAN_BLOCK;
	int nOutter = nInner * 8;
	int nBlock = SIZE_SCAN_BLOCK;
	bool IsNearDetect = false;
	double dbDistance = 9999.0;
	double dbValue = 0.0;
	double dbDifference;

	for (y = nStart_Y; y < (nEnd_Y - nBlock * 3) && (byIndexCount < DefectPixel_Idx_Max); y += STEP_PIXEL)
	{
		for (x = nStart_X; (x < nEnd_X - nBlock * 3) && (byIndexCount < DefectPixel_Idx_Max); x += STEP_PIXEL)
		{
			// 중앙 합
			dbSum_Inner = (double)(nIntegal[(y + nBlock * 2)*(m_iWidth + 1) + (x + nBlock * 2)] + nIntegal[(y + nBlock)*(m_iWidth + 1) + (x + nBlock)]
				- nIntegal[(y + nBlock)*(m_iWidth + 1) + (x + nBlock * 2)] - nIntegal[(y + nBlock * 2)*(m_iWidth + 1) + (x + nBlock)]);
			// 중앙 평균
			dbMean_Inner = dbSum_Inner / (double)nInner;

			// 외곽 합
			dbSum_Outter = (double)(nIntegal[(y + nBlock * 3)*(m_iWidth + 1) + (x + nBlock * 3)] + nIntegal[(y)*(m_iWidth + 1) + (x)]
				- nIntegal[(y)*(m_iWidth + 1) + (x + nBlock * 3)] - nIntegal[(y + nBlock * 3)*(m_iWidth + 1) + (x)] - dbSum_Inner);
			// 외곽 평균
			dbMean_Outter = dbSum_Outter / (double)nOutter;

			// 차이
			dbDifference = dbMean_Outter - dbMean_Inner;

			// Hot Pixel일 때만 한다.
			// Dead Pixel은 하지 않음
			dbDifference *= -1.0;

			// 농도 계산
			dbValue = dbDifference / dbMean_Outter * 100.0;

			if (dbValue > fTreshold && byIndexCount < DefectPixel_Idx_Max)
			{
				IsNearDetect = false;

				for (int i = 0; i < byIndexCount; i++)
				{
					dbDistance = sqrt((double)(sPointX[i] - (x + nBlock))*(sPointX[i] - (x + nBlock)) +
									(double)(sPointY[i] - (y + nBlock))*(sPointY[i] - (y + nBlock)));

					if (dbDistance < (double)nBlock * 1.5)
					{
						IsNearDetect = true;
						break;
					}
				}

				if (IsNearDetect == false)
				{
					// 농도
					fConcentration[byIndexCount] = (float)dbValue;

					// 검색 영역의 평균 값
					sPointX[byIndexCount] = ((x + nBlock) + (x + nBlock * 2)) / 2;
					sPointY[byIndexCount] = ((y + nBlock) + (y + nBlock * 2)) / 2;

					// 갯수
					byIndexCount++;

					// DefectPixel_Idx_Max 이상 된다면 더이상 찾지 않고 검색을 종료한다
					if (byIndexCount >= DefectPixel_Idx_Max)
					{
						SAFE_DELETE_ARRAY(nIntegal);
						return;
					}
				}
			}

		}
	}

	SAFE_DELETE_ARRAY(nIntegal);
}