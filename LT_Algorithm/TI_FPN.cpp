#include "TI_FPN.h"

void CTI_FPN::Initialize(WORD* pGRAYScanBuf, int iWidth, int iHeight)
{
#ifdef DEBUG_PRINT_LOG
	printf("[Start] CTI_FPN::Initialize() : pGRAYScanBuf : %d, iWidth : %d, iHeight : %d\n",
		pGRAYScanBuf != NULL ? 1 : 0, iWidth, iHeight);
#endif
	m_pGRAYScanBuf = pGRAYScanBuf;

	m_iWidth = iWidth;
	m_iHeight = iHeight;

#ifdef DEBUG_PRINT_LOG
	printf("[End] CTI_FPN::Initialize()\n");
#endif
}

ST_CAN_FPN_Result CTI_FPN::Inspection(ST_CAN_FPN_Opt stData)
{
	// ROI INPUT
	m_stData = stData;
	m_stResult.stHeader.iTestItem = stData.stHeader.iTestItem;

#ifdef DEBUG_PRINT_LOG
	printf("[Start] CTI_FPN::Inspection()\n");
	printf("[Info] CTI_FPN::Inspection() : Thre X : %.2f, Thre Y : %.2f\n",
		m_stData.fThresholdX, m_stData.fThresholdY);
#endif

	// TEST	
	FPNGen(m_stData.fThresholdX, m_stData.fThresholdY, m_stResult.sFailCountX, m_stResult.sFailCountY);
#ifdef DEBUG_PRINT_LOG
	printf("[Run] CTI_FPN::Inspection() : FailCount X : %d, FailCount Y : %d\n",
		m_stResult.sFailCountX, m_stResult.sFailCountY);
#endif
	
	// Result
	m_stResult.wResultCode = RC_OK;

#ifdef DEBUG_PRINT_LOG
	printf("[End] CTI_FPN::Inspection()\n");
#endif

	return m_stResult;
}

void CTI_FPN::FPNGen(float fThresholdX, float fThresholdY, unsigned short& sFailCountX, unsigned short& sFailCountY)
{
	int nStart_X = 0;
	int nStart_Y = 0;
	int nImgWidth = m_iWidth;
	int nImgHeight = m_iHeight;

	int nWidth = m_iWidth;
	int nHeight = m_iHeight;
	
	double* pResultValue_X = new double[m_iWidth];
	double* pResultValue_Y = new double[m_iHeight];

	double	*dbSum_X = new double[nImgWidth];
	double	*dbSum_Y = new double[nImgHeight];

	double dbSumText_X[20] = {0.0, };
	double dbSumText_Y[20] = { 0.0, };

	bool* pPassLineX = new bool[m_iWidth];
	bool* pPassLineY = new bool[m_iHeight];

	// 초기화
	for (int i = 0; i < nImgWidth; i++)
	{
		dbSum_X[i] = 0.0;
		pPassLineX[i] = true;
	}
	for (int i = 0; i < nImgHeight; i++)
	{
		dbSum_Y[i] = 0.0;
		pPassLineY[i] = true;
	}

	int nCntX = 0;
	int nCntY = 0;

	int nLimitWidth = nStart_X + (nImgWidth);
	int nLimitHeight = nStart_Y + (nImgHeight);

	if (nLimitWidth > nWidth)
		nLimitWidth = nWidth;
	if (nLimitHeight > nHeight)
		nLimitHeight = nHeight;

	//////////////////// 전체 이미지중 설정된 영역(in_rectInspectionArea)에 대해 (각 Line별) FPN 값을 구함.////
	for (int X = nStart_X; X < nLimitWidth; X++)
	{
		nCntY = 0;
		for (int Y = nStart_Y; Y < nLimitHeight; Y++)
		{
			dbSum_X[nCntX] += m_pGRAYScanBuf[Y*nWidth + X]; //편균 밝기 구하기 위해, 우선 각 Line의 밝기값(세로줄) 누적합(Sum)하는 중..
			dbSum_Y[nCntY] += m_pGRAYScanBuf[Y*nWidth + X]; //편균 밝기 구하기 위해, 우선 각 Line의 밝기값(가로줄) 누적합(Sum)하는 중..
			nCntY++;
		}
		nCntX++;
	}

	//nCntX = 0;
	//for (int X = nStart_X; X < nLimitWidth; X++)
	//{
	//	nCntY = 0;
	//	for (int Y = nStart_Y; Y < nLimitHeight; Y++)
	//	{
	//		if (nCntY + nCntX < 20)
	//		{
	//			dbSumText_X[nCntX] += m_pGRAYScanBuf[Y*nWidth + X];
	//			dbSumText_Y[nCntY] += m_pGRAYScanBuf[Y*nWidth + X];

	//			printf("Sum X : %.2f, Sum Y : %.2f m_pGRAYScanBuf[%4d] : %d\n", 
	//				dbSumText_X[nCntX], dbSumText_Y[nCntY], Y*nWidth + X, m_pGRAYScanBuf[Y*nWidth + X]);
	//		}

	//		nCntY++;
	//	}
	//	nCntX++;
	//}
	
	sFailCountX = 0;
	sFailCountY = 0;
	//각 세로 Line 들에 대해 Sum으로 부터 평균 밝기 값을 각각 구함 
	for (int i = 0; i < nImgWidth; i++)
	{
		pResultValue_X[i] = dbSum_X[i] / (double)nCntY;
		if (fThresholdX < pResultValue_X[i])
		{
			sFailCountX++;
			pPassLineX[i] = false;
		}
	}

	//각 가로 Line들에 대해 Sum으로 부터 평균 밝기 값을 각각 구함 
	for (int i = 0; i < nImgHeight; i++)
	{
		pResultValue_Y[i] = dbSum_Y[i] / (double)nCntX;
		if (fThresholdY < pResultValue_Y[i])
		{
			sFailCountY++;
			pPassLineY[i] = false;
		}
	}

	SAFE_DELETE_ARRAY(pPassLineX);
	SAFE_DELETE_ARRAY(pPassLineY);

	SAFE_DELETE_ARRAY(dbSum_X);
	SAFE_DELETE_ARRAY(dbSum_Y);

	SAFE_DELETE_ARRAY(pResultValue_X);
	SAFE_DELETE_ARRAY(pResultValue_Y);
}
