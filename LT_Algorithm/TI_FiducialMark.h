﻿#pragma once
#include "TI_BaseAlgorithm.h"

class CTI_FiducialMark : CTI_BaseAlgorithm
{
private:
	// Input 데이터
	ST_CAN_FiducialMark_Opt		m_stData;

	// Output 데이터
	ST_CAN_FiducialMark_Result	m_stResult;

protected:
	double	GetDistance(int ix1, int iy1, int ix2, int iy2);

public:
	CPoint 	FiducialMark_Test (ST_CAN_ROI stROI,  BYTE byMarkColor, BYTE byBrightness);
	
	virtual void				Initialize(WORD* pGRAYScanBuf, int iWidth, int iHeight);
	ST_CAN_FiducialMark_Result	Inspection(ST_CAN_FiducialMark_Opt stData);
};

