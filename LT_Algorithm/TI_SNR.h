﻿#pragma once
#include "TI_BaseAlgorithm.h"

class CTI_SNR : CTI_BaseAlgorithm
{
private:
	// Input 데이터
	ST_CAN_SNR_Opt		m_stData;

	// Output 데이터
	ST_CAN_SNR_Result	m_stResult;

protected:

public:
	void SNR_Test(ST_CAN_ROI stROI, unsigned short &sSignal, float &fNoice);

	virtual void			Initialize(WORD* pGRAYScanBuf, int iWidth, int iHeight);
	ST_CAN_SNR_Result		Inspection(ST_CAN_SNR_Opt stData);
};

