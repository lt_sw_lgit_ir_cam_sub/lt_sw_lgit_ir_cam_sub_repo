﻿#include "TI_BaseAlgorithm.h"


void CTI_BaseAlgorithm::Initialize(WORD* pGRAYScanBuf, int iWidth, int iHeight)
{
	m_pGRAYScanBuf = pGRAYScanBuf;

	m_iWidth = iWidth;
	m_iHeight = iHeight;
}

bool CTI_BaseAlgorithm::CheckROI(ST_CAN_ROI stROI)
{
	// 음수이면 Fail
	if (stROI.sWidth < 0 || stROI.sHeight < 0 || 
		stROI.sLeft < 0 || stROI.sTop < 0)
		return false;
	// Left Top이 해상도 밖이면 Fail
	else if (stROI.sLeft > m_iWidth || stROI.sTop > m_iHeight)
		return false;
	// Right Bottom이 해상도 밖이면 Fail
	else if (stROI.sLeft + stROI.sWidth > m_iWidth || stROI.sTop + stROI.sHeight > m_iHeight)
		return false;

	return true;
}