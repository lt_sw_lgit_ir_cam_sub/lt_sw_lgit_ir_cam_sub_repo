#pragma once
#include "TI_BaseAlgorithm.h"

#define STEP_PIXEL		1	// Step Pixel수
#define SIZE_SCAN_BLOCK	4	// Block Size

class CTI_DefectPixel : CTI_BaseAlgorithm
{
private:
	// Input 데이터
	ST_CAN_DefectPixel_Opt		m_stData;

	// Output 데이터
	ST_CAN_DefectPixel_Result	m_stResult;

private:
	
public:
	virtual void			Initialize(WORD* pGRAYScanBuf, int iWidth, int iHeight);
	ST_CAN_DefectPixel_Result	Inspection(ST_CAN_DefectPixel_Opt stData);

	void Particle_DefectPixel_16bit(float fTreshold, BYTE& byIndexCount, WORD* sPointX, WORD* sPointY, float* fConcentration);
};
