﻿#include "TI_SNR.h"

void CTI_SNR::Initialize(WORD* pGRAYScanBuf, int iWidth, int iHeight)
{
#ifdef DEBUG_PRINT_LOG
	printf("[Start] CTI_SNR::Initialize() : pGRAYScanBuf : %d, iWidth : %d, iHeight : %d\n",
		pGRAYScanBuf != NULL ? 1 : 0, iWidth, iHeight);
#endif
	m_pGRAYScanBuf = pGRAYScanBuf;

	m_iWidth = iWidth;
	m_iHeight = iHeight;
	
#ifdef DEBUG_PRINT_LOG
	printf("[End] CTI_SNR::Initialize()\n");
#endif
}

ST_CAN_SNR_Result CTI_SNR::Inspection(ST_CAN_SNR_Opt stData)
{
	// ROI INPUT
	m_stData = stData;
	m_stResult.stHeader.iTestItem = stData.stHeader.iTestItem;
	m_stResult.byRoiCnt = m_stData.byRoiCnt;

#ifdef DEBUG_PRINT_LOG
	printf("[Start] CTI_SNR::Inspection()\n");
	for (int i = 0; i < m_stData.byRoiCnt; i++)
	{
		if (CheckROI(m_stData.stROI[i]) == false)
			continue;

		printf("[Info] CTI_SNR::Inspection() : Cnt : %d, ROI[%d](L_%d, T_%d, W_%d, H_%d)\n",
			m_stData.byRoiCnt, i, m_stData.stROI[i].sLeft, m_stData.stROI[i].sTop, m_stData.stROI[i].sWidth,
			m_stData.stROI[i].sHeight);
	}
#endif

	// TEST
	for (int i = 0; i < m_stData.byRoiCnt; i++)
	{
		if (CheckROI(m_stData.stROI[i]) == false)
			continue;

		SNR_Test(m_stData.stROI[i], m_stResult.sSignal[i], m_stResult.fNoise[i]);

#ifdef DEBUG_PRINT_LOG
		printf("[Run] CTI_SNR::Inspection() : m_stResult.sMarkCenterX[%d] : %d, m_stResult.sMarkCenterY[%d] : %.2f\n",
			i, m_stResult.sSignal[i], i, m_stResult.fNoise[i]);
#endif
	}

	// Result
	m_stResult.wResultCode = RC_OK;

#ifdef DEBUG_PRINT_LOG
	printf("[End] CTI_SNR::Inspection()\n");
#endif

	return m_stResult;
}

//=============================================================================
// Method		: SNR_Test
// Access		: public  
// Returns		: void
// Parameter	: ST_CAN_ROI stROI
// Parameter	: short & sSignal
// Parameter	: float & fNoice
// Qualifier	:
// Last Update	: 2017/11/6 - 11:03
// Desc.		:
//=============================================================================
void CTI_SNR::SNR_Test(ST_CAN_ROI stROI, unsigned short &sSignal, float &fNoice)
{
	double dbData = 0.0;
	double dbVal  = 0.0;

	for (int y = stROI.sTop; y < stROI.sTop + stROI.sHeight; y++)
	{
		for (int x = stROI.sLeft; x < stROI.sLeft + stROI.sWidth; x++)
		{
			dbData += m_pGRAYScanBuf[y * m_iWidth + x];
		}
	}

	sSignal = (short)(dbData / (double)(stROI.sWidth * stROI.sHeight));
	dbData  = 0.0;

	for (int y = stROI.sTop; y < stROI.sTop + stROI.sHeight; y++)
	{
		for (int x = stROI.sLeft; x < stROI.sLeft + stROI.sWidth; x++)
		{
			dbData = abs(sSignal - m_pGRAYScanBuf[y * m_iWidth + x]);
			dbVal += dbData * dbData;
		}
	}

	fNoice = (float)(sqrt(dbVal / (double)(stROI.sWidth * stROI.sHeight)));
}
