#pragma once
#include "TI_BaseAlgorithm.h"

#define SFR_PIXEL_SIZE	5.6

class CTI_SFR : CTI_BaseAlgorithm
{
protected:
	// Input 데이터
	ST_CAN_SFR_Opt	m_stData;

	// Output 데이터
	ST_CAN_SFR_Result	m_stResult;

	//void	SearchROIArea(LPBYTE IN_RGB, int tempWidth);
	//void	SearchROIArea_16bit(WORD *GRAYScanBuf, int tempWidth);

protected:
	void	Get_16bit_ROI(int iROI_Index, int iStartX, int iStartY, int iImgWidth, int iImgHeight, WORD* pTemp);
	void	SFR_Gen_16bit();
	double	GetSFRValue_16bit(WORD *pBuf, int iWidth, int iHeight, double dbLinePair);//Ver.3

	void	MulMatrix(double **A, double **B, double **C, unsigned int Row, unsigned int Col, unsigned int n, unsigned int Mode);
	void	pInverse(double **A, unsigned int Row, unsigned int Col, double **RtnMat);
	void	Inverse(double **dataMat, unsigned int n, double **MatRtn);

public:
	virtual void				Initialize(WORD* pGRAYScanBuf, int iWidth, int iHeight);
	virtual ST_CAN_SFR_Result	Inspection(ST_CAN_SFR_Opt stData);
};