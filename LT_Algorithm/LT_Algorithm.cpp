#include "LT_Algorithm.h"

void CLT_Algorithm::Initialize(int nWidth, int nHeight)
{
	m_nWidth = nWidth;
	m_nHeight = nHeight;
}

WORD CLT_FiducialMark::Execute(BYTE *pIR, BYTE *pDepth, BYTE *pMSG, int &resultLen, BYTE *pResultMSG)
{
#ifdef DEBUG_PRINT_LOG
	printf("[Start] CLT_FiducialMark::Execute() : pIR : %d, pMSG : %d, resultLen : %d, pResultMSG : %d\n",
		pIR != NULL ? 1 : 0,
		pMSG != NULL ? 1 : 0,
		resultLen,
		pResultMSG != NULL ? 1 : 0);
#endif
	WORD wResultCode = RC_Unknown_Code;

	/////////////////////////////////
	// MSG Unpack
	/////////////////////////////////
	// ROI 데이터
	ST_CAN_FiducialMark_Opt inputROI;
	memcpy(&inputROI, pMSG, sizeof(ST_CAN_FiducialMark_Opt));

#ifdef DEBUG_PRINT_LOG
	printf("[Info] CTI_FiducialMark::Inspection() : Cnt : %d, ROI(L_%d, T_%d, W_%d, H_%d), Mark : %d, Brightness : %d\n",
		inputROI.byRoiCnt, inputROI.stROI[0].sLeft, inputROI.stROI[0].sTop, inputROI.stROI[0].sWidth, inputROI.stROI[0].sHeight,
		inputROI.byMarkColor[0], inputROI.byBrightness[0]);
#endif

	/////////////////////////////////
	// 알고리즘 실행
	/////////////////////////////////
	m_FiducialMark.Initialize((WORD*)pIR, m_nWidth, m_nHeight);
	ST_CAN_FiducialMark_Result stResult = m_FiducialMark.Inspection(inputROI);

	/////////////////////////////////
	// Result Pack
	/////////////////////////////////
	resultLen = sizeof(ST_CAN_FiducialMark_Result);
	memcpy(pResultMSG, &stResult, sizeof(ST_CAN_FiducialMark_Result));
	wResultCode = stResult.wResultCode;

#ifdef DEBUG_PRINT_LOG
	printf("[End] CLT_FiducialMark::Execute() : resultLen : %d\n",
		resultLen);
#endif
	return wResultCode;
}

WORD CLT_EdgeLine::Execute(BYTE *pIR, BYTE *pDepth, BYTE *pMSG, int &resultLen, BYTE *pResultMSG)
{
#ifdef DEBUG_PRINT_LOG
	printf("[Start] CLT_EdgeLine::Execute() : pIR : %d, pMSG : %d, resultLen : %d, pResultMSG : %d\n",
		pIR != NULL ? 1 : 0,
		pMSG != NULL ? 1 : 0,
		resultLen,
		pResultMSG != NULL ? 1 : 0);
#endif
	WORD wResultCode = RC_Unknown_Code;

	/////////////////////////////////
	// MSG Unpack
	/////////////////////////////////
	// ROI 데이터
	ST_CAN_EdgeLine_Opt inputROI;
	memcpy(&inputROI, pMSG, sizeof(ST_CAN_EdgeLine_Opt));

#ifdef DEBUG_PRINT_LOG
	printf("[Start] CTI_EdgeLine::Inspection()\n");
	for (int i = 0; i < inputROI.byRoiCnt; i++)
	{
		printf("[Info] CTI_EdgeLine::Inspection() : Cnt : %d, ROI[%d](L_%d, T_%d, W_%d, H_%d), Color : %d, Thre : %d, Brightness : %d, CenterX : %d, CenterY : %d\n",
			inputROI.byRoiCnt, i, inputROI.stROI[i].sLeft, inputROI.stROI[i].sTop, inputROI.stROI[i].sWidth,
			inputROI.stROI[i].sHeight, inputROI.byDetectColor[i], inputROI.byThreshold[i], inputROI.byBrightness[i],
			inputROI.sCenterX[i], inputROI.sCenterY[i]);
	}
#endif

	/////////////////////////////////
	// 알고리즘 실행
	/////////////////////////////////
	m_EdgeLine.Initialize((WORD*)pIR, m_nWidth, m_nHeight);
	ST_CAN_EdgeLine_Result stResult = m_EdgeLine.Inspection(inputROI);

	/////////////////////////////////
	// Result Pack
	/////////////////////////////////
	resultLen = sizeof(ST_CAN_EdgeLine_Result);
	memcpy(pResultMSG, &stResult, sizeof(ST_CAN_EdgeLine_Result));
	wResultCode = stResult.wResultCode;

#ifdef DEBUG_PRINT_LOG
	printf("[End] CLT_EdgeLine::Execute() : resultLen : %d\n",
		resultLen);
#endif
	return wResultCode;
}

WORD CLT_SNR::Execute(BYTE *pIR, BYTE *pDepth, BYTE *pMSG, int &resultLen, BYTE *pResultMSG)
{
#ifdef DEBUG_PRINT_LOG
	printf("[Start] CLT_SNR::Execute() : pIR : %d, pMSG : %d, resultLen : %d, pResultMSG : %d\n",
		pIR != NULL ? 1 : 0,
		pMSG != NULL ? 1 : 0,
		resultLen,
		pResultMSG != NULL ? 1 : 0);
#endif
	WORD wResultCode = RC_Unknown_Code;

	/////////////////////////////////
	// MSG Unpack
	/////////////////////////////////
	// ROI 데이터
	ST_CAN_SNR_Opt inputROI;
	memcpy(&inputROI, pMSG, sizeof(ST_CAN_SNR_Opt));

#ifdef DEBUG_PRINT_LOG
	for (int i = 0; i < inputROI.byRoiCnt; i++)
	{
		printf("[Info] CTI_SNR::Inspection() : Cnt : %d, ROI[%d](L_%d, T_%d, W_%d, H_%d)\n",
			inputROI.byRoiCnt, i, inputROI.stROI[i].sLeft, inputROI.stROI[i].sTop, inputROI.stROI[i].sWidth,
			inputROI.stROI[i].sHeight);
	}
#endif

	/////////////////////////////////
	// 알고리즘 실행
	/////////////////////////////////
	m_SNR.Initialize((WORD*)pIR, m_nWidth, m_nHeight);
	ST_CAN_SNR_Result stResult = m_SNR.Inspection(inputROI);

	/////////////////////////////////
	// Result Pack
	/////////////////////////////////
	resultLen = sizeof(ST_CAN_SNR_Result);
	memcpy(pResultMSG, &stResult, sizeof(ST_CAN_SNR_Result));
	wResultCode = stResult.wResultCode;

#ifdef DEBUG_PRINT_LOG
	printf("[End] CLT_SNR::Execute() : resultLen : %d\n",
		resultLen);
#endif
	return wResultCode;
}

WORD CLT_SFR::Execute(BYTE *pIR, BYTE *pDepth, BYTE *pMSG, int &resultLen, BYTE *pResultMSG)
{
#ifdef DEBUG_PRINT_LOG
	printf("[Start] CLT_SFR::Execute() : pIR : %d, pMSG : %d, resultLen : %d, pResultMSG : %d\n",
		pIR != NULL ? 1 : 0,
		pMSG != NULL ? 1 : 0,
		resultLen,
		pResultMSG != NULL ? 1 : 0);
#endif
	WORD wResultCode = RC_Unknown_Code;

	/////////////////////////////////
	// MSG Unpack
	/////////////////////////////////
	// ROI 데이터
	ST_CAN_SFR_Opt inputROI;
	memcpy(&inputROI, pMSG, sizeof(ST_CAN_SFR_Opt));

#ifdef DEBUG_PRINT_LOG
	for (int i = 0; i < inputROI.byRoiCnt; i++)
	{
		printf("[Info] CTI_SFR::Inspection() : Cnt : %d, ROI[%d](L_%d, T_%d, W_%d, H_%d), LinePair : %.2f\n",
			inputROI.byRoiCnt, i, inputROI.stROI[i].sLeft, inputROI.stROI[i].sTop, inputROI.stROI[i].sWidth,
			inputROI.stROI[i].sHeight, inputROI.fLinePair[i]);
	}
#endif

	/////////////////////////////////
	// 알고리즘 실행
	/////////////////////////////////
	m_SFR.Initialize((WORD*)pIR, m_nWidth, m_nHeight);
	ST_CAN_SFR_Result stResult = m_SFR.Inspection(inputROI);

	/////////////////////////////////
	// Result Pack
	/////////////////////////////////
	resultLen = sizeof(ST_CAN_SFR_Result);
	memcpy(pResultMSG, &stResult, sizeof(ST_CAN_SFR_Result));
	wResultCode = stResult.wResultCode;

#ifdef DEBUG_PRINT_LOG
	printf("[End] CLT_SFR::Execute() : resultLen : %d\n",
		resultLen);
#endif
	return wResultCode;
}

WORD CLT_Particle::Execute(BYTE *pIR, BYTE *pDepth, BYTE *pMSG, int &resultLen, BYTE *pResultMSG)
{
#ifdef DEBUG_PRINT_LOG
	printf("[Start] CLT_Particle::Execute() : pDepth : %d, pMSG : %d, resultLen : %d, pResultMSG : %d\n",
		pDepth != NULL ? 1 : 0,
		pMSG != NULL ? 1 : 0,
		resultLen,
		pResultMSG != NULL ? 1 : 0);
#endif
	WORD wResultCode = RC_Unknown_Code;

	/////////////////////////////////
	// MSG Unpack
	/////////////////////////////////
	// ROI 데이터
	ST_CAN_Particle_Opt inputROI;
	memcpy(&inputROI, pMSG, sizeof(ST_CAN_Particle_Opt));

	/////////////////////////////////
	// 알고리즘 실행
	/////////////////////////////////
	m_Particle.Initialize((WORD*)pDepth, m_nWidth, m_nHeight);
	ST_CAN_Particle_Result stResult = m_Particle.Inspection(inputROI);

	/////////////////////////////////
	// Result Pack
	/////////////////////////////////
	resultLen = sizeof(ST_CAN_Particle_Result);
	memcpy(pResultMSG, &stResult, sizeof(ST_CAN_Particle_Result));
	wResultCode = stResult.wResultCode;

#ifdef DEBUG_PRINT_LOG
	printf("[End] CLT_Particle::Execute() : resultLen : %d\n",
		resultLen);
#endif
	return wResultCode;
}

WORD CLT_RelativeIllumination::Execute(BYTE *pIR, BYTE *pDepth, BYTE *pMSG, int &resultLen, BYTE *pResultMSG)
{
#ifdef DEBUG_PRINT_LOG
	printf("[Start] CLT_RelativeIllumination::Execute() : pDepth : %d, pMSG : %d, resultLen : %d, pResultMSG : %d\n",
		pDepth != NULL ? 1 : 0,
		pMSG != NULL ? 1 : 0,
		resultLen,
		pResultMSG != NULL ? 1 : 0);
#endif
	WORD wResultCode = RC_Unknown_Code;

	/////////////////////////////////
	// MSG Unpack
	/////////////////////////////////
	// ROI 데이터
	ST_CAN_RelativeIllumination_Opt inputROI;
	memcpy(&inputROI, pMSG, sizeof(ST_CAN_RelativeIllumination_Opt));

#ifdef DEBUG_PRINT_LOG
	for (int i = 0; i < RelativeIllumination_Idx_Max; i++)
	{
		printf("[Info] CTI_RelativeIllumination::Inspection() : bRoiIndex[%d] : %d\n",
			i, inputROI.bRoiIndex[i]);
	}
#endif

	/////////////////////////////////
	// 알고리즘 실행
	/////////////////////////////////
	m_RelativeIllumination.Initialize((WORD*)pDepth, m_nWidth, m_nHeight);
	ST_CAN_RelativeIllumination_Result stResult = m_RelativeIllumination.Inspection(inputROI);

	/////////////////////////////////
	// Result Pack
	/////////////////////////////////
	resultLen = sizeof(ST_CAN_RelativeIllumination_Result);
	memcpy(pResultMSG, &stResult, sizeof(ST_CAN_RelativeIllumination_Result));
	wResultCode = stResult.wResultCode;

#ifdef DEBUG_PRINT_LOG
	printf("[End] CLT_RelativeIllumination::Execute() : resultLen : %d\n",
		resultLen);
#endif
	return wResultCode;
}

WORD CLT_SNR_Particle::Execute(BYTE *pIR, BYTE *pDepth, BYTE *pMSG, int &resultLen, BYTE *pResultMSG)
{
#ifdef DEBUG_PRINT_LOG
	printf("[Start] CLT_SNR_Particle::Execute() : pDepth : %d, pMSG : %d, resultLen : %d, pResultMSG : %d\n",
		pDepth != NULL ? 1 : 0,
		pMSG != NULL ? 1 : 0,
		resultLen,
		pResultMSG != NULL ? 1 : 0);
#endif
	WORD wResultCode = RC_Unknown_Code;

	/////////////////////////////////
	// MSG Unpack
	/////////////////////////////////
	// ROI 데이터
	ST_CAN_SNR_Particle_Opt inputROI;
	memcpy(&inputROI, pMSG, sizeof(ST_CAN_SNR_Particle_Opt));

#ifdef DEBUG_PRINT_LOG
	for (int i = 0; i < RelativeIllumination_Idx_Max; i++)
	{
		printf("[Info] CTI_SNR_Particle::Inspection() : bRoiIndex[%d] : %d\n",
			i, inputROI.bRoiIndex[i]);
	}
#endif

	/////////////////////////////////
	// 알고리즘 실행
	/////////////////////////////////
	m_SNR_Particle.Initialize((WORD*)pDepth, m_nWidth, m_nHeight);
	ST_CAN_SNR_Particle_Result stResult = m_SNR_Particle.Inspection(inputROI);

	/////////////////////////////////
	// Result Pack
	/////////////////////////////////
	resultLen = sizeof(ST_CAN_SNR_Particle_Result);
	memcpy(pResultMSG, &stResult, sizeof(ST_CAN_SNR_Particle_Result));
	wResultCode = stResult.wResultCode;

#ifdef DEBUG_PRINT_LOG
	printf("[End] CLT_SNR_Particle::Execute() : resultLen : %d\n",
		resultLen);
#endif
	return wResultCode;
}

WORD CLT_FPN::Execute(BYTE *pIR, BYTE *pDepth, BYTE *pMSG, int &resultLen, BYTE *pResultMSG)
{
#ifdef DEBUG_PRINT_LOG
	printf("[Start] CLT_FPN::Execute() : pDepth : %d, pMSG : %d, resultLen : %d, pResultMSG : %d\n",
		pDepth != NULL ? 1 : 0,
		pMSG != NULL ? 1 : 0,
		resultLen,
		pResultMSG != NULL ? 1 : 0);
#endif
	WORD wResultCode = RC_Unknown_Code;

	/////////////////////////////////
	// MSG Unpack
	/////////////////////////////////
	// ROI 데이터
	ST_CAN_FPN_Opt inputROI;
	memcpy(&inputROI, pMSG, sizeof(ST_CAN_FPN_Opt));

#ifdef DEBUG_PRINT_LOG
	printf("[Info] CTI_FPN::Inspection() : Thre X : %.2f, Thre Y : %.2f\n",
		inputROI.fThresholdX, inputROI.fThresholdY);
#endif

	/////////////////////////////////
	// 알고리즘 실행
	/////////////////////////////////
	m_FPN.Initialize((WORD*)pDepth, m_nWidth, m_nHeight);
	ST_CAN_FPN_Result stResult = m_FPN.Inspection(inputROI);

	/////////////////////////////////
	// Result Pack
	/////////////////////////////////
	resultLen = sizeof(ST_CAN_FPN_Result);
	memcpy(pResultMSG, &stResult, sizeof(ST_CAN_FPN_Result));
	wResultCode = stResult.wResultCode;

#ifdef DEBUG_PRINT_LOG
	printf("[End] CLT_FPN::Execute() : resultLen : %d\n",
		resultLen);
#endif
	return wResultCode;
}

WORD CLT_DefectPixel::Execute(BYTE *pIR, BYTE *pDepth, BYTE *pMSG, int &resultLen, BYTE *pResultMSG)
{
#ifdef DEBUG_PRINT_LOG
	printf("[Start] CLT_DefectPixel::Execute() : pDepth : %d, pMSG : %d, resultLen : %d, pResultMSG : %d\n",
		pDepth != NULL ? 1 : 0,
		pMSG != NULL ? 1 : 0,
		resultLen,
		pResultMSG != NULL ? 1 : 0);
#endif
	WORD wResultCode = RC_Unknown_Code;

	/////////////////////////////////
	// MSG Unpack
	/////////////////////////////////
	// ROI 데이터
	ST_CAN_DefectPixel_Opt inputROI;
	memcpy(&inputROI, pMSG, sizeof(ST_CAN_DefectPixel_Opt));

#ifdef DEBUG_PRINT_LOG
	printf("[Info] CTI_DefectPixel::Inspection() : Thre : %.2f\n", inputROI.fThreshold);
#endif

	/////////////////////////////////
	// 알고리즘 실행
	/////////////////////////////////
	m_DefectPixel.Initialize((WORD*)pDepth, m_nWidth, m_nHeight);
	ST_CAN_DefectPixel_Result stResult = m_DefectPixel.Inspection(inputROI);

	/////////////////////////////////
	// Result Pack
	/////////////////////////////////
	resultLen = sizeof(ST_CAN_DefectPixel_Result);
	memcpy(pResultMSG, &stResult, sizeof(ST_CAN_DefectPixel_Result));
	wResultCode = stResult.wResultCode;

#ifdef DEBUG_PRINT_LOG
	printf("[End] CLT_DefectPixel::Execute() : resultLen : %d\n",
		resultLen);
#endif
	return wResultCode;
}
