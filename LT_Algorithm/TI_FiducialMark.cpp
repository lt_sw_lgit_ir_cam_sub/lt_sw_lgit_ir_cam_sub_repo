﻿#include "TI_FiducialMark.h"

void CTI_FiducialMark::Initialize(WORD* pGRAYScanBuf, int iWidth, int iHeight)
{
#ifdef DEBUG_PRINT_LOG
	printf("[Start] CTI_FiducialMark::Initialize() : pGRAYScanBuf : %d, iWidth : %d, iHeight : %d\n",
		pGRAYScanBuf != NULL ? 1 : 0, iWidth, iHeight);
#endif
	m_pGRAYScanBuf = pGRAYScanBuf;

	m_iWidth = iWidth;
	m_iHeight = iHeight;

#ifdef DEBUG_PRINT_LOG
	printf("[End] CTI_FiducialMark::Initialize()\n");
#endif
}

ST_CAN_FiducialMark_Result CTI_FiducialMark::Inspection(ST_CAN_FiducialMark_Opt stData)
{
	// ROI INPUT
	m_stData = stData;
	m_stResult.stHeader.iTestItem = stData.stHeader.iTestItem;
	m_stResult.byRoiCnt = m_stData.byRoiCnt;
	m_stResult.wResultCode = RC_OK;

#ifdef DEBUG_PRINT_LOG
	printf("[Start] CTI_FiducialMark::Inspection()\n");
	for (int i = 0; i < m_stData.byRoiCnt; i++)
	{
		if (CheckROI(m_stData.stROI[i]) == false)
			continue;

		printf("[Info] CTI_FiducialMark::Inspection() : Cnt : %d, ROI[%d](L_%d, T_%d, W_%d, H_%d), Mark : %d, Brightness : %d\n",
			m_stData.byRoiCnt, i, m_stData.stROI[i].sLeft, m_stData.stROI[i].sTop, m_stData.stROI[i].sWidth, 
			m_stData.stROI[i].sHeight, m_stData.byMarkColor[i], m_stData.byBrightness[i]);
	}
#endif

	// TEST
	CPoint ptResult;
	for (int i = 0; i < m_stData.byRoiCnt; i++)
	{
		if (CheckROI(m_stData.stROI[i]) == false)
			continue;

		ptResult = FiducialMark_Test(m_stData.stROI[i], m_stData.byMarkColor[i], m_stData.byBrightness[i]);

		m_stResult.sMarkCenterX[i] = (unsigned short)ptResult.x;
		m_stResult.sMarkCenterY[i] = (unsigned short)ptResult.y;

#ifdef DEBUG_PRINT_LOG
		printf("[Run] CTI_FiducialMark::Inspection() : m_stResult.sMarkCenterX[%d] %d, m_stResult.sMarkCenterY[%d] : %d\n",
			i, m_stResult.sMarkCenterX[i], i, m_stResult.sMarkCenterY[i]);
#endif
	}

	// Result
#ifdef DEBUG_PRINT_LOG
	printf("[End] CTI_FiducialMark::Inspection()\n");
#endif

	return m_stResult;
}


//=============================================================================
// Method		: FiducleMark_Test
// Access		: public  
// Returns		: CPoint
// Parameter	: ST_CAN_ROI stROI
// Parameter	: BYTE byMarkColor
// Parameter	: BYTE byBrightness
// Qualifier	:
// Last Update	: 2017/11/2 - 18:14
// Desc.		:
//=============================================================================

enum { NormalArea, CornerDark_LT, CornerDark_RT, CornerDark_LB, CornerDark_RB };
CPoint CTI_FiducialMark::FiducialMark_Test(ST_CAN_ROI stROI, BYTE byMarkColor, BYTE byBrightness)
{
#ifdef DEBUG_PRINT_LOG
	printf("[Start] CTI_FiducialMark::FiducialMark_Test() : \n    left : %d, Top : %d, Width : %d, Height : %d, MarkColor : %d, Brightness : %d\n",
		stROI.sLeft, stROI.sTop, stROI.sWidth, stROI.sHeight, byMarkColor, byBrightness);
#endif
	
	bool bIsCornerDarkArea=false;
	int nNumOfCornerDark = NormalArea;
	int Offset_X = 1, Offset_Y = 1;

	if (stROI.sLeft + stROI.sWidth < 80)
	{
		if (stROI.sTop + stROI.sHeight < 80) 
		{
			bIsCornerDarkArea = true;
			nNumOfCornerDark = CornerDark_LT;
			Offset_X *= -1;  Offset_Y *= -1;
		}
		if (stROI.sTop > m_iHeight - 80) 
		{
			bIsCornerDarkArea = true;
			nNumOfCornerDark = CornerDark_LB;
			Offset_X *= -1;  Offset_Y *= 1;
		}
	}

	if (stROI.sLeft > m_iWidth - 80)
	{
		if (stROI.sTop + stROI.sHeight < 80) 
		{
			bIsCornerDarkArea = true;
			nNumOfCornerDark = CornerDark_RT;
			Offset_X *= 1;  Offset_Y *= -1;
		}
		if (stROI.sTop > m_iHeight - 80) 
		{
			bIsCornerDarkArea = true;
			nNumOfCornerDark = CornerDark_RB;
			Offset_X *= 1;  Offset_Y *= 1;
		}
	}


	CPoint ptCenter;
	CvRect *rectArray = new CvRect[2];
	CvMemStorage* contour_storage = cvCreateMemStorage(0);;

	// 초기값
	ptCenter.x = 0;
	ptCenter.y = 0;

	IplImage *OriginImage = cvCreateImage(cvSize(stROI.sWidth, stROI.sHeight), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(stROI.sWidth, stROI.sHeight), IPL_DEPTH_8U, 1);

	WORD wData16Bit;
	BYTE byData8Bit;

	cvSetZero(OriginImage);

	for (int y = stROI.sTop; y < stROI.sTop + stROI.sHeight; y++)
	{
		for (int x = stROI.sLeft; x < stROI.sLeft + stROI.sWidth; x++)
		{
			wData16Bit = *(m_pGRAYScanBuf + y * m_iWidth + x);

			wData16Bit = wData16Bit & 0xF000 ? 0x0FFF : wData16Bit;
			byData8Bit = (wData16Bit >> 4) & 0x00FF;
		
			if (byBrightness >= 0)
				byData8Bit = (byData8Bit << byBrightness) >= 0xFF ? 0xFF : (byData8Bit << byBrightness);
			else
				byData8Bit = (byData8Bit >> (-1 * byBrightness)) <= 0x00 ? 0x00 : (byData8Bit >> (-1 * byBrightness));

			OriginImage->imageData[(y - stROI.sTop) * OriginImage->widthStep + (x - stROI.sLeft)] = byData8Bit;
		}
	}

	cvAdaptiveThreshold(OriginImage, DilateImage, 255, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 15, 3.0);

	if (bIsCornerDarkArea)
	{ // 네 모서리 코너다크 부분(어두운 부분)의 Mark 찾기 인 경우.....
		cvDilate(DilateImage, DilateImage);
		cvDilate(DilateImage, DilateImage);
		cvNot(DilateImage, DilateImage);

		int iCounter = 0;
		CvRect Rect;
		CvSeq *contour = 0;

		cvFindContours(DilateImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

		int MaxRectSize = 0;
		
		double Rate;
		for (; contour != 0; contour = contour->h_next)
		{
			Rect = cvContourBoundingRect(contour, 1);
			Rate = (double)Rect.width / (double)Rect.height;

			// ROI 사이즈 비율 변경 1.5 -> 2.0
			if ((Rect.width > 4) && (Rect.width <= (stROI.sWidth / 2)) && Rect.height > 4 && (Rect.height <= (stROI.sHeight / 2)) && Rate > 0.5 && Rate < 2.0)
			{
				int RectSize = Rect.width + Rect.height;
				int CenterX  = Rect.x + Rect.width / 2;
				int CenterY = Rect.y + Rect.height / 2;

				if (iCounter == 0)
				{
					MaxRectSize = RectSize;
					ptCenter.x = CenterX;
					ptCenter.y = CenterY;
					iCounter++;
				}
				else if ((RectSize >= MaxRectSize-4) && (Offset_X*(ptCenter.x - CenterX))>0 && (Offset_Y*(ptCenter.y - CenterY))>0)
				{
					MaxRectSize = RectSize;
					ptCenter.x = CenterX;
					ptCenter.y = CenterY;
					iCounter++;
				}				
			}
		}
		if (iCounter == 0)
		{
			ptCenter.x = 0;
			ptCenter.y = 0;
			m_stResult.wResultCode = RC_FindMark_Fail;

			cvReleaseMemStorage(&contour_storage);
			cvReleaseImage(&DilateImage);
			cvReleaseImage(&OriginImage);
			SAFE_DELETE_ARRAY(rectArray);

			return ptCenter;
		}

		ptCenter.x += Offset_X*6;
		ptCenter.y += Offset_Y*6;

	}
	else 
	{  // 원래 알고리즘.... 
		if (byMarkColor == Mark_C_BLOCK)  // 바뀐것은 아닌지??? 확인필요.. 주어진 이미지 상으로는 반대로 동작하는 것으로 보임... 2017.12.10 연구소장 의견..
		{
			cvNot(DilateImage, DilateImage);
		}

		cvErode(DilateImage, DilateImage);
		
		int iCounter = 0;
		CvRect Rect;
		CvSeq *contour = 0;

		cvFindContours(DilateImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

		double Rate;
		double old_dist = 99999.0;
		double old_dist2 = 99999.0;
		for (; contour != 0; contour = contour->h_next)
		{
			Rect = cvContourBoundingRect(contour, 1);
			Rate = (double)Rect.width / (double)Rect.height;

			// ROI 사이즈 비율 변경 1.5 -> 2.0
			if ((Rect.width > 4) && (Rect.width <= (stROI.sWidth / 2)) && Rect.height > 4 && (Rect.height <= (stROI.sHeight / 2)) && Rate > 0.5 && Rate < 2.0)
			{
				double distance = GetDistance(Rect.x + Rect.width / 2, Rect.y + Rect.height / 2, stROI.sWidth / 2, stROI.sHeight / 2);

				if (distance < old_dist || distance < old_dist2)
				{
					if (distance >= old_dist && distance < old_dist2)
					{ // 두 거리 사이의 거리가 들어오면 
						rectArray[1] = Rect;		//두번째 거리에 있는 Rect 에 입력
						old_dist2 = distance;
					}
					else // 최소 거리가 들어오면 
					{
						rectArray[1] = rectArray[0];// 가지고 있던 Rect 저장 ( 두번째로 가까운 Rect )
						rectArray[0] = Rect;		// 새로운 Rect 저장		 ( 제일 가까운 Rect )
						old_dist2 = old_dist;
						old_dist = distance;
					}

					iCounter++;
				}
			}
		}

		if (iCounter < 2)
		{
			ptCenter.x = 0;
			ptCenter.y = 0;
			m_stResult.wResultCode = RC_FindMark_Fail;

			cvReleaseMemStorage(&contour_storage);
			cvReleaseImage(&DilateImage);
			cvReleaseImage(&OriginImage);
			SAFE_DELETE_ARRAY(rectArray);

#ifdef DEBUG_PRINT_LOG
			printf("[End] CTI_FiducialMark::FiducialMark_Test() : ptCenter.x : %d, ptCenter.y : %d\n",
				ptCenter.x, ptCenter.y);
#endif

			return ptCenter;
		}

		ptCenter.x = (int)((double)(rectArray[0].x + (rectArray[0].x + rectArray[0].width) + rectArray[1].x + (rectArray[1].x + rectArray[1].width)) / 4.0);
		ptCenter.y = (int)((double)(rectArray[0].y + (rectArray[0].y + rectArray[0].height) + rectArray[1].y + (rectArray[1].y + rectArray[1].height)) / 4.0);
	}

	ptCenter.x += stROI.sLeft;
	ptCenter.y += stROI.sTop;

	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&DilateImage);
	cvReleaseImage(&OriginImage);
	SAFE_DELETE_ARRAY(rectArray);

#ifdef DEBUG_PRINT_LOG
	printf("[End] CTI_FiducialMark::FiducialMark_Test() : ptCenter.x : %d, ptCenter.y : %d\n",
		ptCenter.x, ptCenter.y);
#endif

	return ptCenter;
}

//=============================================================================
// Method		: GetDistance
// Access		: public  
// Returns		: double
// Parameter	: int x1
// Parameter	: int y1
// Parameter	: int x2
// Parameter	: int y2
// Qualifier	:
// Last Update	: 2017/11/2 - 18:58
// Desc.		:
//=============================================================================
double CTI_FiducialMark::GetDistance(int x1, int y1, int x2, int y2)
{
	double result;

	result = sqrt((double)((x2 - x1)*(x2 - x1)) + ((y2 - y1)*(y2 - y1)));

	return result;
}