#pragma once
#include "TI_BaseAlgorithm.h"

class CTI_Particle : CTI_BaseAlgorithm
{
private:
	int			m_nCounter;
	CvRect*		m_pContour;

	char**		m_Area;
	
	// Input 데이터
	ST_CAN_Particle_Opt		m_stData;

	// Output 데이터
	ST_CAN_Particle_Result	m_stResult;

private:
	void SetTestArea(ST_CAN_ROI* pArea);

	double GetDistance(int x1, int y1, int x2, int y2);
	double EllipseDistanceSum(int XC, int YC, double A, double B, int X1, int Y1);

	void Lump_Detection();
	void ParticleGen_16bit();
	void ParticleCluster(ST_CAN_ROI* pROI, float* pConcentration, int nParticleCount, bool* pPassIndex);

public:
	virtual void			Initialize(WORD* pGRAYScanBuf, int iWidth, int iHeight);
	ST_CAN_Particle_Result	Inspection(ST_CAN_Particle_Opt stData);
};
