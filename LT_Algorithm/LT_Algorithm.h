#ifndef LT_ALGORITHM_h__
#define LT_ALGORITHM_h__

#pragma once
#include "Def_Type.h"

#include "TI_FiducialMark.h"
#include "TI_EdgeLine.h"
#include "TI_SNR.h"
#include "TI_SFR.h"

#include "TI_Particle.h"
#include "TI_RelativeIllumination.h"
#include "TI_SNR_Particle.h"
#include "TI_FPN.h"
#include "TI_DefectPixel.h"
// 알고리즘 헤더파일 1
// 알고리즘 헤더파일 2
// 알고리즘 헤더파일 3
// ...

class CLT_Algorithm
{
protected:
	int m_nWidth;
	int m_nHeight;

public:
	virtual void Initialize(int nWidth, int nHeight);
	virtual WORD Execute(BYTE *pIR, BYTE *pDepth, BYTE *pMSG, int &resultLen, BYTE *pResultMSG) = 0;
};

class CLT_FiducialMark : public CLT_Algorithm
{
private:
	CTI_FiducialMark m_FiducialMark;

public:
	virtual WORD Execute(BYTE *pIR, BYTE *pDepth, BYTE *pMSG, int &resultLen, BYTE *pResultMSG);
};

class CLT_EdgeLine : public CLT_Algorithm
{
private:
	CTI_EdgeLine m_EdgeLine;

public:
	virtual WORD Execute(BYTE *pIR, BYTE *pDepth, BYTE *pMSG, int &resultLen, BYTE *pResultMSG);
};

class CLT_SNR : public CLT_Algorithm
{
private:
	CTI_SNR m_SNR;

public:
	virtual WORD Execute(BYTE *pIR, BYTE *pDepth, BYTE *pMSG, int &resultLen, BYTE *pResultMSG);
};

class CLT_SFR : public CLT_Algorithm
{
private:
	CTI_SFR m_SFR;

public:
	virtual WORD Execute(BYTE *pIR, BYTE *pDepth, BYTE *pMSG, int &resultLen, BYTE *pResultMSG);
};

class CLT_Particle : public CLT_Algorithm
{
private:
	CTI_Particle m_Particle;

public:
	virtual WORD Execute(BYTE *pIR, BYTE *pDepth, BYTE *pMSG, int &resultLen, BYTE *pResultMSG);
};

class CLT_RelativeIllumination : public CLT_Algorithm
{
private:
	CTI_RelativeIllumination m_RelativeIllumination;

public:
	virtual WORD Execute(BYTE *pIR, BYTE *pDepth, BYTE *pMSG, int &resultLen, BYTE *pResultMSG);
};

class CLT_SNR_Particle : public CLT_Algorithm
{
private:
	CTI_SNR_Particle m_SNR_Particle;

public:
	virtual WORD Execute(BYTE *pIR, BYTE *pDepth, BYTE *pMSG, int &resultLen, BYTE *pResultMSG);
};

class CLT_FPN : public CLT_Algorithm
{
private:
	CTI_FPN m_FPN;

public:
	virtual WORD Execute(BYTE *pIR, BYTE *pDepth, BYTE *pMSG, int &resultLen, BYTE *pResultMSG);
};

class CLT_DefectPixel : public CLT_Algorithm
{
private:
	CTI_DefectPixel m_DefectPixel;

public:
	virtual WORD Execute(BYTE *pIR, BYTE *pDepth, BYTE *pMSG, int &resultLen, BYTE *pResultMSG);
};

#endif