﻿#pragma once
#include "TI_BaseAlgorithm.h"

#include <vector>

class CTI_EdgeLine : CTI_BaseAlgorithm
{
private:
	// Input 데이터
	ST_CAN_EdgeLine_Opt		m_stData;

	// Output 데이터
	ST_CAN_EdgeLine_Result	m_stResult;

protected:
	double	GetDistance(int ix1, int iy1, int ix2, int iy2);

public:
	virtual void			Initialize(WORD* pGRAYScanBuf, int iWidth, int iHeight);
	ST_CAN_EdgeLine_Result	Inspection(ST_CAN_EdgeLine_Opt stData);

	CPoint 	EdgeLine_Test(ST_CAN_ROI stROI, int iCenterX, int iCenterY, BYTE byDetectColor, BYTE byBrightness, BYTE byThreshold);
};

