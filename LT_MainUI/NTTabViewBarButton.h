﻿//*****************************************************************************
// Filename	: 	NTTabViewBarButton.h
//
// Created	:	
// Modified	:	2010/04/02 - 13:18
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef __NTTabViewBarButton_H__
#define __NTTabViewBarButton_H__

#pragma once

//=============================================================================
// CNTTabViewBarButton
//=============================================================================
class CNTTabViewBarButton : public CMFCToolBarButton
{
	DECLARE_SERIAL(CNTTabViewBarButton)

public:
	CNTTabViewBarButton();
	CNTTabViewBarButton (LPCTSTR lpszLabel, UINT nID);

	virtual ~CNTTabViewBarButton();

	virtual void	OnDraw (	CDC* pDC, 
								const CRect& rect, 
								CMFCToolBarImages* pImages,
								BOOL bHorz = TRUE, 
								BOOL bCustomizeMode = FALSE,
								BOOL bHighlight = FALSE,
								BOOL bDrawBorder = TRUE,
								BOOL bGrayDisabledButtons = TRUE
							);

protected:	

	void			Initialize ();
	virtual BOOL	IsEditable () const
	{
		return FALSE;
	}

	virtual SIZE	OnCalculateSize (CDC* pDC, const CSize& sizeDefault, BOOL bHorz);
};

#endif // __NTTabViewBarButton_H__


