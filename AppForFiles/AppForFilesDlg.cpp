//*****************************************************************************
// Filename	: AppForFilesDlg.cpp
// Created	: 2010/12/28
// Modified	: 2017/03/08
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
// AppForFilesDlg.cpp : 구현 파일
//
#pragma comment( lib, "Psapi.lib" )

#include "stdafx.h"
#include "AppForFiles.h"
#include "AppForFilesDlg.h"
#include <Tlhelp32.h>
#include "Psapi.h"

#include "Common_ProcessFunc.h"

#pragma warning (disable:4996)
#pragma comment( lib, "Version" )

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//=============================================================================
// CAppForFilesDlg 대화 상자
//=============================================================================

#define UM_TRAYNOTIFY			(WM_USER + 1)

#define WM_ALIVE_PROCESS		(WM_USER + 108)
#define ID_EXT_PROGRAM_INFO			108

#define IDC_ED_PATH					1000
#define IDC_BN_PATH					1001
#define IDC_ED_FILTER				1002
#define IDC_CHK_INCLUDESUBFOLDER	1003
#define IDC_ED_DAYS					1004
#define IDC_BN_ADD					1005
#define IDC_BN_DELETE				1006
#define IDC_BN_CLEARLIST			1007
#define IDC_LC_PATHLIST				1008
#define IDC_BN_DOFORFILES			1009

#define		TABSTYLE_COUNT			8
static UINT g_TabOrder[TABSTYLE_COUNT] =
{
	1000, 1001, 1002, 1003, 1004, 1005, 1006, 1007
};


typedef enum enWorklistHeader
{
	WLH_No,
	WLH_Path,
	WLH_Filter,
	WLH_Subfolder,
	WLH_Days,
	WLH_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader[] =
{
	_T("No"),			// WLH_No,
	_T("Path"),			// WLH_Path,
	_T("Filter"),		// WLH_Filter,
	_T("Subfolder"),	// WLH_Subfolder,
	_T("Days"),			// WLH_Days,
	NULL,
};

const int	iListAglin[] =
{
	LVCFMT_LEFT,	 // WLH_No,
	LVCFMT_CENTER,	 // WLH_Path,
	LVCFMT_CENTER, 	 // WLH_Filter,
	LVCFMT_CENTER,	 // WLH_Subfolder,
	LVCFMT_CENTER,	 // WLH_Days,
};

const int	iHeaderWidth[] =
{
	45,		// WLH_No,
	198,	// WLH_Path,
	80,		// WLH_Filter,
	80,		// WLH_Subfolder,
	80,		// WLH_Days,
};

//=============================================================================
//
//=============================================================================
CAppForFilesDlg::CAppForFilesDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CAppForFilesDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	// Initialize NOTIFYICONDATA
	memset(&m_nid, 0 , sizeof(m_nid));
	m_nid.cbSize		= sizeof(m_nid);
	m_nid.uFlags		= NIF_ICON | NIF_TIP | NIF_MESSAGE;

	m_dwCycleWatch		= 10000;

	// CForFiles 세팅
	CString szPath;
	TCHAR szExePath[MAX_PATH] = { 0 };
	GetModuleFileName(NULL, szExePath, MAX_PATH);

	TCHAR drive[_MAX_DRIVE];
	TCHAR dir[_MAX_DIR];
	TCHAR file[_MAX_FNAME];
	TCHAR ext[_MAX_EXT];
	_tsplitpath_s(szExePath, drive, _MAX_DRIVE, dir, _MAX_DIR, file, _MAX_FNAME, ext, _MAX_EXT);
	szPath.Format(_T("%s%s"), drive, dir);

	m_ForFiles.SetUseSettingFiles(TRUE, szPath);
	m_ForFiles.LoadPreviousSettings();
	//m_ForFiles.SetSchedule(0, 1, 0);
	m_ForFiles.StartTimer();
}

CAppForFilesDlg::~CAppForFilesDlg ()
{
	m_ForFiles.StopTimer();


	m_nid.hIcon = NULL;
	Shell_NotifyIcon (NIM_DELETE, &m_nid);	
}

//=============================================================================
// Method		: CAppForFilesDlg::DoDataExchange
// Access		: virtual protected 
// Returns		: void
// Parameter	: CDataExchange * pDX
// Qualifier	:
// Last Update	: 2010/12/30 - 10:17
// Desc.		:
//=============================================================================
void CAppForFilesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAppForFilesDlg, CDialogEx)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_WM_PAINT()
	ON_WM_CTLCOLOR()
	ON_WM_QUERYDRAGICON()
	ON_COMMAND(ID_APP_EXIT,			OnAppExit)
	ON_COMMAND(ID_APP_OPEN,			OnAppOpen)
	ON_MESSAGE(UM_TRAYNOTIFY,		OnTrayNotify)
	ON_MESSAGE(WM_COPYDATA,			OnCopyData)	
	ON_BN_CLICKED(IDOK,				OnBnClickedOk)
	ON_BN_CLICKED(IDC_BN_PATH,		OnBnClickedBnPath	)
	ON_BN_CLICKED(IDC_BN_ADD,		OnBnClickedBnAdd	)
	ON_BN_CLICKED(IDC_BN_DELETE,	OnBnClickedBnDelete	)
	ON_BN_CLICKED(IDC_BN_CLEARLIST,	OnBnClickedBnClear	)
	ON_BN_CLICKED(IDC_BN_DOFORFILES,OnBnClickedBnDoForFiles	)
END_MESSAGE_MAP()

//=============================================================================
// CAppForFilesDlg 메시지 처리기
//=============================================================================

int CAppForFilesDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_st_Title.Create(_T("App ForFiles"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_Path.Create(_T("Path"),	dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_ed_Path.Create(WS_VISIBLE | WS_BORDER | ES_CENTER| WS_TABSTOP , CRect(0, 0, 0, 0), this, IDC_ED_PATH);
	m_bn_Path.Create(_T("..."),		dwStyle | WS_TABSTOP, rectDummy, this, IDC_BN_PATH);

	m_st_Filter.Create(_T("Extension"),	dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_ed_Filter.Create(WS_VISIBLE | WS_BORDER | ES_CENTER| WS_TABSTOP , CRect(0, 0, 0, 0), this, IDC_ED_FILTER);

	m_st_IncSubFolder.Create(_T("Subfolder"),	dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_chk_IncSubFolder.Create(_T("Include Subfolders"),	dwStyle | WS_TABSTOP | BS_AUTOCHECKBOX /*| WS_BORDER*/, rectDummy, this, IDC_CHK_INCLUDESUBFOLDER);
	m_chk_IncSubFolder.SetCheck(BST_CHECKED);

	m_st_Days.Create(_T("Days"),	dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_ed_Days.Create(WS_VISIBLE | WS_BORDER | ES_CENTER| WS_TABSTOP | ES_NUMBER , CRect(0, 0, 0, 0), this, IDC_ED_DAYS);
	m_ed_Days.SetWindowText(_T("30"));

	m_bn_Add.Create(_T("Add"),		dwStyle | WS_TABSTOP, rectDummy, this, IDC_BN_ADD);
	m_bn_Delete.Create(_T("Delete"),		dwStyle | WS_TABSTOP, rectDummy, this, IDC_BN_DELETE);
	m_bn_ClearList.Create(_T("Clear List"),		dwStyle | WS_TABSTOP, rectDummy, this, IDC_BN_CLEARLIST);
	m_lc_PathList.Create(WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP | LVS_REPORT | LVS_SHOWSELALWAYS | LVS_SINGLESEL, rectDummy, this, IDC_LC_PATHLIST);
	m_lc_PathList.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	m_bn_DoForFiles.Create(_T("Delete Old Files"), dwStyle, rectDummy, this, IDC_BN_DOFORFILES);

	m_st_Title.ModifyStyleEx(WS_EX_CLIENTEDGE | WS_EX_CLIENTEDGE, NULL, SWP_FRAMECHANGED);
	m_st_Title.ModifyStyle(NULL, WS_BORDER, SWP_FRAMECHANGED);
	m_st_Path.ModifyStyleEx(WS_EX_CLIENTEDGE | WS_EX_CLIENTEDGE, NULL, SWP_FRAMECHANGED);
	m_st_Path.ModifyStyle(NULL, WS_BORDER, SWP_FRAMECHANGED);
	m_st_Filter.ModifyStyleEx(WS_EX_CLIENTEDGE | WS_EX_CLIENTEDGE, NULL, SWP_FRAMECHANGED);
	m_st_Filter.ModifyStyle(NULL, WS_BORDER, SWP_FRAMECHANGED);
	m_st_IncSubFolder.ModifyStyleEx(WS_EX_CLIENTEDGE | WS_EX_CLIENTEDGE, NULL, SWP_FRAMECHANGED);
	m_st_IncSubFolder.ModifyStyle(NULL, WS_BORDER, SWP_FRAMECHANGED);
	m_st_Days.ModifyStyleEx(WS_EX_CLIENTEDGE | WS_EX_CLIENTEDGE, NULL, SWP_FRAMECHANGED);
	m_st_Days.ModifyStyle(NULL, WS_BORDER, SWP_FRAMECHANGED);

	InitListCtrl();

	EraseTrayIcon();

	RefreshList();

	return 0;
}

//=============================================================================
// Method		: CAppForFilesDlg::OnDestroy
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2011/1/10 - 17:53
// Desc.		:
//=============================================================================
void CAppForFilesDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	StopWatchTimer();
}

//=============================================================================
// Method		: CAppForFilesDlg::OnClose
// Access		: public 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/12/29 - 9:34
// Desc.		:
//=============================================================================
void CAppForFilesDlg::OnClose()
{
// 	DestroyWindow();
// 	CDialogEx::OnClose();

	OnOK();
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/3/8 - 10:26
// Desc.		:
//=============================================================================
void CAppForFilesDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	int iMargin		= 10;
	int iSpacing	= 5;
	int iCateSpacing= 10;
	int iLeft		= iMargin;
	int iTop		= iMargin;
	int iWidth		= cx - iMargin - iMargin;
	int iHeight		= cy - iMargin - iMargin;
	int iCtrlHeight = 40;
	int iCtrlWidth	= 100;
	int iTempWidth	= iWidth - iCtrlWidth - 50 - iSpacing - iSpacing;
	int iTempHeight	= 0;
	int iSubLeft	= iLeft + iCtrlWidth + iSpacing;

	m_st_Title.MoveWindow(iLeft, iTop, iWidth, iCtrlHeight);

	iTop += iCtrlHeight + iCateSpacing;
	m_st_Path.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
	m_ed_Path.MoveWindow(iSubLeft, iTop, iTempWidth, iCtrlHeight);
	iSubLeft += iTempWidth + iSpacing;	
	m_bn_Path.MoveWindow(iSubLeft, iTop, 50, iCtrlHeight);

	iSubLeft	= iLeft + iCtrlWidth + iSpacing;
	iTop += iCtrlHeight + iSpacing;
	m_st_Filter.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
	m_ed_Filter.MoveWindow(iSubLeft, iTop, iTempWidth, iCtrlHeight);

	iTop += iCtrlHeight + iSpacing;
	m_st_IncSubFolder.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
	m_chk_IncSubFolder.MoveWindow(iSubLeft, iTop, iTempWidth, iCtrlHeight);

	iTop += iCtrlHeight + iSpacing;
	m_st_Days.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
	m_ed_Days.MoveWindow(iSubLeft, iTop, iTempWidth, iCtrlHeight);

	iTop += iCtrlHeight + iCateSpacing;
	iCtrlWidth = (iWidth - iCateSpacing - iCateSpacing) / 3;
	m_bn_Add.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
	iLeft += iCtrlWidth + iCateSpacing;
	m_bn_Delete.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
	iLeft += iCtrlWidth + iCateSpacing;
	m_bn_ClearList.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);

	iLeft		= iMargin;
	iTop += iCtrlHeight + iSpacing;
	iTempHeight	= cy - iMargin - iTop - iSpacing - iCtrlHeight;
	m_lc_PathList.MoveWindow(iLeft, iTop, iWidth, iTempHeight);

	iTop += iTempHeight + iSpacing;
	m_bn_DoForFiles.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
}

//=============================================================================
// Method		: OnGetMinMaxInfo
// Access		: protected  
// Returns		: void
// Parameter	: MINMAXINFO * lpMMI
// Qualifier	:
// Last Update	: 2017/3/8 - 10:26
// Desc.		:
//=============================================================================
void CAppForFilesDlg::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMaxTrackSize.x = 520;
	lpMMI->ptMaxTrackSize.y = 720;

	lpMMI->ptMinTrackSize.x = 520;
	lpMMI->ptMinTrackSize.y = 720;

	CDialogEx::OnGetMinMaxInfo(lpMMI);
}

//=============================================================================
// Method		: CAppForFilesDlg::OnPaint
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/12/28 - 12:47
// Desc.		:
//=============================================================================
void CAppForFilesDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//=============================================================================
// Method		: CAppForFilesDlg::OnCtlColor
// Access		: protected 
// Returns		: HBRUSH
// Parameter	: CDC * pDC
// Parameter	: CWnd * pWnd
// Parameter	: UINT nCtlColor
// Qualifier	:
// Last Update	: 2011/1/10 - 17:53
// Desc.		:
//=============================================================================
HBRUSH CAppForFilesDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	if (m_brBkgr.GetSafeHandle() != NULL)
	{
#define AFX_MAX_CLASS_NAME 255
#define AFX_STATIC_CLASS _T("Static")
#define AFX_BUTTON_CLASS _T("Button")

		if (nCtlColor == CTLCOLOR_STATIC)
		{
			TCHAR lpszClassName[AFX_MAX_CLASS_NAME + 1];

			::GetClassName(pWnd->GetSafeHwnd(), lpszClassName, AFX_MAX_CLASS_NAME);
			CString strClass = lpszClassName;

			if (strClass == AFX_STATIC_CLASS)
			{
				pDC->SetBkMode(TRANSPARENT);
				return(HBRUSH) ::GetStockObject(WHITE_BRUSH);
			}

			if (strClass == AFX_BUTTON_CLASS)
			{
				if ((pWnd->GetStyle() & BS_GROUPBOX) == 0)
				{
					pDC->SetBkMode(TRANSPARENT);
				}

				return(HBRUSH) ::GetStockObject(HOLLOW_BRUSH);
			}
		}
	}

	return CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
}

//=============================================================================
// Method		: OnQueryDragIcon
// Access		: protected  
// Returns		: HCURSOR
// Qualifier	:
// Last Update	: 2017/3/8 - 10:26
// Desc.		: 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//				  이 함수를 호출합니다.
//=============================================================================
HCURSOR CAppForFilesDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

//=============================================================================
// Method		: CAppForFilesDlg::OnInitDialog
// Access		: virtual protected 
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2010/12/28 - 12:46
// Desc.		:
//=============================================================================
BOOL CAppForFilesDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// Set tray notification window:
	m_nid.hWnd = GetSafeHwnd ();
	m_nid.uCallbackMessage = UM_TRAYNOTIFY;

	// Set tray icon and tooltip:
	m_nid.hIcon = m_hIcon;

	CString strToolTip = _T("App ForFiles");
	_tcsncpy_s (m_nid.szTip, strToolTip, strToolTip.GetLength ());

	Shell_NotifyIcon (NIM_ADD, &m_nid);

// 	SetBackgroundColor(RGB(0xFF, 0xFF, 0xFF));

 	CString strLog;	
 	strLog.Format(_T("AppForFiles (%s : %s)"), GetVersionInfo(_T("ProductVersion")), GetVersionInfo(_T("FileVersion")));
	this->SetWindowText(strLog);	

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2017/3/8 - 10:27
// Desc.		:
//=============================================================================
BOOL CAppForFilesDlg::PreTranslateMessage(MSG* pMsg)
{
	switch (pMsg->message)
	{
	case WM_KEYDOWN:
		if (VK_TAB == pMsg->wParam)
		{
			UINT nID = GetFocus()->GetDlgCtrlID();
			for (int iCnt = 0; iCnt < TABSTYLE_COUNT; iCnt++)
			{
				if (nID == g_TabOrder[iCnt])
				{
					if ((TABSTYLE_COUNT - 1) == iCnt)
					{
						nID = g_TabOrder[0];
					}
					else
					{
						nID = g_TabOrder[iCnt + 1];
					}

					break;
				}
			}

			GetDlgItem(nID)->SetFocus();
		}
		else if (pMsg->wParam == VK_RETURN)
		{
			UINT nID = GetFocus()->GetDlgCtrlID();
			if ((IDOK != nID) && (IDCANCEL != nID))
			{
				for (int iCnt = 0; iCnt < TABSTYLE_COUNT; iCnt++)
				{
					if (nID == g_TabOrder[iCnt])
					{
						if ((TABSTYLE_COUNT - 1) == iCnt)
						{
							nID = g_TabOrder[0];
						}
						else
						{
							nID = g_TabOrder[iCnt + 1];
						}

						break;
					}
				}

				GetDlgItem(nID)->SetFocus();
			}
			return TRUE;
		}
		else if (pMsg->wParam == VK_ESCAPE)
		{
			// 여기에 ESC키 기능 작성       
			return TRUE;
		}
		break;

	default:
		break;
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/3/8 - 10:27
// Desc.		:
//=============================================================================
BOOL CAppForFilesDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Add your specialized code here and/or call the base class

	return CDialogEx::PreCreateWindow(cs);
}

//=============================================================================
// Method		: CAppForFilesDlg::OnBnClickedOk
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/12/28 - 12:47
// Desc.		:
//=============================================================================
void CAppForFilesDlg::OnBnClickedOk()
{	
	OnOK();
}

//=============================================================================
// Method		: OnBnClickedBnPath
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/8 - 13:44
// Desc.		:
//=============================================================================
void CAppForFilesDlg::OnBnClickedBnPath()
{
	CString			szPath;
	ITEMIDLIST*		pidlBrowse;
	TCHAR			pszPathname[MAX_PATH];
	BROWSEINFO		BrInfo;

	memset(&BrInfo, 0, sizeof(BrInfo));

	BrInfo.hwndOwner		= GetSafeHwnd();
	BrInfo.pidlRoot			= NULL;
	BrInfo.pszDisplayName	= pszPathname;
	BrInfo.lpszTitle		= "선택하고자 하는 폴더를 선택해 주십시오.";
	BrInfo.ulFlags			= BIF_RETURNONLYFSDIRS;

	// 다이얼로그 띄우기
	pidlBrowse = (ITEMIDLIST*)SHBrowseForFolder(&BrInfo);
	if (pidlBrowse != NULL)
	{
		// 패스를 얻어옴
		BOOL bSuccess = ::SHGetPathFromIDList(pidlBrowse, pszPathname);
		if (bSuccess)
		{
			szPath = pszPathname;   //<-- 여기서 값을 입력
		}
		else
		{
			MessageBox("잘못된 폴더명입니다.", "lol", MB_OKCANCEL | MB_ICONASTERISK);
		}
	}
	
	if (FALSE == szPath.IsEmpty())
	{
		m_ed_Path.SetWindowText(szPath);
	}
}

//=============================================================================
// Method		: OnBnClickedBnAdd
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/8 - 16:20
// Desc.		:
//=============================================================================
void CAppForFilesDlg::OnBnClickedBnAdd()
{
	ST_ForFile	stForFile;
	CString szValue;
	
	// Path
	m_ed_Path.GetWindowText(szValue);
	if (!PathFileExists(szValue))
	{
		AfxMessageBox(_T("정상적인 경로를 입력하세요."));
		m_ed_Path.SetFocus();
		return;
	}
	stForFile.szPath = szValue;

	// Filter
	m_ed_Filter.GetWindowText(szValue);
	stForFile.szFilter = szValue;

	// Subfolder
	if (BST_CHECKED == m_chk_IncSubFolder.GetCheck())
	{
		stForFile.bIncludeSubFolder = TRUE;
	}
	else
	{
		stForFile.bIncludeSubFolder = FALSE;
	}

	// Days
	m_ed_Days.GetWindowText(szValue);
	stForFile.wDays = _ttoi(szValue);

	m_ForFiles.Add_PathFilter(&stForFile);
	RefreshList();
}

//=============================================================================
// Method		: OnBnClickedBnDelete
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/8 - 14:46
// Desc.		:
//=============================================================================
void CAppForFilesDlg::OnBnClickedBnDelete()
{
	POSITION pos = m_lc_PathList.GetFirstSelectedItemPosition();

	if (pos != NULL)
	{
		while (pos)
		{
			int nItem = m_lc_PathList.GetNextSelectedItem(pos);
			TRACE(_T("Item %d was selected!\n"), nItem);

			// you could do your own processing on nItem here
			//m_ForFiles.m_listPathz.RemoveAt(nItem);
			m_ForFiles.Delete_PathFilter(nItem);
		}

		RefreshList();
	}
}

//=============================================================================
// Method		: OnBnClickedBnClear
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/8 - 14:53
// Desc.		:
//=============================================================================
void CAppForFilesDlg::OnBnClickedBnClear()
{
	if (IDYES == AfxMessageBox(_T("리스트에 있는 항목을 모두 삭제 하시겠습니까?"), MB_YESNO))
	{
		m_ForFiles.RemoveAll_PathFilters();
		m_lc_PathList.DeleteAllItems();
	}
}

//=============================================================================
// Method		: OnBnClickedBnDoForFiles
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/8 - 16:42
// Desc.		:
//=============================================================================
void CAppForFilesDlg::OnBnClickedBnDoForFiles()
{
	if (0 < m_ForFiles.GetCount_PathFilter())
	{
		m_ForFiles.RunForFiles();
	}
	else
	{
		AfxMessageBox(_T("아이템이 비어 있습니다."));
	}
}

//=============================================================================
// Method		: CAppForFilesDlg::OnAppExit
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/12/28 - 12:47
// Desc.		:
//=============================================================================
void CAppForFilesDlg::OnAppExit() 
{	
	//PostMessage (WM_CLOSE);

	DestroyWindow();
	CDialogEx::OnClose();
}

//=============================================================================
// Method		: CAppForFilesDlg::OnAppOpen
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/12/28 - 12:47
// Desc.		:
//=============================================================================
void CAppForFilesDlg::OnAppOpen() 
{
	ShowWindow (SW_SHOWNORMAL);
}

//=============================================================================
// Method		: CAppForFilesDlg::OnTrayNotify
// Access		: protected 
// Returns		: LRESULT
// Parameter	: WPARAM
// Parameter	: LPARAM lp
// Qualifier	:
// Last Update	: 2010/12/28 - 12:47
// Desc.		:
//=============================================================================
LRESULT CAppForFilesDlg::OnTrayNotify(WPARAM /*wp*/, LPARAM lp)
{
	UINT uiMsg = (UINT) lp;

	switch (uiMsg)
	{
	case WM_RBUTTONUP:
		OnTrayContextMenu ();
		return 1;

	case WM_LBUTTONDBLCLK:
		ShowWindow (SW_SHOWNORMAL);
		return 1;
	}

	return 0;
}

//=============================================================================
// Method		: CAppForFilesDlg::OnTrayContextMenu
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/12/28 - 12:47
// Desc.		:
//=============================================================================
void CAppForFilesDlg::OnTrayContextMenu ()
{
	CPoint point;
	CMenu	menu;
	CMenu*	pTrayMenu;

	menu.LoadMenu(IDR_MENU_TRAY);
	pTrayMenu = menu.GetSubMenu(0);

	::GetCursorPos(&point);

	SetForegroundWindow();
	pTrayMenu->TrackPopupMenu(TPM_LEFTALIGN, point.x, point.y, this, NULL);
	SetForegroundWindow();

	pTrayMenu->DestroyMenu();
	menu.DestroyMenu();
}

//=============================================================================
// Method		: CAppForFilesDlg::OnCopyData
// Access		: protected 
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2010/12/28 - 17:36
// Desc.		:
//=============================================================================
LRESULT CAppForFilesDlg::OnCopyData( WPARAM wParam, LPARAM lParam )
{
	PCOPYDATASTRUCT pMyCDS;
	pMyCDS = (PCOPYDATASTRUCT) lParam;

	switch( pMyCDS->dwData )
	{
	case ID_EXT_PROGRAM_INFO:		
		break;
	}

	return TRUE;
}

//=============================================================================
// Method		: CAppForFilesDlg::GetVersionInfo
// Access		: public 
// Returns		: CString
// Parameter	: HMODULE hLib
// Parameter	: CString csEntry
// Qualifier	:
// Last Update	: 2011/1/10 - 17:42
// Desc.		:
//=============================================================================
CString	CAppForFilesDlg::GetVersionInfo(LPCTSTR lpszEntry)
{
	if (lpszEntry == _T(""))
		return _T("");

	DWORD	dwHandle = 0;
	LPVOID	lpTranslate = NULL;
	UINT	nTranslate = 0;
	LPVOID	lpOutBuffer = NULL;
	UINT	nBytes = 0;

	// 현재 실행된 프로그램의 경로를 저장할 변수이다.
	TCHAR	szFileName[MAX_PATH];

	// 현재 실행된 프로그램의 경로를 얻는다.
	GetModuleFileName(AfxGetInstanceHandle(), szFileName, sizeof(szFileName));

	DWORD dwVersionInfoSize = GetFileVersionInfoSize(szFileName, &dwHandle);
	if (dwVersionInfoSize == 0)
		return _T("");

	LPVOID pFileInfo = (LPVOID)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, dwVersionInfoSize);

	GetFileVersionInfo(szFileName, dwHandle, dwVersionInfoSize, pFileInfo);
	VerQueryValue(pFileInfo, TEXT("\\VarFileInfo\\Translation"), &lpTranslate, &nTranslate);

	WORD *id = (WORD*) lpTranslate;
	TCHAR szString[255];
	_stprintf(szString, _T("\\StringFileInfo\\%04X%04X\\%s"), id[0], id[1], lpszEntry);

	VerQueryValue(pFileInfo, szString, &lpOutBuffer, &nBytes);
	if (nBytes == 0)
		return _T("");

	CString strInfo = (TCHAR *)lpOutBuffer;

	HeapFree(GetProcessHeap(), 0, pFileInfo);

	return strInfo;
}

//=============================================================================
// Method		: FindTrayToolbarWindow
// Access		: public static  
// Returns		: HWND
// Qualifier	:
// Last Update	: 2017/3/8 - 13:22
// Desc.		:
//=============================================================================
HWND CAppForFilesDlg::FindTrayWindow()
{
	HWND hWnd_ToolbarWindow32 = NULL;
	HWND hWnd_ShellTrayWnd;

	hWnd_ShellTrayWnd = ::FindWindow(_T("Shell_TrayWnd"), NULL);
	if (hWnd_ShellTrayWnd)
	{
		HWND hWnd_TrayNotifyWnd = ::FindWindowEx(hWnd_ShellTrayWnd, NULL, _T("TrayNotifyWnd"), NULL);
		if (hWnd_TrayNotifyWnd)
		{
			HWND hWnd_SysPager = ::FindWindowEx(hWnd_TrayNotifyWnd, NULL, _T("SysPager"), NULL);        // WinXP

			// WinXP 에서는 SysPager 까지 추적            
			if (hWnd_SysPager)
				hWnd_ToolbarWindow32 = ::FindWindowEx(hWnd_SysPager, NULL, _T("ToolbarWindow32"), NULL);
			// Win2000 일 경우에는 SysPager 가 없이 TrayNotifyWnd -> ToolbarWindow32 로 넘어간다
			else
				hWnd_ToolbarWindow32 = ::FindWindowEx(hWnd_TrayNotifyWnd, NULL, _T("ToolbarWindow32"), NULL);
		}
	}
	return hWnd_ToolbarWindow32;
}

//=============================================================================
// Method		: EraseTrayIcon
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/3/8 - 13:09
// Desc.		:
//=============================================================================
BOOL CAppForFilesDlg::EraseTrayIcon()
{
	struct TRAYDATA
	{
		HWND hwnd;
		UINT uID;
		UINT uCallbackMessage;
		DWORD Reserved[2];
		HICON hIcon;
	};

	HANDLE		m_hProcess;
	LPVOID		m_lpData;
	TBBUTTON	tb;
	TRAYDATA	tray;
	DWORD		dwTrayPid;
	int			TrayCount;

	// Tray 의 윈도우 핸들 얻기
	HWND m_hTrayWnd = FindTrayWindow();
	if (m_hTrayWnd == NULL)
		return FALSE;
	// Tray 의 개수를 구하고
	TrayCount = (int)::SendMessage(m_hTrayWnd, TB_BUTTONCOUNT, 0, 0);

	// Tray 윈도우 핸들의 PID 를 구한다
	GetWindowThreadProcessId(m_hTrayWnd, &dwTrayPid);

	// 해당 Tray 의 Process 를 열어서 메모리를 할당한다
	m_hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, dwTrayPid);
	if (!m_hProcess)
		return FALSE;

	// 해당 프로세스 내에 메모리를 할당
	m_lpData = VirtualAllocEx(m_hProcess, NULL, sizeof(TBBUTTON), MEM_COMMIT, PAGE_READWRITE);
	if (!m_lpData)
		return FALSE;
	// Tray 만큼 뺑뺑이
	for (int i = 0; i < TrayCount; i++)
	{
		::SendMessage(m_hTrayWnd, TB_GETBUTTON, i, (LPARAM)m_lpData);

		// TBBUTTON 의 구조체와 TRAYDATA 의 내용을 얻기
		ReadProcessMemory(m_hProcess, m_lpData, (LPVOID)&tb, sizeof(TBBUTTON), NULL);
		ReadProcessMemory(m_hProcess, (LPCVOID)tb.dwData, (LPVOID)&tray, sizeof(tray), NULL);
		// 각각 트레이의 프로세스 번호를 얻어서
		DWORD dwProcessId = 0;
		GetWindowThreadProcessId(tray.hwnd, &dwProcessId);

		// Process 가 없는 경우 TrayIcon 을 삭제한다
		if (dwProcessId == 0)
		{
			NOTIFYICONDATA        icon;
			icon.cbSize = sizeof(NOTIFYICONDATA);
			icon.hIcon = tray.hIcon;
			icon.hWnd = tray.hwnd;
			icon.uCallbackMessage = tray.uCallbackMessage;
			icon.uID = tray.uID;
			Shell_NotifyIcon(NIM_DELETE, &icon);
		}
	}
	// 가상 메모리 해제와 프로세스 핸들 닫기
	VirtualFreeEx(m_hProcess, m_lpData, NULL, MEM_RELEASE);
	CloseHandle(m_hProcess);

	return TRUE;
}

//=============================================================================
// Method		: InitListCtrl
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/8 - 13:45
// Desc.		:
//=============================================================================
void CAppForFilesDlg::InitListCtrl()
{
 	for (int nCol = 0; nCol < WLH_MaxCol; nCol++)
 	{
 		m_lc_PathList.InsertColumn(nCol, g_lpszHeader[nCol], iListAglin[nCol], iHeaderWidth[nCol]);
 	}
}

//=============================================================================
// Method		: Insert_PathFilter
// Access		: protected  
// Returns		: void
// Parameter	: __in ST_ForFile * pstForFile
// Qualifier	:
// Last Update	: 2017/3/8 - 14:37
// Desc.		:
//=============================================================================
void CAppForFilesDlg::Insert_PathFilter(__in ST_ForFile* pstForFile)
{
	int iNewCount = m_lc_PathList.GetItemCount();

	CString szItem;

	m_lc_PathList.InsertItem(iNewCount, _T(""));

	// No
	szItem.Format(_T("%d"), iNewCount + 1);
	m_lc_PathList.SetItemText(iNewCount, WLH_No, szItem);

	// Path
	szItem = pstForFile->szPath;
	m_lc_PathList.SetItemText(iNewCount, WLH_Path, szItem);
	
	// Filter
	szItem = pstForFile->szFilter;
	m_lc_PathList.SetItemText(iNewCount, WLH_Filter, szItem);
	
	// Subfolder
	if (pstForFile->bIncludeSubFolder)
	{
		szItem = _T("Yes");
	}
	else
	{
		szItem = _T("No");
	}
	m_lc_PathList.SetItemText(iNewCount, WLH_Subfolder, szItem);

	// Days
	szItem.Format(_T("%d"), pstForFile->wDays);
	m_lc_PathList.SetItemText(iNewCount, WLH_Days, szItem);
}

//=============================================================================
// Method		: Insert_PathFilter
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/8 - 14:06
// Desc.		:
//=============================================================================
void CAppForFilesDlg::RefreshList()
{
	m_lc_PathList.DeleteAllItems();

	//INT_PTR iCount = m_ForFiles.m_listPathz.GetCount();
	INT_PTR iCount = m_ForFiles.GetCount_PathFilter();

	for (INT_PTR iIdx = 0; iIdx < iCount; iIdx++)
	{
		//Insert_PathFilter(&m_ForFiles.m_listPathz.GetAt(iIdx));
		Insert_PathFilter(&m_ForFiles.GetAt_PathFilter(iIdx));
	}
}

//=============================================================================
// Method		: CAppForFilesDlg::StartWatchTimer
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/12/29 - 13:39
// Desc.		: 감시 타이머 구동
//=============================================================================
void CAppForFilesDlg::StartWatchTimer()
{
	m_ForFiles.StartTimer();
}

//=============================================================================
// Method		: CAppForFilesDlg::StopWatchTimer
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/12/29 - 13:39
// Desc.		: 감시 타이머 정지
//=============================================================================
void CAppForFilesDlg::StopWatchTimer()
{
	m_ForFiles.StopTimer();
}