﻿//*****************************************************************************
// Filename	: 	Define_MES_LG.h
// Created	:	2016/5/10 - 15:32
// Modified	:	2016/5/10 - 15:32
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Define_MES_LG_h__
#define Define_MES_LG_h__

#include <afxwin.h>

typedef enum enLG_MES_Const
{
	LG_MES_STX			= 'H',
	LG_MES_ETX			= 'T',
	LG_MES_DELIMETER	= '*',

	LG_MES_DELIMETER_CNT		= 3,
	LG_MES_DELIMETER_MRA_CNT	= 5,
	LG_MES_MinProtocolSize		= 5,
	LG_MES_MaxProtocolSize		= 50,
};

typedef struct _tag_LG_MES_Protocol
{
	char		STX;
	char		Delimeter_1;
	CStringA	szLotID;
	char		Delimeter_2;
	CStringA	szLotTryCount;
	char		Delimeter_3;
	CStringA	szWorkType;
	char		Delimeter_4;
	CStringA	szMSG;
	char		Delimeter_5;
	char		ETX;

	CStringA	szProtocol;

	_tag_LG_MES_Protocol()
	{
		;
	}

	BOOL SetRecvProtocol(__in const char* pszProtocol, __in UINT_PTR nLength)
	{
		if (NULL == pszProtocol)
			return FALSE;

		if (nLength < LG_MES_MinProtocolSize)
			return FALSE;

		szProtocol = pszProtocol;
		CStringA strProtocol = pszProtocol;

		CStringA resToken;
		CStringA szToken;
		szToken.AppendChar(LG_MES_DELIMETER);
		int curPos = 0;

		resToken = strProtocol.Tokenize(szToken, curPos);
		if (resToken.IsEmpty())
			return FALSE;
		STX = resToken.GetAt(0);

		resToken = strProtocol.Tokenize(szToken, curPos);
		if (resToken.IsEmpty())
			return FALSE;
		szLotID = resToken;

		resToken = strProtocol.Tokenize(szToken, curPos);
		if (resToken.IsEmpty())
			return FALSE;
		szLotTryCount = resToken;

		ETX = strProtocol.GetAt(curPos);
		
		// Check STX
		if (LG_MES_STX != STX)
			return FALSE;

		// Check ETX
		if (LG_MES_ETX != ETX)
			return FALSE;

		return TRUE;
	};

	BOOL SetRecvProtocol_MRA(__in const char* pszProtocol, __in UINT_PTR nLength)
	{
		if (NULL == pszProtocol)
			return FALSE;

		if (nLength < LG_MES_MinProtocolSize)
			return FALSE;

		szProtocol = pszProtocol;
		CStringA strProtocol = pszProtocol;

		CStringA resToken;
		CStringA szToken;
		szToken.AppendChar(LG_MES_DELIMETER);
		int curPos = 0;

		resToken = strProtocol.Tokenize(szToken, curPos);
		if (resToken.IsEmpty())
			return FALSE;
		STX = resToken.GetAt(0);

		resToken = strProtocol.Tokenize(szToken, curPos);
		if (resToken.IsEmpty())
			return FALSE;
		szLotID = resToken;

		resToken = strProtocol.Tokenize(szToken, curPos);
		if (resToken.IsEmpty())
			return FALSE;
		szLotTryCount = resToken;

		resToken = strProtocol.Tokenize(szToken, curPos);
		if (resToken.IsEmpty())
			return FALSE;
		szWorkType = resToken;

		resToken = strProtocol.Tokenize(szToken, curPos);
		if (resToken.IsEmpty())
			return FALSE;
		szMSG = resToken;

		ETX = strProtocol.GetAt(curPos);

		// Check STX
		if (LG_MES_STX != STX)
			return FALSE;

		// Check ETX
		if (LG_MES_ETX != ETX)
			return FALSE;

		return TRUE;
	};


}ST_LG_MES_Protocol, *PST_LG_MES_Protocol;

/*
H*LOTID*차수*T  
H*LOTID*1*T

C:\BMS_MES\설비코드_차수_YYYYMMDDHHMISS.txt


NG 판정인 경우
LOTID, 차수, 0, A1R101:항목값1(계측치) : 불량종류 : 0, A2R102 : 항목값2(계측치) : 불량종류 : 0, …, N(n) : 항목값n : 불량종류 : 0[CrLf]

OK 판정인 경우
LOTID, 차수, 1[CrLf]
LOTID,차수,합부,항목값1:항목1합부,항목값2:항목2합부,…,항목값n:항목n합부[CrLf]


*/

#endif // Define_MES_LG_h__
