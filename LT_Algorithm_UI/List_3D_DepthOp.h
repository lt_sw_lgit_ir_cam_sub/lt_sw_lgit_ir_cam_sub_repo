﻿#ifndef List_3D_DepthOp_h__
#define List_3D_DepthOp_h__

#pragma once

#include "List_Cfg_VIBase.h"
#include "Def_T_3D_Depth.h"

typedef enum enListItemNum_3D_DepthOp
{
	T_3D_DepthOp_ItemNum = 1,
};

//-----------------------------------------------------------------------------
// List_CenterPointInfo
//-----------------------------------------------------------------------------
class CList_3D_DepthOp : public CList_Cfg_VIBase
{
	DECLARE_DYNAMIC(CList_3D_DepthOp)

public:
	CList_3D_DepthOp();
	virtual ~CList_3D_DepthOp();
	UINT m_nMessage;
	void SetWindowMessage(UINT nMeg){
		m_nMessage = nMeg;
	};
protected:
	
	DECLARE_MESSAGE_MAP()

	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg void	OnNMClick				(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk				(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL	OnMouseWheel			(UINT nFlags, short zDelta, CPoint pt);

	afx_msg void	OnEnKillFocusComboType	();
	afx_msg void	OnEnSelectComboType		();
	afx_msg void	OnEnKillFocusComboColor	();
	afx_msg void	OnEnSelectComboColor	();

	afx_msg void	OnEnKillFocusEdit		();

	BOOL			UpdateCellData			(UINT nRow, UINT nCol, int iValue);
	BOOL			UpdateCelldbData		(UINT nRow, UINT nCol, double dValue);

	CFont						m_Font;
	CEdit						m_ed_CellEdit;
	UINT						m_nEditCol;
	UINT						m_nEditRow;
	CComboBox					m_cb_Type;
	CComboBox					m_cb_Color;

	ST_3D_Depth_Opt*		m_pstConfigInfo = NULL;

public:

	void SetPtr_ConfigInfo(ST_3D_Depth_Opt* pstConfigInfo)
	{
		if (pstConfigInfo == NULL)
			return;

		m_pstConfigInfo = pstConfigInfo;
	};

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();
	
};

#endif // List_CenterPointInfo_h__
