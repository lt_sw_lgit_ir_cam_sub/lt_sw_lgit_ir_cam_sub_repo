﻿#ifndef Def_T_Distortion_h__
#define Def_T_Distortion_h__

#include <afxwin.h>

#include "Def_T_Common.h"

#pragma pack(push,1)

typedef enum enDistortion_Spec
{
	Spec_Distortion = 0,
	Spec_Distortion_MAX,
};

static LPCTSTR	g_szDistortion_Spec[] =
{
	_T("Distortion"),
	NULL
};

typedef enum enROI_Distortion
{
	ROI_Dis_LT = 0,
	ROI_Dis_LB,
	ROI_Dis_RT,
	ROI_Dis_RB,
	ROI_Dis_CT,
	ROI_Dis_CB,
	ROI_Dis_Max,
};

static LPCTSTR g_szROI_Distortion[] =
{
	_T("LT"),
	_T("LB"),
	_T("RT"),
	_T("RB"),
	_T("CT"),
	_T("CB"),
	NULL
};

typedef struct _tag_Distortion_Opt
{
	ST_RegionROI stRegion[ROI_Dis_Max];

	double	dbOffset;

	ST_Comm_Spec stSpec_Min;
	ST_Comm_Spec stSpec_Max;

	_tag_Distortion_Opt()
	{
		for (UINT nIdx = 0; nIdx < ROI_Dis_Max; nIdx++)
		{
			stRegion[nIdx].Reset();
		}

		dbOffset = 0.0;
		stSpec_Min.Reset();
		stSpec_Max.Reset();
	};

	_tag_Distortion_Opt& operator= (_tag_Distortion_Opt& ref)
	{
		for (UINT nIdx = 0; nIdx < ROI_Dis_Max; nIdx++)
		{
			stRegion[nIdx] = ref.stRegion[nIdx];
		}

		dbOffset	= ref.dbOffset;
		stSpec_Min	= ref.stSpec_Min;
		stSpec_Max	= ref.stSpec_Max;

		return *this;
	};

}ST_Distortion_Opt, *PST_Distortion_Opt;

typedef struct _tag_Distortion_Data
{
	UINT nResult;

	double dbValue;

	CPoint ptCenter[ROI_Dis_Max];
	CRect  rtTestPicROI[ROI_Dis_Max];
	_tag_Distortion_Data()
	{
		Reset();
	};

	void Reset()
	{
		nResult = 1;
		dbValue = 0.0;

		for (UINT nIdx = 0; nIdx < ROI_Dis_Max; nIdx++)
		{
			ptCenter[nIdx].x = -1;
			ptCenter[nIdx].y = -1;

			rtTestPicROI[nIdx].SetRectEmpty();
		}
	};

	void FailData(){
		nResult = 0;
		dbValue = 0.0;

		for (UINT nIdx = 0; nIdx < ROI_Dis_Max; nIdx++)
		{
			ptCenter[nIdx].x = -1;
			ptCenter[nIdx].y = -1;

			rtTestPicROI[nIdx].SetRectEmpty();
		}
	};

	_tag_Distortion_Data& operator= (_tag_Distortion_Data& ref)
	{
		nResult = ref.nResult;
		dbValue = ref.dbValue;

		for (UINT nIdx = 0; nIdx < ROI_Dis_Max; nIdx++)
		{
			ptCenter[nIdx] = ref.ptCenter[nIdx];
			rtTestPicROI[nIdx] = ref.rtTestPicROI[nIdx];
		}

		return *this;
	};

}ST_Distortion_Data, *PST_Distortion_Data;

typedef struct _tag_TI_Distortion
{
	ST_Distortion_Opt	stDistortionOpt;	// 검사 기준 데이터
	ST_Distortion_Data	stDistortionData;	// 측정 데이터

	_tag_TI_Distortion()
	{
	};

	void Reset()
	{
		stDistortionData.Reset();
	};

	_tag_TI_Distortion& operator= (_tag_TI_Distortion& ref)
	{
		stDistortionOpt		= ref.stDistortionOpt;
		stDistortionData	= ref.stDistortionData;

		return *this;
	};

}ST_TI_Distortion, *PST_TI_Distortion;

#pragma pack(pop)

#endif // Def_Distortion_h__