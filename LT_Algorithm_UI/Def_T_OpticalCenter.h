﻿#ifndef Def_T_OpticalCenter_h__
#define Def_T_OpticalCenter_h__

#include <afxwin.h>

#include "Def_T_Common.h"

#pragma pack(push,1)

typedef enum enOpticalCenter_Spec
{
	Spec_OC_X = 0,
	Spec_OC_Y,
	Spec_OC_Max,
};

static LPCTSTR	g_szOpticalCenter_Spec[] =
{
	_T("Optical Center X"),
	_T("Optical Center Y"),
	NULL
};

typedef struct _tag_OpticalCenter_Opt
{
	UINT nStandarX;
	UINT nStandarY;

	// 옵셋 광축
	int iOffsetX;
	int iOffsetY;

	ST_RegionROI stRegion;

	ST_Comm_Spec stSpec_Min[Spec_OC_Max];
	ST_Comm_Spec stSpec_Max[Spec_OC_Max];

	_tag_OpticalCenter_Opt()
	{
		nStandarX	= 0;
		nStandarY	= 0;
		
		iOffsetX	= 0;
		iOffsetY	= 0;

		stRegion.Reset();

		for (UINT nIdx = 0; nIdx < Spec_OC_Max; nIdx++)
		{
			stSpec_Min[nIdx].Reset();
			stSpec_Max[nIdx].Reset();
		}
	};

	_tag_OpticalCenter_Opt& operator= (_tag_OpticalCenter_Opt& ref)
	{
		nStandarX	= ref.nStandarX;
		nStandarY	= ref.nStandarY;

		iOffsetX	= ref.iOffsetX;
		iOffsetY	= ref.iOffsetY;

		stRegion	= ref.stRegion;

		for (UINT nIdx = 0; nIdx < Spec_OC_Max; nIdx++)
		{
			stSpec_Min[nIdx] = ref.stSpec_Min[nIdx];
			stSpec_Max[nIdx] = ref.stSpec_Max[nIdx];
		}

		return *this;
	};

}ST_OpticalCenter_Opt, *PST_OpticalCenter_Opt;

typedef struct _tag_OpticalCenter_Data
{
	// 결과
	UINT nResult;
	UINT nDetectResult;

	// 양불
	UINT nResultX;
	UINT nResultY;

	// 결과 값
	UINT nResultPosX;
	UINT nResultPosY;

	UINT nPixX;
	UINT nPixY;

	int iStandPosDevX;
	int iStandPosDevY;


	void Reset()
	{
		nResult		= 1;
		nDetectResult	= 0;
		nResultX	= 1;
		nResultY	= 1;
		nResultPosX = 0;
		nResultPosY = 0;
		iStandPosDevX = 0;
		iStandPosDevY = 0;
		nPixX = 0;
		nPixY = 0;
	};
	void FailData(){
		nResult = 0;
		nDetectResult = 0;
		nResultX = 0;
		nResultY = 0;
		nResultPosX = 0;
		nResultPosY =0;
		iStandPosDevX = 0;
		iStandPosDevY = 0;
		nPixX = 0;
		nPixY = 0;
	};
	_tag_OpticalCenter_Data()
	{
		Reset();
	};

	_tag_OpticalCenter_Data& operator= (_tag_OpticalCenter_Data& ref)
	{
		nResult		= ref.nResult;
		nResultX	= ref.nResultX;
		nResultY	= ref.nResultY;
		nResultPosX = ref.nResultPosX;
		nResultPosY = ref.nResultPosY;
		iStandPosDevX = ref.iStandPosDevX;
		iStandPosDevY = ref.iStandPosDevY;
		nDetectResult = ref.nDetectResult;
		nPixX = ref.nPixX;
		nPixY = ref.nPixY;

		return *this;
	};

}ST_OpticalCenter_Data, *PST_OpticalCenter_Data;

typedef struct _tag_TI_OpticalCenter
{
	ST_OpticalCenter_Opt	stCenterOpt;	// 검사 기준 데이터
	ST_OpticalCenter_Data	stCenterData;	// 측정 데이터

	_tag_TI_OpticalCenter()
	{
	};

	void Reset()
	{
		stCenterData.Reset();
	};

	_tag_TI_OpticalCenter& operator= (_tag_TI_OpticalCenter& ref)
	{
		stCenterOpt		= ref.stCenterOpt;
		stCenterData	= ref.stCenterData;

		return *this;
	};

}ST_TI_OpticalCenter, *PST_TI_OpticalCenter;

#pragma pack(pop)

#endif // Def_OpticalCenter_h__