﻿#ifndef Def_T_EEPROM_Verify_h__
#define Def_T_EEPROM_Verify_h__

#include <afxwin.h>

#include "Def_T_Common.h"

#pragma pack(push,1)

typedef enum enEEPROM_Verify_Spec
{
	Spec_EEP_OC_X = 0,
	Spec_EEP_OC_Y,
	Spec_EEP_CheckSum1,
	Spec_EEP_CheckSum2,
	Spec_EEP_Max,
};

static LPCTSTR	g_szEEPROM_Verify_Spec[] =
{
	_T("OC X"),			
	_T("OC Y	"),			
	_T("Check Sum [양불판정]"),			
	_T("Check Sum2 [양불판정]"),		
	NULL
};

typedef struct _tag_EEPROM_Verify_Opt
{
	int iBlockSize;
	int iThreshold[Spec_EEP_Max];

	DWORD wslaveid;
	DWORD wocxaddress;
	DWORD wocxlength;
	DWORD wocyaddress;
	DWORD wocylength;
	DWORD waddress;

	ST_Comm_Spec stSpec_Min[Spec_EEP_Max];
	ST_Comm_Spec stSpec_Max[Spec_EEP_Max];

	_tag_EEPROM_Verify_Opt()
	{
		iBlockSize	= 4;
		wslaveid	= 0xAC;
		wocxaddress = 0x40;
		wocxlength	= 4;
		wocyaddress = 0x44;
		wocylength	= 4;
		waddress	= 0xFA;

		for (UINT nIdx = 0; nIdx < Spec_EEP_Max; nIdx++)
		{
			iThreshold[nIdx] = 0;
			stSpec_Min[nIdx].Reset();
			stSpec_Max[nIdx].Reset();
		}
	};

	_tag_EEPROM_Verify_Opt& operator= (_tag_EEPROM_Verify_Opt& ref)
	{
		iBlockSize = ref.iBlockSize;
		wslaveid = ref.wslaveid;
		wocxaddress = ref.wocxaddress;
		wocxlength = ref.wocxlength;
		wocyaddress = ref.wocyaddress;
		wocylength = ref.wocylength;
		waddress = ref.waddress;
	
		for (UINT nIdx = 0; nIdx < Spec_EEP_Max; nIdx++)
		{
			iThreshold[nIdx] = ref.iThreshold[nIdx];
			stSpec_Min[nIdx] = ref.stSpec_Min[nIdx];
			stSpec_Max[nIdx] = ref.stSpec_Max[nIdx];
		}

		return *this;
	};

}ST_EEPROM_Verify_Opt, *PST_EEPROM_Verify_Opt;

typedef struct _tag_EEPROM_Verify_Data
{
	// 결과
	UINT nResult;

	UINT nEachResult[Spec_EEP_Max];
	float fEachData[Spec_EEP_Max];

	float   fOC_X;
	float   fOC_Y;
	float   fCheckSum1;
	float   fCheckSum2;

	_tag_EEPROM_Verify_Data()
	{
		Reset();
	};

	void Reset()
	{
		nResult	= 1;

		fOC_X		= 0;
		fOC_Y		= 0;
		fCheckSum1  = 0;
		fCheckSum2  = 0;

		for (UINT nIdx = 0; nIdx < Spec_EEP_Max; nIdx++)
		{
			nEachResult[nIdx]	= 0;
			fEachData[nIdx] = 0;
		}
	};

	void FailData()
	{
		nResult = 0;

		for (UINT nIdx = 0; nIdx < Spec_EEP_Max; nIdx++)
		{
			nEachResult[nIdx] = 0;
			fEachData[nIdx] = 0;
		}
	};

	_tag_EEPROM_Verify_Data& operator= (_tag_EEPROM_Verify_Data& ref)
	{
		nResult			= ref.nResult;
		fOC_X			= ref.fOC_X;
		fOC_Y			= ref.fOC_Y;
		fCheckSum1		= ref.fCheckSum1;
		fCheckSum2		= ref.fCheckSum2;

		for (UINT nIdx = 0; nIdx < Spec_EEP_Max; nIdx++)
		{
			nEachResult[nIdx]	= ref.nEachResult[nIdx];
			fEachData[nIdx] = ref.fEachData[nIdx];
		}

		return *this;
	};

}ST_EEPROM_Verify_Data, *PST_EEPROM_Verify_Data;


typedef struct _tag_TI_EEPROM_Verify
{
	ST_EEPROM_Verify_Opt		stEEPROM_VerifyOpt;		// 검사 기준 데이터
	ST_EEPROM_Verify_Data		stEEPROM_VerifyData;		// 측정 데이터

	_tag_TI_EEPROM_Verify()
	{
	};

	void Reset()
	{
		stEEPROM_VerifyData.Reset();
	};

	_tag_TI_EEPROM_Verify& operator= (_tag_TI_EEPROM_Verify& ref)
	{
		stEEPROM_VerifyOpt	= ref.stEEPROM_VerifyOpt;
		stEEPROM_VerifyData	= ref.stEEPROM_VerifyData;

		return *this;
	};

}ST_TI_EEPROM_Verify, *PST_TI_EEPROM_Verify;

#pragma pack(pop)

#endif // Def_EEPROM_Verify_h__