﻿// Wnd_Cfg_SFR.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_Cfg_SFR.h"
#include "resource.h"

static LPCTSTR	g_szSFR_Static[] =
{
	_T("PARAMETER & SPEC"),
	_T("CAPTURE COUNT"),
	_T("CAPTURE DELAY"),
	_T("SFR Result"),
	_T("GROUP SPEC		"),
	_T("GROUP TILT SPEC	"),
	_T("ROI				"),
	_T("X1_Tilt_A"),
	_T("X1_Tilt_B"),
	_T("X2_Tilt_A"),
	_T("X2_Tilt_B"),
	_T("Y1_Tilt_A"),
	_T("Y1_Tilt_B"),
	_T("Y2_Tilt_A"),
	_T("Y2_Tilt_B"),
	_T("X1_Tilt_UpperLimit"),
	_T("X2_Tilt_UpperLimit"),
	_T("Y1_Tilt_UpperLimit"),
	_T("Y2_Tilt_UpperLimit"),
	NULL
};

static LPCTSTR	g_szSFR_Button[] =
{
	_T("RESET"),
	_T("TEST"),
	NULL
};

static LPCTSTR	g_szSFR_ResultMode[] =
{
	_T("MTFResult"),
	_T("CyclePerPixelResult"),
	_T("CyclePerMMResult"),
	NULL
};

static LPCTSTR	g_szSFR_Title[] =
{
	_T("0"),
	_T("1"),
	_T("2"),
	_T("3"),
	_T("4"),
	_T("5"),
	_T("6"),
	_T("7"),
	_T("8"),
	_T("9"),
	_T("10"),
	_T("11"),
	_T("12"),
	_T("13"),
	_T("14"),
	_T("15"),
	_T("16"),
	_T("17"),
	_T("18"),
	_T("19"),
	_T("20"),
	_T("21"),
	_T("22"),
	_T("23"),
	_T("24"),
	_T("25"),
	_T("26"),
	_T("27"),
	_T("28"),
	_T("29"),

	NULL
};

// CWnd_Cfg_SFR
typedef enum SFROptID
{
	IDC_BTN_ITEM	= 1001,
	IDC_CMB_ITEM	= 2001,
	IDC_EDT_ITEM	= 3001,
	IDC_LIST_ITEM	= 4001,

	IDC_ED_SPEC_MIN = 5001,
	IDC_ED_SPEC_MAX = 6001,
	IDC_CHK_SPEC_MIN = 7001,
	IDC_CHK_SPEC_MAX = 8001,
};

IMPLEMENT_DYNAMIC(CWnd_Cfg_SFR, CWnd_Cfg_VIBase)

CWnd_Cfg_SFR::CWnd_Cfg_SFR()
{
	m_pstConfigInfo = NULL;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_SFR::~CWnd_Cfg_SFR()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_SFR, CWnd_Cfg_VIBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
END_MESSAGE_MAP()

// CWnd_Cfg_SFR 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_Cfg_SFR::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_Cfg_VIBase::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < STI_SFR_MAX; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdex].Create(g_szSFR_Static[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	m_st_Item[STI_SFR_PARAM].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Item[STI_SFR_SPEC].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Item[STI_SFR_SPEC_2].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Item[STI_SFR_ROI].SetColorStyle(CVGStatic::ColorStyle_DarkGray);

	for (UINT nIdex = 0; nIdex < BTN_SFR_MAX; nIdex++)
	{
		m_bn_Item[nIdex].Create(g_szSFR_Button[nIdex], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_ITEM + nIdex);
		m_bn_Item[nIdex].SetFont(&m_font);
	}

	for (UINT nIdex = 0; nIdex < EDT_SFR_MAX; nIdex++)
	{
		m_ed_Item[nIdex].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EDT_ITEM + nIdex);
		m_ed_Item[nIdex].SetWindowText(_T("0"));
		m_ed_Item[nIdex].SetValidChars(_T("0123456789.-"));
	}

	for (UINT nIdex = 0; nIdex < CMB_SFR_MAX; nIdex++)
	{
		m_cb_Item[nIdex].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CMB_ITEM + nIdex);
	}
	for (UINT nIdex = 0; NULL <g_szSFR_ResultMode[nIdex]; nIdex++)
	{
		m_cb_Item[CMB_SFR_ResultMODE].InsertString(nIdex, g_szSFR_ResultMode[nIdex]);
	}
	m_cb_Item[CMB_SFR_ResultMODE].SetCurSel(0);

	//for (UINT nIdex = STI_SFR_X1_Tilt_A; nIdex < STI_SFR_MAX; nIdex++)
	//{
	//	m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_DarkGray);

	//}
	/*for (UINT nIdex = 0; NULL <g_szSFR_Title[nIdex]; nIdex++)
	{
	m_cb_Item[CMB_SFR_X1_Tilt_A].InsertString(nIdex, g_szSFR_Title[nIdex]);
	}
	m_cb_Item[CMB_SFR_X1_Tilt_A].SetCurSel(0);

	for (UINT nIdex = 0; NULL <g_szSFR_Title[nIdex]; nIdex++)
	{
	m_cb_Item[CMB_SFR_X1_Tilt_B].InsertString(nIdex, g_szSFR_Title[nIdex]);
	}
	m_cb_Item[CMB_SFR_X1_Tilt_B].SetCurSel(0);

	for (UINT nIdex = 0; NULL <g_szSFR_Title[nIdex]; nIdex++)
	{
	m_cb_Item[CMB_SFR_X2_Tilt_A].InsertString(nIdex, g_szSFR_Title[nIdex]);
	}
	m_cb_Item[CMB_SFR_X2_Tilt_A].SetCurSel(0);

	for (UINT nIdex = 0; NULL <g_szSFR_Title[nIdex]; nIdex++)
	{
	m_cb_Item[CMB_SFR_X2_Tilt_B].InsertString(nIdex, g_szSFR_Title[nIdex]);
	}
	m_cb_Item[CMB_SFR_X2_Tilt_B].SetCurSel(0);

	for (UINT nIdex = 0; NULL <g_szSFR_Title[nIdex]; nIdex++)
	{
	m_cb_Item[CMB_SFR_Y1_Tilt_A].InsertString(nIdex, g_szSFR_Title[nIdex]);
	}
	m_cb_Item[CMB_SFR_Y1_Tilt_A].SetCurSel(0);

	for (UINT nIdex = 0; NULL <g_szSFR_Title[nIdex]; nIdex++)
	{
	m_cb_Item[CMB_SFR_Y1_Tilt_B].InsertString(nIdex, g_szSFR_Title[nIdex]);
	}
	m_cb_Item[CMB_SFR_Y1_Tilt_B].SetCurSel(0);

	for (UINT nIdex = 0; NULL <g_szSFR_Title[nIdex]; nIdex++)
	{
	m_cb_Item[CMB_SFR_Y2_Tilt_A].InsertString(nIdex, g_szSFR_Title[nIdex]);
	}
	m_cb_Item[CMB_SFR_Y2_Tilt_A].SetCurSel(0);

	for (UINT nIdex = 0; NULL <g_szSFR_Title[nIdex]; nIdex++)
	{
	m_cb_Item[CMB_SFR_Y2_Tilt_B].InsertString(nIdex, g_szSFR_Title[nIdex]);
	}
	m_cb_Item[CMB_SFR_Y2_Tilt_B].SetCurSel(0);*/

	m_st_CapItem.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_CapItem.SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_CapItem.SetFont_Gdip(L"Arial", 9.0F);
	m_st_CapItem.Create(_T("Item"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_CapSpecMin.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_CapSpecMin.SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_CapSpecMin.SetFont_Gdip(L"Arial", 9.0F);
	m_st_CapSpecMin.Create(_T("Spec Min"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_CapSpecMax.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_CapSpecMax.SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_CapSpecMax.SetFont_Gdip(L"Arial", 9.0F);
	m_st_CapSpecMax.Create(_T("Spec Max"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	/*
	for (UINT nIdx = 0; nIdx < ITM_SFR_Max; nIdx++)
	{
		m_st_Spec[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Spec[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Spec[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Spec[nIdx].Create(g_szSFRGroup[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

		m_ed_SpecMin[nIdx].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_SPEC_MIN + nIdx);
		m_ed_SpecMin[nIdx].SetWindowText(_T("0"));
		m_ed_SpecMin[nIdx].SetValidChars(_T("0123456789.-"));

		m_ed_SpecMax[nIdx].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_SPEC_MAX + nIdx);
		m_ed_SpecMax[nIdx].SetWindowText(_T("0"));
		m_ed_SpecMax[nIdx].SetValidChars(_T("0123456789.-"));

		m_ed_SpecMin[nIdx].SetFont(&m_font);
		m_ed_SpecMax[nIdx].SetFont(&m_font);

		m_chk_SpecMin[nIdx].Create(_T(""), WS_VISIBLE | WS_BORDER | BS_AUTOCHECKBOX, rectDummy, this, IDC_CHK_SPEC_MIN + nIdx);
		m_chk_SpecMax[nIdx].Create(_T(""), WS_VISIBLE | WS_BORDER | BS_AUTOCHECKBOX, rectDummy, this, IDC_CHK_SPEC_MAX + nIdx);

		m_chk_SpecMin[nIdx].SetMouseCursorHand();
		m_chk_SpecMin[nIdx].SetImage(IDB_UNCHECKED_16);
		m_chk_SpecMin[nIdx].SetCheckedImage(IDB_CHECKED_16);
		m_chk_SpecMin[nIdx].SizeToContent();

		m_chk_SpecMax[nIdx].SetMouseCursorHand();
		m_chk_SpecMax[nIdx].SetImage(IDB_UNCHECKED_16);
		m_chk_SpecMax[nIdx].SetCheckedImage(IDB_CHECKED_16);
		m_chk_SpecMax[nIdx].SizeToContent();
	}
	*/
	/*
	for (UINT nIdx = 0; nIdx < ITM_SFR_TiltMax; nIdx++)
	{
		m_st_TiltSpec[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_TiltSpec[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_TiltSpec[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_st_TiltSpec[nIdx].Create(g_szSFRTilt[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

		m_ed_TiltSpecMin[nIdx].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_SPEC_MIN + nIdx);
		m_ed_TiltSpecMin[nIdx].SetWindowText(_T("0"));
		m_ed_TiltSpecMin[nIdx].SetValidChars(_T("0123456789.-"));

		m_ed_TiltSpecMax[nIdx].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_SPEC_MAX + nIdx);
		m_ed_TiltSpecMax[nIdx].SetWindowText(_T("0"));
		m_ed_TiltSpecMax[nIdx].SetValidChars(_T("0123456789.-"));

		m_ed_TiltSpecMin[nIdx].SetFont(&m_font);
		m_ed_TiltSpecMax[nIdx].SetFont(&m_font);

		m_chk_TiltSpecMin[nIdx].Create(_T(""), WS_VISIBLE | WS_BORDER | BS_AUTOCHECKBOX, rectDummy, this, IDC_CHK_SPEC_MIN + nIdx);
		m_chk_TiltSpecMax[nIdx].Create(_T(""), WS_VISIBLE | WS_BORDER | BS_AUTOCHECKBOX, rectDummy, this, IDC_CHK_SPEC_MAX + nIdx);

		m_chk_TiltSpecMin[nIdx].SetMouseCursorHand();
		m_chk_TiltSpecMin[nIdx].SetImage(IDB_UNCHECKED_16);
		m_chk_TiltSpecMin[nIdx].SetCheckedImage(IDB_CHECKED_16);
		m_chk_TiltSpecMin[nIdx].SizeToContent();

		m_chk_TiltSpecMax[nIdx].SetMouseCursorHand();
		m_chk_TiltSpecMax[nIdx].SetImage(IDB_UNCHECKED_16);
		m_chk_TiltSpecMax[nIdx].SetCheckedImage(IDB_CHECKED_16);
		m_chk_TiltSpecMax[nIdx].SizeToContent();
	}
	*/
	m_List.SetOwner(GetOwner());
	m_List.SetWindowMessage(m_wm_ChangeOption);
	m_List.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM);

	m_List_Group.SetOwner(GetOwner());
	m_List_Group.SetWindowMessage(m_wm_ChangeOption);
	m_List_Group.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM + 1);

	m_List_Tilt.SetOwner(GetOwner());
	m_List_Tilt.SetWindowMessage(m_wm_ChangeOption);
	m_List_Tilt.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM + 2);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_Cfg_SFR::OnSize(UINT nType, int cx, int cy)
{
	CWnd_Cfg_VIBase::OnSize(nType, cx, cy);

	int iIdxCnt  = SfOp_ItemNum;
	int iMargin  = 10;
	int iSpacing = 5;

	int iLeft	 = iMargin;
	int iTop	 = iMargin;
	int iWidth   = cx - iMargin - iMargin;
	int iHeight  = cy - iMargin - iMargin;
	
	int iHeaderH = 40;
	int iList_H = iHeaderH + iIdxCnt * 16;

	if (iIdxCnt <= 0 || iList_H > iHeight)
	{
		iList_H = iHeight;
	}

	int iSTWidth = (iWidth - iSpacing - iSpacing )/ 6;
	int iEdWidth = iSTWidth * 2;
	int iSTHeight = 25;
	int	iListHeight = 200;

	// 이름
	iLeft = iMargin;
	m_st_Item[STI_SFR_PARAM].MoveWindow(iLeft, iTop, iWidth, iSTHeight);

// 	iLeft = cx - iMargin - iSTWidth;
// 	m_bn_Item[BTN_SFR_RESET].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);
// 
// 	iTop += iSTHeight + iSpacing;
// 	iLeft = cx - iMargin - iSTWidth;
// 	m_bn_Item[BTN_SFR_TEST].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;
	m_st_Item[STI_SFR_CAPTURE_CNT].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft += iSTWidth + 2;
	m_ed_Item[EDT_SFR_CAPTURE_CNT].MoveWindow(iLeft, iTop, iSTWidth - 2, iSTHeight);

	iLeft += iSTWidth + iSpacing;
	m_st_Item[STI_SFR_DELAY].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft += iSTWidth + 2;
	m_ed_Item[EDT_SFR_DELAY].MoveWindow(iLeft, iTop, iSTWidth - 2, iSTHeight);

	iLeft += iSTWidth + iSpacing;
	m_st_Item[STI_SFR_Result].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft += iSTWidth + 2;
	m_cb_Item[CMB_SFR_ResultMODE].MoveWindow(iLeft, iTop, iSTWidth - 2, iSTHeight);


	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;
	int iSTspecW = (iWidth - iMargin) / 2;
	m_st_Item[STI_SFR_SPEC].MoveWindow(iLeft, iTop, iSTspecW, iSTHeight);

	iLeft += iMargin + iSTspecW;
	m_st_Item[STI_SFR_SPEC_2].MoveWindow(iLeft, iTop, iSTspecW, iSTHeight);


	/*
	//Tilt (Focus만 하게끔 해야됨)
	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;
	m_st_Item[STI_SFR_X1_Tilt_A].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);
	iLeft += iSTWidth + 2;
	m_cb_Item[CMB_SFR_X1_Tilt_A].MoveWindow(iLeft, iTop, iSTWidth - 2, iSTHeight);

	iLeft += iSTWidth + iSpacing;
	m_st_Item[STI_SFR_X1_Tilt_B].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);
	iLeft += iSTWidth + 2;
	m_cb_Item[CMB_SFR_X1_Tilt_B].MoveWindow(iLeft, iTop, iSTWidth - 2, iSTHeight);

	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;
	m_st_Item[STI_SFR_X2_Tilt_A].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);
	iLeft += iSTWidth + 2;
	m_cb_Item[CMB_SFR_X2_Tilt_A].MoveWindow(iLeft, iTop, iSTWidth - 2, iSTHeight);

	iLeft += iSTWidth + iSpacing;
	m_st_Item[STI_SFR_X2_Tilt_B].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);
	iLeft += iSTWidth + 2;
	m_cb_Item[CMB_SFR_X2_Tilt_B].MoveWindow(iLeft, iTop, iSTWidth - 2, iSTHeight);

	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;
	m_st_Item[STI_SFR_Y1_Tilt_A].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);
	iLeft += iSTWidth + 2;
	m_cb_Item[CMB_SFR_Y1_Tilt_A].MoveWindow(iLeft, iTop, iSTWidth - 2, iSTHeight);

	iLeft += iSTWidth + iSpacing;
	m_st_Item[STI_SFR_Y1_Tilt_B].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);
	iLeft += iSTWidth + 2;
	m_cb_Item[CMB_SFR_Y1_Tilt_B].MoveWindow(iLeft, iTop, iSTWidth - 2, iSTHeight);

	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;
	m_st_Item[STI_SFR_Y2_Tilt_A].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);
	iLeft += iSTWidth + 2;
	m_cb_Item[CMB_SFR_Y2_Tilt_A].MoveWindow(iLeft, iTop, iSTWidth - 2, iSTHeight);

	iLeft += iSTWidth + iSpacing;
	m_st_Item[STI_SFR_Y2_Tilt_B].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);
	iLeft += iSTWidth + 2;
	m_cb_Item[CMB_SFR_Y2_Tilt_B].MoveWindow(iLeft, iTop, iSTWidth - 2, iSTHeight);



	//Group
	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;

	int iCtrlWidth = (iWidth - (iSpacing * 2)) / 3;	//170;
	int iLeft_2nd  = iLeft + iCtrlWidth + iSpacing;
	int iLeft_3rd  = iLeft_2nd + iCtrlWidth + iSpacing;
	int iEdWidth   = iCtrlWidth - iSpacing - iSTHeight;

	m_st_CapItem.MoveWindow(iLeft, iTop, iCtrlWidth, iSTHeight);
	m_st_CapSpecMin.MoveWindow(iLeft_2nd, iTop, iCtrlWidth, iSTHeight);
	m_st_CapSpecMax.MoveWindow(iLeft_3rd, iTop, iCtrlWidth, iSTHeight);

	for (UINT nIdx = 0; nIdx < ITM_SFR_Max; nIdx++)
	{
		iTop += iSTHeight + iSpacing;

		m_st_Spec[nIdx].MoveWindow(iLeft, iTop, iCtrlWidth, iSTHeight);

		m_chk_SpecMin[nIdx].MoveWindow(iLeft_2nd, iTop, iSTHeight, iSTHeight);
		m_ed_SpecMin[nIdx].MoveWindow(iLeft_2nd + iSTHeight + iSpacing, iTop, iEdWidth, iSTHeight);

		m_chk_SpecMax[nIdx].MoveWindow(iLeft_3rd, iTop, iSTHeight, iSTHeight);
		m_ed_SpecMax[nIdx].MoveWindow(iLeft_3rd + iSTHeight + iSpacing, iTop, iEdWidth, iSTHeight);
	}

	for (UINT nIdx = 0; nIdx < ITM_SFR_TiltMax; nIdx++)
	{
		iTop += iSTHeight + iSpacing;

		m_st_TiltSpec[nIdx].MoveWindow(iLeft, iTop, iCtrlWidth, iSTHeight);

		m_chk_TiltSpecMin[nIdx].MoveWindow(iLeft_2nd, iTop, iSTHeight, iSTHeight);
		m_ed_TiltSpecMin[nIdx].MoveWindow(iLeft_2nd + iSTHeight + iSpacing, iTop, iEdWidth, iSTHeight);

		m_chk_TiltSpecMax[nIdx].MoveWindow(iLeft_3rd, iTop, iSTHeight, iSTHeight);
		m_ed_TiltSpecMax[nIdx].MoveWindow(iLeft_3rd + iSTHeight + iSpacing, iTop, iEdWidth, iSTHeight);
	}
	*/

	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;
	m_List_Group.MoveWindow(iLeft, iTop, iSTWidth + iEdWidth, iListHeight);

	iLeft = iSTWidth + iEdWidth + iMargin + iMargin;
	m_List_Tilt.MoveWindow(iLeft, iTop, iSTWidth + iEdWidth, iListHeight);


	iLeft = iMargin;
	iTop += iListHeight + iSpacing;
	m_st_Item[STI_SFR_ROI].MoveWindow(iLeft, iTop, iWidth, iSTHeight);

	//List
	iLeft = iMargin;
	iTop += iSTHeight + iSpacing ;
	m_List.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop + iSpacing);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_SFR::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_Cfg_VIBase::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_Cfg_SFR::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd_Cfg_VIBase::OnShowWindow(bShow, nStatus);

	if (NULL != m_pstConfigInfo)
	{
		if (TRUE == bShow)
		{
			switch (m_nOvrNum)
			{
			case 0:
				GetOwner()->SendMessage(m_wm_ShowWindow, 0, Ovr_SFR);
				break;
			case 1:
				GetOwner()->SendMessage(m_wm_ShowWindow, 0, Ovr_SFR_2);
				break;
			case 2:
				GetOwner()->SendMessage(m_wm_ShowWindow, 0, Ovr_SFR_3);
				break;
			default:
				GetOwner()->SendMessage(m_wm_ShowWindow, 0, Ovr_SFR);
				break;
			}

			GetOwner()->SendNotifyMessage(m_wm_ChangeOption, 0, 0);
		}
	}
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_Cfg_SFR::OnRangeBtnCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_BTN_ITEM;

	switch (nIdex)
	{
	case BTN_SFR_RESET:
		GetOwner()->SendMessage(m_wm_ShowWindow, 0, -1);
		break;
	case BTN_SFR_TEST:
		GetOwner()->SendMessage(m_wm_ShowWindow, 0, VTID_SFR);
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: SetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/12 - 17:32
// Desc.		:
//=============================================================================
void CWnd_Cfg_SFR::SetUpdateData()
{
	if (NULL == m_pstConfigInfo)
		return;

	CString strValue;

	strValue.Format(_T("%d"), m_pstConfigInfo->nCaptureCnt);
	m_ed_Item[EDT_SFR_CAPTURE_CNT].SetWindowText(strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->nCaptureDelay);
	m_ed_Item[EDT_SFR_DELAY].SetWindowText(strValue);

	//for (UINT nSpec = 0; nSpec < ITM_SFR_Max; nSpec++)
	//{
	//	strValue.Format(_T("%.2f"), m_pstConfigInfo->stSpec_F_Min[nSpec].dbValue);
	//	m_ed_SpecMin[nSpec].SetWindowText(strValue);

	//	strValue.Format(_T("%.2f"), m_pstConfigInfo->stSpec_F_Max[nSpec].dbValue);
	//	m_ed_SpecMax[nSpec].SetWindowText(strValue);

	//	m_chk_SpecMin[nSpec].SetCheck(m_pstConfigInfo->stSpec_F_Min[nSpec].bEnable);
	//	m_chk_SpecMax[nSpec].SetCheck(m_pstConfigInfo->stSpec_F_Max[nSpec].bEnable);
	//}

	//for (UINT nSpec = 0; nSpec < ITM_SFR_TiltMax; nSpec++)
	//{
	//	strValue.Format(_T("%.2f"), m_pstConfigInfo->stSpec_Tilt_Min[nSpec].dbValue);
	//	m_ed_TiltSpecMin[nSpec].SetWindowText(strValue);

	//	strValue.Format(_T("%.2f"), m_pstConfigInfo->stSpec_Tilt_Max[nSpec].dbValue);
	//	m_ed_TiltSpecMax[nSpec].SetWindowText(strValue);

	//	m_chk_TiltSpecMin[nSpec].SetCheck(m_pstConfigInfo->stSpec_Tilt_Min[nSpec].bEnable);
	//	m_chk_TiltSpecMax[nSpec].SetCheck(m_pstConfigInfo->stSpec_Tilt_Max[nSpec].bEnable);
	//}

	m_List.SetPtr_ConfigInfo(m_pstConfigInfo);
	m_List.InsertFullData();

	m_List_Group.SetPtr_ConfigInfo(m_pstConfigInfo);
	m_List_Group.InsertFullData();

	m_List_Tilt.SetPtr_ConfigInfo(m_pstConfigInfo);
	m_List_Tilt.InsertFullData();

	m_cb_Item[CMB_SFR_ResultMODE].SetCurSel(m_pstConfigInfo->nResultmode);

	//m_cb_Item[CMB_SFR_X1_Tilt_A].SetCurSel(m_pstConfigInfo->nX1_Tilt_A);
	//m_cb_Item[CMB_SFR_X1_Tilt_B].SetCurSel(m_pstConfigInfo->nX1_Tilt_B);
	//m_cb_Item[CMB_SFR_X2_Tilt_A].SetCurSel(m_pstConfigInfo->nX2_Tilt_A);
	//m_cb_Item[CMB_SFR_X2_Tilt_B].SetCurSel(m_pstConfigInfo->nX2_Tilt_B);
	//m_cb_Item[CMB_SFR_Y1_Tilt_A].SetCurSel(m_pstConfigInfo->nY1_Tilt_A);
	//m_cb_Item[CMB_SFR_Y1_Tilt_B].SetCurSel(m_pstConfigInfo->nY1_Tilt_B);
	//m_cb_Item[CMB_SFR_Y2_Tilt_A].SetCurSel(m_pstConfigInfo->nY2_Tilt_A);
	//m_cb_Item[CMB_SFR_Y2_Tilt_B].SetCurSel(m_pstConfigInfo->nY2_Tilt_B);
}

//=============================================================================
// Method		: GetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/14 - 18:04
// Desc.		:
//=============================================================================
void CWnd_Cfg_SFR::GetUpdateData()
{
	if (NULL == m_pstConfigInfo)
		return;

	CString strValue;

	m_ed_Item[EDT_SFR_CAPTURE_CNT].GetWindowText(strValue);
	m_pstConfigInfo->nCaptureCnt = _ttoi(strValue);

	m_ed_Item[EDT_SFR_DELAY].GetWindowText(strValue);
	m_pstConfigInfo->nCaptureDelay = _ttoi(strValue);

	//for (UINT nSpec = 0; nSpec < ITM_SFR_Max; nSpec++)
	//{
	//	m_ed_SpecMin[nSpec].GetWindowText(strValue);
	//	m_pstConfigInfo->stSpec_F_Min[nSpec].dbValue = _ttof(strValue);

	//	m_ed_SpecMax[nSpec].GetWindowText(strValue);
	//	m_pstConfigInfo->stSpec_F_Max[nSpec].dbValue = _ttof(strValue);

	//	m_pstConfigInfo->stSpec_F_Min[nSpec].bEnable = m_chk_SpecMin[nSpec].GetCheck();
	//	m_pstConfigInfo->stSpec_F_Max[nSpec].bEnable = m_chk_SpecMax[nSpec].GetCheck();
	//}

	//for (UINT nSpec = 0; nSpec < ITM_SFR_TiltMax; nSpec++)
	//{
	//	m_ed_TiltSpecMin[nSpec].GetWindowText(strValue);
	//	m_pstConfigInfo->stSpec_Tilt_Min[nSpec].dbValue = _ttof(strValue);

	//	m_ed_TiltSpecMax[nSpec].GetWindowText(strValue);
	//	m_pstConfigInfo->stSpec_Tilt_Max[nSpec].dbValue = _ttof(strValue);

	//	m_pstConfigInfo->stSpec_Tilt_Min[nSpec].bEnable = m_chk_TiltSpecMin[nSpec].GetCheck();
	//	m_pstConfigInfo->stSpec_Tilt_Max[nSpec].bEnable = m_chk_TiltSpecMax[nSpec].GetCheck();
	//}

	m_pstConfigInfo->nResultmode = m_cb_Item[CMB_SFR_ResultMODE].GetCurSel();

	//m_pstConfigInfo->nX1_Tilt_A = m_cb_Item[CMB_SFR_X1_Tilt_A].GetCurSel();
	//m_pstConfigInfo->nX1_Tilt_B = m_cb_Item[CMB_SFR_X1_Tilt_B].GetCurSel();
	//m_pstConfigInfo->nX2_Tilt_A = m_cb_Item[CMB_SFR_X2_Tilt_A].GetCurSel();
	//m_pstConfigInfo->nX2_Tilt_B = m_cb_Item[CMB_SFR_X2_Tilt_B].GetCurSel();
	//m_pstConfigInfo->nY1_Tilt_A = m_cb_Item[CMB_SFR_Y1_Tilt_A].GetCurSel();
	//m_pstConfigInfo->nY1_Tilt_B = m_cb_Item[CMB_SFR_Y1_Tilt_B].GetCurSel();
	//m_pstConfigInfo->nY2_Tilt_A = m_cb_Item[CMB_SFR_Y2_Tilt_A].GetCurSel();
	//m_pstConfigInfo->nY2_Tilt_B = m_cb_Item[CMB_SFR_Y2_Tilt_B].GetCurSel();


	return;
}
