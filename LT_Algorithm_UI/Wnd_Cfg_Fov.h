﻿#ifndef Wnd_Cfg_Fov_h__
#define Wnd_Cfg_Fov_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_T_Fov.h"
#include "Def_TestItem_VI.h"
#include "List_FovOp.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_Fov
//-----------------------------------------------------------------------------
class CWnd_Cfg_Fov : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_Fov)

public:
	CWnd_Cfg_Fov();
	virtual ~CWnd_Cfg_Fov();

	enum enFovStatic
	{
		STI_FOV_PARAM = 0,
		STI_FOV_SPEC,
		STI_FOV_OFFSET_HOR,
		STI_FOV_OFFSET_VER,
		STI_FOV_MAX,
	};

	enum enFovButton
	{
		BTN_FOV_RESET = 0,
		BTN_FOV_TEST,
		BTN_FOV_MAX,
	};

	enum enFovComobox
	{
		CMB_FOV_MAX = 1,
	};

	enum enFovEdit
	{
		EDT_FOV_OFFSET_HOR = 0,
		EDT_FOV_OFFSET_VER,
		EDT_FOV_MAX,
	};

protected:

	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	CFont					m_font;

	CList_FovOp				m_List;

	CVGStatic				m_st_Item[STI_FOV_MAX];
	CButton					m_bn_Item[BTN_FOV_MAX];
	CComboBox				m_cb_Item[CMB_FOV_MAX];
	CMFCMaskedEdit			m_ed_Item[EDT_FOV_MAX];

	CVGStatic				m_st_CapItem;
	CVGStatic				m_st_CapSpecMin;
	CVGStatic				m_st_CapSpecMax;

	CVGStatic				m_st_Spec[Spec_Fov_MAX];
	CMFCMaskedEdit			m_ed_SpecMin[Spec_Fov_MAX];
	CMFCMaskedEdit			m_ed_SpecMax[Spec_Fov_MAX];
	CMFCButton				m_chk_SpecMin[Spec_Fov_MAX];
	CMFCButton				m_chk_SpecMax[Spec_Fov_MAX];

	ST_FOV_Opt*				m_pstConfigInfo = NULL;

public:

	void	SetPtr_RecipeInfo(ST_FOV_Opt* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstConfigInfo = pstRecipeInfo;
	};

	void	SetUpdateData		();
	void	GetUpdateData		();
};

#endif // Wnd_Cfg_Fov_h__
