// Wnd_3DDepthData.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_3DDepthData.h"


// CWnd_3DDepthData

IMPLEMENT_DYNAMIC(CWnd_3DDepthData, CWnd)

CWnd_3DDepthData::CWnd_3DDepthData()
{

}

CWnd_3DDepthData::~CWnd_3DDepthData()
{
}


BEGIN_MESSAGE_MAP(CWnd_3DDepthData, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

// CWnd_3DDepthData 메시지 처리기입니다.

int CWnd_3DDepthData::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	DWORD dwStyle = WS_VISIBLE | WS_CHILD /*| WS_CLIPCHILDREN*/ | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.

 	for (int t = 0; t < (int)m_nHeaderCnt; t++)
 	{
 		m_st_Header[t].SetStaticStyle(CVGStatic::StaticStyle_Title);
 		m_st_Header[t].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Header[t].SetFont_Gdip(L"Arial", 12.0F);
		m_st_Header[t].Create(g_sz3DData[t], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
 
 		for (int i= 0;i<en3DData_inputmax; i++)
 		{
 			m_st_Data[i][t].SetStaticStyle(CVGStatic::StaticStyle_Title);
 			m_st_Data[i][t].SetColorStyle(CVGStatic::ColorStyle_Default);
			m_st_Data[i][t].SetFont_Gdip(L"Arial", 12.0F);
 			m_st_Data[i][t].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
 		}
 	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2018/3/4 - 8:27
// Desc.		:
//=============================================================================
void CWnd_3DDepthData::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iMargin = 0;
	int iSpacing = 0;
	int iCateSpacing = 0;

	int iLeft = iMargin;
	int iTop = iMargin;
	int iWidth = cx - iMargin - iMargin;
	int iHeight = cy - iMargin - iMargin;

	int HeaderW = (iWidth+2) / 2;

	int stW = iWidth;
	int stH = (672 * iWidth) / 1280;

	int HeaderH = iHeight / (10);

	for (UINT nIdx = 0; nIdx < m_nHeaderCnt; nIdx++)
	{
		iLeft = iMargin;
		m_st_Header[nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);
		iLeft += HeaderW - 1;
		m_st_Data[0][nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);
		iTop += (HeaderH - 1);
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2018/3/4 - 8:27
// Desc.		:
//=============================================================================
BOOL CWnd_3DDepthData::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnEraseBkgnd
// Access		: public  
// Returns		: BOOL
// Parameter	: CDC * pDC
// Qualifier	:
// Last Update	: 2018/3/4 - 8:27
// Desc.		:
//=============================================================================
BOOL CWnd_3DDepthData::OnEraseBkgnd(CDC* pDC)
{
	return CWnd::OnEraseBkgnd(pDC);
}

//=============================================================================
// Method		: DataSetting
// Access		: public  
// Returns		: void
// Parameter	: UINT nDataIdx
// Parameter	: UINT nIdx
// Parameter	: BOOL MODE
// Parameter	: double dData
// Qualifier	:
// Last Update	: 2018/3/4 - 8:27
// Desc.		:
//=============================================================================
void CWnd_3DDepthData::DataSetting(UINT nDataIdx, UINT nIdx, BOOL MODE, double dData)
{
	if (MODE == TRUE)
	{
		m_st_Data[nDataIdx][nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
	}
	else
	{
		m_st_Data[nDataIdx][nIdx].SetColorStyle(CVGStatic::ColorStyle_Red);
	}

	CString strData;

	strData.Format(_T("%.2f"), dData);

	m_st_Data[nDataIdx][nIdx].SetWindowText(strData);
}

//=============================================================================
// Method		: DataEachReset
// Access		: public  
// Returns		: void
// Parameter	: UINT nDataIdx
// Parameter	: UINT nIdx
// Qualifier	:
// Last Update	: 2018/3/4 - 8:27
// Desc.		:
//=============================================================================
void CWnd_3DDepthData::DataEachReset(UINT nDataIdx, UINT nIdx)
{
	m_st_Data[nDataIdx][nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_Data[nDataIdx][nIdx].SetWindowText(_T(""));
}

//=============================================================================
// Method		: DataReset
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/4 - 8:27
// Desc.		:
//=============================================================================
void CWnd_3DDepthData::DataReset()
{
	for (UINT nIdx = 0; nIdx < m_nHeaderCnt; nIdx++)
	{
		m_st_Data[0][nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Data[0][nIdx].SetWindowText(_T(""));
	}
}

//=============================================================================
// Method		: DataDisplay
// Access		: public  
// Returns		: void
// Parameter	: ST_Particle_Data stData
// Qualifier	:
// Last Update	: 2018/3/4 - 8:27
// Desc.		:
//=============================================================================
void CWnd_3DDepthData::DataDisplay(ST_3D_Depth_Data stData)
{
	CString strData;

	if (stData.nEachResult[Spec_BrightnessAvg] == TRUE)
	{
		m_st_Data[en3DData_1st][en3DData_BrightnessAvg].SetColorStyle(CVGStatic::ColorStyle_Default);
	}
	else
	{
		m_st_Data[en3DData_1st][en3DData_BrightnessAvg].SetColorStyle(CVGStatic::ColorStyle_Red);
	}

	strData.Format(_T("%.2f"), stData.dbValue[Spec_BrightnessAvg]);
	m_st_Data[en3DData_1st][en3DData_BrightnessAvg].SetWindowText(strData);

	if (stData.nEachResult[Spec_Standard_deviation] == TRUE)
	{
		m_st_Data[en3DData_1st][en3DData_Standard_deviation].SetColorStyle(CVGStatic::ColorStyle_Default);
	}
	else
	{
		m_st_Data[en3DData_1st][en3DData_Standard_deviation].SetColorStyle(CVGStatic::ColorStyle_Red);
	}

	strData.Format(_T("%.2f"), stData.dbValue[Spec_Standard_deviation]);
	m_st_Data[en3DData_1st][en3DData_Standard_deviation].SetWindowText(strData);

}
