#ifndef Wnd_DynamicRangeData_h__
#define Wnd_DynamicRangeData_h__

#pragma once

#include "afxwin.h"

#include "VGStatic.h"
#include "Def_T_Dynamic.h"
// CWnd_DynamicRangeData
typedef enum
{
	enDRData_Header = 0,
	enDRData_MAX=5,
}enumHeaderDRData;

typedef enum
{
	enDRData_Dynamic = 0,
	enDRData_SNR,
	enDRData_inputmax,
}enumDataDRData;

static	LPCTSTR	g_szDRData[] =
{
	_T("Dynamic Range"),
	_T("SNR BW"),
	NULL
};
class CWnd_DynamicRangeData : public CWnd
{
	DECLARE_DYNAMIC(CWnd_DynamicRangeData)

public:
	CWnd_DynamicRangeData();
	virtual ~CWnd_DynamicRangeData();

	UINT m_nHeaderCnt = enDRData_MAX;
	CString m_SzHeaderName[enDRData_MAX];

	//---Class 정의 시 세팅 해야함.
	void SetHeader(UINT nHeaderCnt){

		if (nHeaderCnt > enDRData_MAX)
		{
			nHeaderCnt = enDRData_MAX;
		}
		m_nHeaderCnt = nHeaderCnt;
	};

	void SetHeaderName(UINT nCnt, CString szHeader){

		if (nCnt > enDRData_MAX)
		{
			nCnt = enDRData_MAX-1;
		}
		m_SzHeaderName[nCnt] = szHeader;
	};

	CVGStatic		m_st_Header[enDRData_MAX];
	CVGStatic		m_st_Data[enDRData_inputmax][enDRData_MAX];

//	CVGStatic		m_st_ResultView;


protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	void DataSetting(UINT nDataIdx, UINT nIdx, BOOL MODE, double dData);
	void DataEachReset(UINT nDataIdx, UINT nIdx);
	void DataReset();
	void DataDisplay(ST_Dynamic_Data stData);
};


#endif // Wnd_DynamicRangeData_h__
