//*****************************************************************************
// Filename	: 	Def_T_Common.h
// Created	:	2018/1/27 - 16:47
// Modified	:	2018/1/27 - 16:47
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#ifndef Def_T_Common_h__
#define Def_T_Common_h__

#include <afxwin.h>

typedef enum enItemPos
{
	IPos_Left = 0,
	IPos_Right,
	IPos_Top,
	IPos_Bottom,
	IPos_Max,
};

static LPCTSTR	g_szItemPos[] =
{
	_T("��"),
	_T("��"),
	_T("��"),
	_T("��"),
	NULL
};

static LPCTSTR	g_szSpecUse_Static[] =
{
	_T("��"),
	_T("��"),
	NULL
};


typedef enum enMarkType
{
	MTyp_Circle = 0,
	MTyp_FiducialMark,
	MTyp_Max,
};

static LPCTSTR	g_szMarkType[] =
{
	_T("��"),
	_T("��"),
	NULL
};

typedef enum enMarkColor
{
	MCol_White = 0,
	MCol_Black,
	MCol_Max,
};

static LPCTSTR	g_szMarkColor[] =
{
	_T("White"),
	_T("Black"),
	NULL
};

typedef enum enLineType
{
	LnTy_LR_WB = 0,
	LnTy_LR_BW,
	LnTy_TB_WB,
	LnTy_TB_BW,
};

static LPCTSTR	g_szLineType[] =
{
	_T("����"),
	_T("����"),
	_T("���"),
	_T("���"),
	NULL
};

static LPCTSTR	g_szSFRGroup[] =
{
	_T("CT"),
	_T("LT_0.5"),
	_T("LB_0.5"),
	_T("RT_0.5"),
	_T("RB_0.5"),
	_T("LT_0.7"),
	_T("LB_0.7"),
	_T("RT_0.7"),
	_T("RB_0.7"),
	NULL
};

typedef enum enParticleForm
{
	Par_Form_Rectangle = 0,
	Par_Form_Clrcle,
	Par_Form_Max,
};

typedef struct _tag_Comm_Spec
{
	BOOL	bEnable;

	int		iValue;
	double	dbValue;

	void Reset()
	{
		bEnable = TRUE;
		iValue	= 0;
		dbValue = 0.0;
	};

	_tag_Comm_Spec()
	{
		Reset();
	};

	_tag_Comm_Spec& operator= (_tag_Comm_Spec& ref)
	{
		bEnable	= ref.bEnable;

		iValue	= ref.iValue;
		dbValue = ref.dbValue;

		return *this;
	};

}ST_Comm_Spec, *PST_Comm_Spec;

// ROI
typedef struct _tag_RegionROI
{
	CRect	rtROI;

	UINT	nMarkType;
	UINT	nMarkColor;
	UINT	nBrightness;
	UINT	nItemPos;

	double	dbOffset;
	double	dbOffsetSub;
	double	dbLinePair;

	BOOL	bEnable;
	BOOL	bEllipse;

	double	dbBruiseConc;
	double	dbBruiseSize;

	UINT nSharpness;
	UINT nSFR_Group;

	_tag_RegionROI()
	{
		Reset();
	};

	void Reset()
	{
		nItemPos		= IPos_Left;
		nMarkType		= MTyp_Circle;
		nMarkColor		= MCol_Black;
		nBrightness		= 0;

		dbOffsetSub		= 0.0;
		dbOffset		= 0.0;
		dbLinePair		= 50.0;
		bEnable			= FALSE;
		
		bEllipse		= Par_Form_Rectangle;
		dbBruiseConc	= 0.0;
		dbBruiseSize	= 0.0;
		nSharpness		= 100;
		nSFR_Group			= 0;
		rtROI.SetRectEmpty();
	};

	void RectPosXY(int iPosX, int iPosY)
	{
		CRect rtTemp = rtROI;

		rtROI.left	 = iPosX - (int)(rtTemp.Width() / 2);
		rtROI.right  = rtROI.left + rtTemp.Width();
		rtROI.top	 = iPosY - (int)(rtTemp.Height() / 2);
		rtROI.bottom = rtROI.top + rtTemp.Height();
	};

	void RectPosWH(int iWidth, int iHeight)
	{
		CRect rtTemp = rtROI;

		rtROI.left   = rtTemp.CenterPoint().x - (iWidth / 2);
		rtROI.right  = rtTemp.CenterPoint().x + (iWidth / 2) + iWidth % 2;
		rtROI.top	 = rtTemp.CenterPoint().y - (iHeight / 2);
		rtROI.bottom = rtTemp.CenterPoint().y + (iHeight / 2) + iHeight % 2;
	};

	_tag_RegionROI& operator= (_tag_RegionROI& ref)
	{
		nItemPos		= ref.nItemPos;
		rtROI			= ref.rtROI;
		nMarkType		= ref.nMarkType;
		nMarkColor		= ref.nMarkColor;
		nBrightness		= ref.nBrightness;

		dbOffsetSub		= ref.dbOffsetSub;
		dbOffset		= ref.dbOffset;
		dbLinePair		= ref.dbLinePair;
		bEnable			= ref.bEnable;

		bEllipse		= ref.bEllipse;
		dbBruiseConc	= ref.dbBruiseConc;
		dbBruiseSize	= ref.dbBruiseSize;
		nSharpness		= ref.nSharpness;
		nSFR_Group = ref.nSFR_Group;

		return *this;
	};

}ST_RegionROI, *PST_RegionROI;

typedef struct _tag_ResultRegion
{
	CRect	RegionList;

	double	dbresult;
	double	dbStainBlemish;

	_tag_ResultRegion()
	{
		Reset();
	};

	void Reset()
	{
		dbresult = 0.0;
		dbStainBlemish = 0.0;
		RegionList.SetRectEmpty();
	};

	void RectPosXY(int iPosX, int iPosY)
	{
		CRect rtTemp = RegionList;

		RegionList.left = iPosX - (int)(rtTemp.Width() / 2);
		RegionList.right = RegionList.left + rtTemp.Width();
		RegionList.top = iPosY - (int)(rtTemp.Height() / 2);
		RegionList.bottom = RegionList.top + rtTemp.Height();
	};

	void RectPosWH(int iWidth, int iHeight)
	{
		CRect rtTemp = RegionList;

		RegionList.left = rtTemp.CenterPoint().x - (iWidth / 2);
		RegionList.right = rtTemp.CenterPoint().x + (iWidth / 2) + iWidth % 2;
		RegionList.top = rtTemp.CenterPoint().y - (iHeight / 2);
		RegionList.bottom = rtTemp.CenterPoint().y + (iHeight / 2) + iHeight % 2;
	};

	_tag_ResultRegion& operator= (_tag_ResultRegion& ref)
	{
		dbresult = ref.dbresult;
		dbStainBlemish = ref.dbStainBlemish;
		RegionList = ref.RegionList;

		return *this;
	};

}ST_ResultRegionROI, *PST_ResultRegionROI;
#endif // Def_T_Common_h__
