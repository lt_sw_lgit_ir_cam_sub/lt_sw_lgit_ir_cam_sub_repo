﻿#ifndef Wnd_Cfg_T3D_Depth_h__
#define Wnd_Cfg_T3D_Depth_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_T_3D_Depth.h"
#include "Def_TestItem_VI.h"
#include "List_3D_DepthOp.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_T3D_Depth
//-----------------------------------------------------------------------------
class CWnd_Cfg_T3D_Depth : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_T3D_Depth)

public:
	CWnd_Cfg_T3D_Depth();
	virtual ~CWnd_Cfg_T3D_Depth();

	enum enT3D_Depth_Static
	{
		STI_3D_PARAM = 0,
		STI_3D_SPEC,
		STI_3D_MAX,
	};

	enum enT3D_Depth_Button
	{
		BTN_3D_RESET = 0,
		BTN_3D_TEST,
		BTN_3D_MAX,
	};

	enum enT3D_Depth_Comobox
	{
		CMB_3D_MAX = 1,
	};

	enum enT3D_Depth_Edit
	{
		EDT_3D_MAX = 1,
	};

protected:
	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	CFont						m_font;

	CList_3D_DepthOp		m_List;

	CVGStatic					m_st_Item[STI_3D_MAX];
	CButton						m_bn_Item[BTN_3D_MAX];
	CComboBox					m_cb_Item[CMB_3D_MAX];
	CMFCMaskedEdit				m_ed_Item[EDT_3D_MAX];

	CVGStatic					m_st_CapItem;
	CVGStatic					m_st_CapSpecMin;
	CVGStatic					m_st_CapSpecMax;

	CVGStatic					m_st_Spec[Spec_3D_Depth_Max];
	CMFCMaskedEdit				m_ed_SpecMin[Spec_3D_Depth_Max];
	CMFCMaskedEdit				m_ed_SpecMax[Spec_3D_Depth_Max];
	CMFCButton					m_chk_SpecMin[Spec_3D_Depth_Max];
	CMFCButton					m_chk_SpecMax[Spec_3D_Depth_Max];

	ST_3D_Depth_Opt*		m_pstConfigInfo		= NULL;

public:

	void	SetPtr_RecipeInfo (ST_3D_Depth_Opt* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstConfigInfo = pstRecipeInfo;
	};

	void	SetUpdateData		();
	void	GetUpdateData		();
};

#endif // Wnd_Cfg_T3D_Depth_h__
