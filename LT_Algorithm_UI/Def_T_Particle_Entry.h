﻿#ifndef Def_Particle_Entry_T_h__
#define Def_Particle_Entry_T_h__

#include <afxwin.h>

#include "Def_T_Common.h"

#pragma pack(push,1)

typedef enum enPaticleEntryRegion
{
	Particle_Entry_Region_Center = 0,
	Particle_Entry_Region_Middle,
	Particle_Entry_Region_Side,
	Particle_Entry_Region_MaxEnum,
};

static LPCTSTR g_szPaticleEntryRegion[] =
{
	_T("Center"),
	_T("Middle"),
	_T("Side"),
	NULL
};

typedef enum enParticle_Entry_Spec
{
	Spec_Particle_Entry_Count = 0,
	Spec_Particle_Entry_MAX,
};

static LPCTSTR	g_szParticle_Entry_Spec[] =
{
	_T("Count"),
	NULL
};



typedef struct _tag_Particlee_Entry_Opt
{
	// 노이즈 민감도
	double dbDustDis;

	// 불량 영역 체크
	BOOL	bFailCheck;

	ST_RegionROI stRegion[Particle_Entry_Region_MaxEnum];

	ST_Comm_Spec stSpec_Min;
	ST_Comm_Spec stSpec_Max;

	_tag_Particlee_Entry_Opt()
	{
		dbDustDis = 2;
		bFailCheck = FALSE;

		stSpec_Min.Reset();
		stSpec_Max.Reset();

		for (UINT nIdx = 0; nIdx < Particle_Entry_Region_MaxEnum; nIdx++)
		{
			stRegion[nIdx].Reset();
		}
	};

	/*변수 교환 함수*/
	_tag_Particlee_Entry_Opt& operator= (_tag_Particlee_Entry_Opt& ref)
	{
		dbDustDis = ref.dbDustDis;
		bFailCheck = ref.bFailCheck;
	
		stSpec_Min = ref.stSpec_Min;
		stSpec_Max = ref.stSpec_Max;

		for (UINT nIdx = 0; nIdx < Particle_Entry_Region_MaxEnum; nIdx++)
		{
			stRegion[nIdx] = ref.stRegion[nIdx];
		}

		return *this;
	};

}ST_Particle_Entry_Opt, *PST_Particle_Entry_Op;

typedef struct _tag_Particle_Entry_Data
{
	UINT	nResult;

	// 검출된 이물 갯수
	UINT	nFailCount;
	ST_ResultRegionROI ErrRegionList[UCHAR_MAX];

	/*변수 초기화 함수*/
	_tag_Particle_Entry_Data()
	{
		Reset();
	};

	void Reset()
	{
		nResult		= 1;
		nFailCount	= 0;

		for (UINT nIdx = 0; nIdx < UCHAR_MAX; nIdx++)
			ErrRegionList[nIdx].Reset();
	};
	void FailData(){
		nResult = 0;
		nFailCount = 0;

		for (UINT nIdx = 0; nIdx < UCHAR_MAX; nIdx++)
			ErrRegionList[nIdx].Reset();
	};
	/*변수 교환 함수*/
	_tag_Particle_Entry_Data& operator= (_tag_Particle_Entry_Data& ref)
	{
		nResult		= ref.nResult;
		nFailCount	= ref.nFailCount;

		for (UINT nIdx = 0; nIdx < UCHAR_MAX; nIdx++)
			ErrRegionList[nIdx] = ref.ErrRegionList[nIdx];

		return *this;
	};

}ST_Particle_Entry_Data, *PST_Particle_Entry_Data;

typedef struct _tag_TI_Particle_Entry
{
	ST_Particle_Entry_Opt		stParticleOpt;		// 검사 기준 데이터
	ST_Particle_Entry_Data	stParticleData;		// 측정 데이터

	_tag_TI_Particle_Entry()
	{
	};

	void Reset()
	{
		stParticleData.Reset();
	};

	_tag_TI_Particle_Entry& operator= (_tag_TI_Particle_Entry& ref)
	{
		stParticleOpt	= ref.stParticleOpt;
		stParticleData	= ref.stParticleData;

		return *this;
	};

}ST_TI_Particle_Entry, *PST_TI_Particle_Entry;

#pragma pack(pop)

#endif // Def_Particle_Entry_T_h__

