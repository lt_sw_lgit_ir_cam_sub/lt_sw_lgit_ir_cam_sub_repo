﻿#ifndef Def_T_Shading_h__
#define Def_T_Shading_h__

#include <afxwin.h>

#include "Def_T_Common.h"

#pragma pack(push,1)

#define Index_Shading_Max 9

// 추후 정리
static const UINT nIndexVer_CT[Index_Shading_Max] = {  1,  4,  7, 10, 13, 16, 19, 22, 25 };
static const UINT nIndexVer_CB[Index_Shading_Max] = { 71, 68, 65, 62, 59, 56, 53, 50, 47 };
static const UINT nIndexHov_LC[Index_Shading_Max] = { 27, 28, 29, 30, 31, 32, 33, 34, 35 };
static const UINT nIndexHov_RC[Index_Shading_Max] = { 45, 44, 43, 42, 41, 40, 39, 38, 37 };
static const UINT nIndexDig_LT[Index_Shading_Max] = {  0,  3,  6,  9, 12, 15, 18, 21, 24 };
static const UINT nIndexDig_LB[Index_Shading_Max] = { 70, 67, 64, 61, 58, 55, 52, 49, 46 };
static const UINT nIndexDig_RT[Index_Shading_Max] = {  2,  5,  8, 11, 14, 17, 20, 23, 26 };
static const UINT nIndexDig_RB[Index_Shading_Max] = { 72, 69, 66, 63, 60, 57, 54, 51, 48 };

static int g_nShading[] =
{
	22, 31, 40, 44, 52, 60, 66, 73, 80, 88,
	94, 100, 110, 115, 120, 132, 136, 140, 154, 157,
	160, 176, 178, 180, 198, 199, 200, 211, 212, 213,
	214, 215, 216, 217, 218, 219, 220, 221, 222, 223,
	224, 225, 226, 227, 228, 229, 240, 241, 242, 260,
	262, 264, 280, 283, 286, 300, 304, 308, 320, 325,
	330, 340, 346, 352, 360, 367, 374, 380, 388, 396,
	400, 409, 418
};


typedef enum enIDX_Shading
{
	IDX_Shading_Ver_CT,
	IDX_Shading_Ver_CB,
	IDX_Shading_Hov_LC,
	IDX_Shading_Hov_RC,
	IDX_Shading_Dig_LT,
	IDX_Shading_Dig_LB,
	IDX_Shading_Dig_RT,
	IDX_Shading_Dig_RB,
	IDX_Shading_Max,
};

typedef enum  enShading_Type
{
	Type_Shading_NotUes,
	Type_Shading_Test,
	Type_Shading_Standard,
	Type_Shading_Max,
};

typedef enum enShading_Spec
{
	Spec_Shading = 0,
	Spec_Shading_MAX,
};

static LPCTSTR	g_szShading_Spec[] =
{
	_T("Center Brightness"),
	NULL
};

typedef enum enROI_Shading
{
	ROI_Shading_FST  = 0,
	ROI_Shading_CNT  = 36,
	ROI_Shading_LST  = 72,
	ROI_Shading_Max,
};


typedef struct _tag_ShadingRect
{
	CRect *rt_FullImageRect;
	CRect *rt_TestSectionFullRect;

	int nFullSectionCnt_X;
	int nFullSectionCnt_Y;
	int nTestSectionCnt;

	_tag_ShadingRect()
	{
		rt_FullImageRect = NULL;
		rt_TestSectionFullRect = NULL;
		nFullSectionCnt_X = 0;
		nFullSectionCnt_Y = 0;
		nTestSectionCnt = 0;
	};

	void FullRectCreate(unsigned int nSectionCnt_X, IN unsigned int nSectionCnt_Y){

		if (rt_FullImageRect == NULL)
		{
			rt_FullImageRect = new CRect[nSectionCnt_X* nSectionCnt_Y];
			nFullSectionCnt_X = nSectionCnt_X;
			nFullSectionCnt_Y = nSectionCnt_Y;
		}

	};

	//이미지를 21*21로 나누는 작업
	void FullRectMakeSection(IN unsigned int dwWidth, IN unsigned int dwHeight)
	{
		// 섹션 사이즈
		unsigned int nSectionSizeX = dwWidth / (nFullSectionCnt_X - 1);
		unsigned int nSectionSizeY = dwHeight / (nFullSectionCnt_Y - 1);

		// 위치값을 누적하여 계산
		int nPosX = 0, nPosY = 0;
		int nIndex = 0;

		for (int iIdx_Y = 0; iIdx_Y < nFullSectionCnt_Y; iIdx_Y++)
		{
			nPosX = 0;

			// 좌표 : 가로 줄
			if (iIdx_Y == 0)
				nPosY = 0;
			else if (iIdx_Y != 1)
				nPosY += nSectionSizeY;
			else
				nPosY += nSectionSizeY / 2;

			for (int iIdx_X = 0; iIdx_X < nFullSectionCnt_X; iIdx_X++)
			{
				nIndex = iIdx_Y * nFullSectionCnt_X + iIdx_X;

				// 좌표 : 세로 줄
				if (iIdx_X == 0)
					nPosX = 0;
				else if (iIdx_X != 1)
					nPosX += nSectionSizeX;
				else
					nPosX += nSectionSizeX / 2;



				rt_FullImageRect[nIndex].top = nPosY;
				rt_FullImageRect[nIndex].left = nPosX;


				int nW = 0, nH = 0;
				// 사이즈 : 세로 줄
				if (iIdx_X == 0 || iIdx_X == nFullSectionCnt_X - 1)
					nW = nSectionSizeX / 2;
				else
					nW = nSectionSizeX;
				rt_FullImageRect[nIndex].right = rt_FullImageRect[nIndex].left + nW;

				// 사이즈 : 가로 줄
				if (iIdx_Y == 0 || iIdx_Y == nFullSectionCnt_Y - 1)
					nH = nSectionSizeY / 2;
				else
					nH = nSectionSizeY;

				rt_FullImageRect[nIndex].bottom = rt_FullImageRect[nIndex].top + nH;
			}
		}
	}

	void TestRectCreate(){

		if (rt_TestSectionFullRect == NULL)
		{
			rt_TestSectionFullRect = new CRect[ROI_Shading_Max];
			nTestSectionCnt = ROI_Shading_Max;
		}
	};

	void Release(){
		if (rt_FullImageRect != NULL){
			delete[] rt_FullImageRect;
			rt_FullImageRect = NULL;
		}
		if (rt_TestSectionFullRect != NULL){
			delete[]rt_TestSectionFullRect;
			rt_TestSectionFullRect = NULL;
		}
	};

	//이미지를 21*21 중 Test 위치 선정
	void TestRectMakeSection()
	{
		int iCount = 0;
		for (int nIdx = 0; nIdx < nTestSectionCnt; nIdx++)
		{
			rt_TestSectionFullRect[iCount] = rt_FullImageRect[g_nShading[nIdx]];
			iCount++;
		}
	}

	_tag_ShadingRect& operator= (_tag_ShadingRect& ref)
	{
		rt_FullImageRect = ref.rt_FullImageRect;
		rt_TestSectionFullRect = ref.rt_TestSectionFullRect;
		nFullSectionCnt_X = ref.nFullSectionCnt_X;
		nFullSectionCnt_Y = ref.nFullSectionCnt_Y;
		nTestSectionCnt = ref.nTestSectionCnt;
		return *this;
	};

}ST_ShadingRect, *PST_ShadingRect;


typedef struct _tag_Shading_Opt
{
	bool bUseIndex[ROI_Shading_Max];
	UINT nType[ROI_Shading_Max];

	ST_Comm_Spec stSpec_Min[ROI_Shading_Max];
	ST_Comm_Spec stSpec_Max[ROI_Shading_Max];

	ST_ShadingRect stShading_Rect;

	_tag_Shading_Opt()
	{
		for (UINT nIdx = 0; nIdx < ROI_Shading_Max; nIdx++)
		{
			bUseIndex[nIdx] = true;
			nType[nIdx]		= Type_Shading_Test;
			stSpec_Min[nIdx].Reset();
			stSpec_Max[nIdx].Reset();
		}
	};

	_tag_Shading_Opt& operator= (_tag_Shading_Opt& ref)
	{
		for (UINT nIdx = 0; nIdx < ROI_Shading_Max; nIdx++)
		{
			bUseIndex[nIdx]  = ref.bUseIndex[nIdx];
			nType[nIdx]		 = ref.nType[nIdx];
			stSpec_Min[nIdx] = ref.stSpec_Min[nIdx];
			stSpec_Max[nIdx] = ref.stSpec_Max[nIdx];
		}
		stShading_Rect = ref.stShading_Rect;
		return *this;
	};

}ST_Shading_Opt, *PST_Shading_Opt;

typedef struct _tag_Shading_Data
{
	// 결과
	UINT	nResult;

	BOOL    bUse[ROI_Shading_Max];
	UINT    nEachResult[ROI_Shading_Max];
	
	short	sSignal[ROI_Shading_Max];
	float	fNoise[ROI_Shading_Max];

	float	fPercent[ROI_Shading_Max];

	_tag_Shading_Data()
	{
		nResult		= 1;

		for (UINT nIdx = 0; nIdx < ROI_Shading_Max; nIdx++)
		{
			nEachResult[nIdx]	 = 1;
			sSignal[nIdx]		 = 0;
			fNoise[nIdx]		 = 0.0;
			fPercent[nIdx]		 = 0.0;
			bUse[nIdx] = 0;
		}
	};

	void Reset()
	{
			nResult		= 1;

		for (UINT nIdx = 0; nIdx < ROI_Shading_Max; nIdx++)
		{
			nEachResult[nIdx]	 = 1;
			sSignal[nIdx]		 = 0;
			fNoise[nIdx]		 = 0.0;
			fPercent[nIdx]		 = 0.0;
			bUse[nIdx] = 0;
		}
	};
	void FailData(){
		nResult = 0;

		for (UINT nIdx = 0; nIdx < ROI_Shading_Max; nIdx++)
		{
			nEachResult[nIdx] = 0;
			sSignal[nIdx] = 0;
			fNoise[nIdx] = 0.0;
			fPercent[nIdx] = 0.0;
			bUse[nIdx] = 0;
		}
	};
	_tag_Shading_Data& operator= (_tag_Shading_Data& ref)
	{
		nResult = 1;

		for (UINT nIdx = 0; nIdx < ROI_Shading_Max; nIdx++)
		{
			nEachResult[nIdx]	= ref.nEachResult[nIdx];
			sSignal[nIdx]		= ref.sSignal[nIdx];
			fNoise[nIdx]		= ref.fNoise[nIdx];
			fPercent[nIdx]		= ref.fPercent[nIdx];
			bUse[nIdx] = ref.bUse[nIdx];
		}

		return *this;
	};

}ST_Shading_Data, *PST_Shading_Data;

typedef struct _tag_TI_Shading
{
	ST_Shading_Opt	stShadingOpt;		// 검사 기준 데이터
	ST_Shading_Data	stShadingData;	// 측정 데이터

	_tag_TI_Shading()
	{
	};

	void Reset()
	{
		stShadingData.Reset();
	};

	_tag_TI_Shading& operator= (_tag_TI_Shading& ref)
	{
		stShadingOpt	= ref.stShadingOpt;
		stShadingData = ref.stShadingData;

		return *this;
	};

}ST_TI_Shading, *PST_TI_Shading;


#pragma pack(pop)

#endif // Def_Shading_h__
