﻿#ifndef Wnd_Cfg_Particle_Entry_h__
#define Wnd_Cfg_Particle_Entry_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_T_Particle_Entry.h"
#include "Def_TestItem_VI.h"
#include "List_Particle_EntryOp.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_Particle_Entry
//-----------------------------------------------------------------------------
class CWnd_Cfg_Particle_Entry : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_Particle_Entry)

public:
	CWnd_Cfg_Particle_Entry();
	virtual ~CWnd_Cfg_Particle_Entry();

	enum enParticle_Entry_Static
	{
		STI_PT_PARAMETER = 0,
		STI_PT_SPEC,
		STI_PT_SENSITIVITY,
		STI_PT_MAXNUM,
	};

	enum enPaticle_Entry_Buttonbox
	{
		Btn_PT_MAXNUM = 1,
	};

	enum enParticle_Entry_Comobox
	{
		CMB_PT_MAX = 1,
	};

	enum enPaticle_Entry_Editbox
	{
		Edt_PT_SENSITIVITY = 0,
		Edt_PT_MAXNUM,
	};

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow			(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl			(UINT nID);
	afx_msg void	OnRangeIndexCtrl		(UINT nID);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);

	CList_Particle_EntryOp	m_List;

	CFont				m_font;
	
	CVGStatic			m_st_Item[STI_PT_MAXNUM];
	CMFCButton			m_bn_Item[Btn_PT_MAXNUM];
	CComboBox			m_cb_Item[CMB_PT_MAX];
	CMFCMaskedEdit		m_ed_Item[Edt_PT_MAXNUM];

	CVGStatic	  		m_st_CapItem;
	CVGStatic	  		m_st_CapSpecMin;
	CVGStatic	  		m_st_CapSpecMax;

	CVGStatic	  		m_st_Spec[Spec_Particle_Entry_MAX];
	CMFCMaskedEdit		m_ed_SpecMin[Spec_Particle_Entry_MAX];
	CMFCMaskedEdit		m_ed_SpecMax[Spec_Particle_Entry_MAX];
	CMFCButton	  		m_chk_SpecMin[Spec_Particle_Entry_MAX];
	CMFCButton	  		m_chk_SpecMax[Spec_Particle_Entry_MAX];

	ST_Particle_Entry_Opt*		m_pstConfigInfo = NULL;

public:

	void	SetPtr_RecipeInfo(ST_Particle_Entry_Opt* pstConfigInfo)
	{
		if (pstConfigInfo == NULL)
			return;

		m_pstConfigInfo = pstConfigInfo;
	};


	void SetUpdateData		();
	void GetUpdateData		();
};

#endif // Wnd_Cfg_Particle_Entry_h_
