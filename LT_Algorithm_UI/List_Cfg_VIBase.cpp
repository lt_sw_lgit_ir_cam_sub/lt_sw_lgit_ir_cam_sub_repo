//*****************************************************************************
// Filename	: 	List_Cfg_VIBase.cpp
// Created	:	2018/1/26 - 17:34
// Modified	:	2018/1/26 - 17:34
//
// Author	:	Author
//	
// Purpose	:	
//****************************************************************************
// List_Cfg_VIBase.cpp : implementation file
//

#include "stdafx.h"
#include "List_Cfg_VIBase.h"


// CList_Cfg_VIBase

IMPLEMENT_DYNAMIC(CList_Cfg_VIBase, CListCtrl)

CList_Cfg_VIBase::CList_Cfg_VIBase()
{

}

CList_Cfg_VIBase::~CList_Cfg_VIBase()
{
}


BEGIN_MESSAGE_MAP(CList_Cfg_VIBase, CListCtrl)
END_MESSAGE_MAP()



// CList_Cfg_VIBase message handlers


