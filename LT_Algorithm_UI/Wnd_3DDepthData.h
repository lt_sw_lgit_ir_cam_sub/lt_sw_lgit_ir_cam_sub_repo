#ifndef Wnd_3DDepthData_h__
#define Wnd_3DDepthData_h__

#pragma once

#include "afxwin.h"

#include "VGStatic.h"
#include "Def_T_3D_Depth.h"

// CWnd_3DDepthData
typedef enum
{
	en3DData_Header = 0,
	en3DData_BrightnessAvg = 0,
	en3DData_Standard_deviation,
	en3DData_MAX,
}enumHeader3DData;

typedef enum
{
	en3DData_1st = 0,
	en3DData_inputmax,
}enumData3DData;

static	LPCTSTR	g_sz3DData[] =
{
	_T("BrightnessAvg"),
	_T("Deviation"),
	NULL
};

class CWnd_3DDepthData : public CWnd
{
	DECLARE_DYNAMIC(CWnd_3DDepthData)

public:
	CWnd_3DDepthData();
	virtual ~CWnd_3DDepthData();

	UINT m_nHeaderCnt = en3DData_MAX;
	CString m_SzHeaderName[en3DData_MAX];

	//---Class 정의 시 세팅 해야함.
	void SetHeader(UINT nHeaderCnt){

		if (nHeaderCnt > en3DData_MAX)
		{
			nHeaderCnt = en3DData_MAX;
		}
		m_nHeaderCnt = nHeaderCnt;
	};

	void SetHeaderName(UINT nCnt, CString szHeader){

		if (nCnt > en3DData_MAX)
		{
			nCnt = en3DData_MAX - 1;
		}
		m_SzHeaderName[nCnt] = szHeader;
	};

	CVGStatic		m_st_Header[en3DData_MAX];
	CVGStatic		m_st_Data[en3DData_inputmax][en3DData_MAX];

//	CVGStatic		m_st_ResultView;


protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	void DataSetting(UINT nDataIdx, UINT nIdx, BOOL MODE, double dData);
	void DataEachReset(UINT nDataIdx, UINT nIdx);
	void DataReset();
	void DataDisplay(ST_3D_Depth_Data stData);
};


#endif // Wnd_3DDepthData_h__
