﻿#ifndef List_SFROp_h__
#define List_SFROp_h__

#pragma once

#include "List_Cfg_VIBase.h"
#include "Def_T_SFR.h"

typedef enum enListItemNum_SFROp
{
	SfOp_ItemNum = ROI_SFR_Max,
};

//-----------------------------------------------------------------------------
// List_SFRInfo
//-----------------------------------------------------------------------------
class CList_SFROp : public CList_Cfg_VIBase
{
	DECLARE_DYNAMIC(CList_SFROp)

public:
	CList_SFROp();
	virtual ~CList_SFROp();
	UINT m_nMessage;
	void SetWindowMessage(UINT nMeg){
		m_nMessage = nMeg;
	};
protected:
	
	DECLARE_MESSAGE_MAP()

	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg void	OnNMClick				(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk				(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL	OnMouseWheel			(UINT nFlags, short zDelta, CPoint pt);

	afx_msg void	OnEnKillFocusComboItem	();
	afx_msg void	OnEnSelectComboItem		();

	afx_msg void	OnEnKillFocusComboGroup	();
	afx_msg void	OnEnSelectComboGroup	();

	afx_msg void	OnEnKillFocusEdit		();

	BOOL			UpdateCellData			(UINT nRow, UINT nCol, int iValue);
	BOOL			UpdateCelldbData		(UINT nRow, UINT nCol, double dValue);

	CFont				m_Font;
	CMFCMaskedEdit		m_ed_CellEdit;
	UINT				m_nEditCol;
	UINT				m_nEditRow;

	CComboBox			m_cb_Type;
	CComboBox			m_cb_Color;
	CComboBox			m_cb_Item;
	CComboBox			m_cb_Group;

	ST_SFR_Opt*			m_pstConfigInfo = NULL;
	
public:

	void SetPtr_ConfigInfo(ST_SFR_Opt* pstConfigInfo)
	{
		if (pstConfigInfo == NULL)
			return;

		m_pstConfigInfo = pstConfigInfo;
	};

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();
	
};

#endif // List_SFRInfo_h__
