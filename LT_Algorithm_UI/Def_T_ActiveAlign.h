﻿#ifndef Def_T_ActiveAlign_h__
#define Def_T_ActiveAlign_h__

#include <afxwin.h>

#include "Def_T_Common.h"

#pragma pack(push,1)

typedef struct _tag_ActiveAlign_Opt
{
	UINT nTargetCnt;

	// 옵셋 광축
	int		iTargetX[2];
	int		iTargetY[2];
	double  dbTargetR;

	_tag_ActiveAlign_Opt()
	{
		nTargetCnt = 5;
		dbTargetR  = 0.0;
		for (UINT nIdx = 0; nIdx < 2; nIdx++)
		{
			iTargetX[nIdx] = 0;
			iTargetY[nIdx] = 0;
		}
	};

	_tag_ActiveAlign_Opt& operator= (_tag_ActiveAlign_Opt& ref)
	{
		nTargetCnt = ref.nTargetCnt;
		dbTargetR  = ref.dbTargetR;

		for (UINT nIdx = 0; nIdx < 2; nIdx++)
		{
			iTargetX[nIdx] = ref.iTargetX[nIdx];
			iTargetY[nIdx] = ref.iTargetY[nIdx];
		}

		return *this;
	};

}ST_ActiveAlign_Opt, *PST_ActiveAlign_Opt;

typedef struct _tag_ActiveAlign_Data
{
	UINT nResult;
	UINT nResultX;
	UINT nResultY;
	UINT nResultR;

	int iOC_X;
	int iOC_Y;
	double dbRoatae;

	void Reset()
	{
		nResult  = 1;
		nResultX = 1;
		nResultY = 1;
		nResultR = 1;

		iOC_X	 = 0;
		iOC_Y	 = 0;
		dbRoatae = 0.0;
	};

	_tag_ActiveAlign_Data()
	{
		Reset();
	};

	_tag_ActiveAlign_Data& operator= (_tag_ActiveAlign_Data& ref)
	{
		nResult		= ref.nResult;

		nResultX	= ref.nResultX;
		nResultY	= ref.nResultY;
		nResultR	= ref.nResultR;

		iOC_X		= ref.iOC_X;
		iOC_Y		= ref.iOC_Y;
		dbRoatae	= ref.dbRoatae;

		return *this;
	};

}ST_ActiveAlign_Data, *PST_ActiveAlign_Data;

typedef struct _tag_TI_ActiveAlign
{
	ST_ActiveAlign_Opt	stActiveAlignOpt;	// 검사 기준 데이터
	ST_ActiveAlign_Data	stActiveAlignData;	// 측정 데이터

	_tag_TI_ActiveAlign()
	{
	};

	void Reset()
	{
		stActiveAlignData.Reset();
	};

	_tag_TI_ActiveAlign& operator= (_tag_TI_ActiveAlign& ref)
	{
		stActiveAlignOpt  = ref.stActiveAlignOpt;
		stActiveAlignData = ref.stActiveAlignData;

		return *this;
	};

}ST_TI_ActiveAlign, *PST_TI_ActiveAlign;

#pragma pack(pop)

#endif // Def_ActiveAlign_h__