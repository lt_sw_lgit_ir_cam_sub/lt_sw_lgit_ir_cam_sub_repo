#ifndef Wnd_DefectPixelData_h__
#define Wnd_DefectPixelData_h__

#pragma once

#include "afxwin.h"

#include "VGStatic.h"
#include "Def_T_DefectPixel.h"
// CWnd_DefectPixelData
typedef enum
{
	enDPData_Header = 0,
	enDPData_MAX=1,
}enumHeaderDPData;

typedef enum
{
	enDPData_1st = 0,
	enDPData_inputmax,
}enumDataDPData;
static	LPCTSTR	g_szDPData[] =
{
	_T("Defect Cnt"),
	NULL
};
class CWnd_DefectPixelData : public CWnd
{
	DECLARE_DYNAMIC(CWnd_DefectPixelData)

public:
	CWnd_DefectPixelData();
	virtual ~CWnd_DefectPixelData();

	UINT m_nHeaderCnt = enDPData_MAX;
	CString m_SzHeaderName[enDPData_MAX];

	//---Class 정의 시 세팅 해야함.
	void SetHeader(UINT nHeaderCnt){

		if (nHeaderCnt > enDPData_MAX)
		{
			nHeaderCnt = enDPData_MAX;
		}
		m_nHeaderCnt = nHeaderCnt;
	};

	void SetHeaderName(UINT nCnt, CString szHeader){

		if (nCnt > enDPData_MAX)
		{
			nCnt = enDPData_MAX-1;
		}
		m_SzHeaderName[nCnt] = szHeader;
	};

	CVGStatic		m_st_Header[enDPData_MAX];
	CVGStatic		m_st_Data[enDPData_inputmax][enDPData_MAX];

//	CVGStatic		m_st_ResultView;


protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	void DataSetting(UINT nDataIdx, UINT nIdx, BOOL MODE, double dData);
	void DataEachReset(UINT nDataIdx, UINT nIdx);
	void DataReset();
	void DataDisplay(ST_DefectPixel_Data stData);
};


#endif // Wnd_DefectPixelData_h__
