﻿#ifndef Wnd_Cfg_Intensity_h__
#define Wnd_Cfg_Intensity_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_T_Intensity.h"
#include "Def_TestItem_VI.h"
#include "List_IntensityOp.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_Intensity
//-----------------------------------------------------------------------------
class CWnd_Cfg_Intensity : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_Intensity)

public:
	CWnd_Cfg_Intensity();
	virtual ~CWnd_Cfg_Intensity();

	enum enIntensityStatic
	{
		STI_ITS_PARAM = 0,
		STI_ITS_SPEC,
		STI_ITS_MAX,
	};

	enum enIntensityButton
	{
		BTN_ITS_RESET = 0,
		BTN_ITS_TEST,
		BTN_ITS_MAX,
	};

	enum enIntensityComobox
	{
		CMB_ITS_MAX = 1,
	};

	enum enIntensityEdit
	{
		EDT_ITS_MAX = 1,
	};
	
protected:

	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	CFont						m_font;

	CList_IntensityOp			m_List;

	CVGStatic					m_st_Item[STI_ITS_MAX];
	CButton						m_bn_Item[BTN_ITS_MAX];
	CComboBox					m_cb_Item[CMB_ITS_MAX];
	CMFCMaskedEdit				m_ed_Item[EDT_ITS_MAX];

	CVGStatic					m_st_CapItem;
	CVGStatic					m_st_CapSpecMin;
	CVGStatic					m_st_CapSpecMax;

	CVGStatic					m_st_Spec[Spec_Intensity_MAX];
	CMFCMaskedEdit				m_ed_SpecMin[Spec_Intensity_MAX];
	CMFCMaskedEdit				m_ed_SpecMax[Spec_Intensity_MAX];
	CMFCButton					m_chk_SpecMin[Spec_Intensity_MAX];
	CMFCButton					m_chk_SpecMax[Spec_Intensity_MAX];

	ST_Intensity_Opt*			m_pstConfigInfo = NULL;

public:

	void	SetPtr_RecipeInfo	(ST_Intensity_Opt* pstConfigInfo)
	{
		if (pstConfigInfo == NULL)
			return;

		m_pstConfigInfo = pstConfigInfo;
	};

	void	SetUpdateData		();
	void	GetUpdateData		();
};

#endif // Wnd_Cfg_Intensity_h__
