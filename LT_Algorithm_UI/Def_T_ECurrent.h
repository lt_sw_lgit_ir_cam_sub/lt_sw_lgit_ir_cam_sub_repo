﻿#ifndef Def_T_ECurrent_h__
#define Def_T_ECurrent_h__

#include <afxwin.h>

#include "Def_T_Common.h"

#pragma pack(push,1)

typedef enum enECurrent_Spec
{
	Spec_ECurrent_CH1 = 0,
	Spec_ECurrent_CH2,
	Spec_ECurrent_CH3,
	Spec_ECurrent_CH4,
	Spec_ECurrent_CH5,
	Spec_ECurrent_Max,
};

static LPCTSTR	g_szECurrent_Spec[] =
{
	_T("1.8V"),
	_T("2.8V"),
	NULL
};

static LPCTSTR	g_szECurrent_Spec_Entry[] =
{
	_T("CH 1"),
	_T("CH 2"),
	_T("CH 3"),
	_T("CH 4"),
	_T("CH 5"),
	NULL
};
typedef struct _tag_ECurrent_Opt
{
	double	dbOffset[2][Spec_ECurrent_Max];

	ST_Comm_Spec stSpec_Min[Spec_ECurrent_Max];
	ST_Comm_Spec stSpec_Max[Spec_ECurrent_Max];

	_tag_ECurrent_Opt()
	{
		for (UINT nIdx = 0; nIdx < Spec_ECurrent_Max; nIdx++)
		{
			for (int t = 0; t < 2; t++)
			{
				dbOffset[t][nIdx] = 0.0;
			}
			stSpec_Min[nIdx].Reset();
			stSpec_Max[nIdx].Reset();
		}
	};

	_tag_ECurrent_Opt& operator= (_tag_ECurrent_Opt& ref)
	{
		for (UINT nIdx = 0; nIdx < Spec_ECurrent_Max; nIdx++)
		{
			for (int t = 0; t < 2; t++)
			{
				dbOffset[t][nIdx] = ref.dbOffset[t][nIdx];
			}
			stSpec_Min[nIdx] = ref.stSpec_Min[nIdx];
			stSpec_Max[nIdx] = ref.stSpec_Max[nIdx];
		}

		return *this;
	};

}ST_ECurrent_Opt, *PST_ECurrent_Opt;

typedef struct _tag_ECurrent_Data
{
	// 결과
	UINT nResult;

	UINT nEachResult[Spec_ECurrent_Max];
	double dbValue[Spec_ECurrent_Max];

	_tag_ECurrent_Data()
	{
		Reset();
	};

	void Reset()
	{
		nResult = 1;

		for (UINT nIdx = 0; nIdx < Spec_ECurrent_Max; nIdx++)
		{
			nEachResult[nIdx] = 0;
			dbValue[nIdx] = 0.0;
		}
	};

	void FailData(){
		nResult = 0;

		for (UINT nIdx = 0; nIdx < Spec_ECurrent_Max; nIdx++)
		{
			nEachResult[nIdx] = 0;
			dbValue[nIdx] = 0.0;
		}
	};
	_tag_ECurrent_Data& operator= (_tag_ECurrent_Data& ref)
	{
		nResult		= ref.nResult;

		for (UINT nIdx = 0; nIdx < Spec_ECurrent_Max; nIdx++)
		{
			nEachResult[nIdx] = ref.nEachResult[nIdx];
			dbValue[nIdx] = ref.dbValue[nIdx];
		}

		return *this;
	};

}ST_ECurrent_Data, *PST_ECurrent_Data;

typedef struct _tag_TI_ECurrent
{
	ST_ECurrent_Opt		stECurrentOpt;		// 검사 기준 데이터
	ST_ECurrent_Data	stECurrentData;		// 측정 데이터

	_tag_TI_ECurrent()
	{
	};

	void Reset()
	{
		stECurrentData.Reset();
	};

	_tag_TI_ECurrent& operator= (_tag_TI_ECurrent& ref)
	{
		stECurrentOpt	= ref.stECurrentOpt;
		stECurrentData	= ref.stECurrentData;

		return *this;
	};

}ST_TI_ECurrent, *PST_TI_ECurrent;

#pragma pack(pop)

#endif // Def_ECurrent_h__