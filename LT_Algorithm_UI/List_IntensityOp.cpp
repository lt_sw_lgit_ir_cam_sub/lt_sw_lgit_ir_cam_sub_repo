﻿// List_IntensityOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_IntensityOp.h"

typedef enum enListNum_IntensityOp
{
	IstOp_Object = 0,
	IstOp_PosX,
	IstOp_PosY,
	IstOp_Width,
	IstOp_Height,
	IstOp_Offset,
	IstOp_ItemPos,
	IstOp_MaxCol,
};

static LPCTSTR	g_lpszHeader_IntensityOp[] =
{
	_T(""),
	_T("CenterX"),
	_T("CenterY"),
	_T("Width"),
	_T("Height"),
	_T("Offset"),
	_T("PIC"),					 //SfOp_ItemPos,
	NULL
};

static LPCTSTR	g_lpszItem_IntensityOp[] =
{
	NULL
};

const int	iListAglin_IntensityOp[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_IntensityOp[] =
{
	137,
	137,
	137,
	137,
	137,
	137,
	137,
};

#define IDC_EDT_CELLEDIT		1000
#define IDC_COM_CELLCOMBO_TYPE	2000
#define IDC_COM_CELLCOMBO_COLOR	3000
#define IDC_COM_CELLCOMBO_ITEM	4000

// CList_IntensityOp

IMPLEMENT_DYNAMIC(CList_IntensityOp, CList_Cfg_VIBase)

CList_IntensityOp::CList_IntensityOp()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol = 0;
	m_nEditRow = 0;

	m_pstConfigInfo = NULL;
}

CList_IntensityOp::~CList_IntensityOp()
{
	m_Font.DeleteObject();
}
BEGIN_MESSAGE_MAP(CList_IntensityOp, CList_Cfg_VIBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT	(NM_CLICK,					&CList_IntensityOp::OnNMClick				)
	ON_NOTIFY_REFLECT	(NM_DBLCLK,					&CList_IntensityOp::OnNMDblclk				)
	ON_EN_KILLFOCUS		(IDC_EDT_CELLEDIT,			&CList_IntensityOp::OnEnKillFocusEdit		)
	ON_CBN_KILLFOCUS	(IDC_COM_CELLCOMBO_TYPE,	&CList_IntensityOp::OnEnKillFocusComboType	)
	ON_CBN_SELCHANGE	(IDC_COM_CELLCOMBO_TYPE,	&CList_IntensityOp::OnEnSelectComboType		)	
	ON_CBN_KILLFOCUS	(IDC_COM_CELLCOMBO_COLOR,	&CList_IntensityOp::OnEnKillFocusComboColor	)	
	ON_CBN_SELCHANGE	(IDC_COM_CELLCOMBO_COLOR,	&CList_IntensityOp::OnEnSelectComboColor	)
	ON_CBN_KILLFOCUS	(IDC_COM_CELLCOMBO_ITEM,	&CList_IntensityOp::OnEnKillFocusComboItem	)
	ON_CBN_SELCHANGE	(IDC_COM_CELLCOMBO_ITEM,	&CList_IntensityOp::OnEnSelectComboItem		)	
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_IntensityOp 메시지 처리기입니다.
int CList_IntensityOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CList_Cfg_VIBase::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER | LVS_EX_CHECKBOXES);

	InitHeader();
	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER | ES_NUMBER, CRect(0, 0, 0, 0), this, IDC_EDT_CELLEDIT);
	this->GetHeaderCtrl()->EnableWindow(FALSE);

	m_cb_Type.Create(WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | CBS_DROPDOWNLIST, CRect(0, 0, 0, 0), this, IDC_COM_CELLCOMBO_TYPE);
	m_cb_Type.ResetContent();

	for (UINT nIdx = 0; NULL != g_szMarkType[nIdx]; nIdx++)
	{
		m_cb_Type.InsertString(nIdx, g_szMarkType[nIdx]);
	}

	m_cb_Color.Create(WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | CBS_DROPDOWNLIST, CRect(0, 0, 0, 0), this, IDC_COM_CELLCOMBO_COLOR);
	m_cb_Color.ResetContent();

	for (UINT nIdx = 0; NULL != g_szMarkColor[nIdx]; nIdx++)
	{
		m_cb_Color.InsertString(nIdx, g_szMarkColor[nIdx]);
	}

	m_cb_Item.Create(WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | CBS_DROPDOWNLIST, CRect(0, 0, 0, 0), this, IDC_COM_CELLCOMBO_ITEM);
	m_cb_Item.ResetContent();

	for (UINT nIdex = 0; NULL != g_szItemPos[nIdex]; nIdex++)
	{
		m_cb_Item.InsertString(nIdex, g_szItemPos[nIdex]);
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_IntensityOp::OnSize(UINT nType, int cx, int cy)
{
	CList_Cfg_VIBase::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_IntensityOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CList_Cfg_VIBase::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_IntensityOp::InitHeader()
{
	for (int nCol = 0; nCol < IstOp_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_IntensityOp[nCol], iListAglin_IntensityOp[nCol], iHeaderWidth_IntensityOp[nCol]);
	}

	for (int nCol = 0; nCol < IstOp_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_IntensityOp[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 17:25
// Desc.		:
//=============================================================================
void CList_IntensityOp::InsertFullData()
{
 	DeleteAllItems();

	for (UINT nIdx = 0; nIdx < IstOp_ItemNum; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
 	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/6/26 - 14:26
// Desc.		:
//=============================================================================
void CList_IntensityOp::SetRectRow(UINT nRow)
{
	if (NULL == m_pstConfigInfo)
		return;

	CString strValue;

	strValue.Format(_T("%s"), g_szROI_Intencity[nRow]);
	SetItemText(nRow, IstOp_Object, strValue);

	SetItemState(nRow, 0x2000, LVIS_STATEIMAGEMASK);

	strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.CenterPoint().x);
	SetItemText(nRow, IstOp_PosX, strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.CenterPoint().y);
	SetItemText(nRow, IstOp_PosY, strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.Width());
	SetItemText(nRow, IstOp_Width, strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.Height());
	SetItemText(nRow, IstOp_Height, strValue);

	strValue.Format(_T("%.2f"), m_pstConfigInfo->stRegion[nRow].dbOffset);
	SetItemText(nRow, IstOp_Offset, strValue);

	strValue.Format(_T("%s"), g_szItemPos[m_pstConfigInfo->stRegion[nRow].nItemPos]);
	SetItemText(nRow, IstOp_ItemPos, strValue);
}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_IntensityOp::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	LVHITTESTINFO HitInfo;
	HitInfo.pt = pNMItemActivate->ptAction;

	HitTest(&HitInfo);

	if (ROI_Intencity_CC == pNMItemActivate->iItem)
	{
		m_pstConfigInfo->stRegion[pNMItemActivate->iItem].bEnable = TRUE;
		*pResult = 1;
		return;
	}

	// Check Ctrl Event
	if (HitInfo.flags == LVHT_ONITEMSTATEICON)
	{
		UINT nBuffer;

		nBuffer = GetItemState(pNMItemActivate->iItem, LVIS_STATEIMAGEMASK);

		if (nBuffer == 0x2000)
			m_pstConfigInfo->stRegion[pNMItemActivate->iItem].bEnable = FALSE;

		if (nBuffer == 0x1000)
			m_pstConfigInfo->stRegion[pNMItemActivate->iItem].bEnable = TRUE;
	}

	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_IntensityOp::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (ROI_Intencity_CC == pNMItemActivate->iItem && IstOp_Offset == m_nEditRow)
	{
		m_pstConfigInfo->stRegion[pNMItemActivate->iItem].bEnable = TRUE;
		*pResult = 1;
		return;
	}

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem < IstOp_MaxCol && pNMItemActivate->iSubItem > 0)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);
			
			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

			if (pNMItemActivate->iSubItem == IstOp_ItemPos)
			{
				m_cb_Item.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_cb_Item.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_cb_Item.SetFocus();
				m_cb_Item.SetCurSel(m_pstConfigInfo->stRegion[m_nEditRow].nItemPos);
			}
			else
			{
				m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_ed_CellEdit.SetFocus();
			}
		}
	}

	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
void CList_IntensityOp::OnEnKillFocusEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	if (m_nEditCol == IstOp_Offset)
		UpdateCelldbData(m_nEditRow, m_nEditCol, _ttof(strText));
	else
		UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText));

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
BOOL CList_IntensityOp::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	if (NULL == m_pstConfigInfo)
		return FALSE;

	if (iValue < 0)
		iValue = 0;

	CRect rtTemp = m_pstConfigInfo->stRegion[nRow].rtROI;

	switch (nCol)
	{
	case IstOp_PosX:
		m_pstConfigInfo->stRegion[nRow].RectPosXY(iValue, rtTemp.CenterPoint().y);
		break;
	case IstOp_PosY:
		m_pstConfigInfo->stRegion[nRow].RectPosXY(rtTemp.CenterPoint().x, iValue);
		break;
	case IstOp_Width:
		m_pstConfigInfo->stRegion[nRow].RectPosWH(iValue, rtTemp.Height());
		break;
	case IstOp_Height:
		m_pstConfigInfo->stRegion[nRow].RectPosWH(rtTemp.Width(), iValue);
		break;
	default:
		break;
	}

	if (m_pstConfigInfo->stRegion[nRow].rtROI.left < 0)
	{
		CRect rtTemp = m_pstConfigInfo->stRegion[nRow].rtROI;

		m_pstConfigInfo->stRegion[nRow].rtROI.left = 0;
		m_pstConfigInfo->stRegion[nRow].rtROI.right = m_pstConfigInfo->stRegion[nRow].rtROI.left + rtTemp.Width();
	}

	if (m_pstConfigInfo->stRegion[nRow].rtROI.right >(LONG)m_dwImage_Width)
	{
		CRect rtTemp = m_pstConfigInfo->stRegion[nRow].rtROI;

		m_pstConfigInfo->stRegion[nRow].rtROI.right = (LONG)m_dwImage_Width;
		m_pstConfigInfo->stRegion[nRow].rtROI.left = m_pstConfigInfo->stRegion[nRow].rtROI.right - rtTemp.Width();
	}

	if (m_pstConfigInfo->stRegion[nRow].rtROI.top < 0)
	{
		CRect rtTemp = m_pstConfigInfo->stRegion[nRow].rtROI;

		m_pstConfigInfo->stRegion[nRow].rtROI.top = 0;
		m_pstConfigInfo->stRegion[nRow].rtROI.bottom = rtTemp.Height() + m_pstConfigInfo->stRegion[nRow].rtROI.top;
	}

	if (m_pstConfigInfo->stRegion[nRow].rtROI.bottom >(LONG)m_dwImage_Height)
	{
		CRect rtTemp = m_pstConfigInfo->stRegion[nRow].rtROI;

		m_pstConfigInfo->stRegion[nRow].rtROI.bottom = (LONG)m_dwImage_Height;
		m_pstConfigInfo->stRegion[nRow].rtROI.top = m_pstConfigInfo->stRegion[nRow].rtROI.bottom - rtTemp.Height();
	}

	if (m_pstConfigInfo->stRegion[nRow].rtROI.Height() <= 0)
		m_pstConfigInfo->stRegion[nRow].RectPosWH(m_pstConfigInfo->stRegion[nRow].rtROI.Width(), 1);

	if (m_pstConfigInfo->stRegion[nRow].rtROI.Height() >= (LONG)m_dwImage_Height)
		m_pstConfigInfo->stRegion[nRow].RectPosWH(m_pstConfigInfo->stRegion[nRow].rtROI.Width(), m_dwImage_Height);

	if (m_pstConfigInfo->stRegion[nRow].rtROI.Width() <= 0)
		m_pstConfigInfo->stRegion[nRow].RectPosWH(1, m_pstConfigInfo->stRegion[nRow].rtROI.Height());

	if (m_pstConfigInfo->stRegion[nRow].rtROI.Width() >= (LONG)m_dwImage_Width)
		m_pstConfigInfo->stRegion[nRow].RectPosWH(m_dwImage_Width, m_pstConfigInfo->stRegion[nRow].rtROI.Height());

	CString strValue;

	switch (nCol)
	{
	case IstOp_PosX:
		strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.CenterPoint().x);
		break;
	case IstOp_PosY:
		strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.CenterPoint().y);
		break;
	case IstOp_Width:
		strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.Width());
		break;
	case IstOp_Height:
		strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.Height());
		break;
	default:
		break;
	}

	m_ed_CellEdit.SetWindowText(strValue);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: UpdateCellData_double
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dbValue
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_IntensityOp::UpdateCelldbData(UINT nRow, UINT nCol, double dbValue)
{
	switch (nCol)
	{
	case IstOp_Offset:
		m_pstConfigInfo->stRegion[nRow].dbOffset = dbValue;
		break;
	default:
		break;
	}

	CString str;
	str.Format(_T("%.2f"), dbValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_IntensityOp::GetCellData()
{
	return;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_IntensityOp::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue		= _ttoi(strText);
		double dbValue  = _ttof(strText);

		iValue = iValue + ((zDelta / 120));
		dbValue = dbValue + ((zDelta / 120)*0.1);

		if (iValue < 0)
			iValue = 0;

		if (iValue > 2000)
			iValue = 2000;

		if (dbValue < 0.0)
			dbValue = 0.0;

		if (dbValue > 2.0)
			dbValue = 2.0;
		
		if (m_nEditCol == IstOp_Offset)
		{
			UpdateCelldbData(m_nEditRow, m_nEditCol, dbValue);
		}
		else
		{
			UpdateCellData(m_nEditRow, m_nEditCol, iValue);
		}
	}

	return CList_Cfg_VIBase::OnMouseWheel(nFlags, zDelta, pt);
}

//=============================================================================
// Method		: OnEnKillFocusComboType
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/18 - 14:11
// Desc.		:
//=============================================================================
void CList_IntensityOp::OnEnKillFocusComboType()
{
	return;
}

//=============================================================================
// Method		: OnEnSelectComboType
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/18 - 14:11
// Desc.		:
//=============================================================================
void CList_IntensityOp::OnEnSelectComboType()
{
	return;
}

//=============================================================================
// Method		: OnEnKillFocusComboColor
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/18 - 14:11
// Desc.		:
//=============================================================================
void CList_IntensityOp::OnEnKillFocusComboColor()
{
	return;
}

//=============================================================================
// Method		: OnEnSelectComboColor
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/18 - 14:11
// Desc.		:
//=============================================================================
void CList_IntensityOp::OnEnSelectComboColor()
{
	return;
}

//=============================================================================
// Method		: OnEnKillFocusComboItem
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/2 - 11:36
// Desc.		:
//=============================================================================
void CList_IntensityOp::OnEnKillFocusComboItem()
{
	SetItemText(m_nEditRow, IstOp_ItemPos, g_szItemPos[m_pstConfigInfo->stRegion[m_nEditRow].nItemPos]);
	m_cb_Item.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: OnEnSelectComboItem
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/2 - 11:36
// Desc.		:
//=============================================================================
void CList_IntensityOp::OnEnSelectComboItem()
{
	m_pstConfigInfo->stRegion[m_nEditRow].nItemPos = m_cb_Item.GetCurSel();

	SetItemText(m_nEditRow, IstOp_ItemPos, g_szItemPos[m_pstConfigInfo->stRegion[m_nEditRow].nItemPos]);
	m_cb_Item.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}