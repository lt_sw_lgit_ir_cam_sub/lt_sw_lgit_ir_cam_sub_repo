﻿// List_FPNOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_FPNOp.h"

typedef enum enListNum_FPNOp
{
	FPNOp_Object = 0,
	FPNOp_PosX,
	FPNOp_PosY,
	FPNOp_Width,
	FPNOp_Height,
	FPNOp_MaxCol,
};

static LPCTSTR	g_lpszHeader_FPNOp[] =
{
	_T(""),
	_T("CenterX"),
	_T("CenterY"),
	_T("Width"),
	_T("Height"),
	NULL
};

static LPCTSTR	g_lpszItem_FPNOp[] =
{
	NULL
};

const int	iListAglin_FPNOp[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_FPNOp[] =
{
	105,
	105,
	105,
	105,
	105,
};

#define IDC_EDT_CELLEDIT		1000
#define IDC_COM_CELLCOMBO_TYPE	2000
#define IDC_COM_CELLCOMBO_COLOR	3000


IMPLEMENT_DYNAMIC(CList_FPNOp, CList_Cfg_VIBase)

CList_FPNOp::CList_FPNOp()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol = 0;
	m_nEditRow = 0;
}

CList_FPNOp::~CList_FPNOp()
{
	m_Font.DeleteObject();
}
BEGIN_MESSAGE_MAP(CList_FPNOp, CList_Cfg_VIBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(NM_CLICK, &CList_FPNOp::OnNMClick)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CList_FPNOp::OnNMDblclk)
	ON_NOTIFY_REFLECT(NM_CLICK, &CList_FPNOp::OnNMClick)
	ON_NOTIFY_REFLECT(NM_DBLCLK, &CList_FPNOp::OnNMDblclk)
	ON_EN_KILLFOCUS(IDC_EDT_CELLEDIT, &CList_FPNOp::OnEnKillFocusEdit)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_FPNOp 메시지 처리기입니다.
int CList_FPNOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CList_Cfg_VIBase::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader();
	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER /*| ES_NUMBER*/, CRect(0, 0, 0, 0), this, IDC_EDT_CELLEDIT);
	this->GetHeaderCtrl()->EnableWindow(FALSE);

	m_cb_Type.Create(WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | CBS_DROPDOWNLIST, CRect(0, 0, 0, 0), this, IDC_COM_CELLCOMBO_TYPE);
	m_cb_Type.ResetContent();

	for (UINT nIdex = 0; NULL != g_szMarkType[nIdex]; nIdex++)
	{
		m_cb_Type.InsertString(nIdex, g_szMarkType[nIdex]);
	}

	m_cb_Color.Create(WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | CBS_DROPDOWNLIST, CRect(0, 0, 0, 0), this, IDC_COM_CELLCOMBO_COLOR);
	m_cb_Color.ResetContent();

	for (UINT nIdex = 0; NULL != g_szMarkColor[nIdex]; nIdex++)
	{
		m_cb_Color.InsertString(nIdex, g_szMarkColor[nIdex]);
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_FPNOp::OnSize(UINT nType, int cx, int cy)
{
	CList_Cfg_VIBase::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_FPNOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT;

	return CList_Cfg_VIBase::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_FPNOp::InitHeader()
{
	for (int nCol = 0; nCol < FPNOp_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_FPNOp[nCol], iListAglin_FPNOp[nCol], iHeaderWidth_FPNOp[nCol]);
	}

	for (int nCol = 0; nCol < FPNOp_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_FPNOp[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 17:25
// Desc.		:
//=============================================================================
void CList_FPNOp::InsertFullData()
{
	DeleteAllItems();

	for (UINT nIdx = 0; nIdx < FPNOp_ItemNum; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/6/26 - 14:26
// Desc.		:
//=============================================================================
void CList_FPNOp::SetRectRow(UINT nRow)
{
	if (NULL == m_pstConfigInfo)
		return;

	CString strValue;

	strValue.Format(_T("%s"), g_szROI_FPN[nRow]);
	SetItemText(nRow, FPNOp_Object, strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.CenterPoint().x);
	SetItemText(nRow, FPNOp_PosX, strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.CenterPoint().y);
	SetItemText(nRow, FPNOp_PosY, strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.Width());
	SetItemText(nRow, FPNOp_Width, strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.Height());
	SetItemText(nRow, FPNOp_Height, strValue);
}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_FPNOp::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_FPNOp::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem < FPNOp_MaxCol && pNMItemActivate->iSubItem > 0)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);

			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

	
			m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
			m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
			m_ed_CellEdit.SetFocus();
			
		}
		GetOwner()->SendNotifyMessage(m_nMessage, 0, 0);
	}
	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
void CList_FPNOp::OnEnKillFocusEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText));

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
BOOL CList_FPNOp::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	if (NULL == m_pstConfigInfo)
		return FALSE;

	if (iValue < 0)
		iValue = 0;

	CRect rtTemp = m_pstConfigInfo->stRegion[nRow].rtROI;

	switch (nCol)
	{
	case FPNOp_PosX:
		m_pstConfigInfo->stRegion[nRow].RectPosXY(iValue, rtTemp.CenterPoint().y);
		break;
	case FPNOp_PosY:
		m_pstConfigInfo->stRegion[nRow].RectPosXY(rtTemp.CenterPoint().x, iValue);
		break;
	case FPNOp_Width:
		m_pstConfigInfo->stRegion[nRow].RectPosWH(iValue, rtTemp.Height());
		break;
	case FPNOp_Height:
		m_pstConfigInfo->stRegion[nRow].RectPosWH(rtTemp.Width(), iValue);
		break;
	default:
		break;
	}

	if (m_pstConfigInfo->stRegion[nRow].rtROI.left < 0)
	{
		CRect rtTemp = m_pstConfigInfo->stRegion[nRow].rtROI;

		m_pstConfigInfo->stRegion[nRow].rtROI.left = 0;
		m_pstConfigInfo->stRegion[nRow].rtROI.right = m_pstConfigInfo->stRegion[nRow].rtROI.left + rtTemp.Width();
	}

	if (m_pstConfigInfo->stRegion[nRow].rtROI.right >(LONG)m_dwImage_Width)
	{
		CRect rtTemp = m_pstConfigInfo->stRegion[nRow].rtROI;

		m_pstConfigInfo->stRegion[nRow].rtROI.right = (LONG)m_dwImage_Width;
		m_pstConfigInfo->stRegion[nRow].rtROI.left = m_pstConfigInfo->stRegion[nRow].rtROI.right - rtTemp.Width();
	}

	if (m_pstConfigInfo->stRegion[nRow].rtROI.top < 0)
	{
		CRect rtTemp = m_pstConfigInfo->stRegion[nRow].rtROI;

		m_pstConfigInfo->stRegion[nRow].rtROI.top = 0;
		m_pstConfigInfo->stRegion[nRow].rtROI.bottom = rtTemp.Height() + m_pstConfigInfo->stRegion[nRow].rtROI.top;
	}

	if (m_pstConfigInfo->stRegion[nRow].rtROI.bottom >(LONG)m_dwImage_Height)
	{
		CRect rtTemp = m_pstConfigInfo->stRegion[nRow].rtROI;

		m_pstConfigInfo->stRegion[nRow].rtROI.bottom = (LONG)m_dwImage_Height;
		m_pstConfigInfo->stRegion[nRow].rtROI.top = m_pstConfigInfo->stRegion[nRow].rtROI.bottom - rtTemp.Height();
	}

	if (m_pstConfigInfo->stRegion[nRow].rtROI.Height() <= 0)
		m_pstConfigInfo->stRegion[nRow].RectPosWH(m_pstConfigInfo->stRegion[nRow].rtROI.Width(), 1);

	if (m_pstConfigInfo->stRegion[nRow].rtROI.Height() >= (LONG)m_dwImage_Height)
		m_pstConfigInfo->stRegion[nRow].RectPosWH(m_pstConfigInfo->stRegion[nRow].rtROI.Width(), m_dwImage_Height);

	if (m_pstConfigInfo->stRegion[nRow].rtROI.Width() <= 0)
		m_pstConfigInfo->stRegion[nRow].RectPosWH(1, m_pstConfigInfo->stRegion[nRow].rtROI.Height());

	if (m_pstConfigInfo->stRegion[nRow].rtROI.Width() >= (LONG)m_dwImage_Width)
		m_pstConfigInfo->stRegion[nRow].RectPosWH(m_dwImage_Width, m_pstConfigInfo->stRegion[nRow].rtROI.Height());

	CString strValue;

	switch (nCol)
	{
	case FPNOp_PosX:
		strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.CenterPoint().x);
		break;
	case FPNOp_PosY:
		strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.CenterPoint().y);
		break;
	case FPNOp_Width:
		strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.Width());
		break;
	case FPNOp_Height:
		strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.Height());
		break;
	default:
		break;
	}

	m_ed_CellEdit.SetWindowText(strValue);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: UpdateCellData_double
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dbValue
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_FPNOp::UpdateCelldbData(UINT nRow, UINT nCol, double dbValue)
{
	CString str;
	str.Format(_T("%.1f"), dbValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_FPNOp::GetCellData()
{
	return;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_FPNOp::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue = _ttoi(strText);
		double dbValue = _ttof(strText);

		iValue = iValue + ((zDelta / 120));
		dbValue = dbValue + ((zDelta / 120)*0.1);

		if (iValue < 0)
			iValue = 0;

		if (iValue > 2000)
			iValue = 2000;

		if (dbValue < 0.0)
			dbValue = 0.0;

		if (dbValue > 2.0)
			dbValue = 2.0;

		UpdateCellData(m_nEditRow, m_nEditCol, iValue);
	}

	return CList_Cfg_VIBase::OnMouseWheel(nFlags, zDelta, pt);
}

