﻿#ifndef Wnd_Cfg_DefectPixel_h__
#define Wnd_Cfg_DefectPixel_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_T_DefectPixel.h"
#include "Def_TestItem_VI.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_DefectPixel
//-----------------------------------------------------------------------------
class CWnd_Cfg_DefectPixel : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_DefectPixel)

public:
	CWnd_Cfg_DefectPixel();
	virtual ~CWnd_Cfg_DefectPixel();

	enum enDefectPixelStatic
	{
		STI_DEF_PARAM = 0,
		STI_DEF_SPEC,
		STI_DEF_BLOCKSIZE,
		STI_DEF_THRESHOLD_VeryHotPixel,
		STI_DEF_THRESHOLD_HotPixel,
		STI_DEF_THRESHOLD_VeryBrightPixel,
		STI_DEF_THRESHOLD_BrightPixel,
		STI_DEF_THRESHOLD_VeryDeadPixel,
		STI_DEF_THRESHOLD_DeadPixel,
		STI_DEF_THRESHOLD_LowCol,
		STI_DEF_MAX,
	};

	enum enDefectPixelButton
	{
		BTN_DEF_RESET = 0,
		BTN_DEF_TEST,
		BTN_DEF_MAX,
	};

	enum enDefectPixelComobox
	{
		CMB_DEF_MAX = 1,
	};

	enum enDefectPixelEdit
	{
		EDT_DEF_BLOCKSIZE = 0,
		EDT_DEF_THRESHOLD_VeryHotPixel,
		EDT_DEF_THRESHOLD_HotPixel,
		EDT_DEF_THRESHOLD_VeryBrightPixel,
		EDT_DEF_THRESHOLD_BrightPixel,
		EDT_DEF_THRESHOLD_VeryDeadPixel,
		EDT_DEF_THRESHOLD_DeadPixel,
		EDT_DEF_THRESHOLD_LowCol,
		EDT_DEF_MAX,
	};

protected:
	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	CFont						m_font;

	CVGStatic					m_st_Item[STI_DEF_MAX];
	CButton						m_bn_Item[BTN_DEF_MAX];
	CComboBox					m_cb_Item[CMB_DEF_MAX];
	CMFCMaskedEdit				m_ed_Item[EDT_DEF_MAX];

	CVGStatic					m_st_CapItem;
	CVGStatic					m_st_CapSpecMin;
	CVGStatic					m_st_CapSpecMax;

	CVGStatic					m_st_Spec[Spec_Defect_Max];
	CMFCMaskedEdit				m_ed_SpecMin[Spec_Defect_Max];
	CMFCMaskedEdit				m_ed_SpecMax[Spec_Defect_Max];
	CMFCButton					m_chk_SpecMin[Spec_Defect_Max];
	CMFCButton					m_chk_SpecMax[Spec_Defect_Max];

	ST_DefectPixel_Opt*			m_pstConfigInfo = NULL;

public:

	void	SetPtr_RecipeInfo(__in ST_DefectPixel_Opt* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstConfigInfo = pstRecipeInfo;
	};

	void	SetUpdateData		();
	void	GetUpdateData		();

	//void	Get_TestItemInfo	(__in UINT nBegin, __in UINT nEnd, __out ST_TestItemInfo& stOutTestItemInfo);
};

#endif // Wnd_DefectPixel_h__
