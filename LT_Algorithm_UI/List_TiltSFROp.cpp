﻿// List_TiltSFROp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_TiltSFROp.h"

// CList_TiltSFROp

typedef enum enListNum_TiltSFROp
{
	TiltSfOp_Object = 0,
	TiltSfOp_StardRoi,
	TiltSfOp_CompareRoi,
	TiltSfOp_Spec,
	TiltSfOp_MaxCol,
};

static LPCTSTR	g_lpszHeader_SFROp[] =
{
	_T(""),			//SfOp_Object = 0,
	_T("Group 1"),		//TiltSfOp_StardRoi,
	_T("Group 2"),		//TiltSfOp_GapRoi,
	_T("Spec"),		//TiltSfOp_Spec,
	NULL
};
static LPCTSTR	g_lpszHeader_SFROp_Chn[] =
{
	_T(""),						 //SfOp_Object = 0,
	_T("基准"),		//TiltSfOp_StardRoi,
	_T("比较"),		//TiltSfOp_GapRoi,
	_T("Spec"),				//TiltSfOp_Spec,
	NULL
};
static LPCTSTR	g_lpszItem_SFROp[] =
{
	NULL
};

const int	iListAglin_SFROp[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_SFROp[] =
{
	85,
	85,
	85,
	85,
	//60,
};

#define IDC_EDT_CELLEDIT			1000
#define IDC_COM_CELLCOMBO_TYPE		2000
#define IDC_COM_CELLCOMBO_COLOR		3000
#define IDC_COM_CELLCOMBO_ITEM		4000
#define IDC_COM_CELLCOMBO_ITEM2		4001
#define IDC_CHK_SPEC_MIN_SFR			5000
#define IDC_CHK_SPEC_MAX_SFR			6000

IMPLEMENT_DYNAMIC(CList_TiltSFROp, CList_Cfg_VIBase)

CList_TiltSFROp::CList_TiltSFROp()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
// 	VERIFY(m_Font.CreateFont(
// 		12,						// nHeight
// 		0,						// nWidth
// 		0,						// nEscapement
// 		0,						// nOrientation
// 		FW_BOLD,				// nWeight
// 		FALSE,					// bItalic
// 		FALSE,					// bUnderline
// 		0,						// cStrikeOut
// 		ANSI_CHARSET,			// nCharSet
// 		OUT_DEFAULT_PRECIS,		// nOutPrecision
// 		CLIP_DEFAULT_PRECIS,	// nClipPrecision
// 		ANTIALIASED_QUALITY,	// nQuality
// 		FIXED_PITCH,			// nPitchAndFamily
// 		_T("SimHei")));		// lpszFacename
	m_nEditCol = 0;
	m_nEditRow = 0;
}

CList_TiltSFROp::~CList_TiltSFROp()
{
	m_Font.DeleteObject();
}
BEGIN_MESSAGE_MAP(CList_TiltSFROp, CList_Cfg_VIBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT	(NM_CLICK,					&CList_TiltSFROp::OnNMClick					)
	ON_NOTIFY_REFLECT	(NM_DBLCLK,					&CList_TiltSFROp::OnNMDblclk				)
	ON_EN_KILLFOCUS		(IDC_EDT_CELLEDIT,			&CList_TiltSFROp::OnEnKillFocusEdit			)
	ON_CBN_KILLFOCUS	(IDC_COM_CELLCOMBO_ITEM,	&CList_TiltSFROp::OnEnKillFocusComboItem	)
	ON_CBN_KILLFOCUS	(IDC_COM_CELLCOMBO_ITEM2,	&CList_TiltSFROp::OnEnKillFocusComboItem2	)
	ON_CBN_SELCHANGE	(IDC_COM_CELLCOMBO_ITEM,	&CList_TiltSFROp::OnEnSelectComboItem		)	
	ON_CBN_SELCHANGE	(IDC_COM_CELLCOMBO_ITEM2,	&CList_TiltSFROp::OnEnSelectComboItem2		)	
	ON_COMMAND_RANGE	(IDC_CHK_SPEC_MIN_SFR, IDC_CHK_SPEC_MIN_SFR + 999, OnRangeCheckBtnMinCtrl)
	ON_COMMAND_RANGE	(IDC_CHK_SPEC_MAX_SFR, IDC_CHK_SPEC_MAX_SFR + 999, OnRangeCheckBtnMaxCtrl)
	ON_WM_MOUSEWHEEL()
	ON_WM_VSCROLL()
END_MESSAGE_MAP()

// CList_TiltSFROp 메시지 처리기입니다.
int CList_TiltSFROp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CList_Cfg_VIBase::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	SetFont(&m_Font);
	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT |  LVS_EX_CHECKBOXES);

	InitHeader(); 
	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER, CRect(0, 0, 0, 0), this, IDC_EDT_CELLEDIT);
	m_ed_CellEdit.SetValidChars(_T("0123456789."));

	this->GetHeaderCtrl()->EnableWindow(FALSE);
	
	for (int t = 0; t < 2; t++)
	{
		m_cb_Item[t].Create(WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | CBS_DROPDOWNLIST, CRect(0, 0, 0, 0), this, IDC_COM_CELLCOMBO_ITEM+t);
		m_cb_Item[t].ResetContent();

		for (UINT nIdex = 0; nIdex < ROI_SFR_GROUP_Max; nIdex++)
		{
			CString str;
			//str.Format(_T("%d"), nIdex);
			str = g_szSFRGroup[nIdex];
			m_cb_Item[t].InsertString(nIdex, str);
		}
		m_cb_Item[t].SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
	}
	
	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_TiltSFROp::OnSize(UINT nType, int cx, int cy)
{
	CList_Cfg_VIBase::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_TiltSFROp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CList_Cfg_VIBase::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_TiltSFROp::InitHeader()
{
	for (int nCol = 0; nCol < TiltSfOp_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_SFROp[nCol], iListAglin_SFROp[nCol], iHeaderWidth_SFROp[nCol]);
	}

	for (int nCol = 0; nCol < TiltSfOp_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_SFROp[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 17:25
// Desc.		:
//=============================================================================
void CList_TiltSFROp::InsertFullData()
{
	if ( NULL == m_pstConfigInfo)
		return;

 	DeleteAllItems();
 
	for (UINT nIdx = 0; nIdx < TiltSfOp_ItemNum; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
 	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/6/26 - 14:26
// Desc.		:
//=============================================================================
void CList_TiltSFROp::SetRectRow(UINT nRow)
{
	if (NULL == m_pstConfigInfo)
		return;

	CString strValue;

	strValue.Format(_T("Tilt %d"), nRow);
	SetItemText(nRow, TiltSfOp_Object, strValue);
	//!SH _111828: DATA 연결 부분 
	if (m_pstConfigInfo->stInput_Tilt[nRow].bEnable == TRUE)
		SetItemState(nRow, 0x2000, LVIS_STATEIMAGEMASK);
	else
		SetItemState(nRow, 0x1000, LVIS_STATEIMAGEMASK);

	//strValue.Format(_T("%d"), m_pstConfigInfo->stInput_Tilt[nRow].nStandardGroup);
	strValue = g_szSFRGroup[m_pstConfigInfo->stInput_Tilt[nRow].nStandardGroup];
	SetItemText(nRow, TiltSfOp_StardRoi, strValue);

	//strValue.Format(_T("%d"), m_pstConfigInfo->stInput_Tilt[nRow].nSubtractGroup);
	strValue = g_szSFRGroup[m_pstConfigInfo->stInput_Tilt[nRow].nSubtractGroup];
	SetItemText(nRow, TiltSfOp_CompareRoi, strValue);

	strValue.Format(_T("%.2f"), m_pstConfigInfo->stInput_Tilt[nRow].dbValue);
	SetItemText(nRow, TiltSfOp_Spec, strValue);


// 
// 	strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.Width());
// 	SetItemText(nRow, TiltSfOp_Width, strValue);
// 
// 	strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.Height());
// 	SetItemText(nRow, TiltSfOp_Height, strValue);
// 
// 	strValue.Format(_T("%.2f"), m_pstConfigInfo->stSpec_Min[nRow].dbValue);
// 	SetItemText(nRow, TiltSfOp_MinSpec, strValue);
// 
// 	strValue.Format(_T("%.2f"), m_pstConfigInfo->stSpec_Max[nRow].dbValue);
// 	SetItemText(nRow, TiltSfOp_MaxSpec, strValue);
// 
// 	strValue.Format(_T("%.2f"), m_pstConfigInfo->stRegion[nRow].dbOffset);
// 	SetItemText(nRow, TiltSfOp_Offset, strValue);
// 
// 	strValue.Format(_T("%.2f"), m_pstConfigInfo->stRegion[nRow].dbLinePair);
// 	SetItemText(nRow, TiltSfOp_Linepare, strValue);
// 
// 	strValue.Format(_T("%s"), g_szItemPos[m_pstConfigInfo->stRegion[nRow].nItemPos]);
// 	SetItemText(nRow, TiltSfOp_ItemPos, strValue);

}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_TiltSFROp::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	LVHITTESTINFO HitInfo;
	HitInfo.pt = pNMItemActivate->ptAction;
	
	HitTest(&HitInfo);

	// Check Ctrl Event
	if (HitInfo.flags == LVHT_ONITEMSTATEICON)
	{
		UINT nBuffer;

		nBuffer = GetItemState(pNMItemActivate->iItem, LVIS_STATEIMAGEMASK);
		//!SH _111828: DATA 연결 부분 
		if (nBuffer == 0x2000)
			m_pstConfigInfo->stInput_Tilt[pNMItemActivate->iItem].bEnable = FALSE;

		if (nBuffer == 0x1000)
			m_pstConfigInfo->stInput_Tilt[pNMItemActivate->iItem].bEnable = TRUE;
	}

	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_TiltSFROp::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem == TiltSfOp_Object)
		{
			LVHITTESTINFO HitInfo;
			HitInfo.pt = pNMItemActivate->ptAction;

			HitTest(&HitInfo);

			// Check Ctrl Event
			if (HitInfo.flags == LVHT_ONITEMSTATEICON)
			{
				UINT nBuffer;

				nBuffer = GetItemState(pNMItemActivate->iItem, LVIS_STATEIMAGEMASK);

				//!SH _111828: DATA 연결 부분 
				if (nBuffer == 0x2000)
					m_pstConfigInfo->stInput_Tilt[pNMItemActivate->iItem].bEnable = FALSE;

				if (nBuffer == 0x1000)
					m_pstConfigInfo->stInput_Tilt[pNMItemActivate->iItem].bEnable = TRUE;
			}
		}

		if (pNMItemActivate->iSubItem < TiltSfOp_MaxCol && pNMItemActivate->iSubItem > 0)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);

			
			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

			if (pNMItemActivate->iSubItem == TiltSfOp_StardRoi || pNMItemActivate->iSubItem == TiltSfOp_CompareRoi)
			{
				CString str;
				for (int t = 0; t < m_cb_Item[pNMItemActivate->iSubItem - 1].GetCount(); t++)
				{
					m_cb_Item[pNMItemActivate->iSubItem - 1].GetLBText(t, str);
					if (str == GetItemText(m_nEditRow, m_nEditCol))
					{
						m_cb_Item[pNMItemActivate->iSubItem - 1].SetCurSel(t);
						break;
					}
				}


				m_cb_Item[pNMItemActivate->iSubItem-1].SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_cb_Item[pNMItemActivate->iSubItem-1].SetFocus();
			}
			else
			{
				m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_ed_CellEdit.SetFocus();
			}
		}
		GetOwner()->SendNotifyMessage(m_nMessage, 0, 0);
	}

	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
void CList_TiltSFROp::OnEnKillFocusEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	if (m_nEditCol == TiltSfOp_Spec)
	{
		UpdateCelldbData(m_nEditRow, m_nEditCol, _ttof(strText));
	}
	else
	{
	}

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

	m_nEditCol = -1;
	m_nEditRow = -1;
	ModifyStyle(WS_VSCROLL, 1);

}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
BOOL CList_TiltSFROp::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	if ( NULL == m_pstConfigInfo)
		return FALSE;

	if (iValue < 0)
		iValue = 0;

	//!SH _111828: DATA 연결 부분 
	//CRect rtTemp = m_pstConfigInfo->stRegion[nRow].rtROI;

// 	switch (nCol)
// 	{
// 	case TiltSfOp_PosX:
// 		m_pstConfigInfo->stRegion[nRow].RectPosXY(iValue, rtTemp.CenterPoint().y);
// 		break;
// 	case TiltSfOp_PosY:
// 		m_pstConfigInfo->stRegion[nRow].RectPosXY(rtTemp.CenterPoint().x, iValue);
// 		break;
// 	case TiltSfOp_Width:
// 		m_pstConfigInfo->stRegion[nRow].RectPosWH(iValue, rtTemp.Height());
// 		break;
// 	case TiltSfOp_Height:
// 		m_pstConfigInfo->stRegion[nRow].RectPosWH(rtTemp.Width(), iValue);
// 		break;
// 	default:
// 		break;
// 	}
// 
// 	if (m_pstConfigInfo->stRegion[nRow].rtROI.left < 0)
// 	{
// 		CRect rtTemp = m_pstConfigInfo->stRegion[nRow].rtROI;
// 
// 		m_pstConfigInfo->stRegion[nRow].rtROI.left = 0;
// 		m_pstConfigInfo->stRegion[nRow].rtROI.right = m_pstConfigInfo->stRegion[nRow].rtROI.left + rtTemp.Width();
// 	}
// 
// 	if (m_pstConfigInfo->stRegion[nRow].rtROI.right >(LONG)m_dwImage_Width)
// 	{
// 		CRect rtTemp = m_pstConfigInfo->stRegion[nRow].rtROI;
// 
// 		m_pstConfigInfo->stRegion[nRow].rtROI.right = (LONG)m_dwImage_Width;
// 		m_pstConfigInfo->stRegion[nRow].rtROI.left = m_pstConfigInfo->stRegion[nRow].rtROI.right - rtTemp.Width();
// 	}
// 
// 	if (m_pstConfigInfo->stRegion[nRow].rtROI.top < 0)
// 	{
// 		CRect rtTemp = m_pstConfigInfo->stRegion[nRow].rtROI;
// 
// 		m_pstConfigInfo->stRegion[nRow].rtROI.top = 0;
// 		m_pstConfigInfo->stRegion[nRow].rtROI.bottom = rtTemp.Height() + m_pstConfigInfo->stRegion[nRow].rtROI.top;
// 	}
// 
// 	if (m_pstConfigInfo->stRegion[nRow].rtROI.bottom >(LONG)m_dwImage_Height)
// 	{
// 		CRect rtTemp = m_pstConfigInfo->stRegion[nRow].rtROI;
// 
// 		m_pstConfigInfo->stRegion[nRow].rtROI.bottom = (LONG)m_dwImage_Height;
// 		m_pstConfigInfo->stRegion[nRow].rtROI.top = m_pstConfigInfo->stRegion[nRow].rtROI.bottom - rtTemp.Height();
// 	}
// 
// 	if (m_pstConfigInfo->stRegion[nRow].rtROI.Height() <= 0)
// 		m_pstConfigInfo->stRegion[nRow].RectPosWH(m_pstConfigInfo->stRegion[nRow].rtROI.Width(), 1);
// 
// 	if (m_pstConfigInfo->stRegion[nRow].rtROI.Height() >= (LONG)m_dwImage_Height)
// 		m_pstConfigInfo->stRegion[nRow].RectPosWH(m_pstConfigInfo->stRegion[nRow].rtROI.Width(), m_dwImage_Height);
// 
// 	if (m_pstConfigInfo->stRegion[nRow].rtROI.Width() <= 0)
// 		m_pstConfigInfo->stRegion[nRow].RectPosWH(1, m_pstConfigInfo->stRegion[nRow].rtROI.Height());
// 
// 	if (m_pstConfigInfo->stRegion[nRow].rtROI.Width() >= (LONG)m_dwImage_Width)
// 		m_pstConfigInfo->stRegion[nRow].RectPosWH(m_dwImage_Width, m_pstConfigInfo->stRegion[nRow].rtROI.Height());
// 
// // 	CString strValue;
// // 
// // 	switch (nCol)
// // 	{
// // 	case TiltSfOp_PosX:
// // 		strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.CenterPoint().x);
// // 		break;
// // 	case TiltSfOp_PosY:
// // 		strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.CenterPoint().y);
// // 		break;
// // 	case TiltSfOp_Width:
// // 		strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.Width());
// // 		break;
// // 	case TiltSfOp_Height:
// // 		strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.Height());
// // 		break;
// // 	default:
// // 		break;
// // 	}
// 
// 	m_ed_CellEdit.SetWindowText(strValue);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: UpdateCelldbData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dbValue
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_TiltSFROp::UpdateCelldbData(UINT nRow, UINT nCol, double dbValue)
{
 	switch (nCol)
 	{
 	case TiltSfOp_Spec:
		//!SH _111828: DATA 연결 부분 
 		m_pstConfigInfo->stInput_Tilt[nRow].dbValue = dbValue;
 		break;
 	default:
 		break;
 	}

	CString str;
	str.Format(_T("%.2f"), dbValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_TiltSFROp::GetCellData()
{
	if ( NULL == m_pstConfigInfo)
		return;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_TiltSFROp::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{

 	if (m_ed_CellEdit.IsWindowVisible())
 	{
 		CWnd* pWndFocus = GetFocus();
 		if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
 		{
 			if (m_nEditCol == -1 || m_nEditRow == -1)
 			{
 				return CList_Cfg_VIBase::OnMouseWheel(nFlags, zDelta, pt);
 			}
 			CString strText;
 			m_ed_CellEdit.GetWindowText(strText);
 
 			int iValue = _ttoi(strText);
 			double dbValue = _ttof(strText);
 
 			if (0 < zDelta)
 			{
 				iValue = iValue + ((zDelta / 120));
 				dbValue = dbValue + ((zDelta / 120) * 0.01);
 			}
 			else
 			{
 				if (0 < iValue)
 					iValue = iValue + ((zDelta / 120));
 
 				if (0 < dbValue)
 					dbValue = dbValue + ((zDelta / 120) * 0.01);
 			}
 
 			if (iValue < 0)
 				iValue = 0;
 
 			if (iValue > 2000)
 				iValue = 2000;
 
 			if (dbValue < 0.0)
 				dbValue = 0.0;
 
			if (m_nEditCol == TiltSfOp_Spec)
			{
				UpdateCelldbData(m_nEditRow, m_nEditCol, dbValue);
			}
 		}
 	}
 	

	return CList_Cfg_VIBase::OnMouseWheel(nFlags, zDelta, pt);
}

//=============================================================================
// Method		: OnRangeCheckBtnMinCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2018/2/20 - 9:32
// Desc.		:
//=============================================================================
void CList_TiltSFROp::OnRangeCheckBtnMinCtrl(UINT nID)
{
// 	UINT nIndex = nID - IDC_CHK_SPEC_MIN_SFR;
// 	
// 	if (BST_CHECKED == m_chk_SpecMin[nIndex].GetCheck())
// 	{
// 		m_pstConfigInfo->stSpec_Min[nIndex].bEnable = TRUE;
// 	}
// 	else
// 	{
// 		m_pstConfigInfo->stSpec_Min[nIndex].bEnable = FALSE;
// 	}
}

//=============================================================================
// Method		: OnRangeCheckBtnMaxCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2018/2/20 - 9:32
// Desc.		:
//=============================================================================
void CList_TiltSFROp::OnRangeCheckBtnMaxCtrl(UINT nID)
{
// 	UINT nIndex = nID - IDC_CHK_SPEC_MAX_SFR;
// 
// 	if (BST_CHECKED == m_chk_SpecMax[nIndex].GetCheck())
// 	{
// 		m_pstConfigInfo->stSpec_Max[nIndex].bEnable = TRUE;
// 	}
// 	else
// 	{
// 		m_pstConfigInfo->stSpec_Max[nIndex].bEnable = FALSE;
// 	}
}

//=============================================================================
// Method		: OnEnKillFocusComboItem
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/2 - 11:36
// Desc.		:
//=============================================================================
void CList_TiltSFROp::OnEnKillFocusComboItem()
{
	CString szData;

	m_cb_Item[TiltSfOp_StardRoi - 1].GetLBText(m_cb_Item[TiltSfOp_StardRoi - 1].GetCurSel(), szData);

	SetItemText(m_nEditRow, TiltSfOp_StardRoi, szData);

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());
	m_nEditCol = -1;
	m_nEditRow = -1;


	m_cb_Item[TiltSfOp_StardRoi-1].SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: OnEnSelectComboItem
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/2 - 11:36
// Desc.		:
//=============================================================================
void CList_TiltSFROp::OnEnSelectComboItem()
{
	//!SH _111828: DATA 연결 부분 
	m_pstConfigInfo->stInput_Tilt[m_nEditRow].nStandardGroup = m_cb_Item[TiltSfOp_StardRoi-1].GetCurSel();

	CString szData;

	m_cb_Item[TiltSfOp_StardRoi - 1].GetLBText(m_cb_Item[TiltSfOp_StardRoi - 1].GetCurSel(), szData);

	SetItemText(m_nEditRow, TiltSfOp_StardRoi, szData);
	m_cb_Item[TiltSfOp_StardRoi - 1].SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
	
	m_nEditCol = -1;
	m_nEditRow = -1;

}
void CList_TiltSFROp::OnEnKillFocusComboItem2()
{
	CString szData;

	m_cb_Item[TiltSfOp_CompareRoi - 1].GetLBText(m_cb_Item[TiltSfOp_CompareRoi - 1].GetCurSel(), szData);

	SetItemText(m_nEditRow, TiltSfOp_CompareRoi, szData);

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());
	m_nEditCol = -1;
	m_nEditRow = -1;


	m_cb_Item[TiltSfOp_CompareRoi - 1].SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: OnEnSelectComboItem
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/2 - 11:36
// Desc.		:
//=============================================================================
void CList_TiltSFROp::OnEnSelectComboItem2()
{
	//!SH _111828: DATA 연결 부분 
	m_pstConfigInfo->stInput_Tilt[m_nEditRow].nSubtractGroup = m_cb_Item[TiltSfOp_CompareRoi - 1].GetCurSel();

	CString szData;

	m_cb_Item[TiltSfOp_CompareRoi - 1].GetLBText(m_cb_Item[TiltSfOp_CompareRoi - 1].GetCurSel(), szData);

	SetItemText(m_nEditRow, TiltSfOp_CompareRoi, szData);
	m_cb_Item[TiltSfOp_CompareRoi - 1].SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

	m_nEditCol = -1;
	m_nEditRow = -1;

}

void CList_TiltSFROp::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	//
	if (m_ed_CellEdit.IsWindowVisible())
		OnEnKillFocusEdit();

	if (m_cb_Item[TiltSfOp_StardRoi - 1].IsWindowVisible())
		OnEnKillFocusComboItem();


	CList_Cfg_VIBase::OnVScroll(nSBCode, nPos, pScrollBar);

}
void CList_TiltSFROp::SetLanguageMode(UINT nLanguageMode){


	LPCTSTR* pszText = NULL;

	switch (nLanguageMode)
	{
	case 0:
		pszText = g_lpszHeader_SFROp;
		break;
	case 1:
		pszText = g_lpszHeader_SFROp_Chn;
		break;
	default:
		break;
	}

	for (int nCol = 0; nCol < TiltSfOp_MaxCol; nCol++)
	{
		DeleteColumn(nCol);
		InsertColumn(nCol, pszText[nCol], iListAglin_SFROp[nCol], iHeaderWidth_SFROp[nCol]);
	}
	
	InsertFullData();


}
