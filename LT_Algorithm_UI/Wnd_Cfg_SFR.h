﻿#ifndef Wnd_Cfg_SFR_h__
#define Wnd_Cfg_SFR_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_T_SFR.h"
#include "Def_TestItem_VI.h"
#include "List_SFROp.h"
#include "List_GroupSFROp.h"
#include "List_TiltSFROp.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_SFR
//-----------------------------------------------------------------------------
class CWnd_Cfg_SFR : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_SFR)

public:
	CWnd_Cfg_SFR();
	virtual ~CWnd_Cfg_SFR();

	enum enSFRStatic
	{
		STI_SFR_PARAM = 0,
		STI_SFR_CAPTURE_CNT,
		STI_SFR_DELAY,
		STI_SFR_Result,
		STI_SFR_SPEC,
		STI_SFR_SPEC_2,
		STI_SFR_ROI,
		//STI_SFR_X1_Tilt_A,
		//STI_SFR_X1_Tilt_B,
		//STI_SFR_X2_Tilt_A,
		//STI_SFR_X2_Tilt_B,
		//STI_SFR_Y1_Tilt_A,
		//STI_SFR_Y1_Tilt_B,
		//STI_SFR_Y2_Tilt_A,
		//STI_SFR_Y2_Tilt_B,
		STI_SFR_MAX,
	};

	enum enSFRButton
	{
		BTN_SFR_RESET = 0,
		BTN_SFR_TEST,
		BTN_SFR_MAX,
	};

	enum enSFRComobox
	{
		CMB_SFR_ResultMODE,
		//CMB_SFR_X1_Tilt_A,
		//CMB_SFR_X1_Tilt_B,
		//CMB_SFR_X2_Tilt_A,
		//CMB_SFR_X2_Tilt_B,
		//CMB_SFR_Y1_Tilt_A,
		//CMB_SFR_Y1_Tilt_B,
		//CMB_SFR_Y2_Tilt_A,
		//CMB_SFR_Y2_Tilt_B,
		CMB_SFR_MAX,
	};

	enum enSFREdit
	{
		EDT_SFR_CAPTURE_CNT = 0,
		EDT_SFR_DELAY,
		EDT_SFR_MAX,
	};

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	CFont				m_font;

	CList_SFROp			m_List;
	CList_GroupSFROp	m_List_Group;
	CList_TiltSFROp		m_List_Tilt;


	CVGStatic			m_st_Item[STI_SFR_MAX];
	CButton				m_bn_Item[BTN_SFR_MAX];
	CComboBox			m_cb_Item[CMB_SFR_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_SFR_MAX];

	CVGStatic	  		m_st_CapItem;
	CVGStatic	  		m_st_CapSpecMin;
	CVGStatic	  		m_st_CapSpecMax;

	//CVGStatic	  		m_st_Spec[ITM_SFR_Max];
	//CMFCMaskedEdit		m_ed_SpecMin[ITM_SFR_Max];
	//CMFCMaskedEdit		m_ed_SpecMax[ITM_SFR_Max];
	//CMFCButton	  		m_chk_SpecMin[ITM_SFR_Max];
	//CMFCButton	  		m_chk_SpecMax[ITM_SFR_Max];

	//CVGStatic	  		m_st_TiltSpec[ITM_SFR_TiltMax];
	//CMFCMaskedEdit		m_ed_TiltSpecMin[ITM_SFR_TiltMax];
	//CMFCMaskedEdit		m_ed_TiltSpecMax[ITM_SFR_TiltMax];
	//CMFCButton	  		m_chk_TiltSpecMin[ITM_SFR_TiltMax];
	//CMFCButton	  		m_chk_TiltSpecMax[ITM_SFR_TiltMax];

	ST_SFR_Opt*			m_pstConfigInfo = NULL;

	UINT				m_nOvrNum;

public:

	void	SetPtr_RecipeInfo(ST_SFR_Opt* pstConfigInfo)
	{
		if (pstConfigInfo == NULL)
			return;

		m_pstConfigInfo = pstConfigInfo;
	};

	void	SetOvr_MessageNum(UINT nOvrNum = 0)
	{
		m_nOvrNum = nOvrNum;
	};

	void	SetUpdateData();
	void	GetUpdateData();
};
#endif // Wnd_Cfg_SFR_h__
