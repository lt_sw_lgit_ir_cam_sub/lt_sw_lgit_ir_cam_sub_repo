﻿#ifndef Wnd_Cfg_Torque_h__
#define Wnd_Cfg_Torque_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_T_Torque.h"
#include "Def_TestItem_VI.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_Torque
//-----------------------------------------------------------------------------
class CWnd_Cfg_Torque : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_Torque)

public:
	CWnd_Cfg_Torque();
	virtual ~CWnd_Cfg_Torque();

	enum enTorqueStatic
	{
		STI_TOR_SPEC = 0,
		STI_TOR_MAX,
	};

	enum enTorqueButton
	{
		BTN_TOR_MAX = 1,
	};

	enum enTorqueComobox
	{
		CMB_TOR_MAX = 1,
	};

	enum enTorqueEdit
	{
		EDT_TOR_MAX = 1,
	};

protected:
	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	CFont						m_font;

	CVGStatic					m_st_Item[STI_TOR_MAX];
	CButton						m_bn_Item[BTN_TOR_MAX];
	CComboBox					m_cb_Item[CMB_TOR_MAX];
	CMFCMaskedEdit				m_ed_Item[EDT_TOR_MAX];

	CVGStatic					m_st_CapItem;
	CVGStatic					m_st_CapSpecMin;
	CVGStatic					m_st_CapSpecMax;

	CVGStatic					m_st_Spec[Spec_Tor_Max];
	CMFCMaskedEdit				m_ed_SpecMin[Spec_Tor_Max];
	CMFCMaskedEdit				m_ed_SpecMax[Spec_Tor_Max];
	CMFCButton					m_chk_SpecMin[Spec_Tor_Max];
	CMFCButton					m_chk_SpecMax[Spec_Tor_Max];

	ST_Torque_Opt*			m_pstConfigInfo = NULL;

public:

	void	SetPtr_RecipeInfo(__in ST_Torque_Opt* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstConfigInfo = pstRecipeInfo;
	};

	void	SetUpdateData		();
	void	GetUpdateData		();
};

#endif // Wnd_Torque_h__
