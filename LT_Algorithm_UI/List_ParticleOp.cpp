﻿
// List_ParticleOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_ParticleOp.h"

typedef enum enListNum_ParOp
{
	ParOp_Object,
	ParOp_BruiseConc,
	ParOp_BruiseSize,
	ParOp_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader_ParOp[] =
{
	_T(""),
	_T("Conc"),
	_T("Size"),
	NULL
};

const int	iListAglin_ParOp[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_ParOp[] =
{
	200,
	200,
	200,
	200,
	200,
	200,
	200,
};

// CList_ParticleOp

#define IDC_EDT_CELLEDIT			1000
#define IDC_COM_CELLCOMBO_TYPE		2000
#define IDC_COM_CELLCOMBO_COLOR		3000
#define IDC_CHK_SPEC_MIN			4000
#define IDC_CHK_SPEC_MAX			5000

IMPLEMENT_DYNAMIC(CList_ParticleOp, CList_Cfg_VIBase)

CList_ParticleOp::CList_ParticleOp()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol = 0;
	m_nEditRow = 0; 

	m_pstConfigInfo = NULL;
}

CList_ParticleOp::~CList_ParticleOp()
{
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CList_ParticleOp, CList_Cfg_VIBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_MOUSEWHEEL()
	ON_NOTIFY_REFLECT(NM_CLICK,			&CList_ParticleOp::OnNMClick			)
	ON_NOTIFY_REFLECT(NM_DBLCLK,		&CList_ParticleOp::OnNMDblclk			)
	ON_EN_KILLFOCUS(IDC_EDT_CELLEDIT,	&CList_ParticleOp::OnEnKillFocusEdit	)
END_MESSAGE_MAP()

// CList_ParticleOp 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/3/12 - 19:05
// Desc.		:
//=============================================================================
int CList_ParticleOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CList_Cfg_VIBase::OnCreate(lpCreateStruct) == -1)
		return -1;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	SetFont(&m_Font);
	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER | LVS_EX_CHECKBOXES);

	InitHeader();
	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER | ES_NUMBER, CRect(0, 0, 0, 0), this, IDC_EDT_CELLEDIT);

	this->GetHeaderCtrl()->EnableWindow(FALSE);
	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/3/12 - 19:05
// Desc.		:
//=============================================================================
void CList_ParticleOp::OnSize(UINT nType, int cx, int cy)
{
	CList_Cfg_VIBase::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/3/12 - 19:06
// Desc.		:
//=============================================================================
BOOL CList_ParticleOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT;

	return CList_Cfg_VIBase::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/12 - 18:56
// Desc.		:
//=============================================================================
void CList_ParticleOp::InitHeader()
{
	for (int nCol = 0; nCol < ParOp_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_ParOp[nCol], iListAglin_ParOp[nCol], iHeaderWidth_ParOp[nCol]);
	}

	for (int nCol = 0; nCol < ParOp_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_ParOp[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/12 - 19:07
// Desc.		:
//=============================================================================
void CList_ParticleOp::InsertFullData()
{
	if (NULL == m_pstConfigInfo)
		return;

	DeleteAllItems();

	for (UINT nIdx = 0; nIdx < ParOp_ItemNum; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/3/12 - 19:10
// Desc.		:
//=============================================================================
void CList_ParticleOp::SetRectRow(UINT nRow)
{
	if (NULL == m_pstConfigInfo)
		return;

	CString strValue;

	strValue.Format(_T("%s"), g_szROI_Particle[nRow]);
	SetItemText(nRow, ParOp_Object, strValue);

	if (m_pstConfigInfo->stRegion[nRow].bEllipse == TRUE)
		SetItemState(nRow, 0x2000, LVIS_STATEIMAGEMASK);
	else
		SetItemState(nRow, 0x1000, LVIS_STATEIMAGEMASK);

// 	strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.CenterPoint().x);
// 	SetItemText(nRow, ParOp_PosX, strValue);
// 
// 	strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.CenterPoint().y);
// 	SetItemText(nRow, ParOp_PosY, strValue);
// 
// 	strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.Width());
// 	SetItemText(nRow, ParOp_Width, strValue);
// 
// 	strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.Height());
// 	SetItemText(nRow, ParOp_Height, strValue);

	strValue.Format(_T("%.2f"), m_pstConfigInfo->stRegion[nRow].dbBruiseConc);
	SetItemText(nRow, ParOp_BruiseConc, strValue);

	strValue.Format(_T("%.2f"), m_pstConfigInfo->stRegion[nRow].dbBruiseSize);
	SetItemText(nRow, ParOp_BruiseSize, strValue);
}

//=============================================================================
// Method		: OnNMClick
// Access		: protected  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/3/12 - 19:11
// Desc.		:
//=============================================================================
void CList_ParticleOp::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	LVHITTESTINFO HitInfo;
	HitInfo.pt = pNMItemActivate->ptAction;

	HitTest(&HitInfo);

	// Check Ctrl Event
	if (HitInfo.flags == LVHT_ONITEMSTATEICON)
	{
		UINT nBuffer;

		nBuffer = GetItemState(pNMItemActivate->iItem, LVIS_STATEIMAGEMASK);

		if (nBuffer == 0x2000)
			m_pstConfigInfo->stRegion[pNMItemActivate->iItem].bEllipse = FALSE;

		if (nBuffer == 0x1000)
			m_pstConfigInfo->stRegion[pNMItemActivate->iItem].bEllipse = TRUE;
	}

	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: protected  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/3/12 - 19:11
// Desc.		:
//=============================================================================
void CList_ParticleOp::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem == ParOp_Object)
		{
			LVHITTESTINFO HitInfo;
			HitInfo.pt = pNMItemActivate->ptAction;

			HitTest(&HitInfo);

			// Check Ctrl Event
			if (HitInfo.flags == LVHT_ONITEMSTATEICON)
			{
				UINT nBuffer;

				nBuffer = GetItemState(pNMItemActivate->iItem, LVIS_STATEIMAGEMASK);

				if (nBuffer == 0x2000)
					 m_pstConfigInfo->stRegion[pNMItemActivate->iItem].bEllipse = FALSE;

				if (nBuffer == 0x1000)
					 m_pstConfigInfo->stRegion[pNMItemActivate->iItem].bEllipse = TRUE;
			}
		}

		if (pNMItemActivate->iSubItem < ParOp_MaxCol && pNMItemActivate->iSubItem > 0)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);

			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

			m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
			m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
			m_ed_CellEdit.SetFocus();
		}
	}

	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusEdit
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/12 - 19:11
// Desc.		:
//=============================================================================
void CList_ParticleOp::OnEnKillFocusEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	if (m_nEditCol == ParOp_BruiseConc || m_nEditCol == ParOp_BruiseSize)
	{
		UpdateCelldbData(m_nEditRow, m_nEditCol, _ttof(strText));
	}
	else
	{
		UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText));
	}

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: UpdateCellData
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/3/12 - 19:12
// Desc.		:
//=============================================================================
BOOL CList_ParticleOp::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
// 	if (m_pstConfigInfo == NULL)
// 		return FALSE;
// 
// 	if (iValue < 0)
// 		iValue = 0;
// 
// 	CRect rtTemp =  m_pstConfigInfo->stRegion[nRow].rtROI;
// 
// 	switch (nCol)
// 	{
// 	case ParOp_PosX:
// 		 m_pstConfigInfo->stRegion[nRow].RectPosXY(iValue, rtTemp.CenterPoint().y);
// 		break;
// 	case ParOp_PosY:
// 		 m_pstConfigInfo->stRegion[nRow].RectPosXY(rtTemp.CenterPoint().x, iValue);
// 		break;
// 	case ParOp_Width:
// 		 m_pstConfigInfo->stRegion[nRow].RectPosWH(iValue, rtTemp.Height());
// 		break;
// 	case ParOp_Height:
// 		 m_pstConfigInfo->stRegion[nRow].RectPosWH(rtTemp.Width(), iValue);
// 		break;
// 	default:
// 		break;
// 	}
// 
// 	if ( m_pstConfigInfo->stRegion[nRow].rtROI.left < 0)
// 	{
// 		CRect rtTemp =  m_pstConfigInfo->stRegion[nRow].rtROI;
// 
// 		 m_pstConfigInfo->stRegion[nRow].rtROI.left = 0;
// 		 m_pstConfigInfo->stRegion[nRow].rtROI.right =  m_pstConfigInfo->stRegion[nRow].rtROI.left + rtTemp.Width();
// 	}
// 
// 	if (m_pstConfigInfo->stRegion[nRow].rtROI.right > (LONG)m_dwImage_Width)
// 	{
// 		CRect rtTemp =  m_pstConfigInfo->stRegion[nRow].rtROI;
// 
// 		 m_pstConfigInfo->stRegion[nRow].rtROI.right = (LONG)m_dwImage_Width;
// 		 m_pstConfigInfo->stRegion[nRow].rtROI.left  = m_pstConfigInfo->stRegion[nRow].rtROI.right - rtTemp.Width();
// 	}
// 
// 	if ( m_pstConfigInfo->stRegion[nRow].rtROI.top < 0)
// 	{
// 		CRect rtTemp =  m_pstConfigInfo->stRegion[nRow].rtROI;
// 
// 		 m_pstConfigInfo->stRegion[nRow].rtROI.top = 0;
// 		 m_pstConfigInfo->stRegion[nRow].rtROI.bottom = rtTemp.Height() +  m_pstConfigInfo->stRegion[nRow].rtROI.top;
// 	}
// 
// 	if (m_pstConfigInfo->stRegion[nRow].rtROI.bottom >(LONG)m_dwImage_Height)
// 	{
// 		CRect rtTemp =  m_pstConfigInfo->stRegion[nRow].rtROI;
// 
// 		m_pstConfigInfo->stRegion[nRow].rtROI.bottom = (LONG)m_dwImage_Height;
// 		 m_pstConfigInfo->stRegion[nRow].rtROI.top =  m_pstConfigInfo->stRegion[nRow].rtROI.bottom - rtTemp.Height();
// 	}
// 
// 	if ( m_pstConfigInfo->stRegion[nRow].rtROI.Height() <= 0)
// 		 m_pstConfigInfo->stRegion[nRow].RectPosWH( m_pstConfigInfo->stRegion[nRow].rtROI.Width(), 1);
// 
// 	if (m_pstConfigInfo->stRegion[nRow].rtROI.Height() >= (LONG)m_dwImage_Height)
// 		 m_pstConfigInfo->stRegion[nRow].RectPosWH( m_pstConfigInfo->stRegion[nRow].rtROI.Width(), m_dwImage_Height);
// 
// 	if ( m_pstConfigInfo->stRegion[nRow].rtROI.Width() <= 0)
// 		 m_pstConfigInfo->stRegion[nRow].RectPosWH(1,  m_pstConfigInfo->stRegion[nRow].rtROI.Height());
// 
// 	if (m_pstConfigInfo->stRegion[nRow].rtROI.Width() >= (LONG)m_dwImage_Width)
// 		 m_pstConfigInfo->stRegion[nRow].RectPosWH(m_dwImage_Width,  m_pstConfigInfo->stRegion[nRow].rtROI.Height());
// 
// 	CString strValue;
// 
// 	switch (nCol)
// 	{
// 	case ParOp_PosX:
// 		strValue.Format(_T("%d"),  m_pstConfigInfo->stRegion[nRow].rtROI.CenterPoint().x);
// 		break;
// 	case ParOp_PosY:
// 		strValue.Format(_T("%d"),  m_pstConfigInfo->stRegion[nRow].rtROI.CenterPoint().y);
// 		break;
// 	case ParOp_Width:
// 		strValue.Format(_T("%d"),  m_pstConfigInfo->stRegion[nRow].rtROI.Width());
// 		break;
// 	case ParOp_Height:
// 		strValue.Format(_T("%d"),  m_pstConfigInfo->stRegion[nRow].rtROI.Height());
// 		break;
// 	default:
// 		break;
// 	}
// 
// 	m_ed_CellEdit.SetWindowText(strValue);
// 	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: UpdateCelldbData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dbValue
// Qualifier	:
// Last Update	: 2017/3/17 - 13:05
// Desc.		:
//=============================================================================
BOOL CList_ParticleOp::UpdateCelldbData(UINT nRow, UINT nCol, double dbValue)
{
	switch (nCol)
	{
	case ParOp_BruiseConc:
		 m_pstConfigInfo->stRegion[nRow].dbBruiseConc = dbValue;
		break;
	case ParOp_BruiseSize:
		m_pstConfigInfo->stRegion[nRow].dbBruiseSize = dbValue;
		break;
	default:
		break;
	}

	CString str;
	str.Format(_T("%.2f"), dbValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/12 - 19:13
// Desc.		:
//=============================================================================
void CList_ParticleOp::GetCellData()
{
	if (m_pstConfigInfo == NULL)
		return;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/3/17 - 14:48
// Desc.		:
//=============================================================================
BOOL CList_ParticleOp::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue		= _ttoi(strText);
		double dbValue  = _ttof(strText);

		if (0 < zDelta)
		{
			iValue = iValue + ((zDelta / 120));
			dbValue = dbValue + ((zDelta / 120) * 0.01);
		}
		else
		{
			if (0 < iValue)
				iValue = iValue + ((zDelta / 120));

			if (0 < dbValue)
				dbValue = dbValue + ((zDelta / 120) * 0.01);
		}

		if (iValue < 0)
			iValue = 0;

		if (iValue > 2000)
			iValue = 2000;

		if (dbValue < 0.0)
			dbValue = 0.0;

		if (m_nEditCol == ParOp_BruiseConc || m_nEditCol == ParOp_BruiseSize)
		{
			UpdateCelldbData(m_nEditRow, m_nEditCol, dbValue);
		}
		else
		{
			UpdateCellData(m_nEditRow, m_nEditCol, iValue);
		}
	}

	return CList_Cfg_VIBase::OnMouseWheel(nFlags, zDelta, pt);
}
