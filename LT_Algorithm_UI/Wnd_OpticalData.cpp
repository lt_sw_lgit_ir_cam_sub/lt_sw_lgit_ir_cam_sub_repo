// Wnd_OpticalData.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_OpticalData.h"


// CWnd_OpticalData

IMPLEMENT_DYNAMIC(CWnd_OpticalData, CWnd)

CWnd_OpticalData::CWnd_OpticalData()
{

}

CWnd_OpticalData::~CWnd_OpticalData()
{
}


BEGIN_MESSAGE_MAP(CWnd_OpticalData, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()



// CWnd_OpticalData 메시지 처리기입니다.




int CWnd_OpticalData::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	DWORD dwStyle = WS_VISIBLE | WS_CHILD /*| WS_CLIPCHILDREN*/ | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.

 	for (int t = 0; t < (int)m_nHeaderCnt; t++)
 	{
 		m_st_Header[t].SetStaticStyle(CVGStatic::StaticStyle_Title);
 		m_st_Header[t].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Header[t].SetFont_Gdip(L"Arial", 12.0F);
		m_st_Header[t].Create(g_szOTData[t], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
 
 		for (int i= 0;i<enOTData_inputmax; i++)
 		{
 			m_st_Data[i][t].SetStaticStyle(CVGStatic::StaticStyle_Title);
 			m_st_Data[i][t].SetColorStyle(CVGStatic::ColorStyle_Default);
			m_st_Data[i][t].SetFont_Gdip(L"Arial", 12.0F);
 			m_st_Data[i][t].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
 		}
 	
 
 	}


	return 0;
}


void CWnd_OpticalData::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	int iMargin = 0;
	int iSpacing = 0;
	int iCateSpacing = 0;

	int iLeft = iMargin;
	int iTop = iMargin;
	int iWidth = cx - iMargin - iMargin;
	int iHeight = cy - iMargin - iMargin;

	int HeaderW = (iWidth+2) / 2;

	int stW = iWidth;
	int stH = (672 * iWidth) / 1280;

//	m_st_ResultView.MoveWindow(iLeft, iTop, stW, stH);


//	iTop += stH + 5;
	if (m_bMode == FALSE)
	{
		int HeaderH = iHeight / (10);

		for (UINT nIdx = 0; nIdx < m_nHeaderCnt; nIdx++)
		{
			iLeft = iMargin;
			m_st_Header[nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);
			iLeft += HeaderW - 1;
			m_st_Data[0][nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);
			//	iLeft += HeaderW/2 -1;
			// 		m_st_Data[1][nIdx].MoveWindow(iLeft, iTop, HeaderW / 2, HeaderH);
			iTop += (HeaderH - 1);
		}
	}
	else{
		int HeaderH = iHeight /2;

		for (UINT nIdx = 0; nIdx < m_nHeaderCnt; nIdx++)
		{
			if (enOTData_OffsetX <= nIdx)
			{
				iLeft = iMargin;
				m_st_Header[nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);
				iLeft += HeaderW - 1;
				m_st_Data[0][nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);
				//	iLeft += HeaderW/2 -1;
				// 		m_st_Data[1][nIdx].MoveWindow(iLeft, iTop, HeaderW / 2, HeaderH);
				iTop += (HeaderH - 1);
			}
			else{
				m_st_Header[nIdx].MoveWindow(0, 0, 0, 0);
				m_st_Data[0][nIdx].MoveWindow(0, 0, 0, 0);

			}
			
		}
	}
	
}


BOOL CWnd_OpticalData::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}


BOOL CWnd_OpticalData::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	return CWnd::OnEraseBkgnd(pDC);
}


void CWnd_OpticalData::DataSetting(UINT nDataIdx, UINT nIdx, BOOL MODE, double dData){

	if (MODE == TRUE)
	{
		m_st_Data[nDataIdx][nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
	}
	else{
		m_st_Data[nDataIdx][nIdx].SetColorStyle(CVGStatic::ColorStyle_Red);
	}

	CString strData;


	strData.Format(_T("%6.3f"), dData);

	m_st_Data[nDataIdx][nIdx].SetWindowText(strData);
}

void CWnd_OpticalData::DataEachReset(UINT nDataIdx, UINT nIdx){

	
	m_st_Data[nDataIdx][nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_Data[nDataIdx][nIdx].SetWindowText(_T(""));

}


void CWnd_OpticalData::DataReset(){

	for (UINT nIdx = 0; nIdx < m_nHeaderCnt; nIdx++)
	{
		m_st_Data[0][nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Data[0][nIdx].SetWindowText(_T(""));
	}

}

void CWnd_OpticalData::DataDisplay(ST_OpticalCenter_Data stData)
{
	CString strData;
	
	if (stData.nResultX == TRUE)
	{
		m_st_Data[enOTData_1st][enOTData_PosX].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Data[enOTData_1st][enOTData_OffsetX].SetColorStyle(CVGStatic::ColorStyle_Default);
	}
	else{
		m_st_Data[enOTData_1st][enOTData_PosX].SetColorStyle(CVGStatic::ColorStyle_Red);
		m_st_Data[enOTData_1st][enOTData_OffsetX].SetColorStyle(CVGStatic::ColorStyle_Red);
	}
	strData.Format(_T("%d"), stData.nResultPosX);
	m_st_Data[enOTData_1st][enOTData_PosX].SetWindowText(strData);
	strData.Format(_T("%d"), stData.iStandPosDevX);
	m_st_Data[enOTData_1st][enOTData_OffsetX].SetWindowText(strData);

	if (stData.nResultY == TRUE)
	{
		m_st_Data[enOTData_1st][enOTData_PosY].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Data[enOTData_1st][enOTData_OffsetY].SetColorStyle(CVGStatic::ColorStyle_Default);
	}
	else{
		m_st_Data[enOTData_1st][enOTData_PosY].SetColorStyle(CVGStatic::ColorStyle_Red);
		m_st_Data[enOTData_1st][enOTData_OffsetY].SetColorStyle(CVGStatic::ColorStyle_Red);
	}
	strData.Format(_T("%d"), stData.nResultPosY);
	m_st_Data[enOTData_1st][enOTData_PosY].SetWindowText(strData);
	strData.Format(_T("%d"), stData.iStandPosDevY);
	m_st_Data[enOTData_1st][enOTData_OffsetY].SetWindowText(strData);
}

//=============================================================================
// Method		: DetectDisplay
// Access		: public  
// Returns		: void
// Parameter	: ST_OpticalCenter_Data stData
// Qualifier	:
// Last Update	: 2018/3/9 - 16:39
// Desc.		:
//=============================================================================
void CWnd_OpticalData::DetectDisplay(ST_OpticalCenter_Data stData)
{
	CString strData;

	if (stData.nDetectResult == TRUE)
	{
		m_st_Data[enOTData_1st][enOTData_PosX].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Data[enOTData_1st][enOTData_PosY].SetColorStyle(CVGStatic::ColorStyle_Default);

		m_st_Data[enOTData_1st][enOTData_OffsetX].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Data[enOTData_1st][enOTData_OffsetY].SetColorStyle(CVGStatic::ColorStyle_Default);
	}
	else
	{
		m_st_Data[enOTData_1st][enOTData_PosX].SetColorStyle(CVGStatic::ColorStyle_Red);
		m_st_Data[enOTData_1st][enOTData_PosY].SetColorStyle(CVGStatic::ColorStyle_Red);

		m_st_Data[enOTData_1st][enOTData_OffsetX].SetColorStyle(CVGStatic::ColorStyle_Red);
		m_st_Data[enOTData_1st][enOTData_OffsetY].SetColorStyle(CVGStatic::ColorStyle_Red);
	}

	strData.Format(_T("%d"), stData.nResultPosX);
	m_st_Data[enOTData_1st][enOTData_PosX].SetWindowText(strData);

	strData.Format(_T("%d"), stData.iStandPosDevX);
	m_st_Data[enOTData_1st][enOTData_OffsetX].SetWindowText(strData);

	strData.Format(_T("%d"), stData.nResultPosY);
	m_st_Data[enOTData_1st][enOTData_PosY].SetWindowText(strData);

	strData.Format(_T("%d"), stData.iStandPosDevY);
	m_st_Data[enOTData_1st][enOTData_OffsetY].SetWindowText(strData);
}
