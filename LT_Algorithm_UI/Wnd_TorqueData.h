#ifndef Wnd_TorqueData_h__
#define Wnd_TorqueData_h__

#pragma once

#include "afxwin.h"

#include "VGStatic.h"
#include "Def_T_Torque.h"

// CWnd_TorqueData
typedef enum
{
	enTorData_Header_A	= 0,
	enTorData_Header_B,
	enTorData_Header_C,
	enTorData_Header_D,
	enTorData_MAX,

}enumHeaderTorData;

typedef enum
{
	enTorData_A = 0,
	enTorData_B,
	enTorData_C,
	enTorData_D,
	enTorData_inputmax,

}enumDataTorData;

static	LPCTSTR	g_szTorData[] =
{
	_T("Torque A "),
	_T("Torque B "),
	_T("Torque C "),
	_T("Torque D "),
	NULL
};

class CWnd_TorqueData : public CWnd
{
	DECLARE_DYNAMIC(CWnd_TorqueData)

public:
	CWnd_TorqueData();
	virtual ~CWnd_TorqueData();

	UINT m_nHeaderCnt = enTorData_MAX;
	CString m_SzHeaderName[enTorData_MAX];

	//---Class 정의 시 세팅 해야함.
	void SetHeader(UINT nHeaderCnt)
	{

		if (nHeaderCnt > enTorData_MAX)
		{
			nHeaderCnt = enTorData_MAX;
		}
		m_nHeaderCnt = nHeaderCnt;
	};

	void SetHeaderName(UINT nCnt, CString szHeader)
	{
		if (nCnt > enTorData_MAX)
		{
			nCnt = enTorData_MAX - 1;
		}
		m_SzHeaderName[nCnt] = szHeader;
	};

	CVGStatic		m_st_Header[enTorData_MAX];
	CVGStatic		m_st_Data[enTorData_inputmax];

protected:
	DECLARE_MESSAGE_MAP()

public:
	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);
	afx_msg BOOL	OnEraseBkgnd			(CDC* pDC);

	void DataSetting			(UINT nDataIdx, UINT nIdx, BOOL MODE, double dData);
	void DataEachReset			(UINT nDataIdx, UINT nIdx);
	void DataReset				();
	void DataDisplay			(ST_Torque_Data stData);
};

#endif // Wnd_TorqueData_h__
