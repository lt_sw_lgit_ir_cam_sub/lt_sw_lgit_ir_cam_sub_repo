﻿// List_TiltSFROp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_GroupSFROp.h"

// CList_TiltSFROp

typedef enum enListNum_GroupSFROp
{
	GroupSfOp_Object = 0,
	GroupSfOp_MinSpecUse,
	GroupSfOp_MinSpec,
	GroupSfOp_MaxSpecUse,
	GroupSfOp_MaxSpec,
	//GroupSfOp_StardRoi,
	//GroupSfOp_CompareRoi,
	//GroupSfOp_Spec,
	GroupSfOp_MaxCol,
};

static LPCTSTR	g_lpszHeader_SFRGroupOp[] =
{
	_T(""),			//SfOp_Object = 0,
	_T(""),						// SfOp_MinSpecUse,
	_T("MinSpec"),				// SfOp_MinSpec,
	_T(""),						// SfOp_MaxSpecUse,
	_T("MaxSpec"),				// SfOp_MaxSpec,
//	_T("G1"),		//TiltSfOp_StardRoi,
//	_T("G2"),		//TiltSfOp_GapRoi,
//	_T("Spec"),		//TiltSfOp_Spec,
	NULL
};

static LPCTSTR	g_lpszHeader_SFROp_Chn[] =
{
	_T(""),						 //SfOp_Object = 0,
	_T("基准"),		//TiltSfOp_StardRoi,
	_T("比较"),		//TiltSfOp_GapRoi,
	_T("Spec"),				//TiltSfOp_Spec,
	NULL
};
static LPCTSTR	g_lpszItem_SFROp[] =
{
	NULL
};

const int	iListAglin_SFROp[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_SFROp[] =
{
	80,
	25,						// SfOp_MinSpecUse,
	85,						// SfOp_MinSpec,
	25,						// SfOp_MaxSpecUse,
	80,						// SfOp_MaxSpec,
};

#define IDC_EDT_CELLEDIT			1000
#define IDC_COM_CELLCOMBO_TYPE		2000
#define IDC_COM_CELLCOMBO_COLOR		3000
#define IDC_COM_CELLCOMBO_ITEM		4000
#define IDC_COM_CELLCOMBO_ITEM2		4001
#define IDC_CHK_SPEC_MIN_SFR			5000
#define IDC_CHK_SPEC_MAX_SFR			6000

IMPLEMENT_DYNAMIC(CList_GroupSFROp, CList_Cfg_VIBase)

CList_GroupSFROp::CList_GroupSFROp()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
// 	VERIFY(m_Font.CreateFont(
// 		12,						// nHeight
// 		0,						// nWidth
// 		0,						// nEscapement
// 		0,						// nOrientation
// 		FW_BOLD,				// nWeight
// 		FALSE,					// bItalic
// 		FALSE,					// bUnderline
// 		0,						// cStrikeOut
// 		ANSI_CHARSET,			// nCharSet
// 		OUT_DEFAULT_PRECIS,		// nOutPrecision
// 		CLIP_DEFAULT_PRECIS,	// nClipPrecision
// 		ANTIALIASED_QUALITY,	// nQuality
// 		FIXED_PITCH,			// nPitchAndFamily
// 		_T("SimHei")));		// lpszFacename
	m_nEditCol = 0;
	m_nEditRow = 0;
}

CList_GroupSFROp::~CList_GroupSFROp()
{
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CList_GroupSFROp, CList_Cfg_VIBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT	(NM_CLICK,					&CList_GroupSFROp::OnNMClick					)
	ON_NOTIFY_REFLECT	(NM_DBLCLK,					&CList_GroupSFROp::OnNMDblclk				)
	ON_EN_KILLFOCUS		(IDC_EDT_CELLEDIT,			&CList_GroupSFROp::OnEnKillFocusEdit			)
	ON_CBN_KILLFOCUS	(IDC_COM_CELLCOMBO_ITEM,	&CList_GroupSFROp::OnEnKillFocusComboItem	)
	ON_CBN_KILLFOCUS	(IDC_COM_CELLCOMBO_ITEM2,	&CList_GroupSFROp::OnEnKillFocusComboItem2	)
	ON_CBN_SELCHANGE	(IDC_COM_CELLCOMBO_ITEM,	&CList_GroupSFROp::OnEnSelectComboItem		)	
	ON_CBN_SELCHANGE	(IDC_COM_CELLCOMBO_ITEM2,	&CList_GroupSFROp::OnEnSelectComboItem2		)	
	ON_COMMAND_RANGE	(IDC_CHK_SPEC_MIN_SFR, IDC_CHK_SPEC_MIN_SFR + 999, OnRangeCheckBtnMinCtrl)
	ON_COMMAND_RANGE	(IDC_CHK_SPEC_MAX_SFR, IDC_CHK_SPEC_MAX_SFR + 999, OnRangeCheckBtnMaxCtrl)
	ON_WM_MOUSEWHEEL()
	ON_WM_VSCROLL()
END_MESSAGE_MAP()

// CList_TiltSFROp 메시지 처리기입니다.
int CList_GroupSFROp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CList_Cfg_VIBase::OnCreate(lpCreateStruct) == -1)
		return -1;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	SetFont(&m_Font);
	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER | LVS_EX_CHECKBOXES);

	InitHeader();
	this->GetHeaderCtrl()->EnableWindow(FALSE);

	m_ed_CellEdit.Create(WS_CHILD | WS_BORDER | ES_CENTER | WS_TABSTOP, rectDummy, this, IDC_EDT_CELLEDIT);
	m_ed_CellEdit.SetValidChars(_T("-.0123456789"));


	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_GroupSFROp::OnSize(UINT nType, int cx, int cy)
{
	CList_Cfg_VIBase::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_GroupSFROp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CList_Cfg_VIBase::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_GroupSFROp::InitHeader()
{
	for (int nCol = 0; nCol < GroupSfOp_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_SFRGroupOp[nCol], iListAglin_SFROp[nCol], iHeaderWidth_SFROp[nCol]);
	}

	for (int nCol = 0; nCol < GroupSfOp_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_SFROp[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 17:25
// Desc.		:
//=============================================================================
void CList_GroupSFROp::InsertFullData()
{
	if ( NULL == m_pstConfigInfo)
		return;

 	DeleteAllItems();
 
	for (UINT nIdx = 0; nIdx < GroupSfOp_ItemNum; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
 	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/6/26 - 14:26
// Desc.		:
//=============================================================================
void CList_GroupSFROp::SetRectRow(UINT nRow)
{
	if (NULL == m_pstConfigInfo)
		return;

	CString strValue;

	strValue = g_szSFRGroup[nRow];
	SetItemText(nRow, GroupSfOp_Object, strValue);

	if (m_pstConfigInfo->stInput_Group[nRow].bEnable == TRUE)
		SetItemState(nRow, 0x2000, LVIS_STATEIMAGEMASK);
	else
		SetItemState(nRow, 0x1000, LVIS_STATEIMAGEMASK);

	//!SH _111828: DATA 연결 부분 
	strValue.Format(_T("%s"), g_szSpecUse_Static[m_pstConfigInfo->stInput_Group[nRow].stSpecMin.bEnable]);
	SetItemText(nRow, GroupSfOp_MinSpecUse, strValue);

	strValue.Format(_T("%.2f"), m_pstConfigInfo->stInput_Group[nRow].stSpecMin.dbValue);
	SetItemText(nRow, GroupSfOp_MinSpec, strValue);

	strValue.Format(_T("%s"), g_szSpecUse_Static[m_pstConfigInfo->stInput_Group[nRow].stSpecMax.bEnable]);
	SetItemText(nRow, GroupSfOp_MaxSpecUse, strValue);

	strValue.Format(_T("%.2f"), m_pstConfigInfo->stInput_Group[nRow].stSpecMax.dbValue);
	SetItemText(nRow, GroupSfOp_MaxSpec, strValue);

	//if (m_pstConfigInfo->stTiltSfrOp.stTiltRoi[nRow].bEnable == TRUE)
	//	SetItemState(nRow, 0x2000, LVIS_STATEIMAGEMASK);
	//else
	//	SetItemState(nRow, 0x1000, LVIS_STATEIMAGEMASK);

	//strValue.Format(_T("%d"), m_pstConfigInfo->stTiltSfrOp.stTiltRoi[nRow].nStandardRoiNum);
	//SetItemText(nRow, TiltSfOp_StardRoi, strValue);

	//strValue.Format(_T("%d"), m_pstConfigInfo->stTiltSfrOp.stTiltRoi[nRow].nCompareRoiNum);
	//SetItemText(nRow, TiltSfOp_CompareRoi, strValue);

	//strValue.Format(_T("%.2f"), m_pstConfigInfo->stTiltSfrOp.stTiltRoi[nRow].dbSpec);
	//SetItemText(nRow, TiltSfOp_Spec, strValue);


// 
// 	strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.Width());
// 	SetItemText(nRow, TiltSfOp_Width, strValue);
// 
// 	strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.Height());
// 	SetItemText(nRow, TiltSfOp_Height, strValue);
// 
// 	strValue.Format(_T("%.2f"), m_pstConfigInfo->stSpec_Min[nRow].dbValue);
// 	SetItemText(nRow, TiltSfOp_MinSpec, strValue);
// 
// 	strValue.Format(_T("%.2f"), m_pstConfigInfo->stSpec_Max[nRow].dbValue);
// 	SetItemText(nRow, TiltSfOp_MaxSpec, strValue);
// 
// 	strValue.Format(_T("%.2f"), m_pstConfigInfo->stRegion[nRow].dbOffset);
// 	SetItemText(nRow, TiltSfOp_Offset, strValue);
// 
// 	strValue.Format(_T("%.2f"), m_pstConfigInfo->stRegion[nRow].dbLinePair);
// 	SetItemText(nRow, TiltSfOp_Linepare, strValue);
// 
// 	strValue.Format(_T("%s"), g_szItemPos[m_pstConfigInfo->stRegion[nRow].nItemPos]);
// 	SetItemText(nRow, TiltSfOp_ItemPos, strValue);

}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_GroupSFROp::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	LVHITTESTINFO HitInfo;
	HitInfo.pt = pNMItemActivate->ptAction;
	
	HitTest(&HitInfo);

	// Check Ctrl Event
	if (HitInfo.flags == LVHT_ONITEMSTATEICON)
	{
		UINT nBuffer = GetItemState(pNMItemActivate->iItem, LVIS_STATEIMAGEMASK);

		if (nBuffer == 0x2000)
			m_pstConfigInfo->stInput_Group[pNMItemActivate->iItem].bEnable = FALSE;

		if (nBuffer == 0x1000)
			m_pstConfigInfo->stInput_Group[pNMItemActivate->iItem].bEnable = TRUE;

		GetOwner()->SendNotifyMessage(m_nMessage, 0, 0);
	}

	if (GroupSfOp_MinSpecUse == pNMItemActivate->iSubItem)
	{
		m_pstConfigInfo->stInput_Group[pNMItemActivate->iItem].stSpecMin.bEnable = !m_pstConfigInfo->stInput_Group[pNMItemActivate->iItem].stSpecMin.bEnable;
		SetItemText(pNMItemActivate->iItem, pNMItemActivate->iSubItem, g_szSpecUse_Static[m_pstConfigInfo->stInput_Group[pNMItemActivate->iItem].stSpecMin.bEnable]);
	}

	if (GroupSfOp_MaxSpecUse == pNMItemActivate->iSubItem)
	{
		m_pstConfigInfo->stInput_Group[pNMItemActivate->iItem].stSpecMax.bEnable = !m_pstConfigInfo->stInput_Group[pNMItemActivate->iItem].stSpecMax.bEnable;
		SetItemText(pNMItemActivate->iItem, pNMItemActivate->iSubItem, g_szSpecUse_Static[m_pstConfigInfo->stInput_Group[pNMItemActivate->iItem].stSpecMax.bEnable]);
	}

	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_GroupSFROp::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (GroupSfOp_MinSpecUse == pNMItemActivate->iSubItem || GroupSfOp_MaxSpecUse == pNMItemActivate->iSubItem)
		{
			*pResult = 1;
			return;
		}

		if (pNMItemActivate->iSubItem == GroupSfOp_Object)
		{
			LVHITTESTINFO HitInfo;
			HitInfo.pt = pNMItemActivate->ptAction;

			HitTest(&HitInfo);

			// Check Ctrl Event
			if (HitInfo.flags == LVHT_ONITEMSTATEICON)
			{
				UINT nBuffer;

				nBuffer = GetItemState(pNMItemActivate->iItem, LVIS_STATEIMAGEMASK);

				if (nBuffer == 0x2000)
					m_pstConfigInfo->stInput_Group[pNMItemActivate->iItem].bEnable = FALSE;

				if (nBuffer == 0x1000)
					m_pstConfigInfo->stInput_Group[pNMItemActivate->iItem].bEnable = TRUE;
			}
		}

		if (pNMItemActivate->iSubItem < GroupSfOp_MaxCol && pNMItemActivate->iSubItem > 0)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);

			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

	
			{
				m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_ed_CellEdit.SetFocus();
			}
		}
	}
	GetOwner()->SendNotifyMessage(m_nMessage, 0, 0);

	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
void CList_GroupSFROp::OnEnKillFocusEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	if (m_nEditCol == GroupSfOp_MinSpec || m_nEditCol == GroupSfOp_MaxSpec)
	{
		UpdateCelldbData(m_nEditRow, m_nEditCol, _ttof(strText));
	}
	else
	{
		UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText));
	}

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
BOOL CList_GroupSFROp::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	if ( NULL == m_pstConfigInfo)
		return FALSE;

	if (iValue < 0)
		iValue = 0;


	//m_ed_CellEdit.SetWindowText(szValue);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: UpdateCelldbData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dbValue
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_GroupSFROp::UpdateCelldbData(UINT nRow, UINT nCol, double dbValue)
{
	switch (nCol)
	{
	case GroupSfOp_MinSpec:
		m_pstConfigInfo->stInput_Group[nRow].stSpecMin.dbValue = dbValue;
		break;
	case GroupSfOp_MaxSpec:
		m_pstConfigInfo->stInput_Group[nRow].stSpecMax.dbValue = dbValue;
		break;
	default:
		break;
	}

	CString str;
	str.Format(_T("%.2f"), dbValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_GroupSFROp::GetCellData()
{
	if ( NULL == m_pstConfigInfo)
		return;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_GroupSFROp::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{

 	if (m_ed_CellEdit.IsWindowVisible())
 	{
 		CWnd* pWndFocus = GetFocus();
 		if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
 		{
 			if (m_nEditCol == -1 || m_nEditRow == -1)
 			{
 				return CList_Cfg_VIBase::OnMouseWheel(nFlags, zDelta, pt);
 			}
 			CString strText;
 			m_ed_CellEdit.GetWindowText(strText);
 
 			int iValue = _ttoi(strText);
 			double dbValue = _ttof(strText);
 
 			if (0 < zDelta)
 			{
 				iValue = iValue + ((zDelta / 120));
 				dbValue = dbValue + ((zDelta / 120) * 0.01);
 			}
 			else
 			{
 				if (0 < iValue)
 					iValue = iValue + ((zDelta / 120));
 
 				if (0 < dbValue)
 					dbValue = dbValue + ((zDelta / 120) * 0.01);
 			}
 
 			if (iValue < 0)
 				iValue = 0;
 
 			if (iValue > 2000)
 				iValue = 2000;
 
 			if (dbValue < 0.0)
 				dbValue = 0.0;
 

			if (m_nEditCol == GroupSfOp_MinSpec || m_nEditCol == GroupSfOp_MaxSpec )
			{
				UpdateCelldbData(m_nEditRow, m_nEditCol, dbValue);
			}
			else
			{
				UpdateCellData(m_nEditRow, m_nEditCol, iValue);
			}
 		}
 	}
 	

	return CList_Cfg_VIBase::OnMouseWheel(nFlags, zDelta, pt);
}

//=============================================================================
// Method		: OnRangeCheckBtnMinCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2018/2/20 - 9:32
// Desc.		:
//=============================================================================
void CList_GroupSFROp::OnRangeCheckBtnMinCtrl(UINT nID)
{
// 	UINT nIndex = nID - IDC_CHK_SPEC_MIN_SFR;
// 	
// 	if (BST_CHECKED == m_chk_SpecMin[nIndex].GetCheck())
// 	{
// 		m_pstConfigInfo->stSpec_Min[nIndex].bEnable = TRUE;
// 	}
// 	else
// 	{
// 		m_pstConfigInfo->stSpec_Min[nIndex].bEnable = FALSE;
// 	}
}

//=============================================================================
// Method		: OnRangeCheckBtnMaxCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2018/2/20 - 9:32
// Desc.		:
//=============================================================================
void CList_GroupSFROp::OnRangeCheckBtnMaxCtrl(UINT nID)
{
// 	UINT nIndex = nID - IDC_CHK_SPEC_MAX_SFR;
// 
// 	if (BST_CHECKED == m_chk_SpecMax[nIndex].GetCheck())
// 	{
// 		m_pstConfigInfo->stSpec_Max[nIndex].bEnable = TRUE;
// 	}
// 	else
// 	{
// 		m_pstConfigInfo->stSpec_Max[nIndex].bEnable = FALSE;
// 	}
}

//=============================================================================
// Method		: OnEnKillFocusComboItem
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/2 - 11:36
// Desc.		:
//=============================================================================
void CList_GroupSFROp::OnEnKillFocusComboItem()
{
	CString szData;

	//m_cb_Item[GroupSfOp_StardRoi - 1].GetLBText(m_cb_Item[GroupSfOp_StardRoi - 1].GetCurSel(), szData);

	//SetItemText(m_nEditRow, GroupSfOp_StardRoi, szData);

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());
	m_nEditCol = -1;
	m_nEditRow = -1;


	//m_cb_Item[GroupSfOp_StardRoi - 1].SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: OnEnSelectComboItem
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/2 - 11:36
// Desc.		:
//=============================================================================
void CList_GroupSFROp::OnEnSelectComboItem()
{
	//!SH _111828: DATA 연결 부분 
	//m_pstConfigInfo->stTiltSfrOp.stTiltRoi[m_nEditRow].nStandardRoiNum = m_cb_Item[TiltSfOp_StardRoi-1].GetCurSel();

	CString szData;

	//m_cb_Item[GroupSfOp_StardRoi - 1].GetLBText(m_cb_Item[GroupSfOp_StardRoi - 1].GetCurSel(), szData);

	//SetItemText(m_nEditRow, GroupSfOp_StardRoi, szData);
	//m_cb_Item[GroupSfOp_StardRoi - 1].SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
	
	m_nEditCol = -1;
	m_nEditRow = -1;

}
void CList_GroupSFROp::OnEnKillFocusComboItem2()
{
	CString szData;

	//m_cb_Item[GroupSfOp_CompareRoi - 1].GetLBText(m_cb_Item[GroupSfOp_CompareRoi - 1].GetCurSel(), szData);

	//SetItemText(m_nEditRow, GroupSfOp_CompareRoi, szData);

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());
	m_nEditCol = -1;
	m_nEditRow = -1;


	//m_cb_Item[GroupSfOp_CompareRoi - 1].SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: OnEnSelectComboItem
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/2 - 11:36
// Desc.		:
//=============================================================================
void CList_GroupSFROp::OnEnSelectComboItem2()
{
	//!SH _111828: DATA 연결 부분 
	//m_pstConfigInfo->stTiltSfrOp.stTiltRoi[m_nEditRow].nCompareRoiNum = m_cb_Item[TiltSfOp_CompareRoi - 1].GetCurSel();

	CString szData;

	//m_cb_Item[GroupSfOp_CompareRoi - 1].GetLBText(m_cb_Item[GroupSfOp_CompareRoi - 1].GetCurSel(), szData);

	//SetItemText(m_nEditRow, GroupSfOp_CompareRoi, szData);
	//m_cb_Item[GroupSfOp_CompareRoi - 1].SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

	m_nEditCol = -1;
	m_nEditRow = -1;

}

void CList_GroupSFROp::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	//
	if (m_ed_CellEdit.IsWindowVisible())
		OnEnKillFocusEdit();

	//if (m_cb_Item[GroupSfOp_StardRoi - 1].IsWindowVisible())
	//	OnEnKillFocusComboItem();


	CList_Cfg_VIBase::OnVScroll(nSBCode, nPos, pScrollBar);

}
void CList_GroupSFROp::SetLanguageMode(UINT nLanguageMode){


	LPCTSTR* pszText = NULL;

	switch (nLanguageMode)
	{
	case 0:
		pszText = g_lpszHeader_SFRGroupOp;
		break;
	case 1:
		pszText = g_lpszHeader_SFROp_Chn;
		break;
	default:
		break;
	}

	for (int nCol = 0; nCol < GroupSfOp_MaxCol; nCol++)
	{
		DeleteColumn(nCol);
		InsertColumn(nCol, pszText[nCol], iListAglin_SFROp[nCol], iHeaderWidth_SFROp[nCol]);
	}
	
	InsertFullData();


}
