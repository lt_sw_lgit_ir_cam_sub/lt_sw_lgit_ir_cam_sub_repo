﻿#ifndef Wnd_Cfg_Distortion_h__
#define Wnd_Cfg_Distortion_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_T_Distortion.h"
#include "Def_TestItem_VI.h"
#include "List_DistortionOp.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_Distortion
//-----------------------------------------------------------------------------
class CWnd_Cfg_Distortion : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_Distortion)

public:
	CWnd_Cfg_Distortion();
	virtual ~CWnd_Cfg_Distortion();

	enum enDistortionStatic
	{
		STI_DIS_PARAM = 0,
		STI_DIS_SPEC,
		STI_DIS_OFFSET,
		STI_DIS_MAX,
	};

	enum enDistortionButton
	{
		BTN_DIS_RESET = 0,
		BTN_DIS_TEST,
		BTN_DIS_MAX,
	};

	enum enDistortionComobox
	{
		CMB_DIS_MAX = 1,
	};

	enum enDistortionEdit
	{
		EDT_DIS_OFFSET = 0,
		EDT_DIS_MAX,
	};

protected:

	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	CFont						m_font;

	CList_DistortionOp			m_List;

	CVGStatic					m_st_Item[STI_DIS_MAX];
	CButton						m_bn_Item[BTN_DIS_MAX];
	CComboBox					m_cb_Item[CMB_DIS_MAX];
	CMFCMaskedEdit				m_ed_Item[EDT_DIS_MAX];

	CVGStatic					m_st_CapItem;
	CVGStatic					m_st_CapSpecMin;
	CVGStatic					m_st_CapSpecMax;

	CVGStatic					m_st_Spec[Spec_Distortion_MAX];
	CMFCMaskedEdit				m_ed_SpecMin[Spec_Distortion_MAX];
	CMFCMaskedEdit				m_ed_SpecMax[Spec_Distortion_MAX];
	CMFCButton					m_chk_SpecMin[Spec_Distortion_MAX];
	CMFCButton					m_chk_SpecMax[Spec_Distortion_MAX];

	ST_Distortion_Opt*			m_pstConfigInfo = NULL;

public:

	void	SetPtr_RecipeInfo	(ST_Distortion_Opt* pstConfigInfo)
	{
		if (pstConfigInfo == NULL)
			return;

		m_pstConfigInfo = pstConfigInfo;
	};

	void	SetUpdateData		();
	void	GetUpdateData		();
};

#endif // Wnd_Cfg_Distortion_h__
