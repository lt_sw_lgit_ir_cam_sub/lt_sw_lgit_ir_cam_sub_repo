﻿#ifndef Def_T_FOV_h__
#define Def_T_FOV_h__

#include <afxwin.h>

#include "Def_T_Common.h"

#pragma pack(push,1)

typedef enum enFov_Spec
{
	Spec_Fov_Hor = 0,
	Spec_Fov_Ver,
	Spec_Fov_MAX,
};

static LPCTSTR	g_szFOV_Spec[] =
{
	_T("FOV Hor"),
	_T("FOV Ver"),
	NULL
};

typedef enum enROI_Fov
{
	ROI_Fov_CT = 0,
	ROI_Fov_CB,
	ROI_Fov_CL,
	ROI_Fov_CR,
	ROI_Fov_CC,
	ROI_Fov_Max,
};

static LPCTSTR g_szROI_Fov[] =
{
	_T("CT"),
	_T("CB"),
	_T("CL"),
	_T("CR"),
	_T("Center"),
	NULL
};

typedef struct _tag_FOV_Opt
{
	ST_RegionROI stRegion[ROI_Fov_Max];

	double	dbOffset[Spec_Fov_MAX];

	ST_Comm_Spec stSpec_Min[Spec_Fov_MAX];
	ST_Comm_Spec stSpec_Max[Spec_Fov_MAX];

	_tag_FOV_Opt()
	{
		for (UINT nIdx = 0; nIdx < ROI_Fov_Max; nIdx++)
		{
			stRegion[nIdx].Reset();
		}

		for (UINT nIdx = 0; nIdx < Spec_Fov_MAX; nIdx++)
		{
			dbOffset[nIdx] = 0.0;
			stSpec_Min[nIdx].Reset();
			stSpec_Max[nIdx].Reset();
		}
	};

	_tag_FOV_Opt& operator= (_tag_FOV_Opt& ref)
	{
		for (UINT nIdx = 0; nIdx < ROI_Fov_Max; nIdx++)
		{
			stRegion[nIdx] = ref.stRegion[nIdx];
		}

		for (UINT nIdx = 0; nIdx < Spec_Fov_MAX; nIdx++)
		{
			dbOffset[nIdx] = ref.dbOffset[nIdx];
			stSpec_Min[nIdx] = ref.stSpec_Min[nIdx];
			stSpec_Max[nIdx] = ref.stSpec_Max[nIdx];
		}

		return *this;
	};

}ST_FOV_Opt, *PST_FOV_Opt;

typedef struct _tag_FOV_Data
{
	// 결과
	UINT nResult;

	UINT nEachResult[Spec_Fov_MAX];
	double dbValue[Spec_Fov_MAX];

	CPoint ptCenter[ROI_Fov_Max];
	CRect  rtTestPicROI[ROI_Fov_Max];

	_tag_FOV_Data()
	{
		Reset();
	};

	void Reset()
	{
		nResult = 1;

		for (UINT nIdx = 0; nIdx < Spec_Fov_MAX; nIdx++)
		{
			dbValue[nIdx] = 0.0;
			nEachResult[nIdx] = 0;
		}

		for (UINT nIdx = 0; nIdx < ROI_Fov_Max; nIdx++)
		{
			ptCenter[nIdx].x = -1;
			ptCenter[nIdx].y = -1;
			rtTestPicROI[nIdx].SetRectEmpty();
		}
	};
	void FailData(){
		nResult = 0;

		for (UINT nIdx = 0; nIdx < Spec_Fov_MAX; nIdx++)
		{
			dbValue[nIdx] = 0.0;
			nEachResult[nIdx] = 0;
		}

		for (UINT nIdx = 0; nIdx < ROI_Fov_Max; nIdx++)
		{
			ptCenter[nIdx].x = -1;
			ptCenter[nIdx].y = -1;
			rtTestPicROI[nIdx].SetRectEmpty();
		}
	};
	_tag_FOV_Data& operator= (_tag_FOV_Data& ref)
	{

		nResult = ref.nResult;

		for (UINT nIdx = 0; nIdx < Spec_Fov_MAX; nIdx++)
		{
			dbValue[nIdx] = ref.dbValue[nIdx];
			nEachResult[nIdx] = ref.nEachResult[nIdx];
		}

		for (UINT nIdx = 0; nIdx < ROI_Fov_Max; nIdx++)
		{
			ptCenter[nIdx] = ref.ptCenter[nIdx];
			rtTestPicROI[nIdx] = ref.rtTestPicROI[nIdx];
		}

		return *this;
	};

}ST_FOV_Data, *PST_FOV_Data;

typedef struct _tag_TI_FOV
{
	ST_FOV_Opt	stFOVOpt;	// 검사 기준 데이터
	ST_FOV_Data stFOVData;	// 측정 데이터

	_tag_TI_FOV()
	{
	};

	void Reset()
	{
		stFOVData.Reset();
	};

	_tag_TI_FOV& operator= (_tag_TI_FOV& ref)
	{
		stFOVOpt	= ref.stFOVOpt;
		stFOVData	= ref.stFOVData;

		return *this;
	};

}ST_TI_FOV, *PST_TI_FOV;

#pragma pack(pop)

#endif // Def_FOV_h__