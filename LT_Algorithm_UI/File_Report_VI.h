//*****************************************************************************
// Filename	: 	File_Report_VI.h
// Created	:	2018/1/27 - 13:58
// Modified	:	2018/1/27 - 13:58
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#ifndef File_Report_VI_h__
#define File_Report_VI_h__

#pragma once

#include <afxwin.h>

#include "Def_Communication.h"

#include "Def_T_OpticalCenter.h"
#include "Def_T_Distortion.h"
#include "Def_T_Dynamic.h"
#include "Def_T_Rotate.h"
#include "Def_T_SFR.h"
#include "Def_T_FOV.h"
#include "Def_T_DefectPixel.h"
#include "Def_T_Shading.h"
#include "Def_T_SNR_Light.h"
#include "Def_T_Particle.h"

class CFile_Report_VI
{
public:
	CFile_Report_VI();
	virtual ~CFile_Report_VI();

	BOOL	SaveFinalizeResult					(__in UINT nParaIdx, __in CString szFullPath, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray);

	// 2D IMAGE Q
	BOOL	MakeOpticalCenterXHeadAndData		(__in UINT nParaIdx, __in CString szBarcode, __in ST_OpticalCenter_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray);
	BOOL	MakeOpticalCenterYHeadAndData		(__in UINT nParaIdx, __in CString szBarcode, __in ST_OpticalCenter_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray);
	BOOL	MakeDistortionHeadAndData			(__in UINT nParaIdx, __in CString szBarcode, __in ST_Distortion_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray);
	BOOL	MakeDynamicRangeHeadAndData			(__in UINT nParaIdx, __in UINT nItem, __in CString szBarcode, __in ST_Dynamic_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray);
	BOOL	MakeFOV_HorHeadAndData				(__in UINT nParaIdx, __in CString szBarcode, __in ST_FOV_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray);
	BOOL	MakeFOV_VerHeadAndData				(__in UINT nParaIdx, __in CString szBarcode, __in ST_FOV_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray);
	BOOL	MakeFOV_LTRBHeadAndData				(__in UINT nParaIdx, __in CString szBarcode, __in ST_FOV_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray);
	BOOL	MakeFOV_RTLBHeadAndData				(__in UINT nParaIdx, __in CString szBarcode, __in ST_FOV_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray);
	BOOL	MakeSFRHeadAndData					(__in UINT nParaIdx, __in CString szBarcode, __in ST_SFR_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray);
	BOOL	MakeRotationHeadAndData				(__in UINT nParaIdx, __in CString szBarcode, __in ST_Rotate_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray);

	BOOL	SaveFinalizeResultOpticalCenterX	(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_OpticalCenter_Data stResult, __in UINT nParaIdx = 0);
	BOOL	SaveFinalizeResultOpticalCenterY	(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_OpticalCenter_Data stResult, __in UINT nParaIdx = 0);
	BOOL	SaveFinalizeResultDistortion		(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_Distortion_Data stResult, __in UINT nParaIdx = 0);
	BOOL	SaveFinalizeResultDynamicRange		(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_Dynamic_Data stResult, __in UINT nItem, __in UINT nParaIdx = 0);
	BOOL	SaveFinalizeResultFOV_Hor			(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_FOV_Data stResult, __in UINT nParaIdx = 0);
	BOOL	SaveFinalizeResultFOV_Ver			(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_FOV_Data stResult, __in UINT nParaIdx = 0);
	BOOL	SaveFinalizeResultFOV_LTRB			(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_FOV_Data stResult, __in UINT nParaIdx = 0);
	BOOL	SaveFinalizeResultFOV_RTLB			(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_FOV_Data stResult, __in UINT nParaIdx = 0);
	BOOL	SaveFinalizeResultSFR				(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_SFR_Data stResult, __in UINT nParaIdx = 0);
	BOOL	SaveFinalizeResultRotation			(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_Rotate_Data stResult, __in UINT nParaIdx = 0);

	// 2D PARTICLE
	BOOL	MakeDefectPixelHeadAndData				(__in UINT nParaIdx, __in CString szBarcode, __in float fVoltage, __in ST_DefectPixel_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray);
	BOOL	MakeDeadPixelHeadAndData			(__in UINT nParaIdx, __in CString szBarcode, __in float fVoltage, __in ST_Particle_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray);
	BOOL	MakeBlemishHeadAndData				(__in UINT nParaIdx, __in CString szBarcode, __in float fVoltage, __in ST_Particle_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray);
	BOOL	MakeStainHeadAndData				(__in UINT nParaIdx, __in CString szBarcode, __in float fVoltage, __in ST_Particle_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray);
	BOOL	MakeRtlluminationHeadAndData		(__in UINT nParaIdx, __in CString szBarcode, __in float fVoltage, __in ST_Shading_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray);
	BOOL	MakeLightSNRHeadAndData				(__in UINT nParaIdx, __in CString szBarcode, __in float fVoltage, __in ST_SNR_Light_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray);
	BOOL	MakeLightTemperatureHeadAndData		(__in UINT nParaIdx, __in CString szBarcode, __in long lTemperature, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray);

	BOOL	SaveFinalizeResultTemperature		(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in long lTemperature, __in UINT nParaIdx = 0);
	BOOL	SaveFinalizeResultDefectPixel			(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_DefectPixel_Data stResult, __in float fVoltage, __in UINT nParaIdx = 0);
	BOOL	SaveFinalizeResultDeadPixel			(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_Particle_Data stResult, __in float fVoltage, __in UINT nParaIdx = 0);
	BOOL	SaveFinalizeResultBlemish			(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_Particle_Data stResult, __in float fVoltage, __in UINT nParaIdx = 0);
	BOOL	SaveFinalizeResultStain				(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_Particle_Data stResult, __in float fVoltage, __in UINT nParaIdx = 0);
	BOOL	SaveFinalizeResultRtllumination		(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_Shading_Data	stResult, __in float fVoltage, __in UINT nParaIdx = 0);
	BOOL	SaveFinalizeResultLightSNR			(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_SNR_Light_Data stResult, __in float fVoltage, __in UINT nParaIdx = 0);

};

#endif // File_Report_VI_h__
