#ifndef Wnd_RotateData_h__
#define Wnd_RotateData_h__

#pragma once

#include "afxwin.h"

#include "VGStatic.h"
#include "Def_T_Rotate.h"
// CWnd_RotateData
typedef enum
{
	enRTData_Header = 0,
	enRTData_Degree = 0,
	enRTData_MAX,
}enumHeaderRTData;

typedef enum
{
	enRTData_1st = 0,
	enRTData_inputmax,
}enumDataRTData;
static	LPCTSTR	g_szRTData[] =
{
	_T("Degree"),
	NULL
};

class CWnd_RotateData : public CWnd
{
	DECLARE_DYNAMIC(CWnd_RotateData)

public:
	CWnd_RotateData();
	virtual ~CWnd_RotateData();

	UINT m_nHeaderCnt = enRTData_MAX;
	CString m_SzHeaderName[enRTData_MAX];

	//---Class 정의 시 세팅 해야함.
	void SetHeader(UINT nHeaderCnt){

		if (nHeaderCnt > enRTData_MAX)
		{
			nHeaderCnt = enRTData_MAX;
		}
		m_nHeaderCnt = nHeaderCnt;
	};

	void SetHeaderName(UINT nCnt, CString szHeader){

		if (nCnt > enRTData_MAX)
		{
			nCnt = enRTData_MAX-1;
		}
		m_SzHeaderName[nCnt] = szHeader;
	};

	CVGStatic		m_st_Header[enRTData_MAX];
	CVGStatic		m_st_Data[enRTData_inputmax][enRTData_MAX];

//	CVGStatic		m_st_ResultView;
	BOOL			m_bMode = FALSE;

	void SetDisplayMode(BOOL bMode){
		m_bMode = bMode;
	};

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	void DataSetting(UINT nDataIdx, UINT nIdx, BOOL MODE, double dData);
	void DataEachReset(UINT nDataIdx, UINT nIdx);
	void DataReset();
	void DataDisplay(ST_Rotate_Data stData);
};


#endif // Wnd_RotateData_h__
