// Wnd_SFRData.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_SFRData.h"


// CWnd_SFRData

IMPLEMENT_DYNAMIC(CWnd_SFRData, CWnd)

CWnd_SFRData::CWnd_SFRData()
{

}

CWnd_SFRData::~CWnd_SFRData()
{
}


BEGIN_MESSAGE_MAP(CWnd_SFRData, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()
// CWnd_SFRData 메시지 처리기입니다.

int CWnd_SFRData::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	DWORD dwStyle = WS_VISIBLE | WS_CHILD /*| WS_CLIPCHILDREN*/ | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.
	CString strNum;
	for (int t = 0; t < (int)enSFRData_MAX; t++)
	{
		strNum.Format(_T("%d"), t);
		m_st_Header[t].SetStaticStyle(CVGStatic::StaticStyle_Title);
		m_st_Header[t].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Header[t].SetFont_Gdip(L"Arial", 12.0F);
		m_st_Header[t].Create(strNum, dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

		m_st_Data[t].SetStaticStyle(CVGStatic::StaticStyle_Title);
		m_st_Data[t].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Data[t].SetFont_Gdip(L"Arial", 12.0F);
		m_st_Data[t].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
				
	}

	return 0;
}


void CWnd_SFRData::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	int iMargin = 0;
	int iSpacing = 0;
	int iCateSpacing = 0;

	int iLeft = iMargin;
	int iTop = iMargin;
	int iWidth = cx - iMargin - iMargin;
	int iHeight = cy - iMargin - iMargin;


	if (m_bMode == FALSE)
	{
		int HeaderW = (iWidth) / 6;
		int HeaderH = iHeight / (10);

		for (UINT nIdx = 0; nIdx < 10; nIdx++)
		{
			iLeft = iMargin;
			m_st_Header[nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);
			iLeft += HeaderW;
			m_st_Data[nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);

			iTop += HeaderH;
		}
		iTop = iMargin;
		for (UINT nIdx = 10; nIdx < 20; nIdx++)
		{
			iLeft = iMargin;
			iLeft += HeaderW;
			iLeft += HeaderW;
			m_st_Header[nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);
			iLeft += HeaderW;
			m_st_Data[nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);
			iTop += HeaderH;
		}
		iTop = iMargin;
		for (UINT nIdx = 20; nIdx < 30; nIdx++)
		{
			iLeft = iMargin;
			iLeft += HeaderW;
			iLeft += HeaderW;
			iLeft += HeaderW;
			iLeft += HeaderW;
			m_st_Header[nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);
			iLeft += HeaderW;
			m_st_Data[nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);
			iTop += HeaderH;
		}
	}
	else{
		int HeaderW = (iWidth) / 10;
		int HeaderH = iHeight / (6);
		int Cnt = 0;
		for (int t = 0; t < 5; t++)
		{
			iTop = iMargin;
		
			for (UINT nIdx = 0; nIdx < 6; nIdx++)
			{
				iLeft = iMargin;
				iLeft += (HeaderW*(2*t));
				m_st_Header[Cnt].MoveWindow(iLeft, iTop, HeaderW, HeaderH);
				iLeft += HeaderW;
				m_st_Data[Cnt].MoveWindow(iLeft, iTop, HeaderW, HeaderH);
				Cnt++;
				iTop += HeaderH;
			}
		}
		
		
	}
	

}


BOOL CWnd_SFRData::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}


BOOL CWnd_SFRData::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	return CWnd::OnEraseBkgnd(pDC);
}


void CWnd_SFRData::DataSetting(UINT nIdx, BOOL MODE, double dData){

	if (MODE == TRUE)
	{
		m_st_Data[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
	}
	else{
		m_st_Data[nIdx].SetColorStyle(CVGStatic::ColorStyle_Red);
	}

	CString strData;
	strData.Format(_T("%6.3f"), dData);
	m_st_Data[nIdx].SetWindowText(strData);
}
void CWnd_SFRData::FieldDataSetting(UINT nIdx, double dData){


// 	m_st_Field[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
// 
// 
// 	CString strData;
// 	strData.Format(_T("F.%6.2f"), dData);
// 	m_st_Field[nIdx].SetWindowText(strData);
}

void CWnd_SFRData::DataEachReset(UINT nIdx){

	
		m_st_Data[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Data[nIdx].SetWindowText(_T(""));
// 		m_st_Field[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
// 		m_st_Field[nIdx].SetWindowText(_T(""));

}


void CWnd_SFRData::DataReset(){

	for (UINT nIdx = 0; nIdx < m_nHeaderCnt; nIdx++)
	{
		m_st_Data[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Data[nIdx].SetWindowText(_T(""));
	//	m_st_Field[nIdx].SetWindowText(_T(""));
	}

}

void CWnd_SFRData::DataDisplay(ST_SFR_Data stData)
{
	CString strData;
	for (int t = 0; t < enSFRData_MAX; t++)
	{
		if (stData.bUse[t])
		{
			if (stData.nEachResult[t] == TRUE)
			{
				m_st_Data[t].SetColorStyle(CVGStatic::ColorStyle_Default);
			}
			else{
				m_st_Data[t].SetColorStyle(CVGStatic::ColorStyle_Red);
			}
			strData.Format(_T("%6.2f"), stData.dbValue[t]);
			m_st_Data[t].SetWindowText(strData);

		}
		else{
			m_st_Data[t].SetColorStyle(CVGStatic::ColorStyle_DarkGray);

			m_st_Data[t].SetWindowText(_T("X"));
		}

	}

}
