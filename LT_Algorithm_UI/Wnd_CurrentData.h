#ifndef Wnd_CurrentData_h__
#define Wnd_CurrentData_h__

#pragma once

#include "afxwin.h"

#include "VGStatic.h"
#include "Def_T_ECurrent.h"

// CWnd_CurrentData
typedef enum
{
	enCRData_Header = 0,
	enCRData_MAX=2,
}enumHeaderCRData;

typedef enum
{
	enCRData_1st = 0,
	enCRData_inputmax,
}enumDataCRData;

static	LPCTSTR	g_szCRData[] =
{
	_T("1.8V"),
	_T("2.8V"),
	NULL
};
class CWnd_CurrentData : public CWnd
{
	DECLARE_DYNAMIC(CWnd_CurrentData)

public:
	CWnd_CurrentData();
	virtual ~CWnd_CurrentData();

	UINT m_nHeaderCnt = enCRData_MAX;
	CString m_SzHeaderName[enCRData_MAX];

	//---Class 정의 시 세팅 해야함.
	void SetHeader(UINT nHeaderCnt){

		if (nHeaderCnt > enCRData_MAX)
		{
			nHeaderCnt = enCRData_MAX;
		}
		m_nHeaderCnt = nHeaderCnt;
	};

	void SetHeaderName(UINT nCnt, CString szHeader){

		if (nCnt > enCRData_MAX)
		{
			nCnt = enCRData_MAX-1;
		}
		m_SzHeaderName[nCnt] = szHeader;
	};

	CVGStatic		m_st_Header[enCRData_MAX];
	CVGStatic		m_st_Data[enCRData_inputmax][enCRData_MAX];

//	CVGStatic		m_st_ResultView;


protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	void DataSetting(UINT nDataIdx, UINT nIdx, BOOL MODE, double dData);
	void DataEachReset(UINT nDataIdx, UINT nIdx);
	void DataReset();
	void DataDisplay(ST_ECurrent_Data stData);
};


#endif // Wnd_CurrentData_h__
