#ifndef Wnd_IntensityData_h__
#define Wnd_IntensityData_h__

#pragma once

#include "afxwin.h"

#include "VGStatic.h"
#include "Def_T_Intensity.h"
// CWnd_IntensityData
typedef enum
{
	enITData_Header = 0,
	enITData_MAX=5,
}enumHeaderITData;

typedef enum
{
	enITData_1st = 0,
	enITData_inputmax,
}enumDataITData;
static	LPCTSTR	g_szITData[] =
{
	_T("LT"),
	_T("RT"),
	_T("LB"),
	_T("RB"),
	_T("C"),
	NULL
};
class CWnd_IntensityData : public CWnd
{
	DECLARE_DYNAMIC(CWnd_IntensityData)

public:
	CWnd_IntensityData();
	virtual ~CWnd_IntensityData();

	UINT m_nHeaderCnt = enITData_MAX;
	CString m_SzHeaderName[enITData_MAX];

	//---Class 정의 시 세팅 해야함.
	void SetHeader(UINT nHeaderCnt){

		if (nHeaderCnt > enITData_MAX)
		{
			nHeaderCnt = enITData_MAX;
		}
		m_nHeaderCnt = nHeaderCnt;
	};

	void SetHeaderName(UINT nCnt, CString szHeader){

		if (nCnt > enITData_MAX)
		{
			nCnt = enITData_MAX-1;
		}
		m_SzHeaderName[nCnt] = szHeader;
	};

	CVGStatic		m_st_Header[enITData_MAX];
	CVGStatic		m_st_Data[enITData_inputmax][enITData_MAX];

//	CVGStatic		m_st_ResultView;


protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	void DataSetting(UINT nDataIdx, UINT nIdx, BOOL MODE, double dData);
	void DataEachReset(UINT nDataIdx, UINT nIdx);
	void DataReset();
	void DataDisplay(ST_Intensity_Data stData);
};


#endif // Wnd_IntensityData_h__
