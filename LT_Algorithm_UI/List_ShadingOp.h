﻿#ifndef List_ShadingOp_h__
#define List_ShadingOp_h__

#pragma once

#include "List_Cfg_VIBase.h"
#include "Def_T_Shading.h"

typedef enum enListItemNum_ShadingOp
{
	ShaOp_ItemNum = Index_Shading_Max,
};

//-----------------------------------------------------------------------------
// List_ShadingInfo
//-----------------------------------------------------------------------------
class CList_ShadingOp : public CList_Cfg_VIBase
{
	DECLARE_DYNAMIC(CList_ShadingOp)
	
public:
	CList_ShadingOp();
	virtual ~CList_ShadingOp();

protected:

	DECLARE_MESSAGE_MAP()

	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg void	OnNMClick				(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk				(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL	OnMouseWheel			(UINT nFlags, short zDelta, CPoint pt);

	afx_msg void	OnRangeCheckBtnMinCtrl	(UINT nID);
	afx_msg void	OnRangeCheckBtnMaxCtrl	(UINT nID);

	afx_msg void	OnEnKillFocusEdit		();

	UINT			GetItemIndexNumber		(UINT nRow);
	BOOL			UpdateCellData			(UINT nRow, UINT nCol, int iValue);
	BOOL			UpdateCelldbData		(UINT nRow, UINT nCol, double dValue);

	CFont				m_Font;
	CEdit				m_ed_CellEdit;
	UINT				m_nEditCol;
	UINT				m_nEditRow;
	CButton				m_chk_SpecMin[ShaOp_ItemNum];
	CButton				m_chk_SpecMax[ShaOp_ItemNum];

	CComboBox			m_cb_Type;
	CComboBox			m_cb_Color;

	ST_Shading_Opt*		m_pstConfigInfo  = NULL;
	enIDX_Shading		m_InspectionType = IDX_Shading_Ver_CT;

public:

	void SetPtr_ConfigInfo (__in ST_Shading_Opt* pstConfigInfo)
	{
		if (pstConfigInfo == NULL)
			return;

		m_pstConfigInfo = pstConfigInfo;
	};

	void SetItemType (__in enIDX_Shading nItemType)
	{
		m_InspectionType = nItemType;
	}

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

};
#endif // List_ShadingInfo_h__
