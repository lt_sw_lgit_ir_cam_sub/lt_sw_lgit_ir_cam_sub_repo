﻿#ifndef Def_FPN_T_h__
#define Def_FPN_T_h__

#include <afxwin.h>

#include "Def_T_Common.h"

#pragma pack(push,1)

typedef enum enFPN_Spec
{
	Spec_FPN_X = 0,
	Spec_FPN_Y,
	Spec_FPN_MAX,
};

static LPCTSTR	g_szROI_FPN_spec[] =
{
	_T("FPN X Count"),
	_T("FPN Y Count"),
	NULL
};

static LPCTSTR g_szROI_FPN[] =
{
	_T("Camera Area"),
	NULL
};


typedef enum enROI_FPN
{
	ROI_FPN = 0,
	ROI_FPN_Max,
};

typedef enum enFPN_Fail
{
	ROI_F_FP_FST = 0,
	ROI_F_FP_LST = 200,
	ROI_F_FP_Max,
};

typedef struct _tag_FPN_Opt
{
	ST_RegionROI stRegion[ROI_FPN_Max];

	ST_Comm_Spec stSpec_Min[Spec_FPN_MAX];
	ST_Comm_Spec stSpec_Max[Spec_FPN_MAX];

	double dbThresholdX;
	double dbThresholdY;

	_tag_FPN_Opt()
	{
		
		dbThresholdX = 0.0;
		dbThresholdY = 0.0;
		
		for (UINT nIdx = 0; nIdx < ROI_FPN_Max; nIdx++)
		{
			stRegion[nIdx].Reset();
			
		}

		for (UINT nIdx = 0; nIdx < Spec_FPN_MAX; nIdx++)
		{
			stSpec_Min[nIdx].Reset();
			stSpec_Max[nIdx].Reset();

		}
		
	};

	/*변수 교환 함수*/
	_tag_FPN_Opt& operator= (_tag_FPN_Opt& ref)
	{
		dbThresholdX = ref.dbThresholdX;
		dbThresholdY = ref.dbThresholdY;

		for (UINT nIdx = 0; nIdx < ROI_FPN_Max; nIdx++)
		{
			stRegion[nIdx] = ref.stRegion[nIdx];
		}

		for (UINT nIdx = 0; nIdx < Spec_FPN_MAX; nIdx++)
		{
			stSpec_Min[nIdx] = ref.stSpec_Min[nIdx];
			stSpec_Max[nIdx] = ref.stSpec_Max[nIdx];
		}

		return *this;
	};

}ST_FPN_Opt, *PST_FPN_Op;

typedef struct _tag_FPN_Data
{
	UINT	nResult;
	UINT    nEachResult[Spec_FPN_MAX];

	// 검출된 이물 갯수
	int iFailCount[Spec_FPN_MAX];

	/*변수 초기화 함수*/
	_tag_FPN_Data()
	{
		Reset();
	};

	void Reset()
	{
		nResult		= 1;

		for (int index = 0; index < Spec_FPN_MAX; index++)
		{
			iFailCount[index] = 0;
			nEachResult[index] = 0;
		}
	};
	void FailData()
	{
		nResult = 0;

		for (int index = 0; index < Spec_FPN_MAX; index++)
		{
			iFailCount[index] = 0;
			nEachResult[index] = 0;
		}
	};

	/*변수 교환 함수*/
	_tag_FPN_Data& operator= (_tag_FPN_Data& ref)
	{
		nResult		= ref.nResult;

		for (int index = 0; index < Spec_FPN_MAX; index++)
		{
			iFailCount[index]  = ref.iFailCount[index];
			nEachResult[index] = ref.nEachResult[index];
		}

		return *this;
	};

}ST_FPN_Data, *PST_FPN_Data;

typedef struct _tag_TI_FPN
{
	ST_FPN_Opt		stFPNOpt;		// 검사 기준 데이터
	ST_FPN_Data	stFPNData;		// 측정 데이터

	_tag_TI_FPN()
	{
	};

	void Reset()
	{
		stFPNData.Reset();
	};

	_tag_TI_FPN& operator= (_tag_TI_FPN& ref)
	{
		stFPNOpt	= ref.stFPNOpt;
		stFPNData	= ref.stFPNData;

		return *this;
	};

}ST_TI_FPN, *PST_TI_FPN;

#pragma pack(pop)

#endif // Def_FPN_T_h__

