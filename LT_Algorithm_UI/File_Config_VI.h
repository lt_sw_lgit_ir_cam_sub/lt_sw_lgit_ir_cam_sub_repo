//*****************************************************************************
// Filename	: 	File_Config_VI.h
// Created	:	2018/1/26 - 14:53
// Modified	:	2018/1/26 - 14:53
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#ifndef File_VI_Config_h__
#define File_VI_Config_h__

#pragma once

#include <afxwin.h>
#include "CommonFunction.h"

#include "Def_T_ECurrent.h"
#include "Def_T_OpticalCenter.h"
#include "Def_T_Rotate.h"
#include "Def_T_Distortion.h"
#include "Def_T_Dynamic.h"
#include "Def_T_SFR.h"
#include "Def_T_FOV.h"

#include "Def_T_Intensity.h"
#include "Def_T_DefectPixel.h"
#include "Def_T_Shading.h"
#include "Def_T_SNR_Light.h"
#include "Def_T_Particle.h"

#include "Def_T_ActiveAlign.h"
#include "Def_T_Torque.h"
#include "Def_T_HotPixel.h"

#include "Def_T_FPN.h"
#include "Def_T_3D_Depth.h"
#include "Def_T_EEPROM_Verify.h"
#include "Def_T_TemperatureSensor.h"
#include "Def_T_Particle_Entry.h"

#include "Def_T_Focus.h"

//-----------------------------------------------------------------------------
// CFile_VI_Config
//-----------------------------------------------------------------------------
class CFile_VI_Config
{
public:
	CFile_VI_Config();
	~CFile_VI_Config();
	
	BOOL	Load_Rect					(__in LPCTSTR szPath, __in LPCTSTR szAppName, __in ST_RegionROI& stRegion);
	BOOL	Save_Rect					(__in LPCTSTR szPath, __in LPCTSTR szAppName, __in const ST_RegionROI* pstRect);

	// ����
	BOOL	Load_ECurrentFile			(__in LPCTSTR szPath, __out ST_ECurrent_Opt& stConfigInfo);
	BOOL	Save_ECurrentFile			(__in LPCTSTR szPath, __in const ST_ECurrent_Opt* pstConfigInfo);

	// ����
	BOOL	Load_OpticalCenterFile		(__in LPCTSTR szPath, __out ST_OpticalCenter_Opt& stConfigInfo);
	BOOL	Save_OpticalCenterFile		(__in LPCTSTR szPath, __in const ST_OpticalCenter_Opt* pstConfigInfo);

	// Rotate
	BOOL	Load_RotateFile				(__in LPCTSTR szPath, __out ST_Rotate_Opt& stConfigInfo);
	BOOL	Save_RotateFile				(__in LPCTSTR szPath, __in const ST_Rotate_Opt* pstConfigInfo);

	// Distortion
	BOOL	Load_DistortionFile			(__in LPCTSTR szPath, __out ST_Distortion_Opt& stConfigInfo);
	BOOL	Save_DistortionFile			(__in LPCTSTR szPath, __in const ST_Distortion_Opt* pstConfigInfo);

	// Dynamic
	BOOL	Load_DynamicFile			(__in LPCTSTR szPath, __out ST_Dynamic_Opt& stConfigInfo);
	BOOL	Save_DynamicFile			(__in LPCTSTR szPath, __in const ST_Dynamic_Opt* pstConfigInfo);
	
	// Intencity
	BOOL	Load_IntencityFile			(__in LPCTSTR szPath, __out ST_Intensity_Opt& stConfigInfo);
	BOOL	Save_IntencityFile			(__in LPCTSTR szPath, __in const ST_Intensity_Opt* pstConfigInfo);
	
	// FOV
	BOOL	Load_FOVFile				(__in LPCTSTR szPath, __out ST_FOV_Opt& stConfigInfo);
	BOOL	Save_FOVFile				(__in LPCTSTR szPath, __in const ST_FOV_Opt* pstConfigInfo);

	// SFR
	BOOL	Load_SFRFile				(__in LPCTSTR szPath, __out ST_SFR_Opt& stConfigInfo);
	BOOL	Save_SFRFile				(__in LPCTSTR szPath, __in const ST_SFR_Opt* pstConfigInfo);
	// SFR
	BOOL	Load_SFRFile_Dist			(__in LPCTSTR szPath, __out ST_SFR_Opt* stConfigInfo);
	BOOL	Save_SFRFile_Dist			(__in LPCTSTR szPath, __in const ST_SFR_Opt* pstConfigInfo);

	// DefectPixel
	BOOL	Load_DefectPixelFile		(__in LPCTSTR szPath, __out ST_DefectPixel_Opt& stConfigInfo);
	BOOL	Save_DefectPixelFile		(__in LPCTSTR szPath, __in const ST_DefectPixel_Opt* pstConfigInfo);

	// Shading
	BOOL	Load_ShadingFile			(__in LPCTSTR szPath, __out ST_Shading_Opt& stConfigInfo);
	BOOL	Save_ShadingFile			(__in LPCTSTR szPath, __in const ST_Shading_Opt* pstConfigInfo);

	// SNR_Light
	BOOL	Load_SNR_LightFile			(__in LPCTSTR szPath, __out ST_SNR_Light_Opt& stConfigInfo);
	BOOL	Save_SNR_LightFile			(__in LPCTSTR szPath, __in const ST_SNR_Light_Opt* pstConfigInfo);

	// Particle
	BOOL	Load_ParticleFile			(__in LPCTSTR szPath, __out ST_Particle_Opt& stConfigInfo);
	BOOL	Save_ParticleFile			(__in LPCTSTR szPath, __in const ST_Particle_Opt* pstConfigInfo);

	// AA
	BOOL	Load_ActiveAlignFile		(__in LPCTSTR szPath, __out ST_ActiveAlign_Opt& stConfigInfo);
	BOOL	Save_ActiveAlignFile		(__in LPCTSTR szPath, __in const ST_ActiveAlign_Opt* pstConfigInfo);

	// Torque
	BOOL	Load_TorqueFile				(__in LPCTSTR szPath, __out ST_Torque_Opt& stConfigInfo);
	BOOL	Save_TorqueFile				(__in LPCTSTR szPath, __in const ST_Torque_Opt* pstConfigInfo);

	// HotPixel
	BOOL	Load_HotPixelFile			(__in LPCTSTR szPath, __out ST_HotPixel_Opt& stConfigInfo);
	BOOL	Save_HotPixelFile			(__in LPCTSTR szPath, __in const ST_HotPixel_Opt* pstConfigInfo);

	// FixedPixel
	BOOL	Load_FPNFile				(__in LPCTSTR szPath, __out ST_FPN_Opt& stConfigInfo);
	BOOL	Save_FPNFile				(__in LPCTSTR szPath, __in const ST_FPN_Opt* pstConfigInfo);

	// 3D_Depth
	BOOL	Load_3D_DepthFile			(__in LPCTSTR szPath, __out ST_3D_Depth_Opt& stConfigInfo);
	BOOL	Save_3D_DepthFile			(__in LPCTSTR szPath, __in const ST_3D_Depth_Opt* pstConfigInfo);

	// EEPROM_Verify
	BOOL	Load_EEPROM_VerifyFile		(__in LPCTSTR szPath, __out ST_EEPROM_Verify_Opt& stConfigInfo);
	BOOL	Save_EEPROM_VerifyFile		(__in LPCTSTR szPath, __in const ST_EEPROM_Verify_Opt* pstConfigInfo);

	// TemperatureSensor
	BOOL	Load_TemperatureSensorFile	(__in LPCTSTR szPath, __out ST_TemperatureSensor_Opt& stConfigInfo);
	BOOL	Save_TemperatureSensorFile	(__in LPCTSTR szPath, __in const ST_TemperatureSensor_Opt* pstConfigInfo);

	// Focus
	BOOL	Load_Focus					(__in LPCTSTR szPath, __out ST_Focus_Opt& stConfigInfo);
	BOOL	Save_Focus					(__in LPCTSTR szPath, __in const ST_Focus_Opt* pstConfigInfo);

	// Particle
	BOOL	Load_Particle_EntryFile		(__in LPCTSTR szPath, __out ST_Particle_Entry_Opt& stConfigInfo);
	BOOL	Save_Particle_EntryFile		(__in LPCTSTR szPath, __in const ST_Particle_Entry_Opt* pstConfigInfo);
};

#endif // File_VI_Config_h__
