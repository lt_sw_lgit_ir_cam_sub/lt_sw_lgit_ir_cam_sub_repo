﻿#ifndef List_GroupSFROp_h__
#define List_GroupSFROp_h__

#pragma once

#include "List_Cfg_VIBase.h"
#include "Def_T_SFR.h"

typedef enum enListItemNum_GroupSFROp
{
	GroupSfOp_ItemNum = ROI_SFR_GROUP_Max,
};

//-----------------------------------------------------------------------------
// List_SFRInfo
//-----------------------------------------------------------------------------
class CList_GroupSFROp : public CList_Cfg_VIBase
{
	DECLARE_DYNAMIC(CList_GroupSFROp)

public:
	CList_GroupSFROp();
	virtual ~CList_GroupSFROp();
	UINT m_nMessage;
	void SetWindowMessage(UINT nMeg){
		m_nMessage = nMeg;
	};
protected:
	
	DECLARE_MESSAGE_MAP()

	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg void	OnNMClick				(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk				(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL	OnMouseWheel			(UINT nFlags, short zDelta, CPoint pt);

	afx_msg void	OnRangeCheckBtnMinCtrl	(UINT nID);
	afx_msg void	OnRangeCheckBtnMaxCtrl	(UINT nID);

	afx_msg void	OnEnKillFocusComboItem	();
	afx_msg void	OnEnSelectComboItem		();
	afx_msg void	OnEnKillFocusComboItem2();
	afx_msg void	OnEnSelectComboItem2();

	afx_msg void    OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void	OnEnKillFocusEdit();

	BOOL			UpdateCellData			(UINT nRow, UINT nCol, int iValue);
	BOOL			UpdateCelldbData		(UINT nRow, UINT nCol, double dValue);

	CFont				m_Font;
	CMFCMaskedEdit		m_ed_CellEdit;
	int				m_nEditCol;
	int				m_nEditRow;
// 	CButton				m_chk_SpecMin[SfOp_ItemNum];
// 	CButton				m_chk_SpecMax[SfOp_ItemNum];

	CComboBox			m_cb_Type;
	CComboBox			m_cb_Color;
	CComboBox			m_cb_Item[2];

	//ST_UI_SFR_Group*	m_pstConfigInfo = NULL;
	ST_SFR_Opt*			m_pstConfigInfo = NULL;
public:

	void SetLanguageMode(UINT nLanguageMode);
	//void SetPtr_ConfigInfo(ST_UI_SFR_Group* pstConfigInfo)
	//{
	//	if (pstConfigInfo == NULL)
	//		return;

	//	m_pstConfigInfo = pstConfigInfo;
	//};
	void SetPtr_ConfigInfo(ST_SFR_Opt* pstConfigInfo)
	{
		if (pstConfigInfo == NULL)
			return;

		m_pstConfigInfo = pstConfigInfo;
	};

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();
	
};

#endif // List_SFRInfo_h__
