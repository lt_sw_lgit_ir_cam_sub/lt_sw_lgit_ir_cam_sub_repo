#ifndef Wnd_Particle_EntryData_h__
#define Wnd_Particle_EntryData_h__

#pragma once

#include "afxwin.h"

#include "VGStatic.h"
#include "Def_T_Particle_Entry.h"

// CWnd_Particle_EntryData
typedef enum
{
	enPR_EntryData_Header = 0,
	enPR_EntryData_MAX = 1,
}enumHeaderPR_EntryData;

typedef enum
{
	enPR_EntryData_1st = 0,
	enPR_EntryData_inputmax,
}enumDataPR_EntryData;
static	LPCTSTR	g_szPR_EntryData[] =
{
	_T("Particle Cnt"),
	NULL
};

class CWnd_Particle_EntryData : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Particle_EntryData)

public:
	CWnd_Particle_EntryData();
	virtual ~CWnd_Particle_EntryData();

	UINT m_nHeaderCnt = enPR_EntryData_MAX;
	CString m_SzHeaderName[enPR_EntryData_MAX];

	//---Class 정의 시 세팅 해야함.
	void SetHeader(UINT nHeaderCnt){

		if (nHeaderCnt > enPR_EntryData_MAX)
		{
			nHeaderCnt = enPR_EntryData_MAX;
		}
		m_nHeaderCnt = nHeaderCnt;
	};

	void SetHeaderName(UINT nCnt, CString szHeader){

		if (nCnt > enPR_EntryData_MAX)
		{
			nCnt = enPR_EntryData_MAX - 1;
		}
		m_SzHeaderName[nCnt] = szHeader;
	};

	CVGStatic		m_st_Header[enPR_EntryData_MAX];
	CVGStatic		m_st_Data[enPR_EntryData_inputmax][enPR_EntryData_MAX];

//	CVGStatic		m_st_ResultView;


protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	void DataSetting(UINT nDataIdx, UINT nIdx, BOOL MODE, double dData);
	void DataEachReset(UINT nDataIdx, UINT nIdx);
	void DataReset();
	void DataDisplay(ST_Particle_Entry_Data stData);
};


#endif // Wnd_Particle_EntryData_h__
