﻿#ifndef Def_T_Dynamic_h__
#define Def_T_Dynamic_h__

#include <afxwin.h>

#include "Def_T_Common.h"

#pragma pack(push,1)

typedef enum enParticle_SNR_Light
{
	Light_Par_SNR_White = 0,
	Light_Par_SNR_Gray,
	Light_Par_SNR_Black,
	Light_Par_SNR_MAX,
};

typedef enum enParticle_SNR_Spec
{
	Spec_Par_SNR_Dynamic = 0,
	Spec_Par_SNR_SNRBW,
	Spec_Par_SNR_MAX,
};

static LPCTSTR	g_szDynamic_Spec[] =
{
	_T("Dynamic Range"),
	_T("SNR Black White"),
	NULL
};

typedef enum enROI_Partiecle_SNR
{
	ROI_Par_SNR_LT = 0,
	ROI_Par_SNR_LB,
	ROI_Par_SNR_RT,
	ROI_Par_SNR_RB,
	ROI_Par_SNR_Center,
	ROI_Par_SNR_Max,
};


static LPCTSTR	g_szDynamic_ROI[] =
{
	_T("LT"),
	_T("LB"),
	_T("RT"),
	_T("RB"),
	_T("Center"),
	NULL
};


typedef struct _tag_Dynamic_Opt
{
	ST_RegionROI stRegion[ROI_Par_SNR_Max];

	ST_Comm_Spec stSpec_Min[Spec_Par_SNR_MAX];
	ST_Comm_Spec stSpec_Max[Spec_Par_SNR_MAX];

	_tag_Dynamic_Opt()
	{	
		for (UINT nIdx = 0; nIdx < Spec_Par_SNR_MAX; nIdx++)
		{
			stSpec_Min[nIdx].Reset();
			stSpec_Max[nIdx].Reset();
		}

		for (UINT nIdx = 0; nIdx < ROI_Par_SNR_Max; nIdx++)
		{
			stRegion[nIdx].Reset();
		}
	};

	_tag_Dynamic_Opt& operator= (_tag_Dynamic_Opt& ref)
	{
		for (UINT nIdx = 0; nIdx < Spec_Par_SNR_MAX; nIdx++)
		{
			stSpec_Min[nIdx] = ref.stSpec_Min[nIdx];
			stSpec_Max[nIdx] = ref.stSpec_Max[nIdx];
		}

		for (UINT nIdx = 0; nIdx < ROI_Par_SNR_Max; nIdx++)
		{
			stRegion[nIdx]	 = ref.stRegion[nIdx];
		}

		return *this;
	};

}ST_Dynamic_Opt, *PST_Dynamic_Opt;

typedef struct _tag_Dynamic_Data
{
	UINT nResultDR;
	UINT nResultBW;

	BOOL bUse[ROI_Par_SNR_Max];
	UINT nEtcResultDR[ROI_Par_SNR_Max];
	UINT nEtcResultBW[ROI_Par_SNR_Max];
	double dbValueDR[ROI_Par_SNR_Max];
	double dbValueBW[ROI_Par_SNR_Max];

	short  sSignal[Light_Par_SNR_MAX][ROI_Par_SNR_Max];
	float  fNoise[Light_Par_SNR_MAX][ROI_Par_SNR_Max];

	_tag_Dynamic_Data()
	{
		Reset();
	};

	void Reset()
	{
		nResultDR = 1;
		nResultBW = 1;

		for (UINT nIdx = 0; nIdx < ROI_Par_SNR_Max; nIdx++)
		{
			bUse[nIdx] = 0;
			dbValueDR[nIdx] = 0.0;
			dbValueBW[nIdx] = 0.0;
			nEtcResultDR[nIdx] = 0;
			nEtcResultBW[nIdx] = 0;
		}

		for (UINT nMode = 0; nMode < Light_Par_SNR_MAX; nMode++)
		{
			for (UINT nIdx = 0; nIdx < ROI_Par_SNR_Max; nIdx++)
			{
				sSignal[nMode][nIdx] = 0;
				fNoise[nMode][nIdx]  = 0.0;
			}
		}
	};

	void FailData(){
		nResultDR = 0;
		nResultBW = 0;

		for (UINT nIdx = 0; nIdx < ROI_Par_SNR_Max; nIdx++)
		{
			bUse[nIdx] = 0;
			dbValueDR[nIdx] = 0.0;
			dbValueBW[nIdx] = 0.0;
			nEtcResultDR[nIdx] = 0;
			nEtcResultBW[nIdx] = 0;
		}

		for (UINT nMode = 0; nMode < Light_Par_SNR_MAX; nMode++)
		{
			for (UINT nIdx = 0; nIdx < ROI_Par_SNR_Max; nIdx++)
			{
				sSignal[nMode][nIdx] = 0;
				fNoise[nMode][nIdx] = 0.0;
			}
		}
	};

	_tag_Dynamic_Data& operator= (_tag_Dynamic_Data& ref)
	{
		nResultDR = ref.nResultDR;
		nResultBW = ref.nResultBW;

		for (UINT nIdx = 0; nIdx < ROI_Par_SNR_Max; nIdx++)
		{
			bUse[nIdx] = ref.bUse[nIdx];

			dbValueDR[nIdx]	= ref.dbValueDR[nIdx];
			dbValueBW[nIdx]	= ref.dbValueBW[nIdx];

			nEtcResultDR[nIdx] = ref.nEtcResultDR[nIdx];
			nEtcResultBW[nIdx] = ref.nEtcResultBW[nIdx];
		}

		
		for (UINT nMode = 0; nMode < Light_Par_SNR_MAX; nMode++)
		{

			for (UINT nIdx = 0; nIdx < ROI_Par_SNR_Max; nIdx++)
			{
				sSignal[nMode][nIdx] = ref.sSignal[nMode][nIdx];
				fNoise[nMode][nIdx]	 = ref.fNoise[nMode][nIdx];
			}
		}

		return *this;
	};

}ST_Dynamic_Data, *PST_Dynamic_Data;

typedef struct _tag_TI_Dynamic
{
	ST_Dynamic_Opt	stDynamicOpt;	// 검사 기준 데이터
	ST_Dynamic_Data	stDynamicData;	// 측정 데이터

	_tag_TI_Dynamic()
	{
	};

	void Reset()
	{
		stDynamicData.Reset();
	};

	_tag_TI_Dynamic& operator= (_tag_TI_Dynamic& ref)
	{
		stDynamicOpt	= ref.stDynamicOpt;
		stDynamicData	= ref.stDynamicData;

		return *this;
	};

}ST_TI_Dynamic, *PST_TI_Dynamic;

#pragma pack(pop)

#endif // Def_Dynamic_h__

