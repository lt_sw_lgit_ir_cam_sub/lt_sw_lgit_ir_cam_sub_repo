#ifndef Wnd_ShadingData_h__
#define Wnd_ShadingData_h__

#pragma once

#include "afxwin.h"

#include "VGStatic.h"
#include "Def_T_Shading.h"

// CWnd_ShadingData
typedef enum
{
	enSHData_Header = 0,
	enSHData_CT = 0,
	enSHData_CB,
	enSHData_LC,
	enSHData_RC,
	enSHData_D_LT,
	enSHData_D_LB,
	enSHData_D_RT,
	enSHData_D_RB,
	enSHData_C,
	enSHData_MAX,
}enumHeaderSHData;

static	LPCTSTR	g_szSHData[] =
{
	_T("CT"),
	_T("CB"),
	_T("LC"),
	_T("RC"),
	_T("D_LT"),
	_T("D_LB"),
	_T("D_RT"),
	_T("D_RB"),
	_T("C"),
	NULL
};
class CWnd_ShadingData : public CWnd
{
	DECLARE_DYNAMIC(CWnd_ShadingData)

public:
	CWnd_ShadingData();
	virtual ~CWnd_ShadingData();

	UINT m_nHeaderCnt = enSHData_MAX;
	CString m_SzHeaderName[enSHData_MAX];

	//---Class 정의 시 세팅 해야함.
	void SetHeader(UINT nHeaderCnt){
		m_nHeaderCnt = nHeaderCnt;
	};

	void SetHeaderName(UINT nCnt, CString szHeader){
		m_SzHeaderName[nCnt] = szHeader;
	};

	CVGStatic		m_st_Header[enSHData_MAX];
	CVGStatic		m_st_Data[enSHData_MAX][9];
	//CVGStatic		m_st_Field[enSHData_MAX];


protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	void DataSetting(UINT nIdx, BOOL MODE, double dData);
	void FieldDataSetting(UINT nIdx, double dData);
	void DataEachReset(UINT nIdx);
	void DataReset();
	void DataDisplay(ST_Shading_Data stData);
	UINT GetItemIndexNumber(UINT nType, UINT nRow);
};


#endif // Wnd_ShadingData_h__
