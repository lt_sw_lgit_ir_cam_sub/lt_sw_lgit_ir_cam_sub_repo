//*****************************************************************************
// Filename	: 	Def_TestItem_VI.h
// Created	:	2018/1/26 - 10:25
// Modified	:	2018/1/26 - 10:25
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#ifndef Def_TestItem_VI_h__
#define Def_TestItem_VI_h__

//#include "Def_Communication.h"

#pragma pack(push,1)

typedef enum enOverlayItem
{
	Ovr_OpticalCenter = 0,
	Ovr_ECurrent,
	Ovr_FOV,
	Ovr_SFR,
	Ovr_SFR_G0,
	Ovr_SFR_G1,
	Ovr_SFR_G2,
	Ovr_SFR_G3,
	Ovr_SFR_G4,
	Ovr_SFR_X1Tilt,
	Ovr_SFR_X2Tilt,
	Ovr_SFR_Y1Tilt,
	Ovr_SFR_Y2Tilt,
	Ovr_SFR_2,
	Ovr_SFR_G0_2,
	Ovr_SFR_G1_2,
	Ovr_SFR_G2_2,
	Ovr_SFR_G3_2,
	Ovr_SFR_G4_2,
	Ovr_SFR_X1Tilt_2,
	Ovr_SFR_X2Tilt_2,
	Ovr_SFR_Y1Tilt_2,
	Ovr_SFR_Y2Tilt_2,
	Ovr_SFR_3,
	Ovr_SFR_G0_3,
	Ovr_SFR_G1_3,
	Ovr_SFR_G2_3,
	Ovr_SFR_G3_3,
	Ovr_SFR_G4_3,
	Ovr_SFR_X1Tilt_3,
	Ovr_SFR_X2Tilt_3,
	Ovr_SFR_Y1Tilt_3,
	Ovr_SFR_Y2Tilt_3,
	Ovr_Distortion,
	Ovr_Rotate,
	Ovr_Particle,
	Ovr_Particle_SNR,
	Ovr_DefectPixel,
	Ovr_Intensity,
	Ovr_Shading,
	Ovr_SNR_Light,
	Ovr_HotPixel,
	Ovr_FPN,
	Ovr_3D_Depth,
	Ovr_EEPROM_Verify,
	Ovr_ActiveAlign,
	Ovr_PreFocus,
	Ovr_ReleaseScrew,
	Ovr_LockingScrew,
	Ovr_Temperature,
	Ovr_Particle_Entry,
	Ovr_MaxEnum,
};

typedef enum enVisionTest_ItemID
{
	VTID_ECurrent = 0,		//
	VTID_OpticalCenter,		//
	VTID_DynamicRange,		//
	VTID_SNR_BW,			//
	VTID_SNR_IQ,			// SNR_Center_Intensity
	VTID_FOV,				//
	VTID_SFR,				//
	VTID_Distortion,		//
	VTID_Rotation,			//
	VTID_Tilt,				//
	VTID_Stain,				// 
	VTID_Blemish,			// 
	VTID_DeadPixel,			// 
	VTID_DefectPixel,			// 
	VTID_Rtllumination,		// 
	VTID_Light_SNR,			// 
	VTID_FPN,				//
	VTID_VCSEL,				//
	VTID_Hot_Pixel,
	VTID_Fixed_Pattern,
	VTID_3D_Depth,
	VTID_EEPROM_Verify,
	VTID_Intensity,			//
	VTID_TemperatureSonsor,
	VTID_Particle_Entry,
	VTID_MaxEnum,
};

static LPCTSTR g_szVisionTest_ItemName[] =
{
	_T("Current"),
	_T("OpticalCenter"),
	_T("DynamicRange"),
	_T("SNR_BW"),
	_T("SNR_IQ"),
	_T("FOV"),
	_T("SFR"),
	_T("Distortion"),
	_T("Rotation"),
	_T("Tilt"),
	_T("Stain"),
	_T("Blemish"),
	_T("DeadPixel"),
	_T("DefectPixel"),
	_T("Rtllumination"),
	_T("Light_SNR"),
	_T("FPN"),
	_T("VCSEL"),
	_T("Shading(RI)"),
	_T("Hot_Pixel"),
	_T("Fixed_pattern"),
	_T("3D_Depth"),
	_T("EEPROM_Verify"),
	_T("TemperatureSensor"),
	_T("Particle"),
	NULL
};


typedef enum enParticleTestIndex
{
	Particle_1st = 0,
	Particle_2nd,
	Particle_3nd,
	Particle_Max,
};

static	LPCWCHAR	g_szParticleName[] =
{
	_T("MAX"),
	_T("MID"),
	_T("MIN"),
	NULL
};



#pragma pack(pop)

#endif // Def_TestItem_VI_h__
