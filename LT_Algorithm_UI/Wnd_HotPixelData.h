#ifndef Wnd_HotPixelData_h__
#define Wnd_HotPixelData_h__

#pragma once

#include "afxwin.h"

#include "VGStatic.h"
#include "Def_T_HotPixel.h"

// CWnd_HotPixelData
typedef enum
{
	enHOTData_Header = 0,
	enHOTData_MAX,
}enumHeaderHOTData;

typedef enum
{
	enHOTData_1st = 0,
	enHOTData_inputmax,
}enumDataHOTData;
static	LPCTSTR	g_szHotPixelData[] =
{
	_T("HotPixel Cnt"),
	NULL
};

class CWnd_HotPixelData : public CWnd
{
	DECLARE_DYNAMIC(CWnd_HotPixelData)

public:
	CWnd_HotPixelData();
	virtual ~CWnd_HotPixelData();

	UINT m_nHeaderCnt = enHOTData_MAX;
	CString m_SzHeaderName[enHOTData_MAX];

	//---Class 정의 시 세팅 해야함.
	void SetHeader(UINT nHeaderCnt){

		if (nHeaderCnt > enHOTData_MAX)
		{
			nHeaderCnt = enHOTData_MAX;
		}
		m_nHeaderCnt = nHeaderCnt;
	};

	void SetHeaderName(UINT nCnt, CString szHeader){

		if (nCnt > enHOTData_MAX)
		{
			nCnt = enHOTData_MAX-1;
		}
		m_SzHeaderName[nCnt] = szHeader;
	};

	CVGStatic		m_st_Header[enHOTData_MAX];
	CVGStatic		m_st_Data[enHOTData_inputmax][enHOTData_MAX];

//	CVGStatic		m_st_ResultView;


protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	void DataSetting(UINT nDataIdx, UINT nIdx, BOOL MODE, double dData);
	void DataEachReset(UINT nDataIdx, UINT nIdx);
	void DataReset();
	void DataDisplay(ST_HotPixel_Data stData);
};


#endif // Wnd_HotPixelData_h__
