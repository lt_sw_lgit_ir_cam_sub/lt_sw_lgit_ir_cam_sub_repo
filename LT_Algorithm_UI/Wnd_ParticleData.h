#ifndef Wnd_ParticleData_h__
#define Wnd_ParticleData_h__

#pragma once

#include "afxwin.h"

#include "VGStatic.h"
#include "Def_T_Particle.h"

// CWnd_ParticleData
typedef enum
{
	enPRData_Header = 0,
	enPRData_MAX=1,
}enumHeaderPRData;

typedef enum
{
	enPRData_1st = 0,
	enPRData_inputmax,
}enumDataPRData;
static	LPCTSTR	g_szPRData[] =
{
	_T("Particle Cnt"),
	NULL
};

class CWnd_ParticleData : public CWnd
{
	DECLARE_DYNAMIC(CWnd_ParticleData)

public:
	CWnd_ParticleData();
	virtual ~CWnd_ParticleData();

	UINT m_nHeaderCnt = enPRData_MAX;
	CString m_SzHeaderName[enPRData_MAX];

	//---Class 정의 시 세팅 해야함.
	void SetHeader(UINT nHeaderCnt){

		if (nHeaderCnt > enPRData_MAX)
		{
			nHeaderCnt = enPRData_MAX;
		}
		m_nHeaderCnt = nHeaderCnt;
	};

	void SetHeaderName(UINT nCnt, CString szHeader){

		if (nCnt > enPRData_MAX)
		{
			nCnt = enPRData_MAX-1;
		}
		m_SzHeaderName[nCnt] = szHeader;
	};

	CVGStatic		m_st_Header[enPRData_MAX];
	CVGStatic		m_st_Data[enPRData_inputmax][enPRData_MAX];

//	CVGStatic		m_st_ResultView;


protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	void DataSetting(UINT nDataIdx, UINT nIdx, BOOL MODE, double dData);
	void DataEachReset(UINT nDataIdx, UINT nIdx);
	void DataReset();
	void DataDisplay(ST_Particle_Data stData);
};


#endif // Wnd_ParticleData_h__
