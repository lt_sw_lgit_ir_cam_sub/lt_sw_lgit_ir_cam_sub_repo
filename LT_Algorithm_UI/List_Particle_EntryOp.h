﻿#ifndef List_Particle_EntryOp_h__
#define List_Particle_EntryOp_h__

#pragma once

#include "List_Cfg_VIBase.h"
#include "Def_T_Particle_Entry.h"

typedef enum enListItemNum_Particle_EntryOp
{
	ParEntryOp_ItemNum = Particle_Entry_Region_MaxEnum,
};

//-----------------------------------------------------------------------------
// List_Particle_EntryInfo
//-----------------------------------------------------------------------------
class CList_Particle_EntryOp : public CList_Cfg_VIBase
{
	DECLARE_DYNAMIC(CList_Particle_EntryOp)

public:
	CList_Particle_EntryOp();
	virtual ~CList_Particle_EntryOp();

protected:

	DECLARE_MESSAGE_MAP()

	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg void	OnNMClick				(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk				(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL	OnMouseWheel			(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void	OnEnKillFocusEdit		();

	BOOL			UpdateCellData			(UINT nRow, UINT nCol, int iValue);
	BOOL			UpdateCelldbData		(UINT nRow, UINT nCol, double dValue);

	CFont				m_Font;
	CEdit				m_ed_CellEdit;
	UINT				m_nEditCol;
	UINT				m_nEditRow;

	CComboBox			m_cb_Type;
	CComboBox			m_cb_Color;

	ST_Particle_Entry_Opt*	m_pstConfigInfo = NULL;

public:

	void SetPtr_ConfigInfo(ST_Particle_Entry_Opt* pstConfigInfo)
	{
		if (pstConfigInfo == NULL)
			return;

		m_pstConfigInfo = pstConfigInfo;
	};
	
	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();
};

#endif // List_Particle_EntryInfo_h__
