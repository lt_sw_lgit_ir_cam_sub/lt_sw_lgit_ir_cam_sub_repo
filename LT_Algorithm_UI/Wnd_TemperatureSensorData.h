#ifndef Wnd_TemperatureSensorData_h__
#define Wnd_TemperatureSensorData_h__

#pragma once

#include "afxwin.h"

#include "VGStatic.h"
#include "Def_T_TemperatureSensor.h"
// CWnd_RotateData
typedef enum
{
	enTempData_Header = 0,
	enTempData_Temperature = 0,
	enTempData_MAX,
}enumHeaderTempData;

typedef enum
{
	enTempData_1st = 0,
	enTempData_inputmax,
}enumDataTempData;
static	LPCTSTR	g_szTempData[] =
{
	_T("Temperature"),
	NULL
};

class CWnd_TemperatureSensorData : public CWnd
{
	DECLARE_DYNAMIC(CWnd_TemperatureSensorData)

public:
	CWnd_TemperatureSensorData();
	virtual ~CWnd_TemperatureSensorData();

	UINT m_nHeaderCnt = enTempData_MAX;
	CString m_SzHeaderName[enTempData_MAX];

	//---Class 정의 시 세팅 해야함.
	void SetHeader(UINT nHeaderCnt){

		if (nHeaderCnt > enTempData_MAX)
		{
			nHeaderCnt = enTempData_MAX;
		}
		m_nHeaderCnt = nHeaderCnt;
	};

	void SetHeaderName(UINT nCnt, CString szHeader){

		if (nCnt > enTempData_MAX)
		{
			nCnt = enTempData_MAX-1;
		}
		m_SzHeaderName[nCnt] = szHeader;
	};

	CVGStatic		m_st_Header[enTempData_MAX];
	CVGStatic		m_st_Data[enTempData_inputmax][enTempData_MAX];

//	CVGStatic		m_st_ResultView;
	BOOL			m_bMode = FALSE;

	void SetDisplayMode(BOOL bMode){
		m_bMode = bMode;
	};

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	void DataSetting(UINT nDataIdx, UINT nIdx, BOOL MODE, double dData);
	void DataEachReset(UINT nDataIdx, UINT nIdx);
	void DataReset();
	void DataDisplay(ST_TemperatureSensor_Data stData);
};


#endif // Wnd_RotateData_h__
