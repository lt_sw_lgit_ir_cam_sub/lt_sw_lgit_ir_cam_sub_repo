﻿#ifndef Def_T_SFR_h__
#define Def_T_SFR_h__

#include <afxwin.h>

#include "Def_T_Common.h"

#pragma pack(push,1)

typedef enum enROI_SFR
{
	ROI_SFR_FST = 0,
	ROI_SFR_LST = 29,
	ROI_SFR_Max,
};

typedef enum enUI_SFR_Group
{
	ROI_SFR_GROUP_FST = 0,
	ROI_SFR_GROUP_LST = 8,
	ROI_SFR_GROUP_Max,
};

typedef enum enUI_SFR_Tilt
{
	ROI_SFR_TILT_FST = 0,
	ROI_SFR_TILT_LST = 3,
	ROI_SFR_TILT_Max,
};

//typedef enum enIndex_SFR
//{
//	ITM_SFR_G0 = 0,
//	ITM_SFR_G1,
//	ITM_SFR_G2,
//	ITM_SFR_G3,
//	ITM_SFR_G4,
//	ITM_SFR_Max,
//};

//typedef enum enIndex_SFR_Tilt
//{
//	ITM_SFR_X1_Tilt_UpperLimit = 0,
//	ITM_SFR_X2_Tilt_UpperLimit,
//	ITM_SFR_Y1_Tilt_UpperLimit,
//	ITM_SFR_Y2_Tilt_UpperLimit,
//	ITM_SFR_TiltMax,
//};

//static LPCTSTR	g_szSFR[] =
//{
//	_T("G0"),
//	_T("G1"),
//	_T("G2"),
//	_T("G3"),
//	_T("G4"),
//	NULL
//};

//static LPCTSTR	g_szSFRTilt[] =
//{
//	_T("X1_Tilt_UpperLimit"),
//	_T("X2_Tilt_UpperLimit"),
//	_T("Y1_Tilt_UpperLimit"),
//	_T("Y2_Tilt_UpperLimit"),
//	NULL
//};
typedef struct _tag_Input_SFR
{

	ST_Comm_Spec		stSpecMin;
	ST_Comm_Spec		stSpecMax;


	
	_tag_Input_SFR()
	{
		Reset();
	};

	void Reset()
	{
	
		stSpecMin.Reset();
		stSpecMax.Reset();

	
	};

	_tag_Input_SFR& operator= (_tag_Input_SFR& ref)
	{
		

		stSpecMin = ref.stSpecMin;
		stSpecMax = ref.stSpecMax;


		return *this;
	};

}ST_Input_SFR, *PST_Input_SFR;


typedef struct _tag_Input_SFR_Group
{
	//!SH _181129: ROI는 필요 없으나 뿌릴 위치는 정해야함.
	BOOL		bEnable;
	double		dbOffset;

	ST_Comm_Spec		stSpecMin;
	ST_Comm_Spec		stSpecMax;

	_tag_Input_SFR_Group()
	{
		Reset();
	};

	void Reset()
	{

		bEnable = FALSE;
		dbOffset = 0.0;

		stSpecMin.Reset();
		stSpecMax.Reset();

	};

	_tag_Input_SFR_Group& operator= (_tag_Input_SFR_Group& ref)
	{

		bEnable = ref.bEnable;
		dbOffset = ref.dbOffset;

		stSpecMin = ref.stSpecMin;
		stSpecMax = ref.stSpecMax;

		return *this;
	};

}ST_Input_SFR_Group, *PST_Input_SFR_Group;


typedef struct _tag_Input_SFR_Tilt
{
	//!SH _181129: ROI는 필요 없으나 뿌릴 위치는 정해야함.
	BOOL		bEnable;
	double		dbOffset;

	UINT		nStandardGroup;
	UINT		nSubtractGroup;

	int		iValue;
	double	dbValue;

	//ST_Spec		stSpecMin;
	//ST_Spec		stSpecMax;

	_tag_Input_SFR_Tilt()
	{
		Reset();
	};

	void Reset()
	{

		bEnable = FALSE;
		dbOffset = 0.0;

		nStandardGroup = 0;
		nSubtractGroup = 0;

		iValue = 0;
		dbValue = 0.0;
		//	stSpecMin.Reset();
		//	stSpecMax.Reset();

	};

	_tag_Input_SFR_Tilt& operator= (_tag_Input_SFR_Tilt& ref)
	{

		bEnable = ref.bEnable;
		dbOffset = ref.dbOffset;

		nStandardGroup = ref.nStandardGroup;
		nSubtractGroup = ref.nSubtractGroup;

		iValue = ref.iValue;
		dbValue = ref.dbValue;

		//stSpecMin = ref.stSpecMin;
		//stSpecMax = ref.stSpecMax;

		return *this;
	};

}ST_Input_SFR_Tilt, *PST_Input_SFR_Tilt;

// SFR 영역에 대한 옵션
typedef struct _tag_SFR_Opt
{
	// 3. CalcSFR
	ST_Input_SFR		stInput[ROI_SFR_Max];
	ST_Input_SFR_Group	stInput_Group[ROI_SFR_GROUP_Max];
	ST_Input_SFR_Tilt	stInput_Tilt[ROI_SFR_TILT_Max];

	UINT nCaptureCnt;
	UINT nCaptureDelay;

	UINT nResultmode;

	//UINT nX1_Tilt_A;
	//UINT nX1_Tilt_B;
	//UINT nX2_Tilt_A;
	//UINT nX2_Tilt_B;
	//UINT nY1_Tilt_A;
	//UINT nY1_Tilt_B;
	//UINT nY2_Tilt_A;
	//UINT nY2_Tilt_B;

	ST_RegionROI stRegion[ROI_SFR_Max];

	//ST_Comm_Spec stSpec_Min[ROI_SFR_Max];
	//ST_Comm_Spec stSpec_Max[ROI_SFR_Max];

	//ST_Comm_Spec stSpec_F_Min[ITM_SFR_Max];
	//ST_Comm_Spec stSpec_F_Max[ITM_SFR_Max];

	//ST_Comm_Spec stSpec_Tilt_Min[ITM_SFR_TiltMax];
	//ST_Comm_Spec stSpec_Tilt_Max[ITM_SFR_TiltMax];

	_tag_SFR_Opt()
	{
		nCaptureCnt		= 0;
		nCaptureDelay	= 0;
		nResultmode		= 0;

		//nX1_Tilt_A = 0;
		//nX1_Tilt_B = 0;
		//nX2_Tilt_A = 0;
		//nX2_Tilt_B = 0;
		//nY1_Tilt_A = 0;
		//nY1_Tilt_B = 0;
		//nY2_Tilt_A = 0;
		//nY2_Tilt_B = 0;

		//for (UINT nIdx = 0; nIdx < ROI_SFR_Max; nIdx++)
		//{
		//	stRegion[nIdx].Reset();
		//	//stSpec_Min[nIdx].Reset();
		//	//stSpec_Max[nIdx].Reset();
		//}

		/*	
		for (UINT nIdx = 0; nIdx < ITM_SFR_Max; nIdx++)
		{
			stSpec_F_Min[nIdx].Reset();
			stSpec_F_Max[nIdx].Reset();
		}
		*/

		for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
		{
			stInput[nROI].Reset();
		}

		for (UINT nROI = 0; nROI < ROI_SFR_GROUP_Max; nROI++)
		{
			stInput_Group[nROI].Reset();
		}

		for (UINT nROI = 0; nROI < ROI_SFR_TILT_Max; nROI++)
		{
			stInput_Tilt[nROI].Reset();
		}
	};

	_tag_SFR_Opt& operator= (_tag_SFR_Opt& ref)
	{
		nCaptureCnt		= ref.nCaptureCnt;
		nCaptureDelay	= ref.nCaptureDelay;
		nResultmode		= ref.nResultmode;

		//nX1_Tilt_A		= ref.nX1_Tilt_A;
		//nX1_Tilt_B		= ref.nX1_Tilt_B;
		//nX2_Tilt_A		= ref.nX2_Tilt_A;
		//nX2_Tilt_B		= ref.nX2_Tilt_B;
		//nY1_Tilt_A		= ref.nY1_Tilt_A;
		//nY1_Tilt_B		= ref.nY1_Tilt_B;
		//nY2_Tilt_A		= ref.nY2_Tilt_A;
		//nY2_Tilt_B		= ref.nY2_Tilt_B;

		//for (UINT nIdx = 0; nIdx < ROI_SFR_Max; nIdx++)
		//{
		//	stRegion[nIdx]	 = ref.stRegion[nIdx];
		//	//stSpec_Min[nIdx] = ref.stSpec_Min[nIdx];
		//	//stSpec_Max[nIdx] = ref.stSpec_Max[nIdx];
		//}

		//for (UINT nIdx = 0; nIdx < ITM_SFR_Max; nIdx++)
		//{
		//	stSpec_F_Min[nIdx] = ref.stSpec_F_Min[nIdx];
		//	stSpec_F_Max[nIdx] = ref.stSpec_F_Max[nIdx];
		//}

		for (UINT nIdx = 0; nIdx < ROI_SFR_Max; nIdx++)
		{
			stInput[nIdx] = ref.stInput[nIdx];
		}

		for (UINT nIdx = 0; nIdx < ROI_SFR_GROUP_Max; nIdx++)
		{
			stInput_Group[nIdx] = ref.stInput_Group[nIdx];
		}

		for (UINT nIdx = 0; nIdx < ROI_SFR_TILT_Max; nIdx++)
		{
			stInput_Tilt[nIdx] = ref.stInput_Tilt[nIdx];
		}

		return *this;
	};

}ST_SFR_Opt, *PST_SFR_Opt;

//각각 영역에 대한 Data정보
typedef struct _tag_SFR_Data
{
	// 최종 결과
	UINT	nResult;
	UINT	nTiltResult;
	UINT    nEachResult[ROI_SFR_Max];
//	UINT    nEachResult_G[ITM_SFR_Max];
	UINT    nEachResult_G[ROI_SFR_GROUP_Max];
	double    dbEachResultGroup[ROI_SFR_GROUP_Max];
	//double	  dbEachResultTilt[ROI_SFR_TILT_Max];

	//double	dbMinValue[ITM_SFR_Max];
	double	dbValue[ROI_SFR_Max];
	CRect	rtTestPicROI[ROI_SFR_Max];
	BOOL	bUse[ROI_SFR_Max];
		
	//double dbTiltdata[ITM_SFR_TiltMax];
	double dbTiltdata[ROI_SFR_TILT_Max];
	UINT    nEachTiltResult[ROI_SFR_TILT_Max];

	_tag_SFR_Data()
	{
		nResult = 1;
		nTiltResult = 1;

		for (UINT nIdex = 0; nIdex < ROI_SFR_Max; nIdex++)
		{
			bUse[nIdex]			= 0;
			nEachResult[nIdex]	= 1;
			dbValue[nIdex]		= 0.0;
			rtTestPicROI[nIdex].SetRectEmpty();
		}

		for (UINT nIdex = 0; nIdex < ROI_SFR_GROUP_Max; nIdex++)
		{
			nEachResult_G[nIdex] = 1;
			dbEachResultGroup[nIdex] = 0;
		}

		//for (UINT nIdex = 0; nIdex < ITM_SFR_Max; nIdex++)
		//{
		//	nEachResult_G[nIdex] = 1;
		//	dbMinValue[nIdex] = 0.0;
		//}

		for (UINT nIdex = 0; nIdex < ROI_SFR_TILT_Max; nIdex++)
		{
			nEachTiltResult[nIdex] = 1;
			dbTiltdata[nIdex] = 0.0;
		}
	};

	void Reset()
	{
		nResult = 1;

		nTiltResult = 1;
		for (UINT nIdex = 0; nIdex < ROI_SFR_Max; nIdex++)
		{
			bUse[nIdex]			= 0;
			nEachResult[nIdex]	= 1;
			dbValue[nIdex]		= 0.0;
			rtTestPicROI[nIdex].SetRectEmpty();
		}

		for (UINT nIdex = 0; nIdex < ROI_SFR_GROUP_Max; nIdex++)
		{
			nEachResult_G[nIdex] = 1;
			dbEachResultGroup[nIdex] = 0;
		}

		//for (UINT nIdex = 0; nIdex < ITM_SFR_Max; nIdex++)
		//{
		//	nEachResult_G[nIdex] = 1;
		//	dbMinValue[nIdex] = 0.0;
		//}

		for (UINT nIdex = 0; nIdex < ROI_SFR_TILT_Max; nIdex++)
		{
			nEachTiltResult[nIdex] = 1;
			dbTiltdata[nIdex] = 0.0;
		}
	};
	void FailData(){
		nResult = 0;
		nTiltResult = 0;

		for (UINT nIdex = 0; nIdex < ROI_SFR_Max; nIdex++)
		{
			bUse[nIdex]			= 0;
			nEachResult[nIdex]	= 0;
			dbValue[nIdex]		= 0.0;
			rtTestPicROI[nIdex].SetRectEmpty();
		}

		for (UINT nIdex = 0; nIdex < ROI_SFR_GROUP_Max; nIdex++)
		{
			nEachResult_G[nIdex] = 1;
			dbEachResultGroup[nIdex] = 0;
		}

		//for (UINT nIdex = 0; nIdex < ITM_SFR_Max; nIdex++)
		//{
		//	nEachResult_G[nIdex] = 1;
		//	dbMinValue[nIdex] = 0.0;
		//}

		for (UINT nIdex = 0; nIdex < ROI_SFR_TILT_Max; nIdex++)
		{
			nEachTiltResult[nIdex] = 1;
			dbTiltdata[nIdex] = 0.0;
		}
	};
	_tag_SFR_Data& operator= (_tag_SFR_Data& ref)
	{
		nResult = ref.nResult;
		nTiltResult = ref.nTiltResult;

		for (UINT nIdex = 0; nIdex < ROI_SFR_Max; nIdex++)
		{
			bUse[nIdex]			= ref.bUse[nIdex];
			nEachResult[nIdex]	= ref.nEachResult[nIdex];
			dbValue[nIdex]		= ref.dbValue[nIdex];
			rtTestPicROI[nIdex] = ref.rtTestPicROI[nIdex];
		}

		for (UINT nIdex = 0; nIdex < ROI_SFR_GROUP_Max; nIdex++)
		{
			nEachResult_G[nIdex] = ref.nEachResult_G[nIdex];
			dbEachResultGroup[nIdex] = ref.dbEachResultGroup[nIdex];
		}

		//for (UINT nIdex = 0; nIdex < ITM_SFR_Max; nIdex++)
		//{
		//	nEachResult_G[nIdex]  = ref.nEachResult_G[nIdex];
		//	dbMinValue[nIdex]	  = ref.dbMinValue[nIdex];
		//}

		for (UINT nIdex = 0; nIdex < ROI_SFR_TILT_Max; nIdex++)
		{
			nEachTiltResult[nIdex] = ref.nEachTiltResult[nIdex];
			dbTiltdata[nIdex] = ref.dbTiltdata[nIdex];
		}
		return *this;
	};

}ST_SFR_Data, *PST_SFR_Data;

typedef struct _tag_TI_SFR
{
	ST_SFR_Opt	stSFROpt;	// 검사 기준 데이터
	ST_SFR_Data	stSFRData;	// 측정 데이터

	_tag_TI_SFR()
	{
	};

	void Reset()
	{
		stSFRData.Reset();
	};

	_tag_TI_SFR& operator= (_tag_TI_SFR& ref)
	{
		stSFROpt	= ref.stSFROpt;
		stSFRData	= ref.stSFRData;

		return *this;
	};

}ST_TI_SFR, *PST_TI_SFR;

#pragma pack(pop)

#endif // Def_SFR_h__