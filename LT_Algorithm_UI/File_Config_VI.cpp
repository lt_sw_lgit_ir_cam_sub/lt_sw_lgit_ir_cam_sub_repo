//*****************************************************************************
// Filename	: 	File_Config_VI.cpp
// Created	:	2018/1/26 - 14:58
// Modified	:	2018/1/26 - 14:58
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#include "File_Config_VI.h"

#define		AppName_ECurrent			_T("ECurrent")
#define		AppName_OpticalCenter		_T("OpticalCenter")
#define		AppName_DistortionOpt		_T("Distortion")
#define		AppName_DynamicOpt			_T("Dynamic")
#define		AppName_IntensityOpt		_T("Intensity")
#define		AppName_RotateOpt			_T("Rotate")
#define		AppName_SFROpt				_T("SFR")
#define		AppName_SNR_BWOpt			_T("SNR_BW")
#define		AppName_SNR_IQOpt			_T("SNR_IQ")
#define		AppName_TiltOpt				_T("Tilt")
#define		AppName_FovOpt				_T("FOV")
#define		AppName_DefectPixelOpt		_T("DefectPixel")
#define		AppName_ShadingOpt			_T("Shading")
#define		AppName_SNR_LightOpt		_T("SNR_Light")
#define		AppName_ParticleOpt			_T("Particle")
#define		AppName_Particle_EntryOpt	_T("Particle_Entry")
#define		AppName_ActiveAlignOpt		_T("ActiveAlign")
#define		AppName_TorqueOpt			_T("Torque")
#define		AppName_HotPixelOpt			_T("HotPixel")
#define		AppName_FPNOpt				_T("FPN")
#define		AppName_3DDepthOpt			_T("3DDepth")
#define		AppName_EEPROMOpt			_T("EEPROM")
#define		AppName_TemperatureOpt		_T("TemperatureSensor")
#define		AppName_FocusOpt			_T("Focus")

static LPCTSTR g_szAppName_SFR[] =
{
	_T("SFR_A"),
	_T("SFR_B"),
	_T("SFR_C"),
	NULL,
};


CFile_VI_Config::CFile_VI_Config()
{
}


CFile_VI_Config::~CFile_VI_Config()
{
}

//=============================================================================
// Method		: // Load_Rect
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in LPCTSTR szAppName
// Parameter	: __in ST_RegionROI & stRegion
// Qualifier	:
// Last Update	: 2018/2/18 - 15:02
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config:: Load_Rect(__in LPCTSTR szPath, __in LPCTSTR szAppName, __in ST_RegionROI& stRegion)
{
	TCHAR   inBuff[255] = { 0, };

	GetPrivateProfileString(szAppName, _T("left"), _T("0"), inBuff, 80, szPath);
	stRegion.rtROI.left = _ttoi(inBuff);

	GetPrivateProfileString(szAppName, _T("top"), _T("0"), inBuff, 80, szPath);
	stRegion.rtROI.top = _ttoi(inBuff);

	GetPrivateProfileString(szAppName, _T("right"), _T("0"), inBuff, 80, szPath);
	stRegion.rtROI.right = _ttoi(inBuff);

	GetPrivateProfileString(szAppName, _T("bottom"), _T("0"), inBuff, 80, szPath);
	stRegion.rtROI.bottom = _ttoi(inBuff);

	GetPrivateProfileString(szAppName, _T("MarkType"), _T("0"), inBuff, 255, szPath);
	stRegion.nMarkType = _ttoi(inBuff);

	GetPrivateProfileString(szAppName, _T("ItemPos"), _T("0"), inBuff, 255, szPath);
	stRegion.nItemPos = _ttoi(inBuff);

	GetPrivateProfileString(szAppName, _T("MarkColor"), _T("0"), inBuff, 255, szPath);
	stRegion.nMarkColor = _ttoi(inBuff);

	GetPrivateProfileString(szAppName, _T("Brightness"), _T("0"), inBuff, 255, szPath);
	stRegion.nBrightness = _ttoi(inBuff);

	GetPrivateProfileString(szAppName, _T("Offset"), _T("0"), inBuff, 255, szPath);
	stRegion.dbOffset = _ttof(inBuff);

	GetPrivateProfileString(szAppName, _T("OffsetSub"), _T("0"), inBuff, 255, szPath);
	stRegion.dbOffsetSub = _ttof(inBuff);

	GetPrivateProfileString(szAppName, _T("LinePair"), _T("0"), inBuff, 255, szPath);
	stRegion.dbLinePair = _ttof(inBuff);

	GetPrivateProfileString(szAppName, _T("Enable"), _T("1"), inBuff, 255, szPath);
	stRegion.bEnable = _ttoi(inBuff);

	GetPrivateProfileString(szAppName, _T("Ellipse"), _T("1"), inBuff, 255, szPath);
	stRegion.bEllipse = _ttoi(inBuff);

	GetPrivateProfileString(szAppName, _T("BruiseConc"), _T("0"), inBuff, 255, szPath);
	stRegion.dbBruiseConc = _ttof(inBuff);

	GetPrivateProfileString(szAppName, _T("BruiseSize"), _T("0"), inBuff, 255, szPath);
	stRegion.dbBruiseSize = _ttof(inBuff);

	GetPrivateProfileString(szAppName, _T("Sharpness"), _T("0"), inBuff, 255, szPath);
	stRegion.nSharpness = _ttoi(inBuff);

	GetPrivateProfileString(szAppName, _T("Group"), _T("0"), inBuff, 255, szPath);
	stRegion.nSFR_Group = _ttoi(inBuff);

	return TRUE;
}

//=============================================================================
// Method		: //Save_Rect
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in LPCTSTR szAppName
// Parameter	: __in const ST_RegionROI * pstRegion
// Qualifier	:
// Last Update	: 2018/2/18 - 15:02
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_Rect(__in LPCTSTR szPath, __in LPCTSTR szAppName, __in const ST_RegionROI* pstRegion)
{
	CString strValue;

	strValue.Format(_T("%d"), pstRegion->rtROI.left);
	WritePrivateProfileString(szAppName, _T("left"), strValue, szPath);

	strValue.Format(_T("%d"), pstRegion->rtROI.top);
	WritePrivateProfileString(szAppName, _T("top"), strValue, szPath);

	strValue.Format(_T("%d"), pstRegion->rtROI.right);
	WritePrivateProfileString(szAppName, _T("right"), strValue, szPath);

	strValue.Format(_T("%d"), pstRegion->rtROI.bottom);
	WritePrivateProfileString(szAppName, _T("bottom"), strValue, szPath);

	strValue.Format(_T("%d"), pstRegion->nMarkType);
	WritePrivateProfileString(szAppName, _T("MarkType"), strValue, szPath);

	strValue.Format(_T("%d"), pstRegion->nItemPos);
	WritePrivateProfileString(szAppName, _T("ItemPos"), strValue, szPath);

	strValue.Format(_T("%d"), pstRegion->nMarkColor);
	WritePrivateProfileString(szAppName, _T("MarkColor"), strValue, szPath);

	strValue.Format(_T("%d"), pstRegion->nBrightness);
	WritePrivateProfileString(szAppName, _T("Brightness"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstRegion->dbOffset);
	WritePrivateProfileString(szAppName, _T("Offset"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstRegion->dbOffsetSub);
	WritePrivateProfileString(szAppName, _T("OffsetSub"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstRegion->dbLinePair);
	WritePrivateProfileString(szAppName, _T("LinePair"), strValue, szPath);

	strValue.Format(_T("%d"), pstRegion->bEnable);
	WritePrivateProfileString(szAppName, _T("Enable"), strValue, szPath);

	strValue.Format(_T("%d"), pstRegion->bEllipse);
	WritePrivateProfileString(szAppName, _T("Ellipse"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstRegion->dbBruiseConc);
	WritePrivateProfileString(szAppName, _T("BruiseConc"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstRegion->dbBruiseSize);
	WritePrivateProfileString(szAppName, _T("BruiseSize"), strValue, szPath);

	strValue.Format(_T("%d"), pstRegion->nSharpness);
	WritePrivateProfileString(szAppName, _T("Sharpness"), strValue, szPath);

	strValue.Format(_T("%d"), pstRegion->nSFR_Group);
	WritePrivateProfileString(szAppName, _T("Group"), strValue, szPath);

	return TRUE;
}

//=============================================================================
// Method		: Load_ECurrentFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_ECurrent_Opt & stConfigInfo
// Qualifier	:
// Last Update	: 2018/2/26 - 17:42
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_ECurrentFile(__in LPCTSTR szPath, __out ST_ECurrent_Opt& stConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_ECurrent;

	for (UINT nIdx = 0; nIdx < Spec_ECurrent_Max; nIdx++)
	{
		strAppName.Format(_T("%s_SPEC_%d"), AppName_ECurrent, nIdx);
		for (int t = 0; t < 2;t++)
		{
			CString str;
			if (t ==0)
			{
				str = _T("Offset");
			}
			else{
				str = _T("Offset_right");

			}
			GetPrivateProfileString(strAppName, str, _T("0"), inBuff, 255, szPath);
			stConfigInfo.dbOffset[t][nIdx] = _ttof(inBuff);

		}
	
		GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_nValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_dbValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].dbValue = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_nValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_dbValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].dbValue = _ttof(inBuff);
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_ECurrentFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_ECurrent_Opt * pstConfigInfo
// Qualifier	:
// Last Update	: 2018/2/26 - 17:42
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_ECurrentFile(__in LPCTSTR szPath, __in const ST_ECurrent_Opt* pstConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfigInfo)
		return FALSE;

	CString strValue;
	CString	strAppName = AppName_ECurrent;

	for (UINT nIdx = 0; nIdx < Spec_ECurrent_Max; nIdx++)
	{
		strAppName.Format(_T("%s_SPEC_%d"), AppName_ECurrent, nIdx);


		for (int t = 0; t < 2; t++)
		{
			CString str;
			if (t == 0)
			{
				str = _T("Offset");
			}
			else{
				str = _T("Offset_right");

			}

			strValue.Format(_T("%.2f"), pstConfigInfo->dbOffset[t][nIdx]);
			WritePrivateProfileString(strAppName, str, strValue, szPath);

		}


		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min[nIdx].bEnable);
		WritePrivateProfileString(strAppName, _T("Min_Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min[nIdx].iValue);
		WritePrivateProfileString(strAppName, _T("Min_nValue"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Min[nIdx].dbValue);
		WritePrivateProfileString(strAppName, _T("Min_dbValue"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max[nIdx].bEnable);
		WritePrivateProfileString(strAppName, _T("Max_Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max[nIdx].iValue);
		WritePrivateProfileString(strAppName, _T("Max_nValue"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Max[nIdx].dbValue);
		WritePrivateProfileString(strAppName, _T("Max_dbValue"), strValue, szPath);
	}

	return TRUE;
}

//=============================================================================
// Method		: Load_OpticalCenterFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_OpticalCenter_Opt & stConfigInfo
// Qualifier	:
// Last Update	: 2018/2/18 - 12:33
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_OpticalCenterFile(__in LPCTSTR szPath, __out ST_OpticalCenter_Opt& stConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_OpticalCenter;

	GetPrivateProfileString(strAppName, _T("StandarX"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.nStandarX = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("StandarY"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.nStandarY = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("OffsetX"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.iOffsetX = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("OffsetY"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.iOffsetY = _ttoi(inBuff);

	for (UINT nIdx = 0; nIdx < Spec_OC_Max; nIdx++)
	{
		strAppName.Format(_T("%s_SPEC_%d"), AppName_OpticalCenter, nIdx);

		GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_nValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_dbValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].dbValue = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_nValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_dbValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].dbValue = _ttof(inBuff);
	}

	// ROI 历厘
	strAppName.Format(_T("%s_ROI_0"), AppName_OpticalCenter);
	Load_Rect(szPath, strAppName, stConfigInfo.stRegion);

	return TRUE;
}

//=============================================================================
// Method		: Save_OpticalCenterFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_OpticalCenter_Opt * pstConfigInfo
// Qualifier	:
// Last Update	: 2018/2/18 - 12:44
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_OpticalCenterFile(__in LPCTSTR szPath, __in const ST_OpticalCenter_Opt* pstConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfigInfo)
		return FALSE;

	CString strValue;
	CString	strAppName = AppName_OpticalCenter;

	strValue.Format(_T("%d"), pstConfigInfo->nStandarX);
	WritePrivateProfileString(strAppName, _T("StandarX"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfigInfo->nStandarY);
	WritePrivateProfileString(strAppName, _T("StandarY"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfigInfo->iOffsetX);
	WritePrivateProfileString(strAppName, _T("OffsetX"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfigInfo->iOffsetY);
	WritePrivateProfileString(strAppName, _T("OffsetY"), strValue, szPath);

	for (UINT nIdx = 0; nIdx < Spec_OC_Max; nIdx++)
	{
		strAppName.Format(_T("%s_SPEC_%d"), AppName_OpticalCenter, nIdx);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min[nIdx].bEnable);
		WritePrivateProfileString(strAppName, _T("Min_Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min[nIdx].iValue);
		WritePrivateProfileString(strAppName, _T("Min_nValue"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Min[nIdx].dbValue);
		WritePrivateProfileString(strAppName, _T("Min_dbValue"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max[nIdx].bEnable);
		WritePrivateProfileString(strAppName, _T("Max_Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max[nIdx].iValue);
		WritePrivateProfileString(strAppName, _T("Max_nValue"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Max[nIdx].dbValue);
		WritePrivateProfileString(strAppName, _T("Max_dbValue"), strValue, szPath);
	}

	// ROI 历厘
	strAppName.Format(_T("%s_ROI_0"), AppName_OpticalCenter);
	Save_Rect(szPath, strAppName, &pstConfigInfo->stRegion);

	return TRUE;
}

//=============================================================================
// Method		: Load_RotateFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_Rotate_Opt & stConfigInfo
// Qualifier	:
// Last Update	: 2017/12/3 - 13:45
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_RotateFile(__in LPCTSTR szPath, __out ST_Rotate_Opt& stConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppTotal = AppName_RotateOpt;
	CString	strAppName;

	GetPrivateProfileString(strAppTotal, _T("Standard"), _T("0"), inBuff, 80, szPath);
	stConfigInfo.dbStandard = _ttof(inBuff);
	
	GetPrivateProfileString(strAppTotal, _T("Offset"), _T("0"), inBuff, 80, szPath);
	stConfigInfo.dbOffset = _ttof(inBuff);

	strAppName.Format(_T("%s_SPEC"), AppName_RotateOpt);

	GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Min.bEnable = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("Min_nValue"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Min.iValue = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("Min_dbValue"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Min.dbValue = _ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Max.bEnable = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("Max_nValue"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Max.iValue = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("Max_dbValue"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Max.dbValue = _ttof(inBuff);

	// ROI 历厘
	for (UINT nIdx = 0; nIdx < ROI_Rot_Max; nIdx++)
	{
		strAppName.Format(_T("%s_ROI_%d"), AppName_RotateOpt, nIdx);

		Load_Rect(szPath, strAppName, stConfigInfo.stRegion[nIdx]);
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_RotateFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_Rotate_Opt * pstConfigInfo
// Qualifier	:
// Last Update	: 2017/12/3 - 13:56
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_RotateFile(__in LPCTSTR szPath, __in const ST_Rotate_Opt* pstConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfigInfo)
		return FALSE;

	CString strValue;
	CString	strAppName = AppName_RotateOpt;


	strValue.Format(_T("%.2f"), pstConfigInfo->dbStandard);
	WritePrivateProfileString(strAppName, _T("Standard"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstConfigInfo->dbOffset);
	WritePrivateProfileString(strAppName, _T("Offset"), strValue, szPath);

	strAppName.Format(_T("%s_SPEC"), AppName_RotateOpt);

	strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min.bEnable);
	WritePrivateProfileString(strAppName, _T("Min_Enable"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min.iValue);
	WritePrivateProfileString(strAppName, _T("Min_nValue"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Min.dbValue);
	WritePrivateProfileString(strAppName, _T("Min_dbValue"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max.bEnable);
	WritePrivateProfileString(strAppName, _T("Max_Enable"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max.iValue);
	WritePrivateProfileString(strAppName, _T("Max_nValue"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Max.dbValue);
	WritePrivateProfileString(strAppName, _T("Max_dbValue"), strValue, szPath);

	// ROI 历厘
	for (UINT nIdx = 0; nIdx < ROI_Rot_Max; nIdx++)
	{
		strAppName.Format(_T("%s_ROI_%d"), AppName_RotateOpt, nIdx);

		Save_Rect(szPath, strAppName, &pstConfigInfo->stRegion[nIdx]);
	}

	return TRUE;
}

//=============================================================================
// Method		: Load_DistortionFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_Distortion_Opt & stConfigInfo
// Qualifier	:
// Last Update	: 2017/12/3 - 13:40
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_DistortionFile(__in LPCTSTR szPath, __out ST_Distortion_Opt& stConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_DistortionOpt;

	strAppName.Format(_T("%s_SPEC"), AppName_DistortionOpt);

	GetPrivateProfileString(strAppName, _T("Offset"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.dbOffset = _ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Min.bEnable = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("Min_nValue"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Min.iValue = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("Min_dbValue"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Min.dbValue = _ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Max.bEnable = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("Max_nValue"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Max.iValue = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("Max_dbValue"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Max.dbValue = _ttof(inBuff);

	// ROI 历厘
	for (UINT nIdx = 0; nIdx < ROI_Dis_Max; nIdx++)
	{
		strAppName.Format(_T("%s_ROI_%d"), AppName_DistortionOpt, nIdx);

		Load_Rect(szPath, strAppName, stConfigInfo.stRegion[nIdx]);
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_DistortionFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_Distortion_Opt * pstConfigInfo
// Qualifier	:
// Last Update	: 2017/12/3 - 13:49
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_DistortionFile(__in LPCTSTR szPath, __in const ST_Distortion_Opt* pstConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfigInfo)
		return FALSE;

	CString strValue;
	CString	strAppName = AppName_DistortionOpt;

	strAppName.Format(_T("%s_SPEC"), AppName_DistortionOpt);

	strValue.Format(_T("%.2f"), pstConfigInfo->dbOffset);
	WritePrivateProfileString(strAppName, _T("Offset"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min.bEnable);
	WritePrivateProfileString(strAppName, _T("Min_Enable"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min.iValue);
	WritePrivateProfileString(strAppName, _T("Min_nValue"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Min.dbValue);
	WritePrivateProfileString(strAppName, _T("Min_dbValue"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max.bEnable);
	WritePrivateProfileString(strAppName, _T("Max_Enable"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max.iValue);
	WritePrivateProfileString(strAppName, _T("Max_nValue"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Max.dbValue);
	WritePrivateProfileString(strAppName, _T("Max_dbValue"), strValue, szPath);

	// ROI 历厘
	for (UINT nIdx = 0; nIdx < ROI_Dis_Max; nIdx++)
	{
		strAppName.Format(_T("%s_ROI_%d"), AppName_DistortionOpt, nIdx);

		Save_Rect(szPath, strAppName, &pstConfigInfo->stRegion[nIdx]);
	}

	return TRUE;
}

//=============================================================================
// Method		: Load_FOVFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_FOV_Opt & stConfigInfo
// Qualifier	:
// Last Update	: 2017/12/3 - 14:29
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_FOVFile(__in LPCTSTR szPath, __out ST_FOV_Opt& stConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_FovOpt;

	for (UINT nIdx = 0; nIdx < Spec_Fov_MAX; nIdx++)
	{
		strAppName.Format(_T("%s_SPEC_%d"), AppName_FovOpt, nIdx);

		GetPrivateProfileString(strAppName, _T("Offset"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.dbOffset[nIdx] = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_nValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_dbValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].dbValue = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_nValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_dbValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].dbValue = _ttof(inBuff);
	}

	// ROI 历厘
	for (UINT nIdx = 0; nIdx < ROI_Fov_Max; nIdx++)
	{
		strAppName.Format(_T("%s_ROI_%d"), AppName_FovOpt, nIdx);

		Load_Rect(szPath, strAppName, stConfigInfo.stRegion[nIdx]);
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_FOVFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_FOV_Opt * pstConfigInfo
// Qualifier	:
// Last Update	: 2017/12/3 - 14:31
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_FOVFile(__in LPCTSTR szPath, __in const ST_FOV_Opt* pstConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfigInfo)
		return FALSE;

	CString strValue;
	CString	strAppName = AppName_FovOpt;

	for (UINT nIdx = 0; nIdx < Spec_Fov_MAX; nIdx++)
	{
		strAppName.Format(_T("%s_SPEC_%d"), AppName_FovOpt, nIdx);

		strValue.Format(_T("%.2f"), pstConfigInfo->dbOffset[nIdx]);
		WritePrivateProfileString(strAppName, _T("Offset"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min[nIdx].bEnable);
		WritePrivateProfileString(strAppName, _T("Min_Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min[nIdx].iValue);
		WritePrivateProfileString(strAppName, _T("Min_nValue"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Min[nIdx].dbValue);
		WritePrivateProfileString(strAppName, _T("Min_dbValue"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max[nIdx].bEnable);
		WritePrivateProfileString(strAppName, _T("Max_Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max[nIdx].iValue);
		WritePrivateProfileString(strAppName, _T("Max_nValue"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Max[nIdx].dbValue);
		WritePrivateProfileString(strAppName, _T("Max_dbValue"), strValue, szPath);
	}

	// ROI 历厘
	for (UINT nIdx = 0; nIdx < ROI_Fov_Max; nIdx++)
	{
		strAppName.Format(_T("%s_ROI_%d"), AppName_FovOpt, nIdx);

		Save_Rect(szPath, strAppName, &pstConfigInfo->stRegion[nIdx]);
	}

	return TRUE;
}

//=============================================================================
// Method		: Load_DynamicFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_Dynamic_Opt & stConfigInfo
// Qualifier	:
// Last Update	: 2017/12/3 - 13:52
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_DynamicFile(__in LPCTSTR szPath, __out ST_Dynamic_Opt& stConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_DynamicOpt;

	for (UINT nIdx = 0; nIdx < Spec_Par_SNR_MAX; nIdx++)
	{
		strAppName.Format(_T("%s_SPEC_%d"), AppName_DynamicOpt, nIdx);

		GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_nValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_dbValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].dbValue = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_nValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_dbValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].dbValue = _ttof(inBuff);
	}

	// ROI 历厘
	for (UINT nIdx = 0; nIdx < ROI_Par_SNR_Max; nIdx++)
	{
		strAppName.Format(_T("%s_ROI_%d"), AppName_DynamicOpt, nIdx);

		Load_Rect(szPath, strAppName, stConfigInfo.stRegion[nIdx]);
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_DynamicFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_Dynamic_Opt * pstConfigInfo
// Qualifier	:
// Last Update	: 2017/12/3 - 13:53
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_DynamicFile(__in LPCTSTR szPath, __in const ST_Dynamic_Opt* pstConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfigInfo)
		return FALSE;

	CString strValue;
	CString	strAppName = AppName_DynamicOpt;

	for (UINT nIdx = 0; nIdx < Spec_Par_SNR_MAX; nIdx++)
	{
		strAppName.Format(_T("%s_SPEC_%d"), AppName_DynamicOpt, nIdx);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min[nIdx].bEnable);
		WritePrivateProfileString(strAppName, _T("Min_Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min[nIdx].iValue);
		WritePrivateProfileString(strAppName, _T("Min_nValue"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Min[nIdx].dbValue);
		WritePrivateProfileString(strAppName, _T("Min_dbValue"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max[nIdx].bEnable);
		WritePrivateProfileString(strAppName, _T("Max_Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max[nIdx].iValue);
		WritePrivateProfileString(strAppName, _T("Max_nValue"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Max[nIdx].dbValue);
		WritePrivateProfileString(strAppName, _T("Max_dbValue"), strValue, szPath);
	}

	// ROI 历厘
	for (UINT nIdx = 0; nIdx < ROI_Par_SNR_Max; nIdx++)
	{
		strAppName.Format(_T("%s_ROI_%d"), AppName_DynamicOpt, nIdx);

		Save_Rect(szPath, strAppName, &pstConfigInfo->stRegion[nIdx]);
	}

	return TRUE;
}

//=============================================================================
// Method		: Load_IntencityFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_Intensity_Opt & stConfigInfo
// Qualifier	:
// Last Update	: 2018/2/20 - 13:45
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_IntencityFile(__in LPCTSTR szPath, __out ST_Intensity_Opt& stConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_IntensityOpt;

	for (UINT nIdx = 0; nIdx < ROI_Intencity_Max; nIdx++)
	{
		strAppName.Format(_T("%s_SPEC_%d"), AppName_IntensityOpt, nIdx);

		GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_nValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_dbValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].dbValue = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_nValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_dbValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].dbValue = _ttof(inBuff);
	}

	// ROI 历厘
	for (UINT nIdx = 0; nIdx < ROI_Intencity_Max; nIdx++)
	{
		strAppName.Format(_T("%s_ROI_%d"), AppName_IntensityOpt, nIdx);

		Load_Rect(szPath, strAppName, stConfigInfo.stRegion[nIdx]);
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_IntencityFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_Intensity_Opt * pstConfigInfo
// Qualifier	:
// Last Update	: 2018/2/20 - 13:46
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_IntencityFile(__in LPCTSTR szPath, __in const ST_Intensity_Opt* pstConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfigInfo)
		return FALSE;

	CString strValue;
	CString	strAppName = AppName_IntensityOpt;

	for (UINT nIdx = 0; nIdx < ROI_Intencity_Max; nIdx++)
	{
		strAppName.Format(_T("%s_SPEC_%d"), AppName_IntensityOpt, nIdx);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min[nIdx].bEnable);
		WritePrivateProfileString(strAppName, _T("Min_Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min[nIdx].iValue);
		WritePrivateProfileString(strAppName, _T("Min_nValue"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Min[nIdx].dbValue);
		WritePrivateProfileString(strAppName, _T("Min_dbValue"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max[nIdx].bEnable);
		WritePrivateProfileString(strAppName, _T("Max_Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max[nIdx].iValue);
		WritePrivateProfileString(strAppName, _T("Max_nValue"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Max[nIdx].dbValue);
		WritePrivateProfileString(strAppName, _T("Max_dbValue"), strValue, szPath);
	}

	// ROI 历厘
	for (UINT nIdx = 0; nIdx < ROI_Intencity_Max; nIdx++)
	{
		strAppName.Format(_T("%s_ROI_%d"), AppName_IntensityOpt, nIdx);

		Save_Rect(szPath, strAppName, &pstConfigInfo->stRegion[nIdx]);
	}

	return TRUE;
}

//=============================================================================
// Method		: Load_SFRFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_SFR_Opt & stConfigInfo
// Qualifier	:
// Last Update	: 2017/12/3 - 14:00
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_SFRFile(__in LPCTSTR szPath, __out ST_SFR_Opt& stConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_SFROpt;

	GetPrivateProfileString(strAppName, _T("CaptureCnt"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.nCaptureCnt = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("CaptureDelay"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.nCaptureDelay = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("Resultmode"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.nResultmode = _ttoi(inBuff);

	//GetPrivateProfileString(strAppName, _T("X1_Tilt_A"), _T("0"), inBuff, 255, szPath);
	//stConfigInfo.nX1_Tilt_A = _ttoi(inBuff);

	//GetPrivateProfileString(strAppName, _T("X1_Tilt_B"), _T("0"), inBuff, 255, szPath);
	//stConfigInfo.nX1_Tilt_B = _ttoi(inBuff);

	//GetPrivateProfileString(strAppName, _T("X2_Tilt_A"), _T("0"), inBuff, 255, szPath);
	//stConfigInfo.nX2_Tilt_A = _ttoi(inBuff);

	//GetPrivateProfileString(strAppName, _T("X2_Tilt_B"), _T("0"), inBuff, 255, szPath);
	//stConfigInfo.nX2_Tilt_B = _ttoi(inBuff);

	//GetPrivateProfileString(strAppName, _T("Y1_Tilt_A"), _T("0"), inBuff, 255, szPath);
	//stConfigInfo.nY1_Tilt_A = _ttoi(inBuff);

	//GetPrivateProfileString(strAppName, _T("Y1_Tilt_B"), _T("0"), inBuff, 255, szPath);
	//stConfigInfo.nY1_Tilt_B = _ttoi(inBuff);

	//GetPrivateProfileString(strAppName, _T("Y2_Tilt_A"), _T("0"), inBuff, 255, szPath);
	//stConfigInfo.nY2_Tilt_A = _ttoi(inBuff);

	//GetPrivateProfileString(strAppName, _T("Y2_Tilt_B"), _T("0"), inBuff, 255, szPath);
	//stConfigInfo.nY2_Tilt_B = _ttoi(inBuff);

	for (UINT nIdx = 0; nIdx < ROI_SFR_Max; nIdx++)
	{
		strAppName.Format(_T("%s_SPEC_%d"), AppName_SFROpt, nIdx);

		GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stInput[nIdx].stSpecMin.bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stInput[nIdx].stSpecMax.bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_Value"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stInput[nIdx].stSpecMin.dbValue = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Value"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stInput[nIdx].stSpecMax.dbValue = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_nValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stInput[nIdx].stSpecMin.iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_nValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stInput[nIdx].stSpecMax.iValue = _ttoi(inBuff);
	}

	
	//for (UINT nIdx = 0; nIdx < ITM_SFR_Max; nIdx++)
	//{
	//	strAppName.Format(_T("%s_F_SPEC_%d"), AppName_SFROpt, nIdx);

	//	GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
	//	stConfigInfo.stSpec_F_Min[nIdx].bEnable = _ttoi(inBuff);

	//	GetPrivateProfileString(strAppName, _T("Min_nValue"), _T("0"), inBuff, 255, szPath);
	//	stConfigInfo.stSpec_F_Min[nIdx].iValue = _ttoi(inBuff);

	//	GetPrivateProfileString(strAppName, _T("Min_dbValue"), _T("0"), inBuff, 255, szPath);
	//	stConfigInfo.stSpec_F_Min[nIdx].dbValue = _ttof(inBuff);

	//	GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
	//	stConfigInfo.stSpec_F_Max[nIdx].bEnable = _ttoi(inBuff);

	//	GetPrivateProfileString(strAppName, _T("Max_nValue"), _T("0"), inBuff, 255, szPath);
	//	stConfigInfo.stSpec_F_Max[nIdx].iValue = _ttoi(inBuff);

	//	GetPrivateProfileString(strAppName, _T("Max_dbValue"), _T("0"), inBuff, 255, szPath);
	//	stConfigInfo.stSpec_F_Max[nIdx].dbValue = _ttof(inBuff);
	//}

	//for (UINT nIdx = 0; nIdx < ITM_SFR_TiltMax; nIdx++)
	//{
	//	strAppName.Format(_T("%s_Tilt_SPEC_%d"), AppName_SFROpt, nIdx);

	//	GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
	//	stConfigInfo.stSpec_Tilt_Min[nIdx].bEnable = _ttoi(inBuff);

	//	GetPrivateProfileString(strAppName, _T("Min_nValue"), _T("0"), inBuff, 255, szPath);
	//	stConfigInfo.stSpec_Tilt_Min[nIdx].iValue = _ttoi(inBuff);

	//	GetPrivateProfileString(strAppName, _T("Min_dbValue"), _T("0"), inBuff, 255, szPath);
	//	stConfigInfo.stSpec_Tilt_Min[nIdx].dbValue = _ttof(inBuff);

	//	GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
	//	stConfigInfo.stSpec_Tilt_Max[nIdx].bEnable = _ttoi(inBuff);

	//	GetPrivateProfileString(strAppName, _T("Max_nValue"), _T("0"), inBuff, 255, szPath);
	//	stConfigInfo.stSpec_Tilt_Max[nIdx].iValue = _ttoi(inBuff);

	//	GetPrivateProfileString(strAppName, _T("Max_dbValue"), _T("0"), inBuff, 255, szPath);
	//	stConfigInfo.stSpec_Tilt_Max[nIdx].dbValue = _ttof(inBuff);
	//}

	// ROI 历厘
	for (UINT nIdx = 0; nIdx < ROI_SFR_Max; nIdx++)
	{
		strAppName.Format(_T("%s_ROI_%d"), AppName_SFROpt, nIdx);

		Load_Rect(szPath, strAppName, stConfigInfo.stRegion[nIdx]);
	}

	for (UINT nROI = 0; nROI < ROI_SFR_GROUP_Max; nROI++)
	{
		strAppName.Format(_T("%s_GROUP_%d"), g_szAppName_SFR, nROI);

		GetPrivateProfileString(strAppName, _T("Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stInput_Group[nROI].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stInput_Group[nROI].stSpecMin.bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stInput_Group[nROI].stSpecMax.bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_Value"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stInput_Group[nROI].stSpecMin.dbValue = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Value"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stInput_Group[nROI].stSpecMax.dbValue = _ttof(inBuff);
	}

	for (UINT nROI = 0; nROI < ROI_SFR_TILT_Max; nROI++)
	{
		strAppName.Format(_T("%s_TILT_%d"), g_szAppName_SFR, nROI);

		GetPrivateProfileString(strAppName, _T("Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stInput_Tilt[nROI].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Standard"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stInput_Tilt[nROI].nStandardGroup = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Subtract"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stInput_Tilt[nROI].nSubtractGroup = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Tilt_Spec"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stInput_Tilt[nROI].dbValue = _ttof(inBuff);
	}

	return TRUE;
}

BOOL CFile_VI_Config::Load_SFRFile_Dist(__in LPCTSTR szPath, __out ST_SFR_Opt* stConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_SFROpt;

	for (int nSFRDist = 0; nSFRDist < 3; nSFRDist++)
	{
		//CString tempCnt;
		//tempCnt.Format(_T("%d"), nSFRDist);

		GetPrivateProfileString(g_szAppName_SFR[nSFRDist], _T("CaptureCnt"), _T("0"), inBuff, 255, szPath);
		stConfigInfo[nSFRDist].nCaptureCnt = _ttoi(inBuff);

		GetPrivateProfileString(g_szAppName_SFR[nSFRDist], _T("CaptureDelay"), _T("0"), inBuff, 255, szPath);
		stConfigInfo[nSFRDist].nCaptureDelay = _ttoi(inBuff);

		GetPrivateProfileString(g_szAppName_SFR[nSFRDist], _T("Resultmode"), _T("0"), inBuff, 255, szPath);
		stConfigInfo[nSFRDist].nResultmode = _ttoi(inBuff);

		//GetPrivateProfileString(g_szAppName_SFR[nSFRDist], _T("X1_Tilt_A"), _T("0"), inBuff, 255, szPath);
		//stConfigInfo[nSFRDist].nX1_Tilt_A = _ttoi(inBuff);

		//GetPrivateProfileString(g_szAppName_SFR[nSFRDist], _T("X1_Tilt_B"), _T("0"), inBuff, 255, szPath);
		//stConfigInfo[nSFRDist].nX1_Tilt_B = _ttoi(inBuff);

		//GetPrivateProfileString(g_szAppName_SFR[nSFRDist], _T("X2_Tilt_A"), _T("0"), inBuff, 255, szPath);
		//stConfigInfo[nSFRDist].nX2_Tilt_A = _ttoi(inBuff);

		//GetPrivateProfileString(g_szAppName_SFR[nSFRDist], _T("X2_Tilt_B"), _T("0"), inBuff, 255, szPath);
		//stConfigInfo[nSFRDist].nX2_Tilt_B = _ttoi(inBuff);

		//GetPrivateProfileString(g_szAppName_SFR[nSFRDist], _T("Y1_Tilt_A"), _T("0"), inBuff, 255, szPath);
		//stConfigInfo[nSFRDist].nY1_Tilt_A = _ttoi(inBuff);

		//GetPrivateProfileString(g_szAppName_SFR[nSFRDist], _T("Y1_Tilt_B"), _T("0"), inBuff, 255, szPath);
		//stConfigInfo[nSFRDist].nY1_Tilt_B = _ttoi(inBuff);

		//GetPrivateProfileString(g_szAppName_SFR[nSFRDist], _T("Y2_Tilt_A"), _T("0"), inBuff, 255, szPath);
		//stConfigInfo[nSFRDist].nY2_Tilt_A = _ttoi(inBuff);

		//GetPrivateProfileString(g_szAppName_SFR[nSFRDist], _T("Y2_Tilt_B"), _T("0"), inBuff, 255, szPath);
		//stConfigInfo[nSFRDist].nY2_Tilt_B = _ttoi(inBuff);

		for (UINT nIdx = 0; nIdx < ROI_SFR_Max; nIdx++)
		{
			//strAppName.Format(_T("%s_SPEC_%d"), AppName_SFROpt, nIdx);
			strAppName.Format(_T("%s_SPEC_%d"), g_szAppName_SFR[nSFRDist], nIdx);

			GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
			stConfigInfo[nSFRDist].stInput[nIdx].stSpecMin.bEnable = _ttoi(inBuff);

			GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
			stConfigInfo[nSFRDist].stInput[nIdx].stSpecMax.bEnable = _ttoi(inBuff);

			GetPrivateProfileString(strAppName, _T("Min_Value"), _T("0"), inBuff, 255, szPath);
			stConfigInfo[nSFRDist].stInput[nIdx].stSpecMin.dbValue = _ttof(inBuff);

			GetPrivateProfileString(strAppName, _T("Max_Value"), _T("0"), inBuff, 255, szPath);
			stConfigInfo[nSFRDist].stInput[nIdx].stSpecMax.dbValue = _ttof(inBuff);

			GetPrivateProfileString(strAppName, _T("Min_nValue"), _T("0"), inBuff, 255, szPath);
			stConfigInfo[nSFRDist].stInput[nIdx].stSpecMin.iValue = _ttoi(inBuff);

			GetPrivateProfileString(strAppName, _T("Max_nValue"), _T("0"), inBuff, 255, szPath);
			stConfigInfo[nSFRDist].stInput[nIdx].stSpecMax.iValue = _ttoi(inBuff);
		}

		//for (UINT nIdx = 0; nIdx < ITM_SFR_Max; nIdx++)
		//{
		//	//strAppName.Format(_T("%s_F_SPEC_%d"), AppName_SFROpt, nIdx);
		//	strAppName.Format(_T("%s_F_SPEC_%d"), g_szAppName_SFR[nSFRDist], nIdx);

		//	GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		//	stConfigInfo[nSFRDist].stSpec_F_Min[nIdx].bEnable = _ttoi(inBuff);

		//	GetPrivateProfileString(strAppName, _T("Min_nValue"), _T("0"), inBuff, 255, szPath);
		//	stConfigInfo[nSFRDist].stSpec_F_Min[nIdx].iValue = _ttoi(inBuff);

		//	GetPrivateProfileString(strAppName, _T("Min_dbValue"), _T("0"), inBuff, 255, szPath);
		//	stConfigInfo[nSFRDist].stSpec_F_Min[nIdx].dbValue = _ttof(inBuff);

		//	GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		//	stConfigInfo[nSFRDist].stSpec_F_Max[nIdx].bEnable = _ttoi(inBuff);

		//	GetPrivateProfileString(strAppName, _T("Max_nValue"), _T("0"), inBuff, 255, szPath);
		//	stConfigInfo[nSFRDist].stSpec_F_Max[nIdx].iValue = _ttoi(inBuff);

		//	GetPrivateProfileString(strAppName, _T("Max_dbValue"), _T("0"), inBuff, 255, szPath);
		//	stConfigInfo[nSFRDist].stSpec_F_Max[nIdx].dbValue = _ttof(inBuff);
		//}

		//for (UINT nIdx = 0; nIdx < ITM_SFR_TiltMax; nIdx++)
		//{
		//	//strAppName.Format(_T("%s_Tilt_SPEC_%d"), AppName_SFROpt, nIdx);
		//	strAppName.Format(_T("%s_Tilt_SPEC_%d"), g_szAppName_SFR[nSFRDist], nIdx);

		//	GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		//	stConfigInfo[nSFRDist].stSpec_Tilt_Min[nIdx].bEnable = _ttoi(inBuff);

		//	GetPrivateProfileString(strAppName, _T("Min_nValue"), _T("0"), inBuff, 255, szPath);
		//	stConfigInfo[nSFRDist].stSpec_Tilt_Min[nIdx].iValue = _ttoi(inBuff);

		//	GetPrivateProfileString(strAppName, _T("Min_dbValue"), _T("0"), inBuff, 255, szPath);
		//	stConfigInfo[nSFRDist].stSpec_Tilt_Min[nIdx].dbValue = _ttof(inBuff);

		//	GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		//	stConfigInfo[nSFRDist].stSpec_Tilt_Max[nIdx].bEnable = _ttoi(inBuff);

		//	GetPrivateProfileString(strAppName, _T("Max_nValue"), _T("0"), inBuff, 255, szPath);
		//	stConfigInfo[nSFRDist].stSpec_Tilt_Max[nIdx].iValue = _ttoi(inBuff);

		//	GetPrivateProfileString(strAppName, _T("Max_dbValue"), _T("0"), inBuff, 255, szPath);
		//	stConfigInfo[nSFRDist].stSpec_Tilt_Max[nIdx].dbValue = _ttof(inBuff);
		//}

		// ROI 历厘
		for (UINT nIdx = 0; nIdx < ROI_SFR_Max; nIdx++)
		{
			//strAppName.Format(_T("%s_ROI_%d"), AppName_SFROpt, nIdx);
			strAppName.Format(_T("%s_ROI_%d"), g_szAppName_SFR[nSFRDist], nIdx);

			Load_Rect(szPath, strAppName, stConfigInfo[nSFRDist].stRegion[nIdx]);
		}


		for (UINT nROI = 0; nROI < ROI_SFR_GROUP_Max; nROI++)
		{
			strAppName.Format(_T("%s_GROUP_%d"), g_szAppName_SFR[nSFRDist], nROI);

			GetPrivateProfileString(strAppName, _T("Enable"), _T("0"), inBuff, 255, szPath);
			stConfigInfo[nSFRDist].stInput_Group[nROI].bEnable = _ttoi(inBuff);

			GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
			stConfigInfo[nSFRDist].stInput_Group[nROI].stSpecMin.bEnable = _ttoi(inBuff);

			GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
			stConfigInfo[nSFRDist].stInput_Group[nROI].stSpecMax.bEnable = _ttoi(inBuff);

			GetPrivateProfileString(strAppName, _T("Min_Value"), _T("0"), inBuff, 255, szPath);
			stConfigInfo[nSFRDist].stInput_Group[nROI].stSpecMin.dbValue = _ttof(inBuff);

			GetPrivateProfileString(strAppName, _T("Max_Value"), _T("0"), inBuff, 255, szPath);
			stConfigInfo[nSFRDist].stInput_Group[nROI].stSpecMax.dbValue = _ttof(inBuff);
		}

		for (UINT nROI = 0; nROI < ROI_SFR_TILT_Max; nROI++)
		{
			strAppName.Format(_T("%s_TILT_%d"), g_szAppName_SFR[nSFRDist], nROI);

			GetPrivateProfileString(strAppName, _T("Enable"), _T("0"), inBuff, 255, szPath);
			stConfigInfo[nSFRDist].stInput_Tilt[nROI].bEnable = _ttoi(inBuff);

			GetPrivateProfileString(strAppName, _T("Standard"), _T("0"), inBuff, 255, szPath);
			stConfigInfo[nSFRDist].stInput_Tilt[nROI].nStandardGroup = _ttoi(inBuff);

			GetPrivateProfileString(strAppName, _T("Subtract"), _T("0"), inBuff, 255, szPath);
			stConfigInfo[nSFRDist].stInput_Tilt[nROI].nSubtractGroup = _ttoi(inBuff);

			GetPrivateProfileString(strAppName, _T("Tilt_Spec"), _T("0"), inBuff, 255, szPath);
			stConfigInfo[nSFRDist].stInput_Tilt[nROI].dbValue = _ttof(inBuff);
		}

	}
	return TRUE;
}
//=============================================================================
// Method		: Save_SFRFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_SFR_Opt * pstConfigInfo
// Qualifier	:
// Last Update	: 2017/12/3 - 14:23
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_SFRFile(__in LPCTSTR szPath, __in const ST_SFR_Opt* pstConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfigInfo)
		return FALSE;

	CString strValue;
	CString	strAppName = AppName_SFROpt;

	strValue.Format(_T("%d"), pstConfigInfo->nCaptureCnt);
	WritePrivateProfileString(strAppName, _T("CaptureCnt"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfigInfo->nCaptureDelay);
	WritePrivateProfileString(strAppName, _T("CaptureDelay"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfigInfo->nResultmode);
	WritePrivateProfileString(strAppName, _T("Resultmode"), strValue, szPath);

	//strValue.Format(_T("%d"), pstConfigInfo->nX1_Tilt_A);
	//WritePrivateProfileString(strAppName, _T("X1_Tilt_A"), strValue, szPath);

	//strValue.Format(_T("%d"), pstConfigInfo->nX1_Tilt_B);
	//WritePrivateProfileString(strAppName, _T("X1_Tilt_B"), strValue, szPath);

	//strValue.Format(_T("%d"), pstConfigInfo->nX2_Tilt_A);
	//WritePrivateProfileString(strAppName, _T("X2_Tilt_A"), strValue, szPath);

	//strValue.Format(_T("%d"), pstConfigInfo->nX2_Tilt_B);
	//WritePrivateProfileString(strAppName, _T("X2_Tilt_B"), strValue, szPath);

	//strValue.Format(_T("%d"), pstConfigInfo->nY1_Tilt_A);
	//WritePrivateProfileString(strAppName, _T("Y1_Tilt_A"), strValue, szPath);

	//strValue.Format(_T("%d"), pstConfigInfo->nY1_Tilt_B);
	//WritePrivateProfileString(strAppName, _T("Y1_Tilt_B"), strValue, szPath);

	//strValue.Format(_T("%d"), pstConfigInfo->nY2_Tilt_A);
	//WritePrivateProfileString(strAppName, _T("Y2_Tilt_A"), strValue, szPath);

	//strValue.Format(_T("%d"), pstConfigInfo->nY2_Tilt_B);
	//WritePrivateProfileString(strAppName, _T("Y2_Tilt_B"), strValue, szPath);

	for (UINT nIdx = 0; nIdx < ROI_SFR_Max; nIdx++)
	{
		strAppName.Format(_T("%s_SPEC_%d"), AppName_SFROpt, nIdx);

		strValue.Format(_T("%d"), pstConfigInfo->stInput[nIdx].stSpecMin.bEnable);
		WritePrivateProfileString(strAppName, _T("Min_Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stInput[nIdx].stSpecMax.bEnable);
		WritePrivateProfileString(strAppName, _T("Max_Enable"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stInput[nIdx].stSpecMin.dbValue);
		WritePrivateProfileString(strAppName, _T("Min_Value"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stInput[nIdx].stSpecMax.dbValue);
		WritePrivateProfileString(strAppName, _T("Max_Value"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stInput[nIdx].stSpecMin.iValue);
		WritePrivateProfileString(strAppName, _T("Min_nValue"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stInput[nIdx].stSpecMax.iValue);
		WritePrivateProfileString(strAppName, _T("Max_nValue"), strValue, szPath);
	}


	//for (UINT nIdx = 0; nIdx < ITM_SFR_Max; nIdx++)
	//{
	//	strAppName.Format(_T("%s_F_SPEC_%d"), AppName_SFROpt, nIdx);

	//	strValue.Format(_T("%d"), pstConfigInfo->stSpec_F_Min[nIdx].bEnable);
	//	WritePrivateProfileString(strAppName, _T("Min_Enable"), strValue, szPath);

	//	strValue.Format(_T("%d"), pstConfigInfo->stSpec_F_Min[nIdx].iValue);
	//	WritePrivateProfileString(strAppName, _T("Min_nValue"), strValue, szPath);

	//	strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_F_Min[nIdx].dbValue);
	//	WritePrivateProfileString(strAppName, _T("Min_dbValue"), strValue, szPath);

	//	strValue.Format(_T("%d"), pstConfigInfo->stSpec_F_Max[nIdx].bEnable);
	//	WritePrivateProfileString(strAppName, _T("Max_Enable"), strValue, szPath);

	//	strValue.Format(_T("%d"), pstConfigInfo->stSpec_F_Max[nIdx].iValue);
	//	WritePrivateProfileString(strAppName, _T("Max_nValue"), strValue, szPath);

	//	strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_F_Max[nIdx].dbValue);
	//	WritePrivateProfileString(strAppName, _T("Max_dbValue"), strValue, szPath);
	//}

	//for (UINT nIdx = 0; nIdx < ITM_SFR_TiltMax; nIdx++)
	//{
	//	strAppName.Format(_T("%s_Tilt_SPEC_%d"), AppName_SFROpt, nIdx);

	//	strValue.Format(_T("%d"), pstConfigInfo->stSpec_Tilt_Min[nIdx].bEnable);
	//	WritePrivateProfileString(strAppName, _T("Min_Enable"), strValue, szPath);

	//	strValue.Format(_T("%d"), pstConfigInfo->stSpec_Tilt_Min[nIdx].iValue);
	//	WritePrivateProfileString(strAppName, _T("Min_nValue"), strValue, szPath);

	//	strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Tilt_Min[nIdx].dbValue);
	//	WritePrivateProfileString(strAppName, _T("Min_dbValue"), strValue, szPath);

	//	strValue.Format(_T("%d"), pstConfigInfo->stSpec_Tilt_Max[nIdx].bEnable);
	//	WritePrivateProfileString(strAppName, _T("Max_Enable"), strValue, szPath);

	//	strValue.Format(_T("%d"), pstConfigInfo->stSpec_Tilt_Max[nIdx].iValue);
	//	WritePrivateProfileString(strAppName, _T("Max_nValue"), strValue, szPath);

	//	strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Tilt_Max[nIdx].dbValue);
	//	WritePrivateProfileString(strAppName, _T("Max_dbValue"), strValue, szPath);
	//}



	// ROI 历厘
	for (UINT nIdx = 0; nIdx < ROI_SFR_Max; nIdx++)
	{
		strAppName.Format(_T("%s_ROI_%d"), AppName_SFROpt, nIdx);

		Save_Rect(szPath, strAppName, &pstConfigInfo->stRegion[nIdx]);
	}

	for (UINT nROI = 0; nROI < ROI_SFR_GROUP_Max; nROI++)
	{
		strAppName.Format(_T("%s_GROUP_%d"), g_szAppName_SFR, nROI);

		strValue.Format(_T("%d"), pstConfigInfo->stInput_Group[nROI].bEnable);
		WritePrivateProfileString(strAppName, _T("Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stInput_Group[nROI].stSpecMin.bEnable);
		WritePrivateProfileString(strAppName, _T("Min_Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stInput_Group[nROI].stSpecMax.bEnable);
		WritePrivateProfileString(strAppName, _T("Max_Enable"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stInput_Group[nROI].stSpecMin.dbValue);
		WritePrivateProfileString(strAppName, _T("Min_Value"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stInput_Group[nROI].stSpecMax.dbValue);
		WritePrivateProfileString(strAppName, _T("Max_Value"), strValue, szPath);
	}

	for (UINT nROI = 0; nROI < ROI_SFR_TILT_Max; nROI++)
	{
		strAppName.Format(_T("%s_TILT_%d"), g_szAppName_SFR, nROI);

		strValue.Format(_T("%d"), pstConfigInfo->stInput_Tilt[nROI].bEnable);
		WritePrivateProfileString(strAppName, _T("Enable"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stInput_Tilt[nROI].dbValue);
		WritePrivateProfileString(strAppName, _T("Tilt_Spec"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stInput_Tilt[nROI].nStandardGroup);
		WritePrivateProfileString(strAppName, _T("Standard"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stInput_Tilt[nROI].nSubtractGroup);
		WritePrivateProfileString(strAppName, _T("Subtract"), strValue, szPath);
	}
	
	return TRUE;
}

BOOL CFile_VI_Config::Save_SFRFile_Dist(__in LPCTSTR szPath, __in const ST_SFR_Opt* pstConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfigInfo)
		return FALSE;

	CString strValue;
	CString	strAppName = AppName_SFROpt;

	for (int nSFRDist = 0; nSFRDist < 3; nSFRDist++)
	{
		strValue.Format(_T("%d"), pstConfigInfo[nSFRDist].nCaptureCnt);
		WritePrivateProfileString(g_szAppName_SFR[nSFRDist], _T("CaptureCnt"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo[nSFRDist].nCaptureDelay);
		WritePrivateProfileString(g_szAppName_SFR[nSFRDist], _T("CaptureDelay"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo[nSFRDist].nResultmode);
		WritePrivateProfileString(g_szAppName_SFR[nSFRDist], _T("Resultmode"), strValue, szPath);

		//strValue.Format(_T("%d"), pstConfigInfo[nSFRDist].nX1_Tilt_A);
		//WritePrivateProfileString(g_szAppName_SFR[nSFRDist], _T("X1_Tilt_A"), strValue, szPath);

		//strValue.Format(_T("%d"), pstConfigInfo[nSFRDist].nX1_Tilt_B);
		//WritePrivateProfileString(g_szAppName_SFR[nSFRDist], _T("X1_Tilt_B"), strValue, szPath);

		//strValue.Format(_T("%d"), pstConfigInfo[nSFRDist].nX2_Tilt_A);
		//WritePrivateProfileString(g_szAppName_SFR[nSFRDist], _T("X2_Tilt_A"), strValue, szPath);

		//strValue.Format(_T("%d"), pstConfigInfo[nSFRDist].nX2_Tilt_B);
		//WritePrivateProfileString(g_szAppName_SFR[nSFRDist], _T("X2_Tilt_B"), strValue, szPath);

		//strValue.Format(_T("%d"), pstConfigInfo[nSFRDist].nY1_Tilt_A);
		//WritePrivateProfileString(g_szAppName_SFR[nSFRDist], _T("Y1_Tilt_A"), strValue, szPath);

		//strValue.Format(_T("%d"), pstConfigInfo[nSFRDist].nY1_Tilt_B);
		//WritePrivateProfileString(g_szAppName_SFR[nSFRDist], _T("Y1_Tilt_B"), strValue, szPath);

		//strValue.Format(_T("%d"), pstConfigInfo[nSFRDist].nY2_Tilt_A);
		//WritePrivateProfileString(g_szAppName_SFR[nSFRDist], _T("Y2_Tilt_A"), strValue, szPath);

		//strValue.Format(_T("%d"), pstConfigInfo[nSFRDist].nY2_Tilt_B);
		//WritePrivateProfileString(g_szAppName_SFR[nSFRDist], _T("Y2_Tilt_B"), strValue, szPath);

		for (UINT nIdx = 0; nIdx < ROI_SFR_Max; nIdx++)
		{
			//strAppName.Format(_T("%s_SPEC_%d"), AppName_SFROpt, nIdx);
			strAppName.Format(_T("%s_SPEC_%d"), g_szAppName_SFR[nSFRDist], nIdx);

			strValue.Format(_T("%d"), pstConfigInfo[nSFRDist].stInput[nIdx].stSpecMin.bEnable);
			WritePrivateProfileString(strAppName, _T("Min_Enable"), strValue, szPath);

			strValue.Format(_T("%d"), pstConfigInfo[nSFRDist].stInput[nIdx].stSpecMax.bEnable);
			WritePrivateProfileString(strAppName, _T("Max_Enable"), strValue, szPath);

			strValue.Format(_T("%.2f"), pstConfigInfo[nSFRDist].stInput[nIdx].stSpecMin.dbValue);
			WritePrivateProfileString(strAppName, _T("Min_Value"), strValue, szPath);

			strValue.Format(_T("%.2f"), pstConfigInfo[nSFRDist].stInput[nIdx].stSpecMax.dbValue);
			WritePrivateProfileString(strAppName, _T("Max_Value"), strValue, szPath);

			strValue.Format(_T("%d"), pstConfigInfo[nSFRDist].stInput[nIdx].stSpecMin.iValue);
			WritePrivateProfileString(strAppName, _T("Min_nValue"), strValue, szPath);

			strValue.Format(_T("%d"), pstConfigInfo[nSFRDist].stInput[nIdx].stSpecMax.iValue);
			WritePrivateProfileString(strAppName, _T("Max_nValue"), strValue, szPath);
		}


		//for (UINT nIdx = 0; nIdx < ITM_SFR_Max; nIdx++)
		//{
		//	//strAppName.Format(_T("%s_F_SPEC_%d"), AppName_SFROpt, nIdx);
		//	strAppName.Format(_T("%s_F_SPEC_%d"), g_szAppName_SFR[nSFRDist], nIdx);

		//	strValue.Format(_T("%d"), pstConfigInfo[nSFRDist].stSpec_F_Min[nIdx].bEnable);
		//	WritePrivateProfileString(strAppName, _T("Min_Enable"), strValue, szPath);

		//	strValue.Format(_T("%d"), pstConfigInfo[nSFRDist].stSpec_F_Min[nIdx].iValue);
		//	WritePrivateProfileString(strAppName, _T("Min_nValue"), strValue, szPath);

		//	strValue.Format(_T("%.2f"), pstConfigInfo[nSFRDist].stSpec_F_Min[nIdx].dbValue);
		//	WritePrivateProfileString(strAppName, _T("Min_dbValue"), strValue, szPath);

		//	strValue.Format(_T("%d"), pstConfigInfo[nSFRDist].stSpec_F_Max[nIdx].bEnable);
		//	WritePrivateProfileString(strAppName, _T("Max_Enable"), strValue, szPath);

		//	strValue.Format(_T("%d"), pstConfigInfo[nSFRDist].stSpec_F_Max[nIdx].iValue);
		//	WritePrivateProfileString(strAppName, _T("Max_nValue"), strValue, szPath);

		//	strValue.Format(_T("%.2f"), pstConfigInfo[nSFRDist].stSpec_F_Max[nIdx].dbValue);
		//	WritePrivateProfileString(strAppName, _T("Max_dbValue"), strValue, szPath);
		//}

		//for (UINT nIdx = 0; nIdx < ITM_SFR_TiltMax; nIdx++)
		//{
		//	//strAppName.Format(_T("%s_Tilt_SPEC_%d"), AppName_SFROpt, nIdx);
		//	strAppName.Format(_T("%s_Tilt_SPEC_%d"), g_szAppName_SFR[nSFRDist], nIdx);

		//	strValue.Format(_T("%d"), pstConfigInfo[nSFRDist].stSpec_Tilt_Min[nIdx].bEnable);
		//	WritePrivateProfileString(strAppName, _T("Min_Enable"), strValue, szPath);

		//	strValue.Format(_T("%d"), pstConfigInfo[nSFRDist].stSpec_Tilt_Min[nIdx].iValue);
		//	WritePrivateProfileString(strAppName, _T("Min_nValue"), strValue, szPath);

		//	strValue.Format(_T("%.2f"), pstConfigInfo[nSFRDist].stSpec_Tilt_Min[nIdx].dbValue);
		//	WritePrivateProfileString(strAppName, _T("Min_dbValue"), strValue, szPath);

		//	strValue.Format(_T("%d"), pstConfigInfo[nSFRDist].stSpec_Tilt_Max[nIdx].bEnable);
		//	WritePrivateProfileString(strAppName, _T("Max_Enable"), strValue, szPath);

		//	strValue.Format(_T("%d"), pstConfigInfo[nSFRDist].stSpec_Tilt_Max[nIdx].iValue);
		//	WritePrivateProfileString(strAppName, _T("Max_nValue"), strValue, szPath);

		//	strValue.Format(_T("%.2f"), pstConfigInfo[nSFRDist].stSpec_Tilt_Max[nIdx].dbValue);
		//	WritePrivateProfileString(strAppName, _T("Max_dbValue"), strValue, szPath);
		//}
		
		// ROI 历厘
		for (UINT nIdx = 0; nIdx < ROI_SFR_Max; nIdx++)
		{
			//strAppName.Format(_T("%s_ROI_%d"), AppName_SFROpt, nIdx);
			strAppName.Format(_T("%s_ROI_%d"), g_szAppName_SFR[nSFRDist], nIdx);

			Save_Rect(szPath, strAppName, &pstConfigInfo[nSFRDist].stRegion[nIdx]);
		}

		for (UINT nROI = 0; nROI < ROI_SFR_GROUP_Max; nROI++)
		{
			strAppName.Format(_T("%s_GROUP_%d"), g_szAppName_SFR[nSFRDist], nROI);

			strValue.Format(_T("%d"), pstConfigInfo[nSFRDist].stInput_Group[nROI].bEnable);
			WritePrivateProfileString(strAppName, _T("Enable"), strValue, szPath);

			strValue.Format(_T("%d"), pstConfigInfo[nSFRDist].stInput_Group[nROI].stSpecMin.bEnable);
			WritePrivateProfileString(strAppName, _T("Min_Enable"), strValue, szPath);

			strValue.Format(_T("%d"), pstConfigInfo[nSFRDist].stInput_Group[nROI].stSpecMax.bEnable);
			WritePrivateProfileString(strAppName, _T("Max_Enable"), strValue, szPath);

			strValue.Format(_T("%.2f"), pstConfigInfo[nSFRDist].stInput_Group[nROI].stSpecMin.dbValue);
			WritePrivateProfileString(strAppName, _T("Min_Value"), strValue, szPath);

			strValue.Format(_T("%.2f"), pstConfigInfo[nSFRDist].stInput_Group[nROI].stSpecMax.dbValue);
			WritePrivateProfileString(strAppName, _T("Max_Value"), strValue, szPath);
		}

		for (UINT nROI = 0; nROI < ROI_SFR_TILT_Max; nROI++)
		{
			strAppName.Format(_T("%s_TILT_%d"), g_szAppName_SFR[nSFRDist], nROI);

			strValue.Format(_T("%d"), pstConfigInfo[nSFRDist].stInput_Tilt[nROI].bEnable);
			WritePrivateProfileString(strAppName, _T("Enable"), strValue, szPath);

			strValue.Format(_T("%.2f"), pstConfigInfo[nSFRDist].stInput_Tilt[nROI].dbValue);
			WritePrivateProfileString(strAppName, _T("Tilt_Spec"), strValue, szPath);

			strValue.Format(_T("%d"), pstConfigInfo[nSFRDist].stInput_Tilt[nROI].nStandardGroup);
			WritePrivateProfileString(strAppName, _T("Standard"), strValue, szPath);

			strValue.Format(_T("%d"), pstConfigInfo[nSFRDist].stInput_Tilt[nROI].nSubtractGroup);
			WritePrivateProfileString(strAppName, _T("Subtract"), strValue, szPath);
		}
	}
	return TRUE;
}

//=============================================================================
// Method		: Save_DefectPixelFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_DefectPixel_Opt * pstConfigInfo
// Qualifier	:
// Last Update	: 2017/12/3 - 14:21
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_DefectPixelFile(__in LPCTSTR szPath, __in const ST_DefectPixel_Opt* pstConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfigInfo)
		return FALSE;

	CString strValue;
	CString	strAppName = AppName_DefectPixelOpt;

	strValue.Format(_T("%d"), pstConfigInfo->iBlockSize);
	WritePrivateProfileString(strAppName, _T("BlockSize"), strValue, szPath);

	for (UINT nIdx = 0; nIdx < Spec_Defect_Max; nIdx++)
	{
		strAppName.Format(_T("%s_SPEC_%d"), AppName_DefectPixelOpt, nIdx);

		strValue.Format(_T("%d"), pstConfigInfo->iThreshold[nIdx]);
		WritePrivateProfileString(strAppName, _T("Threshold"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min[nIdx].bEnable);
		WritePrivateProfileString(strAppName, _T("Min_Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min[nIdx].iValue);
		WritePrivateProfileString(strAppName, _T("Min_nValue"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Min[nIdx].dbValue);
		WritePrivateProfileString(strAppName, _T("Min_dbValue"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max[nIdx].bEnable);
		WritePrivateProfileString(strAppName, _T("Max_Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max[nIdx].iValue);
		WritePrivateProfileString(strAppName, _T("Max_nValue"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Max[nIdx].dbValue);
		WritePrivateProfileString(strAppName, _T("Max_dbValue"), strValue, szPath);
	}

	return TRUE;
}

//=============================================================================
// Method		: Load_DefectPixelFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_DefectPixel_Opt & stConfigInfo
// Qualifier	:
// Last Update	: 2017/12/3 - 14:22
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_DefectPixelFile(__in LPCTSTR szPath, __out ST_DefectPixel_Opt& stConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_DefectPixelOpt;

	GetPrivateProfileString(strAppName, _T("BlockSize"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.iBlockSize = _ttoi(inBuff);

	for (UINT nIdx = 0; nIdx < Spec_Defect_Max; nIdx++)
	{
		strAppName.Format(_T("%s_SPEC_%d"), AppName_DefectPixelOpt, nIdx);

		GetPrivateProfileString(strAppName, _T("Threshold"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.iThreshold[nIdx] = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_nValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_dbValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].dbValue = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_nValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_dbValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].dbValue = _ttof(inBuff);
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_ShadingFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_Shading_Opt * pstConfigInfo
// Qualifier	:
// Last Update	: 2017/12/3 - 14:18
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_ShadingFile(__in LPCTSTR szPath, __in const ST_Shading_Opt* pstConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfigInfo)
		return FALSE;

	CString strValue;
	CString	strAppName = AppName_ShadingOpt;


	for (UINT nIdx = 0; nIdx < ROI_Shading_Max; nIdx++)
	{
		strAppName.Format(_T("%s_SPEC_%d"), AppName_ShadingOpt, nIdx);

		strValue.Format(_T("%d"), pstConfigInfo->bUseIndex[nIdx] ? TRUE : FALSE);
		WritePrivateProfileString(strAppName, _T("UseIndex"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->nType[nIdx]);
		WritePrivateProfileString(strAppName, _T("Type"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min[nIdx].bEnable);
		WritePrivateProfileString(strAppName, _T("Min_Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min[nIdx].iValue);
		WritePrivateProfileString(strAppName, _T("Min_nValue"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Min[nIdx].dbValue);
		WritePrivateProfileString(strAppName, _T("Min_dbValue"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max[nIdx].bEnable);
		WritePrivateProfileString(strAppName, _T("Max_Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max[nIdx].iValue);
		WritePrivateProfileString(strAppName, _T("Max_nValue"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Max[nIdx].dbValue);
		WritePrivateProfileString(strAppName, _T("Max_dbValue"), strValue, szPath);
	}

	return TRUE;
}

//=============================================================================
// Method		: Load_ShadingFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_Shading_Opt & stConfigInfo
// Qualifier	:
// Last Update	: 2017/12/3 - 14:19
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_ShadingFile(__in LPCTSTR szPath, __out ST_Shading_Opt& stConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_ShadingOpt;

	for (UINT nIdx = 0; nIdx < ROI_Shading_Max; nIdx++)
	{
		strAppName.Format(_T("%s_SPEC_%d"), AppName_ShadingOpt, nIdx);

		GetPrivateProfileString(strAppName, _T("UseIndex"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.bUseIndex[nIdx] = (1 == _ttoi(inBuff)) ? true : false;

		GetPrivateProfileString(strAppName, _T("Type"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.nType[nIdx] = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_nValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_dbValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].dbValue = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_nValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_dbValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].dbValue = _ttof(inBuff);
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_SNR_LightFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_SNR_Light_Opt * pstConfigInfo
// Qualifier	:
// Last Update	: 2017/12/3 - 14:16
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_SNR_LightFile(__in LPCTSTR szPath, __in const ST_SNR_Light_Opt* pstConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfigInfo)
		return FALSE;

	CString strValue;
	CString	strAppName = AppName_SNR_LightOpt;

	for (UINT nROI = 0; nROI < ROI_SNR_Light_Max; nROI++)
	{
		strAppName.Format(_T("ROI_%d"), nROI);

		strValue.Format(_T("%d"), pstConfigInfo->bIndex[nROI] ? TRUE : FALSE);
		WritePrivateProfileString(strAppName, _T("Index"), strValue, szPath);
	}

	strAppName.Format(_T("%s_SPEC"), AppName_SNR_LightOpt);

	strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min.bEnable);
	WritePrivateProfileString(strAppName, _T("Min_Enable"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min.iValue);
	WritePrivateProfileString(strAppName, _T("Min_nValue"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Min.dbValue);
	WritePrivateProfileString(strAppName, _T("Min_dbValue"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max.bEnable);
	WritePrivateProfileString(strAppName, _T("Max_Enable"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max.iValue);
	WritePrivateProfileString(strAppName, _T("Max_nValue"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Max.dbValue);
	WritePrivateProfileString(strAppName, _T("Max_dbValue"), strValue, szPath);

	return TRUE;
}

//=============================================================================
// Method		: Load_SNR_LightFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_SNR_Light_Opt & stConfigInfo
// Qualifier	:
// Last Update	: 2017/12/3 - 14:16
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_SNR_LightFile(__in LPCTSTR szPath, __out ST_SNR_Light_Opt& stConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_SNR_LightOpt;

	for (UINT nROI = 0; nROI < ROI_SNR_Light_Max; nROI++)
	{
		strAppName.Format(_T("ROI_%d"), nROI);

		GetPrivateProfileString(strAppName, _T("Index"), _T("1"), inBuff, 255, szPath);
		stConfigInfo.bIndex[nROI] = (1 == _ttoi(inBuff)) ? true : false;
	}

	strAppName.Format(_T("%s_SPEC"), AppName_SNR_LightOpt);

	GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Min.bEnable = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("Min_nValue"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Min.iValue = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("Min_dbValue"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Min.dbValue = _ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Max.bEnable = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("Max_nValue"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Max.iValue = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("Max_dbValue"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Max.dbValue = _ttof(inBuff);

	return TRUE;
}

//=============================================================================
// Method		: Load_ParticleFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_Particle_Opt & stConfigInfo
// Qualifier	:
// Last Update	: 2017/12/3 - 14:10
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_ParticleFile(__in LPCTSTR szPath, __out ST_Particle_Opt& stConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_ParticleOpt;

	GetPrivateProfileString(strAppName, _T("DustDis"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.dbDustDis = _ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("EdgeW"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.iEdgeW = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("EdgeH"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.iEdgeH = _ttoi(inBuff);

	strAppName.Format(_T("%s_SPEC"), AppName_ParticleOpt);

	GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Min.bEnable = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("Min_nValue"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Min.iValue = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("Min_dbValue"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Min.dbValue = _ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Max.bEnable = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("Max_nValue"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Max.iValue = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("Max_dbValue"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Max.dbValue = _ttof(inBuff);

	// ROI 历厘
	for (UINT nIdx = 0; nIdx < ROI_Particle_Max; nIdx++)
	{
		strAppName.Format(_T("%s_ROI_%d"), AppName_ParticleOpt, nIdx);

		Load_Rect(szPath, strAppName, stConfigInfo.stRegion[nIdx]);
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_ParticleFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_Particle_Opt * pstConfigInfo
// Qualifier	:
// Last Update	: 2017/12/3 - 14:11
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_ParticleFile(__in LPCTSTR szPath, __in const ST_Particle_Opt* pstConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfigInfo)
		return FALSE;

	CString strValue;
	CString	strAppName = AppName_ParticleOpt;

	strValue.Format(_T("%.2f"), pstConfigInfo->dbDustDis);
	WritePrivateProfileString(strAppName, _T("DustDis"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfigInfo->iEdgeW);
	WritePrivateProfileString(strAppName, _T("EdgeW"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfigInfo->iEdgeH);
	WritePrivateProfileString(strAppName, _T("EdgeH"), strValue, szPath);

	strAppName.Format(_T("%s_SPEC"), AppName_ParticleOpt);

	strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min.bEnable);
	WritePrivateProfileString(strAppName, _T("Min_Enable"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min.iValue);
	WritePrivateProfileString(strAppName, _T("Min_nValue"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Min.dbValue);
	WritePrivateProfileString(strAppName, _T("Min_dbValue"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max.bEnable);
	WritePrivateProfileString(strAppName, _T("Max_Enable"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max.iValue);
	WritePrivateProfileString(strAppName, _T("Max_nValue"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Max.dbValue);
	WritePrivateProfileString(strAppName, _T("Max_dbValue"), strValue, szPath);

	// ROI 历厘
	for (UINT nIdx = 0; nIdx < ROI_Particle_Max; nIdx++)
	{
		strAppName.Format(_T("%s_ROI_%d"), AppName_ParticleOpt, nIdx);

		Save_Rect(szPath, strAppName, &pstConfigInfo->stRegion[nIdx]);
	}

	return TRUE;
}

//=============================================================================
// Method		: Load_ActiveAlignFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_ActiveAlign_Opt & stConfigInfo
// Qualifier	:
// Last Update	: 2018/3/8 - 19:22
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_ActiveAlignFile(__in LPCTSTR szPath, __out ST_ActiveAlign_Opt& stConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_ActiveAlignOpt;

	GetPrivateProfileString(strAppName, _T("TargetCnt"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.nTargetCnt = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("TargetR"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.dbTargetR = _ttof(inBuff);

	for (UINT nIdx = 0; nIdx < 2; nIdx++)
	{
		strAppName.Format(_T("%s_TARGET_%d"), AppName_ActiveAlignOpt, nIdx);

		GetPrivateProfileString(strAppName, _T("TargetX"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.iTargetX[nIdx] = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("TargetY"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.iTargetY[nIdx] = _ttoi(inBuff);
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_ActiveAlignFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_ActiveAlign_Opt * pstConfigInfo
// Qualifier	:
// Last Update	: 2018/3/8 - 19:22
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_ActiveAlignFile(__in LPCTSTR szPath, __in const ST_ActiveAlign_Opt* pstConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfigInfo)
		return FALSE;

	CString strValue;
	CString	strAppName = AppName_ActiveAlignOpt;

	strValue.Format(_T("%d"), pstConfigInfo->nTargetCnt);
	WritePrivateProfileString(strAppName, _T("TargetCnt"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstConfigInfo->dbTargetR);
	WritePrivateProfileString(strAppName, _T("TargetR"), strValue, szPath);

	for (UINT nIdx = 0; nIdx < 2; nIdx++)
	{
		strAppName.Format(_T("%s_TARGET_%d"), AppName_ActiveAlignOpt, nIdx);

		strValue.Format(_T("%d"), pstConfigInfo->iTargetX[nIdx]);
		WritePrivateProfileString(strAppName, _T("TargetX"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->iTargetY[nIdx]);
		WritePrivateProfileString(strAppName, _T("TargetY"), strValue, szPath);
	}


	return TRUE;
}

//=============================================================================
// Method		: Load_TorqueFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_Torque_Opt & stConfigInfo
// Qualifier	:
// Last Update	: 2018/3/12 - 9:27
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_TorqueFile(__in LPCTSTR szPath, __out ST_Torque_Opt& stConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_TorqueOpt;

	for (UINT nIdx = 0; nIdx < Spec_Tor_Max; nIdx++)
	{
		strAppName.Format(_T("%s_SPEC_%d"), AppName_TorqueOpt, nIdx);

		GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_nValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_dbValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].dbValue = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_nValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_dbValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].dbValue = _ttof(inBuff);
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_TorqueFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_Torque_Opt * pstConfigInfo
// Qualifier	:
// Last Update	: 2018/3/12 - 9:27
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_TorqueFile(__in LPCTSTR szPath, __in const ST_Torque_Opt* pstConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfigInfo)
		return FALSE;

	CString strValue;
	CString	strAppName = AppName_TorqueOpt;

	for (UINT nIdx = 0; nIdx < Spec_Tor_Max; nIdx++)
	{
		strAppName.Format(_T("%s_SPEC_%d"), AppName_TorqueOpt, nIdx);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min[nIdx].bEnable);
		WritePrivateProfileString(strAppName, _T("Min_Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min[nIdx].iValue);
		WritePrivateProfileString(strAppName, _T("Min_nValue"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Min[nIdx].dbValue);
		WritePrivateProfileString(strAppName, _T("Min_dbValue"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max[nIdx].bEnable);
		WritePrivateProfileString(strAppName, _T("Max_Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max[nIdx].iValue);
		WritePrivateProfileString(strAppName, _T("Max_nValue"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Max[nIdx].dbValue);
		WritePrivateProfileString(strAppName, _T("Max_dbValue"), strValue, szPath);
	}

	return TRUE;
}


//=============================================================================
// Method		: Load_HotPixelFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_DefectPixel_Opt & stConfigInfo
// Qualifier	:
// Last Update	: 2017/12/3 - 14:22
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_HotPixelFile(__in LPCTSTR szPath, __out ST_HotPixel_Opt& stConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_HotPixelOpt;

	GetPrivateProfileString(strAppName, _T("Threshold"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.dbThreshold = _ttof(inBuff);

	strAppName.Format(_T("%s_SPEC"), AppName_DefectPixelOpt);

	GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Min.bEnable = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("Min_nValue"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Min.iValue = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("Min_dbValue"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Min.dbValue = _ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Max.bEnable = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("Max_nValue"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Max.iValue = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("Max_dbValue"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Max.dbValue = _ttof(inBuff);

	return TRUE;
}

//=============================================================================
// Method		: Save_DefectPixelFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_DefectPixel_Opt * pstConfigInfo
// Qualifier	:
// Last Update	: 2017/12/3 - 14:21
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_HotPixelFile(__in LPCTSTR szPath, __in const ST_HotPixel_Opt* pstConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfigInfo)
		return FALSE;

	CString strValue;
	CString	strAppName = AppName_HotPixelOpt;

	strValue.Format(_T("%.2f"), pstConfigInfo->dbThreshold);
	WritePrivateProfileString(strAppName, _T("Threshold"), strValue, szPath);

	strAppName.Format(_T("%s_SPEC"), AppName_HotPixelOpt);

	strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min.bEnable);
	WritePrivateProfileString(strAppName, _T("Min_Enable"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min.iValue);
	WritePrivateProfileString(strAppName, _T("Min_nValue"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Min.dbValue);
	WritePrivateProfileString(strAppName, _T("Min_dbValue"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max.bEnable);
	WritePrivateProfileString(strAppName, _T("Max_Enable"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max.iValue);
	WritePrivateProfileString(strAppName, _T("Max_nValue"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Max.dbValue);
	WritePrivateProfileString(strAppName, _T("Max_dbValue"), strValue, szPath);

	return TRUE;
}

//=============================================================================
// Method		: Load_FPNFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_FPN_Opt & stConfigInfo
// Qualifier	:
// Last Update	: 2018/5/12 - 16:56
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_FPNFile(__in LPCTSTR szPath, __out ST_FPN_Opt& stConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_FPNOpt;

	GetPrivateProfileString(strAppName, _T("ThresholdX"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.dbThresholdX = _ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("ThresholdY"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.dbThresholdY = _ttof(inBuff);

	for (UINT nIdx = 0; nIdx < Spec_FPN_MAX; nIdx++)
	{
		strAppName.Format(_T("%s_SPEC_%d"), AppName_FPNOpt, nIdx);

		GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_nValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_dbValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].dbValue = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_nValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_dbValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].dbValue = _ttof(inBuff);

	}

	// ROI 历厘
	for (UINT nIdx = 0; nIdx < ROI_FPN_Max; nIdx++)
	{
		strAppName.Format(_T("%s_ROI_%d"), AppName_FPNOpt, nIdx);

		Load_Rect(szPath, strAppName, stConfigInfo.stRegion[nIdx]);
	}


	return TRUE;
}

//=============================================================================
// Method		: Save_FPNFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_FPN_Opt * pstConfigInfo
// Qualifier	:
// Last Update	: 2018/5/12 - 16:56
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_FPNFile(__in LPCTSTR szPath, __in const ST_FPN_Opt* pstConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfigInfo)
		return FALSE;

	CString strValue;
	CString	strAppName = AppName_FPNOpt;

	strValue.Format(_T("%.2f"), pstConfigInfo->dbThresholdX);
	WritePrivateProfileString(strAppName, _T("ThresholdX"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstConfigInfo->dbThresholdY);
	WritePrivateProfileString(strAppName, _T("ThresholdY"), strValue, szPath);

	for (UINT nIdx = 0; nIdx < Spec_FPN_MAX; nIdx++)
	{
		strAppName.Format(_T("%s_SPEC_%d"), AppName_FPNOpt, nIdx);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min[nIdx].bEnable);
		WritePrivateProfileString(strAppName, _T("Min_Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min[nIdx].iValue);
		WritePrivateProfileString(strAppName, _T("Min_nValue"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Min[nIdx].dbValue);
		WritePrivateProfileString(strAppName, _T("Min_dbValue"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max[nIdx].bEnable);
		WritePrivateProfileString(strAppName, _T("Max_Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max[nIdx].iValue);
		WritePrivateProfileString(strAppName, _T("Max_nValue"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Max[nIdx].dbValue);
		WritePrivateProfileString(strAppName, _T("Max_dbValue"), strValue, szPath);

	}

	// ROI 历厘
	for (UINT nIdx = 0; nIdx < ROI_FPN_Max; nIdx++)
	{
		strAppName.Format(_T("%s_ROI_%d"), AppName_FPNOpt, nIdx);

		Save_Rect(szPath, strAppName, &pstConfigInfo->stRegion[nIdx]);
	}

	return TRUE;
}

//=============================================================================
// Method		: Load_3D_DepthFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_3D_Depth_Opt & stConfigInfo
// Qualifier	:
// Last Update	: 2018/5/12 - 16:56
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_3D_DepthFile(__in LPCTSTR szPath, __out ST_3D_Depth_Opt& stConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_3DDepthOpt;

	for (UINT nIdx = 0; nIdx < Spec_3D_Depth_Max; nIdx++)
	{
		strAppName.Format(_T("%s_SPEC_%d"), AppName_3DDepthOpt, nIdx);

		GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_nValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_dbValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].dbValue = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_nValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_dbValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].dbValue = _ttof(inBuff);

	}

	return TRUE;
}

//=============================================================================
// Method		: Save_3D_DepthFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_3D_Depth_Opt * pstConfigInfo
// Qualifier	:
// Last Update	: 2018/5/12 - 16:56
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_3D_DepthFile(__in LPCTSTR szPath, __in const ST_3D_Depth_Opt* pstConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfigInfo)
		return FALSE;

	CString strValue;
	CString	strAppName = AppName_3DDepthOpt;

	for (UINT nIdx = 0; nIdx < Spec_3D_Depth_Max; nIdx++)
	{
		strAppName.Format(_T("%s_SPEC_%d"), AppName_3DDepthOpt, nIdx);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min[nIdx].bEnable);
		WritePrivateProfileString(strAppName, _T("Min_Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min[nIdx].iValue);
		WritePrivateProfileString(strAppName, _T("Min_nValue"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Min[nIdx].dbValue);
		WritePrivateProfileString(strAppName, _T("Min_dbValue"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max[nIdx].bEnable);
		WritePrivateProfileString(strAppName, _T("Max_Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max[nIdx].iValue);
		WritePrivateProfileString(strAppName, _T("Max_nValue"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Max[nIdx].dbValue);
		WritePrivateProfileString(strAppName, _T("Max_dbValue"), strValue, szPath);
	}

	return TRUE;
}

//=============================================================================
// Method		: Load_EEPROM_VerifyFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_EEPROM_Verify_Opt & stConfigInfo
// Qualifier	:
// Last Update	: 2018/5/12 - 16:56
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_EEPROM_VerifyFile(__in LPCTSTR szPath, __out ST_EEPROM_Verify_Opt& stConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_EEPROMOpt;

	GetPrivateProfileString(strAppName, _T("SlaveID"), _T("172"), inBuff, 255, szPath);
	stConfigInfo.wslaveid= _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("OCX_ADD"), _T("64"), inBuff, 255, szPath);
	stConfigInfo.wocxaddress = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("OCY_ADD"), _T("68"), inBuff, 255, szPath);
	stConfigInfo.wocyaddress = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("OCX_LEN"), _T("4"), inBuff, 255, szPath);
	stConfigInfo.wocxlength = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("OCY_LEN"), _T("4"), inBuff, 255, szPath);
	stConfigInfo.wocylength = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("CHECKSUM_ADD"), _T("250"), inBuff, 255, szPath);
	stConfigInfo.waddress = _ttoi(inBuff);

	for (UINT nIdx = 0; nIdx < Spec_EEP_Max; nIdx++)
	{
		strAppName.Format(_T("%s_SPEC_%d"), AppName_EEPROMOpt, nIdx);

		GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_nValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_dbValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].dbValue = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_nValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_dbValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].dbValue = _ttof(inBuff);

	}

	return TRUE;
}

//=============================================================================
// Method		: Save_EEPROM_VerifyFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_EEPROM_Verify_Opt * pstConfigInfo
// Qualifier	:
// Last Update	: 2018/5/12 - 16:56
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_EEPROM_VerifyFile(__in LPCTSTR szPath, __in const ST_EEPROM_Verify_Opt* pstConfigInfo)
{

	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfigInfo)
		return FALSE;

	CString strValue;
	CString	strAppName = AppName_EEPROMOpt;

	strValue.Format(_T("%d"), pstConfigInfo->wslaveid);
	WritePrivateProfileString(strAppName, _T("SlaveID"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfigInfo->wocxaddress);
	WritePrivateProfileString(strAppName, _T("OCX_ADD"), strValue, szPath);


	strValue.Format(_T("%d"), pstConfigInfo->wocyaddress);
	WritePrivateProfileString(strAppName, _T("OCY_ADD"), strValue, szPath);


	strValue.Format(_T("%d"), pstConfigInfo->wocxlength);
	WritePrivateProfileString(strAppName, _T("OCX_LEN"), strValue, szPath);


	strValue.Format(_T("%d"), pstConfigInfo->wocylength);
	WritePrivateProfileString(strAppName, _T("OCY_LEN"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfigInfo->waddress);
	WritePrivateProfileString(strAppName, _T("CHECKSUM_ADD"), strValue, szPath);

	for (UINT nIdx = 0; nIdx < Spec_EEP_Max; nIdx++)
	{
		strAppName.Format(_T("%s_SPEC_%d"), AppName_EEPROMOpt, nIdx);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min[nIdx].bEnable);
		WritePrivateProfileString(strAppName, _T("Min_Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min[nIdx].iValue);
		WritePrivateProfileString(strAppName, _T("Min_nValue"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Min[nIdx].dbValue);
		WritePrivateProfileString(strAppName, _T("Min_dbValue"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max[nIdx].bEnable);
		WritePrivateProfileString(strAppName, _T("Max_Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max[nIdx].iValue);
		WritePrivateProfileString(strAppName, _T("Max_nValue"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Max[nIdx].dbValue);
		WritePrivateProfileString(strAppName, _T("Max_dbValue"), strValue, szPath);

	}


	return TRUE;
}

//=============================================================================
// Method		: Load_TemperatureSensorFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_EEPROM_Verify_Opt & stConfigInfo
// Qualifier	:
// Last Update	: 2018/5/12 - 16:56
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_TemperatureSensorFile(__in LPCTSTR szPath, __out ST_TemperatureSensor_Opt& stConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_TemperatureOpt;

	GetPrivateProfileString(strAppName, _T("SlaveID"), _T("172"), inBuff, 255, szPath);
	stConfigInfo.wslaveid = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("Address"), _T("64"), inBuff, 255, szPath);
	stConfigInfo.waddress = _ttoi(inBuff);

	for (UINT nIdx = 0; nIdx < Spec_Temp_Max; nIdx++)
	{
		strAppName.Format(_T("%s_SPEC_%d"), AppName_TemperatureOpt, nIdx);

		GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_nValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_dbValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].dbValue = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_nValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_dbValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].dbValue = _ttof(inBuff);

	}

	return TRUE;
}

//=============================================================================
// Method		: Save_TemperatureSensorFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_EEPROM_Verify_Opt * pstConfigInfo
// Qualifier	:
// Last Update	: 2018/5/12 - 16:56
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_TemperatureSensorFile(__in LPCTSTR szPath, __in const ST_TemperatureSensor_Opt* pstConfigInfo)
{

	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfigInfo)
		return FALSE;

	CString strValue;
	CString	strAppName = AppName_TemperatureOpt;

	strValue.Format(_T("%d"), pstConfigInfo->wslaveid);
	WritePrivateProfileString(strAppName, _T("SlaveID"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfigInfo->waddress);
	WritePrivateProfileString(strAppName, _T("Address"), strValue, szPath);

	for (UINT nIdx = 0; nIdx < Spec_Temp_Max; nIdx++)
	{
		strAppName.Format(_T("%s_SPEC_%d"), AppName_TemperatureOpt, nIdx);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min[nIdx].bEnable);
		WritePrivateProfileString(strAppName, _T("Min_Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min[nIdx].iValue);
		WritePrivateProfileString(strAppName, _T("Min_nValue"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Min[nIdx].dbValue);
		WritePrivateProfileString(strAppName, _T("Min_dbValue"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max[nIdx].bEnable);
		WritePrivateProfileString(strAppName, _T("Max_Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max[nIdx].iValue);
		WritePrivateProfileString(strAppName, _T("Max_nValue"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Max[nIdx].dbValue);
		WritePrivateProfileString(strAppName, _T("Max_dbValue"), strValue, szPath);

	}


	return TRUE;
}

//=============================================================================
// Method		: Load_Focus
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_Focus_Opt & stConfigInfo
// Qualifier	:
// Last Update	: 2018/7/5 - 8:45
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_Focus(__in LPCTSTR szPath, __out ST_Focus_Opt& stConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_FocusOpt;

	for (UINT nIdx = 0; nIdx < ROI_Focus_Max; nIdx++)
	{
		CString szkeyName;

		szkeyName.Format(_T("SelectROI_%d"), nIdx);

		GetPrivateProfileString(strAppName, szkeyName, _T("0"), inBuff, 255, szPath);
		stConfigInfo.nSelectROI[nIdx] = _ttoi(inBuff);

		szkeyName.Format(_T("MinSFR_%d"), nIdx);

		GetPrivateProfileString(strAppName, szkeyName, _T("0.5"), inBuff, 255, szPath);
		stConfigInfo.dbMinSFR[nIdx] = _ttof(inBuff);

		szkeyName.Format(_T("MaxSFR_%d"), nIdx);

		GetPrivateProfileString(strAppName, szkeyName, _T("0.6"), inBuff, 255, szPath);
		stConfigInfo.dbMaxSFR[nIdx] = _ttof(inBuff);
	}

	for (UINT nIdx = 0; nIdx < Spec_Focus_Max; nIdx++)
	{
		strAppName.Format(_T("%s_SPEC_%d"), AppName_FocusOpt, nIdx);

		GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_nValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_dbValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Min[nIdx].dbValue = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_nValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_dbValue"), _T("0"), inBuff, 255, szPath);
		stConfigInfo.stSpec_Max[nIdx].dbValue = _ttof(inBuff);
	}
	return TRUE;
}

//=============================================================================
// Method		: Save_Focus
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_Focus_Opt * pstConfigInfo
// Qualifier	:
// Last Update	: 2018/7/5 - 8:45
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_Focus(__in LPCTSTR szPath, __in const ST_Focus_Opt* pstConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfigInfo)
		return FALSE;

	CString strValue;
	CString	strAppName = AppName_FocusOpt;

	for (UINT nIdx = 0; nIdx < ROI_Focus_Max; nIdx++)
	{
		CString szkeyName;

		szkeyName.Format(_T("SelectROI_%d"), nIdx);

		strValue.Format(_T("%d"), pstConfigInfo->nSelectROI[nIdx]);
		WritePrivateProfileString(strAppName, szkeyName, strValue, szPath);
		
		szkeyName.Format(_T("MinSFR_%d"), nIdx);

		strValue.Format(_T("%.2f"), pstConfigInfo->dbMinSFR[nIdx]);
		WritePrivateProfileString(strAppName, szkeyName, strValue, szPath);

		szkeyName.Format(_T("MaxSFR_%d"), nIdx);

		strValue.Format(_T("%.2f"), pstConfigInfo->dbMaxSFR[nIdx]);
		WritePrivateProfileString(strAppName, szkeyName, strValue, szPath);
	}

	for (UINT nIdx = 0; nIdx < Spec_Focus_Max; nIdx++)
	{
		strAppName.Format(_T("%s_SPEC_%d"), AppName_FocusOpt, nIdx);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min[nIdx].bEnable);
		WritePrivateProfileString(strAppName, _T("Min_Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min[nIdx].iValue);
		WritePrivateProfileString(strAppName, _T("Min_nValue"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Min[nIdx].dbValue);
		WritePrivateProfileString(strAppName, _T("Min_dbValue"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max[nIdx].bEnable);
		WritePrivateProfileString(strAppName, _T("Max_Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max[nIdx].iValue);
		WritePrivateProfileString(strAppName, _T("Max_nValue"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Max[nIdx].dbValue);
		WritePrivateProfileString(strAppName, _T("Max_dbValue"), strValue, szPath);
	}

	return TRUE;
}

//=============================================================================
// Method		: Load_Particle_EntryFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_Particle_Opt & stConfigInfo
// Qualifier	:
// Last Update	: 2017/12/3 - 14:10
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_Particle_EntryFile(__in LPCTSTR szPath, __out ST_Particle_Entry_Opt& stConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_Particle_EntryOpt;

	GetPrivateProfileString(strAppName, _T("FailCheck"), _T("0"), inBuff, 80, szPath);
	stConfigInfo.bFailCheck = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("DustDis"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.dbDustDis = _ttof(inBuff);
	
	strAppName.Format(_T("%s_SPEC"), AppName_Particle_EntryOpt);

	GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Min.bEnable = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("Min_nValue"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Min.iValue = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("Min_dbValue"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Min.dbValue = _ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Max.bEnable = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("Max_nValue"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Max.iValue = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("Max_dbValue"), _T("0"), inBuff, 255, szPath);
	stConfigInfo.stSpec_Max.dbValue = _ttof(inBuff);


	// ROI 历厘
	for (UINT nIdx = 0; nIdx < Particle_Entry_Region_MaxEnum; nIdx++)
	{
		strAppName.Format(_T("%s_ROI_%d"), AppName_Particle_EntryOpt, nIdx);

		Load_Rect(szPath, strAppName, stConfigInfo.stRegion[nIdx]);
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_Particle_EntryFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_Particle_Opt * pstConfigInfo
// Qualifier	:
// Last Update	: 2017/12/3 - 14:11
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_Particle_EntryFile(__in LPCTSTR szPath, __in const ST_Particle_Entry_Opt* pstConfigInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfigInfo)
		return FALSE;

	CString strValue;
	CString	strAppName = AppName_Particle_EntryOpt;

	strValue.Format(_T("%.2f"), pstConfigInfo->dbDustDis);
	WritePrivateProfileString(strAppName, _T("DustDis"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfigInfo->bFailCheck);
	WritePrivateProfileString(strAppName, _T("FailCheck"), strValue, szPath);

	strAppName.Format(_T("%s_SPEC"), AppName_Particle_EntryOpt);

	strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min.bEnable);
	WritePrivateProfileString(strAppName, _T("Min_Enable"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfigInfo->stSpec_Min.iValue);
	WritePrivateProfileString(strAppName, _T("Min_nValue"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Min.dbValue);
	WritePrivateProfileString(strAppName, _T("Min_dbValue"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max.bEnable);
	WritePrivateProfileString(strAppName, _T("Max_Enable"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfigInfo->stSpec_Max.iValue);
	WritePrivateProfileString(strAppName, _T("Max_nValue"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstConfigInfo->stSpec_Max.dbValue);
	WritePrivateProfileString(strAppName, _T("Max_dbValue"), strValue, szPath);

	// ROI 历厘
	for (UINT nIdx = 0; nIdx < Particle_Entry_Region_MaxEnum; nIdx++)
	{
		strAppName.Format(_T("%s_ROI_%d"), AppName_Particle_EntryOpt, nIdx);

		Save_Rect(szPath, strAppName, &pstConfigInfo->stRegion[nIdx]);
	}

	return TRUE;
}
