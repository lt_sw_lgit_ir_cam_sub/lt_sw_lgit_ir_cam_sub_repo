﻿#ifndef Def_T_HotPixel_h__
#define Def_T_HotPixel_h__

#include <afxwin.h>

#include "Def_T_Common.h"

#pragma pack(push,1)

typedef enum enHotPixel_Fail
{
	ROI_F_HO_FST = 0,
	ROI_F_HO_LST = 49,
	ROI_F_HO_Max,
};

typedef enum enHotPixel_Spec
{
	Spec_HotPixel = 0,
	Spec_HotPixel_Max,
};

static LPCTSTR	g_szHotPixel_Spec[] =
{
	_T("HotPixel Count"),
	NULL
};

typedef struct _tag_HotPixel_Opt
{
	ST_RegionROI stRegion;

	double dbThreshold;

	ST_Comm_Spec stSpec_Min;
	ST_Comm_Spec stSpec_Max;

	_tag_HotPixel_Opt()
	{
		stRegion.Reset();

		dbThreshold = 0.0;

		for (UINT nIdx = 0; nIdx < Spec_HotPixel_Max; nIdx++)
		{
			stSpec_Min.Reset();
			stSpec_Max.Reset();
		}
	};

	_tag_HotPixel_Opt& operator= (_tag_HotPixel_Opt& ref)
	{
		stRegion	= ref.stRegion;
		dbThreshold = ref.dbThreshold;

		stSpec_Min = ref.stSpec_Min;
		stSpec_Max = ref.stSpec_Max;

		return *this;
	};

}ST_HotPixel_Opt, *PST_HotPixel_Opt;

typedef struct _tag_HotPixel_Data
{
	// 결과
	UINT nResult;

	int iFailCount;

	CRect	rtFailROI[ROI_F_HO_Max];
	float	fConcen[ROI_F_HO_Max];


	void Reset()
	{
		nResult		= 1;
		iFailCount  = 0;

		for (UINT nIdx = 0; nIdx < ROI_F_HO_Max; nIdx++)
		{
			rtFailROI[nIdx].SetRectEmpty();
			fConcen[nIdx] = 0;
		}
	};

	void FailData()
	{
		nResult		= 0;
		iFailCount	= 0;

		for (UINT nIdx = 0; nIdx < ROI_F_HO_Max; nIdx++)
		{
			rtFailROI[nIdx].SetRectEmpty();
			fConcen[nIdx] = 0;
		}
	};

	_tag_HotPixel_Data()
	{
		Reset();
	};

	_tag_HotPixel_Data& operator= (_tag_HotPixel_Data& ref)
	{
		nResult		= ref.nResult;
		iFailCount	= ref.iFailCount;

		for (UINT nIdx = 0; nIdx < ROI_F_HO_Max; nIdx++)
		{
			rtFailROI[nIdx] = ref.rtFailROI[nIdx];
			fConcen[nIdx]	= ref.fConcen[nIdx];
		}

		return *this;
	};

}ST_HotPixel_Data, *PST_HotPixel_Data;

typedef struct _tag_TI_HotPixel
{
	ST_HotPixel_Opt		stHotPixelOpt;	// 검사 기준 데이터
	ST_HotPixel_Data	stHotPixelData;	// 측정 데이터

	_tag_TI_HotPixel()
	{
	};

	void Reset()
	{
		stHotPixelData.Reset();
	};

	_tag_TI_HotPixel& operator= (_tag_TI_HotPixel& ref)
	{
		stHotPixelOpt		= ref.stHotPixelOpt;
		stHotPixelData		= ref.stHotPixelData;

		return *this;
	};

}ST_TI_HotPixel, *PST_TI_HotPixel;

#pragma pack(pop)

#endif // Def_HotPixel_h__