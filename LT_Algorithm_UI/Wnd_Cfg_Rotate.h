﻿#ifndef Wnd_Cfg_Rotate_h__
#define Wnd_Cfg_Rotate_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_T_Rotate.h"
#include "Def_TestItem_VI.h"
#include "List_RotateOp.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_Rotate
//-----------------------------------------------------------------------------
class CWnd_Cfg_Rotate : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_Rotate)

public:
	CWnd_Cfg_Rotate();
	virtual ~CWnd_Cfg_Rotate();

	enum enRotateStatic
	{
		STI_ROT_PARAM = 0,
		STI_ROT_SPEC,
		STI_ROT_STANDARD,
		STI_ROT_OFFSET,
		STI_ROT_MAX,
	};

	enum enRotateButton
	{
		BTN_ROT_RESET = 0,
		BTN_ROT_TEST,
		BTN_ROT_MAX,
	};

	enum enRotateComobox
	{
		CMB_ROT_MAX = 1,
	};

	enum enRotateEdit
	{
		EDT_ROT_STANDARD,
		EDT_ROT_OFFSET,
		EDT_ROT_MAX,
	};

protected:
	DECLARE_MESSAGE_MAP()

	ST_Rotate_Opt*		m_pstConfigInfo = NULL;
	CList_RotateOp		m_List;

	CFont				m_font;

	CVGStatic			m_st_Item[STI_ROT_MAX];
	CButton				m_bn_Item[BTN_ROT_MAX];
	CComboBox			m_cb_Item[CMB_ROT_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_ROT_MAX];

	CVGStatic	  		m_st_CapItem;
	CVGStatic	  		m_st_CapSpecMin;
	CVGStatic	  		m_st_CapSpecMax;

	CVGStatic	  		m_st_Spec[Spec_Rotate_MAX];
	CMFCMaskedEdit		m_ed_SpecMin[Spec_Rotate_MAX];
	CMFCMaskedEdit		m_ed_SpecMax[Spec_Rotate_MAX];
	CMFCButton	  		m_chk_SpecMin[Spec_Rotate_MAX];
	CMFCButton	  		m_chk_SpecMax[Spec_Rotate_MAX];

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

public:

	void	SetPtr_RecipeInfo(ST_Rotate_Opt* pstConfigInfo)
	{
		if (pstConfigInfo == NULL)
			return;

		m_pstConfigInfo = pstConfigInfo;
	};

	void SetUpdateData		();
	void GetUpdateData		();

	//void Get_TestItemInfo	(__out ST_TestItemInfo& stOutTestItemInfo);
};

#endif // Wnd_Cfg_Rotate_h__
