﻿#ifndef Def_Particle_T_h__
#define Def_Particle_T_h__

#include <afxwin.h>

#include "Def_T_Common.h"

#pragma pack(push,1)

typedef enum enParticle_Spec
{
	Spec_Particle_Stain = 0,
	Spec_Particle_MAX,
};

static LPCTSTR	g_szParticle_Spec[] =
{
	_T("Stain Count"),
	_T("Blemish Count"),
	NULL
};

typedef enum enParticleType
{
	Par_Type_Stain = 0,
	Par_Type_Max,
};

static LPCTSTR g_szParticle_Type[] =
{
	_T("Stain"),
	NULL
};

typedef enum enROI_Particle
{
	ROI_Particle_Edge = 0,
	ROI_Particle_Ver_Edge,
	ROI_Particle_Hor_Edge,
	ROI_Particle_Center,
	ROI_Particle_Max,
};

static LPCTSTR g_szROI_Particle[] =
{
	_T("Edge"),
	_T("Vertical Edge"),
	_T("Horizon Edge"),
	_T("Center"),
	NULL
};

typedef enum enParticle_Fail
{
	ROI_F_Par_FST = 0,
	ROI_F_Par_LST = 200,
	ROI_F_Par_Max,
};

typedef struct _tag_Particlee_Opt
{
	// 노이즈 민감도
	double dbDustDis;
	int		iEdgeW;
	int		iEdgeH;

	ST_RegionROI stRegion[ROI_Particle_Max];

	ST_Comm_Spec stSpec_Min;
	ST_Comm_Spec stSpec_Max;

	_tag_Particlee_Opt()
	{
		dbDustDis = 2;
		iEdgeW=32;
		iEdgeH = 32;

		stSpec_Min.Reset();
		stSpec_Max.Reset();
		
		for (UINT nIdx = 0; nIdx < ROI_Particle_Max; nIdx++)
		{
			stRegion[nIdx].Reset();
		}
	};

	/*변수 교환 함수*/
	_tag_Particlee_Opt& operator= (_tag_Particlee_Opt& ref)
	{
		dbDustDis	= ref.dbDustDis;

		iEdgeW = ref.iEdgeW;
		iEdgeH = ref.iEdgeH;
	
		stSpec_Min	= ref.stSpec_Min;
		stSpec_Max	= ref.stSpec_Max;

		for (UINT nIdx = 0; nIdx < ROI_Particle_Max; nIdx++)
		{
			stRegion[nIdx] = ref.stRegion[nIdx];
		}

		return *this;
	};

}ST_Particle_Opt, *PST_Particle_Op;

typedef struct _tag_Particle_Data
{
	UINT	nResult;

	// 검출된 이물 갯수
	UINT	nFailCount;

	UINT	nFailType[ROI_F_Par_Max];
	CRect	rtFailROI[ROI_F_Par_Max];

	double	dbConcentration[ROI_F_Par_Max];	// 농도


	UINT   nFailTypeCount[ROI_Particle_Max];
	double dFailTypeConcentration[ROI_Particle_Max];

	/*변수 초기화 함수*/
	_tag_Particle_Data()
	{
		Reset();
	};

	void Reset()
	{
		nResult		= 1;
		nFailCount	= 0;

		for (UINT nIdx = 0; nIdx < ROI_F_Par_Max; nIdx++)
		{
			nFailType[nIdx] = Par_Type_Max;
			rtFailROI[nIdx].SetRectEmpty();

			dbConcentration[nIdx] = 0.0;
		}

		for (int t = 0; t < ROI_Particle_Max; t++)
		{
			nFailTypeCount[t] =0;
			dFailTypeConcentration[t] = 0;
		}
	};
	void FailData(){
		nResult = 0;
		nFailCount = 0;

		for (UINT nIdx = 0; nIdx < ROI_F_Par_Max; nIdx++)
		{
			nFailType[nIdx] = Par_Type_Max;
			rtFailROI[nIdx].SetRectEmpty();

			dbConcentration[nIdx] = 0.0;
		}

		for (int t = 0; t < ROI_Particle_Max; t++)
		{
			nFailTypeCount[t] = 0;
			dFailTypeConcentration[t] = 0;
		}
	};
	/*변수 교환 함수*/
	_tag_Particle_Data& operator= (_tag_Particle_Data& ref)
	{
		nResult		= ref.nResult;
		nFailCount	= ref.nFailCount;

		for (UINT nIdx = 0; nIdx < ROI_F_Par_Max; nIdx++)
		{
			nFailType[nIdx]			= ref.nFailType[nIdx];
			rtFailROI[nIdx]			= ref.rtFailROI[nIdx];
			dbConcentration[nIdx]	= ref.dbConcentration[nIdx];
		}

		for (int t = 0; t < ROI_Particle_Max; t++)
		{
			nFailTypeCount[t] = ref.nFailTypeCount[t];
			dFailTypeConcentration[t] = ref.dFailTypeConcentration[t];
		}

		return *this;
	};

}ST_Particle_Data, *PST_Particle_Data;

typedef struct _tag_TI_Particle
{
	ST_Particle_Opt		stParticleOpt;		// 검사 기준 데이터
	ST_Particle_Data	stParticleData;		// 측정 데이터

	_tag_TI_Particle()
	{
	};

	void Reset()
	{
		stParticleData.Reset();
	};

	_tag_TI_Particle& operator= (_tag_TI_Particle& ref)
	{
		stParticleOpt	= ref.stParticleOpt;
		stParticleData	= ref.stParticleData;

		return *this;
	};

}ST_TI_Particle, *PST_TI_Particle;

#pragma pack(pop)

#endif // Def_Particle_T_h__

