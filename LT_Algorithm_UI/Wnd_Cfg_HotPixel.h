﻿#ifndef Wnd_Cfg_HotPixel_h__
#define Wnd_Cfg_HotPixel_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_T_HotPixel.h"
#include "Def_TestItem_VI.h"
#include "List_HotPixelOp.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_HotPixel
//-----------------------------------------------------------------------------
class CWnd_Cfg_HotPixel : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_HotPixel)

public:
	CWnd_Cfg_HotPixel();
	virtual ~CWnd_Cfg_HotPixel();

	enum enHotPixel_Static
	{
		STI_HP_PARAM = 0,
		STI_HP_SPEC,
		STI_HP_Threshold,
		STI_HP_MAX,
	};

	enum enHotPixel_Button
	{
		BTN_HP_RESET = 0,
		BTN_HP_TEST,
		BTN_HP_MAX,
	};

	enum enHotPixel_Comobox
	{
		CMB_HP_MAX = 1,
	};

	enum enHotPixel_Edit
	{
		EDT_HP_Threshold = 0,
		EDT_HP_MAX,
	};

protected:
	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	CFont						m_font;

	CList_HotPixelOp		m_List;

	CVGStatic					m_st_Item[STI_HP_MAX];
	CButton						m_bn_Item[BTN_HP_MAX];
	CComboBox					m_cb_Item[CMB_HP_MAX];
	CMFCMaskedEdit				m_ed_Item[EDT_HP_MAX];

	CVGStatic					m_st_CapItem;
	CVGStatic					m_st_CapSpecMin;
	CVGStatic					m_st_CapSpecMax;

	CVGStatic					m_st_Spec[Spec_HotPixel_Max];
	CMFCMaskedEdit				m_ed_SpecMin[Spec_HotPixel_Max];
	CMFCMaskedEdit				m_ed_SpecMax[Spec_HotPixel_Max];
	CMFCButton					m_chk_SpecMin[Spec_HotPixel_Max];
	CMFCButton					m_chk_SpecMax[Spec_HotPixel_Max];

	ST_HotPixel_Opt*		m_pstConfigInfo		= NULL;

public:

	void	SetPtr_RecipeInfo (ST_HotPixel_Opt* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstConfigInfo = pstRecipeInfo;
	};

	void	SetUpdateData		();
	void	GetUpdateData		();
};

#endif // Wnd_Cfg_HotPixel_h__
