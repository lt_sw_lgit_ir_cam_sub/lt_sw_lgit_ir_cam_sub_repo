// Wnd_ShadingData.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_ShadingData.h"


// CWnd_ShadingData

IMPLEMENT_DYNAMIC(CWnd_ShadingData, CWnd)

CWnd_ShadingData::CWnd_ShadingData()
{

}

CWnd_ShadingData::~CWnd_ShadingData()
{
}


BEGIN_MESSAGE_MAP(CWnd_ShadingData, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()
// CWnd_ShadingData 메시지 처리기입니다.

int CWnd_ShadingData::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	DWORD dwStyle = WS_VISIBLE | WS_CHILD /*| WS_CLIPCHILDREN*/ | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.
	CString strNum;
	for (int t = 0; t < (int)enSHData_MAX; t++)
	{
		m_st_Header[t].SetStaticStyle(CVGStatic::StaticStyle_Title);
		m_st_Header[t].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Header[t].SetFont_Gdip(L"Arial", 10.0F);
		m_st_Header[t].Create(g_szSHData[t], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

		for (int k = 0; k < 9; k ++)
		{
			m_st_Data[t][k].SetStaticStyle(CVGStatic::StaticStyle_Title);
			m_st_Data[t][k].SetColorStyle(CVGStatic::ColorStyle_Default);
			m_st_Data[t][k].SetFont_Gdip(L"Arial", 10.0F);
			m_st_Data[t][k].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
		}				
	}

	return 0;
}


void CWnd_ShadingData::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

	int iMargin = 0;
	int iSpacing = 0;
	int iCateSpacing = 0;

	int iLeft = iMargin;
	int iTop = iMargin;
	int iWidth = cx - iMargin - iMargin;
	int iHeight = cy - iMargin - iMargin;

	int HeaderW = (iWidth) / enSHData_MAX;

	int HeaderH =iHeight / (10);
	iLeft = iMargin;

	for (UINT nIdx = 0; nIdx < enSHData_MAX; nIdx++)
	{
		iTop = iMargin;
		m_st_Header[nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);
		iTop += HeaderH;

		if (nIdx < enSHData_C)
		{
			for (UINT t = 0; t < 9; t++)
			{
				m_st_Data[nIdx][t].MoveWindow(iLeft, iTop, HeaderW, HeaderH);
				iTop += HeaderH;
			}

		}
		else{
			for (UINT t = 0; t < 1; t++)
			{
				m_st_Data[nIdx][t].MoveWindow(iLeft, iTop, HeaderW, HeaderH);
				iTop += HeaderH;
			}
		}
		

		iLeft += HeaderW;

	}
	
// 	iTop = iMargin;
// 	for (UINT nIdx = 20; nIdx < 30; nIdx++)
// 	{
// 		iLeft = iMargin;
// 		iLeft += HeaderW;
// 		iLeft += HeaderW;
// 		iLeft += HeaderW;
// 		iLeft += HeaderW;
// 		m_st_Header[nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);
// 		iLeft += HeaderW;
// 		m_st_Data[nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);
// 		iTop += HeaderH;
// 	}

}


BOOL CWnd_ShadingData::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}


BOOL CWnd_ShadingData::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	return CWnd::OnEraseBkgnd(pDC);
}


void CWnd_ShadingData::DataSetting(UINT nIdx, BOOL MODE, double dData){

// 	if (MODE == TRUE)
// 	{
// 		m_st_Data[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
// 	}
// 	else{
// 		m_st_Data[nIdx].SetColorStyle(CVGStatic::ColorStyle_Red);
// 	}
// 
// 	CString strData;
// 	strData.Format(_T("%6.3f"), dData);
// 	m_st_Data[nIdx].SetWindowText(strData);
}
void CWnd_ShadingData::FieldDataSetting(UINT nIdx, double dData){


// 	m_st_Field[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
// 
// 
// 	CString strData;
// 	strData.Format(_T("F.%6.2f"), dData);
// 	m_st_Field[nIdx].SetWindowText(strData);
}

void CWnd_ShadingData::DataEachReset(UINT nIdx){

// 	
// 		m_st_Data[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
// 		m_st_Data[nIdx].SetWindowText(_T(""));
// 		m_st_Field[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
// 		m_st_Field[nIdx].SetWindowText(_T(""));

}


void CWnd_ShadingData::DataReset(){

	for (UINT nIdx = 0; nIdx < m_nHeaderCnt; nIdx++)
	{
		for (int t = 0; t < 9; t++)
		{
			m_st_Data[nIdx][t].SetColorStyle(CVGStatic::ColorStyle_Default);
			m_st_Data[nIdx][t].SetWindowText(_T(""));
		}	
	}

}

void CWnd_ShadingData::DataDisplay(ST_Shading_Data stData)
{
	CString strData;

	for (UINT nIdx = 0; nIdx < m_nHeaderCnt; nIdx++)
	{
		
			if (nIdx < enSHData_C)
			{
				for (int t = 0; t < 9; t++)
				{
					if (stData.bUse[GetItemIndexNumber(nIdx, t)] == TRUE)
					{
						if (stData.nEachResult[GetItemIndexNumber(nIdx, t)] == TRUE)
						{
							m_st_Data[nIdx][t].SetColorStyle(CVGStatic::ColorStyle_Default);
						}
						else{
							m_st_Data[nIdx][t].SetColorStyle(CVGStatic::ColorStyle_Red);
						}

						strData.Format(_T("%6.2f"), stData.fPercent[GetItemIndexNumber(nIdx, t)]);
						m_st_Data[nIdx][t].SetWindowText(strData);
					}
					else{
						m_st_Data[nIdx][t].SetColorStyle(CVGStatic::ColorStyle_DarkGray);

						m_st_Data[nIdx][t].SetWindowText(_T("X"));
					}
				}
			}
			else{
				for (int t = 0; t < 1; t++)
				{
					if (stData.bUse[GetItemIndexNumber(nIdx, t)] == TRUE)
					{
						if (stData.nEachResult[ROI_Shading_CNT] == TRUE)
						{
							m_st_Data[nIdx][t].SetColorStyle(CVGStatic::ColorStyle_Default);
						}
						else{
							m_st_Data[nIdx][t].SetColorStyle(CVGStatic::ColorStyle_Red);
						}

						strData.Format(_T("%d"), stData.sSignal[ROI_Shading_CNT]);
						m_st_Data[nIdx][t].SetWindowText(strData);
					}
					else{
						m_st_Data[nIdx][t].SetColorStyle(CVGStatic::ColorStyle_DarkGray);

						m_st_Data[nIdx][t].SetWindowText(_T("X"));
					}
				}
			}
	
	
	}
}
UINT CWnd_ShadingData::GetItemIndexNumber(UINT nType, UINT nRow)
{
	switch (nType)
	{
	case IDX_Shading_Ver_CT:
		return	nIndexVer_CT[nRow];
		break;
	case IDX_Shading_Ver_CB:
		return	nIndexVer_CB[nRow];
		break;
	case IDX_Shading_Hov_LC:
		return	nIndexHov_LC[nRow];
		break;
	case IDX_Shading_Hov_RC:
		return	nIndexHov_RC[nRow];
		break;
	case IDX_Shading_Dig_LT:
		return	nIndexDig_LT[nRow];
		break;
	case IDX_Shading_Dig_LB:
		return	nIndexDig_LB[nRow];
		break;
	case IDX_Shading_Dig_RT:
		return	nIndexDig_RT[nRow];
		break;
	case IDX_Shading_Dig_RB:
		return	nIndexDig_RB[nRow];
		break;
	default:
		break;
	}

	return	0;
}
