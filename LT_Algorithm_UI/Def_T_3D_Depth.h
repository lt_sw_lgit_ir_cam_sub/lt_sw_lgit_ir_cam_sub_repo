﻿#ifndef Def_T_3D_Depth_h__
#define Def_T_3D_Depth_h__

#include <afxwin.h>

#include "Def_T_Common.h"

#pragma pack(push,1)

typedef enum en3D_Depth_Spec
{
	Spec_BrightnessAvg = 0,
	Spec_Standard_deviation,
	Spec_3D_Depth_Max,
};

static LPCTSTR	g_sz3D_Depth_Spec[] =
{
	_T("Brightness"),
	_T("Deviation"),
	NULL
};

typedef struct _tag_3D_Depth_Opt
{

	ST_RegionROI stRegion;

	ST_Comm_Spec stSpec_Min[Spec_3D_Depth_Max];
	ST_Comm_Spec stSpec_Max[Spec_3D_Depth_Max];

	_tag_3D_Depth_Opt()
	{
		stRegion.Reset();

		for (UINT nIdx = 0; nIdx < Spec_3D_Depth_Max; nIdx++)
		{
			stSpec_Min[nIdx].Reset();
			stSpec_Max[nIdx].Reset();
		}
	};

	_tag_3D_Depth_Opt& operator= (_tag_3D_Depth_Opt& ref)
	{
		stRegion	= ref.stRegion;

		for (UINT nIdx = 0; nIdx < Spec_3D_Depth_Max; nIdx++)
		{
			stSpec_Min[nIdx] = ref.stSpec_Min[nIdx];
			stSpec_Max[nIdx] = ref.stSpec_Max[nIdx];
		}

		return *this;
	};

}ST_3D_Depth_Opt, *PST_3D_Depth_Opt;

typedef struct _tag_3D_Depth_Data
{
	// 결과
	UINT nResult;
	UINT nDetectResult;

	UINT nEachResult[Spec_3D_Depth_Max];

	// 결과 값
	double dbValue[Spec_3D_Depth_Max];

	void Reset()
	{
		nResult		= 1;
		nDetectResult	= 0;

		for (int index = 0; index < Spec_3D_Depth_Max;index++)
		{
			dbValue[index] = 0.0;
			nEachResult[index] = 0;
		}

	};
	
	void FailData()
	{
		nResult = 0;
		nDetectResult = 0;

		for (int index = 0; index < Spec_3D_Depth_Max; index++)
		{
			dbValue[index] = 0.0;
		}
	};

	_tag_3D_Depth_Data()
	{
		Reset();
	};

	_tag_3D_Depth_Data& operator= (_tag_3D_Depth_Data& ref)
	{
		nResult			= ref.nResult;
		nDetectResult	= ref.nDetectResult;

		for (int index = 0; index < Spec_3D_Depth_Max; index++)
		{
			dbValue[index]		= ref.dbValue[index];
			nEachResult[index]	= ref.nEachResult[index];
		}

		return *this;
	};

}ST_3D_Depth_Data, *PST_3D_Depth_Data;

typedef struct _tag_TI_3D_Depth
{
	ST_3D_Depth_Opt		st3D_DepthOpt;	// 검사 기준 데이터
	ST_3D_Depth_Data	st3D_DepthData;	// 측정 데이터

	_tag_TI_3D_Depth()
	{
	};

	void Reset()
	{
		st3D_DepthData.Reset();
	};

	_tag_TI_3D_Depth& operator= (_tag_TI_3D_Depth& ref)
	{
		st3D_DepthOpt		= ref.st3D_DepthOpt;
		st3D_DepthData		= ref.st3D_DepthData;

		return *this;
	};

}ST_TI_3D_Depth, *PST_TI_3D_Depth;

#pragma pack(pop)

#endif // Def_3D_Depth_h__