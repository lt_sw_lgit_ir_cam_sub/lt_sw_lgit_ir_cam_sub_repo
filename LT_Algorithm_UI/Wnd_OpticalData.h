#ifndef Wnd_OpticalData_h__
#define Wnd_OpticalData_h__

#pragma once

#include "afxwin.h"

#include "VGStatic.h"
#include "Def_T_OpticalCenter.h"
// CWnd_OpticalData
typedef enum
{
	enOTData_Header = 0,
	enOTData_PosX = 0,
	enOTData_PosY,
	enOTData_OffsetX,
	enOTData_OffsetY,
	enOTData_MAX,
}enumHeaderOTData;

typedef enum
{
	enOTData_1st = 0,
	enOTData_inputmax,
}enumDataOTData;
static	LPCTSTR	g_szOTData[] =
{
	_T("Pos X"),
	_T("Pos Y"),
	_T("Offset X"),
	_T("Offset Y"),
	NULL
};

class CWnd_OpticalData : public CWnd
{
	DECLARE_DYNAMIC(CWnd_OpticalData)

public:
	CWnd_OpticalData();
	virtual ~CWnd_OpticalData();

	UINT m_nHeaderCnt = enOTData_MAX;
	CString m_SzHeaderName[enOTData_MAX];

	//---Class 정의 시 세팅 해야함.
	void SetHeader(UINT nHeaderCnt){

		if (nHeaderCnt > enOTData_MAX)
		{
			nHeaderCnt = enOTData_MAX;
		}
		m_nHeaderCnt = nHeaderCnt;
	};

	void SetHeaderName(UINT nCnt, CString szHeader){

		if (nCnt > enOTData_MAX)
		{
			nCnt = enOTData_MAX-1;
		}
		m_SzHeaderName[nCnt] = szHeader;
	};

	CVGStatic		m_st_Header[enOTData_MAX];
	CVGStatic		m_st_Data[enOTData_inputmax][enOTData_MAX];

	BOOL			m_bMode = FALSE;

	void SetDisplayMode(BOOL bMode){
		m_bMode = bMode;
	};

//	CVGStatic		m_st_ResultView;


protected:
	DECLARE_MESSAGE_MAP()

public:
	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	afx_msg BOOL	OnEraseBkgnd		(CDC* pDC);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);

	void	DataSetting					(UINT nDataIdx, UINT nIdx, BOOL MODE, double dData);
	void	DataEachReset				(UINT nDataIdx, UINT nIdx);
	void	DataReset					();

	void	DataDisplay					(ST_OpticalCenter_Data stData);
	void	DetectDisplay				(ST_OpticalCenter_Data stData);
};


#endif // Wnd_OpticalData_h__
