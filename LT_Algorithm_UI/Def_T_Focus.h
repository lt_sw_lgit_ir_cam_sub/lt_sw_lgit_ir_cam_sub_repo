﻿#ifndef Def_T_Focus_h__
#define Def_T_Focus_h__

#include <afxwin.h>

#include "Def_T_Common.h"

#pragma pack(push,1)

typedef enum enFocus_ROI
{
	ROI_Focus_P1 = 0,
	ROI_Focus_P2,
	ROI_Focus_Max,
};

static LPCTSTR	g_szFocus_ROI[] =
{
	_T("ROI P1"),
	_T("ROI P2"),
	NULL
};

typedef enum enFocus_Spec
{
	Spec_Focus_P1 = 0,
	Spec_Focus_P2,
	Spec_Focus_Max,
};

static LPCTSTR	g_szFocus_Spec[] =
{
	_T("MAX SFR [P1] (±)"),
	_T("MAX SFR [P2] (±)"),
	NULL
};

// Focus 영역에 대한 옵션
typedef struct _tag_Focus_Opt
{
	// 그래프 그리기 옵션
	double	dbMinSFR[ROI_Focus_Max];
	double	dbMaxSFR[ROI_Focus_Max];

	// BestFocus 옵션
	UINT	nSelectROI[ROI_Focus_Max];

	ST_Comm_Spec stSpec_Min[Spec_Focus_Max];
	ST_Comm_Spec stSpec_Max[Spec_Focus_Max];

	_tag_Focus_Opt()
	{
		for (UINT nIdx = 0; nIdx < ROI_Focus_Max; nIdx++)
		{
			nSelectROI[nIdx] = 0;
			dbMinSFR[nIdx]	 = 0.40;
			dbMaxSFR[nIdx]	 = 0.60;
		}

		for (UINT nIdx = 0; nIdx < Spec_Focus_Max; nIdx++)
		{
			stSpec_Min[nIdx].Reset();
			stSpec_Max[nIdx].Reset();
		}
	};

	_tag_Focus_Opt& operator= (_tag_Focus_Opt& ref)
	{
		for (UINT nIdx = 0; nIdx < ROI_Focus_Max; nIdx++)
		{
			nSelectROI[nIdx]	= ref.nSelectROI[nIdx];
			dbMinSFR[nIdx]		= ref.dbMinSFR[nIdx];
			dbMaxSFR[nIdx]		= ref.dbMaxSFR[nIdx];
		}

		for (UINT nIdx = 0; nIdx < Spec_Focus_Max; nIdx++)
		{
			stSpec_Min[nIdx] = ref.stSpec_Min[nIdx];
			stSpec_Max[nIdx] = ref.stSpec_Max[nIdx];
		}

		return *this;
	};

}ST_Focus_Opt, *PST_Focus_Opt;

//각각 영역에 대한 Data정보
typedef struct _tag_Focus_Data
{
	// 최종 결과
	UINT	nResult;

	// Best SFR
	BOOL	bBestSFR[ROI_Focus_Max];

	// Max SFR
	BOOL	bMaxSFR[ROI_Focus_Max];

	// Max Value
	double	dbMaxValue[ROI_Focus_Max];

	// Max Value
	double	dbCurrentValue[ROI_Focus_Max];

	_tag_Focus_Data()
	{
		Reset();
	};

	void Reset()
	{
		nResult = 1;

		for (UINT nIdx = 0; nIdx < ROI_Focus_Max; nIdx++)
		{
			bBestSFR[nIdx]		 = FALSE;
			bMaxSFR[nIdx]		 = FALSE;
			dbMaxValue[nIdx]	 = 0.0;
			dbCurrentValue[nIdx] = 0.0;
		}
	};

	_tag_Focus_Data& operator= (_tag_Focus_Data& ref)
	{
		nResult				= ref.nResult;

		for (UINT nIdx = 0; nIdx < ROI_Focus_Max; nIdx++)
		{
			bBestSFR[nIdx]			= ref.bBestSFR[nIdx];
			bMaxSFR[nIdx]			= ref.bMaxSFR[nIdx];
			dbMaxValue[nIdx]		= ref.dbMaxValue[nIdx];
			dbCurrentValue[nIdx]	= ref.dbCurrentValue[nIdx];
		}

		return *this;
	};

}ST_Focus_Data, *PST_Focus_Data;

typedef struct _tag_TI_Focus
{
	ST_Focus_Opt	stFocusOpt;		// 검사 기준 데이터
	ST_Focus_Data	stFocusData;	// 측정 데이터

	_tag_TI_Focus()
	{
	};

	void Reset()
	{
		stFocusData.Reset();
	};

	_tag_TI_Focus& operator= (_tag_TI_Focus& ref)
	{
		stFocusOpt	= ref.stFocusOpt;
		stFocusData	= ref.stFocusData;

		return *this;
	};

}ST_TI_Focus, *PST_TI_Focus;

#pragma pack(pop)

#endif // Def_Focus_h__