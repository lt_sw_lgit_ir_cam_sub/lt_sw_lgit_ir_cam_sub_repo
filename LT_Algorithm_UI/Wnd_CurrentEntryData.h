#ifndef Wnd_CurrentEntryData_h__
#define Wnd_CurrentEntryData_h__

#pragma once

#include "afxwin.h"

#include "VGStatic.h"
#include "Def_T_ECurrent.h"

// CWnd_CurrentEntryData
typedef enum
{
	enCREData_Header = 0,
	enCREData_MAX	= 5,
}enumHeaderCREData;

typedef enum
{
	enCREData_1st = 0,
	enCREData_inputmax,
}enumDataCREData;

static	LPCTSTR	g_szCREData[] =
{
	_T("1.8V"),
	_T("3.3V"),
	_T("9.0V"),
	_T("14.7V"),
	_T("-5.7V"),
	NULL
};
class CWnd_CurrentEntryData : public CWnd
{
	DECLARE_DYNAMIC(CWnd_CurrentEntryData)

public:
	CWnd_CurrentEntryData();
	virtual ~CWnd_CurrentEntryData();

	UINT m_nHeaderCnt = enCREData_MAX;
	CString m_SzHeaderName[enCREData_MAX];

	//---Class 정의 시 세팅 해야함.
	void SetHeader(UINT nHeaderCnt){

		if (nHeaderCnt > enCREData_MAX)
		{
			nHeaderCnt = enCREData_MAX;
		}
		m_nHeaderCnt = nHeaderCnt;
	};

	void SetHeaderName(UINT nCnt, CString szHeader){

		if (nCnt > enCREData_MAX)
		{
			nCnt = enCREData_MAX-1;
		}
		m_SzHeaderName[nCnt] = szHeader;
	};

	CVGStatic		m_st_Header[enCREData_MAX];
	CVGStatic		m_st_Data[enCREData_inputmax][enCREData_MAX];

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	void DataSetting(UINT nDataIdx, UINT nIdx, BOOL MODE, double dData);
	void DataEachReset(UINT nDataIdx, UINT nIdx);
	void DataReset();
	void DataDisplay(ST_ECurrent_Data stData);
};


#endif // Wnd_CurrentEntryData_h__
