﻿// List_OpticalCenterOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_OpticalCenterOp.h"

typedef enum enListNum_OpticalOp
{
	OptOp_Object = 0,
	OptOp_PosX,
	OptOp_PosY,
	OptOp_Width,
	OptOp_Height,
	OptOp_Type,
	OptOp_Color,
	OptOp_Brightness,
	OptOp_Sharpness,
	OptOp_MaxCol,
};

static LPCTSTR	g_lpszHeader_OpticalOp[] =
{
	_T(""),
	_T("CenterX"),
	_T("CenterY"),
	_T("Width"),
	_T("Height"),
	_T("Type"),
	_T("Color"),
	_T("Brightness"),
	_T("Sharpness"),
	NULL
};

static LPCTSTR	g_lpszItem_OpticalOp[] =
{
	NULL
};

const int	iListAglin_OpticalOp[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_OpticalOp[] =
{
	105,
	105,
	105,
	105,
	105,
	105,
	105,
	105,
	105,
};

#define IDC_EDT_CELLEDIT			1000
#define IDC_COM_CELLCOMBO_TYPE		2000
#define IDC_COM_CELLCOMBO_COLOR		3000

// CList_OpticalCenterOp

IMPLEMENT_DYNAMIC(CList_OpticalCenterOp, CList_Cfg_VIBase)

CList_OpticalCenterOp::CList_OpticalCenterOp()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol = 0;
	m_nEditRow = 0;
}

CList_OpticalCenterOp::~CList_OpticalCenterOp()
{
	m_Font.DeleteObject();
}
BEGIN_MESSAGE_MAP(CList_OpticalCenterOp, CList_Cfg_VIBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT	(NM_CLICK,					&CList_OpticalCenterOp::OnNMClick				)
	ON_NOTIFY_REFLECT	(NM_DBLCLK,					&CList_OpticalCenterOp::OnNMDblclk				)
	ON_EN_KILLFOCUS		(IDC_EDT_CELLEDIT,			&CList_OpticalCenterOp::OnEnKillFocusEdit		)
	ON_CBN_KILLFOCUS	(IDC_COM_CELLCOMBO_TYPE,	&CList_OpticalCenterOp::OnEnKillFocusComboType	)
	ON_CBN_SELCHANGE	(IDC_COM_CELLCOMBO_TYPE,	&CList_OpticalCenterOp::OnEnSelectComboType		)	
	ON_CBN_KILLFOCUS	(IDC_COM_CELLCOMBO_COLOR,	&CList_OpticalCenterOp::OnEnKillFocusComboColor	)	
	ON_CBN_SELCHANGE	(IDC_COM_CELLCOMBO_COLOR,	&CList_OpticalCenterOp::OnEnSelectComboColor	)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_OpticalCenterOp 메시지 처리기입니다.
int CList_OpticalCenterOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CList_Cfg_VIBase::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader();
	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER /*| ES_NUMBER*/, CRect(0, 0, 0, 0), this, IDC_EDT_CELLEDIT);
	this->GetHeaderCtrl()->EnableWindow(FALSE);
	
	m_cb_Type.Create(WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | CBS_DROPDOWNLIST, CRect(0, 0, 0, 0), this, IDC_COM_CELLCOMBO_TYPE);
	m_cb_Type.ResetContent();

	for (UINT nIdex = 0; NULL != g_szMarkType[nIdex]; nIdex++)
	{
		m_cb_Type.InsertString(nIdex, g_szMarkType[nIdex]);
	}

	m_cb_Color.Create(WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | CBS_DROPDOWNLIST, CRect(0, 0, 0, 0), this, IDC_COM_CELLCOMBO_COLOR);
	m_cb_Color.ResetContent();

	for (UINT nIdex = 0; NULL != g_szMarkColor[nIdex]; nIdex++)
	{
		m_cb_Color.InsertString(nIdex, g_szMarkColor[nIdex]);
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_OpticalCenterOp::OnSize(UINT nType, int cx, int cy)
{
	CList_Cfg_VIBase::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_OpticalCenterOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | WS_BORDER | WS_TABSTOP ;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CList_Cfg_VIBase::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_OpticalCenterOp::InitHeader()
{
	for (int nCol = 0; nCol < OptOp_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_OpticalOp[nCol], iListAglin_OpticalOp[nCol], iHeaderWidth_OpticalOp[nCol]);
	}

	for (int nCol = 0; nCol < OptOp_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_OpticalOp[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 17:25
// Desc.		:
//=============================================================================
void CList_OpticalCenterOp::InsertFullData()
{
	if (NULL == m_pstConfigInfo)
		return;
	
	DeleteAllItems();

	for (UINT nIdx = 0; nIdx < OptOp_ItemNum; nIdx++)
	{
		InsertItem(0, _T(""));
		SetRectRow(nIdx);
 	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/6/26 - 14:26
// Desc.		:
//=============================================================================
void CList_OpticalCenterOp::SetRectRow(UINT nRow)
{
	if (NULL == m_pstConfigInfo)
		return;

	CString strValue;

	strValue.Format(_T("%s"), _T("ROI"));
	SetItemText(nRow, OptOp_Object, strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->stRegion.rtROI.CenterPoint().x);
	SetItemText(nRow, OptOp_PosX, strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->stRegion.rtROI.CenterPoint().y);
	SetItemText(nRow, OptOp_PosY, strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->stRegion.rtROI.Width());
	SetItemText(nRow, OptOp_Width, strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->stRegion.rtROI.Height());
	SetItemText(nRow, OptOp_Height, strValue);

	strValue.Format(_T("%s"), g_szMarkType[m_pstConfigInfo->stRegion.nMarkType]);
	SetItemText(nRow, OptOp_Type, strValue);

	strValue.Format(_T("%s"), g_szMarkColor[m_pstConfigInfo->stRegion.nMarkColor]);
	SetItemText(nRow, OptOp_Color, strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->stRegion.nBrightness);
	SetItemText(nRow, OptOp_Brightness, strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->stRegion.nSharpness);
	SetItemText(nRow, OptOp_Sharpness, strValue);
}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_OpticalCenterOp::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_OpticalCenterOp::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem < OptOp_MaxCol && pNMItemActivate->iSubItem > 0)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);
			
			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);
			
			if (pNMItemActivate->iSubItem == OptOp_Type)
			{
				m_cb_Type.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_cb_Type.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_cb_Type.SetFocus();
				m_cb_Type.SetCurSel(m_pstConfigInfo->stRegion.nMarkType);
			}
			else if (pNMItemActivate->iSubItem == OptOp_Color)
			{
				m_cb_Color.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_cb_Color.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_cb_Color.SetFocus();
				m_cb_Color.SetCurSel(m_pstConfigInfo->stRegion.nMarkColor);
			}
			else			
			{
				m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_ed_CellEdit.SetFocus();
			}
		}
		GetOwner()->SendNotifyMessage(m_nMessage, 0, 0);
	}
	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
void CList_OpticalCenterOp::OnEnKillFocusEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText));

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
BOOL CList_OpticalCenterOp::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	if (m_pstConfigInfo== NULL)
		return FALSE;

	if (iValue < 0)
		iValue = 0;

	CRect rtTemp = m_pstConfigInfo->stRegion.rtROI;

	switch (nCol)
	{
	case OptOp_PosX:
		m_pstConfigInfo->stRegion.RectPosXY(iValue, rtTemp.CenterPoint().y);
		break;
	case OptOp_PosY:
		m_pstConfigInfo->stRegion.RectPosXY(rtTemp.CenterPoint().x, iValue);
		break;
	case OptOp_Width:
		m_pstConfigInfo->stRegion.RectPosWH(iValue, rtTemp.Height());
		break;
	case OptOp_Height:
		m_pstConfigInfo->stRegion.RectPosWH(rtTemp.Width(), iValue);
		break;
	case OptOp_Brightness:
		if (iValue >= 255)
			iValue = 255;

		m_pstConfigInfo->stRegion.nBrightness = iValue;
		break;
	case OptOp_Sharpness:
		if (iValue >= 127)
			iValue = 127;

		m_pstConfigInfo->stRegion.nSharpness = iValue;
		break;
	default:
		break;
	}

	if (m_pstConfigInfo->stRegion.rtROI.left < 0)
	{
		CRect rtTemp = m_pstConfigInfo->stRegion.rtROI;

		m_pstConfigInfo->stRegion.rtROI.left  = 0;
		m_pstConfigInfo->stRegion.rtROI.right = m_pstConfigInfo->stRegion.rtROI.left + rtTemp.Width();
	}

	if (m_pstConfigInfo->stRegion.rtROI.right >(LONG)m_dwImage_Width)
	{
		CRect rtTemp = m_pstConfigInfo->stRegion.rtROI;

		m_pstConfigInfo->stRegion.rtROI.right = (LONG)m_dwImage_Width;
		m_pstConfigInfo->stRegion.rtROI.left = m_pstConfigInfo->stRegion.rtROI.right - rtTemp.Width();
	}

	if (m_pstConfigInfo->stRegion.rtROI.top < 0)
	{
		CRect rtTemp = m_pstConfigInfo->stRegion.rtROI;

		m_pstConfigInfo->stRegion.rtROI.top = 0;
		m_pstConfigInfo->stRegion.rtROI.bottom = rtTemp.Height() + m_pstConfigInfo->stRegion.rtROI.top;
	}

	if (m_pstConfigInfo->stRegion.rtROI.bottom >(LONG)m_dwImage_Height)
	{
		CRect rtTemp = m_pstConfigInfo->stRegion.rtROI;

		m_pstConfigInfo->stRegion.rtROI.bottom = (LONG)m_dwImage_Height;
		m_pstConfigInfo->stRegion.rtROI.top = m_pstConfigInfo->stRegion.rtROI.bottom - rtTemp.Height();
	}

	if (m_pstConfigInfo->stRegion.rtROI.Height() <= 0)
		m_pstConfigInfo->stRegion.RectPosWH(m_pstConfigInfo->stRegion.rtROI.Width(), 1);

	if (m_pstConfigInfo->stRegion.rtROI.Height() >= (LONG)m_dwImage_Height)
		m_pstConfigInfo->stRegion.RectPosWH(m_pstConfigInfo->stRegion.rtROI.Width(), m_dwImage_Height);

	if (m_pstConfigInfo->stRegion.rtROI.Width() <= 0)
		m_pstConfigInfo->stRegion.RectPosWH(1, m_pstConfigInfo->stRegion.rtROI.Height());

	if (m_pstConfigInfo->stRegion.rtROI.Width() >= (LONG)m_dwImage_Width)
		m_pstConfigInfo->stRegion.RectPosWH(m_dwImage_Width, m_pstConfigInfo->stRegion.rtROI.Height());

	CString strValue;

	switch (nCol)
	{
	case OptOp_PosX:
		strValue.Format(_T("%d"), m_pstConfigInfo->stRegion.rtROI.CenterPoint().x);
		break;
	case OptOp_PosY:
		strValue.Format(_T("%d"), m_pstConfigInfo->stRegion.rtROI.CenterPoint().y);
		break;
	case OptOp_Width:
		strValue.Format(_T("%d"), m_pstConfigInfo->stRegion.rtROI.Width());
		break;
	case OptOp_Height:
		strValue.Format(_T("%d"), m_pstConfigInfo->stRegion.rtROI.Height());
		break;
	case OptOp_Brightness:
		strValue.Format(_T("%d"), m_pstConfigInfo->stRegion.nBrightness);
		break;
	case OptOp_Sharpness:
		strValue.Format(_T("%d"), m_pstConfigInfo->stRegion.nSharpness);
		break;
	default:
		break;
	}

	m_ed_CellEdit.SetWindowText(strValue);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: UpdateCellData_double
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dValue
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_OpticalCenterOp::UpdateCelldbData(UINT nRow, UINT nCol, double dValue)
{
	CString str;
	str.Format(_T("%.2f"), dValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_OpticalCenterOp::GetCellData()
{
	return;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_OpticalCenterOp::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue		= _ttoi(strText);
		double dValue	= _ttof(strText);

		iValue = iValue + zDelta / 120;
		dValue = dValue + zDelta / 120 * 0.1;

		if (iValue < 0)
			iValue = 0;

		if (iValue > 2000)
			iValue = 2000;

		if (dValue < 0.0)
			dValue = 0.0;

		if (dValue > 2.0)
			dValue = 2.0;
		
		UpdateCellData(m_nEditRow, m_nEditCol, iValue);
	}

	return CList_Cfg_VIBase::OnMouseWheel(nFlags, zDelta, pt);
}

//=============================================================================
// Method		: OnEnKillFocusComboType
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/18 - 14:11
// Desc.		:
//=============================================================================
void CList_OpticalCenterOp::OnEnKillFocusComboType()
{
	SetItemText(0, OptOp_Type, g_szMarkType[m_pstConfigInfo->stRegion.nMarkType]);
	m_cb_Type.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: OnEnSelectComboType
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/18 - 14:11
// Desc.		:
//=============================================================================
void CList_OpticalCenterOp::OnEnSelectComboType()
{
	m_pstConfigInfo->stRegion.nMarkType = m_cb_Type.GetCurSel();

	SetItemText(0, OptOp_Type, g_szMarkType[m_pstConfigInfo->stRegion.nMarkType]);
	m_cb_Type.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: OnEnKillFocusComboColor
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/18 - 14:11
// Desc.		:
//=============================================================================
void CList_OpticalCenterOp::OnEnKillFocusComboColor()
{
	SetItemText(0, OptOp_Color, g_szMarkColor[m_pstConfigInfo->stRegion.nMarkColor]);
	m_cb_Color.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: OnEnSelectComboColor
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/18 - 14:11
// Desc.		:
//=============================================================================
void CList_OpticalCenterOp::OnEnSelectComboColor()
{
	m_pstConfigInfo->stRegion.nMarkColor = m_cb_Color.GetCurSel();

	SetItemText(0, OptOp_Color, g_szMarkColor[m_pstConfigInfo->stRegion.nMarkColor]);
	m_cb_Color.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}
