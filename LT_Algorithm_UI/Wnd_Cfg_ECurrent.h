﻿#ifndef Wnd_Cfg_ECurrent_h__
#define Wnd_Cfg_ECurrent_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_T_ECurrent.h"
#include "Def_TestItem_VI.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_ECurrent
//-----------------------------------------------------------------------------
class CWnd_Cfg_ECurrent : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_ECurrent)

public:
	CWnd_Cfg_ECurrent();
	virtual ~CWnd_Cfg_ECurrent();

	enum enECurrent_CameraMode
	{
		ECUR_CM_2,  //1.8,2.8
		ECUR_CM_5,  //ch1-5
		ECUR_CM_MAX,
	};

	enum enECurrent_Static
	{
		STI_ECUR_PARAM = 0,
		STI_ECUR_SPEC,
		STI_ECUR_OFFSET_18V_L,
		STI_ECUR_OFFSET_28V_L,
		STI_ECUR_OFFSET_18V_R,
		STI_ECUR_OFFSET_28V_R,
		STI_ECUR_MAX,
	};
	enum enECurrent_Static_entry
	{
		STI_ECUR_OFFSET_CH1_L  = 2,
		STI_ECUR_OFFSET_CH2_L,
		STI_ECUR_OFFSET_CH3_L,
		STI_ECUR_OFFSET_CH4_L,
		STI_ECUR_OFFSET_CH5_L,
		STI_ECUR_OFFSET_CH1_R,
		STI_ECUR_OFFSET_CH2_R,
		STI_ECUR_OFFSET_CH3_R,
		STI_ECUR_OFFSET_CH4_R,
		STI_ECUR_OFFSET_CH5_R,
		STI_ECUR_ENTRY_MAX,
	};
	enum enECurrent_Button
	{
		BTN_ECUR_RESET = 0,
		BTN_ECUR_TEST,
		BTN_ECUR_MAX,
	};

	enum enECurrent_Comobox
	{
		CMB_ECUR_MAX = 1,
	};

	enum enECurrent_Edit
	{
		EDT_ECUR_OFFSET_18V_L = 0,
		EDT_ECUR_OFFSET_28V_L,
		EDT_ECUR_OFFSET_18V_R,
		EDT_ECUR_OFFSET_28V_R,
		EDT_ECUR_MAX,
	};

	enum enECurrent_Edit_Entry
	{
		EDT_ECUR_OFFSET_CH1_L = 0,
		EDT_ECUR_OFFSET_CH2_L,
		EDT_ECUR_OFFSET_CH3_L,
		EDT_ECUR_OFFSET_CH4_L,
		EDT_ECUR_OFFSET_CH5_L,
		EDT_ECUR_OFFSET_CH1_R,
		EDT_ECUR_OFFSET_CH2_R,
		EDT_ECUR_OFFSET_CH3_R,
		EDT_ECUR_OFFSET_CH4_R,
		EDT_ECUR_OFFSET_CH5_R,
		EDT_ECUR_ENTRY_MAX,
	};
protected:
	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	CFont						m_font;

	CVGStatic				m_st_Item[STI_ECUR_ENTRY_MAX];
	CButton					m_bn_Item[BTN_ECUR_MAX];
	CComboBox				m_cb_Item[CMB_ECUR_MAX];
	CMFCMaskedEdit			m_ed_Item[EDT_ECUR_ENTRY_MAX];

	CVGStatic				m_st_CapItem;
	CVGStatic				m_st_CapSpecMin;
	CVGStatic				m_st_CapSpecMax;

	CVGStatic				m_st_Spec[Spec_ECurrent_Max];
	CMFCMaskedEdit			m_ed_SpecMin[Spec_ECurrent_Max];
	CMFCMaskedEdit			m_ed_SpecMax[Spec_ECurrent_Max];
	CMFCButton				m_chk_SpecMin[Spec_ECurrent_Max];
	CMFCButton				m_chk_SpecMax[Spec_ECurrent_Max];

	ST_ECurrent_Opt*		m_pstConfigInfo		= NULL;

	// 0 -> Camera1, 1 -> Camera2
	UINT					m_nModelType = 0;
	UINT					m_nCameraModelType = 0;
public:

	void	SetPtr_RecipeInfo (ST_ECurrent_Opt* pstConfigInfo)
	{
		if (pstConfigInfo == NULL)
			return;

		m_pstConfigInfo = pstConfigInfo;
	};

	// 0 -> Camera1, 1 -> Camera2
	void OnSetModelType(__in UINT nType)
	{
		m_nModelType = nType;

		CRect rc;
		GetClientRect(rc);
		OnSize(SIZE_RESTORED, rc.Width(), rc.Height());
	};

	void OnSetCameraModelType(__in UINT nType)
	{
		m_nCameraModelType = nType;

	};


	void	SetUpdateData		();
	void	GetUpdateData		();
	void Model_Change();
};

#endif // Wnd_Cfg_ECurrent_h__
