#ifndef Wnd_SFRData_h__
#define Wnd_SFRData_h__

#pragma once

#include "afxwin.h"

#include "VGStatic.h"
#include "Def_T_SFR.h"

// CWnd_SFRData
typedef enum
{
	enSFRData_Header = 0,
	enSFRData_MAX=30,
}enumHeaderSFRData;

class CWnd_SFRData : public CWnd
{
	DECLARE_DYNAMIC(CWnd_SFRData)

public:
	CWnd_SFRData();
	virtual ~CWnd_SFRData();

	UINT m_nHeaderCnt =30;
	CString m_SzHeaderName[enSFRData_MAX];

	//---Class 정의 시 세팅 해야함.
	void SetHeader(UINT nHeaderCnt){
		m_nHeaderCnt = nHeaderCnt;
	};

	void SetHeaderName(UINT nCnt, CString szHeader){
		m_SzHeaderName[nCnt] = szHeader;
	};

	CVGStatic		m_st_Header[enSFRData_MAX];
	CVGStatic		m_st_Data[enSFRData_MAX];
	CVGStatic		m_st_Field[enSFRData_MAX];

	BOOL			m_bMode = FALSE;

	void SetDisplayMode(BOOL bMode){
		m_bMode = bMode;
	};
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	void DataSetting(UINT nIdx, BOOL MODE, double dData);
	void FieldDataSetting(UINT nIdx, double dData);
	void DataEachReset(UINT nIdx);
	void DataReset();
	void DataDisplay(ST_SFR_Data stData);
};


#endif // Wnd_SFRData_h__
