//*****************************************************************************
// Filename	: 	List_Cfg_VIBase.h
// Created	:	2018/1/26 - 17:33
// Modified	:	2018/1/26 - 17:33
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#ifndef List_Cfg_VIBase_h__
#define List_Cfg_VIBase_h__

#pragma once

#include <afxwin.h>

//-----------------------------------------------------------------------------
// CList_Cfg_VIBase
//-----------------------------------------------------------------------------
class CList_Cfg_VIBase : public CListCtrl
{
	DECLARE_DYNAMIC(CList_Cfg_VIBase)

public:
	CList_Cfg_VIBase();
	virtual ~CList_Cfg_VIBase();

protected:
	DECLARE_MESSAGE_MAP()


	
	DWORD	m_dwImage_Width		= 1280;
	DWORD	m_dwImage_Height	= 960;


};


#endif // List_Cfg_VIBase_h__
