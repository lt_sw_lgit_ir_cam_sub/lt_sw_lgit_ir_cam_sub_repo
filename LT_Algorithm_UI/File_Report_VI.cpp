//*****************************************************************************
// Filename	: 	File_Report_VI.cpp
// Created	:	2018/1/27 - 14:00
// Modified	:	2018/1/27 - 14:00
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#include "File_Report_VI.h"
#include "CommonFunction.h"

#ifndef Para_Left
	#define Para_Left 0
#endif

#ifndef Para_Right
	#define Para_Right 1
#endif


CFile_Report_VI::CFile_Report_VI()
{
}


CFile_Report_VI::~CFile_Report_VI()
{
}

//=============================================================================
// Method		: SaveFinalizeResult
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nParaIdx
// Parameter	: __in CString szFullPath
// Parameter	: __in CStringArray * pszHeadArray
// Parameter	: __in CStringArray * pszDataArray
// Qualifier	:
// Last Update	: 2017/11/18 - 13:46
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::SaveFinalizeResult(__in UINT nParaIdx, __in CString szFullPath, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray)
{
	CString szDirPath;
	CString szBuff;
	CString szData;

	CFile File;
	CFileException e;
	if (!PathFileExists(szFullPath))
	{
		if (!File.Open(szFullPath, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}

		// Header 추가
		CString szHeader = _T("");
		for (int _a = 0; _a < pszHeadArray->GetCount(); _a++)
		{
			szHeader += pszHeadArray->GetAt(_a);
			szHeader += _T(",");
		}

		for (int _a = 0; _a < pszDataArray->GetCount(); _a++)
		{
			szData += pszDataArray->GetAt(_a);
			szData += _T(",");
		}

		szHeader += _T("\r\n");
		szBuff = szHeader + szData;
		szBuff += _T("\r\n");
	}
	else
	{
		if (!File.Open(szFullPath, CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}

		for (int _a = 0; _a < pszDataArray->GetCount(); _a++)
		{
			szData += pszDataArray->GetAt(_a);
			szData += _T(",");
		}

		szBuff = szData;
		szBuff += _T("\r\n");
	}

	File.SeekToEnd();
	File.Write(szBuff.GetBuffer(), szBuff.GetLength() * sizeof(TCHAR));
	File.Flush();
	szBuff.ReleaseBuffer();

	File.Close();
	return TRUE;
}

//=============================================================================
// Method		: MakeOpticalCenterXHeadAndData
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nParaIdx
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_OpticalCenter_Data stResult
// Parameter	: __in CStringArray * pszHeadArray
// Parameter	: __in CStringArray * pszDataArray
// Qualifier	:
// Last Update	: 2017/12/14 - 11:47
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::MakeOpticalCenterXHeadAndData(__in UINT nParaIdx, __in CString szBarcode, __in ST_OpticalCenter_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray)
{
	CString sz;
	CString szHeaderz[] = { _T("Date"), _T("WIP_ID"), _T("Para"), _T("Result"), _T("Value") };

	ASSERT(pszHeadArray == NULL);
	ASSERT(pszDataArray == NULL);

	pszHeadArray->RemoveAll();
	pszDataArray->RemoveAll();

	SYSTEMTIME tm;
	GetLocalTime(&tm);

	UINT nCount = sizeof(szHeaderz) / sizeof(szHeaderz[0]);

	for (UINT nAdd = 0; nAdd < nCount; nAdd++)
	{
		pszHeadArray->Add(szHeaderz[nAdd]);
	}

	// Data
	sz.Format(_T("%04d/%02d/%02d[%02d:%02d:%02d]"), tm.wYear, tm.wMonth, tm.wDay, tm.wHour, tm.wMinute, tm.wSecond);
	pszDataArray->Add(sz);

	// Barcode
	sz.Format(_T("%s"), szBarcode);
	pszDataArray->Add(sz);

	// Index
	if (Para_Left == nParaIdx)
		sz = _T("L");
	else
		sz = _T("R");

	pszDataArray->Add(sz);

	// Result
	sz.Format(_T("%d"), stResult.nResult);
	pszDataArray->Add(sz);

//	sz.Format(_T("%d"), stResult.nStandardX);
	pszDataArray->Add(sz);

	return TRUE;
}

//=============================================================================
// Method		: MakeOpticalCenterYHeadAndData
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nParaIdx
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_OpticalCenter_Data stResult
// Parameter	: __in CStringArray * pszHeadArray
// Parameter	: __in CStringArray * pszDataArray
// Qualifier	:
// Last Update	: 2017/12/14 - 11:47
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::MakeOpticalCenterYHeadAndData(__in UINT nParaIdx, __in CString szBarcode, __in ST_OpticalCenter_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray)
{
	CString sz;
	CString szHeaderz[] = { _T("Date"), _T("WIP_ID"), _T("Para"), _T("Result"), _T("Value") };

	ASSERT(pszHeadArray == NULL);
	ASSERT(pszDataArray == NULL);

	pszHeadArray->RemoveAll();
	pszDataArray->RemoveAll();

	SYSTEMTIME tm;
	GetLocalTime(&tm);

	UINT nCount = sizeof(szHeaderz) / sizeof(szHeaderz[0]);

	for (UINT nAdd = 0; nAdd < nCount; nAdd++)
	{
		pszHeadArray->Add(szHeaderz[nAdd]);
	}

	// Data
	sz.Format(_T("%04d/%02d/%02d[%02d:%02d:%02d]"), tm.wYear, tm.wMonth, tm.wDay, tm.wHour, tm.wMinute, tm.wSecond);
	pszDataArray->Add(sz);

	// Barcode
	sz.Format(_T("%s"), szBarcode);
	pszDataArray->Add(sz);

	// Index
	if (Para_Left == nParaIdx)
		sz = _T("L");
	else
		sz = _T("R");

	pszDataArray->Add(sz);

	// Result
	sz.Format(_T("%d"), stResult.nResult);
	pszDataArray->Add(sz);

	//sz.Format(_T("%d"), stResult.nStandardY);
	pszDataArray->Add(sz);

	return TRUE;
}

//=============================================================================
// Method		: MakeDistortionHeadAndData
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nParaIdx
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_Distortion_Data stResult
// Parameter	: __in CStringArray * pszHeadArray
// Parameter	: __in CStringArray * pszDataArray
// Qualifier	:
// Last Update	: 2017/12/14 - 11:09
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::MakeDistortionHeadAndData(__in UINT nParaIdx, __in CString szBarcode, __in ST_Distortion_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray)
{
// 	CString sz;
// 	CString szHeaderz[] = { _T("Date"), _T("WIP_ID"), _T("Para"), _T("Result"), _T("Value"), _T("LT_CenterX"), _T("LT_CenterY"), _T("LB_CenterX"), _T("LB_CenterY"), _T("CT_CenterX"), _T("CT_CenterY"), _T("CB_CenterX"), _T("CB_CenterY"), _T("RT_CenterX"), _T("RT_CenterY"), _T("RB_CenterX"), _T("RB_CenterY") };
// 
// 	ASSERT(pszHeadArray == NULL);
// 	ASSERT(pszDataArray == NULL);
// 
// 	pszHeadArray->RemoveAll();
// 	pszDataArray->RemoveAll();
// 
// 	SYSTEMTIME tm;
// 	GetLocalTime(&tm);
// 
// 	UINT nCount = sizeof(szHeaderz) / sizeof(szHeaderz[0]);
// 
// 	for (UINT nAdd = 0; nAdd < nCount; nAdd++)
// 	{
// 		pszHeadArray->Add(szHeaderz[nAdd]);
// 	}
// 
// 	// Data
// 	sz.Format(_T("%04d/%02d/%02d[%02d:%02d:%02d]"), tm.wYear, tm.wMonth, tm.wDay, tm.wHour, tm.wMinute, tm.wSecond);
// 	pszDataArray->Add(sz);
// 
// 	// Barcode
// 	sz.Format(_T("%s"), szBarcode);
// 	pszDataArray->Add(sz);
// 
// 	// Index
// 	if (Para_Left == nParaIdx)
// 		sz = _T("L");
// 	else
// 		sz = _T("R");
// 
// 	pszDataArray->Add(sz);
// 
// 	// Result
// 	sz.Format(_T("%d"), stResult.nResult);
// 	pszDataArray->Add(sz);
// 
// 	// Value
// 	sz.Format(_T("%.2f"), stResult.dbValue);
// 	pszDataArray->Add(sz);
// 
// 	for (UINT nIdx = 0; nIdx < ROI_DT_Max; nIdx++)
// 	{
// 		// X Center
// 		sz.Format(_T("%d"), stResult.ptCenter[nIdx].x);
// 		pszDataArray->Add(sz);
// 
// 		// Y Center
// 		sz.Format(_T("%d"), stResult.ptCenter[nIdx].y);
// 		pszDataArray->Add(sz);
// 	}

	return TRUE;
}

//=============================================================================
// Method		: MakeDynamicRangeHeadAndData
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nParaIdx
// Parameter	: __in UINT nItem
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_Dynamic_Data stResult
// Parameter	: __in CStringArray * pszHeadArray
// Parameter	: __in CStringArray * pszDataArray
// Qualifier	:
// Last Update	: 2017/12/14 - 11:47
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::MakeDynamicRangeHeadAndData(__in UINT nParaIdx, __in UINT nItem, __in CString szBarcode, __in ST_Dynamic_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray)
{
// 	CString sz;
// 	CString szHeaderz[] = { _T("Date"), _T("WIP_ID"), _T("Para"), _T("Index"), _T("Result"), _T("Value"), _T("Signal[W]"), _T("Noise[W]"), _T("Signal[B]"), _T("Noise[B]") };
// 
// 	ASSERT(pszHeadArray == NULL);
// 	ASSERT(pszDataArray == NULL);
// 
// 	pszHeadArray->RemoveAll();
// 	pszDataArray->RemoveAll();
// 
// 	SYSTEMTIME tm;
// 	GetLocalTime(&tm);
// 
// 	UINT nCount = sizeof(szHeaderz) / sizeof(szHeaderz[0]);
// 
// 	for (UINT nAdd = 0; nAdd < nCount; nAdd++)
// 	{
// 		pszHeadArray->Add(szHeaderz[nAdd]);
// 	}
// 
// 	// Data
// 	sz.Format(_T("%04d/%02d/%02d[%02d:%02d:%02d]"), tm.wYear, tm.wMonth, tm.wDay, tm.wHour, tm.wMinute, tm.wSecond);
// 	pszDataArray->Add(sz);
// 
// 	// Barcode
// 	sz.Format(_T("%s"), szBarcode);
// 	pszDataArray->Add(sz);
// 
// 	// Para
// 	if (Para_Left == nParaIdx)
// 		sz = _T("L");
// 	else
// 		sz = _T("R");
// 
// 	pszDataArray->Add(sz);
// 
// 	// Index
// 	if (IDX_DY_1ST == nItem)
// 		sz = _T("T");
// 	else
// 		sz = _T("B");
// 
// 	pszDataArray->Add(sz);
// 
// 	// Result
// 	sz.Format(_T("%d"), stResult.nResult);
// 	pszDataArray->Add(sz);
// 
// 	// Result
// 	sz.Format(_T("%.2f"), stResult.dbValue);
// 	pszDataArray->Add(sz);
// 
// 	for (UINT nIdx = 0; nIdx < ROI_DY_Max; nIdx++)
// 	{
// 		sz.Format(_T("%d"), stResult.sSignal[nIdx]);
// 		pszDataArray->Add(sz);
// 
// 		sz.Format(_T("%.2f"), stResult.fNoise[nIdx]);
// 		pszDataArray->Add(sz);
// 	}

	return TRUE;
}

//=============================================================================
// Method		: MakeFOV_HorHeadAndData
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nParaIdx
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_FOV_Data stResult
// Parameter	: __in CStringArray * pszHeadArray
// Parameter	: __in CStringArray * pszDataArray
// Qualifier	:
// Last Update	: 2017/12/14 - 11:48
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::MakeFOV_HorHeadAndData(__in UINT nParaIdx, __in CString szBarcode, __in ST_FOV_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray)
{
// 	CString sz;
// 	CString szHeaderz[] = { _T("Date"), _T("WIP_ID"), _T("Para"), _T("Result"), _T("Value"), _T("LC_CenterX"), _T("LC_CenterY"), _T("RC_CenterX"), _T("RC_CenterY"), _T("CenterX"), _T("CenterY") };
// 
// 	ASSERT(pszHeadArray == NULL);
// 	ASSERT(pszDataArray == NULL);
// 
// 	pszHeadArray->RemoveAll();
// 	pszDataArray->RemoveAll();
// 
// 	SYSTEMTIME tm;
// 	GetLocalTime(&tm);
// 
// 	UINT nCount = sizeof(szHeaderz) / sizeof(szHeaderz[0]);
// 
// 	for (UINT nAdd = 0; nAdd < nCount; nAdd++)
// 	{
// 		pszHeadArray->Add(szHeaderz[nAdd]);
// 	}
// 
// 	// Data
// 	sz.Format(_T("%04d/%02d/%02d[%02d:%02d:%02d]"), tm.wYear, tm.wMonth, tm.wDay, tm.wHour, tm.wMinute, tm.wSecond);
// 	pszDataArray->Add(sz);
// 
// 	// Barcode
// 	sz.Format(_T("%s"), szBarcode);
// 	pszDataArray->Add(sz);
// 
// 	// Para
// 	if (Para_Left == nParaIdx)
// 		sz = _T("L");
// 	else
// 		sz = _T("R");
// 
// 	pszDataArray->Add(sz);
// 
// 	// Result
// 	sz.Format(_T("%d"), stResult.nResult);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%.2f"), stResult.dbValue[Fo_Item_Hor]);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptCenter[ROI_FO_CL].x);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptCenter[ROI_FO_CL].y);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptCenter[ROI_FO_CR].x);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptCenter[ROI_FO_CR].y);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptOptCenter.x);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptOptCenter.y);
// 	pszDataArray->Add(sz);

	return TRUE;
}

//=============================================================================
// Method		: MakeFOV_VerHeadAndData
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nParaIdx
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_FOV_Data stResult
// Parameter	: __in CStringArray * pszHeadArray
// Parameter	: __in CStringArray * pszDataArray
// Qualifier	:
// Last Update	: 2017/12/14 - 11:48
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::MakeFOV_VerHeadAndData(__in UINT nParaIdx, __in CString szBarcode, __in ST_FOV_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray)
{
// 	CString sz;
// 	CString szHeaderz[] = { _T("Date"), _T("WIP_ID"), _T("Para"), _T("Result"), _T("Value"), _T("TC_CenterX"), _T("TC_CenterY"), _T("BC_CenterX"), _T("BC_CenterY"), _T("CenterX"), _T("CenterY") };
// 
// 	ASSERT(pszHeadArray == NULL);
// 	ASSERT(pszDataArray == NULL);
// 
// 	pszHeadArray->RemoveAll();
// 	pszDataArray->RemoveAll();
// 
// 	SYSTEMTIME tm;
// 	GetLocalTime(&tm);
// 
// 	UINT nCount = sizeof(szHeaderz) / sizeof(szHeaderz[0]);
// 
// 	for (UINT nAdd = 0; nAdd < nCount; nAdd++)
// 	{
// 		pszHeadArray->Add(szHeaderz[nAdd]);
// 	}
// 
// 	// Data
// 	sz.Format(_T("%04d/%02d/%02d[%02d:%02d:%02d]"), tm.wYear, tm.wMonth, tm.wDay, tm.wHour, tm.wMinute, tm.wSecond);
// 	pszDataArray->Add(sz);
// 
// 	// Barcode
// 	sz.Format(_T("%s"), szBarcode);
// 	pszDataArray->Add(sz);
// 
// 	// Para
// 	if (Para_Left == nParaIdx)
// 		sz = _T("L");
// 	else
// 		sz = _T("R");
// 
// 	pszDataArray->Add(sz);
// 
// 	// Result
// 	sz.Format(_T("%d"), stResult.nResult);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%.2f"), stResult.dbValue[Fo_Item_Ver]);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptCenter[ROI_FO_CT].x);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptCenter[ROI_FO_CT].y);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptCenter[ROI_FO_CB].x);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptCenter[ROI_FO_CB].y);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptOptCenter.x);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptOptCenter.y);
// 	pszDataArray->Add(sz);

	return TRUE;
}

//=============================================================================
// Method		: MakeFOV_LTRBHeadAndData
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nParaIdx
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_FOV_Data stResult
// Parameter	: __in CStringArray * pszHeadArray
// Parameter	: __in CStringArray * pszDataArray
// Qualifier	:
// Last Update	: 2017/12/14 - 11:48
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::MakeFOV_LTRBHeadAndData(__in UINT nParaIdx, __in CString szBarcode, __in ST_FOV_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray)
{
// 	CString sz;
// 	CString szHeaderz[] = { _T("Date"), _T("WIP_ID"), _T("Para"), _T("Result"), _T("Value"), _T("LT_CenterX"), _T("LT_CenterY"), _T("RB_CenterX"), _T("RB_CenterY"), _T("CenterX"), _T("CenterY") };
// 
// 	ASSERT(pszHeadArray == NULL);
// 	ASSERT(pszDataArray == NULL);
// 
// 	pszHeadArray->RemoveAll();
// 	pszDataArray->RemoveAll();
// 
// 	SYSTEMTIME tm;
// 	GetLocalTime(&tm);
// 
// 	UINT nCount = sizeof(szHeaderz) / sizeof(szHeaderz[0]);
// 
// 	for (UINT nAdd = 0; nAdd < nCount; nAdd++)
// 	{
// 		pszHeadArray->Add(szHeaderz[nAdd]);
// 	}
// 
// 	// Data
// 	sz.Format(_T("%04d/%02d/%02d[%02d:%02d:%02d]"), tm.wYear, tm.wMonth, tm.wDay, tm.wHour, tm.wMinute, tm.wSecond);
// 	pszDataArray->Add(sz);
// 
// 	// Barcode
// 	sz.Format(_T("%s"), szBarcode);
// 	pszDataArray->Add(sz);
// 
// 	// Para
// 	if (Para_Left == nParaIdx)
// 		sz = _T("L");
// 	else
// 		sz = _T("R");
// 
// 	pszDataArray->Add(sz);
// 
// 	// Result
// 	sz.Format(_T("%d"), stResult.nResult);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%.2f"), stResult.dbValue[Fo_Item_LTRB]);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptCenter[ROI_FO_LT].x);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptCenter[ROI_FO_LT].y);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptCenter[ROI_FO_RB].x);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptCenter[ROI_FO_RB].y);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptOptCenter.x);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptOptCenter.y);
// 	pszDataArray->Add(sz);

	return TRUE;
}

//=============================================================================
// Method		: MakeFOV_RTLBHeadAndData
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nParaIdx
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_FOV_Data stResult
// Parameter	: __in CStringArray * pszHeadArray
// Parameter	: __in CStringArray * pszDataArray
// Qualifier	:
// Last Update	: 2017/12/14 - 11:48
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::MakeFOV_RTLBHeadAndData(__in UINT nParaIdx, __in CString szBarcode, __in ST_FOV_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray)
{
// 	CString sz;
// 	CString szHeaderz[] = { _T("Date"), _T("WIP_ID"), _T("Para"), _T("Result"), _T("Value"), _T("RT_CenterX"), _T("RT_CenterY"), _T("LB_CenterX"), _T("LB_CenterY"), _T("CenterX"), _T("CenterY") };
// 
// 	ASSERT(pszHeadArray == NULL);
// 	ASSERT(pszDataArray == NULL);
// 
// 	pszHeadArray->RemoveAll();
// 	pszDataArray->RemoveAll();
// 
// 	SYSTEMTIME tm;
// 	GetLocalTime(&tm);
// 
// 	UINT nCount = sizeof(szHeaderz) / sizeof(szHeaderz[0]);
// 
// 	for (UINT nAdd = 0; nAdd < nCount; nAdd++)
// 	{
// 		pszHeadArray->Add(szHeaderz[nAdd]);
// 	}
// 
// 	// Data
// 	sz.Format(_T("%04d/%02d/%02d[%02d:%02d:%02d]"), tm.wYear, tm.wMonth, tm.wDay, tm.wHour, tm.wMinute, tm.wSecond);
// 	pszDataArray->Add(sz);
// 
// 	// Barcode
// 	sz.Format(_T("%s"), szBarcode);
// 	pszDataArray->Add(sz);
// 
// 	// Para
// 	if (Para_Left == nParaIdx)
// 		sz = _T("L");
// 	else
// 		sz = _T("R");
// 
// 	pszDataArray->Add(sz);
// 
// 	// Result
// 	sz.Format(_T("%d"), stResult.nResult);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%.2f"), stResult.dbValue[Fo_Item_LTRB]);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptCenter[ROI_FO_RT].x);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptCenter[ROI_FO_RT].y);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptCenter[ROI_FO_LB].x);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptCenter[ROI_FO_LB].y);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptOptCenter.x);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptOptCenter.y);
// 	pszDataArray->Add(sz);

	return TRUE;
}

//=============================================================================
// Method		: MakeSFRHeadAndData
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nParaIdx
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_SFR_Data stResult
// Parameter	: __in CStringArray * pszHeadArray
// Parameter	: __in CStringArray * pszDataArray
// Qualifier	:
// Last Update	: 2017/12/14 - 11:48
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::MakeSFRHeadAndData(__in UINT nParaIdx, __in CString szBarcode, __in ST_SFR_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray)
{
	CString sz;
	CString szHeaderz[] = { _T("Date"), _T("WIP_ID"), _T("Para"), _T("Result") };

	ASSERT(pszHeadArray == NULL);
	ASSERT(pszDataArray == NULL);

	pszHeadArray->RemoveAll();
	pszDataArray->RemoveAll();

	SYSTEMTIME tm;
	GetLocalTime(&tm);

	UINT nCount = sizeof(szHeaderz) / sizeof(szHeaderz[0]);

	for (UINT nAdd = 0; nAdd < nCount; nAdd++)
	{
		pszHeadArray->Add(szHeaderz[nAdd]);
	}

	for (UINT nAdd = 0; nAdd < ROI_SFR_Max; nAdd++)
	{
		sz.Format(_T("%d"), nAdd);
		pszHeadArray->Add(sz);
	}

	// Data
	sz.Format(_T("%04d/%02d/%02d[%02d:%02d:%02d]"), tm.wYear, tm.wMonth, tm.wDay, tm.wHour, tm.wMinute, tm.wSecond);
	pszDataArray->Add(sz);

	// Barcode
	sz.Format(_T("%s"), szBarcode);
	pszDataArray->Add(sz);

	// Para
	if (Para_Left == nParaIdx)
		sz = _T("L");
	else
		sz = _T("R");

	pszDataArray->Add(sz);

	// Result
	sz.Format(_T("%d"), stResult.nResult);
	pszDataArray->Add(sz);

	for (UINT nIdx = 0; nIdx < ROI_SFR_Max; nIdx++)
	{
		sz.Format(_T("%.2f"), stResult.dbValue[nIdx]);
		pszDataArray->Add(sz);
	}

	return TRUE;
}

//=============================================================================
// Method		: MakeRotationHeadAndData
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nParaIdx
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_Rotate_Data stResult
// Parameter	: __in CStringArray * pszHeadArray
// Parameter	: __in CStringArray * pszDataArray
// Qualifier	:
// Last Update	: 2017/12/14 - 11:48
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::MakeRotationHeadAndData(__in UINT nParaIdx, __in CString szBarcode, __in ST_Rotate_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray)
{
// 	CString sz;
// 	CString szHeaderz[] = { _T("Date"), _T("WIP_ID"), _T("Para"), _T("Result"), _T("Value"), _T("LT_CenterX"), _T("LT_CenterY"), _T("RT_CenterX"), _T("RT_CenterY"), _T("RL_CenterX"), _T("RL_CenterY"), _T("RB_CenterX"), _T("RB_CenterY") };
// 
// 	ASSERT(pszHeadArray == NULL);
// 	ASSERT(pszDataArray == NULL);
// 
// 	pszHeadArray->RemoveAll();
// 	pszDataArray->RemoveAll();
// 
// 	SYSTEMTIME tm;
// 	GetLocalTime(&tm);
// 
// 	UINT nCount = sizeof(szHeaderz) / sizeof(szHeaderz[0]);
// 
// 	for (UINT nAdd = 0; nAdd < nCount; nAdd++)
// 	{
// 		pszHeadArray->Add(szHeaderz[nAdd]);
// 	}
// 
// 	// Data
// 	sz.Format(_T("%04d/%02d/%02d[%02d:%02d:%02d]"), tm.wYear, tm.wMonth, tm.wDay, tm.wHour, tm.wMinute, tm.wSecond);
// 	pszDataArray->Add(sz);
// 
// 	// Barcode
// 	sz.Format(_T("%s"), szBarcode);
// 	pszDataArray->Add(sz);
// 
// 	// Para
// 	if (Para_Left == nParaIdx)
// 		sz = _T("L");
// 	else
// 		sz = _T("R");
// 
// 	pszDataArray->Add(sz);
// 
// 	// Result
// 	sz.Format(_T("%d"), stResult.nResult);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%.2f"), stResult.dbValue);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptCenter[ROI_Pos_LT].x);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptCenter[ROI_Pos_LT].y);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptCenter[ROI_Pos_RT].x);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptCenter[ROI_Pos_RT].y);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptCenter[ROI_Pos_LB].x);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptCenter[ROI_Pos_LB].y);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptCenter[ROI_Pos_RB].x);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.ptCenter[ROI_Pos_RB].y);
// 	pszDataArray->Add(sz);

	return TRUE;
}

//=============================================================================
// Method		: SaveFinalizeResultDistortion
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in CString szModel
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_Distortion_Data stResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/14 - 11:13
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::SaveFinalizeResultDistortion(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_Distortion_Data stResult, __in UINT nParaIdx /*= 0*/)
{
	CString szFullPath;

	CStringArray arrHeaderz;
	CStringArray arrDataz;

	MakeDistortionHeadAndData(nParaIdx, szBarcode, stResult, &arrHeaderz, &arrDataz);

	SYSTEMTIME tm;
	GetLocalTime(&tm);

	// 폴더 생성
	CString szDatePath;
	szDatePath.Format(_T("%s%s\\%04d-%02d-%02d\\"), szPath, szModel, tm.wYear, tm.wMonth, tm.wDay);
	MakeDirectory(szDatePath);

	szFullPath.Format(_T("%sDistortion.csv"), szDatePath);
	SaveFinalizeResult(nParaIdx, szFullPath, &arrHeaderz, &arrDataz);

	return TRUE;
}

//=============================================================================
// Method		: SaveFinalizeResultOpticalCenterX
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in CString szModel
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_OpticalCenter_Data stResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/14 - 11:35
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::SaveFinalizeResultOpticalCenterX(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_OpticalCenter_Data stResult, __in UINT nParaIdx /*= 0*/)
{
	CString szFullPath;

	CStringArray arrHeaderz;
	CStringArray arrDataz;

	MakeOpticalCenterXHeadAndData(nParaIdx, szBarcode, stResult, &arrHeaderz, &arrDataz);

	SYSTEMTIME tm;
	GetLocalTime(&tm);

	// 폴더 생성
	CString szDatePath;
	szDatePath.Format(_T("%s%s\\%04d-%02d-%02d\\"), szPath, szModel, tm.wYear, tm.wMonth, tm.wDay);
	MakeDirectory(szDatePath);

	szFullPath.Format(_T("%sOpticalCenterX.csv"), szDatePath);
	SaveFinalizeResult(nParaIdx, szFullPath, &arrHeaderz, &arrDataz);

	return TRUE;
}

//=============================================================================
// Method		: SaveFinalizeResultOpticalCenterY
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in CString szModel
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_OpticalCenter_Data stResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/14 - 11:39
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::SaveFinalizeResultOpticalCenterY(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_OpticalCenter_Data stResult, __in UINT nParaIdx /*= 0*/)
{
	CString szFullPath;

	CStringArray arrHeaderz;
	CStringArray arrDataz;

	MakeOpticalCenterYHeadAndData(nParaIdx, szBarcode, stResult, &arrHeaderz, &arrDataz);

	SYSTEMTIME tm;
	GetLocalTime(&tm);

	// 폴더 생성
	CString szDatePath;
	szDatePath.Format(_T("%s%s\\%04d-%02d-%02d\\"), szPath, szModel, tm.wYear, tm.wMonth, tm.wDay);
	MakeDirectory(szDatePath);

	szFullPath.Format(_T("%sOpticalCenterY.csv"), szDatePath);
	SaveFinalizeResult(nParaIdx, szFullPath, &arrHeaderz, &arrDataz);

	return TRUE;
}

//=============================================================================
// Method		: SaveFinalizeResultDynamicRange
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in CString szModel
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_Dynamic_Data stResult
// Parameter	: __in UINT nItem
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/14 - 11:39
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::SaveFinalizeResultDynamicRange(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_Dynamic_Data stResult, __in UINT nItem, __in UINT nParaIdx /*= 0*/)
{
	CString szFullPath;

	CStringArray arrHeaderz;
	CStringArray arrDataz;

	MakeDynamicRangeHeadAndData(nParaIdx, nItem, szBarcode, stResult, &arrHeaderz, &arrDataz);

	SYSTEMTIME tm;
	GetLocalTime(&tm);

	// 폴더 생성
	CString szDatePath;
	szDatePath.Format(_T("%s%s\\%04d-%02d-%02d\\"), szPath, szModel, tm.wYear, tm.wMonth, tm.wDay);
	MakeDirectory(szDatePath);

	szFullPath.Format(_T("%sDynamicRange.csv"), szDatePath);
	SaveFinalizeResult(nParaIdx, szFullPath, &arrHeaderz, &arrDataz);

	return TRUE;
}

//=============================================================================
// Method		: SaveFinalizeResultFOV_Hor
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in CString szModel
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_FOV_Data stResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/14 - 11:39
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::SaveFinalizeResultFOV_Hor(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_FOV_Data stResult, __in UINT nParaIdx /*= 0*/)
{
	CString szFullPath;

	CStringArray arrHeaderz;
	CStringArray arrDataz;

	MakeFOV_HorHeadAndData(nParaIdx, szBarcode, stResult, &arrHeaderz, &arrDataz);

	SYSTEMTIME tm;
	GetLocalTime(&tm);

	// 폴더 생성
	CString szDatePath;
	szDatePath.Format(_T("%s%s\\%04d-%02d-%02d\\"), szPath, szModel, tm.wYear, tm.wMonth, tm.wDay);
	MakeDirectory(szDatePath);

	szFullPath.Format(_T("%sFOV_Hor.csv"), szDatePath);
	SaveFinalizeResult(nParaIdx, szFullPath, &arrHeaderz, &arrDataz);

	return TRUE;
}

//=============================================================================
// Method		: SaveFinalizeResultFOV_Ver
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in CString szModel
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_FOV_Data stResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/14 - 11:39
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::SaveFinalizeResultFOV_Ver(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_FOV_Data stResult, __in UINT nParaIdx /*= 0*/)
{
	CString szFullPath;

	CStringArray arrHeaderz;
	CStringArray arrDataz;

	MakeFOV_VerHeadAndData(nParaIdx, szBarcode, stResult, &arrHeaderz, &arrDataz);

	SYSTEMTIME tm;
	GetLocalTime(&tm);

	// 폴더 생성
	CString szDatePath;
	szDatePath.Format(_T("%s%s\\%04d-%02d-%02d\\"), szPath, szModel, tm.wYear, tm.wMonth, tm.wDay);
	MakeDirectory(szDatePath);

	szFullPath.Format(_T("%sFOV_Ver.csv"), szDatePath);
	SaveFinalizeResult(nParaIdx, szFullPath, &arrHeaderz, &arrDataz);

	return TRUE;
}

//=============================================================================
// Method		: SaveFinalizeResultFOV_LTRB
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in CString szModel
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_FOV_Data stResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/14 - 11:39
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::SaveFinalizeResultFOV_LTRB(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_FOV_Data stResult, __in UINT nParaIdx /*= 0*/)
{
	CString szFullPath;

	CStringArray arrHeaderz;
	CStringArray arrDataz;

	MakeFOV_LTRBHeadAndData(nParaIdx, szBarcode, stResult, &arrHeaderz, &arrDataz);

	SYSTEMTIME tm;
	GetLocalTime(&tm);

	// 폴더 생성
	CString szDatePath;
	szDatePath.Format(_T("%s%s\\%04d-%02d-%02d\\"), szPath, szModel, tm.wYear, tm.wMonth, tm.wDay);
	MakeDirectory(szDatePath);

	szFullPath.Format(_T("%sFOV_LTRB.csv"), szDatePath);
	SaveFinalizeResult(nParaIdx, szFullPath, &arrHeaderz, &arrDataz);

	return TRUE;
}

//=============================================================================
// Method		: SaveFinalizeResultFOV_RTLB
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in CString szModel
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_FOV_Data stResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/14 - 11:39
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::SaveFinalizeResultFOV_RTLB(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_FOV_Data stResult, __in UINT nParaIdx /*= 0*/)
{
	CString szFullPath;

	CStringArray arrHeaderz;
	CStringArray arrDataz;

	MakeFOV_RTLBHeadAndData(nParaIdx, szBarcode, stResult, &arrHeaderz, &arrDataz);

	SYSTEMTIME tm;
	GetLocalTime(&tm);

	// 폴더 생성
	CString szDatePath;
	szDatePath.Format(_T("%s%s\\%04d-%02d-%02d\\"), szPath, szModel, tm.wYear, tm.wMonth, tm.wDay);
	MakeDirectory(szDatePath);

	szFullPath.Format(_T("%sFOV_RTLB.csv"), szDatePath);
	SaveFinalizeResult(nParaIdx, szFullPath, &arrHeaderz, &arrDataz);

	return TRUE;
}

//=============================================================================
// Method		: SaveFinalizeResultSFR
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in CString szModel
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_SFR_Data stResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/14 - 11:39
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::SaveFinalizeResultSFR(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_SFR_Data stResult, __in UINT nParaIdx /*= 0*/)
{
	CString szFullPath;

	CStringArray arrHeaderz;
	CStringArray arrDataz;

	MakeSFRHeadAndData(nParaIdx, szBarcode, stResult, &arrHeaderz, &arrDataz);

	SYSTEMTIME tm;
	GetLocalTime(&tm);

	// 폴더 생성
	CString szDatePath;
	szDatePath.Format(_T("%s%s\\%04d-%02d-%02d\\"), szPath, szModel, tm.wYear, tm.wMonth, tm.wDay);
	MakeDirectory(szDatePath);

	szFullPath.Format(_T("%sSFR.csv"), szDatePath);
	SaveFinalizeResult(nParaIdx, szFullPath, &arrHeaderz, &arrDataz);

	return TRUE;
}

//=============================================================================
// Method		: SaveFinalizeResultRotation
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in CString szModel
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_Rotate_Data stResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/14 - 11:39
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::SaveFinalizeResultRotation(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_Rotate_Data stResult, __in UINT nParaIdx /*= 0*/)
{
	CString szFullPath;

	CStringArray arrHeaderz;
	CStringArray arrDataz;

	MakeRotationHeadAndData(nParaIdx, szBarcode, stResult, &arrHeaderz, &arrDataz);

	SYSTEMTIME tm;
	GetLocalTime(&tm);

	// 폴더 생성
	CString szDatePath;
	szDatePath.Format(_T("%s%s\\%04d-%02d-%02d\\"), szPath, szModel, tm.wYear, tm.wMonth, tm.wDay);
	MakeDirectory(szDatePath);

	szFullPath.Format(_T("%sRotation.csv"), szDatePath);
	SaveFinalizeResult(nParaIdx, szFullPath, &arrHeaderz, &arrDataz);

	return TRUE;
}


//=============================================================================
// Method		: MakeDefectPixelHeadAndData
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nParaIdx
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_DefectPixel_Data stResult
// Parameter	: __in CStringArray * pszHeadArray
// Parameter	: __in CStringArray * pszDataArray
// Qualifier	:
// Last Update	: 2017/12/14 - 14:08
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::MakeDefectPixelHeadAndData(__in UINT nParaIdx, __in CString szBarcode, __in float fVoltage, __in ST_DefectPixel_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray)
{
// 	CString sz;
// 	CString szHeaderz[] = { _T("Date"), _T("WIP_ID"), _T("Light"), _T("Result"), _T("Value") };
// 
// 	ASSERT(pszHeadArray == NULL);
// 	ASSERT(pszDataArray == NULL);
// 
// 	pszHeadArray->RemoveAll();
// 	pszDataArray->RemoveAll();
// 
// 	SYSTEMTIME tm;
// 	GetLocalTime(&tm);
// 
// 	UINT nCount = sizeof(szHeaderz) / sizeof(szHeaderz[0]);
// 
// 	for (UINT nAdd = 0; nAdd < nCount; nAdd++)
// 	{
// 		pszHeadArray->Add(szHeaderz[nAdd]);
// 	}
// 
// 	for (UINT nAdd = 0; nAdd < ROI_F_DefectPixel_Max; nAdd++)
// 	{
// 		sz.Format(_T("X:%d"), nAdd);
// 		pszHeadArray->Add(sz);
// 
// 		sz.Format(_T("Y:%d"), nAdd);
// 		pszHeadArray->Add(sz);
// 	}
// 
// 	// Data
// 	sz.Format(_T("%04d/%02d/%02d[%02d:%02d:%02d]"), tm.wYear, tm.wMonth, tm.wDay, tm.wHour, tm.wMinute, tm.wSecond);
// 	pszDataArray->Add(sz);
// 
// 	// Barcode
// 	sz.Format(_T("%s"), szBarcode);
// 	pszDataArray->Add(sz);
// 
// 	// 광원 전압
// 	sz.Format(_T("%.2fV"), fVoltage);
// 	pszDataArray->Add(sz);
// 
// 	// Result
// 	sz.Format(_T("%d"), stResult.nResult);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.nFailCount);
// 	pszDataArray->Add(sz);
// 
// 	for (UINT nIdx = 0; nIdx < ROI_F_DefectPixel_Max; nIdx++)
// 	{
// 		sz.Format(_T("%d"), stResult.ptCenter[nIdx].x);
// 		pszDataArray->Add(sz);
// 
// 		sz.Format(_T("%d"), stResult.ptCenter[nIdx].y);
// 		pszDataArray->Add(sz);
// 	}

	return TRUE;
}

//=============================================================================
// Method		: MakeDeadPixelHeadAndData
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nParaIdx
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_Particle_Data stResult
// Parameter	: __in CStringArray * pszHeadArray
// Parameter	: __in CStringArray * pszDataArray
// Qualifier	:
// Last Update	: 2017/12/14 - 14:08
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::MakeDeadPixelHeadAndData(__in UINT nParaIdx, __in CString szBarcode, __in float fVoltage, __in ST_Particle_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray)
{
// 	CString sz;
// 	CString szHeaderz[] = { _T("Date"), _T("WIP_ID"), _T("Light"), _T("Result"), _T("Value") };
// 
// 	ASSERT(pszHeadArray == NULL);
// 	ASSERT(pszDataArray == NULL);
// 
// 	pszHeadArray->RemoveAll();
// 	pszDataArray->RemoveAll();
// 
// 	SYSTEMTIME tm;
// 	GetLocalTime(&tm);
// 
// 	UINT nCount = sizeof(szHeaderz) / sizeof(szHeaderz[0]);
// 
// 	for (UINT nAdd = 0; nAdd < nCount; nAdd++)
// 	{
// 		pszHeadArray->Add(szHeaderz[nAdd]);
// 	}
// 
// 	for (UINT nAdd = 0; nAdd < 20; nAdd++)
// 	{
// 		sz.Format(_T("X[%d]"), nAdd);
// 		pszHeadArray->Add(sz);
// 
// 		sz.Format(_T("Y[%d]"), nAdd);
// 		pszHeadArray->Add(sz);
// 	}
// 
// 	// Data
// 	sz.Format(_T("%04d/%02d/%02d[%02d:%02d:%02d]"), tm.wYear, tm.wMonth, tm.wDay, tm.wHour, tm.wMinute, tm.wSecond);
// 	pszDataArray->Add(sz);
// 
// 	// Barcode
// 	sz.Format(_T("%s"), szBarcode);
// 	pszDataArray->Add(sz);
// 
// 	// 광원 전압
// 	sz.Format(_T("%.2fV"), fVoltage);
// 	pszDataArray->Add(sz);
// 
// 	// Result
// 	sz.Format(_T("%d"), stResult.nResult);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.nFailCount[Particle_Type_DeadPixel]);
// 	pszDataArray->Add(sz);
// 
// 	UINT nIdx = 0;
// 
// 	for (UINT nAdd = 0; nAdd < 60; nAdd++)
// 	{
// 		if (Particle_Type_DeadPixel == stResult.byFailType[nAdd])
// 		{
// 			if (nIdx > 20)
// 			{
// 				return TRUE;
// 			}
// 
// 			sz.Format(_T("X:%d"), stResult.rtFailRoi[nAdd].left);
// 			pszHeadArray->Add(szHeaderz[nIdx]);
// 
// 			sz.Format(_T("Y:%d"), stResult.rtFailRoi[nAdd].top);
// 			pszHeadArray->Add(szHeaderz[nIdx]);
// 
// 			nIdx++;
// 		}
//	}

	return TRUE;
}

//=============================================================================
// Method		: MakeBlemishHeadAndData
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nParaIdx
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_Particle_Data stResult
// Parameter	: __in CStringArray * pszHeadArray
// Parameter	: __in CStringArray * pszDataArray
// Qualifier	:
// Last Update	: 2017/12/14 - 14:08
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::MakeBlemishHeadAndData(__in UINT nParaIdx, __in CString szBarcode, __in float fVoltage, __in ST_Particle_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray)
{
// 	CString sz;
// 	CString szHeaderz[] = { _T("Date"), _T("WIP_ID"), _T("Light"), _T("Result"), _T("Value") };
// 
// 	ASSERT(pszHeadArray == NULL);
// 	ASSERT(pszDataArray == NULL);
// 
// 	pszHeadArray->RemoveAll();
// 	pszDataArray->RemoveAll();
// 
// 	SYSTEMTIME tm;
// 	GetLocalTime(&tm);
// 
// 	UINT nCount = sizeof(szHeaderz) / sizeof(szHeaderz[0]);
// 
// 	for (UINT nAdd = 0; nAdd < nCount; nAdd++)
// 	{
// 		pszHeadArray->Add(szHeaderz[nAdd]);
// 	}
// 
// 	for (UINT nAdd = 0; nAdd < 20; nAdd++)
// 	{
// 		sz.Format(_T("X[%d]"), nAdd);
// 		pszHeadArray->Add(sz);
// 
// 		sz.Format(_T("Y[%d]"), nAdd);
// 		pszHeadArray->Add(sz);
// 	}
// 
// 	// Data
// 	sz.Format(_T("%04d/%02d/%02d[%02d:%02d:%02d]"), tm.wYear, tm.wMonth, tm.wDay, tm.wHour, tm.wMinute, tm.wSecond);
// 	pszDataArray->Add(sz);
// 
// 	// Barcode
// 	sz.Format(_T("%s"), szBarcode);
// 	pszDataArray->Add(sz);
// 
// 	// 광원 전압
// 	sz.Format(_T("%.2fV"), fVoltage);
// 	pszDataArray->Add(sz);
// 
// 	// Result
// 	sz.Format(_T("%d"), stResult.nResult);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.nFailCount[Particle_Type_Blemish]);
// 	pszDataArray->Add(sz);
// 
// 	UINT nIdx = 0;
// 
// 	for (UINT nAdd = 0; nAdd < 60; nAdd++)
// 	{
// 		if (Particle_Type_Blemish == stResult.byFailType[nAdd])
// 		{
// 			if (nIdx > 20)
// 			{
// 				return TRUE;
// 			}
// 
// 			sz.Format(_T("X:%d"), stResult.rtFailRoi[nAdd].left);
// 			pszHeadArray->Add(szHeaderz[nIdx]);
// 
// 			sz.Format(_T("Y:%d"), stResult.rtFailRoi[nAdd].top);
// 			pszHeadArray->Add(szHeaderz[nIdx]);
// 
// 			nIdx++;
// 		}
// 	}

	return TRUE;
}

//=============================================================================
// Method		: MakeStainHeadAndData
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nParaIdx
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_Particle_Data stResult
// Parameter	: __in CStringArray * pszHeadArray
// Parameter	: __in CStringArray * pszDataArray
// Qualifier	:
// Last Update	: 2017/12/14 - 14:08
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::MakeStainHeadAndData(__in UINT nParaIdx, __in CString szBarcode, __in float fVoltage, __in ST_Particle_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray)
{
// 	CString sz;
// 	CString szHeaderz[] = { _T("Date"), _T("WIP_ID"), _T("Light"), _T("Result"), _T("Value") };
// 
// 	ASSERT(pszHeadArray == NULL);
// 	ASSERT(pszDataArray == NULL);
// 
// 	pszHeadArray->RemoveAll();
// 	pszDataArray->RemoveAll();
// 
// 	SYSTEMTIME tm;
// 	GetLocalTime(&tm);
// 
// 	UINT nCount = sizeof(szHeaderz) / sizeof(szHeaderz[0]);
// 
// 	for (UINT nAdd = 0; nAdd < nCount; nAdd++)
// 	{
// 		pszHeadArray->Add(szHeaderz[nAdd]);
// 	}
// 
// 	for (UINT nAdd = 0; nAdd < 20; nAdd++)
// 	{
// 		sz.Format(_T("X[%d]"), nAdd);
// 		pszHeadArray->Add(sz);
// 
// 		sz.Format(_T("Y[%d]"), nAdd);
// 		pszHeadArray->Add(sz);
// 	}
// 
// 	// Data
// 	sz.Format(_T("%04d/%02d/%02d[%02d:%02d:%02d]"), tm.wYear, tm.wMonth, tm.wDay, tm.wHour, tm.wMinute, tm.wSecond);
// 	pszDataArray->Add(sz);
// 
// 	// Barcode
// 	sz.Format(_T("%s"), szBarcode);
// 	pszDataArray->Add(sz);
// 
// 	// 광원 전압
// 	sz.Format(_T("%.2fV"), fVoltage);
// 	pszDataArray->Add(sz);
// 
// 	// Result
// 	sz.Format(_T("%d"), stResult.nResult);
// 	pszDataArray->Add(sz);
// 
// 	sz.Format(_T("%d"), stResult.nFailCount[Particle_Type_Stain]);
// 	pszDataArray->Add(sz);
// 
// 	UINT nIdx = 0;
// 
// 	for (UINT nAdd = 0; nAdd < 60; nAdd++)
// 	{
// 		if (Particle_Type_Stain == stResult.byFailType[nAdd])
// 		{
// 			if (nIdx > 20)
// 			{
// 				return TRUE;
// 			}
// 
// 			sz.Format(_T("X:%d"), stResult.rtFailRoi[nAdd].left);
// 			pszHeadArray->Add(szHeaderz[nIdx]);
// 
// 			sz.Format(_T("Y:%d"), stResult.rtFailRoi[nAdd].top);
// 			pszHeadArray->Add(szHeaderz[nIdx]);
// 
// 			nIdx++;
// 		}
// 	}

	return TRUE;
}

//=============================================================================
// Method		: MakeRtlluminationHeadAndData
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nParaIdx
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_Shading_Data stResult
// Parameter	: __in CStringArray * pszHeadArray
// Parameter	: __in CStringArray * pszDataArray
// Qualifier	:
// Last Update	: 2017/12/14 - 14:08
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::MakeRtlluminationHeadAndData(__in UINT nParaIdx, __in CString szBarcode, __in float fVoltage, __in ST_Shading_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray)
{
	CString sz;
	CString szHeaderz[] = { _T("Date"), _T("WIP_ID"), _T("Light"), _T("Result") };

	ASSERT(pszHeadArray == NULL);
	ASSERT(pszDataArray == NULL);

	pszHeadArray->RemoveAll();
	pszDataArray->RemoveAll();

	SYSTEMTIME tm;
	GetLocalTime(&tm);

	UINT nCount = sizeof(szHeaderz) / sizeof(szHeaderz[0]);

	for (UINT nAdd = 0; nAdd < nCount; nAdd++)
	{
		pszHeadArray->Add(szHeaderz[nAdd]);
	}

	for (UINT nAdd = 0; nAdd < ROI_Shading_Max; nAdd++)
	{
		sz.Format(_T("Value[%d]"), nAdd);
		pszHeadArray->Add(sz);

		sz.Format(_T("Percent[%d]"), nAdd);
		pszHeadArray->Add(sz);
	}

	// Data
	sz.Format(_T("%04d/%02d/%02d[%02d:%02d:%02d]"), tm.wYear, tm.wMonth, tm.wDay, tm.wHour, tm.wMinute, tm.wSecond);
	pszDataArray->Add(sz);

	// Barcode
	sz.Format(_T("%s"), szBarcode);
	pszDataArray->Add(sz);

	// 광원 전압
	sz.Format(_T("%.2fV"), fVoltage);
	pszDataArray->Add(sz);

	// Result
	sz.Format(_T("%d"), stResult.nResult);
	pszDataArray->Add(sz);

	for (UINT nAdd = 0; nAdd < ROI_Shading_Max; nAdd++)
	{
		sz.Format(_T("%d"), stResult.sSignal[nAdd]);
		pszDataArray->Add(sz);

		sz.Format(_T("%.2f"), stResult.fPercent[nAdd]);
		pszDataArray->Add(sz);
	}

	return TRUE;
}

//=============================================================================
// Method		: MakeLightSNRHeadAndData
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nParaIdx
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_SNR_Light_Data stResult
// Parameter	: __in CStringArray * pszHeadArray
// Parameter	: __in CStringArray * pszDataArray
// Qualifier	:
// Last Update	: 2017/12/14 - 14:08
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::MakeLightSNRHeadAndData(__in UINT nParaIdx, __in CString szBarcode, __in float fVoltage, __in ST_SNR_Light_Data stResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray)
{
	CString sz;
	CString szHeaderz[] = { _T("Date"), _T("WIP_ID"), _T("Light"), _T("Result") };

	ASSERT(pszHeadArray == NULL);
	ASSERT(pszDataArray == NULL);

	pszHeadArray->RemoveAll();
	pszDataArray->RemoveAll();

	SYSTEMTIME tm;
	GetLocalTime(&tm);

	UINT nCount = sizeof(szHeaderz) / sizeof(szHeaderz[0]);

	for (UINT nAdd = 0; nAdd < nCount; nAdd++)
	{
		pszHeadArray->Add(szHeaderz[nAdd]);
	}

	for (UINT nAdd = 0; nAdd < ROI_SNR_Light_Max; nAdd++)
	{
		sz.Format(_T("Value[%d]"), nAdd);
		pszHeadArray->Add(sz);
	}

	// Data
	sz.Format(_T("%04d/%02d/%02d[%02d:%02d:%02d]"), tm.wYear, tm.wMonth, tm.wDay, tm.wHour, tm.wMinute, tm.wSecond);
	pszDataArray->Add(sz);

	// Barcode
	sz.Format(_T("%s"), szBarcode);
	pszDataArray->Add(sz);

	// 광원 전압
	sz.Format(_T("%.2fV"), fVoltage);
	pszDataArray->Add(sz);

	// Result
	sz.Format(_T("%d"), stResult.nResult);
	pszDataArray->Add(sz);

	for (UINT nAdd = 0; nAdd < ROI_SNR_Light_Max; nAdd++)
	{
		sz.Format(_T("%.2f"), stResult.dbEtcValue[nAdd]);
		pszHeadArray->Add(sz);
	}

	return TRUE;
}

//=============================================================================
// Method		: MakeLightTemperatureHeadAndData
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nParaIdx
// Parameter	: __in CString szBarcode
// Parameter	: __in long lTemperature
// Parameter	: __in CStringArray * pszHeadArray
// Parameter	: __in CStringArray * pszDataArray
// Qualifier	:
// Last Update	: 2017/12/19 - 13:19
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::MakeLightTemperatureHeadAndData(__in UINT nParaIdx, __in CString szBarcode, __in long lTemperature, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray)
{
	CString sz;
	CString szHeaderz[] = { _T("Date"), _T("WIP_ID"), _T("Value") };

	ASSERT(pszHeadArray == NULL);
	ASSERT(pszDataArray == NULL);

	pszHeadArray->RemoveAll();
	pszDataArray->RemoveAll();

	SYSTEMTIME tm;
	GetLocalTime(&tm);

	UINT nCount = sizeof(szHeaderz) / sizeof(szHeaderz[0]);

	for (UINT nAdd = 0; nAdd < nCount; nAdd++)
	{
		pszHeadArray->Add(szHeaderz[nAdd]);
	}

	// Data
	sz.Format(_T("%04d/%02d/%02d[%02d:%02d:%02d]"), tm.wYear, tm.wMonth, tm.wDay, tm.wHour, tm.wMinute, tm.wSecond);
	pszDataArray->Add(sz);

	// Barcode
	sz.Format(_T("%s"), szBarcode);
	pszDataArray->Add(sz);

	sz.Format(_T("%d(C)"), lTemperature);
	pszDataArray->Add(sz);

	return TRUE;
}

//=============================================================================
// Method		: SaveFinalizeResultTemperature
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in CString szModel
// Parameter	: __in CString szBarcode
// Parameter	: __in long lTemperature
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/19 - 13:18
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::SaveFinalizeResultTemperature(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in long lTemperature, __in UINT nParaIdx /*= 0*/)
{
	CString szFullPath;

	CStringArray arrHeaderz;
	CStringArray arrDataz;

	MakeLightTemperatureHeadAndData(nParaIdx, szBarcode, lTemperature, &arrHeaderz, &arrDataz);

	SYSTEMTIME tm;
	GetLocalTime(&tm);

	// 폴더 생성
	CString szDatePath;
	szDatePath.Format(_T("%s%s\\%04d-%02d-%02d\\"), szPath, szModel, tm.wYear, tm.wMonth, tm.wDay);
	MakeDirectory(szDatePath);

	szFullPath.Format(_T("%sTemperature.csv"), szDatePath);
	SaveFinalizeResult(nParaIdx, szFullPath, &arrHeaderz, &arrDataz);

	return TRUE;
}

//=============================================================================
// Method		: SaveFinalizeResultDefectPixel
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in CString szModel
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_DefectPixel_Data stResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/14 - 14:08
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::SaveFinalizeResultDefectPixel(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_DefectPixel_Data stResult, __in float fVoltage, __in UINT nParaIdx /*= 0*/)
{
	CString szFullPath;

	CStringArray arrHeaderz;
	CStringArray arrDataz;

	MakeDefectPixelHeadAndData(nParaIdx, szBarcode, fVoltage, stResult, &arrHeaderz, &arrDataz);

	SYSTEMTIME tm;
	GetLocalTime(&tm);

	// 폴더 생성
	CString szDatePath;
	szDatePath.Format(_T("%s%s\\%04d-%02d-%02d\\"), szPath, szModel, tm.wYear, tm.wMonth, tm.wDay);
	MakeDirectory(szDatePath);

	szFullPath.Format(_T("%sDefectPixel.csv"), szDatePath);
	SaveFinalizeResult(nParaIdx, szFullPath, &arrHeaderz, &arrDataz);

	return TRUE;
}

//=============================================================================
// Method		: SaveFinalizeResultDeadPixel
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in CString szModel
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_Particle_Data stResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/14 - 14:08
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::SaveFinalizeResultDeadPixel(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_Particle_Data stResult, __in float fVoltage, __in UINT nParaIdx /*= 0*/)
{
	CString szFullPath;

	CStringArray arrHeaderz;
	CStringArray arrDataz;

	MakeDeadPixelHeadAndData(nParaIdx, szBarcode, fVoltage, stResult, &arrHeaderz, &arrDataz);

	SYSTEMTIME tm;
	GetLocalTime(&tm);

	// 폴더 생성
	CString szDatePath;
	szDatePath.Format(_T("%s%s\\%04d-%02d-%02d\\"), szPath, szModel, tm.wYear, tm.wMonth, tm.wDay);
	MakeDirectory(szDatePath);

	szFullPath.Format(_T("%sDeadPixel.csv"), szDatePath);
	SaveFinalizeResult(nParaIdx, szFullPath, &arrHeaderz, &arrDataz);

	return TRUE;
}

//=============================================================================
// Method		: SaveFinalizeResultBlemish
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in CString szModel
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_Particle_Data stResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/14 - 14:08
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::SaveFinalizeResultBlemish(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_Particle_Data stResult, __in float fVoltage, __in UINT nParaIdx /*= 0*/)
{
	CString szFullPath;

	CStringArray arrHeaderz;
	CStringArray arrDataz;

	MakeBlemishHeadAndData(nParaIdx, szBarcode, fVoltage, stResult, &arrHeaderz, &arrDataz);

	SYSTEMTIME tm;
	GetLocalTime(&tm);

	// 폴더 생성
	CString szDatePath;
	szDatePath.Format(_T("%s%s\\%04d-%02d-%02d\\"), szPath, szModel, tm.wYear, tm.wMonth, tm.wDay);
	MakeDirectory(szDatePath);

	szFullPath.Format(_T("%sBlemish.csv"), szDatePath);
	SaveFinalizeResult(nParaIdx, szFullPath, &arrHeaderz, &arrDataz);

	return TRUE;
}

//=============================================================================
// Method		: SaveFinalizeResultStain
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in CString szModel
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_Particle_Data stResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/14 - 14:09
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::SaveFinalizeResultStain(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_Particle_Data stResult, __in float fVoltage, __in UINT nParaIdx /*= 0*/)
{
	CString szFullPath;

	CStringArray arrHeaderz;
	CStringArray arrDataz;

	MakeStainHeadAndData(nParaIdx, szBarcode, fVoltage, stResult, &arrHeaderz, &arrDataz);

	SYSTEMTIME tm;
	GetLocalTime(&tm);

	// 폴더 생성
	CString szDatePath;
	szDatePath.Format(_T("%s%s\\%04d-%02d-%02d\\"), szPath, szModel, tm.wYear, tm.wMonth, tm.wDay);
	MakeDirectory(szDatePath);

	szFullPath.Format(_T("%sStain.csv"), szDatePath);
	SaveFinalizeResult(nParaIdx, szFullPath, &arrHeaderz, &arrDataz);

	return TRUE;
}

//=============================================================================
// Method		: SaveFinalizeResultRtllumination
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in CString szModel
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_Shading_Data stResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/14 - 14:09
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::SaveFinalizeResultRtllumination(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_Shading_Data stResult, __in float fVoltage, __in UINT nParaIdx /*= 0*/)
{
	CString szFullPath;

	CStringArray arrHeaderz;
	CStringArray arrDataz;

	MakeRtlluminationHeadAndData(nParaIdx, szBarcode, fVoltage, stResult, &arrHeaderz, &arrDataz);

	SYSTEMTIME tm;
	GetLocalTime(&tm);

	// 폴더 생성
	CString szDatePath;
	szDatePath.Format(_T("%s%s\\%04d-%02d-%02d\\"), szPath, szModel, tm.wYear, tm.wMonth, tm.wDay);
	MakeDirectory(szDatePath);

	szFullPath.Format(_T("%sRtllumination.csv"), szDatePath);
	SaveFinalizeResult(nParaIdx, szFullPath, &arrHeaderz, &arrDataz);

	return TRUE;
}

//=============================================================================
// Method		: SaveFinalizeResultLightSNR
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in CString szModel
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_SNR_Light_Data stResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/14 - 14:09
// Desc.		:
//=============================================================================
BOOL CFile_Report_VI::SaveFinalizeResultLightSNR(__in LPCTSTR szPath, __in CString szModel, __in CString szBarcode, __in ST_SNR_Light_Data stResult, __in float fVoltage, __in UINT nParaIdx /*= 0*/)
{
	CString szFullPath;

	CStringArray arrHeaderz;
	CStringArray arrDataz;

	MakeLightSNRHeadAndData(nParaIdx, szBarcode, fVoltage, stResult, &arrHeaderz, &arrDataz);

	SYSTEMTIME tm;
	GetLocalTime(&tm);

	// 폴더 생성
	CString szDatePath;
	szDatePath.Format(_T("%s%s\\%04d-%02d-%02d\\"), szPath, szModel, tm.wYear, tm.wMonth, tm.wDay);
	MakeDirectory(szDatePath);

	szFullPath.Format(_T("%sLightSNR.csv"), szDatePath);
	SaveFinalizeResult(nParaIdx, szFullPath, &arrHeaderz, &arrDataz);

	return TRUE;
}