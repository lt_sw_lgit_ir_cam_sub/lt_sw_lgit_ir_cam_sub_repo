﻿// List_SFROp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_SFROp.h"

// CList_SFROp

typedef enum enListNum_SFROp
{
	SfOp_Object = 0,
	SfOp_PosX,
	SfOp_PosY,
	SfOp_Width,
	SfOp_Height,
	SfOp_Linepare,
	SfOp_Offset,
	SfOp_MinSpecUse,
	SfOp_MinSpec,
	SfOp_MaxSpecUse,
	SfOp_MaxSpec,
	SfOp_Group,
	SfOp_ItemPos,
	SfOp_MaxCol,
};

static LPCTSTR	g_lpszHeader_SFROp[] =
{
	_T(""),						 //SfOp_Object = 0,
	_T("CenterX"),				 //SfOp_PosX,
	_T("CenterY"),				 //SfOp_PosY,
	_T("Width"),				 //SfOp_Width,
	_T("Height"),				 //SfOp_Height,
	_T("Linepare"),				 //SfOp_Linepare,
	_T("Offset"),				 //SfOp_Offset,
	_T(""),						 //SfOp_MinSpecUse,
	_T("MinSpec"),				 //SfOp_MinSpec,
	_T(""),						 //SfOp_MaxSpecUse,
	_T("MaxSpec"),				 //SfOp_MaxSpec,
	_T("Group"),				 //SfOp_Group,
	_T("PIC"),					 //SfOp_ItemPos,
	NULL
};

static LPCTSTR	g_lpszItem_SFROp[] =
{
	NULL
};

const int	iListAglin_SFROp[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_SFROp[] =
{
	50,
	85,
	85,
	85,
	85,
	90,
	90,
	25,
	90,
	25,
	90,
	70,
	70,
};

#define IDC_EDT_CELLEDIT			1000
#define IDC_COM_CELLCOMBO_TYPE		2000
#define IDC_COM_CELLCOMBO_COLOR		3000
#define IDC_COM_CELLCOMBO_ITEM		4000
#define IDC_COM_CELLCOMBO_GROUP		5000

IMPLEMENT_DYNAMIC(CList_SFROp, CList_Cfg_VIBase)

CList_SFROp::CList_SFROp()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol = 0;
	m_nEditRow = 0;
}

CList_SFROp::~CList_SFROp()
{
	m_Font.DeleteObject();
}
BEGIN_MESSAGE_MAP(CList_SFROp, CList_Cfg_VIBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT	(NM_CLICK,					&CList_SFROp::OnNMClick					)
	ON_NOTIFY_REFLECT	(NM_DBLCLK,					&CList_SFROp::OnNMDblclk				)
	ON_EN_KILLFOCUS		(IDC_EDT_CELLEDIT,			&CList_SFROp::OnEnKillFocusEdit			)
	ON_CBN_KILLFOCUS	(IDC_COM_CELLCOMBO_ITEM,	&CList_SFROp::OnEnKillFocusComboItem	)
	ON_CBN_SELCHANGE	(IDC_COM_CELLCOMBO_ITEM,	&CList_SFROp::OnEnSelectComboItem		)	
	ON_CBN_KILLFOCUS	(IDC_COM_CELLCOMBO_GROUP,	&CList_SFROp::OnEnKillFocusComboGroup	)	
	ON_CBN_SELCHANGE	(IDC_COM_CELLCOMBO_GROUP,	&CList_SFROp::OnEnSelectComboGroup		)	
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_SFROp 메시지 처리기입니다.
int CList_SFROp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CList_Cfg_VIBase::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	SetFont(&m_Font);
	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER | LVS_EX_CHECKBOXES);

	InitHeader(); 

	m_ed_CellEdit.Create(WS_CHILD | WS_BORDER | ES_CENTER | WS_TABSTOP, rectDummy, this, IDC_EDT_CELLEDIT);
	m_ed_CellEdit.SetValidChars(_T("-.0123456789"));

	this->GetHeaderCtrl()->EnableWindow(FALSE);
	
	m_cb_Item.Create(WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | CBS_DROPDOWNLIST, CRect(0, 0, 0, 0), this, IDC_COM_CELLCOMBO_ITEM);
	m_cb_Item.ResetContent();

	for (UINT nIdex = 0; NULL != g_szItemPos[nIdex]; nIdex++)
	{
		m_cb_Item.InsertString(nIdex, g_szItemPos[nIdex]);
	}

	m_cb_Group.Create(WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | CBS_DROPDOWNLIST, CRect(0, 0, 0, 0), this, IDC_COM_CELLCOMBO_GROUP);
	m_cb_Group.ResetContent();

	//for (UINT nIdex = 0; NULL != g_szSFR[nIdex]; nIdex++)
	for (UINT nIdex = 0; NULL != g_szSFRGroup[nIdex]; nIdex++)
	{
		m_cb_Group.InsertString(nIdex, g_szSFRGroup[nIdex]);
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_SFROp::OnSize(UINT nType, int cx, int cy)
{
	CList_Cfg_VIBase::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_SFROp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CList_Cfg_VIBase::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_SFROp::InitHeader()
{
	for (int nCol = 0; nCol < SfOp_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_SFROp[nCol], iListAglin_SFROp[nCol], iHeaderWidth_SFROp[nCol]);
	}

	for (int nCol = 0; nCol < SfOp_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_SFROp[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 17:25
// Desc.		:
//=============================================================================
void CList_SFROp::InsertFullData()
{
	if ( NULL == m_pstConfigInfo)
		return;

 	DeleteAllItems();
 
	for (UINT nIdx = 0; nIdx < SfOp_ItemNum; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
 	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/6/26 - 14:26
// Desc.		:
//=============================================================================
void CList_SFROp::SetRectRow(UINT nRow)
{
	if (NULL == m_pstConfigInfo)
		return;

	CString strValue;

	strValue.Format(_T("%d"), nRow);
	SetItemText(nRow, SfOp_Object, strValue);

	if (m_pstConfigInfo->stRegion[nRow].bEnable == TRUE)
		SetItemState(nRow, 0x2000, LVIS_STATEIMAGEMASK);
	else
		SetItemState(nRow, 0x1000, LVIS_STATEIMAGEMASK);

	strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.CenterPoint().x);
	SetItemText(nRow, SfOp_PosX, strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.CenterPoint().y);
	SetItemText(nRow, SfOp_PosY, strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.Width());
	SetItemText(nRow, SfOp_Width, strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.Height());
	SetItemText(nRow, SfOp_Height, strValue);

	strValue.Format(_T("%.2f"), m_pstConfigInfo->stRegion[nRow].dbOffset);
	SetItemText(nRow, SfOp_Offset, strValue);

	strValue.Format(_T("%.2f"), m_pstConfigInfo->stRegion[nRow].dbLinePair);
	SetItemText(nRow, SfOp_Linepare, strValue);

	strValue.Format(_T("%s"), g_szSFRGroup[m_pstConfigInfo->stRegion[nRow].nSFR_Group]);
	SetItemText(nRow, SfOp_Group, strValue);

	strValue.Format(_T("%s"), g_szItemPos[m_pstConfigInfo->stRegion[nRow].nItemPos]);
	SetItemText(nRow, SfOp_ItemPos, strValue);

	strValue.Format(_T("%s"), g_szSpecUse_Static[m_pstConfigInfo->stInput[nRow].stSpecMin.bEnable]);
	SetItemText(nRow, SfOp_MinSpecUse, strValue);

	strValue.Format(_T("%.2f"), m_pstConfigInfo->stInput[nRow].stSpecMin.dbValue);
	SetItemText(nRow, SfOp_MinSpec, strValue);

	strValue.Format(_T("%s"), g_szSpecUse_Static[m_pstConfigInfo->stInput[nRow].stSpecMax.bEnable]);
	SetItemText(nRow, SfOp_MaxSpecUse, strValue);

	strValue.Format(_T("%.2f"), m_pstConfigInfo->stInput[nRow].stSpecMax.dbValue);
	SetItemText(nRow, SfOp_MaxSpec, strValue);

}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_SFROp::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	LVHITTESTINFO HitInfo;
	HitInfo.pt = pNMItemActivate->ptAction;
	
	HitTest(&HitInfo);

	// Check Ctrl Event
	if (HitInfo.flags == LVHT_ONITEMSTATEICON)
	{
		UINT nBuffer;

		nBuffer = GetItemState(pNMItemActivate->iItem, LVIS_STATEIMAGEMASK);

		if (nBuffer == 0x2000)
			m_pstConfigInfo->stRegion[pNMItemActivate->iItem].bEnable = FALSE;

		if (nBuffer == 0x1000)
			m_pstConfigInfo->stRegion[pNMItemActivate->iItem].bEnable = TRUE;
	}

	if (SfOp_MinSpecUse == pNMItemActivate->iSubItem)
	{
		m_pstConfigInfo->stInput[pNMItemActivate->iItem].stSpecMin.bEnable = !m_pstConfigInfo->stInput[pNMItemActivate->iItem].stSpecMin.bEnable;
		SetItemText(pNMItemActivate->iItem, pNMItemActivate->iSubItem, g_szSpecUse_Static[m_pstConfigInfo->stInput[pNMItemActivate->iItem].stSpecMin.bEnable]);
	}

	if (SfOp_MaxSpecUse == pNMItemActivate->iSubItem)
	{
		m_pstConfigInfo->stInput[pNMItemActivate->iItem].stSpecMax.bEnable = !m_pstConfigInfo->stInput[pNMItemActivate->iItem].stSpecMax.bEnable;
		SetItemText(pNMItemActivate->iItem, pNMItemActivate->iSubItem, g_szSpecUse_Static[m_pstConfigInfo->stInput[pNMItemActivate->iItem].stSpecMax.bEnable]);
	}
	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_SFROp::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (SfOp_MinSpecUse == pNMItemActivate->iSubItem || SfOp_MaxSpecUse == pNMItemActivate->iSubItem)
		{
			*pResult = 1;
			return;
		}

		if (pNMItemActivate->iSubItem == SfOp_Object)
		{
			LVHITTESTINFO HitInfo;
			HitInfo.pt = pNMItemActivate->ptAction;

			HitTest(&HitInfo);

			// Check Ctrl Event
			if (HitInfo.flags == LVHT_ONITEMSTATEICON)
			{
				UINT nBuffer;

				nBuffer = GetItemState(pNMItemActivate->iItem, LVIS_STATEIMAGEMASK);

				if (nBuffer == 0x2000)
					m_pstConfigInfo->stRegion[pNMItemActivate->iItem].bEnable = FALSE;

				if (nBuffer == 0x1000)
					m_pstConfigInfo->stRegion[pNMItemActivate->iItem].bEnable = TRUE;
			}
		}

		if (pNMItemActivate->iSubItem < SfOp_MaxCol && pNMItemActivate->iSubItem > 0)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);
			
			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

			if (pNMItemActivate->iSubItem == SfOp_ItemPos)
			{
				m_cb_Item.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_cb_Item.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_cb_Item.SetFocus();
				m_cb_Item.SetCurSel(m_pstConfigInfo->stRegion[m_nEditRow].nItemPos);
			}
			else if (pNMItemActivate->iSubItem == SfOp_Group)
			{
				m_cb_Group.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_cb_Group.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_cb_Group.SetFocus();
				m_cb_Group.SetCurSel(m_pstConfigInfo->stRegion[m_nEditRow].nSFR_Group);
			}
			else
			{
				m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_ed_CellEdit.SetFocus();
			}

			
		}
		GetOwner()->SendNotifyMessage(m_nMessage, 0, 0);
	}

	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
void CList_SFROp::OnEnKillFocusEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	if (m_nEditCol == SfOp_MinSpec || m_nEditCol == SfOp_MaxSpec || m_nEditCol == SfOp_Offset || m_nEditCol == SfOp_Linepare)
	{
		UpdateCelldbData(m_nEditRow, m_nEditCol, _ttof(strText));
	}
	else
	{
		UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText));
	}

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
BOOL CList_SFROp::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	if ( NULL == m_pstConfigInfo)
		return FALSE;

	if (iValue < 0)
		iValue = 0;

	CRect rtTemp = m_pstConfigInfo->stRegion[nRow].rtROI;

	switch (nCol)
	{
	case SfOp_PosX:
		m_pstConfigInfo->stRegion[nRow].RectPosXY(iValue, rtTemp.CenterPoint().y);
		break;
	case SfOp_PosY:
		m_pstConfigInfo->stRegion[nRow].RectPosXY(rtTemp.CenterPoint().x, iValue);
		break;
	case SfOp_Width:
		m_pstConfigInfo->stRegion[nRow].RectPosWH(iValue, rtTemp.Height());
		break;
	case SfOp_Height:
		m_pstConfigInfo->stRegion[nRow].RectPosWH(rtTemp.Width(), iValue);
		break;
	default:
		break;
	}

	if (m_pstConfigInfo->stRegion[nRow].rtROI.left < 0)
	{
		CRect rtTemp = m_pstConfigInfo->stRegion[nRow].rtROI;

		m_pstConfigInfo->stRegion[nRow].rtROI.left = 0;
		m_pstConfigInfo->stRegion[nRow].rtROI.right = m_pstConfigInfo->stRegion[nRow].rtROI.left + rtTemp.Width();
	}

	if (m_pstConfigInfo->stRegion[nRow].rtROI.right >(LONG)m_dwImage_Width)
	{
		CRect rtTemp = m_pstConfigInfo->stRegion[nRow].rtROI;

		m_pstConfigInfo->stRegion[nRow].rtROI.right = (LONG)m_dwImage_Width;
		m_pstConfigInfo->stRegion[nRow].rtROI.left = m_pstConfigInfo->stRegion[nRow].rtROI.right - rtTemp.Width();
	}

	if (m_pstConfigInfo->stRegion[nRow].rtROI.top < 0)
	{
		CRect rtTemp = m_pstConfigInfo->stRegion[nRow].rtROI;

		m_pstConfigInfo->stRegion[nRow].rtROI.top = 0;
		m_pstConfigInfo->stRegion[nRow].rtROI.bottom = rtTemp.Height() + m_pstConfigInfo->stRegion[nRow].rtROI.top;
	}

	if (m_pstConfigInfo->stRegion[nRow].rtROI.bottom >(LONG)m_dwImage_Height)
	{
		CRect rtTemp = m_pstConfigInfo->stRegion[nRow].rtROI;

		m_pstConfigInfo->stRegion[nRow].rtROI.bottom = (LONG)m_dwImage_Height;
		m_pstConfigInfo->stRegion[nRow].rtROI.top = m_pstConfigInfo->stRegion[nRow].rtROI.bottom - rtTemp.Height();
	}

	if (m_pstConfigInfo->stRegion[nRow].rtROI.Height() <= 0)
		m_pstConfigInfo->stRegion[nRow].RectPosWH(m_pstConfigInfo->stRegion[nRow].rtROI.Width(), 1);

	if (m_pstConfigInfo->stRegion[nRow].rtROI.Height() >= (LONG)m_dwImage_Height)
		m_pstConfigInfo->stRegion[nRow].RectPosWH(m_pstConfigInfo->stRegion[nRow].rtROI.Width(), m_dwImage_Height);

	if (m_pstConfigInfo->stRegion[nRow].rtROI.Width() <= 0)
		m_pstConfigInfo->stRegion[nRow].RectPosWH(1, m_pstConfigInfo->stRegion[nRow].rtROI.Height());

	if (m_pstConfigInfo->stRegion[nRow].rtROI.Width() >= (LONG)m_dwImage_Width)
		m_pstConfigInfo->stRegion[nRow].RectPosWH(m_dwImage_Width, m_pstConfigInfo->stRegion[nRow].rtROI.Height());

	CString strValue;

	switch (nCol)
	{
	case SfOp_PosX:
		strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.CenterPoint().x);
		break;
	case SfOp_PosY:
		strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.CenterPoint().y);
		break;
	case SfOp_Width:
		strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.Width());
		break;
	case SfOp_Height:
		strValue.Format(_T("%d"), m_pstConfigInfo->stRegion[nRow].rtROI.Height());
		break;
	default:
		break;
	}

	m_ed_CellEdit.SetWindowText(strValue);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: UpdateCelldbData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dbValue
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_SFROp::UpdateCelldbData(UINT nRow, UINT nCol, double dbValue)
{
	switch (nCol)
	{
	case SfOp_Linepare:
		m_pstConfigInfo->stRegion[nRow].dbLinePair = dbValue;
		break;
	case SfOp_Offset:
		m_pstConfigInfo->stRegion[nRow].dbOffset = dbValue;
		break;
	case SfOp_MinSpec:
		m_pstConfigInfo->stInput[nRow].stSpecMin.dbValue = dbValue;
		break;
	case SfOp_MaxSpec:
		m_pstConfigInfo->stInput[nRow].stSpecMax.dbValue = dbValue;
		break;
	default:
		break;
	}

	CString str;
	str.Format(_T("%.2f"), dbValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_SFROp::GetCellData()
{
	if ( NULL == m_pstConfigInfo)
		return;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_SFROp::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue		= _ttoi(strText);
		double dbValue	= _ttof(strText);


		if (m_nEditCol == SfOp_Offset || m_nEditCol == SfOp_MinSpec || m_nEditCol == SfOp_MaxSpec)
		{
			iValue = iValue + ((zDelta / 120));
			dbValue = dbValue + ((zDelta / 120) * 0.01);
		}
		else
		{
			if (0 < zDelta)
			{
				iValue = iValue + ((zDelta / 120));
				dbValue = dbValue + ((zDelta / 120) * 0.01);
			}
			else
			{
				if (0 < iValue)
					iValue = iValue + ((zDelta / 120));

				if (0 < dbValue)
					dbValue = dbValue + ((zDelta / 120) * 0.01);
			}

			if (iValue < 0)
				iValue = 0;

			if (iValue > 2000)
				iValue = 2000;

			if (dbValue < 0.0)
				dbValue = 0.0;

		}
	

		if (m_nEditCol == SfOp_MinSpec || m_nEditCol == SfOp_MaxSpec || m_nEditCol == SfOp_Offset || m_nEditCol == SfOp_Linepare)
		{
			UpdateCelldbData(m_nEditRow, m_nEditCol, dbValue);
		}
		else
		{
			UpdateCellData(m_nEditRow, m_nEditCol, iValue);
		}
	}

	return CList_Cfg_VIBase::OnMouseWheel(nFlags, zDelta, pt);
}

//=============================================================================
// Method		: OnEnKillFocusComboItem
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/2 - 11:36
// Desc.		:
//=============================================================================
void CList_SFROp::OnEnKillFocusComboItem()
{
	SetItemText(m_nEditRow, SfOp_ItemPos, g_szItemPos[m_pstConfigInfo->stRegion[m_nEditRow].nItemPos]);
	m_cb_Item.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: OnEnSelectComboItem
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/2 - 11:36
// Desc.		:
//=============================================================================
void CList_SFROp::OnEnSelectComboItem()
{
	m_pstConfigInfo->stRegion[m_nEditRow].nItemPos = m_cb_Item.GetCurSel();

	SetItemText(m_nEditRow, SfOp_ItemPos, g_szItemPos[m_pstConfigInfo->stRegion[m_nEditRow].nItemPos]);
	m_cb_Item.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: OnEnKillFocusComboGroup
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/6/3 - 16:11
// Desc.		:
//=============================================================================
void CList_SFROp::OnEnKillFocusComboGroup()
{
	SetItemText(m_nEditRow, SfOp_Group, g_szSFRGroup[m_pstConfigInfo->stRegion[m_nEditRow].nSFR_Group]);
	m_cb_Group.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: OnEnSelectComboGroup
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/6/3 - 16:11
// Desc.		:
//=============================================================================
void CList_SFROp::OnEnSelectComboGroup()
{
	m_pstConfigInfo->stRegion[m_nEditRow].nSFR_Group = m_cb_Group.GetCurSel();

	SetItemText(m_nEditRow, SfOp_Group, g_szSFRGroup[m_pstConfigInfo->stRegion[m_nEditRow].nSFR_Group]);
	m_cb_Group.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}
