// Wnd_CurrentEntryData.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_CurrentEntryData.h"

// CWnd_CurrentEntryData

IMPLEMENT_DYNAMIC(CWnd_CurrentEntryData, CWnd)

CWnd_CurrentEntryData::CWnd_CurrentEntryData()
{
}

CWnd_CurrentEntryData::~CWnd_CurrentEntryData()
{
}

BEGIN_MESSAGE_MAP(CWnd_CurrentEntryData, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

// CWnd_CurrentEntryData 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2018/5/12 - 11:20
// Desc.		:
//=============================================================================
int CWnd_CurrentEntryData::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	DWORD dwStyle = WS_VISIBLE | WS_CHILD /*| WS_CLIPCHILDREN*/ | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

 	for (int t = 0; t < (int)m_nHeaderCnt; t++)
 	{
 		m_st_Header[t].SetStaticStyle(CVGStatic::StaticStyle_Title);
 		m_st_Header[t].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Header[t].SetFont_Gdip(L"Arial", 12.0F);
		m_st_Header[t].Create(g_szCREData[t], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
 
 		for (int i= 0;i<enCREData_inputmax; i++)
 		{
 			m_st_Data[i][t].SetStaticStyle(CVGStatic::StaticStyle_Title);
 			m_st_Data[i][t].SetColorStyle(CVGStatic::ColorStyle_Default);
			m_st_Data[i][t].SetFont_Gdip(L"Arial", 12.0F);
 			m_st_Data[i][t].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
 		}
 	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2018/5/12 - 11:20
// Desc.		:
//=============================================================================
void CWnd_CurrentEntryData::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iMargin = 0;
	int iSpacing = 0;
	int iCateSpacing = 0;

	int iLeft = iMargin;
	int iTop = iMargin;
	int iWidth = cx - iMargin - iMargin;
	int iHeight = cy - iMargin - iMargin;

	int HeaderW = (iWidth+2) / 2;

	int stW = iWidth;
	int stH = (672 * iWidth) / 1280;

	int HeaderH = iHeight / (10);


	for (UINT nIdx = 0; nIdx < m_nHeaderCnt; nIdx++)
	{
		iLeft = iMargin;
		m_st_Header[nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);
		iLeft += HeaderW - 1;
		m_st_Data[0][nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);
 		iTop += (HeaderH - 1);
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2018/5/12 - 11:19
// Desc.		:
//=============================================================================
BOOL CWnd_CurrentEntryData::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}


//=============================================================================
// Method		: OnEraseBkgnd
// Access		: public  
// Returns		: BOOL
// Parameter	: CDC * pDC
// Qualifier	:
// Last Update	: 2018/5/12 - 11:19
// Desc.		:
//=============================================================================
BOOL CWnd_CurrentEntryData::OnEraseBkgnd(CDC* pDC)
{
	return CWnd::OnEraseBkgnd(pDC);
}


//=============================================================================
// Method		: DataSetting
// Access		: public  
// Returns		: void
// Parameter	: UINT nDataIdx
// Parameter	: UINT nIdx
// Parameter	: BOOL MODE
// Parameter	: double dData
// Qualifier	:
// Last Update	: 2018/5/12 - 11:19
// Desc.		:
//=============================================================================
void CWnd_CurrentEntryData::DataSetting(UINT nDataIdx, UINT nIdx, BOOL MODE, double dData)
{
	if (MODE == TRUE)
	{
		m_st_Data[nDataIdx][nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
	}
	else{
		m_st_Data[nDataIdx][nIdx].SetColorStyle(CVGStatic::ColorStyle_Red);
	}

	CString strData;

	strData.Format(_T("%6.3f"), dData);

	m_st_Data[nDataIdx][nIdx].SetWindowText(strData);
}

//=============================================================================
// Method		: DataEachReset
// Access		: public  
// Returns		: void
// Parameter	: UINT nDataIdx
// Parameter	: UINT nIdx
// Qualifier	:
// Last Update	: 2018/5/12 - 11:19
// Desc.		:
//=============================================================================
void CWnd_CurrentEntryData::DataEachReset(UINT nDataIdx, UINT nIdx)
{
	m_st_Data[nDataIdx][nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_Data[nDataIdx][nIdx].SetWindowText(_T(""));
}


//=============================================================================
// Method		: DataReset
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/5/12 - 11:19
// Desc.		:
//=============================================================================
void CWnd_CurrentEntryData::DataReset()
{
	for (UINT nIdx = 0; nIdx < m_nHeaderCnt; nIdx++)
	{
		m_st_Data[0][nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Data[0][nIdx].SetWindowText(_T(""));
	}
}

//=============================================================================
// Method		: DataDisplay
// Access		: public  
// Returns		: void
// Parameter	: ST_ECurrent_Data stData
// Qualifier	:
// Last Update	: 2018/5/12 - 11:19
// Desc.		:
//=============================================================================
void CWnd_CurrentEntryData::DataDisplay(ST_ECurrent_Data stData)
{
	CString strData;

	for (int t = 0; t < m_nHeaderCnt; t++)
	{
		if (stData.nEachResult[t]== TRUE)
		{
			m_st_Data[0][t].SetColorStyle(CVGStatic::ColorStyle_Default);
		}
		else
		{
			m_st_Data[0][t].SetColorStyle(CVGStatic::ColorStyle_Red);
		}

		strData.Format(_T("%.2f mA"), stData.dbValue[t]);
		m_st_Data[0][t].SetWindowText(strData);
	}
}