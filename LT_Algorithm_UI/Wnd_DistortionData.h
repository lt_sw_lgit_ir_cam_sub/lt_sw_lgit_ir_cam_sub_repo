#ifndef Wnd_DistortionData_h__
#define Wnd_DistortionData_h__

#pragma once

#include "afxwin.h"

#include "VGStatic.h"
#include "Def_T_Distortion.h"

// CWnd_DistortionData
typedef enum
{
	enDTData_Header = 0,
	enDTData_Data = 0,
	enDTData_MAX,
}enumHeaderDTData;

typedef enum
{
	enDTData_1st = 0,
	enDTData_inputmax,
}enumDataDTData;

static	LPCTSTR	g_szDTData[] =
{
	_T("Distortion [ % ]"),
	NULL
};
class CWnd_DistortionData : public CWnd
{
	DECLARE_DYNAMIC(CWnd_DistortionData)

public:
	CWnd_DistortionData();
	virtual ~CWnd_DistortionData();

	UINT m_nHeaderCnt = enDTData_MAX;
	CString m_SzHeaderName[enDTData_MAX];

	//---Class 정의 시 세팅 해야함.
	void SetHeader(UINT nHeaderCnt){

		if (nHeaderCnt > enDTData_MAX)
		{
			nHeaderCnt = enDTData_MAX;
		}
		m_nHeaderCnt = nHeaderCnt;
	};

	void SetHeaderName(UINT nCnt, CString szHeader){

		if (nCnt > enDTData_MAX)
		{
			nCnt = enDTData_MAX-1;
		}
		m_SzHeaderName[nCnt] = szHeader;
	};

	CVGStatic		m_st_Header[enDTData_MAX];
	CVGStatic		m_st_Data[enDTData_inputmax][enDTData_MAX];

//	CVGStatic		m_st_ResultView;


protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	void DataSetting(UINT nDataIdx, UINT nIdx, BOOL MODE, double dData);
	void DataEachReset(UINT nDataIdx, UINT nIdx);
	void DataReset();
	void DataDisplay(ST_Distortion_Data stData);
};


#endif // Wnd_DistortionData_h__
