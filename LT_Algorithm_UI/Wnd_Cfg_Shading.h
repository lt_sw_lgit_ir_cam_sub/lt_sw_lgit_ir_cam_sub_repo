﻿#ifndef Wnd_Cfg_Shading_h__
#define Wnd_Cfg_Shading_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_T_Shading.h"
#include "Def_TestItem_VI.h"
#include "List_ShadingOp.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_Shading
//-----------------------------------------------------------------------------
class CWnd_Cfg_Shading : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_Shading)

public:
	CWnd_Cfg_Shading();
	virtual ~CWnd_Cfg_Shading();

	enum enShadingStatic
	{
		STI_SHA_PARAM = 0,
		STI_SHA_SPEC,
		STI_SHA_Ver_CT,
		STI_SHA_Ver_CB,
		STI_SHA_Hov_LC,
		STI_SHA_Hov_RC,
		STI_SHA_Dig_LT,
		STI_SHA_Dig_LB,
		STI_SHA_Dig_RT,
		STI_SHA_Dig_RB,
		STI_SHA_MAX,
	};

	enum enShadingButton
	{
		BTN_SHA_RESET = 0,
		BTN_SHA_TEST,
		BTN_SHA_MAX,
	};

	enum enShadingComobox
	{
		CMB_SHA_MAX = 1,
	};

	enum enShadingEdit
	{
		EDT_SHA_MAX = 1,
	};

protected:

	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow			(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl			(UINT nID);
	afx_msg void	OnRangeIndexCtrl		(UINT nID);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);

	CFont				m_font;

	CList_ShadingOp		m_List[IDX_Shading_Max];
	CVGStatic			m_st_Idex[ROI_Shading_Max];

	CVGStatic			m_st_Item[STI_SHA_MAX];
	CButton				m_bn_Item[BTN_SHA_MAX];
	CComboBox			m_cb_Item[CMB_SHA_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_SHA_MAX];

	CVGStatic	  		m_st_CapItem;
	CVGStatic	  		m_st_CapSpecMin;
	CVGStatic	  		m_st_CapSpecMax;

	CVGStatic	  		m_st_Spec[Spec_Shading_MAX];
	CMFCMaskedEdit		m_ed_SpecMin[Spec_Shading_MAX];
	CMFCMaskedEdit		m_ed_SpecMax[Spec_Shading_MAX];
	CMFCButton	  		m_chk_SpecMin[Spec_Shading_MAX];
	CMFCButton	  		m_chk_SpecMax[Spec_Shading_MAX];

	ST_Shading_Opt*		m_pstConfigInfo = NULL;


public:

	void	SetPtr_RecipeInfo (ST_Shading_Opt* pstConfigInfo)
	{
		if (pstConfigInfo == NULL)
			return;

		m_pstConfigInfo = pstConfigInfo;
	};

	void SetUpdateData		();
	void GetUpdateData		();

	//void Get_TestItemInfo	(__out ST_TestItemInfo& stOutTestItemInfo);

};
#endif // Wnd_Cfg_Shading_h__
