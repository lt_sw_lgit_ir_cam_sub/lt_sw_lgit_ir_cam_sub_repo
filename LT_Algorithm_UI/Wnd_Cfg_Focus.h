﻿#ifndef Wnd_Cfg_Focus_h__
#define Wnd_Cfg_Focus_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_T_Focus.h"
#include "Def_T_SFR.h"
#include "Def_TestItem_VI.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_Focus
//-----------------------------------------------------------------------------
class CWnd_Cfg_Focus : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_Focus)

public:
	CWnd_Cfg_Focus();
	virtual ~CWnd_Cfg_Focus();

	enum enFocus_Static
	{
		STI_GP_PARAM = 0,
		STI_GP_SPEC,
		STI_GP_SELECT,
		STI_GP_MIN_SFR,
		STI_GP_MAX_SFR,
		STI_GP_MAX,
	};

	enum enFocus_Button
	{
		BTN_GP_MAX = 1,
	};

	enum enFocus_Comobox
	{
		CMB_GP_SELECT_P1 = 0,
		CMB_GP_SELECT_P2,
		CMB_GP_MAX,
	};

	enum enFocus_Edit
	{
		EDT_GP_MIN_SFR_P1 = 0,
		EDT_GP_MAX_SFR_P1,
		EDT_GP_MIN_SFR_P2,
		EDT_GP_MAX_SFR_P2,
		EDT_GP_MAX,
	};

protected:
	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	CFont						m_font;

	CVGStatic					m_st_Item[STI_GP_MAX];
	CButton						m_bn_Item[BTN_GP_MAX];
	CComboBox					m_cb_Item[CMB_GP_MAX];
	CMFCMaskedEdit				m_ed_Item[EDT_GP_MAX];

	CVGStatic					m_st_CapItem;
	CVGStatic					m_st_CapSpecMin;
	CVGStatic					m_st_CapSpecMax;

	CVGStatic					m_st_Spec[Spec_Focus_Max];
	CMFCMaskedEdit				m_ed_SpecMin[Spec_Focus_Max];
	CMFCMaskedEdit				m_ed_SpecMax[Spec_Focus_Max];
	CMFCButton					m_chk_SpecMin[Spec_Focus_Max];
	CMFCButton					m_chk_SpecMax[Spec_Focus_Max];

	ST_Focus_Opt*				m_pstConfigInfo		= NULL;

public:

	void	SetPtr_RecipeInfo (ST_Focus_Opt* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstConfigInfo = pstRecipeInfo;
	};

	void	SetUpdateData		();
	void	GetUpdateData		();
};

#endif // Wnd_Cfg_Focus_h__
