#ifndef Wnd_FPNData_h__
#define Wnd_FPNData_h__

#pragma once

#include "afxwin.h"

#include "VGStatic.h"
#include "Def_T_FPN.h"

// CWnd_FPNData
typedef enum
{
	enFPNData_Header = 0,
	enFPNData_X = 0,
	enFPNData_Y,
	enFPNData_MAX,
}enumHeaderFPNData;

typedef enum
{
	enFPNData_1st = 0,
	enFPNData_inputmax,
}enumDataFPNData;

static	LPCTSTR	g_szFPNData[] =
{
	_T("FPN X"),
	_T("FPN Y"),
	NULL
};

class CWnd_FPNData : public CWnd
{
	DECLARE_DYNAMIC(CWnd_FPNData)

public:
	CWnd_FPNData();
	virtual ~CWnd_FPNData();

	UINT m_nHeaderCnt = enFPNData_MAX;
	CString m_SzHeaderName[enFPNData_MAX];

	//---Class 정의 시 세팅 해야함.
	void SetHeader(UINT nHeaderCnt){

		if (nHeaderCnt > enFPNData_MAX)
		{
			nHeaderCnt = enFPNData_MAX;
		}
		m_nHeaderCnt = nHeaderCnt;
	};

	void SetHeaderName(UINT nCnt, CString szHeader){

		if (nCnt > enFPNData_MAX)
		{
			nCnt = enFPNData_MAX - 1;
		}
		m_SzHeaderName[nCnt] = szHeader;
	};

	CVGStatic		m_st_Header[enFPNData_MAX];
	CVGStatic		m_st_Data[enFPNData_inputmax][enFPNData_MAX];

	//	CVGStatic		m_st_ResultView;


protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	void DataSetting(UINT nDataIdx, UINT nIdx, BOOL MODE, double dData);
	void DataEachReset(UINT nDataIdx, UINT nIdx);
	void DataReset();
	void DataDisplay(ST_FPN_Data stData);
};


#endif // Wnd_FPNData_h__
