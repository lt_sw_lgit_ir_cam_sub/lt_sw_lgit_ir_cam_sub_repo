﻿#ifndef Wnd_Cfg_FPN_h__
#define Wnd_Cfg_FPN_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_T_FPN.h"
#include "Def_TestItem_VI.h"
#include "List_FPNOp.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_FPN
//-----------------------------------------------------------------------------
class CWnd_Cfg_FPN : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_FPN)

public:
	CWnd_Cfg_FPN();
	virtual ~CWnd_Cfg_FPN();

	enum enFPNStatic
	{
		STI_FP_PARAM = 0,
		STI_FP_SPEC,
		STI_FP_ThresholdX,
		STI_FP_ThresholdY,
		STI_FP_MAX,
	};

	enum enFPNButton
	{
		BTN_FP_RESET = 0,
		BTN_FP_TEST,
		BTN_FP_MAX,
	};

	enum enFPNComobox
	{
		CMB_FP_MAX = 1,
	};

	enum enFPNEdit
	{
		EDT_FP_ThresholdX = 0,
		EDT_FP_ThresholdY,
		EDT_FP_MAX,
	};

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow			(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl			(UINT nID);
	afx_msg void	OnRangeIndexCtrl		(UINT nID);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);

	CList_FPNOp	m_List;

	CFont				m_font;
	
	CVGStatic			m_st_Item[STI_FP_MAX];
	CButton				m_bn_Item[BTN_FP_MAX];
	CComboBox			m_cb_Item[CMB_FP_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_FP_MAX];

	CVGStatic	  		m_st_CapItem;
	CVGStatic	  		m_st_CapSpecMin;
	CVGStatic	  		m_st_CapSpecMax;

	CVGStatic	  		m_st_Spec[Spec_FPN_MAX];
	CMFCMaskedEdit		m_ed_SpecMin[Spec_FPN_MAX];
	CMFCMaskedEdit		m_ed_SpecMax[Spec_FPN_MAX];
	CMFCButton	  		m_chk_SpecMin[Spec_FPN_MAX];
	CMFCButton	  		m_chk_SpecMax[Spec_FPN_MAX];

	ST_FPN_Opt*		m_pstConfigInfo = NULL;

public:

	void	SetPtr_RecipeInfo	(ST_FPN_Opt* pstConfigInfo)
	{
		if (pstConfigInfo == NULL)
			return;

		m_pstConfigInfo = pstConfigInfo;
	};


	void SetUpdateData		();
	void GetUpdateData		();
};

#endif // Wnd_Cfg_FPN_h__
