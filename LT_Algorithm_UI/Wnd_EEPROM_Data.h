#ifndef Wnd_EEPROM_Data_h__
#define Wnd_EEPROM_Data_h__

#pragma once

#include "afxwin.h"

#include "VGStatic.h"
#include "Def_T_EEPROM_Verify.h"

// CWnd_EEPROM_Data
typedef enum
{
	enData_Header = 0,
	enEEData_MAX=4,
}enumHeaderEEData;

typedef enum
{
	enEEData_1st = 0,
	enEEData_inputmax,
}enumDataEEData;

static	LPCTSTR	g_szEEData[] =
{
	_T("OC X"),
	_T("OC Y"),
	_T("Check Sum"),
	_T("Check Sum 2"),
	NULL
};
class CWnd_EEPROM_Data : public CWnd
{
	DECLARE_DYNAMIC(CWnd_EEPROM_Data)

public:
	CWnd_EEPROM_Data();
	virtual ~CWnd_EEPROM_Data();

	UINT m_nHeaderCnt = enEEData_MAX;
	CString m_SzHeaderName[enEEData_MAX];

	//---Class 정의 시 세팅 해야함.
	void SetHeader(UINT nHeaderCnt){

		if (nHeaderCnt > enEEData_MAX)
		{
			nHeaderCnt = enEEData_MAX;
		}
		m_nHeaderCnt = nHeaderCnt;
	};

	void SetHeaderName(UINT nCnt, CString szHeader){

		if (nCnt > enEEData_MAX)
		{
			nCnt = enEEData_MAX-1;
		}
		m_SzHeaderName[nCnt] = szHeader;
	};

	CVGStatic		m_st_Header[enEEData_MAX];
	CVGStatic		m_st_Data[enEEData_inputmax][enEEData_MAX];

//	CVGStatic		m_st_ResultView;


protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	void DataSetting(UINT nDataIdx, UINT nIdx, BOOL MODE, double dData);
	void DataEachReset(UINT nDataIdx, UINT nIdx);
	void DataReset();
	void DataDisplay(ST_EEPROM_Verify_Data stData);
};


#endif // Wnd_EEPROM_Data_h__
