﻿// Wnd_Cfg_Shading.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_Cfg_Shading.h"
#include "resource.h"

static LPCTSTR	g_szShading_Static[] =
{
	_T("PARAMETER"),
	_T("SPEC"),
	_T("Ver CT"),
	_T("Ver CB"),
	_T("Hov LC"),
	_T("Hov RC"),
	_T("Dig LT"),
	_T("Dig LB"),
	_T("Dig RT"),
	_T("Dig RB"),
	NULL
};

static LPCTSTR	g_szShading_Button[] =
{
	_T("RESET"),
	_T("TEST"),
	NULL
};

// CWnd_Cfg_Shading
typedef enum ShadingOptID
{
	IDC_BTN_ITEM  = 1001,
	IDC_CMB_ITEM  = 2001,
	IDC_EDT_ITEM  = 3001,
	IDC_LIST_ITEM = 4001,

	IDC_ED_SPEC_MIN  = 5001,
	IDC_ED_SPEC_MAX  = 6001,
	IDC_CHK_SPEC_MIN = 7001,
	IDC_CHK_SPEC_MAX = 8001,

	IDC_INDEX_ITEM	 = 9001,
};

IMPLEMENT_DYNAMIC(CWnd_Cfg_Shading, CWnd_Cfg_VIBase)

CWnd_Cfg_Shading::CWnd_Cfg_Shading()
{
	m_pstConfigInfo = NULL;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_Shading::~CWnd_Cfg_Shading()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_Shading, CWnd_Cfg_VIBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM,		IDC_BTN_ITEM	+ 999,					OnRangeBtnCtrl	)
	ON_COMMAND_RANGE(IDC_INDEX_ITEM,	IDC_INDEX_ITEM	+ ROI_Shading_Max,		OnRangeIndexCtrl)
END_MESSAGE_MAP()

// CWnd_Cfg_Shading 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_Cfg_Shading::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_Cfg_VIBase::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();
	
	for (UINT nIdx = 0; nIdx < STI_SHA_MAX; nIdx++)
	{
		m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Item[nIdx].SetFont_Gdip(L"Arial", 8.5F);
		m_st_Item[nIdx].Create(g_szShading_Static[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	m_st_Item[STI_SHA_PARAM].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Item[STI_SHA_SPEC].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Item[STI_SHA_Ver_CT].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Item[STI_SHA_Ver_CB].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Item[STI_SHA_Hov_LC].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Item[STI_SHA_Hov_RC].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Item[STI_SHA_Dig_LT].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Item[STI_SHA_Dig_LB].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Item[STI_SHA_Dig_RT].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Item[STI_SHA_Dig_RB].SetColorStyle(CVGStatic::ColorStyle_DarkGray);

	for (UINT nIdx = 0; nIdx < BTN_SHA_MAX; nIdx++)
	{
		m_bn_Item[nIdx].Create(g_szShading_Button[nIdx], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_ITEM + nIdx);
		m_bn_Item[nIdx].SetFont(&m_font);
	}

	for (UINT nIdx = 0; nIdx < EDT_SHA_MAX; nIdx++)
	{
		m_ed_Item[nIdx].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EDT_ITEM + nIdx);
		m_ed_Item[nIdx].SetWindowText(_T("0"));
		m_ed_Item[nIdx].SetValidChars(_T("0123456789.-"));
	}

	for (UINT nIdx = 0; nIdx < CMB_SHA_MAX; nIdx++)
	{
		m_cb_Item[nIdx].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CMB_ITEM + nIdx);
	}

	for (UINT nIdx = 0; nIdx < ROI_Shading_Max; nIdx++)
	{
		CString szCnt;
		szCnt.Format(_T("%d"), nIdx);

		m_st_Idex[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Idex[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Idex[nIdx].SetFont_Gdip(L"Arial", 8.0F);
		m_st_Idex[nIdx].Create(szCnt, WS_VISIBLE | WS_CHILD | SS_CENTER | SS_NOTIFY | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_INDEX_ITEM + nIdx);
	}

	for (UINT nIdx = 0; nIdx < IDX_Shading_Max; nIdx++)
	{
		m_List[nIdx].Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM + nIdx);
	}

	m_st_CapItem.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_CapItem.SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_CapItem.SetFont_Gdip(L"Arial", 9.0F);
	m_st_CapItem.Create(_T("Item"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_CapSpecMin.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_CapSpecMin.SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_CapSpecMin.SetFont_Gdip(L"Arial", 9.0F);
	m_st_CapSpecMin.Create(_T("Spec Min"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_CapSpecMax.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_CapSpecMax.SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_CapSpecMax.SetFont_Gdip(L"Arial", 9.0F);
	m_st_CapSpecMax.Create(_T("Spec Max"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);


	for (UINT nIdx = 0; nIdx < Spec_Shading_MAX; nIdx++)
	{
		m_st_Spec[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Spec[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Spec[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Spec[nIdx].Create(g_szShading_Spec[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

		m_ed_SpecMin[nIdx].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_SPEC_MIN + nIdx);
		m_ed_SpecMin[nIdx].SetWindowText(_T("0"));
		m_ed_SpecMin[nIdx].SetValidChars(_T("0123456789.-"));

		m_ed_SpecMax[nIdx].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_SPEC_MAX + nIdx);
		m_ed_SpecMax[nIdx].SetWindowText(_T("0"));
		m_ed_SpecMax[nIdx].SetValidChars(_T("0123456789.-"));

		m_ed_SpecMin[nIdx].SetFont(&m_font);
		m_ed_SpecMax[nIdx].SetFont(&m_font);

		m_chk_SpecMin[nIdx].Create(_T(""), WS_VISIBLE | WS_BORDER | BS_AUTOCHECKBOX, rectDummy, this, IDC_CHK_SPEC_MIN + nIdx);
		m_chk_SpecMax[nIdx].Create(_T(""), WS_VISIBLE | WS_BORDER | BS_AUTOCHECKBOX, rectDummy, this, IDC_CHK_SPEC_MAX + nIdx);

		m_chk_SpecMin[nIdx].SetMouseCursorHand();
		m_chk_SpecMin[nIdx].SetImage(IDB_UNCHECKED_16);
		m_chk_SpecMin[nIdx].SetCheckedImage(IDB_CHECKED_16);
		m_chk_SpecMin[nIdx].SizeToContent();

		m_chk_SpecMax[nIdx].SetMouseCursorHand();
		m_chk_SpecMax[nIdx].SetImage(IDB_UNCHECKED_16);
		m_chk_SpecMax[nIdx].SetCheckedImage(IDB_CHECKED_16);
		m_chk_SpecMax[nIdx].SizeToContent();

	}
	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_Cfg_Shading::OnSize(UINT nType, int cx, int cy)
{
	CWnd_Cfg_VIBase::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMargin  = 10;
	int iSpacing = 5;

	int iLeft	 = iMargin;
	int iTop	 = iMargin;
	int iWidth   = cx - iMargin - iMargin;
	int iHeight  = cy - iMargin - iMargin;
	iWidth = iWidth / 3 * 2; 

	int iSTWidth = iWidth / 5;
	int iSTHeight = 25;

	// 이름
	m_st_Item[STI_SHA_PARAM].MoveWindow(iLeft, iTop, iWidth, iSTHeight);

	// TEST 
// 	iTop += iSTHeight + iSpacing;
// 	iLeft = iMargin + iWidth - iSTWidth - iSTWidth - iSpacing;
// 	m_bn_Item[BTN_SHA_RESET].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);
// 
// 	iLeft = iMargin + iWidth - iSTWidth;
// 	m_bn_Item[BTN_SHA_TEST].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	int iIdexCnt	= 0;
	int iWidthCnt	= 19;
	int iHeightCnt	= 19;
	
	int iIdxWidth   = (iWidth - iWidthCnt) / iWidthCnt;
	int iIdxHeight  = (iHeight / 2 - iHeightCnt - 1) / iHeightCnt;

	int iTempCnt	= (iWidthCnt - 1) / 2;

	int iMarginTemp = 18;
	iLeft = iMarginTemp;
 	iTop += iSTHeight + iSpacing;

	for (UINT nIdx = 0; nIdx < (UINT)(iHeightCnt - 1) / 2; nIdx++)
	{
		m_st_Idex[iIdexCnt++].MoveWindow(iLeft, iTop, iIdxWidth, iIdxHeight);

		iLeft += iIdxWidth * iTempCnt + iTempCnt;
		m_st_Idex[iIdexCnt++].MoveWindow(iLeft, iTop, iIdxWidth, iIdxHeight);

		iLeft += iIdxWidth * iTempCnt + iTempCnt;
		m_st_Idex[iIdexCnt++].MoveWindow(iLeft, iTop, iIdxWidth, iIdxHeight);

		iTempCnt -= 1;
		iLeft = iMarginTemp + iIdxWidth * (nIdx + 1) + (nIdx + 1);
		iTop += iIdxHeight + 1;
	}

	iLeft = iMarginTemp;
	for (UINT nIdx = 0; nIdx < (UINT)iWidthCnt; nIdx++)
	{
		m_st_Idex[iIdexCnt++].MoveWindow(iLeft, iTop, iIdxWidth, iIdxHeight);
		iLeft = iMarginTemp + iIdxWidth * (nIdx + 1) + (nIdx + 1);
	}

	int iLeftTemp = iMarginTemp + iIdxWidth * (7 + 1) + (7 + 1);

	iTop += iIdxHeight + 1;
	iLeft = iLeftTemp;
	iTempCnt = 1;

	for (UINT nIdx = 0; nIdx < (UINT)(iHeightCnt - 1) / 2; nIdx++)
	{
		m_st_Idex[iIdexCnt++].MoveWindow(iLeft, iTop, iIdxWidth, iIdxHeight);

		iLeft += iIdxWidth * iTempCnt + iTempCnt;
		m_st_Idex[iIdexCnt++].MoveWindow(iLeft, iTop, iIdxWidth, iIdxHeight);

		iLeft += iIdxWidth * iTempCnt + iTempCnt;
		m_st_Idex[iIdexCnt++].MoveWindow(iLeft, iTop, iIdxWidth, iIdxHeight);

		iTempCnt += 1;
		iLeft = iLeftTemp - iIdxWidth * (nIdx + 1) - (nIdx + 1);
		iTop += iIdxHeight + 1;
	}

	iLeft = iMargin;
	iTop += iMargin;
	m_st_Item[STI_SHA_SPEC].MoveWindow(iLeft, iTop, iWidth, iSTHeight);

	// 스펙
	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;

	int iCtrlWidth = (iWidth - (iSpacing * 2)) / 3;	//170;
	int iLeft_2nd = iLeft + iCtrlWidth + iSpacing;
	int iLeft_3rd = iLeft_2nd + iCtrlWidth + iSpacing;
	int iEdWidth = iCtrlWidth - iSpacing - iSTHeight;

	m_st_CapItem.MoveWindow(iLeft, iTop, iCtrlWidth, iSTHeight);
	m_st_CapSpecMin.MoveWindow(iLeft_2nd, iTop, iCtrlWidth, iSTHeight);
	m_st_CapSpecMax.MoveWindow(iLeft_3rd, iTop, iCtrlWidth, iSTHeight);

	for (UINT nIdx = 0; nIdx < Spec_Shading_MAX; nIdx++)
	{
		iTop += iSTHeight + iSpacing;

		m_st_Spec[nIdx].MoveWindow(iLeft, iTop, iCtrlWidth, iSTHeight);

		m_chk_SpecMin[nIdx].MoveWindow(iLeft_2nd, iTop, iSTHeight, iSTHeight);
		m_ed_SpecMin[nIdx].MoveWindow(iLeft_2nd + iSTHeight + iSpacing, iTop, iEdWidth, iSTHeight);

		m_chk_SpecMax[nIdx].MoveWindow(iLeft_3rd, iTop, iSTHeight, iSTHeight);
		m_ed_SpecMax[nIdx].MoveWindow(iLeft_3rd + iSTHeight + iSpacing, iTop, iEdWidth, iSTHeight);
	}

	iLeft = iWidth + iMargin + iMargin;

	int iList_H  = 170;
	int iList_W  = (cx - iLeft - iMargin - iMargin) / 2;
	int iLeftSub = iLeft + iList_W + iMargin;
	iTop = iMargin;

	for (UINT nIdx = 0; nIdx < IDX_Shading_Max; nIdx += 2)
	{
		if (nIdx >= 2)
		{
			iSTHeight = 16;
		}

		m_st_Item[STI_SHA_Ver_CT + nIdx ].MoveWindow(iLeft, iTop, iList_W, iSTHeight);
		m_st_Item[STI_SHA_Ver_CT + nIdx + 1].MoveWindow(iLeftSub, iTop, iList_W, iSTHeight);
		iTop += iSTHeight + 1;

		m_List[nIdx].MoveWindow(iLeft, iTop, iList_W, iList_H);
		m_List[nIdx + 1].MoveWindow(iLeftSub, iTop, iList_W, iList_H);
		iTop += iList_H + 4;
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_Shading::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_Cfg_VIBase::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_Cfg_Shading::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd_Cfg_VIBase::OnShowWindow(bShow, nStatus);

	if (NULL != m_pstConfigInfo)
	{
		if (TRUE == bShow)
		{
			GetOwner()->SendMessage(m_wm_ShowWindow, 0, Ovr_Shading);
			GetOwner()->SendNotifyMessage(m_wm_ChangeOption, 0, 0);
		}
	}
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_Cfg_Shading::OnRangeBtnCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_BTN_ITEM;

	switch (nIdex)
	{
	case BTN_SHA_RESET:
		GetOwner()->SendMessage(m_wm_ShowWindow, 0, -1);
		break;
	case BTN_SHA_TEST:
		GetOwner()->SendMessage(m_wm_ShowWindow, 0, VTID_Rtllumination);
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: OnRangeIndexCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_Cfg_Shading::OnRangeIndexCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_INDEX_ITEM;

	GetOwner()->SendNotifyMessage(m_wm_ChangeOption, 0, 0);


	if (ROI_Shading_CNT == nIdex)
	{
		m_st_Idex[nIdex].SetColorStyle(CVGStatic::ColorStyle_Yellow);
		m_pstConfigInfo->bUseIndex[nIdex]	= TRUE;
		m_pstConfigInfo->nType[nIdex]		= Type_Shading_Standard;
		return;
	}

	switch (m_pstConfigInfo->nType[nIdex])
	{
	case Type_Shading_NotUes:
		m_st_Idex[nIdex].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_pstConfigInfo->bUseIndex[nIdex] = TRUE;
		m_pstConfigInfo->nType[nIdex] = Type_Shading_Test;
		break;
	case Type_Shading_Test:
		m_st_Idex[nIdex].SetColorStyle(CVGStatic::ColorStyle_Yellow);
		m_pstConfigInfo->bUseIndex[nIdex] = TRUE;
		m_pstConfigInfo->nType[nIdex] = Type_Shading_Standard;
		break;
	case Type_Shading_Standard:
		m_st_Idex[nIdex].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_pstConfigInfo->bUseIndex[nIdex] = FALSE;
		m_pstConfigInfo->nType[nIdex] = Type_Shading_NotUes;
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: SetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/12 - 17:32
// Desc.		:
//=============================================================================
void CWnd_Cfg_Shading::SetUpdateData()
{
	if (m_pstConfigInfo == NULL)
		return;

	CString strValue;

	for (UINT nIdx = 0; nIdx < IDX_Shading_Max; nIdx++)
	{
		m_List[nIdx].SetPtr_ConfigInfo(m_pstConfigInfo);
		m_List[nIdx].SetItemType((enIDX_Shading)nIdx);
		m_List[nIdx].InsertFullData();
	}

	for (UINT nIdx = 0; nIdx < ROI_Shading_Max; nIdx++)
	{
		switch (m_pstConfigInfo->nType[nIdx])
		{
		case Type_Shading_NotUes:
			m_st_Idex[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
			break;
		case Type_Shading_Test:
			m_st_Idex[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
			break;
		case Type_Shading_Standard:
			m_st_Idex[nIdx].SetColorStyle(CVGStatic::ColorStyle_Yellow);
			break;
		default:
			break;
		}
	}

	for (UINT nSpec = 0; nSpec < Spec_Shading_MAX; nSpec++)
	{
		strValue.Format(_T("%.2f"), m_pstConfigInfo->stSpec_Min[ROI_Shading_CNT].dbValue);
		m_ed_SpecMin[nSpec].SetWindowText(strValue);

		strValue.Format(_T("%.2f"), m_pstConfigInfo->stSpec_Max[ROI_Shading_CNT].dbValue);
		m_ed_SpecMax[nSpec].SetWindowText(strValue);

		m_chk_SpecMin[nSpec].SetCheck(m_pstConfigInfo->stSpec_Min[ROI_Shading_CNT].bEnable);
		m_chk_SpecMax[nSpec].SetCheck(m_pstConfigInfo->stSpec_Max[ROI_Shading_CNT].bEnable);
	}
}

//=============================================================================
// Method		: GetUpdateData
// Access		: public  
// Returns		: void
// Parameter	: __out ST_TestItemOpt & stTestItemOpt
// Qualifier	:
// Last Update	: 2017/10/14 - 18:07
// Desc.		:
//=============================================================================
void CWnd_Cfg_Shading::GetUpdateData()
{
	if (m_pstConfigInfo == NULL)
		return;

	CString strValue;

	for (UINT nSpec = 0; nSpec < Spec_Shading_MAX; nSpec++)
	{
		m_ed_SpecMin[nSpec].GetWindowText(strValue);
		m_pstConfigInfo->stSpec_Min[ROI_Shading_CNT].dbValue = _ttof(strValue);

		m_ed_SpecMax[nSpec].GetWindowText(strValue);
		m_pstConfigInfo->stSpec_Max[ROI_Shading_CNT].dbValue = _ttof(strValue);

		m_pstConfigInfo->stSpec_Min[ROI_Shading_CNT].bEnable = m_chk_SpecMin[nSpec].GetCheck();
		m_pstConfigInfo->stSpec_Max[ROI_Shading_CNT].bEnable = m_chk_SpecMax[nSpec].GetCheck();
	}
}
