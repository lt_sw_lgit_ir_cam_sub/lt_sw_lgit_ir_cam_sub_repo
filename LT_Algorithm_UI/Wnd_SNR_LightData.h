#ifndef Wnd_SNR_LightData_h__
#define Wnd_SNR_LightData_h__

#pragma once

#include "afxwin.h"

#include "VGStatic.h"
#include "Def_T_SNR_Light.h"

// CWnd_SNR_LightData
typedef enum
{
	enSNRL_Header = 0,
	enSNRL_MAX=7,
}enumHeaderSNRL;

typedef enum
{
	enSNRL_1st = 0,
	enSNRL_inputmax=8,
}enumDataSNRL;

class CWnd_SNR_LightData : public CWnd
{
	DECLARE_DYNAMIC(CWnd_SNR_LightData)

public:
	CWnd_SNR_LightData();
	virtual ~CWnd_SNR_LightData();

	UINT m_nHeaderCnt = enSNRL_MAX;
	CString m_SzHeaderName[enSNRL_MAX];

	//---Class 정의 시 세팅 해야함.
	void SetHeader(UINT nHeaderCnt){

		if (nHeaderCnt > enSNRL_MAX)
		{
			nHeaderCnt = enSNRL_MAX;
		}
		m_nHeaderCnt = nHeaderCnt;
	};

	void SetHeaderName(UINT nCnt, CString szHeader){

		if (nCnt > enSNRL_MAX)
		{
			nCnt = enSNRL_MAX-1;
		}
		m_SzHeaderName[nCnt] = szHeader;
	};

	CVGStatic		m_st_Header[enSNRL_MAX][enSNRL_inputmax];
	CVGStatic		m_st_Data[enSNRL_MAX][enSNRL_inputmax];

//	CVGStatic		m_st_ResultView;


protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	void DataSetting(UINT nDataIdx, UINT nIdx, BOOL MODE, double dData);
	void DataEachReset(UINT nDataIdx, UINT nIdx);
	void DataReset();
	void DataDisplay(ST_SNR_Light_Data stData);
};


#endif // Wnd_SNR_LightData_h__
