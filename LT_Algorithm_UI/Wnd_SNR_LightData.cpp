// Wnd_SNR_LightData.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_SNR_LightData.h"

// CWnd_SNR_LightData

IMPLEMENT_DYNAMIC(CWnd_SNR_LightData, CWnd)

CWnd_SNR_LightData::CWnd_SNR_LightData()
{

}

CWnd_SNR_LightData::~CWnd_SNR_LightData()
{
}

BEGIN_MESSAGE_MAP(CWnd_SNR_LightData, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

// CWnd_SNR_LightData 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2018/3/3 - 10:27
// Desc.		:
//=============================================================================
int CWnd_SNR_LightData::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	DWORD dwStyle = WS_VISIBLE | WS_CHILD /*| WS_CLIPCHILDREN*/ | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	int nCnt = 0;
	CString str;
	for (int t = 0; t < (int)enSNRL_MAX; t++)
 	{

 		for (int i= 0;i<enSNRL_inputmax; i++)
 		{
			str.Format(_T("%d"), nCnt);
			m_st_Header[t][i].SetStaticStyle(CVGStatic::StaticStyle_Title);
			m_st_Header[t][i].SetColorStyle(CVGStatic::ColorStyle_Default);
			m_st_Header[t][i].SetFont_Gdip(L"Arial", 10.0F);
			m_st_Header[t][i].Create(str, dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

			m_st_Data[t][i].SetStaticStyle(CVGStatic::StaticStyle_Title);
			m_st_Data[t][i].SetColorStyle(CVGStatic::ColorStyle_Default);
			m_st_Data[t][i].SetFont_Gdip(L"Arial", 10.0F);
			m_st_Data[t][i].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
			nCnt++;
 		}
 	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2018/3/3 - 10:27
// Desc.		:
//=============================================================================
void CWnd_SNR_LightData::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iMargin = 0;
	int iSpacing = 0;
	int iCateSpacing = 0;

	int iLeft = iMargin;
	int iTop = iMargin;
	int iWidth = cx - iMargin - iMargin;
	int iHeight = cy - iMargin - iMargin;

	int HeaderW = (iWidth) / 14;

	int stW = iWidth;
	int stH = (672 * iWidth) / 1280;

	int HeaderH = iHeight / (8);

	iLeft = iMargin;
	iTop = iMargin;
	for (int t = 0; t < (int)enSNRL_MAX; t++)
	{
		iTop = iMargin;
		for (int i = 0; i < enSNRL_inputmax; i++)
		{
			m_st_Header[t][i].MoveWindow(iLeft, iTop, HeaderW, HeaderH);
			iLeft += HeaderW;
			m_st_Data[t][i].MoveWindow(iLeft, iTop, HeaderW, HeaderH);
			iTop += (HeaderH);
			iLeft -= HeaderW;
		}

		iLeft += HeaderW;
		iLeft += HeaderW;
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2018/3/3 - 10:27
// Desc.		:
//=============================================================================
BOOL CWnd_SNR_LightData::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnEraseBkgnd
// Access		: public  
// Returns		: BOOL
// Parameter	: CDC * pDC
// Qualifier	:
// Last Update	: 2018/3/3 - 10:27
// Desc.		:
//=============================================================================
BOOL CWnd_SNR_LightData::OnEraseBkgnd(CDC* pDC)
{
	return CWnd::OnEraseBkgnd(pDC);
}

//=============================================================================
// Method		: DataSetting
// Access		: public  
// Returns		: void
// Parameter	: UINT nDataIdx
// Parameter	: UINT nIdx
// Parameter	: BOOL MODE
// Parameter	: double dData
// Qualifier	:
// Last Update	: 2018/3/3 - 10:27
// Desc.		:
//=============================================================================
void CWnd_SNR_LightData::DataSetting(UINT nDataIdx, UINT nIdx, BOOL MODE, double dData)
{

	if (MODE == TRUE)
	{
		m_st_Data[nDataIdx][nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
	}
	else
	{
		m_st_Data[nDataIdx][nIdx].SetColorStyle(CVGStatic::ColorStyle_Red);
	}

	CString strData;

	strData.Format(_T("%6.3f"), dData);

	m_st_Data[nDataIdx][nIdx].SetWindowText(strData);
}

//=============================================================================
// Method		: DataEachReset
// Access		: public  
// Returns		: void
// Parameter	: UINT nDataIdx
// Parameter	: UINT nIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 10:27
// Desc.		:
//=============================================================================
void CWnd_SNR_LightData::DataEachReset(UINT nDataIdx, UINT nIdx)
{
	m_st_Data[nDataIdx][nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_Data[nDataIdx][nIdx].SetWindowText(_T(""));
}

//=============================================================================
// Method		: DataReset
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/3 - 10:27
// Desc.		:
//=============================================================================
void CWnd_SNR_LightData::DataReset()
{
	for (int t = 0; t < (int)enSNRL_MAX; t++)
	{
		for (int nIdx = 0; nIdx < enSNRL_inputmax; nIdx++)
		{
			m_st_Data[t][nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
			m_st_Data[t][nIdx].SetWindowText(_T(""));
		}
	}
}

//=============================================================================
// Method		: DataDisplay
// Access		: public  
// Returns		: void
// Parameter	: ST_SNR_Light_Data stData
// Qualifier	:
// Last Update	: 2018/3/3 - 10:26
// Desc.		:
//=============================================================================
void CWnd_SNR_LightData::DataDisplay(ST_SNR_Light_Data stData)
{
	CString strData;
	int nCnt = 0;
	for (int t = 0; t < (int)enSNRL_MAX; t++)
	{
		for (int nIdx = 0; nIdx < enSNRL_inputmax; nIdx++)
		{
			if (stData.bUse[nCnt])
			{
				if (TRUE == stData.nResult && stData.nFinalIndx == (t * enSNRL_inputmax) + nIdx)
				{
					m_st_Data[t][nIdx].SetColorStyle(CVGStatic::ColorStyle_Yellow);
				}

				if (FALSE == stData.nResult && stData.nFinalIndx == (t * enSNRL_inputmax) + nIdx)
				{
					m_st_Data[t][nIdx].SetColorStyle(CVGStatic::ColorStyle_Red);
				}

				strData.Format(_T("%6.2f"), stData.dbEtcValue[nCnt]);
				m_st_Data[t][nIdx].SetWindowText(strData);
			}
			else{
				m_st_Data[t][nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);

				m_st_Data[t][nIdx].SetWindowText(_T("X"));
			}
			nCnt++;

		}
	}
}
