﻿#ifndef Wnd_Cfg_Particle_h__
#define Wnd_Cfg_Particle_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_T_Particle.h"
#include "Def_TestItem_VI.h"
#include "List_ParticleOp.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_Particle
//-----------------------------------------------------------------------------
class CWnd_Cfg_Particle : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_Particle)

public:
	CWnd_Cfg_Particle();
	virtual ~CWnd_Cfg_Particle();

	enum enParticleStatic
	{
		STI_PAR_PARAM = 0,
		STI_PAR_SPEC,
		STI_PAR_SENSITIVITY,
		STI_PAR_EDGE_W,
		STI_PAR_EDGE_H,
		STI_PAR_MAX,
	};

	enum enParticleButton
	{
		BTN_PAR_RESET = 0,
		BTN_PAR_TEST,
		BTN_PAR_MAX,
	};

	enum enParticleComobox
	{
		CMB_PAR_MAX = 1,
	};

	enum enParticleEdit
	{
		EDT_PAR_SENSITIVITY = 0,
		EDT_PAR_EDGE_W,
		EDT_PAR_EDGE_H,
		EDT_PAR_MAX,
	};

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow			(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl			(UINT nID);
	afx_msg void	OnRangeIndexCtrl		(UINT nID);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);

	CList_ParticleOp	m_List;

	CFont				m_font;
	
	CVGStatic			m_st_Item[STI_PAR_MAX];
	CButton				m_bn_Item[BTN_PAR_MAX];
	CComboBox			m_cb_Item[CMB_PAR_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_PAR_MAX];

	CVGStatic	  		m_st_CapItem;
	CVGStatic	  		m_st_CapSpecMin;
	CVGStatic	  		m_st_CapSpecMax;

	CVGStatic	  		m_st_Spec[Spec_Particle_MAX];
	CMFCMaskedEdit		m_ed_SpecMin[Spec_Particle_MAX];
	CMFCMaskedEdit		m_ed_SpecMax[Spec_Particle_MAX];
	CMFCButton	  		m_chk_SpecMin[Spec_Particle_MAX];
	CMFCButton	  		m_chk_SpecMax[Spec_Particle_MAX];

	ST_Particle_Opt*		m_pstConfigInfo = NULL;

public:

	void	SetPtr_RecipeInfo	(ST_Particle_Opt* pstConfigInfo)
	{
		if (pstConfigInfo == NULL)
			return;

		m_pstConfigInfo = pstConfigInfo;
	};


	void SetUpdateData		();
	void GetUpdateData		();
};

#endif // Wnd_Cfg_Particle_h__
