﻿#ifndef Wnd_Cfg_SNR_Light_h__
#define Wnd_Cfg_SNR_Light_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_T_SNR_Light.h"
#include "Def_TestItem_VI.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_SNR_Light
//-----------------------------------------------------------------------------
class CWnd_Cfg_SNR_Light : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_SNR_Light)

public:
	CWnd_Cfg_SNR_Light();
	virtual ~CWnd_Cfg_SNR_Light();

	enum enSNR_L_Static
	{
		STI_SNR_L_PARAM = 0,
		STI_SNR_L_SPEC,
		STI_SNR_L_MAX,
	};

	enum enSNR_L_Button
	{
		BTN_SNR_L_RESET = 0,
		BTN_SNR_L_TEST,
		BTN_SNR_L_MAX,
	};

	enum enSNR_L_Comobox
	{
		CMB_SNR_L_MAX = 1,
	};

	enum enSNR_L_Edit
	{
		EDT_SNR_L_MAX = 1,
	};


protected:
	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow			(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl			(UINT nID);
	afx_msg void	OnRangeIndexCtrl		(UINT nID);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);

	CFont						m_font;

	CVGStatic					m_st_Idex[ROI_SNR_Light_Max];

	CVGStatic					m_st_Item[STI_SNR_L_MAX];
	CButton						m_bn_Item[BTN_SNR_L_MAX];
	CComboBox					m_cb_Item[CMB_SNR_L_MAX];
	CMFCMaskedEdit				m_ed_Item[EDT_SNR_L_MAX];

	CVGStatic					m_st_CapItem;
	CVGStatic					m_st_CapSpecMin;
	CVGStatic					m_st_CapSpecMax;

	CVGStatic					m_st_Spec[Spec_SNR_Light_MAX];
	CMFCMaskedEdit				m_ed_SpecMin[Spec_SNR_Light_MAX];
	CMFCMaskedEdit				m_ed_SpecMax[Spec_SNR_Light_MAX];
	CMFCButton					m_chk_SpecMin[Spec_SNR_Light_MAX];
	CMFCButton					m_chk_SpecMax[Spec_SNR_Light_MAX];

	ST_SNR_Light_Opt*			m_pstConfigInfo = NULL;

public:

	void	SetPtr_RecipeInfo	(ST_SNR_Light_Opt* pstConfigInfo)
	{
		if (pstConfigInfo == NULL)
			return;

		m_pstConfigInfo = pstConfigInfo;
	};

	void SetUpdateData		();
	void GetUpdateData		();
};

#endif // Wnd_Cfg_SNR_Light_h__
