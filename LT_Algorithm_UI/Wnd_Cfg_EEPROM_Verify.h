﻿#ifndef Wnd_Cfg_EEPROM_Verify_h__
#define Wnd_Cfg_EEPROM_Verify_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_T_EEPROM_Verify.h"
#include "Def_TestItem_VI.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_EEPROM_Verify
//-----------------------------------------------------------------------------
class CWnd_Cfg_EEPROM_Verify : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_EEPROM_Verify)

public:
	CWnd_Cfg_EEPROM_Verify();
	virtual ~CWnd_Cfg_EEPROM_Verify();

	enum enEEPROM_VerifyStatic
	{
		STI_EV_PARAM = 0,
		STI_EV_SPEC,
		STI_EV_SlaveID,
		STI_EV_Address_OCX,
		STI_EV_Address_OCY,
		STI_EV_Length_OCX,
		STI_EV_Length_OCY,
		STI_EV_CheckSum,
		STI_EV_MAX,
	};

	enum enEEPROM_VerifyButton
	{
		BTN_EV_RESET = 0,
		BTN_EV_TEST,
		BTN_EV_MAX,
	};

	enum enEEPROM_VerifyComobox
	{
		CMB_EV_MAX = 1,
	};

	enum enEEPROM_VerifyEdit
	{
		EDT_EV_SlaveID = 0,
		EDT_EV_Address_OCX,
		EDT_EV_Address_OCY,
		EDT_EV_Length_OCX,
		EDT_EV_Length_OCY,
		EDT_EV_CheckSum,
		EDT_EV_MAX,
	};

protected:
	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	CFont						m_font;

	CVGStatic					m_st_Item[STI_EV_MAX];
	CButton						m_bn_Item[BTN_EV_MAX];
	CComboBox					m_cb_Item[CMB_EV_MAX];
	CMFCMaskedEdit				m_ed_Item[EDT_EV_MAX];

	CVGStatic					m_st_CapItem;
	CVGStatic					m_st_CapSpecMin;
	CVGStatic					m_st_CapSpecMax;

	CVGStatic					m_st_Spec[Spec_EEP_Max];
	CMFCMaskedEdit				m_ed_SpecMin[Spec_EEP_Max];
	CMFCMaskedEdit				m_ed_SpecMax[Spec_EEP_Max];
	CMFCButton					m_chk_SpecMin[Spec_EEP_Max];
	CMFCButton					m_chk_SpecMax[Spec_EEP_Max];

	ST_EEPROM_Verify_Opt*			m_pstConfigInfo = NULL;

public:

	void	SetPtr_RecipeInfo(__in ST_EEPROM_Verify_Opt* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstConfigInfo = pstRecipeInfo;
	};

	void	SetUpdateData		();
	void	GetUpdateData		();

	UINT Edit_GetValue(UINT nID, BOOL bHex = TRUE);
	void Edit_SetValue(UINT nID, DWORD wData, BOOL bHex = TRUE);
	//void	Get_TestItemInfo	(__in UINT nBegin, __in UINT nEnd, __out ST_TestItemInfo& stOutTestItemInfo);
};

#endif // Wnd_EEPROM_Verify_h__
