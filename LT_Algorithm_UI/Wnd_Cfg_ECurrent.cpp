﻿// Wnd_Cfg_ECurrent.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_Cfg_ECurrent.h"
#include "resource.h"

static LPCTSTR	g_szECurrent_Static[] =
{
	_T("PARAMETER"),
	_T("SPEC"),
	_T("OFFSET 1.8 V"),
	_T("OFFSET 2.8 V"),
	_T("OFFSET 1.8 V [R]"),
	_T("OFFSET 2.8 V [R]"),
	NULL
};
static LPCTSTR	g_szECurrent_StaticEntry[] =
{
	_T("PARAMETER"),
	_T("SPEC"),
	_T("OFFSET CH 1"),
	_T("OFFSET CH 2"),
	_T("OFFSET CH 3"),
	_T("OFFSET CH 4"),
	_T("OFFSET CH 5"),

	_T("OFFSET CH 1 [R]"),
	_T("OFFSET CH 2 [R]"),
	_T("OFFSET CH 3 [R]"),
	_T("OFFSET CH 4 [R]"),
	_T("OFFSET CH 5 [R]"),
	NULL
};
static LPCTSTR	g_szECurrent_Button[] =
{
	_T("RESET"),
	_T("TEST"),
	NULL
};

// CWnd_Cfg_ECurrent
typedef enum ECurrent_OptID
{
	IDC_BTN_ITEM	= 1001,
	IDC_CMB_ITEM	= 2001,
	IDC_EDT_ITEM	= 3001,
	IDC_LIST_ITEM	= 4001,

	IDC_ED_SPEC_MIN  = 5001,
	IDC_ED_SPEC_MAX  = 6001,
	IDC_CHK_SPEC_MIN = 7001,
	IDC_CHK_SPEC_MAX = 8001,
};

IMPLEMENT_DYNAMIC(CWnd_Cfg_ECurrent, CWnd_Cfg_VIBase)

CWnd_Cfg_ECurrent::CWnd_Cfg_ECurrent()
{
	m_pstConfigInfo = NULL;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_ECurrent::~CWnd_Cfg_ECurrent()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_ECurrent, CWnd_Cfg_VIBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
END_MESSAGE_MAP()

// CWnd_Cfg_ECurrent 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_Cfg_ECurrent::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_Cfg_VIBase::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < STI_ECUR_ENTRY_MAX; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdex].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	m_st_Item[STI_ECUR_PARAM].SetText(g_szECurrent_Static[STI_ECUR_PARAM]);
	m_st_Item[STI_ECUR_SPEC].SetText(g_szECurrent_Static[STI_ECUR_SPEC]);
	m_st_Item[STI_ECUR_PARAM].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Item[STI_ECUR_SPEC ].SetColorStyle(CVGStatic::ColorStyle_DarkGray);

	for (UINT nIdex = 0; nIdex < BTN_ECUR_MAX; nIdex++)
	{
		m_bn_Item[nIdex].Create(g_szECurrent_Button[nIdex], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_ITEM + nIdex);
		m_bn_Item[nIdex].SetFont(&m_font);
	}

	for (UINT nIdex = 0; nIdex < EDT_ECUR_ENTRY_MAX; nIdex++)
	{
		m_ed_Item[nIdex].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EDT_ITEM + nIdex);
		m_ed_Item[nIdex].SetWindowText(_T("0"));
		m_ed_Item[nIdex].SetValidChars(_T("0123456789.-"));
	}

	for (UINT nIdex = 0; nIdex < CMB_ECUR_MAX; nIdex++)
	{
		m_cb_Item[nIdex].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CMB_ITEM + nIdex);
	}

	m_st_CapItem.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_CapItem.SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_CapItem.SetFont_Gdip(L"Arial", 9.0F);
	m_st_CapItem.Create(_T("Item"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_CapSpecMin.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_CapSpecMin.SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_CapSpecMin.SetFont_Gdip(L"Arial", 9.0F);
	m_st_CapSpecMin.Create(_T("Spec Min"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_CapSpecMax.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_CapSpecMax.SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_CapSpecMax.SetFont_Gdip(L"Arial", 9.0F);
	m_st_CapSpecMax.Create(_T("Spec Max"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	for (UINT nIdx = 0; nIdx < Spec_ECurrent_Max; nIdx++)
	{
		m_st_Spec[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Spec[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Spec[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Spec[nIdx].Create(g_szECurrent_Spec[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

		m_ed_SpecMin[nIdx].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_SPEC_MIN + nIdx);
		m_ed_SpecMin[nIdx].SetWindowText(_T("0"));
		m_ed_SpecMin[nIdx].SetValidChars(_T("0123456789.-"));

		m_ed_SpecMax[nIdx].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_SPEC_MAX + nIdx);
		m_ed_SpecMax[nIdx].SetWindowText(_T("0"));
		m_ed_SpecMax[nIdx].SetValidChars(_T("0123456789.-"));

		m_ed_SpecMin[nIdx].SetFont(&m_font);
		m_ed_SpecMax[nIdx].SetFont(&m_font);

		m_chk_SpecMin[nIdx].Create(_T(""), WS_VISIBLE | WS_BORDER | BS_AUTOCHECKBOX, rectDummy, this, IDC_CHK_SPEC_MIN + nIdx);
		m_chk_SpecMax[nIdx].Create(_T(""), WS_VISIBLE | WS_BORDER | BS_AUTOCHECKBOX, rectDummy, this, IDC_CHK_SPEC_MAX + nIdx);

		m_chk_SpecMin[nIdx].SetMouseCursorHand();
		m_chk_SpecMin[nIdx].SetImage(IDB_UNCHECKED_16);
		m_chk_SpecMin[nIdx].SetCheckedImage(IDB_CHECKED_16);
		m_chk_SpecMin[nIdx].SizeToContent();

		m_chk_SpecMax[nIdx].SetMouseCursorHand();
		m_chk_SpecMax[nIdx].SetImage(IDB_UNCHECKED_16);
		m_chk_SpecMax[nIdx].SetCheckedImage(IDB_CHECKED_16);
		m_chk_SpecMax[nIdx].SizeToContent();
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_Cfg_ECurrent::OnSize(UINT nType, int cx, int cy)
{
	CWnd_Cfg_VIBase::OnSize(nType, cx, cy);

	int iIdxCnt  = 0;
	int iMargin  = 10;
	int iSpacing = 5;

	int iLeft	 = iMargin;
	int iTop	 = iMargin;
	int iWidth   = cx - iMargin - iMargin;
	int iHeight  = cy - iMargin - iMargin;
	
	int iHeaderH = 40;
	int iList_H = iHeaderH + iIdxCnt * 20;
	int iCtrl_W = iWidth / 5;
	int iCtrl_H = 25;

	if (iIdxCnt <= 0 || iList_H > iHeight)
	{
		iList_H = iHeight;
	}

	int iSTWidth	= iWidth / 5;
	int iSTHeight	= 25;
	int iTopTemp	= 0;
	m_st_Item[STI_ECUR_PARAM].MoveWindow(iLeft, iTop, iWidth, iSTHeight);

#if 0
	
	m_st_Item[STI_ECUR_OFFSET_18V_L].MoveWindow(0, 0, 0, 0);
	m_ed_Item[EDT_ECUR_OFFSET_18V_L].MoveWindow(0, 0, 0, 0);
	
	m_st_Item[STI_ECUR_OFFSET_28V_L].MoveWindow(0, 0, 0, 0);
	m_ed_Item[EDT_ECUR_OFFSET_28V_L].MoveWindow(0, 0, 0, 0);

	m_st_Item[STI_ECUR_OFFSET_18V_R].MoveWindow(0, 0, 0, 0);
	m_ed_Item[EDT_ECUR_OFFSET_18V_R].MoveWindow(0, 0, 0, 0);
	
	m_st_Item[STI_ECUR_OFFSET_28V_R].MoveWindow(0, 0, 0, 0);
	m_ed_Item[EDT_ECUR_OFFSET_28V_R].MoveWindow(0, 0, 0, 0);

	// ITEM
	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;
	m_st_Item[STI_ECUR_OFFSET_18V_L].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft += iSTWidth + 2;
	m_ed_Item[EDT_ECUR_OFFSET_18V_L].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);

	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;
	m_st_Item[STI_ECUR_OFFSET_28V_L].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iLeft += iSTWidth + 2;
	m_ed_Item[EDT_ECUR_OFFSET_28V_L].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);

	CString szText;

	if (1 == m_nModelType)
	{
		szText.Format(_T("%s [L]"), g_szECurrent_Static[STI_ECUR_OFFSET_18V_L]);
		m_st_Item[STI_ECUR_OFFSET_18V_L].SetText(szText);

		szText.Format(_T("%s [L]"), g_szECurrent_Static[STI_ECUR_OFFSET_28V_L]);
		m_st_Item[STI_ECUR_OFFSET_28V_L].SetText(szText);

		iLeft = iMargin;
		iTop += iSTHeight + iSpacing;
		m_st_Item[STI_ECUR_OFFSET_18V_R].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

		iLeft += iSTWidth + 2;
		m_ed_Item[EDT_ECUR_OFFSET_18V_R].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);

		iLeft = iMargin;
		iTop += iSTHeight + iSpacing;
		m_st_Item[STI_ECUR_OFFSET_28V_R].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

		iLeft += iSTWidth + 2;
		m_ed_Item[EDT_ECUR_OFFSET_28V_R].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);
	}
	else
	{
		szText.Format(_T("%s"), g_szECurrent_Static[STI_ECUR_OFFSET_18V_L]);
		m_st_Item[STI_ECUR_OFFSET_18V_L].SetText(szText);

		szText.Format(_T("%s"), g_szECurrent_Static[STI_ECUR_OFFSET_28V_L]);
		m_st_Item[STI_ECUR_OFFSET_28V_L].SetText(szText);
	}

	// 이름
	iLeft = iMargin;
	iTop += iSTHeight + iSpacing + iSpacing + iSpacing;
	m_st_Item[STI_ECUR_SPEC].MoveWindow(iLeft, iTop, iWidth, iSTHeight);

	// 스펙
	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;

	int iCtrlWidth	= (iWidth - (iSpacing * 2)) / 3;	//170;
	int iLeft_2nd	= iLeft + iCtrlWidth + iSpacing;
	int iLeft_3rd	= iLeft_2nd + iCtrlWidth + iSpacing;
	int iEdWidth	= iCtrlWidth - iSpacing - iSTHeight;
	
	m_st_CapItem.MoveWindow(iLeft, iTop, iCtrlWidth, iSTHeight);
	m_st_CapSpecMin.MoveWindow(iLeft_2nd, iTop, iCtrlWidth, iSTHeight);
	m_st_CapSpecMax.MoveWindow(iLeft_3rd, iTop, iCtrlWidth, iSTHeight);

	for (UINT nIdx = 0; nIdx < Spec_ECurrent_CH3; nIdx++)
	{
		iTop += iSTHeight + iSpacing;

		m_st_Spec[nIdx].MoveWindow(iLeft, iTop, iCtrlWidth, iSTHeight);

		m_chk_SpecMin[nIdx].MoveWindow(iLeft_2nd, iTop, iSTHeight, iSTHeight);
		m_ed_SpecMin[nIdx].MoveWindow(iLeft_2nd + iSTHeight + iSpacing, iTop, iEdWidth, iSTHeight);

		m_chk_SpecMax[nIdx].MoveWindow(iLeft_3rd, iTop, iSTHeight, iSTHeight);
		m_ed_SpecMax[nIdx].MoveWindow(iLeft_3rd + iSTHeight + iSpacing, iTop, iEdWidth, iSTHeight);
	}

#else

	Model_Change();
	//전체적으로 가리기
	for (int t = 0; t < Spec_ECurrent_Max; t++)
	{
		m_st_Item[STI_ECUR_OFFSET_CH1_L + t].MoveWindow(0, 0, 0, 0);
		m_ed_Item[EDT_ECUR_OFFSET_CH1_L + t].MoveWindow(0, 0, 0, 0);

		m_st_Item[STI_ECUR_OFFSET_CH1_R + t].MoveWindow(0, 0, 0, 0);
		m_ed_Item[EDT_ECUR_OFFSET_CH1_R + t].MoveWindow(0, 0, 0, 0);

		m_st_Spec[t].MoveWindow(0, 0, 0, 0);

		m_chk_SpecMin[t].MoveWindow(0, 0, 0, 0);
		m_ed_SpecMin[t].MoveWindow(0, 0, 0, 0);

		m_chk_SpecMax[t].MoveWindow(0, 0, 0, 0);
		m_ed_SpecMax[t].MoveWindow(0, 0, 0, 0);
	}
	if (m_nCameraModelType == ECUR_CM_2)
	{

		iLeft = iMargin;
		iTop += iSTHeight + iSpacing;
		m_st_Item[STI_ECUR_OFFSET_18V_L].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

		iLeft += iSTWidth + 2;
		m_ed_Item[EDT_ECUR_OFFSET_18V_L].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);

		iLeft = iMargin;
		iTop += iSTHeight + iSpacing;
		m_st_Item[STI_ECUR_OFFSET_28V_L].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

		iLeft += iSTWidth + 2;
		m_ed_Item[EDT_ECUR_OFFSET_28V_L].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);

		CString szText;

		if (1 == m_nModelType)
		{
			szText.Format(_T("%s [L]"), g_szECurrent_Static[STI_ECUR_OFFSET_18V_L]);
			m_st_Item[STI_ECUR_OFFSET_18V_L].SetText(szText);

			szText.Format(_T("%s [L]"), g_szECurrent_Static[STI_ECUR_OFFSET_28V_L]);
			m_st_Item[STI_ECUR_OFFSET_28V_L].SetText(szText);

			iLeft = iMargin;
			iTop += iSTHeight + iSpacing;
			m_st_Item[STI_ECUR_OFFSET_18V_R].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

			iLeft += iSTWidth + 2;
			m_ed_Item[EDT_ECUR_OFFSET_18V_R].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);

			iLeft = iMargin;
			iTop += iSTHeight + iSpacing;
			m_st_Item[STI_ECUR_OFFSET_28V_R].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

			iLeft += iSTWidth + 2;
			m_ed_Item[EDT_ECUR_OFFSET_28V_R].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);
		}
		else
		{
			szText.Format(_T("%s"), g_szECurrent_Static[STI_ECUR_OFFSET_18V_L]);
			m_st_Item[STI_ECUR_OFFSET_18V_L].SetText(szText);

			szText.Format(_T("%s"), g_szECurrent_Static[STI_ECUR_OFFSET_28V_L]);
			m_st_Item[STI_ECUR_OFFSET_28V_L].SetText(szText);
		}

		// 이름
		iLeft = iMargin;
		iTop += iSTHeight + iSpacing + iSpacing + iSpacing;
		m_st_Item[STI_ECUR_SPEC].MoveWindow(iLeft, iTop, iWidth, iSTHeight);

		// 스펙
		iLeft = iMargin;
		iTop += iSTHeight + iSpacing;

		int iCtrlWidth = (iWidth - (iSpacing * 2)) / 3;	//170;
		int iLeft_2nd = iLeft + iCtrlWidth + iSpacing;
		int iLeft_3rd = iLeft_2nd + iCtrlWidth + iSpacing;
		int iEdWidth = iCtrlWidth - iSpacing - iSTHeight;

		m_st_CapItem.MoveWindow(iLeft, iTop, iCtrlWidth, iSTHeight);
		m_st_CapSpecMin.MoveWindow(iLeft_2nd, iTop, iCtrlWidth, iSTHeight);
		m_st_CapSpecMax.MoveWindow(iLeft_3rd, iTop, iCtrlWidth, iSTHeight);

		for (UINT nIdx = 0; nIdx < Spec_ECurrent_CH3; nIdx++)
		{
			iTop += iSTHeight + iSpacing;

			m_st_Spec[nIdx].MoveWindow(iLeft, iTop, iCtrlWidth, iSTHeight);

			m_chk_SpecMin[nIdx].MoveWindow(iLeft_2nd, iTop, iSTHeight, iSTHeight);
			m_ed_SpecMin[nIdx].MoveWindow(iLeft_2nd + iSTHeight + iSpacing, iTop, iEdWidth, iSTHeight);

			m_chk_SpecMax[nIdx].MoveWindow(iLeft_3rd, iTop, iSTHeight, iSTHeight);
			m_ed_SpecMax[nIdx].MoveWindow(iLeft_3rd + iSTHeight + iSpacing, iTop, iEdWidth, iSTHeight);
		}

	}
	else{

		for (int t = 0; t < Spec_ECurrent_Max; t++)
		{
			iLeft = iMargin;
			iTop += iSTHeight + iSpacing;
			m_st_Item[STI_ECUR_OFFSET_CH1_L+t].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

			iLeft += iSTWidth + 2;
			m_ed_Item[EDT_ECUR_OFFSET_CH1_L + t].MoveWindow(iLeft, iTop, iSTWidth * 2, iSTHeight);
		}

		// 이름
		iLeft = iMargin;
		iTop += iSTHeight + iSpacing + iSpacing + iSpacing;
		m_st_Item[STI_ECUR_SPEC].MoveWindow(iLeft, iTop, iWidth, iSTHeight);

		// 스펙
		iLeft = iMargin;
		iTop += iSTHeight + iSpacing;

		int iCtrlWidth = (iWidth - (iSpacing * 2)) / 3;	//170;
		int iLeft_2nd = iLeft + iCtrlWidth + iSpacing;
		int iLeft_3rd = iLeft_2nd + iCtrlWidth + iSpacing;
		int iEdWidth = iCtrlWidth - iSpacing - iSTHeight;

		m_st_CapItem.MoveWindow(iLeft, iTop, iCtrlWidth, iSTHeight);
		m_st_CapSpecMin.MoveWindow(iLeft_2nd, iTop, iCtrlWidth, iSTHeight);
		m_st_CapSpecMax.MoveWindow(iLeft_3rd, iTop, iCtrlWidth, iSTHeight);

		for (UINT nIdx = 0; nIdx < Spec_ECurrent_Max; nIdx++)
		{
			iTop += iSTHeight + iSpacing;

			m_st_Spec[nIdx].MoveWindow(iLeft, iTop, iCtrlWidth, iSTHeight);

			m_chk_SpecMin[nIdx].MoveWindow(iLeft_2nd, iTop, iSTHeight, iSTHeight);
			m_ed_SpecMin[nIdx].MoveWindow(iLeft_2nd + iSTHeight + iSpacing, iTop, iEdWidth, iSTHeight);

			m_chk_SpecMax[nIdx].MoveWindow(iLeft_3rd, iTop, iSTHeight, iSTHeight);
			m_ed_SpecMax[nIdx].MoveWindow(iLeft_3rd + iSTHeight + iSpacing, iTop, iEdWidth, iSTHeight);
		}

	}


#endif
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_ECurrent::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_Cfg_VIBase::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_Cfg_ECurrent::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd_Cfg_VIBase::OnShowWindow(bShow, nStatus);

	if (NULL != m_pstConfigInfo)
	{
		if (TRUE == bShow)
		{
			GetOwner()->SendMessage(m_wm_ShowWindow, 0, Ovr_ECurrent);
		}
	}
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_Cfg_ECurrent::OnRangeBtnCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_BTN_ITEM;

	switch (nIdex)
	{
	case BTN_ECUR_RESET:
		GetOwner()->SendMessage(m_wm_ShowWindow, 0, -1);
		break;

	case BTN_ECUR_TEST:
		GetOwner()->SendMessage(m_wm_ShowWindow, 0, VTID_ECurrent);
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: SetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/12 - 17:32
// Desc.		:
//=============================================================================
void CWnd_Cfg_ECurrent::SetUpdateData()
{
	if (NULL == m_pstConfigInfo)
		return;

	CString strValue;
	if (m_nCameraModelType == ECUR_CM_2)
	{
		strValue.Format(_T("%.2f"), m_pstConfigInfo->dbOffset[0][Spec_ECurrent_CH1]);
		m_ed_Item[EDT_ECUR_OFFSET_18V_L].SetWindowText(strValue);

		strValue.Format(_T("%.2f"), m_pstConfigInfo->dbOffset[0][Spec_ECurrent_CH2]);
		m_ed_Item[EDT_ECUR_OFFSET_28V_L].SetWindowText(strValue);


		strValue.Format(_T("%.2f"), m_pstConfigInfo->dbOffset[1][Spec_ECurrent_CH1]);
		m_ed_Item[EDT_ECUR_OFFSET_18V_R].SetWindowText(strValue);

		strValue.Format(_T("%.2f"), m_pstConfigInfo->dbOffset[1][Spec_ECurrent_CH2]);
		m_ed_Item[EDT_ECUR_OFFSET_28V_R].SetWindowText(strValue);

		for (UINT nSpec = 0; nSpec < Spec_ECurrent_CH3; nSpec++)
		{
			strValue.Format(_T("%.2f"), m_pstConfigInfo->stSpec_Min[nSpec].dbValue);
			m_ed_SpecMin[nSpec].SetWindowText(strValue);

			strValue.Format(_T("%.2f"), m_pstConfigInfo->stSpec_Max[nSpec].dbValue);
			m_ed_SpecMax[nSpec].SetWindowText(strValue);

			m_chk_SpecMin[nSpec].SetCheck(m_pstConfigInfo->stSpec_Min[nSpec].bEnable);
			m_chk_SpecMax[nSpec].SetCheck(m_pstConfigInfo->stSpec_Max[nSpec].bEnable);
		}
	}
	else{

		for (int t = 0; t < Spec_ECurrent_Max; t++)
		{
			strValue.Format(_T("%.2f"), m_pstConfigInfo->dbOffset[0][t]);
			m_ed_Item[EDT_ECUR_OFFSET_CH1_L +t ].SetWindowText(strValue);


			strValue.Format(_T("%.2f"), m_pstConfigInfo->dbOffset[1][t]);
			m_ed_Item[EDT_ECUR_OFFSET_CH1_R + t].SetWindowText(strValue);
		}
		

		for (UINT nSpec = 0; nSpec < Spec_ECurrent_Max; nSpec++)
		{
			strValue.Format(_T("%.2f"), m_pstConfigInfo->stSpec_Min[nSpec].dbValue);
			m_ed_SpecMin[nSpec].SetWindowText(strValue);

			strValue.Format(_T("%.2f"), m_pstConfigInfo->stSpec_Max[nSpec].dbValue);
			m_ed_SpecMax[nSpec].SetWindowText(strValue);

			m_chk_SpecMin[nSpec].SetCheck(m_pstConfigInfo->stSpec_Min[nSpec].bEnable);
			m_chk_SpecMax[nSpec].SetCheck(m_pstConfigInfo->stSpec_Max[nSpec].bEnable);
		}
	}
	
}

//=============================================================================
// Method		: GetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/14 - 18:07
// Desc.		:
//=============================================================================
void CWnd_Cfg_ECurrent::GetUpdateData()
{
	if (NULL == m_pstConfigInfo)
		return;

	CString strValue;
	
	if (m_nCameraModelType == ECUR_CM_2)
	{
		m_ed_Item[EDT_ECUR_OFFSET_18V_L].GetWindowText(strValue);
		m_pstConfigInfo->dbOffset[0][Spec_ECurrent_CH1] = _ttof(strValue);

		m_ed_Item[EDT_ECUR_OFFSET_28V_L].GetWindowText(strValue);
		m_pstConfigInfo->dbOffset[0][Spec_ECurrent_CH2] = _ttof(strValue);

		m_ed_Item[EDT_ECUR_OFFSET_18V_R].GetWindowText(strValue);
		m_pstConfigInfo->dbOffset[1][Spec_ECurrent_CH1] = _ttof(strValue);

		m_ed_Item[EDT_ECUR_OFFSET_28V_R].GetWindowText(strValue);
		m_pstConfigInfo->dbOffset[1][Spec_ECurrent_CH2] = _ttof(strValue);

		for (UINT nSpec = 0; nSpec < Spec_ECurrent_CH3; nSpec++)
		{
			m_ed_SpecMin[nSpec].GetWindowText(strValue);
			m_pstConfigInfo->stSpec_Min[nSpec].dbValue = _ttof(strValue);

			m_ed_SpecMax[nSpec].GetWindowText(strValue);
			m_pstConfigInfo->stSpec_Max[nSpec].dbValue = _ttof(strValue);

			m_pstConfigInfo->stSpec_Min[nSpec].bEnable = m_chk_SpecMin[nSpec].GetCheck();
			m_pstConfigInfo->stSpec_Max[nSpec].bEnable = m_chk_SpecMax[nSpec].GetCheck();
		}
	}
	else{
		for (int t = 0; t < Spec_ECurrent_Max; t++)
		{
			m_ed_Item[EDT_ECUR_OFFSET_CH1_L+t].GetWindowText(strValue);
			m_pstConfigInfo->dbOffset[0][Spec_ECurrent_CH1 +t ] = _ttof(strValue);

			m_ed_Item[EDT_ECUR_OFFSET_CH1_R + t].GetWindowText(strValue);
			m_pstConfigInfo->dbOffset[1][Spec_ECurrent_CH1 + t] = _ttof(strValue);
		}
		for (UINT nSpec = 0; nSpec < Spec_ECurrent_Max; nSpec++)
		{
			m_ed_SpecMin[nSpec].GetWindowText(strValue);
			m_pstConfigInfo->stSpec_Min[nSpec].dbValue = _ttof(strValue);

			m_ed_SpecMax[nSpec].GetWindowText(strValue);
			m_pstConfigInfo->stSpec_Max[nSpec].dbValue = _ttof(strValue);

			m_pstConfigInfo->stSpec_Min[nSpec].bEnable = m_chk_SpecMin[nSpec].GetCheck();
			m_pstConfigInfo->stSpec_Max[nSpec].bEnable = m_chk_SpecMax[nSpec].GetCheck();
		}

	}

	
}


void CWnd_Cfg_ECurrent::Model_Change(){

	if (m_nCameraModelType == 0)
	{
		for (int t = 0; t < 4; t++)
		{
			m_st_Item[STI_ECUR_OFFSET_18V_L + t].SetText(g_szECurrent_Static[STI_ECUR_OFFSET_18V_L + t]);
		}

		for (int t = 0; t < 2; t++)
		{
			m_st_Spec[t].SetText(g_szECurrent_Spec[t]);
		}
	}
	else{
		for (int t = 0; t < 10; t++)
		{
			m_st_Item[STI_ECUR_OFFSET_CH1_L + t].SetText(g_szECurrent_StaticEntry[STI_ECUR_OFFSET_CH1_L + t]);
		}

		for (int t = 0; t < 5; t++)
		{
			m_st_Spec[t].SetText(g_szECurrent_Spec_Entry[t]);
		}
	}

}