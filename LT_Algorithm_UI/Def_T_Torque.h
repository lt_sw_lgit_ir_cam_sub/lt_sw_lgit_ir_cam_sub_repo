﻿#ifndef Def_T_Torque_h__
#define Def_T_Torque_h__

#include <afxwin.h>

#include "Def_T_Common.h"

#pragma pack(push,1)

typedef enum enTorque_Status
{
	Torque_Init,
	Torque_Rele,
	Torque_Lock,
	Torque_Pass,
	Torque_MaxNum,
};

typedef enum enTorque_Spec
{
	Spec_Tor_A = 0,
	Spec_Tor_B,
	Spec_Tor_C,
	Spec_Tor_D,
	Spec_Cnt_A,
	Spec_Cnt_B,
	Spec_Cnt_C,
	Spec_Cnt_D,
	Spec_Tor_Max,
};

static LPCTSTR	g_szTorque_Spec[] =
{
	_T("Torque A"),
	_T("Torque B"),
	_T("Torque C"),
	_T("Torque D"),
	_T("Lock Count A"),
	_T("Lock Count B"),
	_T("Lock Count C"),
	_T("Lock Count D"),
	NULL
};

typedef struct _tag_Torque_Opt
{
	ST_Comm_Spec stSpec_Min[Spec_Tor_Max];
	ST_Comm_Spec stSpec_Max[Spec_Tor_Max];

	_tag_Torque_Opt()
	{
		for (UINT nIdx = 0; nIdx < Spec_Tor_Max; nIdx++)
		{
			stSpec_Min[nIdx].Reset();
			stSpec_Max[nIdx].Reset();
		}
	};

	_tag_Torque_Opt& operator= (_tag_Torque_Opt& ref)
	{
		for (UINT nIdx = 0; nIdx < Spec_Tor_Max; nIdx++)
		{
			stSpec_Min[nIdx]	= ref.stSpec_Min[nIdx];
			stSpec_Max[nIdx]	= ref.stSpec_Max[nIdx];
		}

		return *this;
	};

}ST_Torque_Opt, *PST_Torque_Opt;

typedef struct _tag_Torque_Data
{
	UINT nResult;
	UINT nEachResult[Spec_Tor_Max];

	double dbValue[Spec_Tor_Max];

	enTorque_Status enStatus[Spec_Tor_Max];

	void Reset()
	{
		nResult = 1;

		for (UINT nIdx = 0; nIdx < Spec_Tor_Max; nIdx++)
		{
			nEachResult[nIdx]		= 1;
			dbValue[nIdx]			= 0.0;
			enStatus[nIdx]			= Torque_Init;
		}
	};

	_tag_Torque_Data()
	{
		Reset();
	};

	_tag_Torque_Data& operator= (_tag_Torque_Data& ref)
	{
		nResult	= ref.nResult;

		for (UINT nIdx = 0; nIdx < Spec_Tor_Max; nIdx++)
		{
			nEachResult[nIdx]	= ref.nEachResult[nIdx];
			dbValue[nIdx]		= ref.dbValue[nIdx];
			enStatus[nIdx]		= ref.enStatus[nIdx];
		}

		return *this;
	};

}ST_Torque_Data, *PST_Torque_Data;

typedef struct _tag_TI_Torque
{
	ST_Torque_Opt	stTorqueOpt;	// 검사 기준 데이터
	ST_Torque_Data	stTorqueData;	// 측정 데이터

	_tag_TI_Torque()
	{
	};

	void Reset()
	{
		stTorqueData.Reset();
	};

	_tag_TI_Torque& operator= (_tag_TI_Torque& ref)
	{
		stTorqueOpt  = ref.stTorqueOpt;
		stTorqueData = ref.stTorqueData;

		return *this;
	};

}ST_TI_Torque, *PST_TI_Torque;

#pragma pack(pop)

#endif // Def_Torque_h__