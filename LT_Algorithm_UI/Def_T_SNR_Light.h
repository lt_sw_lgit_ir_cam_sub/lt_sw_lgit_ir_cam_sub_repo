﻿#ifndef Def_T_SNR_Light_h__
#define Def_T_SNR_Light_h__

#include <afxwin.h>

#include "Def_T_Common.h"

#pragma pack(push,1)

static int g_nSNR_Light[] =
{
	29, 30, 31, 32, 33, 46, 47, 48, 49, 55,
	56, 57, 58, 66, 80, 86, 102, 107, 123, 128,
	144, 148, 166, 169, 187, 190, 208, 211, 229, 232,
	250, 253, 271, 274, 292, 296, 312, 317, 333, 338,
	354, 360, 374, 382, 383, 384, 385, 391, 392, 393,
	394, 407, 408, 409, 410, 411
};


typedef enum enSNR_L_Spec
{
	Spec_SNR_Light = 0,
	Spec_SNR_Light_MAX,
};

typedef enum enROI_SNR_Light
{
	ROI_SNR_Light_FST = 0,
	ROI_SNR_Light_LST = 55,
	ROI_SNR_Light_Max,
};



typedef struct _tag_SNR_LightRect
{
	CRect *rt_FullImageRect;
	CRect *rt_TestSectionFullRect;

	int nFullSectionCnt_X;
	int nFullSectionCnt_Y;
	int nTestSectionCnt;

	_tag_SNR_LightRect()
	{
		rt_FullImageRect = NULL;
		rt_TestSectionFullRect = NULL;
		nFullSectionCnt_X=0;
		nFullSectionCnt_Y = 0;
		nTestSectionCnt=0;
	};

	void FullRectCreate(unsigned int nSectionCnt_X, IN unsigned int nSectionCnt_Y){
		
		if (rt_FullImageRect == NULL)
		{
			rt_FullImageRect = new CRect[nSectionCnt_X* nSectionCnt_Y];
			nFullSectionCnt_X = nSectionCnt_X;
			nFullSectionCnt_Y = nSectionCnt_Y;
		}
	
	};

	//이미지를 21*21로 나누는 작업
	void FullRectMakeSection(IN unsigned int dwWidth, IN unsigned int dwHeight)
	{
		// 섹션 사이즈
		unsigned int nSectionSizeX = dwWidth / (nFullSectionCnt_X - 1);
		unsigned int nSectionSizeY = dwHeight / (nFullSectionCnt_Y - 1);

		// 위치값을 누적하여 계산
		int nPosX = 0, nPosY = 0;
		int nIndex = 0;

		for (int iIdx_Y = 0; iIdx_Y < nFullSectionCnt_Y; iIdx_Y++)
		{
			nPosX = 0;

			// 좌표 : 가로 줄
			if (iIdx_Y == 0)
				nPosY = 0;
			else if (iIdx_Y != 1)
				nPosY += nSectionSizeY;
			else
				nPosY += nSectionSizeY / 2;

			for (int iIdx_X = 0; iIdx_X < nFullSectionCnt_X; iIdx_X++)
			{
				nIndex = iIdx_Y * nFullSectionCnt_X + iIdx_X;

				// 좌표 : 세로 줄
				if (iIdx_X == 0)
					nPosX = 0;
				else if (iIdx_X != 1)
					nPosX += nSectionSizeX;
				else
					nPosX += nSectionSizeX / 2;

				

				rt_FullImageRect[nIndex].top = nPosY;
				rt_FullImageRect[nIndex].left = nPosX;


				int nW=0, nH = 0;
				// 사이즈 : 세로 줄
				if (iIdx_X == 0 || iIdx_X == nFullSectionCnt_X - 1)
					nW = nSectionSizeX / 2;
				else
					nW = nSectionSizeX;
				rt_FullImageRect[nIndex].right = rt_FullImageRect[nIndex].left + nW;

				// 사이즈 : 가로 줄
				if (iIdx_Y == 0 || iIdx_Y == nFullSectionCnt_Y - 1)
					nH = nSectionSizeY / 2;
				else
					nH = nSectionSizeY;

				rt_FullImageRect[nIndex].bottom = rt_FullImageRect[nIndex].top + nH;
			}
		}
	}

	void TestRectCreate(){

		if (rt_TestSectionFullRect == NULL)
		{
			rt_TestSectionFullRect = new CRect[ROI_SNR_Light_Max];
			nTestSectionCnt = ROI_SNR_Light_Max;
		}
	};

	void Release(){
		if (rt_FullImageRect != NULL){
			delete[] rt_FullImageRect;
			rt_FullImageRect = NULL;
		}
		if (rt_TestSectionFullRect != NULL){
			delete[]rt_TestSectionFullRect;
			rt_TestSectionFullRect = NULL;
		}
	};

	//이미지를 21*21 중 Test 위치 선정
	void TestRectMakeSection()
	{
		int iCount = 0;
		for (int nIdx = 0; nIdx < nTestSectionCnt; nIdx++)
		{
			rt_TestSectionFullRect[iCount] = rt_FullImageRect[g_nSNR_Light[nIdx]];
			iCount++;
		}
	}

	_tag_SNR_LightRect& operator= (_tag_SNR_LightRect& ref)
	{
		rt_FullImageRect = ref.rt_FullImageRect;
		rt_TestSectionFullRect = ref.rt_TestSectionFullRect;
		nFullSectionCnt_X = ref.nFullSectionCnt_X;
		nFullSectionCnt_Y = ref.nFullSectionCnt_Y;
		nTestSectionCnt = ref.nTestSectionCnt;
		return *this;
	};

}ST_SNR_LightRect, *PST_SNR_LightRect;
typedef struct _tag_SNR_Light_Opt
{
	bool	bIndex[ROI_SNR_Light_Max];

	ST_Comm_Spec stSpec_Min;
	ST_Comm_Spec stSpec_Max;

	//Test영역
	ST_SNR_LightRect st_SNR_LightRect;

	_tag_SNR_Light_Opt()
	{
		stSpec_Min.Reset();
		stSpec_Max.Reset();

		for (UINT nIdx = 0; nIdx < ROI_SNR_Light_Max; nIdx++)
		{
			bIndex[nIdx]	= TRUE;
		}
	};

	_tag_SNR_Light_Opt& operator= (_tag_SNR_Light_Opt& ref)
	{
		stSpec_Min = ref.stSpec_Min;
		stSpec_Max = ref.stSpec_Max;

		for (UINT nIdx = 0; nIdx < ROI_SNR_Light_Max; nIdx++)
		{
			bIndex[nIdx]	= ref.bIndex[nIdx];
		}
		
		st_SNR_LightRect = ref.st_SNR_LightRect;

		return *this;
	};

}ST_SNR_Light_Opt, *PST_SNR_Light_Opt;

typedef struct _tag_SNR_Light_Data
{
	// 결과
	UINT	nResult;

	double	dbFinalValue;
	UINT	nFinalIndx;
	BOOL bUse[ROI_SNR_Light_Max];

	double	dbEtcValue[ROI_SNR_Light_Max];
	
	short	sSignal[ROI_SNR_Light_Max];
	float	fNoise[ROI_SNR_Light_Max];

	_tag_SNR_Light_Data()
	{
		Reset();
	};

	void Reset()
	{
		nResult		 = 1;
		dbFinalValue = 0.0;
		nFinalIndx	 = 0;

		for (UINT nIdx = 0; nIdx < ROI_SNR_Light_Max; nIdx++)
		{
			dbEtcValue[nIdx]	= 0.0;
			sSignal[nIdx]		= 0;
			fNoise[nIdx]		= 0.0;
			bUse[nIdx]=0;
		}
	};
	void FailData(){
		nResult = 0;
		dbFinalValue = 0.0;
		nFinalIndx = 0;

		for (UINT nIdx = 0; nIdx < ROI_SNR_Light_Max; nIdx++)
		{
			dbEtcValue[nIdx] = 0.0;
			sSignal[nIdx] = 0;
			fNoise[nIdx] = 0.0;
			bUse[nIdx] = 0;
		}
	};
	_tag_SNR_Light_Data& operator= (_tag_SNR_Light_Data& ref)
	{
		nResult			= ref.nResult;
		dbFinalValue	= ref.dbFinalValue;
		nFinalIndx		= ref.nFinalIndx;

		for (UINT nIdx = 0; nIdx < ROI_SNR_Light_Max; nIdx++)
		{
			dbEtcValue[nIdx]	= ref.dbEtcValue[nIdx];
			sSignal[nIdx]		= ref.sSignal[nIdx];
			fNoise[nIdx]		= ref.fNoise[nIdx];
			bUse[nIdx] = ref.bUse[nIdx];
		}

		return *this;
	};

}ST_SNR_Light_Data, *PST_SNR_Light_Data;

typedef struct _tag_TI_SNR_Light
{
	ST_SNR_Light_Opt	stSNR_LightOpt;		// 검사 기준 데이터
	ST_SNR_Light_Data	stSNR_LightData;	// 측정 데이터

	_tag_TI_SNR_Light()
	{
	};

	void Reset()
	{
		stSNR_LightData.Reset();
	};

	_tag_TI_SNR_Light& operator= (_tag_TI_SNR_Light& ref)
	{
		stSNR_LightOpt = ref.stSNR_LightOpt;
		stSNR_LightData = ref.stSNR_LightData;

		return *this;
	};

}ST_TI_SNR_Light, *PST_TI_SNR_Light;

#pragma pack(pop)

#endif // Def_SNR_Light_h__