﻿#ifndef Wnd_Cfg_Dynamic_h__
#define Wnd_Cfg_Dynamic_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_T_Dynamic.h"
#include "Def_TestItem_VI.h"
#include "List_DynamicOp.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_Dynamic
//-----------------------------------------------------------------------------
class CWnd_Cfg_Dynamic : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_Dynamic)

public:
	CWnd_Cfg_Dynamic();
	virtual ~CWnd_Cfg_Dynamic();

	enum enDynamicStatic
	{
		STI_DYN_PARAM = 0,
		STI_DYN_SPEC,
		STI_DYN_MAX,
	};

	enum enDynamicButton
	{
		BTN_DYN_RESET = 0,
		BTN_DYN_TEST,
		BTN_DYN_MAX,
	};

	enum enDynamicComobox
	{
		CMB_DYN_MAX = 1,
	};

	enum enDynamicEdit
	{
		EDT_DYN_MAX = 1,
	};

protected:
	
	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	CFont						m_font;

	CList_DynamicOp				m_List;

	CVGStatic					m_st_Item[STI_DYN_MAX];
	CButton						m_bn_Item[BTN_DYN_MAX];
	CComboBox					m_cb_Item[CMB_DYN_MAX];
	CMFCMaskedEdit				m_ed_Item[EDT_DYN_MAX];

	CVGStatic					m_st_CapItem;
	CVGStatic					m_st_CapSpecMin;
	CVGStatic					m_st_CapSpecMax;

	CVGStatic					m_st_Spec[Spec_Par_SNR_MAX];
	CMFCMaskedEdit				m_ed_SpecMin[Spec_Par_SNR_MAX];
	CMFCMaskedEdit				m_ed_SpecMax[Spec_Par_SNR_MAX];
	CMFCButton					m_chk_SpecMin[Spec_Par_SNR_MAX];
	CMFCButton					m_chk_SpecMax[Spec_Par_SNR_MAX];

	ST_Dynamic_Opt*				m_pstConfigInfo = NULL;

public:

	void	SetPtr_RecipeInfo(ST_Dynamic_Opt* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstConfigInfo = pstRecipeInfo;
	};

	void	SetUpdateData		();
	void	GetUpdateData		();
};

#endif // Wnd_Cfg_Dynamic_h__
