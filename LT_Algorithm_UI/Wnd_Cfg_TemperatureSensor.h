﻿#ifndef Wnd_Cfg_TemperatureSensor_h__
#define Wnd_Cfg_TemperatureSensor_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_T_TemperatureSensor.h"
#include "Def_TestItem_VI.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_TemperatureSensor
//-----------------------------------------------------------------------------
class CWnd_Cfg_TemperatureSensor : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_TemperatureSensor)

public:
	CWnd_Cfg_TemperatureSensor();
	virtual ~CWnd_Cfg_TemperatureSensor();

	enum enTemperatureSensorStatic
	{
		STI_Temp_PARAM = 0,
		STI_Temp_SPEC,
		STI_Temp_SlaveID,
		STI_Temp_Address,
		STI_Temp_MAX,
	};

	enum enTemperatureSensorButton
	{
		BTN_Temp_RESET = 0,
		BTN_Temp_TEST,
		BTN_Temp_MAX,
	};

	enum enTemperatureSensorComobox
	{
		CMB_Temp_MAX = 1,
	};

	enum enTemperatureSensorEdit
	{
		EDT_Temp_SlaveID = 0,
		EDT_Temp_Address,
		EDT_Temp_MAX,
	};

protected:
	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	CFont						m_font;

	CVGStatic					m_st_Item[STI_Temp_MAX];
	CButton						m_bn_Item[BTN_Temp_MAX];
	CComboBox					m_cb_Item[CMB_Temp_MAX];
	CMFCMaskedEdit				m_ed_Item[EDT_Temp_MAX];

	CVGStatic					m_st_CapItem;
	CVGStatic					m_st_CapSpecMin;
	CVGStatic					m_st_CapSpecMax;

	CVGStatic					m_st_Spec[Spec_Temp_Max];
	CMFCMaskedEdit				m_ed_SpecMin[Spec_Temp_Max];
	CMFCMaskedEdit				m_ed_SpecMax[Spec_Temp_Max];
	CMFCButton					m_chk_SpecMin[Spec_Temp_Max];
	CMFCButton					m_chk_SpecMax[Spec_Temp_Max];

	ST_TemperatureSensor_Opt*			m_pstConfigInfo = NULL;

public:

	void	SetPtr_RecipeInfo(__in ST_TemperatureSensor_Opt* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstConfigInfo = pstRecipeInfo;
	};

	void	SetUpdateData		();
	void	GetUpdateData		();

	UINT Edit_GetValue(UINT nID, BOOL bHex = TRUE);
	void Edit_SetValue(UINT nID, DWORD wData, BOOL bHex = TRUE);
	//void	Get_TestItemInfo	(__in UINT nBegin, __in UINT nEnd, __out ST_TestItemInfo& stOutTestItemInfo);
};

#endif // Wnd_EEPROM_Verify_h__
