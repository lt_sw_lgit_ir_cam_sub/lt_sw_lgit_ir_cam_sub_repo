﻿#ifndef Def_T_TemperatureSensor_h__
#define Def_T_TemperatureSensor_h__

#include <afxwin.h>

#include "Def_T_Common.h"

#pragma pack(push,1)

typedef enum enTemperatureSensor_Spec
{
	Spec_Temp = 0,
	Spec_Temp_Max,
};

static LPCTSTR	g_szTemperatureSensor_Spec[] =
{
	_T("Temperature"),			
	NULL
};

typedef struct _tag_TemperatureSensor_Opt
{
	DWORD wslaveid;
	DWORD waddress;

	ST_Comm_Spec stSpec_Min[Spec_Temp_Max];
	ST_Comm_Spec stSpec_Max[Spec_Temp_Max];

	_tag_TemperatureSensor_Opt()
	{
		wslaveid	= 0xAC;
		waddress = 0x40;

		for (UINT nIdx = 0; nIdx < Spec_Temp_Max; nIdx++)
		{
			stSpec_Min[nIdx].Reset();
			stSpec_Max[nIdx].Reset();
		}
	};

	_tag_TemperatureSensor_Opt& operator= (_tag_TemperatureSensor_Opt& ref)
	{
		wslaveid = ref.wslaveid;
		waddress = ref.waddress;
	
		for (UINT nIdx = 0; nIdx < Spec_Temp_Max; nIdx++)
		{
			stSpec_Min[nIdx] = ref.stSpec_Min[nIdx];
			stSpec_Max[nIdx] = ref.stSpec_Max[nIdx];
		}

		return *this;
	};

}ST_TemperatureSensor_Opt, *PST_TemperatureSensor_Opt;

typedef struct _tag_TemperatureSensor_Data
{
	// 결과
	UINT nResult;

	UINT nEachResult[Spec_Temp_Max];
	double dbEachData[Spec_Temp_Max];

	double   dbTemperature;

	_tag_TemperatureSensor_Data()
	{
		Reset();
	};

	void Reset()
	{
		nResult	= 1;

		dbTemperature = 0;

		for (UINT nIdx = 0; nIdx < Spec_Temp_Max; nIdx++)
		{
			nEachResult[nIdx]	= 0;
			dbEachData[nIdx] = 0;
		}
	};

	void FailData()
	{
		nResult = 0;

		for (UINT nIdx = 0; nIdx < Spec_Temp_Max; nIdx++)
		{
			nEachResult[nIdx] = 0;
			dbEachData[nIdx] = 0;
		}
	};

	_tag_TemperatureSensor_Data& operator= (_tag_TemperatureSensor_Data& ref)
	{
		nResult			= ref.nResult;
		dbTemperature = ref.dbTemperature;

		for (UINT nIdx = 0; nIdx < Spec_Temp_Max; nIdx++)
		{
			nEachResult[nIdx]	= ref.nEachResult[nIdx];
			dbEachData[nIdx] = ref.dbEachData[nIdx];
		}

		return *this;
	};

}ST_TemperatureSensor_Data, *PST_TemperatureSensor_Data;


typedef struct _tag_TI_TemperatureSensor
{
	ST_TemperatureSensor_Opt		stTemperatureSensorOpt;		// 검사 기준 데이터
	ST_TemperatureSensor_Data		stTemperatureSensorData;		// 측정 데이터

	_tag_TI_TemperatureSensor()
	{
	};

	void Reset()
	{
		stTemperatureSensorData.Reset();
	};

	_tag_TI_TemperatureSensor& operator= (_tag_TI_TemperatureSensor& ref)
	{
		stTemperatureSensorOpt = ref.stTemperatureSensorOpt;
		stTemperatureSensorData = ref.stTemperatureSensorData;

		return *this;
	};

}ST_TI_TemperatureSensor, *PST_TI_TemperatureSensor;

#pragma pack(pop)

#endif // Def_TemperatureSensor_h__