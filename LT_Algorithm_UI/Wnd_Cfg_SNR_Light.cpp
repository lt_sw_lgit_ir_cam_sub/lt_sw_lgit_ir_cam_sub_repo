﻿// Wnd_Cfg_SNR_Light.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_Cfg_SNR_Light.h"
#include "resource.h"

static LPCTSTR	g_szSNR_Light_Static[] =
{
	_T("PARAMETER"),
	_T("SPEC"),
	NULL
};

static LPCTSTR	g_szSNR_Light_Button[] =
{
	_T("RESET"),
	_T("TEST"),
	NULL
};

static LPCTSTR	g_szSNR_Light_Spec[] =
{
	_T("Decibel / dB"),
	NULL
};

// CWnd_Cfg_SNR_Light
typedef enum SNR_LightOptID
{
	IDC_BTN_ITEM  = 1001,
	IDC_CMB_ITEM  = 2001,
	IDC_EDT_ITEM  = 3001,
	IDC_LIST_ITEM = 4001,

	IDC_ED_SPEC_MIN  = 5001,
	IDC_ED_SPEC_MAX  = 6001,
	IDC_CHK_SPEC_MIN = 7001,
	IDC_CHK_SPEC_MAX = 8001,

	IDC_INDEX_ITEM	 = 9001,
};


IMPLEMENT_DYNAMIC(CWnd_Cfg_SNR_Light, CWnd_Cfg_VIBase)

CWnd_Cfg_SNR_Light::CWnd_Cfg_SNR_Light()
{
	m_pstConfigInfo = NULL;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_SNR_Light::~CWnd_Cfg_SNR_Light()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_SNR_Light, CWnd_Cfg_VIBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM,		IDC_BTN_ITEM	+ 999,					OnRangeBtnCtrl	)
	ON_COMMAND_RANGE(IDC_INDEX_ITEM,	IDC_INDEX_ITEM	+ ROI_SNR_Light_Max,	OnRangeIndexCtrl)
END_MESSAGE_MAP()

// CWnd_Cfg_SNR_Light 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_Cfg_SNR_Light::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_Cfg_VIBase::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < STI_SNR_L_MAX; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdex].Create(g_szSNR_Light_Static[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	m_st_Item[STI_SNR_L_PARAM].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Item[STI_SNR_L_SPEC ].SetColorStyle(CVGStatic::ColorStyle_DarkGray);

	for (UINT nIdex = 0; nIdex < BTN_SNR_L_MAX; nIdex++)
	{
		m_bn_Item[nIdex].Create(g_szSNR_Light_Button[nIdex], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_ITEM + nIdex);
		m_bn_Item[nIdex].SetFont(&m_font);
	}

	for (UINT nIdex = 0; nIdex < EDT_SNR_L_MAX; nIdex++)
	{
		m_ed_Item[nIdex].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EDT_ITEM + nIdex);
		m_ed_Item[nIdex].SetWindowText(_T("0"));
		m_ed_Item[nIdex].SetValidChars(_T("0123456789.-"));
	}

	for (UINT nIdex = 0; nIdex < CMB_SNR_L_MAX; nIdex++)
	{
		m_cb_Item[nIdex].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CMB_ITEM + nIdex);
	}

	m_st_CapItem.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_CapItem.SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_CapItem.SetFont_Gdip(L"Arial", 9.0F);
	m_st_CapItem.Create(_T("Item"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_CapSpecMin.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_CapSpecMin.SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_CapSpecMin.SetFont_Gdip(L"Arial", 9.0F);
	m_st_CapSpecMin.Create(_T("Spec Min"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_CapSpecMax.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_CapSpecMax.SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_CapSpecMax.SetFont_Gdip(L"Arial", 9.0F);
	m_st_CapSpecMax.Create(_T("Spec Max"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	for (UINT nIdx = 0; nIdx < Spec_SNR_Light_MAX; nIdx++)
	{
		m_st_Spec[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Spec[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Spec[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Spec[nIdx].Create(g_szSNR_Light_Spec[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

		m_ed_SpecMin[nIdx].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_SPEC_MIN + nIdx);
		m_ed_SpecMin[nIdx].SetWindowText(_T("0"));
		m_ed_SpecMin[nIdx].SetValidChars(_T("0123456789.-"));

		m_ed_SpecMax[nIdx].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_SPEC_MAX + nIdx);
		m_ed_SpecMax[nIdx].SetWindowText(_T("0"));
		m_ed_SpecMax[nIdx].SetValidChars(_T("0123456789.-"));

		m_ed_SpecMin[nIdx].SetFont(&m_font);
		m_ed_SpecMax[nIdx].SetFont(&m_font);

		m_chk_SpecMin[nIdx].Create(_T(""), WS_VISIBLE | WS_BORDER | BS_AUTOCHECKBOX, rectDummy, this, IDC_CHK_SPEC_MIN + nIdx);
		m_chk_SpecMax[nIdx].Create(_T(""), WS_VISIBLE | WS_BORDER | BS_AUTOCHECKBOX, rectDummy, this, IDC_CHK_SPEC_MAX + nIdx);

		m_chk_SpecMin[nIdx].SetMouseCursorHand();
		m_chk_SpecMin[nIdx].SetImage(IDB_UNCHECKED_16);
		m_chk_SpecMin[nIdx].SetCheckedImage(IDB_CHECKED_16);
		m_chk_SpecMin[nIdx].SizeToContent();

		m_chk_SpecMax[nIdx].SetMouseCursorHand();
		m_chk_SpecMax[nIdx].SetImage(IDB_UNCHECKED_16);
		m_chk_SpecMax[nIdx].SetCheckedImage(IDB_CHECKED_16);
		m_chk_SpecMax[nIdx].SizeToContent();
	}

	for (UINT nIdex = 0; nIdex < ROI_SNR_Light_Max; nIdex++)
	{
		CString szCnt;
		szCnt.Format(_T("%d"), nIdex);

		m_st_Idex[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Idex[nIdex].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Idex[nIdex].SetFont_Gdip(L"Arial", 8.0F);
		m_st_Idex[nIdex].Create(szCnt, WS_VISIBLE | WS_CHILD | SS_CENTER | SS_NOTIFY | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_INDEX_ITEM + nIdex);
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_Cfg_SNR_Light::OnSize(UINT nType, int cx, int cy)
{
	CWnd_Cfg_VIBase::OnSize(nType, cx, cy);

	int iMargin  = 10;
	int iSpacing = 5;

	int iLeft	 = iMargin;
	int iTop	 = iMargin;
	int iWidth   = cx - iMargin - iMargin;
	int iHeight  = cy - iMargin - iMargin;
	
	int iSTWidth = iWidth / 5;
	int iSTHeight = 25;

	// 이름
	m_st_Item[STI_SNR_L_PARAM].MoveWindow(iLeft, iTop, iWidth, iSTHeight);

	// TEST 
// 	iTop += iSTHeight + iSpacing;
// 	iLeft = cx - iMargin - iSTWidth;
// 	m_bn_Item[BTN_SNR_L_RESET].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);
// 
// 	iTop += iSTHeight + iSpacing;
// 	iLeft = cx - iMargin - iSTWidth;
// 	m_bn_Item[BTN_SNR_L_TEST].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	int iIdexCnt	= 0;
	int iWidthCnt	= 19;
	int iHeightCnt	= 20;

	int iIdxWidth	= (iWidth - iWidthCnt - 1) / iWidthCnt;
	int iIdxHeight	= (iHeight / 2 - iHeightCnt - 1) / iHeightCnt;
	int iTempCnt	= 7;

	int iMarginTemp = 10;

	iLeft	 = iMarginTemp;
	iLeft	+= iIdxWidth * iTempCnt + iTempCnt;
 	iTop	+= iSTHeight + iSpacing;

	for (UINT nIdx = 0; nIdx < 5; nIdx++)
	{
		m_st_Idex[iIdexCnt++].MoveWindow(iLeft, iTop, iIdxWidth, iIdxHeight);
		iLeft += iIdxWidth + 1;
	}

	iTempCnt = 3;
	iLeft	 = iMarginTemp;
	iLeft	+= iIdxWidth * iTempCnt + iTempCnt;
	iTop	+= iIdxHeight + 1;

	for (UINT nIdx = 0; nIdx < 4; nIdx++)
	{
		m_st_Idex[iIdexCnt++].MoveWindow(iLeft, iTop, iIdxWidth, iIdxHeight);
		iLeft += iIdxWidth + 1;
	}

	iTempCnt = 5;
	iLeft += iIdxWidth * iTempCnt + iTempCnt;

	for (UINT nIdx = 0; nIdx < 4; nIdx++)
	{
		m_st_Idex[iIdexCnt++].MoveWindow(iLeft, iTop, iIdxWidth, iIdxHeight);
		iLeft += iIdxWidth + 1;
	}

	iTempCnt = 2;
	iLeft = iMarginTemp;
	iLeft += iIdxWidth * iTempCnt + iTempCnt;
	iTop += iIdxHeight + 1;
	m_st_Idex[iIdexCnt++].MoveWindow(iLeft, iTop, iIdxWidth, iIdxHeight);

	iTempCnt = 14;
	iLeft += iIdxWidth * iTempCnt + iTempCnt;
	m_st_Idex[iIdexCnt++].MoveWindow(iLeft, iTop, iIdxWidth, iIdxHeight);

	for (UINT nIdx = 0; nIdx < 3; nIdx++)
	{
		iTempCnt = 1;
		iLeft = iMarginTemp;
		iLeft += iIdxWidth * iTempCnt + iTempCnt;
		iTop += iIdxHeight + 1;
		m_st_Idex[iIdexCnt++].MoveWindow(iLeft, iTop, iIdxWidth, iIdxHeight);

		iTempCnt = 16;
		iLeft += iIdxWidth * iTempCnt + iTempCnt;
		m_st_Idex[iIdexCnt++].MoveWindow(iLeft, iTop, iIdxWidth, iIdxHeight);
	}

	for (UINT nIdx = 0; nIdx < 7; nIdx++)
	{
		iTempCnt = 0;
		iLeft = iMarginTemp;
		iLeft += iIdxWidth * iTempCnt + iTempCnt;
		iTop += iIdxHeight + 1;
		m_st_Idex[iIdexCnt++].MoveWindow(iLeft, iTop, iIdxWidth, iIdxHeight);

		iTempCnt = 18;
		iLeft += iIdxWidth * iTempCnt + iTempCnt;
		m_st_Idex[iIdexCnt++].MoveWindow(iLeft, iTop, iIdxWidth, iIdxHeight);
	}

	for (UINT nIdx = 0; nIdx < 3; nIdx++)
	{
		iTempCnt = 1;
		iLeft = iMarginTemp;
		iLeft += iIdxWidth * iTempCnt + iTempCnt;
		iTop += iIdxHeight + 1;
		m_st_Idex[iIdexCnt++].MoveWindow(iLeft, iTop, iIdxWidth, iIdxHeight);

		iTempCnt = 16;
		iLeft += iIdxWidth * iTempCnt + iTempCnt;
		m_st_Idex[iIdexCnt++].MoveWindow(iLeft, iTop, iIdxWidth, iIdxHeight);
	}

	iTempCnt = 2;
	iLeft = iMarginTemp;
	iLeft += iIdxWidth * iTempCnt + iTempCnt;
	iTop += iIdxHeight + 1;
	m_st_Idex[iIdexCnt++].MoveWindow(iLeft, iTop, iIdxWidth, iIdxHeight);

	iTempCnt = 14;
	iLeft += iIdxWidth * iTempCnt + iTempCnt;
	m_st_Idex[iIdexCnt++].MoveWindow(iLeft, iTop, iIdxWidth, iIdxHeight);

	iTempCnt = 3;
	iLeft = iMarginTemp;
	iLeft += iIdxWidth * iTempCnt + iTempCnt;
	iTop += iIdxHeight + 1;

	for (UINT nIdx = 0; nIdx < 4; nIdx++)
	{
		m_st_Idex[iIdexCnt++].MoveWindow(iLeft, iTop, iIdxWidth, iIdxHeight);
		iLeft += iIdxWidth + 1;
	}

	iTempCnt = 5;
	iLeft += iIdxWidth * iTempCnt + iTempCnt;

	for (UINT nIdx = 0; nIdx < 4; nIdx++)
	{
		m_st_Idex[iIdexCnt++].MoveWindow(iLeft, iTop, iIdxWidth, iIdxHeight);
		iLeft += iIdxWidth + 1;
	}

	iTempCnt = 7;
	iLeft = iMarginTemp;
	iLeft += iIdxWidth * iTempCnt + iTempCnt;
	iTop += iIdxHeight + 1;

	for (UINT nIdx = 0; nIdx < 5; nIdx++)
	{
		m_st_Idex[iIdexCnt++].MoveWindow(iLeft, iTop, iIdxWidth, iIdxHeight);
		iLeft += iIdxWidth + 1;
	}

	// 이름
	iLeft = iMargin;
	iTop += iIdxHeight + iSpacing + iSpacing + iSpacing;
	m_st_Item[STI_SNR_L_SPEC].MoveWindow(iLeft, iTop, iWidth, iSTHeight);

	// 스펙
	iLeft = iMargin;
	iTop += iSTHeight + iSpacing;

	int iCtrlWidth	= (iWidth - (iSpacing * 2)) / 3;	//170;
	int iLeft_2nd	= iLeft + iCtrlWidth + iSpacing;
	int iLeft_3rd	= iLeft_2nd + iCtrlWidth + iSpacing;
	int iEdWidth	= iCtrlWidth - iSpacing - iSTHeight;
	
	m_st_CapItem.MoveWindow(iLeft, iTop, iCtrlWidth, iSTHeight);
	m_st_CapSpecMin.MoveWindow(iLeft_2nd, iTop, iCtrlWidth, iSTHeight);
	m_st_CapSpecMax.MoveWindow(iLeft_3rd, iTop, iCtrlWidth, iSTHeight);

	for (UINT nIdx = 0; nIdx < Spec_SNR_Light_MAX; nIdx++)
	{
		iTop += iSTHeight + iSpacing;

		m_st_Spec[nIdx].MoveWindow(iLeft, iTop, iCtrlWidth, iSTHeight);

		m_chk_SpecMin[nIdx].MoveWindow(iLeft_2nd, iTop, iSTHeight, iSTHeight);
		m_ed_SpecMin[nIdx].MoveWindow(iLeft_2nd + iSTHeight + iSpacing, iTop, iEdWidth, iSTHeight);

		m_chk_SpecMax[nIdx].MoveWindow(iLeft_3rd, iTop, iSTHeight, iSTHeight);
		m_ed_SpecMax[nIdx].MoveWindow(iLeft_3rd + iSTHeight + iSpacing, iTop, iEdWidth, iSTHeight);
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_SNR_Light::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_Cfg_VIBase::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_Cfg_SNR_Light::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd_Cfg_VIBase::OnShowWindow(bShow, nStatus);

	if (NULL != m_pstConfigInfo)
	{
		if (TRUE == bShow)
		{
			GetOwner()->SendMessage(m_wm_ShowWindow, 0, Ovr_SNR_Light);
			GetOwner()->SendNotifyMessage(m_wm_ChangeOption, 0, 0);
		}
	}

}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_Cfg_SNR_Light::OnRangeBtnCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_BTN_ITEM;

	switch (nIdex)
	{
	case BTN_SNR_L_RESET:
		GetOwner()->SendMessage(m_wm_ShowWindow, 0, -1);
		break;
	case BTN_SNR_L_TEST:
		GetOwner()->SendMessage(m_wm_ShowWindow, 0, Ovr_SNR_Light);
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: OnRangeIndexCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2018/2/20 - 10:42
// Desc.		:
//=============================================================================
void CWnd_Cfg_SNR_Light::OnRangeIndexCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_INDEX_ITEM;

	GetOwner()->SendNotifyMessage(m_wm_ChangeOption, 0, 0);


	if (TRUE == m_pstConfigInfo->bIndex[nIdex])
	{
		m_st_Idex[nIdex].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_pstConfigInfo->bIndex[nIdex] = FALSE;
	}
	else
	{
		m_st_Idex[nIdex].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_pstConfigInfo->bIndex[nIdex] = TRUE;
	}
}

//=============================================================================
// Method		: SetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/12 - 17:32
// Desc.		:
//=============================================================================
void CWnd_Cfg_SNR_Light::SetUpdateData()
{
	if (m_pstConfigInfo == NULL)
		return;

	CString strValue;

	for (UINT nIdx = 0; nIdx < ROI_SNR_Light_Max; nIdx++)
	{
		if (TRUE == m_pstConfigInfo->bIndex[nIdx])
		{
			m_st_Idex[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		}
		else
		{
			m_st_Idex[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		}
	}

	for (UINT nSpec = 0; nSpec < Spec_SNR_Light_MAX; nSpec++)
	{
		strValue.Format(_T("%.2f"), m_pstConfigInfo->stSpec_Min.dbValue);
		m_ed_SpecMin[nSpec].SetWindowText(strValue);

		strValue.Format(_T("%.2f"), m_pstConfigInfo->stSpec_Max.dbValue);
		m_ed_SpecMax[nSpec].SetWindowText(strValue);

		m_chk_SpecMin[nSpec].SetCheck(m_pstConfigInfo->stSpec_Min.bEnable);
		m_chk_SpecMax[nSpec].SetCheck(m_pstConfigInfo->stSpec_Max.bEnable);
	}
}

//=============================================================================
// Method		: GetUpdateData
// Access		: public  
// Returns		: void
// Parameter	: __out ST_TestItemOpt & stTestItemOpt
// Qualifier	:
// Last Update	: 2017/10/14 - 18:07
// Desc.		:
//=============================================================================
void CWnd_Cfg_SNR_Light::GetUpdateData()
{
	if (m_pstConfigInfo == NULL)
		return;

	CString strValue;

	for (UINT nSpec = 0; nSpec < Spec_SNR_Light_MAX; nSpec++)
	{
		m_ed_SpecMin[nSpec].GetWindowText(strValue);
		m_pstConfigInfo->stSpec_Min.dbValue = _ttof(strValue);

		m_ed_SpecMax[nSpec].GetWindowText(strValue);
		m_pstConfigInfo->stSpec_Max.dbValue = _ttof(strValue);

		m_pstConfigInfo->stSpec_Min.bEnable = m_chk_SpecMin[nSpec].GetCheck();
		m_pstConfigInfo->stSpec_Max.bEnable = m_chk_SpecMax[nSpec].GetCheck();
	}
}