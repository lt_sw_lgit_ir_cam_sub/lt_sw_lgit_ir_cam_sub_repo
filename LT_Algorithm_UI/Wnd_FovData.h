#ifndef Wnd_FovData_h__
#define Wnd_FovData_h__

#pragma once

#include "afxwin.h"

#include "VGStatic.h"
#include "Def_T_FOV.h"
// CWnd_FovData
typedef enum
{
	enFOData_Header = 0,
	enFOData_Horizon = 0,
	enFOData_Vertical,
	enFOData_MAX,
}enumHeaderFOData;

typedef enum
{
	enFOData_1st = 0,
	enFOData_inputmax,
}enumDataFOData;

static	LPCTSTR	g_szFOData[] =
{
	_T("Horizon"),
	_T("Vertical"),
	NULL
};

class CWnd_FovData : public CWnd
{
	DECLARE_DYNAMIC(CWnd_FovData)

public:
	CWnd_FovData();
	virtual ~CWnd_FovData();

	UINT m_nHeaderCnt = enFOData_MAX;
	CString m_SzHeaderName[enFOData_MAX];

	//---Class 정의 시 세팅 해야함.
	void SetHeader(UINT nHeaderCnt){

		if (nHeaderCnt > enFOData_MAX)
		{
			nHeaderCnt = enFOData_MAX;
		}
		m_nHeaderCnt = nHeaderCnt;
	};

	void SetHeaderName(UINT nCnt, CString szHeader){

		if (nCnt > enFOData_MAX)
		{
			nCnt = enFOData_MAX-1;
		}
		m_SzHeaderName[nCnt] = szHeader;
	};

	CVGStatic		m_st_Header[enFOData_MAX];
	CVGStatic		m_st_Data[enFOData_inputmax][enFOData_MAX];

//	CVGStatic		m_st_ResultView;


protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	void DataSetting(UINT nDataIdx, UINT nIdx, BOOL MODE, double dData);
	void DataEachReset(UINT nDataIdx, UINT nIdx);
	void DataReset();
	void DataDisplay(ST_FOV_Data stData);
};


#endif // Wnd_FovData_h__
