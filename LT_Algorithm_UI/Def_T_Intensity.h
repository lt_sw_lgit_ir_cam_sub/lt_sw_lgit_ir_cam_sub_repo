﻿#ifndef Def_T_Intensity_h__
#define Def_T_Intensity_h__

#include <afxwin.h>

#include "Def_T_Common.h"

#pragma pack(push,1)

typedef enum enIntensity_Spec
{
	Spec_Intensity_LT = 0,
	Spec_Intensity_LB,
	Spec_Intensity_RT,
	Spec_Intensity_RB,
	Spec_Intensity_CC,
	Spec_Intensity_MAX,
};

static LPCTSTR	g_szIntensity_Spec[] =
{
	_T("LT Percent"),
	_T("LB Percent"),
	_T("RT Percent"),
	_T("RB Percent"),
	_T("Center Percent"),
	NULL
};

typedef enum enROI_Intencity
{
	ROI_Intencity_LT = 0,
	ROI_Intencity_LB,
	ROI_Intencity_RT,
	ROI_Intencity_RB,
	ROI_Intencity_CC,
	ROI_Intencity_Max,
};

static LPCTSTR g_szROI_Intencity[] =
{
	_T("LT"),
	_T("LB"),
	_T("RT"),
	_T("RB"),
	_T("Center"),
	NULL
};

typedef struct _tag_Intensity_Opt
{
	ST_RegionROI stRegion[ROI_Intencity_Max];

	ST_Comm_Spec stSpec_Min[Spec_Intensity_MAX];
	ST_Comm_Spec stSpec_Max[Spec_Intensity_MAX];

	_tag_Intensity_Opt()
	{	
		for (UINT nIdx = 0; nIdx < ROI_Intencity_Max; nIdx++)
		{
			stSpec_Min[nIdx].Reset();
			stSpec_Max[nIdx].Reset();
		}

		for (UINT nIdx = 0; nIdx < ROI_Intencity_Max; nIdx++)
		{
			stRegion[nIdx].Reset();
		}
	};

	_tag_Intensity_Opt& operator= (_tag_Intensity_Opt& ref)
	{
		for (UINT nIdx = 0; nIdx < ROI_Intencity_Max; nIdx++)
		{
			stSpec_Min[nIdx] = ref.stSpec_Min[nIdx];
			stSpec_Max[nIdx] = ref.stSpec_Max[nIdx];
		}

		for (UINT nIdx = 0; nIdx < ROI_Intencity_Max; nIdx++)
		{
			stRegion[nIdx]	 = ref.stRegion[nIdx];
		}

		return *this;
	};

}ST_Intensity_Opt, *PST_Intensity_Opt;

typedef struct _tag_Intensity_Data
{
	UINT nResult;

	UINT		nEachResult[ROI_Intencity_Max];
	BOOL		bUse[ROI_Intencity_Max];
	double		dbValue[ROI_Intencity_Max];

	float		fSignal[ROI_Intencity_Max];
	float		fNoise[ROI_Intencity_Max];

	float		fPercent[ROI_Intencity_Max];

	_tag_Intensity_Data()
	{
		Reset();
	};

	void Reset()
	{
		nResult = 1;

		for (UINT nIdx = 0; nIdx < ROI_Intencity_Max; nIdx++)
		{
			nEachResult[nIdx] = 0;
			dbValue[nIdx]	 = 0.0;
			fSignal[nIdx] = 0;
			fNoise[nIdx]	 = 0.0;
			fPercent[nIdx]	 = 0.0;
			bUse[nIdx] = 0;
		}
	};
	void FailData(){
		nResult = 0;

		for (UINT nIdx = 0; nIdx < ROI_Intencity_Max; nIdx++)
		{
			nEachResult[nIdx] = 0;
			dbValue[nIdx] = 0.0;
			fSignal[nIdx] = 0;
			fNoise[nIdx] = 0.0;
			fPercent[nIdx] = 0.0;
			bUse[nIdx] = 0;
		}
	};
	_tag_Intensity_Data& operator= (_tag_Intensity_Data& ref)
	{
		nResult	 = ref.nResult;

		for (UINT nIdx = 0; nIdx < ROI_Intencity_Max; nIdx++)
		{
			nEachResult[nIdx] = ref.nEachResult[nIdx];
			dbValue[nIdx] = ref.dbValue[nIdx];
			fSignal[nIdx] = ref.fSignal[nIdx];
			fNoise[nIdx]	= ref.fNoise[nIdx];
			fPercent[nIdx] = ref.fPercent[nIdx];
			bUse[nIdx] = ref.bUse[nIdx];
		}

		return *this;
	};

}ST_Intensity_Data, *PST_Intensity_Data;

typedef struct _tag_TI_Intensity
{
	ST_Intensity_Opt	stIntensityOpt;		// 검사 기준 데이터
	ST_Intensity_Data	stIntensityData;	// 측정 데이터

	_tag_TI_Intensity()
	{
	};

	void Reset()
	{
		stIntensityData.Reset();
	};

	_tag_TI_Intensity& operator= (_tag_TI_Intensity& ref)
	{
		stIntensityOpt	= ref.stIntensityOpt;
		stIntensityData	= ref.stIntensityData;

		return *this;
	};

}ST_TI_Intensity, *PST_TI_Intensity;

#pragma pack(pop)

#endif // Def_Intensity_h__

