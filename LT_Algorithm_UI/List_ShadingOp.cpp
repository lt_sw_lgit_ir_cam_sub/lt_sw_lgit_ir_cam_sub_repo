﻿// List_ShadingOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_ShadingOp.h"

typedef enum enListNumShadingOp
{
	ShaOp_Object = 0,
	ShaOp_MinSpecUse,
	ShaOp_MinSpec,
	ShaOp_MaxSpecUse,
	ShaOp_MaxSpec,
	ShaOp_MaxCol,
};

static LPCTSTR	g_lpszHeader_ShadingOp[] =
{
	_T(""),
	_T(""),
	_T("Min"),
	_T(""),
	_T("Max"),
	NULL
};

static LPCTSTR	g_lpszItem_ShadingOp[] =
{
	NULL
};

const int	iListAglin_ShadingOp[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_ShadingOp[] =
{
	23,
	15,
	48,
	15,
	48,
};

#define IDC_EDT_CELLEDIT			1000
#define IDC_COM_CELLCOMBO_TYPE		2000
#define IDC_COM_CELLCOMBO_COLOR		3000
#define IDC_CHK_SPEC_MIN			4000
#define IDC_CHK_SPEC_MAX			5000

IMPLEMENT_DYNAMIC(CList_ShadingOp, CList_Cfg_VIBase)

CList_ShadingOp::CList_ShadingOp()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol = 0;
	m_nEditRow = 0;

	m_pstConfigInfo = NULL;
}

CList_ShadingOp::~CList_ShadingOp()
{
	m_Font.DeleteObject();
}
BEGIN_MESSAGE_MAP(CList_ShadingOp, CList_Cfg_VIBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT	(NM_CLICK,					&CList_ShadingOp::OnNMClick				)
	ON_NOTIFY_REFLECT	(NM_DBLCLK,					&CList_ShadingOp::OnNMDblclk			)
	ON_EN_KILLFOCUS		(IDC_EDT_CELLEDIT,			&CList_ShadingOp::OnEnKillFocusEdit		)
	ON_COMMAND_RANGE	(IDC_CHK_SPEC_MIN, IDC_CHK_SPEC_MIN + 999, OnRangeCheckBtnMinCtrl	)
	ON_COMMAND_RANGE	(IDC_CHK_SPEC_MAX, IDC_CHK_SPEC_MAX + 999, OnRangeCheckBtnMaxCtrl	)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_ShadingOp 메시지 처리기입니다.
int CList_ShadingOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CList_Cfg_VIBase::OnCreate(lpCreateStruct) == -1)
		return -1;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	SetFont(&m_Font);
	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader();
	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER | ES_NUMBER, CRect(0, 0, 0, 0), this, IDC_EDT_CELLEDIT);

	for (UINT nIdx = 0; nIdx < ShaOp_ItemNum; nIdx++)
	{
		GetSubItemRect(nIdx, ShaOp_MinSpecUse, LVIR_BOUNDS, rectDummy);
		ClientToScreen(rectDummy);
		ScreenToClient(rectDummy);

		m_chk_SpecMin[nIdx].Create(_T(""), WS_VISIBLE | WS_BORDER | BS_AUTOCHECKBOX, rectDummy, this, IDC_CHK_SPEC_MIN + nIdx);
		m_chk_SpecMin[nIdx].SetWindowPos(NULL, rectDummy.left, rectDummy.top, rectDummy.Width(), rectDummy.Height(), SWP_SHOWWINDOW);

		GetSubItemRect(nIdx, ShaOp_MaxSpecUse, LVIR_BOUNDS, rectDummy);
		ClientToScreen(rectDummy);
		ScreenToClient(rectDummy);

		m_chk_SpecMax[nIdx].Create(_T(""), WS_VISIBLE | WS_BORDER | BS_AUTOCHECKBOX, rectDummy, this, IDC_CHK_SPEC_MAX + nIdx);
		m_chk_SpecMax[nIdx].SetWindowPos(NULL, rectDummy.left, rectDummy.top, rectDummy.Width(), rectDummy.Height(), SWP_SHOWWINDOW);
	}

	this->GetHeaderCtrl()->EnableWindow(FALSE);
	
	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_ShadingOp::OnSize(UINT nType, int cx, int cy)
{
	CList_Cfg_VIBase::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_ShadingOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CList_Cfg_VIBase::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_ShadingOp::InitHeader()
{
	for (int nCol = 0; nCol < ShaOp_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_ShadingOp[nCol], iListAglin_ShadingOp[nCol], iHeaderWidth_ShadingOp[nCol]);
	}

	for (int nCol = 0; nCol < ShaOp_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_ShadingOp[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 17:25
// Desc.		:
//=============================================================================
void CList_ShadingOp::InsertFullData()
{
	if (m_pstConfigInfo == NULL)
		return;

 	DeleteAllItems();

	for (UINT nIdx = 0; nIdx < ShaOp_ItemNum; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
 	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/6/26 - 14:26
// Desc.		:
//=============================================================================
void CList_ShadingOp::SetRectRow(UINT nRow)
{
	if (NULL == m_pstConfigInfo)
		return;

	CString strValue;

	strValue.Format(_T("%d"), GetItemIndexNumber(nRow));
	SetItemText(nRow, ShaOp_Object, strValue);

	if (m_pstConfigInfo->nType[GetItemIndexNumber(nRow)] == TRUE)
		SetItemState(nRow, 0x2000, LVIS_STATEIMAGEMASK);
	else
		SetItemState(nRow, 0x1000, LVIS_STATEIMAGEMASK);

	strValue.Format(_T("%.1f"), m_pstConfigInfo->stSpec_Min[GetItemIndexNumber(nRow)].dbValue);
	SetItemText(nRow, ShaOp_MinSpec, strValue);

	strValue.Format(_T("%.1f"), m_pstConfigInfo->stSpec_Max[GetItemIndexNumber(nRow)].dbValue);
	SetItemText(nRow, ShaOp_MaxSpec, strValue);

	m_chk_SpecMin[nRow].SetCheck(m_pstConfigInfo->stSpec_Min[GetItemIndexNumber(nRow)].bEnable);
	m_chk_SpecMax[nRow].SetCheck(m_pstConfigInfo->stSpec_Max[GetItemIndexNumber(nRow)].bEnable);
}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_ShadingOp::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	LVHITTESTINFO HitInfo;
	HitInfo.pt = pNMItemActivate->ptAction;

	HitTest(&HitInfo);

	// Check Ctrl Event
	if (HitInfo.flags == LVHT_ONITEMSTATEICON)
	{
		UINT nBuffer;

		nBuffer = GetItemState(pNMItemActivate->iItem, LVIS_STATEIMAGEMASK);

		if (nBuffer == 0x2000)
			m_pstConfigInfo->nType[GetItemIndexNumber(pNMItemActivate->iItem)] = FALSE;

		if (nBuffer == 0x1000)
			m_pstConfigInfo->nType[GetItemIndexNumber(pNMItemActivate->iItem)] = TRUE;
	}

	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_ShadingOp::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem == ShaOp_Object)
		{
			LVHITTESTINFO HitInfo;
			HitInfo.pt = pNMItemActivate->ptAction;

			HitTest(&HitInfo);

			// Check Ctrl Event
			if (HitInfo.flags == LVHT_ONITEMSTATEICON)
			{
				UINT nBuffer;

				nBuffer = GetItemState(pNMItemActivate->iItem, LVIS_STATEIMAGEMASK);

				if (nBuffer == 0x2000)
					m_pstConfigInfo->nType[GetItemIndexNumber(pNMItemActivate->iItem)] = FALSE;

				if (nBuffer == 0x1000)
					m_pstConfigInfo->nType[GetItemIndexNumber(pNMItemActivate->iItem)] = TRUE;
			}
		}

		if (pNMItemActivate->iSubItem < ShaOp_MaxCol && pNMItemActivate->iSubItem > 0)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);

			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

			m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
			m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
			m_ed_CellEdit.SetFocus();
		}
	}

	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
void CList_ShadingOp::OnEnKillFocusEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	if (m_nEditCol == ShaOp_MinSpec || m_nEditCol == ShaOp_MaxSpec)
	{
		UpdateCelldbData(m_nEditRow, m_nEditCol, _ttof(strText));
	}
	else
	{
		UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText));
	}

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

}

//=============================================================================
// Method		: GetItemIndexNumber
// Access		: protected  
// Returns		: UINT
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2018/2/21 - 8:53
// Desc.		:
//=============================================================================
UINT CList_ShadingOp::GetItemIndexNumber(UINT nRow)
{
	switch (m_InspectionType)
	{
	case IDX_Shading_Ver_CT:
		return	nIndexVer_CT[nRow];
		break;
	case IDX_Shading_Ver_CB:
		return	nIndexVer_CB[nRow];
		break;
	case IDX_Shading_Hov_LC:
		return	nIndexHov_LC[nRow];
		break;
	case IDX_Shading_Hov_RC:
		return	nIndexHov_RC[nRow];
		break;
	case IDX_Shading_Dig_LT:
		return	nIndexDig_LT[nRow];
		break;
	case IDX_Shading_Dig_LB:
		return	nIndexDig_LB[nRow];
		break;
	case IDX_Shading_Dig_RT:
		return	nIndexDig_RT[nRow];
		break;
	case IDX_Shading_Dig_RB:
		return	nIndexDig_RB[nRow];
		break;
	default:
		break;
	}

	return	0;
}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
BOOL CList_ShadingOp::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	return TRUE;
}


//=============================================================================
// Method		: UpdateCellData_double
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dValue
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_ShadingOp::UpdateCelldbData(UINT nRow, UINT nCol, double dbValue)
{
	switch (nCol)
	{
	case ShaOp_MinSpec:
		m_pstConfigInfo->stSpec_Min[GetItemIndexNumber(nRow)].dbValue = dbValue;
		break;
	case ShaOp_MaxSpec:
		m_pstConfigInfo->stSpec_Max[GetItemIndexNumber(nRow)].dbValue = dbValue;
		break;
	default:
		break;
	}

	CString str;
	str.Format(_T("%.1f"), dbValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_ShadingOp::GetCellData()
{
	return;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_ShadingOp::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue = _ttoi(strText);
		double dbValue = _ttof(strText);

		if (0 < zDelta)
		{
			iValue = iValue + ((zDelta / 120));
			dbValue = dbValue + ((zDelta / 120) * 0.1);
		}
		else
		{
			if (0 < iValue)
				iValue = iValue + ((zDelta / 120));

			if (0 < dbValue)
				dbValue = dbValue + ((zDelta / 120) * 0.1);
		}

		if (iValue < 0)
			iValue = 0;

		if (iValue > 2000)
			iValue = 2000;

		if (dbValue < 0.0)
			dbValue = 0.0;

		if (m_nEditCol == ShaOp_MinSpec || m_nEditCol == ShaOp_MaxSpec)
		{
			UpdateCelldbData(m_nEditRow, m_nEditCol, dbValue);
		}
		else
		{
			UpdateCellData(m_nEditRow, m_nEditCol, iValue);
		}
	}

	return CList_Cfg_VIBase::OnMouseWheel(nFlags, zDelta, pt);
}

//=============================================================================
// Method		: OnRangeCheckBtnMinCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2018/2/20 - 9:32
// Desc.		:
//=============================================================================
void CList_ShadingOp::OnRangeCheckBtnMinCtrl(UINT nID)
{
	UINT nIndex = nID - IDC_CHK_SPEC_MIN;

	if (BST_CHECKED == m_chk_SpecMin[nIndex].GetCheck())
	{
		m_pstConfigInfo->stSpec_Min[GetItemIndexNumber(nIndex)].bEnable = TRUE;
	}
	else
	{
		m_pstConfigInfo->stSpec_Min[GetItemIndexNumber(nIndex)].bEnable = FALSE;
	}
}

//=============================================================================
// Method		: OnRangeCheckBtnMaxCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2018/2/20 - 9:32
// Desc.		:
//=============================================================================
void CList_ShadingOp::OnRangeCheckBtnMaxCtrl(UINT nID)
{
	UINT nIndex = nID - IDC_CHK_SPEC_MAX;

	if (BST_CHECKED == m_chk_SpecMax[nIndex].GetCheck())
	{
		m_pstConfigInfo->stSpec_Max[GetItemIndexNumber(nIndex)].bEnable = TRUE;
	}
	else
	{
		m_pstConfigInfo->stSpec_Max[GetItemIndexNumber(nIndex)].bEnable = FALSE;
	}

}
