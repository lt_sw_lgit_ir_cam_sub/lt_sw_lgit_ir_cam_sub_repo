﻿#ifndef Wnd_Cfg_ActiveAlign_h__
#define Wnd_Cfg_ActiveAlign_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_T_ActiveAlign.h"
#include "Def_TestItem_VI.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_ActiveAlign
//-----------------------------------------------------------------------------
class CWnd_Cfg_ActiveAlign : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_ActiveAlign)

public:
	CWnd_Cfg_ActiveAlign();
	virtual ~CWnd_Cfg_ActiveAlign();

	enum enActiveAlignStatic
	{
		STI_AA_PARAM = 0,
		STI_AA_TARGET_CNT,
		STI_AA_TARGET_R,
		STI_AA_TARGET_X_L,
		STI_AA_TARGET_Y_L,
		STI_AA_TARGET_X_R,
		STI_AA_TARGET_Y_R,
		STI_AA_MAX,
	};

	enum enActiveAlignButton
	{
		BTN_AA_RESET = 0,
		BTN_AA_TEST,
		BTN_AA_MAX,
	};

	enum enActiveAlignComobox
	{
		CMB_AA_MAX = 1,
	};

	enum enActiveAlignEdit
	{
		EDT_AA_TARGET_CNT = 0,
		EDT_AA_TARGET_R,
		EDT_AA_TARGET_X_L,
		EDT_AA_TARGET_Y_L,
		EDT_AA_TARGET_X_R,
		EDT_AA_TARGET_Y_R,
		EDT_AA_MAX,
	};

protected:
	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	CFont						m_font;

	CVGStatic					m_st_Item[STI_AA_MAX];
	CButton						m_bn_Item[BTN_AA_MAX];
	CComboBox					m_cb_Item[CMB_AA_MAX];
	CMFCMaskedEdit				m_ed_Item[EDT_AA_MAX];

	ST_ActiveAlign_Opt*			m_pstConfigInfo = NULL;

	// 0 -> Camera1, 1 -> Camera2
	UINT						m_nModelType = 0;
public:

	void	SetPtr_RecipeInfo(__in ST_ActiveAlign_Opt* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstConfigInfo = pstRecipeInfo;
	};

	// 0 -> Camera1, 1 -> Camera2
	void OnSetModelType(__in UINT nType)
	{
		m_nModelType = nType;

		CRect rc;
		GetClientRect(rc);
		OnSize(SIZE_RESTORED, rc.Width(), rc.Height());
	};

	void	SetUpdateData		();
	void	GetUpdateData		();
};

#endif // Wnd_ActiveAlign_h__
