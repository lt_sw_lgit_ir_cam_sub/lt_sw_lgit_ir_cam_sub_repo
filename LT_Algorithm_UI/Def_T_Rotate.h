﻿#ifndef Def_T_Rotate_h__
#define Def_T_Rotate_h__

#include <afxwin.h>

#include "Def_T_Common.h"

#pragma pack(push,1)

typedef enum enRotate_Spec
{
	Spec_Rotate = 0,
	Spec_Rotate_MAX,
};

static LPCTSTR	g_szRotate_Spec[] =
{
	_T("Rotate Degree"),
	NULL
};

typedef enum enROI_Rotate
{
	ROI_Rot_LT = 0,
	ROI_Rot_LB,
	ROI_Rot_RT,
	ROI_Rot_RB,
	ROI_Rot_Max,
};

static LPCTSTR g_szROI_Rotate[] =
{
	_T("LT"),
	_T("LB"),
	_T("RT"),
	_T("RB"),
	NULL
};

typedef struct _tag_Rotate_Opt
{
	double dbStandard;
	double dbOffset;

	ST_RegionROI stRegion[ROI_Rot_Max];

	ST_Comm_Spec stSpec_Min;
	ST_Comm_Spec stSpec_Max;

	_tag_Rotate_Opt()
	{	
		for (UINT nIdx = 0; nIdx < ROI_Rot_Max; nIdx++)
		{
			stRegion[nIdx].Reset();
		}

		dbStandard  = 0.0;
		dbOffset	= 0.0;

		stSpec_Min.Reset();
		stSpec_Max.Reset();
	};

	_tag_Rotate_Opt& operator= (_tag_Rotate_Opt& ref)
	{
		dbStandard  = ref.dbStandard;
		dbOffset	= ref.dbOffset;

		for (UINT nIdx = 0; nIdx < ROI_Rot_Max; nIdx++)
		{
			stRegion[nIdx] = ref.stRegion[nIdx];
		}

		stSpec_Min = ref.stSpec_Min;
		stSpec_Max = ref.stSpec_Max;

		return *this;
	};

}ST_Rotate_Opt, *PST_Rotate_Op;

typedef struct _tag_Rotate_Data
{
	UINT nResult;
	UINT nDetectResult;

	double dbValue;

	CRect	rtTestPicROI[ROI_Rot_Max];

	CPoint ptCenter[ROI_Rot_Max];

	_tag_Rotate_Data()
	{
		nResult = 1;
		dbValue = 0.0;
		nDetectResult = TRUE;
		for (UINT nIdx = 0; nIdx < ROI_Rot_Max; nIdx++)
		{
			ptCenter[nIdx].x = -1;
			ptCenter[nIdx].y = -1;

			rtTestPicROI[nIdx].SetRectEmpty();
		}
	};

	void Reset()
	{
		nResult = 1;
		dbValue = 0.0;
		nDetectResult = TRUE;

		for (UINT nIdx = 0; nIdx < ROI_Rot_Max; nIdx++)
		{
			ptCenter[nIdx].x = - 1;
			ptCenter[nIdx].y = - 1;
			rtTestPicROI[nIdx].SetRectEmpty();
		}
	};
	void FailData(){
		nResult = 0;
		dbValue = 0.0;
		nDetectResult = TRUE;

		for (UINT nIdx = 0; nIdx < ROI_Rot_Max; nIdx++)
		{
			ptCenter[nIdx].x = -1;
			ptCenter[nIdx].y = -1;
			rtTestPicROI[nIdx].SetRectEmpty();
		}
	};
	_tag_Rotate_Data& operator= (_tag_Rotate_Data& ref)
	{
		nResult	 = ref.nResult;
		dbValue  = ref.dbValue;
		nDetectResult  = ref.nDetectResult;

		for (UINT nIdx = 0; nIdx < ROI_Rot_Max; nIdx++)
		{
			ptCenter[nIdx].x = ref.ptCenter[nIdx].x;
			ptCenter[nIdx].y = ref.ptCenter[nIdx].y;
			rtTestPicROI[nIdx] = ref.rtTestPicROI[nIdx];
		}

		return *this;
	};

}ST_Rotate_Data, *PST_Rotate_Data;

typedef struct _tag_TI_Rotate
{
	CString			szItemName;		// 테스트 항목
	ST_Rotate_Opt	stRotateOpt;	// 검사 기준 데이터
	ST_Rotate_Data	stRotateData;	// 측정 데이터

	_tag_TI_Rotate()
	{
		szItemName = _T("Rotate");
	};

	void Reset()
	{
		stRotateData.Reset();
	};

	_tag_TI_Rotate& operator= (_tag_TI_Rotate& ref)
	{
		stRotateOpt		= ref.stRotateOpt;
		stRotateData	= ref.stRotateData;

		return *this;
	};

}ST_TI_Rotate, *PST_TI_Rotate;

#pragma pack(pop)

#endif // Def_Rotate_h__

