//*****************************************************************************
// Filename	: 	Cal_Stereo.h
// Created	:	2018/2/22 - 11:50
// Modified	:	2018/2/22 - 11:50
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#ifndef Cal_Stereo_h__
#define Cal_Stereo_h__


#pragma once

#include "MRA2_stereo_calibration.h"

//=============================================================================
// CCal_Stereo
//=============================================================================
class CCal_Stereo
{
public:
	CCal_Stereo();
	~CCal_Stereo();

protected:
	
	MRA2_HANDLE			m_hCal_Single		= NULL;
	MRA2_HANDLE			m_hCal_Stereo		= NULL;

	BOOL				m_bOpened_Single	= FALSE;
	BOOL				m_bOpened_Stereo	= FALSE;

public:

	// Mono Cal
	BOOL	Start_Calibration_Mono			();	// 테스트 용도

	BOOL	Set_Parameter_Mono				(__in int nBoard_W, __in int nBoard_H);
	BOOL	Detect_Pattern_Mono				(__in LPBYTE lpbyIn_Image, 
											 __in MRA2_IMAGE_FORMAT nIn_Format, 
											 __in int nWidth, 
											 __in int nHeight, 
											 __out MRA2_POINT* pstOutPoints, 
											 __out int* piOutCount, 
											 __in bool bIsCircle = true, 
											 __in bool bDisplayCorners = false);
	BOOL	Exec_CalIntrinsic_Mono			(__in MRA2_CAMERA_FLAG nFlag, __out SINGLE_CALIBRATION_RESULT& stCalResult);
	void	Close_CalHandle_Mono			();

	// Stereo Cal
	BOOL	Start_Calibration_Stereo		(); // 테스트 용도

	BOOL	Set_Parameter_Stereo			(__in int nBoard_W, __in int nBoard_H);
	BOOL	Detect_Pattern_Stereo			(__in LPBYTE lpbyIn_Image_Left, 
											 __in LPBYTE lpbyIn_Image_Right, 
											 __in MRA2_IMAGE_FORMAT nIn_Format, 
											 __in int nWidth, 
											 __in int nHeight,
											 __out MRA2_POINT* pstOutLeftPoints,
											 __out int* piOutLeftCount,
											 __out MRA2_POINT* pstOutRightPoints,
											 __out int* piOutRightCount,
											 __in bool bIsCircle = true, 
											 __in bool bDisplayCorners = false);


	BOOL	Exec_CalIntrinsic_Setreo		(__in MRA2_CAMERA_FLAG nFlag, __out STEREO_CALIBRATION_RESULT& stCalResult);
	void	Close_CalHandle_Stereo			();

};


#endif // Cal_Stereo_h__
