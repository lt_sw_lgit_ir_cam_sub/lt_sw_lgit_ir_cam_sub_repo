﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_StdInfo.h
// Created	:	2017/1/3 - 10:37
// Modified	:	2017/1/3 - 10:37
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Cfg_StdInfo_h__
#define Wnd_Cfg_StdInfo_h__

#pragma once

#include "Wnd_BaseView.h"
#include "Def_DataStruct.h"

class CWnd_Cfg_StdInfo : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_Cfg_StdInfo)

public:
	CWnd_Cfg_StdInfo();
	virtual ~CWnd_Cfg_StdInfo();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);
	virtual BOOL	PreTranslateMessage		(MSG* pMsg);

	CFont		m_font_Default;
	CFont		m_font_Data;

public:

	// 모델 데이터를 UI에 표시
	void			SetRecipeInfo(__in const ST_RecipeInfo* pRecipeInfo);
	// UI에 표시된 데이터의 구조체 반환	
	void			GetRecipeInfo(__out ST_RecipeInfo& stRecipeInfo);

};

#endif // Wnd_Cfg_StdInfo_h__
