﻿#ifndef Def_Mes_ImgT_h__
#define Def_Mes_ImgT_h__

#include <afxwin.h>


#pragma pack(push,1)


static LPCTSTR g_szMESEntryHeader[] =
{
	_T("EEPROM_OC_X"),
	_T("EEPROM_OC_Y"),
	_T("EEPROM_CHECK_1"),
	_T("EEPROM_CHECK_2"),
	_T("Reserved"),
	_T("Reserved"), //eeprom 6
	_T("Current_1.8V"),
	_T("Current_3.3V"),
	_T("Current_9.0V"),
	_T("Current_14.7V"),
	_T("Current_-5.7V"),//current 5
	_T("TemperatureSensor"), //TemperatureSensor 1
	_T("SFR_X1Tilt"),
	_T("SFR_X2Tilt"),
	_T("SFR_Y1Tilt"),
	_T("SFR_Y2Tilt"),
	_T("SFR_MIN_0"),
	_T("SFR_MIN_1"),
	_T("SFR_MIN_2"),
	_T("SFR_MIN_3"),
	_T("SFR_MIN_4"),
	_T("SFR_0"),
	_T("SFR_1"),
	_T("SFR_2"),
	_T("SFR_3"),
	_T("SFR_4"),
	_T("SFR_5"),
	_T("SFR_6"),
	_T("SFR_7"),
	_T("SFR_8"),
	_T("SFR_9"),
	_T("SFR_10"),
	_T("SFR_11"),
	_T("SFR_12"),
	_T("SFR_13"),
	_T("SFR_14"),
	_T("SFR_15"),
	_T("SFR_16"),
	_T("SFR_17"),
	_T("SFR_18"),
	_T("SFR_19"),
	_T("SFR_20"),
	_T("SFR_21"),
	_T("SFR_22"),
	_T("SFR_23"),
	_T("SFR_24"),
	_T("SFR_25"),
	_T("SFR_26"),
	_T("SFR_27"),
	_T("SFR_28"),
	_T("SFR_29"), //SFR 39
	_T("Fov_수평"),
	_T("Fov_수직"),
	_T("Reserved"),
	_T("Reserved"), //FOV 4
	_T("Distortion"),
	_T("Reserved"),
	_T("Reserved"), //Distortion 3 
	_T("3D_Depth_Brightness"),
	_T("3D_Depth_Deviation"),
	_T("Reserved"),
	_T("Reserved"), //3D_Depth 4
	_T("Particle"), //Particle 1
	_T("Shading_1"),
	_T("Shading_2"),
	_T("Shading_3"),
	_T("Shading_4"),
	_T("Shading_5"),
	_T("Shading_6"),
	_T("Shading_7"),
	_T("Shading_8"),
	_T("Shading_9"),
	_T("Shading_10"),
	_T("Shading_11"),
	_T("Shading_12"),
	_T("Shading_13"),
	_T("Shading_14"),
	_T("Shading_15"),
	_T("Shading_16"),
	_T("Shading_17"),
	_T("Shading_18"),
	_T("Shading_19"),
	_T("Shading_20"),
	_T("Shading_21"),
	_T("Shading_22"),
	_T("Shading_23"),
	_T("Shading_24"),
	_T("Shading_25"),
	_T("Shading_26"),
	_T("Shading_27"),
	_T("Shading_28"),
	_T("Shading_29"),
	_T("Shading_30"),
	_T("Shading_31"),
	_T("Shading_32"),
	_T("Shading_33"),
	_T("Shading_34"),
	_T("Shading_35"),
	_T("Shading_36"),
	_T("Shading_37"),
	_T("Shading_38"),
	_T("Shading_39"),
	_T("Shading_40"),
	_T("Shading_41"),
	_T("Shading_42"),
	_T("Shading_43"),
	_T("Shading_44"),
	_T("Shading_45"),
	_T("Shading_46"),
	_T("Shading_47"),
	_T("Shading_48"),
	_T("Shading_49"),
	_T("Shading_50"),
	_T("Shading_51"),
	_T("Shading_52"),
	_T("Shading_53"),
	_T("Shading_54"),
	_T("Shading_55"),
	_T("Shading_56"),
	_T("Shading_57"),
	_T("Shading_58"),
	_T("Shading_59"),
	_T("Shading_60"),
	_T("Shading_61"),
	_T("Shading_62"),
	_T("Shading_63"),
	_T("Shading_64"),
	_T("Shading_65"),
	_T("Shading_66"),
	_T("Shading_67"),
	_T("Shading_68"),
	_T("Shading_69"),
	_T("Shading_70"),
	_T("Shading_71"),
	_T("Shading_72"),
	_T("Shading_73"), //Shading 73
	_T("SNRLIGHT_1"),
	_T("SNRLIGHT_2"),
	_T("SNRLIGHT_3"),
	_T("SNRLIGHT_4"),
	_T("SNRLIGHT_5"),
	_T("SNRLIGHT_6"),
	_T("SNRLIGHT_7"),
	_T("SNRLIGHT_8"),
	_T("SNRLIGHT_9"),
	_T("SNRLIGHT_10"),
	_T("SNRLIGHT_11"),
	_T("SNRLIGHT_12"),
	_T("SNRLIGHT_13"),
	_T("SNRLIGHT_14"),
	_T("SNRLIGHT_15"),
	_T("SNRLIGHT_16"),
	_T("SNRLIGHT_17"),
	_T("SNRLIGHT_18"),
	_T("SNRLIGHT_19"),
	_T("SNRLIGHT_20"),
	_T("SNRLIGHT_21"),
	_T("SNRLIGHT_22"),
	_T("SNRLIGHT_23"),
	_T("SNRLIGHT_24"),
	_T("SNRLIGHT_25"),
	_T("SNRLIGHT_26"),
	_T("SNRLIGHT_27"),
	_T("SNRLIGHT_28"),
	_T("SNRLIGHT_29"),
	_T("SNRLIGHT_30"),
	_T("SNRLIGHT_31"),
	_T("SNRLIGHT_32"),
	_T("SNRLIGHT_33"),
	_T("SNRLIGHT_34"),
	_T("SNRLIGHT_35"),
	_T("SNRLIGHT_36"),
	_T("SNRLIGHT_37"),
	_T("SNRLIGHT_38"),
	_T("SNRLIGHT_39"),
	_T("SNRLIGHT_40"),
	_T("SNRLIGHT_41"),
	_T("SNRLIGHT_42"),
	_T("SNRLIGHT_43"),
	_T("SNRLIGHT_44"),
	_T("SNRLIGHT_45"),
	_T("SNRLIGHT_46"),
	_T("SNRLIGHT_47"),
	_T("SNRLIGHT_48"),
	_T("SNRLIGHT_49"),
	_T("SNRLIGHT_50"),
	_T("SNRLIGHT_51"),
	_T("SNRLIGHT_52"),
	_T("SNRLIGHT_53"),
	_T("SNRLIGHT_54"),
	_T("SNRLIGHT_55"),
	_T("SNRLIGHT_56"),//SNR_Light 56
	_T("D/R_1 영역 - 밝기(W)"),
	_T("D/R_1 영역 - 밝기(G)"),
	_T("D/R_1 영역 - 밝기(B)"),
	_T("D/R_1 영역 - STD(W)"),
	_T("D/R_1 영역 - STD(G"),
	_T("D/R_1 영역 - STD(B)"),
	_T("D/R_1 영역 - SNR_BW"),
	_T("D/R_1 영역 - DynamicRange"),
	_T("D/R_2 영역 - 밝기(W)"),
	_T("D/R_2 영역 - 밝기(G)"),
	_T("D/R_2 영역 - 밝기(B)"),
	_T("D/R_2 영역 - STD(W)"),
	_T("D/R_2 영역 - STD(G)"),
	_T("D/R_2 영역 - STD(B)"),
	_T("D/R_2 영역 - SNR_BW"),
	_T("D/R_2 영역 - DynamicRange"),
	_T("D/R_3 영역 - 밝기(W)"),
	_T("D/R_3 영역 - 밝기(G)"),
	_T("D/R_3 영역 - 밝기(B)"),
	_T("D/R_3 영역 - STD(W)"),
	_T("D/R_3 영역 - STD(G)"),
	_T("D/R_3 영역 - STD(B)"),
	_T("D/R_3 영역 - SNR_BW"),
	_T("D/R_3 영역 - DynamicRange"),
	_T("D/R_4 영역 - 밝기(W)"),
	_T("D/R_4 영역 - 밝기(G)"),
	_T("D/R_4 영역 - 밝기(B)"),
	_T("D/R_4 영역 - STD(W)"),
	_T("D/R_4 영역 - STD(G)"),
	_T("D/R_4 영역 - STD(B"),
	_T("D/R_4 영역 - SNR_BW"),
	_T("D/R_4 영역 - DynamicRange"),
	_T("D/R_5 영역 - 밝기(W)"),
	_T("D/R_5 영역 - 밝기(G)"),
	_T("D/R_5 영역 - 밝기(B)"),
	_T("D/R_5 영역 - STD(W)"),
	_T("D/R_5 영역 - STD(G)"),
	_T("D/R_5 영역 - STD(B)"),
	_T("D/R_5 영역 - SNR_BW"),
	_T("D/R_5 영역 - DynamicRange"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"), //DR 48
	_T("OpticalCenter_X"),
	_T("OpticalCenter_Y"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//OpticalCenter 5
	_T("Rotation"),
	_T("Reserved"),
	_T("Reserved"), //Rotation 3
	_T("FPN_X"),
	_T("FPN_Y"),
	_T("Reserved"),
	_T("Reserved"), //FPN 4
	_T("HotPixel"),
	_T("Reserved"),
	_T("Reserved"),//HotPixel 3
};


static LPCTSTR g_szMESHeader[] =
{
	_T("Current_1.8V"),
	_T("Current_2.8V"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("OpticalCenter_X"),
	_T("OpticalCenter_Y"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("SFR_Tilt_0_A"),
	_T("SFR_Tilt_1_A"),
	_T("SFR_Tilt_2_A"),
	_T("SFR_Tilt_3_A"),
	_T("SFR_CT_A"),
	_T("SFR_LT_0.5_A"),
	_T("SFR_LB_0.5_A"),
	_T("SFR_RT_0.5_A"),
	_T("SFR_RB_0.5_A"), //!SH _181206: 4개씩 추가
	_T("SFR_LT_0.7_A"),
	_T("SFR_LB_0.7_A"),
	_T("SFR_RT_0.7_A"),
	_T("SFR_RB_0.7_A"),
	_T("SFR_0_A"),
	_T("SFR_1_A"),
	_T("SFR_2_A"),
	_T("SFR_3_A"),
	_T("SFR_4_A"),
	_T("SFR_5_A"),
	_T("SFR_6_A"),
	_T("SFR_7_A"),
	_T("SFR_8_A"),
	_T("SFR_9_A"),
	_T("SFR_10_A"),
	_T("SFR_11_A"),
	_T("SFR_12_A"),
	_T("SFR_13_A"),
	_T("SFR_14_A"),
	_T("SFR_15_A"),
	_T("SFR_16_A"),
	_T("SFR_17_A"),
	_T("SFR_18_A"),
	_T("SFR_19_A"),
	_T("SFR_20_A"),
	_T("SFR_21_A"),
	_T("SFR_22_A"),
	_T("SFR_23_A"),
	_T("SFR_24_A"),
	_T("SFR_25_A"),
	_T("SFR_26_A"),
	_T("SFR_27_A"),
	_T("SFR_28_A"),
	_T("SFR_29_A"),
	_T("SFR_Tilt_0_B"),
	_T("SFR_Tilt_1_B"),
	_T("SFR_Tilt_2_B"),
	_T("SFR_Tilt_3_B"),
	_T("SFR_CT_B"),
	_T("SFR_LT_0.5_B"),
	_T("SFR_LB_0.5_B"),
	_T("SFR_RT_0.5_B"),
	_T("SFR_RB_0.5_B"),
	_T("SFR_LT_0.7_B"),
	_T("SFR_LB_0.7_B"),
	_T("SFR_RT_0.7_B"),
	_T("SFR_RB_0.7_B"),
	_T("SFR_0_B"),
	_T("SFR_1_B"),
	_T("SFR_2_B"),
	_T("SFR_3_B"),
	_T("SFR_4_B"),
	_T("SFR_5_B"),
	_T("SFR_6_B"),
	_T("SFR_7_B"),
	_T("SFR_8_B"),
	_T("SFR_9_B"),
	_T("SFR_10_B"),
	_T("SFR_11_B"),
	_T("SFR_12_B"),
	_T("SFR_13_B"),
	_T("SFR_14_B"),
	_T("SFR_15_B"),
	_T("SFR_16_B"),
	_T("SFR_17_B"),
	_T("SFR_18_B"),
	_T("SFR_19_B"),
	_T("SFR_20_B"),
	_T("SFR_21_B"),
	_T("SFR_22_B"),
	_T("SFR_23_B"),
	_T("SFR_24_B"),
	_T("SFR_25_B"),
	_T("SFR_26_B"),
	_T("SFR_27_B"),
	_T("SFR_28_B"),
	_T("SFR_29_B"),
	_T("SFR_Tilt_0_C"),
	_T("SFR_Tilt_1_C"),
	_T("SFR_Tilt_2_C"),
	_T("SFR_Tilt_3_C"),
	_T("SFR_CT_C"),
	_T("SFR_LT_0.5_C"),
	_T("SFR_LB_0.5_C"),
	_T("SFR_RT_0.5_C"),
	_T("SFR_RB_0.5_C"),
	_T("SFR_LT_0.7_C"),
	_T("SFR_LB_0.7_C"),
	_T("SFR_RT_0.7_C"),
	_T("SFR_RB_0.7_C"),
	_T("SFR_0_C"),
	_T("SFR_1_C"),
	_T("SFR_2_C"),
	_T("SFR_3_C"),
	_T("SFR_4_C"),
	_T("SFR_5_C"),
	_T("SFR_6_C"),
	_T("SFR_7_C"),
	_T("SFR_8_C"),
	_T("SFR_9_C"),
	_T("SFR_10_C"),
	_T("SFR_11_C"),
	_T("SFR_12_C"),
	_T("SFR_13_C"),
	_T("SFR_14_C"),
	_T("SFR_15_C"),
	_T("SFR_16_C"),
	_T("SFR_17_C"),
	_T("SFR_18_C"),
	_T("SFR_19_C"),
	_T("SFR_20_C"),
	_T("SFR_21_C"),
	_T("SFR_22_C"),
	_T("SFR_23_C"),
	_T("SFR_24_C"),
	_T("SFR_25_C"),
	_T("SFR_26_C"),
	_T("SFR_27_C"),
	_T("SFR_28_C"),
	_T("SFR_29_C"),
//	_T("SFR_30"),
	_T("Rotation"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Distortion"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Fov_수평"),
	_T("Fov_수직"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Stain_#1 농도"),
	_T("Stain_#1 개수"),
	_T("Stain_#2 농도"),
	_T("Stain_#2 개수"),
	_T("Stain_#3 농도"),
	_T("Stain_#3 개수"),
	_T("Stain_#4 농도"),
	_T("Stain_#4 개수"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("DefectPixel_Very Hot Pixel"),
	_T("DefectPixel_Hot Pixel"),
	_T("DefectPixel_Very Bright Pixel"),
	_T("DefectPixel_Bright Pixel"),
	_T("DefectPixel_Very Dark Pixel"),
	_T("DefectPixel_Dark Pixel"),
	_T("DefectPixel_Cluster"),
	_T("DefectPixel_Dark Row"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("D/R_1 영역 - 밝기(W)"),
	_T("D/R_1 영역 - 밝기(G)"),
	_T("D/R_1 영역 - 밝기(B)"),
	_T("D/R_1 영역 - STD(W)"),
	_T("D/R_1 영역 - STD(G"),
	_T("D/R_1 영역 - STD(B)"),
	_T("D/R_1 영역 - SNR_BW"),
	_T("D/R_1 영역 - DynamicRange"),
	_T("D/R_2 영역 - 밝기(W)"),
	_T("D/R_2 영역 - 밝기(G)"),
	_T("D/R_2 영역 - 밝기(B)"),
	_T("D/R_2 영역 - STD(W)"),
	_T("D/R_2 영역 - STD(G)"),
	_T("D/R_2 영역 - STD(B)"),
	_T("D/R_2 영역 - SNR_BW"),
	_T("D/R_2 영역 - DynamicRange"),
	_T("D/R_3 영역 - 밝기(W)"),
	_T("D/R_3 영역 - 밝기(G)"),
	_T("D/R_3 영역 - 밝기(B)"),
	_T("D/R_3 영역 - STD(W)"),
	_T("D/R_3 영역 - STD(G)"),
	_T("D/R_3 영역 - STD(B)"),
	_T("D/R_3 영역 - SNR_BW"),
	_T("D/R_3 영역 - DynamicRange"),
	_T("D/R_4 영역 - 밝기(W)"),
	_T("D/R_4 영역 - 밝기(G)"),
	_T("D/R_4 영역 - 밝기(B)"),
	_T("D/R_4 영역 - STD(W)"),
	_T("D/R_4 영역 - STD(G)"),
	_T("D/R_4 영역 - STD(B"),
	_T("D/R_4 영역 - SNR_BW"),
	_T("D/R_4 영역 - DynamicRange"),
	_T("D/R_5 영역 - 밝기(W)"),
	_T("D/R_5 영역 - 밝기(G)"),
	_T("D/R_5 영역 - 밝기(B)"),
	_T("D/R_5 영역 - STD(W)"),
	_T("D/R_5 영역 - STD(G)"),
	_T("D/R_5 영역 - STD(B)"),
	_T("D/R_5 영역 - SNR_BW"),
	_T("D/R_5 영역 - DynamicRange"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Shading_Center - 밝기"),
	_T("Shading_Center - ratio"),
	_T("Shading_우상 - 밝기"),
	_T("Shading_우상 - ratio"),
	_T("Shading_우하 - 밝기"),
	_T("Shading_우하 - ratio"),
	_T("Shading_좌상 - 밝기"),
	_T("Shading_좌상 - ratio"),
	_T("Shading_좌하 - 밝기"),
	_T("Shading_좌하 - ratio"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),  //-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-10
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
};




//항목 순서
typedef enum enMesDataIndex
{
	MesDataIdx_ECurrent = 0,
	MesDataIdx_OpticalCenter,
	MesDataIdx_SFR_A,
	MesDataIdx_SFR_B,
	MesDataIdx_SFR_C,
	MesDataIdx_Rotation,
	MesDataIdx_Distortion,
	MesDataIdx_FOV,
	MesDataIdx_Stain,
	MesDataIdx_DefectPixel,
	MesDataIdx_Particle_SNR,		//dynamic Range
	MesDataIdx_Intensity,
	MesDataIdx_SNR_Light,
	MesDataIdx_Shading,
//#ifdef SET_GWANGJU
	//MesDataIdx_HotPixel,
	//MesDataIdx_FPN,
	//MesDataIdx_3DDepth,
	//MesDataIdx_EEPROM,
//#endif
	MesDataIdx_MAX,
};


//각 항목에 대한 Data 갯수
typedef enum enMesData_DataNUM 
{
	MESDataNum_ECurrent			= 5,
	MESDataNum_OpticalCenter	= 5,
	MESDataNum_SFR				= 43,//39,
	MESDataNum_Rotation			= 3,
	MESDataNum_Distortion		= 3,
	MESDataNum_FOV				= 4,
	MESDataNum_Stain			= 11 ,
	MESDataNum_DefectPixel		= 11,
	MESDataNum_Particle_SNR		= 48,
	MESDataNum_Intensity		= 15,
	MESDataNum_SNR_Light		= 56,
	MESDataNum_Shading			= 73,//광량비 Shading
	MESDataNum_HotPixel			= 3,
	MESDataNum_FPN				= 4,
	MESDataNum_3DDepth			= 4,
	MESDataNum_EEPROM			= 6,
} Mesata_DataNUM;

//항목 순서
typedef enum enMesEntryDataIndex
{
	MesEntryDataIdx_EEPROM = 0,
	MesEntryDataIdx_ECurrent,
	MesEntryDataIdx_TemperatureSensor,
	MesEntryDataIdx_SFR,
	MesEntryDataIdx_FOV,
	MesEntryDataIdx_Distortion,
	MesEntryDataIdx_3DDepth,
	MesEntryDataIdx_Particle_Entry,
	MesEntryDataIdx_Shading,
	MesEntryDataIdx_SNR_Light,
	MesEntryDataIdx_Particle_SNR,		//dynamic Range
	MesEntryDataIdx_OpticalCenter,
	MesEntryDataIdx_Rotation,
	MesEntryDataIdx_FPN,
	MesEntryDataIdx_HotPixel,
	MesEntryDataIdx_MAX,
};


//각 항목에 대한 Data 갯수
typedef enum enMesEntryData_DataNUM
{
	MESEntryDataNum_EEPROM = 6,
	MESEntryDataNum_ECurrent = 5,
	MESEntryDataNum_TemperatureSensor = 1,
	MESEntryDataNum_SFR = 39,
	MESEntryDataNum_FOV = 4,
	MESEntryDataNum_Distortion = 3,
	MESEntryDataNum_3DDepth = 4,
	MESEntryDataNum_Particle = 1,
	MESEntryDataNum_Shading = 73,//광량비 Shading
	MESEntryDataNum_SNR_Light = 56,
	MESEntryDataNum_Particle_SNR = 48,
	MESEntryDataNum_OpticalCenter = 5,
	MESEntryDataNum_Rotation = 3,
	MESEntryDataNum_FPN = 4,
	MESEntryDataNum_HotPixel = 3,
} MesEntryData_DataNUM;


typedef struct _tag_MesData
{
	CString szMesTestData[2][MesDataIdx_MAX];
	CString szMesTestLogData[2][MesDataIdx_MAX];

	int		nMesDataNum[MesDataIdx_MAX];
	CString szDataName[MesDataIdx_MAX];

	_tag_MesData()
	{
		Reset(0);
		Reset(1);

		nMesDataNum[MesDataIdx_ECurrent]		= MESDataNum_ECurrent;
		nMesDataNum[MesDataIdx_OpticalCenter]	= MESDataNum_OpticalCenter;
		nMesDataNum[MesDataIdx_Rotation]		= MESDataNum_Rotation;
		nMesDataNum[MesDataIdx_Distortion]		= MESDataNum_Distortion;
		nMesDataNum[MesDataIdx_FOV]				= MESDataNum_FOV;
		nMesDataNum[MesDataIdx_SFR_A]			= MESDataNum_SFR;
		nMesDataNum[MesDataIdx_SFR_B] = MESDataNum_SFR;
		nMesDataNum[MesDataIdx_SFR_C] = MESDataNum_SFR;
		nMesDataNum[MesDataIdx_Stain]			= MESDataNum_Stain;
		nMesDataNum[MesDataIdx_DefectPixel]		= MESDataNum_DefectPixel;
		nMesDataNum[MesDataIdx_Particle_SNR]	= MESDataNum_Particle_SNR;
		nMesDataNum[MesDataIdx_Intensity]		= MESDataNum_Intensity;
		nMesDataNum[MesDataIdx_SNR_Light]		= MESDataNum_SNR_Light;
		nMesDataNum[MesDataIdx_Shading]			= MESDataNum_Shading;

		szDataName[MesDataIdx_ECurrent]			= _T("Current");
		szDataName[MesDataIdx_OpticalCenter]	= _T("Optical Center");
		szDataName[MesDataIdx_Rotation]			= _T("Rotate");
		szDataName[MesDataIdx_Distortion]		= _T("Distortion");
		szDataName[MesDataIdx_FOV]				= _T("Fov");
		szDataName[MesDataIdx_SFR_A]			= _T("SFR_A");
		szDataName[MesDataIdx_SFR_B] = _T("SFR_B");
		szDataName[MesDataIdx_SFR_C] = _T("SFR_C");
		szDataName[MesDataIdx_Stain]			= _T("Statin");
		szDataName[MesDataIdx_DefectPixel]		= _T("DefectPixel");
		szDataName[MesDataIdx_Particle_SNR]		= _T("DynamicRange");
		szDataName[MesDataIdx_Intensity]		= _T("Shading[RI]");
		szDataName[MesDataIdx_SNR_Light]		= _T("SNR Light");
		szDataName[MesDataIdx_Shading]			= _T("Shading[SNR]");


//#ifdef SET_GWANGJU
		//nMesDataNum[MesDataIdx_HotPixel]	= MESDataNum_HotPixel;
		//nMesDataNum[MesDataIdx_FPN]			= MESDataNum_FPN;
		//nMesDataNum[MesDataIdx_3DDepth]		= MESDataNum_3DDepth;
		//nMesDataNum[MesDataIdx_EEPROM]		= MESDataNum_EEPROM;

		//szDataName[MesDataIdx_HotPixel]		= _T("HotPixel");
		//szDataName[MesDataIdx_FPN]			= _T("FPN");
		//szDataName[MesDataIdx_3DDepth]		= _T("3D_Depth");
		//szDataName[MesDataIdx_EEPROM]		= _T("EEPROM");
//#endif

	};

	void Reset(int Num)
	{
		for (int t = 0; t < MesDataIdx_MAX; t++)
		{
			szMesTestData[Num][t].Empty();
		}
	};

	_tag_MesData& operator= (_tag_MesData& ref)
	{
		for (int k = 0; k < 2; k++)
		{
			for (int t = 0; t < MesDataIdx_MAX; t++)
			{
				szMesTestData[k][t] = ref.szMesTestData[k][t];
			}
		}
		
		return *this;
	};

}ST_MES_Data, *PST_MES_Data;


typedef struct _tag_Mes_EntryData
{
	CString szMesTestData[2][MesEntryDataIdx_MAX];
	CString szMesTestLogData[2][MesEntryDataIdx_MAX];

	int		nMesDataNum[MesEntryDataIdx_MAX];
	CString szDataName[MesEntryDataIdx_MAX];

	_tag_Mes_EntryData()
	{
		Reset(0);
		Reset(1);

		nMesDataNum[MesEntryDataIdx_EEPROM] = MESEntryDataNum_EEPROM;
		nMesDataNum[MesEntryDataIdx_ECurrent] = MESEntryDataNum_ECurrent;
		nMesDataNum[MesEntryDataIdx_TemperatureSensor] = MESEntryDataNum_TemperatureSensor;
		nMesDataNum[MesEntryDataIdx_SFR] = MESEntryDataNum_SFR;
		nMesDataNum[MesEntryDataIdx_FOV] = MESEntryDataNum_FOV;
		nMesDataNum[MesEntryDataIdx_Distortion] = MESEntryDataNum_Distortion;
		nMesDataNum[MesEntryDataIdx_3DDepth] = MESEntryDataNum_3DDepth;
		nMesDataNum[MesEntryDataIdx_Particle_Entry] = MESEntryDataNum_Particle;
		nMesDataNum[MesEntryDataIdx_Shading] = MESEntryDataNum_Shading;
		nMesDataNum[MesEntryDataIdx_SNR_Light] = MESEntryDataNum_SNR_Light;
		nMesDataNum[MesEntryDataIdx_Particle_SNR] = MESEntryDataNum_Particle_SNR;
		nMesDataNum[MesEntryDataIdx_OpticalCenter] = MESEntryDataNum_OpticalCenter;
		nMesDataNum[MesEntryDataIdx_Rotation] = MESEntryDataNum_Rotation;
		nMesDataNum[MesEntryDataIdx_FPN] = MESEntryDataNum_FPN;
		nMesDataNum[MesEntryDataIdx_HotPixel] = MESEntryDataNum_HotPixel;

		szDataName[MesEntryDataIdx_EEPROM] = _T("EEPROM");
		szDataName[MesEntryDataIdx_ECurrent] = _T("Current");
		szDataName[MesEntryDataIdx_TemperatureSensor] = _T("TemperatureSensor");
		szDataName[MesEntryDataIdx_SFR] = _T("SFR");
		szDataName[MesEntryDataIdx_FOV] = _T("Fov");
		szDataName[MesEntryDataIdx_Distortion] = _T("Distortion");
		szDataName[MesEntryDataIdx_3DDepth] = _T("3D_Depth");
		szDataName[MesEntryDataIdx_Particle_Entry] = _T("Particle_Entry");
		szDataName[MesEntryDataIdx_Shading] = _T("Shading[SNR]");
		szDataName[MesEntryDataIdx_SNR_Light] = _T("SNR Light");
		szDataName[MesEntryDataIdx_Particle_SNR] = _T("DynamicRange");
		szDataName[MesEntryDataIdx_OpticalCenter] = _T("Optical Center");
		szDataName[MesEntryDataIdx_Rotation] = _T("Rotate");
		szDataName[MesEntryDataIdx_FPN] = _T("FPN");
		szDataName[MesEntryDataIdx_HotPixel] = _T("HotPixel");

	};

	void Reset(int Num)
	{
		for (int t = 0; t < MesEntryDataIdx_MAX; t++)
		{
			szMesTestData[Num][t].Empty();
		}
	};

	_tag_Mes_EntryData& operator= (_tag_Mes_EntryData& ref)
	{
		for (int k = 0; k < 2; k++)
		{
			for (int t = 0; t < MesEntryDataIdx_MAX; t++)
			{
				szMesTestData[k][t] = ref.szMesTestData[k][t];
			}
		}

		return *this;
	};

}ST_MES_Entry_Data, *PST_MES_Entry_Data;
#pragma pack(pop)

#endif // Def_FOV_h__