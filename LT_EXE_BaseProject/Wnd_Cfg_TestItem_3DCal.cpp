//*****************************************************************************
// Filename	: 	Wnd_Cfg_TestItem_3DCal.cpp
// Created	:	2017/9/24 - 16:11
// Modified	:	2017/9/24 - 16:11
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_Cfg_TestItem_3DCal.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_Cfg_TestItem_3DCal.h"

// CWnd_Cfg_TestItem_3DCal

IMPLEMENT_DYNAMIC(CWnd_Cfg_TestItem_3DCal, CWnd_BaseView)

CWnd_Cfg_TestItem_3DCal::CWnd_Cfg_TestItem_3DCal()
{
	m_InspectionType = enInsptrSysType::Sys_3D_Cal;
}

CWnd_Cfg_TestItem_3DCal::~CWnd_Cfg_TestItem_3DCal()
{
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_TestItem_3DCal, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()

//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/9/28 - 17:51
// Desc.		:
//=============================================================================
int CWnd_Cfg_TestItem_3DCal::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();
	
	UINT nID_Index = 0;

	m_tc_Option.Create(CMFCTabCtrl::STYLE_3D, rectDummy, this, 1, CMFCTabCtrl::LOCATION_BOTTOM);

	// 3D Calibration
	m_wnd_Cfg2DCal.SetOwner(GetOwner());
	m_wnd_Cfg2DCal.Create(NULL, _T("Parameter"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_wnd_Cfg3D_Depth.SetOwner(GetOwner());
	m_wnd_Cfg3D_Depth.Create(NULL, _T("Depth Parameter"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_wnd_Cfg3D_Eval.SetOwner(GetOwner());
	m_wnd_Cfg3D_Eval.Create(NULL, _T("Eval Parameter"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_wnd_Cfg3DCal_Spec.SetOwner(GetOwner());
	m_wnd_Cfg3DCal_Spec.Create(NULL, _T("Depth Spec"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_wnd_Cfg3DCal_SpecEval.SetOwner(GetOwner());
	m_wnd_Cfg3DCal_SpecEval.Create(NULL, _T("Eval Spec"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	// 검사기 별로 탭 컨트롤 정의
	SetSysAddTabCreate();

	m_tc_Option.SetActiveTab(0);
	m_tc_Option.EnableTabSwap(FALSE);
	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/9/28 - 17:52
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_3DCal::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMargin = 5;
	int iLeft	= iMargin;
	int iTop	= iMargin;
	int iWidth	= cx - iMargin - iMargin;
	int iHeight = cy - iMargin - iMargin;

	m_tc_Option.MoveWindow(iLeft, iTop, iWidth, iHeight);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: protected  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/10/21 - 11:21
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_3DCal::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (TRUE == bShow)
	{
		m_tc_Option.SetActiveTab(0);
	}
}

//=============================================================================
// Method		: OnNMClickTestItem
// Access		: protected  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/9/28 - 18:36
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_3DCal::OnNMClickTestItem(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	*pResult = 0;
}

//=============================================================================
// Method		: SetSysAddTabCreate
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/15 - 10:43
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_3DCal::SetSysAddTabCreate()
{
	UINT nItemCnt = 0;
	
	m_tc_Option.AddTab(&m_wnd_Cfg2DCal, _T("2DCAL Param"), nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_wnd_Cfg3D_Depth, _T("Depth Param"), nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_wnd_Cfg3DCal_Spec, _T("Depth Spec"), nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_wnd_Cfg3D_Eval, _T("Eval Param"), nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_wnd_Cfg3DCal_SpecEval, _T("Eval Spec"), nItemCnt++, TRUE);
}

//=============================================================================
// Method		: SetInitListCtrl
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/28 - 18:20
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_3DCal::SetInitListCtrl()
{
// 	m_lc_TestList.SetFont(&m_font_Default);
// 	m_lc_TestList.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);
// 
// 	m_lc_TestList.InsertColumn(0, _T(""),			LVCFMT_CENTER, 0);
// 	m_lc_TestList.InsertColumn(1, _T("TEST ITEMS"), LVCFMT_CENTER, 190);
// 	CString strText;
// 
// 	switch (m_InspectionType)
// 	{
// 	case Sys_Focusing:
// 		for (UINT nIdx = 0; nIdx < TI_Foc_MaxEnum; nIdx++)
// 		{
// 			m_lc_TestList.InsertItem(nIdx, _T(""));
// 			m_lc_TestList.SetItemText(nIdx, 1, g_szTestItem_Focusing[nIdx]);
// 		}
// 		break;
// 	case Sys_2D_Cal:
// 		for (UINT nIdx = 0; nIdx < TI_2D_MaxEnum; nIdx++)
// 		{
// 			m_lc_TestList.InsertItem(nIdx, _T(""));
// 			m_lc_TestList.SetItemText(nIdx, 1, g_szTestItem_2D_CAL[nIdx]);
// 		}
// 		break;
// 	case Sys_Image_Test:
// 		for (UINT nIdx = 0; nIdx < TI_ImgT_MaxEnum; nIdx++)
// 		{
// 			m_lc_TestList.InsertItem(nIdx, _T(""));
// 			m_lc_TestList.SetItemText(nIdx, 1, g_szTestItem_ImgT[nIdx]);
// 		}
// 		break;
// 	case Sys_Stereo_Cal:
// 		for (UINT nIdx = 0; nIdx < TI_Ste_MaxEnum; nIdx++)
// 		{
// 			m_lc_TestList.InsertItem(nIdx, _T(""));
// 			m_lc_TestList.SetItemText(nIdx, 1, g_szTestItem_Ste_CAL[nIdx]);
// 		}
// 		break;
// 	case Sys_3D_Cal:
// 		for (UINT nIdx = 0; nIdx < TI_3D_MaxEnum; nIdx++)
// 		{
// 			m_lc_TestList.InsertItem(nIdx, _T(""));
// 			m_lc_TestList.SetItemText(nIdx, 1, g_szTestItem_3D_CAL[nIdx]);
// 		}
// 		break;
// 	default:
// 		break;
// 	}
}

// CWnd_Cfg_TestItem_3DCal message handlers
//=============================================================================
// Method		: SetSystemType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2017/9/26 - 14:12
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_3DCal::SetSystemType(__in enInsptrSysType nSysType)
{
	m_wnd_Cfg2DCal.SetSystemType(nSysType);
	m_InspectionType = nSysType;
}

//=============================================================================
// Method		: Set_RecipeInfo
// Access		: public  
// Returns		: void
// Parameter	: __in ST_RecipeInfo * pstRecipeInfo
// Qualifier	:
// Last Update	: 2017/11/10 - 20:50
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_3DCal::Set_RecipeInfo(__in ST_RecipeInfo* pstRecipeInfo)
{
	//m_wnd_Cfg2DCal.Set_2DCal_Para(&pstRecipeInfo->st2D_CAL.Parameter);
	//m_wnd_Cfg3D_Depth.Set_3DCal_Para(&pstRecipeInfo->st3D_CAL.Depth_Param);
	//m_wnd_Cfg3D_Eval.Set_3DCal_Para(&pstRecipeInfo->st3D_CAL.Eval_Param);
	//m_wnd_Cfg3DCal_Spec.Set_TestItemInfo(&pstRecipeInfo->TestItemInfo);
	//m_wnd_Cfg3DCal_SpecEval.Set_TestItemInfo(&pstRecipeInfo->TestItemInfo);
}

//=============================================================================
// Method		: Get_RecipeInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_RecipeInfo & stOutRecipInfo
// Qualifier	:
// Last Update	: 2017/11/10 - 20:50
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_3DCal::Get_RecipeInfo(__out ST_RecipeInfo& stOutRecipInfo)
{
// 		m_wnd_Cfg2DCal.Get_2DCal_Para(stOutRecipInfo.st2D_CAL.Parameter);
// 		m_wnd_Cfg3D_Depth.Get_3DCal_Para(stOutRecipInfo.st3D_CAL.Depth_Param);
// 		m_wnd_Cfg3D_Eval.Get_3DCal_Para(stOutRecipInfo.st3D_CAL.Eval_Param);
// 		m_wnd_Cfg3DCal_Spec.Get_TestItemInfo(stOutRecipInfo.TestItemInfo);
// 		m_wnd_Cfg3DCal_SpecEval.Get_TestItemInfo(stOutRecipInfo.TestItemInfo);

}
