﻿//*****************************************************************************
// Filename	: Wnd_IO_View.cpp
// Created	: 2016/05/29
// Modified	: 2016/09/22
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************

#include "stdafx.h"
#include "Wnd_IO_View.h"
#include "Def_Digital_IO.h"

//=============================================================================
// CWnd_IO_View
//=============================================================================
IMPLEMENT_DYNAMIC(CWnd_IO_View, CWnd_BaseView)

//=============================================================================
//
//=============================================================================
CWnd_IO_View::CWnd_IO_View()
{
}

CWnd_IO_View::~CWnd_IO_View()
{
}

BEGIN_MESSAGE_MAP(CWnd_IO_View, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()

//=============================================================================
// CWnd_IO_View 메시지 처리기입니다.
//=============================================================================
//=============================================================================
// Method		: CWnd_IO_View::OnCreate
// Access		: protected 
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2015/12/6 - 15:50
// Desc.		:
//=============================================================================
int CWnd_IO_View::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();
	
	m_Wnd_IOTable.Create(NULL, _T(""), dwStyle, rectDummy, this, 10);

	return 0;
}

//=============================================================================
// Method		: CWnd_IO_View::OnSize
// Access		: protected 
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2015/12/6 - 15:50
// Desc.		:
//=============================================================================
void CWnd_IO_View::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	int iMagrin		= 5;
	int iSpacing	= 5;
	int iLeft		= iMagrin;
	int iTop		= iMagrin;
	int iWidth		= cx - iMagrin - iMagrin;
	int iHeight		= cy - iMagrin - iMagrin;

	switch (m_InspectionType)
	{
	case Sys_Focusing:
	case Sys_2D_Cal:
	case Sys_Image_Test:
	case Sys_Stereo_Cal:
	case Sys_3D_Cal:
	default:
		m_Wnd_IOTable.MoveWindow(iLeft, iTop, iWidth, iHeight);
		break;
	}
}

//=============================================================================
// Method		: CWnd_IO_View::PreCreateWindow
// Access		: virtual protected 
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2015/12/6 - 15:50
// Desc.		:
//=============================================================================
BOOL CWnd_IO_View::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS, 
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_BaseView::PreCreateWindow(cs);
}

//=============================================================================
// Method		: SetSystemType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2017/9/26 - 13:58
// Desc.		:
//=============================================================================
void CWnd_IO_View::SetSystemType(__in enInsptrSysType nSysType)
{
	m_InspectionType = nSysType;
	
	switch (m_InspectionType)
	{
	case Sys_2D_Cal:
		m_Wnd_IOTable.Set_DIO_Count(DI_2D_MaxEnum, DO_2D_MaxEnum);
		m_Wnd_IOTable.Set_DIO_Name(g_szDI_2DCal, g_szDO_2DCal);
		break;
	case Sys_3D_Cal:
		break;
	case Sys_Stereo_Cal:
		m_Wnd_IOTable.Set_DIO_Count(DI_St_MaxEnum, DO_St_MaxEnum);
		m_Wnd_IOTable.Set_DIO_Name(g_szDI_Stereo, g_szDO_Stereo);
		break;
	case Sys_Focusing:
		m_Wnd_IOTable.Set_DIO_Count(DI_Fo_MaxEnum, DO_Fo_MaxEnum);
		m_Wnd_IOTable.Set_DIO_Name(g_szDI_Focusing, g_szDO_Focusing);
		break;
	case Sys_Image_Test:
		m_Wnd_IOTable.Set_DIO_Count(DI_ImgT_MaxEnum, DO_ImgT_MaxEnum);
		m_Wnd_IOTable.Set_DIO_Name(g_szDI_ImageTest, g_szDO_ImageTest);
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: SetPtr_Device
// Access		: public  
// Returns		: void
// Parameter	: __in ST_Device * pstDevice
// Qualifier	:
// Last Update	: 2018/1/18 - 16:53
// Desc.		:
//=============================================================================
void CWnd_IO_View::SetPtr_Device(__in ST_Device* pstDevice)
{
	if (pstDevice == NULL)
		return;

	m_pDevice = pstDevice;
	m_Wnd_IOTable.SetPtr_Device(&m_pDevice->DigitalIOCtrl);
}

//=============================================================================
// Method		: Set_IO_DI_OffsetData
// Access		: public  
// Returns		: void
// Parameter	: __in BYTE byBitOffset
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2017/10/31 - 17:00
// Desc.		:
//=============================================================================
void CWnd_IO_View::Set_IO_DI_OffsetData(__in BYTE byBitOffset, __in BOOL bOnOff)
{
	m_Wnd_IOTable.Set_IO_DI_OffsetData(byBitOffset, bOnOff);
}

//=============================================================================
// Method		: Set_IO_DI_Data
// Access		: public  
// Returns		: void
// Parameter	: __in LPBYTE lpbyDIData
// Parameter	: __in UINT nCount
// Qualifier	:
// Last Update	: 2018/2/14 - 16:58
// Desc.		:
//=============================================================================
void CWnd_IO_View::Set_IO_DI_Data(__in LPBYTE lpbyDIData, __in UINT nCount)
{
	m_Wnd_IOTable.Set_IO_DI_Data(lpbyDIData, nCount);
}

//=============================================================================
// Method		: Set_IO_DO_OffsetData
// Access		: public  
// Returns		: void
// Parameter	: __in BYTE byBitOffset
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2018/2/14 - 16:58
// Desc.		:
//=============================================================================
void CWnd_IO_View::Set_IO_DO_OffsetData(__in BYTE byBitOffset, __in BOOL bOnOff)
{
	m_Wnd_IOTable.Set_IO_DO_OffsetData(byBitOffset, bOnOff);
}

//=============================================================================
// Method		: Set_IO_DO_Data
// Access		: public  
// Returns		: void
// Parameter	: __in LPBYTE lpbyDOData
// Parameter	: __in UINT nCount
// Qualifier	:
// Last Update	: 2018/2/14 - 16:58
// Desc.		:
//=============================================================================
void CWnd_IO_View::Set_IO_DO_Data(__in LPBYTE lpbyDOData, __in UINT nCount)
{
	m_Wnd_IOTable.Set_IO_DO_Data(lpbyDOData, nCount);
}