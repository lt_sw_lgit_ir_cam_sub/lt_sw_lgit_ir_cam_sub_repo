﻿//*****************************************************************************
// Filename	: 	TestManager_EQP_3DCal.cpp
// Created	:	2016/5/9 - 13:32
// Modified	:	2016/08/10
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "TestManager_EQP_3DCal.h"
#include "CommonFunction.h"
#include "File_Recipe.h"
#include "Def_Digital_IO.h"

#include <Mmsystem.h>
#pragma comment (lib,"winmm.lib")

CTestManager_EQP_3DCal::CTestManager_EQP_3DCal()
{
	OnInitialize();
}

CTestManager_EQP_3DCal::~CTestManager_EQP_3DCal()
{
	TRACE(_T("<<< Start ~CTestManager_EQP_3DCal >>> \n"));

	this->OnFinalize();

	TRACE(_T("<<< End ~CTestManager_EQP_3DCal >>> \n"));
}

//=============================================================================
// Method		: OnLoadOption
// Access		: virtual protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/9/28 - 20:04
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP_3DCal::OnLoadOption()
{
	BOOL bReturn = CTestManager_EQP::OnLoadOption();



	return bReturn;
}

//=============================================================================
// Method		: InitDevicez
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in HWND hWndOwner
// Qualifier	:
// Last Update	: 2017/11/11 - 21:37
// Desc.		:
//=============================================================================
void CTestManager_EQP_3DCal::InitDevicez(__in HWND hWndOwner /*= NULL*/)
{
	CTestManager_EQP::InitDevicez(hWndOwner);

}

//=============================================================================
// Method		: StartOperation_LoadUnload
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:19
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::StartOperation_LoadUnload(__in BOOL bLoad)
{
	LRESULT lResult = RC_OK;
	lResult = CTestManager_EQP::StartOperation_LoadUnload(bLoad);



	return lResult;
}

//=============================================================================
// Method		: StartOperation_Inspection
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:21
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::StartOperation_Inspection(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lResult = RC_OK;
	lResult = CTestManager_EQP::StartOperation_Inspection(nParaIdx);



	return lResult;
}

//=============================================================================
// Method		: StartOperation_Manual
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bLoad
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:22
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::StartOperation_Manual(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lResult = RC_OK;
	lResult = CTestManager_EQP::StartOperation_Manual(nStepIdx, nParaIdx);



	return lResult;
}

//=============================================================================
// Method		: StartOperation_InspManual
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/20 - 11:15
// Desc.		: 이미지 검사 개별 테스트용
//=============================================================================
LRESULT CTestManager_EQP_3DCal::StartOperation_InspManual(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lResult = RC_OK;
	lResult = CTestManager_EQP::StartOperation_InspManual(nParaIdx);



	return lResult;
}

//=============================================================================
// Method		: StopProcess_LoadUnload
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/19 - 19:35
// Desc.		:
//=============================================================================
void CTestManager_EQP_3DCal::StopProcess_LoadUnload()
{
	CTestManager_EQP::StopProcess_LoadUnload();

}

//=============================================================================
// Method		: StopProcess_Test_All
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/10 - 21:14
// Desc.		:
//=============================================================================
void CTestManager_EQP_3DCal::StopProcess_Test_All()
{
	CTestManager_EQP::StopProcess_Test_All();

}

//=============================================================================
// Method		: StopProcess_InspManual
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/20 - 11:15
// Desc.		:
//=============================================================================
void CTestManager_EQP_3DCal::StopProcess_InspManual(__in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::StopProcess_InspManual(nParaIdx);

}

//=============================================================================
// Method		: AutomaticProcess_LoadUnload
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:23
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP_3DCal::AutomaticProcess_LoadUnload(__in BOOL bLoad)
{
	BOOL bResult = TRUE;
	bResult = CTestManager_EQP::AutomaticProcess_LoadUnload(bLoad);



	return bResult;
}

//=============================================================================
// Method		: AutomaticProcess_Test_All
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:26
// Desc.		:
//=============================================================================
void CTestManager_EQP_3DCal::AutomaticProcess_Test_All(__in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::AutomaticProcess_Test_All(nParaIdx);


}

//=============================================================================
// Method		: AutomaticProcess_Test_InspManual
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/15 - 3:06
// Desc.		:
//=============================================================================
void CTestManager_EQP_3DCal::AutomaticProcess_Test_InspManual(__in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::AutomaticProcess_Test_InspManual(nParaIdx);


}

//=============================================================================
// Method		: OnInitial_LoadUnload
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:28
// Desc.		:
//=============================================================================
void CTestManager_EQP_3DCal::OnInitial_LoadUnload(__in BOOL bLoad)
{
	CTestManager_EQP::OnInitial_LoadUnload(bLoad);


}

//=============================================================================
// Method		: OnFinally_LoadUnload
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:29
// Desc.		:
//=============================================================================
void CTestManager_EQP_3DCal::OnFinally_LoadUnload(__in BOOL bLoad)
{
	CTestManager_EQP::OnFinally_LoadUnload(bLoad);


}

//=============================================================================
// Method		: OnStart_LoadUnload
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:30
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::OnStart_LoadUnload(__in BOOL bLoad)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnStart_LoadUnload(bLoad);



	return lReturn;
}

//=============================================================================
// Method		: OnInitial_Test_All
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 19:36
// Desc.		:
//=============================================================================
void CTestManager_EQP_3DCal::OnInitial_Test_All(__in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::OnInitial_Test_All(nParaIdx);


}

//=============================================================================
// Method		: OnFinally_Test_All
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in LRESULT lResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/23 - 13:05
// Desc.		:
//=============================================================================
void CTestManager_EQP_3DCal::OnFinally_Test_All(__in LRESULT lResult, __in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::OnFinally_Test_All(lResult, nParaIdx);


}

//=============================================================================
// Method		: OnStart_Test_All
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:30
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::OnStart_Test_All(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnStart_Test_All(nParaIdx);



	return lReturn;
}

//=============================================================================
// Method		: OnInitial_Test_InspManual
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/8 - 9:11
// Desc.		:
//=============================================================================
void CTestManager_EQP_3DCal::OnInitial_Test_InspManual(__in UINT nTestItemID, __in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::OnInitial_Test_InspManual(nTestItemID, nParaIdx);


}

//=============================================================================
// Method		: OnFinally_Test_InspManual
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nTestItemID
// Parameter	: __in LRESULT lResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/8 - 9:12
// Desc.		:
//=============================================================================
void CTestManager_EQP_3DCal::OnFinally_Test_InspManual(__in UINT nTestItemID, __in LRESULT lResult, __in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::OnFinally_Test_InspManual(nTestItemID, lResult, nParaIdx);


}

//=============================================================================
// Method		: OnStart_Test_InspManual
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/15 - 3:05
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::OnStart_Test_InspManual(__in UINT nTestItemID, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnStart_Test_InspManual(nTestItemID, nParaIdx);


	return lReturn;
}

//=============================================================================
// Method		: Load_Product
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::Load_Product()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::Load_Product();



	return lReturn;
}

//=============================================================================
// Method		: Unload_Product
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::Unload_Product()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::Unload_Product();



	return lReturn;
}

LRESULT CTestManager_EQP_3DCal::StartTest_Equipment(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = StartTest_3D_CAL();

	return lReturn;
}

//=============================================================================
// Method		: StartTest_3D_CAL
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/14 - 19:41
// Desc.		:
//  [검사동작 시퀀스]
// 1. PC -> CAN 프로토콜 전송[3D Cal 모드 진입]
// 2. Y 축 움직여 거리별로 측정
//    700mm -> 600 -> 500 -> 400 -> 300 -> 200 -> Reboot(부팅5초 소요)
//    -> 250 -> 450(측정&광량비측정) -> 650 -> 홈(Dummy Hand 검사)
// 3. 양/불 판정
// 4. Production mode 삭제
// 5. Reboot
// 6. Sleep mode Check
// 7. DTC Check
//=============================================================================
LRESULT CTestManager_EQP_3DCal::StartTest_3D_CAL(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;
	INT_PTR iStepCnt = m_stInspInfo.RecipeInfo.StepInfo.GetCount();
	DWORD	dwElapTime = 0;
	UINT	nDepthStepIdx = 0;
	UINT	nEvalStepIdx = 0;

	// * 설정된 스텝 진행
	for (INT nStepIdx = 0; nStepIdx < iStepCnt; nStepIdx++)
	{
		// 스텝 진행 시간 체크
		dwElapTime = Check_TimeStepStart();

		// UI 스텝 선택
		OnSet_TestStepSelect(nStepIdx, nParaIdx);

		// * 팔레트 전진/후진
		if (TRUE == pStepInfo->StepList[nStepIdx].bUseMoveY)
		{
			// * Y축 이동
			lReturn = OnMotion_3DCAL_Test(pStepInfo->StepList[nStepIdx].nMoveY, nParaIdx);
		}

		// * 검사 시작
		if (OpMode_DryRun != m_stInspInfo.OperateMode)
		{
			if (TRUE == pStepInfo->StepList[nStepIdx].bTest)
			{
				// 알파값 조정??
				if (pStepInfo->StepList[nStepIdx].nTestItem == TI_3D_Depth_Alpha_Capture)
				{
					lReturn = StartTest_3D_CAL_TestItem(pStepInfo->StepList[nStepIdx].nTestItem, nStepIdx, pStepInfo->StepList[nStepIdx].wAlpha, nDepthStepIdx++, nParaIdx);
					Sleep(DELAY_3DEPTH);
					lReturn = StartTest_3D_CAL_TestItem(pStepInfo->StepList[nStepIdx].nTestItem, nStepIdx, pStepInfo->StepList[nStepIdx].wAlpha_2nd, nDepthStepIdx++, nParaIdx);
				}
				else if (pStepInfo->StepList[nStepIdx].nTestItem == TI_3D_Eval_Alpha_Capture)
				{
					lReturn = StartTest_3D_CAL_TestItem(pStepInfo->StepList[nStepIdx].nTestItem, nStepIdx, pStepInfo->StepList[nStepIdx].wAlpha, nEvalStepIdx++, nParaIdx);
					Sleep(DELAY_3DEPTH);
					lReturn = StartTest_3D_CAL_TestItem(pStepInfo->StepList[nStepIdx].nTestItem, nStepIdx, pStepInfo->StepList[nStepIdx].wAlpha_2nd, nEvalStepIdx++, nParaIdx);
				}
				else
				{
					lReturn = StartTest_3D_CAL_TestItem(pStepInfo->StepList[nStepIdx].nTestItem, nStepIdx, 0, 0, nParaIdx);
				}
			}
		}

		// 스텝 진행 시간 체크
		dwElapTime = Check_TimeStepStop(dwElapTime);

		// 진행 시간 설정
		m_stInspInfo.Set_ElapTime(nParaIdx, nStepIdx, dwElapTime);

		// 스텝 결과 UI에 표시
		OnSet_TestStepResult(nStepIdx, nParaIdx);

		// * 검사 프로그레스		
		OnSet_TestProgressStep((UINT)iStepCnt, nStepIdx + 1);

		// UI 갱신 딜레이
		Sleep(100);

		//Sleep(200);
		// 스텝 딜레이
		if (0 < pStepInfo->StepList[nStepIdx].dwDelay)
		{
			Sleep(pStepInfo->StepList[nStepIdx].dwDelay);
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: AutomaticProcess_TestItem
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nCornerStep
// Qualifier	:
// Last Update	: 2017/12/12 - 13:31
// Desc.		:
//=============================================================================
void CTestManager_EQP_3DCal::AutomaticProcess_TestItem(__in UINT nParaIdx, __in UINT nTestItemID, __in UINT nStepIdx, __in UINT nCornerStep /*= 0*/)
{
	// 	if (TI_2D_Detect_CornerExt == nTestItemID)
	// 	{
	// 		LRESULT lReturn = _TI_2DCAL_CornerExt(nStepIdx, nCornerStep, nParaIdx);
	// 	}
}

//=============================================================================
// Method		: StartTest_3D_CAL_TestItem
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nStepIdx
// Parameter	: __in WORD wAlpha
// Parameter	: __in UINT nCornerStep
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/6 - 16:57
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::StartTest_3D_CAL_TestItem(__in UINT nTestItemID, __in UINT nStepIdx, __in WORD wAlpha /*= 0*/, __in UINT nCornerStep /*= 0*/, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	switch (nTestItemID)
	{
	case TI_3D_Send_2DCalCornerParam:
		lReturn = _TI_3DCAL_SetParameter(nStepIdx, nParaIdx);
		break;

	case TI_3D_Send_Depth_Param:
		lReturn = _TI_3DCAL_Depth_SendParameter(nStepIdx, nParaIdx);
		break;

	case TI_3D_Depth_Alpha_Capture:
		Sleep(100);
		lReturn = _TI_3DCAL_Depth_AlphaCapture(nStepIdx, wAlpha, nCornerStep, nParaIdx);
		break;

	case TI_3D_Depth_Result:
		lReturn = _TI_3DCAL_Depth_Result(nStepIdx, nParaIdx);
		break;

	case TI_3D_Send_Eval_Param:
		lReturn = _TI_3DCAL_Eval_SendParameter(nStepIdx, nParaIdx);
		break;

	case TI_3D_Eval_Alpha_Capture:
		Sleep(100);
		lReturn = _TI_3DCAL_Eval_AlphaCapture(nStepIdx, wAlpha, nCornerStep, nParaIdx);
		break;

	case TI_3D_Eval_Result:
		lReturn = _TI_3DCAL_Eval_Result(nStepIdx, nParaIdx);
		break;

	case TI_3D_Re_Depth_DistCoef5:
	case TI_3D_Re_Depth_TotalFittingError:
	case TI_3D_Re_Depth_AccracyMap0_17:
	case TI_3D_Re_Depth_AccracyMap18_29:
	case TI_3D_Re_Depth_InvalidRatio:
	case TI_3D_Re_Depth_FailureCnt:
		lReturn = _TI_3DCAL_Depth_JudgeResult(nStepIdx, nTestItemID, nParaIdx);
		break;

	case TI_3D_Re_Eval_Chess0:
	case TI_3D_Re_Eval_Chess1:
	case TI_3D_Re_Eval_Chess2:
	case TI_3D_Re_Eval_TofDist0:
	case TI_3D_Re_Eval_TofDist1:
	case TI_3D_Re_Eval_TofDist2:
	case TI_3D_Re_Eval_Error:
	case TI_3D_Re_Eval_ShadingH0:
	case TI_3D_Re_Eval_ShadingV0:
	case TI_3D_Re_Eval_ShadingH1:
	case TI_3D_Re_Eval_ShadingV1:
	case TI_3D_Re_Eval_FailureCnt:
	case TI_3D_Re_Eval_IntensityC:
		lReturn = _TI_3DCAL_Eval_JudgeResult(nStepIdx, nTestItemID, nParaIdx);
		break;

	default:
		break;
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_Initialize
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/6 - 21:56
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::_TI_Cm_Initialize(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_Initialize(nStepIdx, nParaIdx);



	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_Finalize
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/13 - 13:54
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::_TI_Cm_Finalize(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_Finalize(nStepIdx, nParaIdx);



	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_MeasurCurrent
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/13 - 13:54
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::_TI_Cm_MeasurCurrent(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_MeasurCurrent(nStepIdx, nParaIdx);



	return lReturn;
}

LRESULT CTestManager_EQP_3DCal::_TI_Cm_MeasurVoltage(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_MeasurVoltage(nStepIdx, nParaIdx);



	return lReturn;
}

LRESULT CTestManager_EQP_3DCal::_TI_Cm_CaptureImage(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/, __in BOOL bSaveImage /*= FALSE*/, __in LPCTSTR szFileName /*= NULL*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_CaptureImage(nStepIdx, nParaIdx, bSaveImage, szFileName);



	return lReturn;
}

LRESULT CTestManager_EQP_3DCal::_TI_Cm_CameraRegisterSet(__in UINT nStepIdx, __in UINT nRegisterType, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_CameraRegisterSet(nStepIdx, nRegisterType, nParaIdx);



	return lReturn;
}

LRESULT CTestManager_EQP_3DCal::_TI_Cm_CameraAlphaSet(__in UINT nStepIdx, __in UINT nAlpha, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_CameraAlphaSet(nStepIdx, nAlpha, nParaIdx);



	return lReturn;
}

LRESULT CTestManager_EQP_3DCal::_TI_Cm_CameraPowerOnOff(__in UINT nStepIdx, __in enPowerOnOff nOnOff, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_CameraPowerOnOff(nStepIdx, nOnOff, nParaIdx);



	return lReturn;
}

//=============================================================================
// Method		: _TI_3DCAL_SetParameter
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/20 - 11:26
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::_TI_3DCAL_SetParameter(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	return lReturn;
}

//=============================================================================
// Method		: _TI_3DCAL_CornerExt
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nCornerStep
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/15 - 2:33
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::_TI_3DCAL_CornerExt(__in UINT nStepIdx, __in UINT nCornerStep, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	return lReturn;
}


//=============================================================================
// Method		: _TI_3DCAL_Depth_SendParameter
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/15 - 2:33
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::_TI_3DCAL_Depth_SendParameter(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	// *** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;

	// CAN Init가 되어 있어야 함 (Loading 루틴에서 처리)

	// 파라미터 전송	
	//lReturn = OnCAN_3DCal_SendParameters(nParaIdx);

	// 스텝 결과
	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}


//=============================================================================
// Method		: _TI_3DCAL_Depth_AlphaCapture
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in WORD wAlpha
// Parameter	: __in UINT nCornerStep
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/15 - 2:33
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::_TI_3DCAL_Depth_AlphaCapture(__in UINT nStepIdx, __in WORD wAlpha, __in UINT nCornerStep, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	// *** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;

	// 알파 설정
	//lReturn = OnCAN_SetAlpha(wAlpha, enCapEqpType::Capt_3D_DEPT, (enStepIndex)nCornerStep, nParaIdx);

	Sleep(300);
	if (RC_OK == lReturn)
	{
		// 이미지 캡쳐 요청
		//lReturn = OnCAN_ReqImageCapture((enStepIndex)nCornerStep, TRUE, nParaIdx);
	}

	// 스텝 결과
	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_3DCAL_Depth_Result
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/15 - 2:26
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::_TI_3DCAL_Depth_Result(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	// *** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;

	// 결과값 요청
	//lReturn = OnCAN_3DCal_RequestResult((enStepIndex)0, nParaIdx);

	// 스텝 결과
	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_3DCAL_Depth_JudgeResult
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nTestItem
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/14 - 16:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::_TI_3DCAL_Depth_JudgeResult(__in UINT nStepIdx, __in UINT nTestItem, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	CArray <COleVariant, COleVariant&> VarArray;
	COleVariant varResult;

	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[nTestItem].vt);

	// 	switch (nTestItem)
	// 	{
	// 	case TI_3D_Re_Depth_DistCoef5:
	// 		{
	// 		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].st3D_Depth.fArfDistCoef[5];
	// 			VarArray.Add(varResult);
	// 		}
	// 		break;
	// 
	// 	case TI_3D_Re_Depth_TotalFittingError:
	// 	{
	// 		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].st3D_Depth.fTotalFittingError;
	// 		VarArray.Add(varResult);
	// 	}
	// 		break;
	// 
	// 	case TI_3D_Re_Depth_AccracyMap0_17:
	// 	{
	// 		double dbAvg = 0.0;
	// 		double dbSum = 0.0;
	// 		for (UINT nIdx = 0; nIdx < 18; nIdx++)
	// 		{
	// 			if (nIdx != 5)
	// 			{
	// 				dbSum += m_stInspInfo.CamInfo[nParaIdx].st3D_Depth.fArtAccracyMap[nIdx];
	// 			}
	// 		}
	// 
	// 		dbAvg = dbSum / 17;
	// 
	// 		varResult.dblVal = dbAvg;  
	// 		VarArray.Add(varResult);
	// 	}
	// 	break;
	// 
	// 	case TI_3D_Re_Depth_AccracyMap18_29:
	// 	{
	// 		double dbAvg = 0.0;
	// 		double dbSum = 0.0;
	// 		for (UINT nIdx = 18; nIdx < 30; nIdx++)
	// 		{
	// 			dbSum += m_stInspInfo.CamInfo[nParaIdx].st3D_Depth.fArtAccracyMap[nIdx];
	// 		}
	// 
	// 		dbAvg = dbSum / 12;
	// 
	// 		varResult.dblVal = dbAvg;  
	// 		VarArray.Add(varResult);
	// 	}
	// 	break;
	// 
	// 	case TI_3D_Re_Depth_InvalidRatio:
	// 	{
	// 		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].st3D_Depth.fInvalidRatio;
	// 		VarArray.Add(varResult);
	// 	}
	// 		break;
	// 
	// 	case TI_3D_Re_Depth_FailureCnt:
	// 	{
	// 		varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].st3D_Depth.nFailureCnt;
	// 		VarArray.Add(varResult);
	// 	}
	// 		break;
	// 
	// 	default:
	// 		break;
	// 	}

	m_stInspInfo.Set_Measurement(nParaIdx, nStepIdx, (UINT)VarArray.GetCount(), VarArray.GetData());

	return lReturn;
}

LRESULT CTestManager_EQP_3DCal::_TI_3DCAL_Eval_SendParameter(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	// *** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;

	// CAN Init가 되어 있어야 함 (Loading 루틴에서 처리)

	// 파라미터 전송	
	//lReturn = OnCAN_3DCal_EvalSendParameters(nParaIdx);

	// 스텝 결과
	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

LRESULT CTestManager_EQP_3DCal::_TI_3DCAL_Eval_AlphaCapture(__in UINT nStepIdx, __in WORD wAlpha, __in UINT nCornerStep, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	// *** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;

	// 알파 설정
	//lReturn = OnCAN_SetAlpha(wAlpha, enCapEqpType::Capt_3D_EVAL, (enStepIndex)nCornerStep, nParaIdx);

	Sleep(300);
	if (RC_OK == lReturn)
	{
		// 이미지 캡쳐 요청
		//lReturn = OnCAN_ReqImageCapture((enStepIndex)nCornerStep, FALSE, nParaIdx);
	}

	// 스텝 결과
	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

LRESULT CTestManager_EQP_3DCal::_TI_3DCAL_Eval_Result(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	// *** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;

	// 결과값 요청
	//lReturn = OnCAN_3DCal_EvalRequestResult((enStepIndex)0, nParaIdx);

	// 스텝 결과
	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_3DCAL_Eval_JudgeResult
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nTestItem
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/14 - 16:51
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::_TI_3DCAL_Eval_JudgeResult(__in UINT nStepIdx, __in UINT nTestItem, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;
	CArray <COleVariant, COleVariant&> VarArray;
	COleVariant varResult;

	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[nTestItem].vt);

	// 	switch (nTestItem)
	// 	{
	// 	case TI_3D_Re_Eval_Chess0:
	// 	{
	// 		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].st3D_Eval.arstEval[2].fChess;
	// 		VarArray.Add(varResult);
	// 	}
	// 		break;
	// 
	// 	case TI_3D_Re_Eval_Chess1:
	// 		{
	// 		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].st3D_Eval.arstEval[1].fChess;
	// 			VarArray.Add(varResult);
	// 		}
	// 		break;
	// 
	// 	case TI_3D_Re_Eval_Chess2:
	// 	{
	// 		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].st3D_Eval.arstEval[0].fChess;
	// 		VarArray.Add(varResult);
	// 	}
	// 		break;
	// 
	// 	case TI_3D_Re_Eval_TofDist0:
	// 	{
	// 		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].st3D_Eval.arstEval[2].fTofDist;
	// 		VarArray.Add(varResult);
	// 	}
	// 	break;
	// 
	// 	case TI_3D_Re_Eval_TofDist1:
	// 	{
	// 		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].st3D_Eval.arstEval[1].fTofDist;
	// 		VarArray.Add(varResult);
	// 	}
	// 	break;
	// 
	// 	case TI_3D_Re_Eval_TofDist2:
	// 	{
	// 		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].st3D_Eval.arstEval[0].fTofDist;
	// 		VarArray.Add(varResult);
	// 	}
	// 	break;
	// 
	// 	case TI_3D_Re_Eval_Error:
	// 	{
	// 		for (UINT nIdx = 0; nIdx < MAX_EVAL_Re_Error; nIdx++)
	// 		{
	// 			for (UINT nErrIdx = 0; nErrIdx < MAX_EVAL_ARFERROR; nErrIdx++)
	// 			{
	// 				varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].st3D_Eval.arstEval[nIdx].arfError[nErrIdx];
	// 				VarArray.Add(varResult);
	// 			}
	// 		}
	// 	}
	// 	break;
	// 
	// 	case TI_3D_Re_Eval_ShadingH0:
	// 	{
	// 		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].st3D_Eval.arstShadingH[0].fValue;
	// 		VarArray.Add(varResult);
	// 	}
	// 	break;
	// 
	// 	case TI_3D_Re_Eval_ShadingV0:
	// 	{
	// 		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].st3D_Eval.arstShadingV[0].fValue;
	// 		VarArray.Add(varResult);
	// 	}
	// 	break;
	// 
	// 	case TI_3D_Re_Eval_ShadingH1:
	// 	{
	// 			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].st3D_Eval.arstShadingH[1].fValue;
	// 			VarArray.Add(varResult);
	// 	}
	// 		break;
	// 
	// 	case TI_3D_Re_Eval_ShadingV1:
	// 	{
	// 			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].st3D_Eval.arstShadingV[1].fValue;
	// 			VarArray.Add(varResult);
	// 	}
	// 		break;
	// 
	// 	case TI_3D_Re_Eval_FailureCnt:
	// 	{
	// 		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].st3D_Eval.nFailureCnt;
	// 		VarArray.Add(varResult);
	// 	}
	// 		break;
	// 
	// 	case TI_3D_Re_Eval_IntensityC:
	// 	{
	// 		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].st3D_Eval.fIntensityC;
	// 		VarArray.Add(varResult);
	// 	}
	// 		break;
	// 
	// 	default:
	// 		break;
	// 	}

	m_stInspInfo.Set_Measurement(nParaIdx, nStepIdx, (UINT)VarArray.GetCount(), VarArray.GetData());

	return lReturn;
}

//=============================================================================
// Method		: MES_CheckBarcodeValidation
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/4 - 16:25
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::MES_CheckBarcodeValidation(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::MES_CheckBarcodeValidation(nParaIdx);



	return lReturn;
}

//=============================================================================
// Method		: MES_SendInspectionData
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/4 - 16:39
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::MES_SendInspectionData(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	if (enOperateMode::OpMode_Production == m_stInspInfo.OperateMode)
	{
		lReturn = MES_MakeInspecData_3D_CAL(nParaIdx);


		// MES 로그 남기기

	}


	return lReturn;
}

//=============================================================================
// Method		: MES_MakeInspecData_3D_CAL
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/9 - 19:17
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::MES_MakeInspecData_3D_CAL(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;


	return lReturn;
}

//=============================================================================
// Method		: OnPopupMessageTest_All
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enResultCode nResultCode
// Qualifier	:
// Last Update	: 2016/7/21 - 18:59
// Desc.		:
//=============================================================================
void CTestManager_EQP_3DCal::OnPopupMessageTest_All(__in enResultCode nResultCode)
{
	CTestManager_EQP::OnPopupMessageTest_All(nResultCode);


}

//=============================================================================
// Method		: OnMotion_MoveToTestPos
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/10/19 - 20:42
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::OnMotion_MoveToTestPos(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_MoveToTestPos(nParaIdx);



	return lReturn;
}

//=============================================================================
// Method		: OnMotion_MoveToStandbyPos
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/15 - 9:58
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::OnMotion_MoveToStandbyPos(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_MoveToStandbyPos(nParaIdx);



	return lReturn;
}

LRESULT CTestManager_EQP_3DCal::OnMotion_MoveToUnloadPos(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_MoveToUnloadPos(nParaIdx);



	return lReturn;
}

//=============================================================================
// Method		: OnMotion_Origin
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/11/15 - 3:27
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::OnMotion_Origin()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_Origin();



	return lReturn;
}

//=============================================================================
// Method		: OnMotion_LoadProduct
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/11/15 - 3:05
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::OnMotion_LoadProduct()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_LoadProduct();



	return lReturn;
}

//=============================================================================
// Method		: OnMotion_UnloadProduct
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/11/15 - 3:16
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::OnMotion_UnloadProduct()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_UnloadProduct();



	return lReturn;
}

//=============================================================================
// Method		: OnMotion_3DCAL_Test
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in double dbDistanceY
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/15 - 3:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::OnMotion_3DCAL_Test(__in double dbDistanceY, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

#ifndef MOTION_NOT_USE
	// Distance Accuray (450mm, 600mm)
	// 100 ~ 800mm

	// * 모터 점검

	// * 로딩된 팔레트가 있는가?

	// * 로딩된 팔레트 위치 체크

	// * 더미 핸드 위치 체크 (충돌 방지)

	// * 정해진 거리 위치로 팔레트 이동


	lReturn = m_MotionSequence.OnActionTesting(nParaIdx, 0, dbDistanceY);

#endif

	return lReturn;
}

//=============================================================================
// Method		: OnMotion_3DCAL_Hand
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bHandIn
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/15 - 3:32
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::OnMotion_3DCAL_Hand(__in BOOL bHandIn, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

#ifndef MOTION_NOT_USE
	// * 모터 점검

	// * 로딩된 팔레트 위치 체크 (충돌 방지)

	// * 더미 핸드 이동

	if (bHandIn) // 더미 핸드 투입
	{
		//		m_MotionSequence.OnActionTestingDummy(nParaIdx);
	}
	else // 더미 핸드 배출
	{
		//		m_MotionSequence.OnActionDummy(FALSE);
	}
#endif

	return lReturn;
}

//=============================================================================
// Method		: OnDIn_DetectSignal
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BYTE byBitOffset
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2018/2/1 - 17:22
// Desc.		:
//=============================================================================
void CTestManager_EQP_3DCal::OnDIn_DetectSignal(__in BYTE byBitOffset, __in BOOL bOnOff)
{
	CTestManager_EQP::OnDIn_DetectSignal(byBitOffset, bOnOff);
	//OnDIn_DetectSignal_SteCal(byBitOffset, bOnOff);


}

void CTestManager_EQP_3DCal::OnDIn_DetectSignal_SteCal(__in BYTE byBitOffset, __in BOOL bOnOff)
{
	// 	enDI_2DCal nDI_Signal = (enDI_2DCal)byBitOffset;
	// 
	// 	switch (nDI_Signal)
	// 	{
	// 	case DI_2D_00_MainPower:
	// 		if (FALSE == bOnOff)
	// 		{
	// 			OnDIn_MainPower();
	// 		}
	// 		break;
	// 
	// 	case DI_2D_01_EMO:
	// 		if (FALSE == bOnOff)
	// 		{
	// 			OnDIn_EMO();
	// 		}
	// 		break;
	// 
	// 	case DI_2D_02_AreaSensor:
	// 		if (FALSE == bOnOff)
	// 		{
	// 			OnDIn_AreaSensor();
	// 		}
	// 		break;
	// 
	// 	case DI_2D_03_DoorSensor:
	// 		if (FALSE == bOnOff)
	// 		{
	// 			OnDIn_DoorSensor();
	// 		}
	// 		break;
	// 
	// 	case DI_2D_04_JIG_CoverCheck:
	// 		if (bOnOff)
	// 		{
	// 			OnDIn_JIGCoverCheck();
	// 		}
	// 		break;
	// 	
	// 	case DI_2D_06_Start:
	// 	{
	// 		if (bOnOff)
	// 		{
	// 			//StartOperation_AutoAll();
	// 			StartOperation_LoadUnload(TRUE);
	// 		}
	// 		
	// 	}
	// 		break;
	// 
	// 	case DI_2D_07_Stop:
	// 	{
	// 		if (bOnOff)
	// 		{
	// 			StopProcess_Test_All();
	// 		}
	// 
	// 	}
	// 		break;
	// 	
	// 	default:
	// 		break;
	// 	}
	// 
}

void CTestManager_EQP_3DCal::OnDIO_InitialSignal()
{
	CTestManager_EQP::OnDIO_InitialSignal();


}

//=============================================================================
// Method		: OnDIn_MainPower
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 19:48
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::OnDIn_MainPower()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_MainPower();



	return lReturn;
}

//=============================================================================
// Method		: OnDIn_EMO
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 20:12
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::OnDIn_EMO()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_EMO();



	return lReturn;
}

//=============================================================================
// Method		: OnDIn_AreaSensor
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 20:12
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::OnDIn_AreaSensor()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_AreaSensor();



	return lReturn;
}

//=============================================================================
// Method		: OnDIn_DoorSensor
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 20:12
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::OnDIn_DoorSensor()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_DoorSensor();



	return lReturn;
}

//=============================================================================
// Method		: OnDIn_JIGCoverCheck
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 19:41
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::OnDIn_JIGCoverCheck()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_JIGCoverCheck();



	return lReturn;
}

LRESULT CTestManager_EQP_3DCal::OnDIn_Start()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_Start();



	return lReturn;
}

LRESULT CTestManager_EQP_3DCal::OnDIn_Stop()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_Stop();



	return lReturn;
}

//=============================================================================
// Method		: OnDIn_CheckSafetyIO
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 18:36
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::OnDIn_CheckSafetyIO()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_CheckSafetyIO();



	return lReturn;
}

LRESULT CTestManager_EQP_3DCal::OnDIn_CheckJIGCoverStatus()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_CheckJIGCoverStatus();



	return lReturn;
}

//=============================================================================
// Method		: OnDOut_BoardPower
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bOn
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/6 - 16:55
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::OnDOut_BoardPower(__in BOOL bOn, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_BoardPower(bOn, nParaIdx);



	return lReturn;
}

LRESULT CTestManager_EQP_3DCal::OnDOut_FluorescentLamp(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_FluorescentLamp(bOn);



	return lReturn;
}

LRESULT CTestManager_EQP_3DCal::OnDOut_StartLamp(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_StartLamp(bOn);



	return lReturn;
}

LRESULT CTestManager_EQP_3DCal::OnDOut_StopLamp(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_StopLamp(bOn);



	return lReturn;
}

LRESULT CTestManager_EQP_3DCal::OnDOut_TowerLamp(__in enLampColor nLampColor, __in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_TowerLamp(nLampColor, bOn);



	return lReturn;
}

LRESULT CTestManager_EQP_3DCal::OnDOut_Buzzer(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_Buzzer(bOn);



	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_EEPROM_Write
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/2/12 - 14:37
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::OnDAQ_EEPROM_Write(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	switch (m_stInspInfo.RecipeInfo.ModelType)
	{
	case Model_OMS_Entry:
		lReturn = OnDAQ_EEPROM_OMS_Entry_3D(nIdxBrd);
		break;

	case Model_OMS_Front:
		lReturn = OnDAQ_EEPROM_OMS_Front_3D(nIdxBrd);
		break;

	case Model_OMS_Front_Set:
		lReturn = OnDAQ_EEPROM_OMS_FrontSet_3D(nIdxBrd);
		break;

	case Model_MRA2:
	case Model_IKC:
	default:
		break;
	}

	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_EEPROM_OMS_Entry_3D
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/2/13 - 17:21
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::OnDAQ_EEPROM_OMS_Entry_3D(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;


	return lReturn;
}

LRESULT CTestManager_EQP_3DCal::OnDAQ_EEPROM_OMS_Front_3D(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;


	return lReturn;
}

LRESULT CTestManager_EQP_3DCal::OnDAQ_EEPROM_OMS_FrontSet_3D(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;


	return lReturn;
}

//=============================================================================
// Method		: OnManualSequence
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaID
// Parameter	: __in enParaManual enFunc
// Qualifier	: 메뉴얼 시퀀스 동작 테스트
// Last Update	: 2017/10/27 - 18:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::OnManualSequence(__in UINT nParaID, __in enParaManual enFunc)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnManualSequence(nParaID, enFunc);



	return lReturn;
}

//=============================================================================
// Method		: OnManualTestItem
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaID
// Parameter	: __in UINT nStepIdx
// Qualifier	:
// Last Update	: 2017/10/27 - 18:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::OnManualTestItem(__in UINT nParaID, __in UINT nStepIdx)
{
	LRESULT lReturn = RC_OK;

	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;
	INT_PTR iStepCnt = m_stInspInfo.RecipeInfo.StepInfo.GetCount();


	// * 검사 시작
	if (TRUE == pStepInfo->StepList[nStepIdx].bTest)
	{
		if (OpMode_DryRun != m_stInspInfo.OperateMode)
		{
			StartTest_3D_CAL_TestItem(pStepInfo->StepList[nStepIdx].nTestItem, nStepIdx, nParaID);
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: OnCheck_DeviceComm
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::OnCheck_DeviceComm()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnCheck_DeviceComm();



	return lReturn;
}

//=============================================================================
// Method		: OnCheck_SaftyJIG
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::OnCheck_SaftyJIG()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnCheck_SaftyJIG();



	return lReturn;
}

//=============================================================================
// Method		: OnCheck_RecipeInfo
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_3DCal::OnCheck_RecipeInfo()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnCheck_RecipeInfo();



	return lReturn;
}

//=============================================================================
// Method		: OnMakeReport
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 14:25
// Desc.		:
//=============================================================================
void CTestManager_EQP_3DCal::OnJugdement_And_Report(__in UINT nParaIdx)
{
	CFile_Report fileReport;


	//fileReport.SaveFinalizeResult2DCal(nParaIdx, m_stInspInfo.CamInfo[nParaIdx].szBarcode, m_stInspInfo.CamInfo[nParaIdx].st2D_Result);

}


void CTestManager_EQP_3DCal::OnJugdement_And_Report_All()
{
	OnUpdateYield();
	OnSaveWorklist();
}

//=============================================================================
// Method		: CreateTimer_UpdateUI
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in DWORD DueTime
// Parameter	: __in DWORD Period
// Qualifier	:
// Last Update	: 2017/11/8 - 14:17
// Desc.		: 파워 서플라이 모니터링
//=============================================================================
void CTestManager_EQP_3DCal::CreateTimer_UpdateUI(__in DWORD DueTime /*= 5000*/, __in DWORD Period /*= 250*/)
{
	// 		__super::CreateTimer_UpdateUI(5000, 2500);
}

void CTestManager_EQP_3DCal::DeleteTimer_UpdateUI()
{
	// 		__super::DeleteTimer_UpdateUI();
}

//=============================================================================
// Method		: OnMonitor_TimeCheck
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 13:28
// Desc.		:
//=============================================================================
void CTestManager_EQP_3DCal::OnMonitor_TimeCheck()
{
	CTestManager_EQP::OnMonitor_TimeCheck();

}

//=============================================================================
// Method		: OnMonitor_UpdateUI
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 21:19
// Desc.		: Power Supply 모니터링
//=============================================================================
void CTestManager_EQP_3DCal::OnMonitor_UpdateUI()
{
	CTestManager_EQP::OnMonitor_UpdateUI();

}

//=============================================================================
// Method		: OnSaveWorklist
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/5/10 - 20:24
// Desc.		:
//=============================================================================
void CTestManager_EQP_3DCal::OnSaveWorklist()
{
	// 	m_stInspInfo.WorklistInfo.Reset();
	// 
	// 	ST_CamInfo* pstCam = NULL;
	// 
	// 	CString szText;
	// 	CString szTime;
	// 	SYSTEMTIME lcTime;
	// 
	// 	GetLocalTime(&lcTime);
	// 	szTime.Format(_T("'%04d-%02d-%02d %02d:%02d:%02d.%03d"), lcTime.wYear, lcTime.wMonth, lcTime.wDay,
	// 		lcTime.wHour, lcTime.wMinute, lcTime.wSecond, lcTime.wMilliseconds);
	// 
	// 	for (UINT nChIdx = 0; nChIdx < USE_CHANNEL_CNT; nChIdx++)
	// 	{
	// 		if (FALSE == m_stInspInfo.bTestEnable[nChIdx])
	// 			continue;
	// 
	// 		pstCam = &m_stInspInfo.CamInfo[nChIdx];
	// 
	// 		m_stInspInfo.WorklistInfo.Time = szTime;
	// 		m_stInspInfo.WorklistInfo.EqpID = m_stInspInfo.szEquipmentID;	// g_szInsptrSysType[m_InspectionType];
	// 		m_stInspInfo.WorklistInfo.SWVersion;
	// 		m_stInspInfo.WorklistInfo.Model = m_stInspInfo.RecipeInfo.szModelCode;
	// 		m_stInspInfo.WorklistInfo.Barcode = pstCam->szBarcode;
	// 		m_stInspInfo.WorklistInfo.Result = g_TestResult[pstCam->nJudgment].szText;
	// 
	// 		szText.Format(_T("%d"), nChIdx + 1);
	// 		m_stInspInfo.WorklistInfo.Socket = szText;
	// 
	// 
	// 		m_stInspInfo.WorklistInfo.MakeItemz();
	// 
	// 		// 파일 저장
	// 		m_Worklist.Save_FinalResult_List(m_stInspInfo.Path.szReport, &pstCam->TestTime.tmStart_Loading, &m_stInspInfo.WorklistInfo);
	// 
	// 		// UI 표시
	// 		OnInsertWorklist();
	// 
	// 	}// End of for
}

//=============================================================================
// Method		: OnInitialize
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/26 - 6:03
// Desc.		:
//=============================================================================
void CTestManager_EQP_3DCal::OnInitialize()
{
	CTestManager_EQP::OnInitialize();

}

//=============================================================================
// Method		: OnFinalize
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/26 - 6:03
// Desc.		:
//=============================================================================
void CTestManager_EQP_3DCal::OnFinalize()
{
	CTestManager_EQP::OnFinalize();

}

//=============================================================================
// Method		: SetPermissionMode
// Access		: public  
// Returns		: void
// Parameter	: __in enPermissionMode nAcessMode
// Qualifier	:
// Last Update	: 2016/11/9 - 18:52
// Desc.		:
//=============================================================================
void CTestManager_EQP_3DCal::SetPermissionMode(__in enPermissionMode nAcessMode)
{
	CTestManager_EQP::SetPermissionMode(nAcessMode);


}
