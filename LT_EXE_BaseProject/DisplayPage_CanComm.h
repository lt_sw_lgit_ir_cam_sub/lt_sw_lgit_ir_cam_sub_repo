#pragma once

#ifndef DisplayPage_CanComm_h__
#define DisplayPage_CanComm_h__

#include "Wnd_MT_PCBComm.h"
#include "Wnd_MT_Calibration.h"
#include "Wnd_MT_ImageProc.h"
#include "Wnd_MT_Particle.h"

#include "Def_MTCtrl_Item.h"

class CDisplayPage_CanComm
{
public:
	CDisplayPage_CanComm();
	virtual		~CDisplayPage_CanComm();

	void SetWnd_MT_PCBComm(__in CWnd_MT_PCBComm* pWnd)
	{
		m_wnd_MT_PCBComm = pWnd;
	}

	void SetWnd_MT_PCBCommPg2(__in CWnd_MT_Calibration* pWnd)
	{
		m_wnd_MT_Calibration = pWnd;
	}

	void SetWnd_MT_PCBCommPg3(__in CWnd_MT_ImageProc* pWnd)
	{
		m_wnd_MT_ImageTest = pWnd;
	}

	void SetWnd_MT_PCBCommPg4(__in CWnd_MT_Particle* pWnd)
	{
		m_wnd_MT_Particle = pWnd;
	}

	virtual	void	OnCanPg_TextSetSendProtocol		(__in UINT nParaIdx, __in enMTCtrl_TestItem enItem, __in CString szValue);
	virtual	void	OnCanPg_TextSetRecvProtocol		(__in UINT nParaIdx, __in enMTCtrl_TestItem enItem, __in CString szValue);
	virtual void	OnCanPg_TextSetResetInfo		();

	virtual	void	OnCanPg2_TextGetTestValue		(__in UINT nParaIdx, __in enMTCtrl_Calibration enItem, __out CString &szValue);
	virtual	void	OnCanPg2_TextSetSendProtocol	(__in UINT nParaIdx, __in enMTCtrl_Calibration enItem, __in CString szValue);
	virtual	void	OnCanPg2_TextSetRecvProtocol	(__in UINT nParaIdx, __in enMTCtrl_Calibration enItem, __in CString szValue);
	virtual void	OnCanPg2_TextSetResetInfo		();

	virtual	void	OnCanPg3_TextSetSendProtocol	(__in UINT nParaIdx, __in enMTCtrl_VsAlgo enItem, __in CString szValue);
	virtual	void	OnCanPg3_TextSetRecvProtocol	(__in UINT nParaIdx, __in enMTCtrl_VsAlgo enItem, __in CString szValue);
	virtual void	OnCanPg3_TextSetResetInfo		();

	virtual	void	OnCanPg4_TextGetTestValue		(__in UINT nParaIdx, __in enMTCtrl_Particle enItem, __out CString &szValue);
	virtual	void	OnCanPg4_TextSetSendProtocol	(__in UINT nParaIdx, __in enMTCtrl_Particle enItem, __in CString szValue);
	virtual	void	OnCanPg4_TextSetRecvProtocol	(__in UINT nParaIdx, __in enMTCtrl_Particle enItem, __in CString szValue);
	virtual void	OnCanPg4_TextSetResetInfo		();

protected:
	CWnd_MT_PCBComm*		m_wnd_MT_PCBComm;
	CWnd_MT_Calibration*	m_wnd_MT_Calibration;
	CWnd_MT_ImageProc*		m_wnd_MT_ImageTest;
	CWnd_MT_Particle*		m_wnd_MT_Particle;
};

#endif // DisplayPage_CanComm_h__