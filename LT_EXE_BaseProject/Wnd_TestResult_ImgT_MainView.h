﻿#ifndef Wnd_TestResult_ImgT_MainView_h__
#define Wnd_TestResult_ImgT_MainView_h__

#pragma once

#include "VGStatic.h"
#include "Def_DataStruct_Cm.h"

#include "Wnd_CurrentEntryData.h"
#include "Wnd_CurrentData.h"
#include "Wnd_DefectPixelData.h"
#include "Wnd_DistortionData.h"
#include "Wnd_DynamicRangeData.h"
#include "Wnd_FovData.h"
#include "Wnd_IntensityData.h"
#include "Wnd_OpticalData.h"
#include "Wnd_RotateData.h"
#include "Wnd_SFRData.h"
#include "Wnd_ShadingData.h"
#include "Wnd_SNR_LightData.h"
#include "Wnd_ParticleData.h"
#include "Wnd_Particle_EntryData.h"

#include "Wnd_HotPixelData.h"
#include "Wnd_3DDepthData.h"
#include "Wnd_FPNData.h"
#include "Wnd_EEPROM_Data.h"
#include "Wnd_TemperatureSensorData.h"
// CWnd_TestResult_ImgT_MainView
enum enTestResult_ImgT_Main_Static
{
	STI_TR_ImgT_Main_Title,
	STI_TR_ImgT_Main_MAX,
};

static LPCTSTR	g_szTestResult_ImgT_Main_Static[] =
{
	_T("TEST RESULT"),
	NULL
};

class CWnd_TestResult_ImgT_MainView : public CWnd
{
	DECLARE_DYNAMIC(CWnd_TestResult_ImgT_MainView)

public:
	CWnd_TestResult_ImgT_MainView();
	virtual ~CWnd_TestResult_ImgT_MainView();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	enInsptrSysType			m_InspectionType;

	CFont					m_font;
	
	CVGStatic				m_st_default;
	CVGStatic				m_st_Item[STI_TR_ImgT_Main_MAX];

	CComboBox		m_cb_TestItem;


	int m_iSelectTest[20];


	CWnd_CurrentEntryData			m_Wnd_CurrentEntryData;
	CWnd_CurrentData				m_Wnd_CurrentData;
	CWnd_DefectPixelData			m_Wnd_DefectPixelData;
	CWnd_DistortionData				m_Wnd_DistortionData;
	CWnd_DynamicRangeData			m_Wnd_DynamicRangeData;
	CWnd_FovData					m_Wnd_FovData;
	CWnd_IntensityData				m_Wnd_IntensityData;
	CWnd_OpticalData				m_Wnd_OpticalData;
	CWnd_RotateData					m_Wnd_RotateData;
	//CWnd_SFRData					m_Wnd_SFRData;
	CWnd_SFRData					m_Wnd_SFRData[SFR_TestNum_MaxEnum];
	CWnd_ShadingData				m_Wnd_ShadingData;
	CWnd_SNR_LightData				m_Wnd_SNR_LightData;
	CWnd_ParticleData				m_Wnd_ParticleData;

	CWnd_HotPixelData				m_Wnd_HotPixelData;
	CWnd_3DDepthData				m_Wnd_3DDepthData;
	CWnd_FPNData					m_Wnd_FPNData;
	CWnd_EEPROM_Data				m_Wnd_EEPROMData;
	CWnd_TemperatureSensorData		m_Wnd_TemperatureSensorData;
	CWnd_Particle_EntryData			m_Wnd_Particle_EntryData;

	void	SetShowWindowResult (int iItem);
	void	MoveWindow_Result	(int x, int y, int nWidth, int nHeight);

public:
	afx_msg void OnLbnSelChangeTest();

	void SelectNum(UINT nSelect);

	enModelType		m_ModelType;

	void SetModelType(__in enModelType nModelType)
	{
		m_ModelType = nModelType;
		SelectNum(0);
	}

	// 검사기 종류 설정
	void	SetSystemType (__in enInsptrSysType nSysType)
	{
		m_InspectionType = nSysType;
	};

	void SetTestItem		(__in int iTestItem);
	void SetUpdateResult	(__in const ST_RecipeInfo_Base* pstRecipeInfo, __in int iTestItem);
	void SetUpdateClear		();

	void AllDataReset();
	void SetUI_Reset(int nIdx);
	void SetUIData(int nIdx, LPVOID pParam);
	void SetClearTab();
	void SetAddTab(int nIdx, int &iItemCnt);
};
#endif // Wnd_TestResult_ImgT_MainView_h__
