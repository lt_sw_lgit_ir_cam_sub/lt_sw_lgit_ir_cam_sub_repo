//*****************************************************************************
// Filename	: 	Cal_Stereo.cpp
// Created	:	2018/2/23 - 14:44
// Modified	:	2018/2/23 - 14:44
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#include "stdafx.h"
#include "Cal_Stereo.h"

#if (SET_INSPECTOR == SYS_STEREO_CAL)
#ifdef _WIN64	// 64bit
	#ifdef _DEBUG
		#ifdef _UNICODE
			#pragma comment (lib,"../LT_Cal_Library/StereoCal/x64/MRA2_stereo_calibration_api_d.lib")
		#else
			#pragma comment (lib,"../LT_Cal_Library/StereoCal/x64/MRA2_stereo_calibration_api_d.lib")
		#endif
	#else
		#ifdef _UNICODE
			#pragma comment (lib,"../LT_Cal_Library/StereoCal/x64/MRA2_stereo_calibration_api.lib")
		#else
			#pragma comment (lib,"../LT_Cal_Library/StereoCal/x64/MRA2_stereo_calibration_api.lib")
		#endif
	#endif
#endif
#endif

CCal_Stereo::CCal_Stereo()
{
}


CCal_Stereo::~CCal_Stereo()
{
}

//=============================================================================
// Method		: Start_Calibration_Mono
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2018/2/23 - 14:28
// Desc.		:
//=============================================================================
BOOL CCal_Stereo::Start_Calibration_Mono()
{
	BOOL bReturn = TRUE;

#if (SET_INSPECTOR == SYS_STEREO_CAL)
	int board_w = 5;
	int board_h = 5;

	MRA2_HANDLE mra2_handle = NULL;
	int width	= 0;
	int height	= 0;
	void* bmpData = NULL;

	mra2_handle = MRA2_single_camera_start(board_w, board_h);

	//bool found = MRA2_find_single_camera_pattern(mra2_handle, bmpData, MRA2_IMAGE_FORMAT_24_RGB, width, height, true, true);
	bool found = MRA2_find_single_camera_pattern(mra2_handle, bmpData, MRA2_IMAGE_FORMAT_24_RGB, width, height, true, NULL, NULL);


	SINGLE_CALIBRATION_RESULT single_calibration_result;
	if (MRA2_find_single_camera_parameter(mra2_handle, MRA2_CAMERA_FLAG::MRA2_CAMERA_FLAG_1, &single_calibration_result))
	{
		;
	}

	MRA2_single_camera_stop(mra2_handle);

#endif

	return bReturn;
}

//=============================================================================
// Method		: Set_Parameter_Mono
// Access		: public  
// Returns		: void
// Parameter	: __in int nBoard_W
// Parameter	: __in int nBoard_H
// Qualifier	:
// Last Update	: 2018/2/23 - 14:28
// Desc.		:
//=============================================================================
BOOL CCal_Stereo::Set_Parameter_Mono(__in int nBoard_W, __in int nBoard_H)
{
	BOOL bReturn = TRUE;

#if (SET_INSPECTOR == SYS_STEREO_CAL)
	if (m_hCal_Single)
	{
		Close_CalHandle_Mono();
	}

	__try
	{
		m_hCal_Single = MRA2_single_camera_start(nBoard_W, nBoard_H);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : MRA2_single_camera_start()\n"));
	}

	if (NULL == m_hCal_Single)
	{
		TRACE(_T("MRA2_single_camera_start() is Return NULL\n"));
		bReturn = FALSE;
	}
	
#endif
	return bReturn;
}

//=============================================================================
// Method		: Detect_Pattern_Mono
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPBYTE lpbyIn_Image
// Parameter	: __in MRA2_IMAGE_FORMAT nIn_Format
// Parameter	: __in int nWidth
// Parameter	: __in int nHeight
// Parameter	: __out MRA2_POINT * pstOutPoints
// Parameter	: __out int * piOutCount
// Parameter	: __in bool bIsCircle
// Parameter	: __in bool bDisplayCorners
// Qualifier	:
// Last Update	: 2018/4/4 - 16:16
// Desc.		:
//=============================================================================
BOOL CCal_Stereo::Detect_Pattern_Mono(__in LPBYTE lpbyIn_Image, __in MRA2_IMAGE_FORMAT nIn_Format, __in int nWidth, __in int nHeight, __out MRA2_POINT* pstOutPoints, __out int* piOutCount, __in bool bIsCircle /*= true*/, __in bool bDisplayCorners /*= false*/)
{
	BOOL bFound = TRUE;
#if (SET_INSPECTOR == SYS_STEREO_CAL)

	if (NULL == m_hCal_Single)
	{
		TRACE(_T("CCal_Stereo::Detect_Pattern_Mono() m_hCal_Single is NULL\n"));
		return FALSE;
	}

	__try
	{
		//bFound = MRA2_find_single_camera_pattern(m_hCal_Single, lpbyIn_Image, nIn_Format, nWidth, nHeight, bIsCircle, bDisplayCorners);
		bFound = MRA2_find_single_camera_pattern(m_hCal_Single, lpbyIn_Image, nIn_Format, nWidth, nHeight, bIsCircle, pstOutPoints, piOutCount);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : MRA2_find_single_camera_pattern()\n"));
		bFound = FALSE;
	}

	if (FALSE == bFound)
	{
		TRACE(_T("MRA2_find_single_camera_pattern() is Failed\n"));
		//Close_CalHandle_Mono();
	}

#endif
	return bFound;
}

//=============================================================================
// Method		: Exec_CalIntrinsic_Mono
// Access		: public  
// Returns		: BOOL
// Parameter	: __in MRA2_CAMERA_FLAG nFlag
// Parameter	: __out SINGLE_CALIBRATION_RESULT & stCalResult
// Qualifier	:
// Last Update	: 2018/4/4 - 15:53
// Desc.		:
//=============================================================================
BOOL CCal_Stereo::Exec_CalIntrinsic_Mono(__in MRA2_CAMERA_FLAG nFlag, __out SINGLE_CALIBRATION_RESULT& stCalResult)
{
	BOOL bReturn = TRUE;
#if (SET_INSPECTOR == SYS_STEREO_CAL)

	if (NULL == m_hCal_Single)
	{
		TRACE(_T("CCal_Stereo::Exec_CalIntrinsic_Mono() m_hCal_Single is NULL\n"));
		return FALSE;
	}

	__try
	{
		bReturn = MRA2_find_single_camera_parameter(m_hCal_Single, nFlag, &stCalResult);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : MRA2_find_single_camera_parameter()\n"));
		bReturn = FALSE;
	}
	
	if (FALSE == bReturn)
	{
		TRACE(_T("MRA2_find_single_camera_parameter() is Failed\n"));
	}

	//Close_CalHandle_Mono();
#endif
	return bReturn;
}

//=============================================================================
// Method		: Close_CalHandle_Mono
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/23 - 14:28
// Desc.		:
//=============================================================================
void CCal_Stereo::Close_CalHandle_Mono()
{
#if (SET_INSPECTOR == SYS_STEREO_CAL)
	if (NULL != m_hCal_Single)
	{
		MRA2_single_camera_stop(m_hCal_Single);
		m_hCal_Single = NULL;
	}
#endif
}

//=============================================================================
// Method		: Start_Calibration_Stereo
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2018/2/23 - 14:28
// Desc.		:
//=============================================================================
BOOL CCal_Stereo::Start_Calibration_Stereo()
{
	BOOL bReturn = TRUE;

#if (SET_INSPECTOR == SYS_STEREO_CAL)
	int nx = 5;
	int ny = 5;

	MRA2_HANDLE mra2_handle = NULL;
	mra2_handle = MRA2_stereo_camera_start(nx, ny);


	int width = 0;
	int height = 0;
	void* bmpDataLeft = NULL;
	void* bmpDataRight = NULL;
	//if (MRA2_find_stereo_camera_pattern(mra2_handle, bmpDataLeft, bmpDataRight, MRA2_IMAGE_FORMAT_24_RGB, width, height, true, true))
	if (MRA2_find_stereo_camera_pattern(mra2_handle, bmpDataLeft, bmpDataRight, MRA2_IMAGE_FORMAT_24_RGB, width, height, true, NULL, NULL, NULL, NULL))
	{
		;
	}

	STEREO_CALIBRATION_RESULT stereo_calibration_result;
	if (MRA2_find_stereo_camera_parameter(mra2_handle, MRA2_CAMERA_FLAG::MRA2_CAMERA_FLAG_1, &stereo_calibration_result))
	{
		;
	}

	MRA2_stereo_camera_stop(mra2_handle);

#endif
	return bReturn;
}

//=============================================================================
// Method		: Set_Parameter_Stereo
// Access		: public  
// Returns		: void
// Parameter	: __in int nBoard_W
// Parameter	: __in int nBoard_H
// Qualifier	:
// Last Update	: 2018/2/23 - 14:28
// Desc.		:
//=============================================================================
BOOL CCal_Stereo::Set_Parameter_Stereo(__in int nBoard_W, __in int nBoard_H)
{
	BOOL bReturn = TRUE;

#if (SET_INSPECTOR == SYS_STEREO_CAL)
	if (m_hCal_Stereo)
	{
		Close_CalHandle_Stereo();
	}

	__try
	{
		m_hCal_Stereo = MRA2_stereo_camera_start(nBoard_W, nBoard_H);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : MRA2_stereo_camera_start()\n"));
	}

	if (NULL == m_hCal_Stereo)
	{
		TRACE(_T("MRA2_stereo_camera_start() is Return NULL\n"));
		bReturn = FALSE;
	}
#endif

	return bReturn;
}

//=============================================================================
// Method		: Detect_Pattern_Stereo
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPBYTE lpbyIn_Image_Left
// Parameter	: __in LPBYTE lpbyIn_Image_Right
// Parameter	: __in MRA2_IMAGE_FORMAT nIn_Format
// Parameter	: __in int nWidth
// Parameter	: __in int nHeight
// Parameter	: __out MRA2_POINT * pstOutLeftPoints
// Parameter	: __out int * piOutLeftCount
// Parameter	: __out MRA2_POINT * pstOutRightPoints
// Parameter	: __out int * piOutRightCount
// Parameter	: __in bool bIsCircle
// Parameter	: __in bool bDisplayCorners
// Qualifier	:
// Last Update	: 2018/4/4 - 16:17
// Desc.		:
//=============================================================================
BOOL CCal_Stereo::Detect_Pattern_Stereo(__in LPBYTE lpbyIn_Image_Left, __in LPBYTE lpbyIn_Image_Right, __in MRA2_IMAGE_FORMAT nIn_Format, __in int nWidth, __in int nHeight, __out MRA2_POINT* pstOutLeftPoints, __out int* piOutLeftCount, __out MRA2_POINT* pstOutRightPoints, __out int* piOutRightCount, __in bool bIsCircle /*= true*/, __in bool bDisplayCorners /*= false*/)
{
	BOOL bFound = TRUE;
#if (SET_INSPECTOR == SYS_STEREO_CAL)

	if (NULL == m_hCal_Stereo)
	{
		TRACE(_T("CCal_Stereo::Detect_Pattern_Stereo() m_hCal_Stereo is NULL\n"));
		return FALSE;
	}

	__try
	{
		//bFound = MRA2_find_stereo_camera_pattern(m_hCal_Stereo, lpbyIn_Image_Left, lpbyIn_Image_Right, nIn_Format, nWidth, nHeight, bIsCircle, bDisplayCorners);
		bFound = MRA2_find_stereo_camera_pattern(m_hCal_Stereo, lpbyIn_Image_Left, lpbyIn_Image_Right, nIn_Format, nWidth, nHeight, bIsCircle, pstOutLeftPoints, piOutLeftCount, pstOutRightPoints, piOutRightCount);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : MRA2_find_stereo_camera_pattern()\n"));
		bFound = FALSE;
	}

	if (FALSE == bFound)
	{
		TRACE(_T("MRA2_find_stereo_camera_pattern() is Failed\n"));
		//Close_CalHandle_Stereo();
	}
#endif
	return bFound;
}

//=============================================================================
// Method		: Exec_CalIntrinsic_Setreo
// Access		: public  
// Returns		: BOOL
// Parameter	: __in MRA2_CAMERA_FLAG nFlag
// Parameter	: __out STEREO_CALIBRATION_RESULT & stCalResult
// Qualifier	:
// Last Update	: 2018/4/4 - 15:53
// Desc.		:
//=============================================================================
BOOL CCal_Stereo::Exec_CalIntrinsic_Setreo(__in MRA2_CAMERA_FLAG nFlag, __out STEREO_CALIBRATION_RESULT& stCalResult)
{
	BOOL bReturn = TRUE;
	double dReturn = 0.0f;
#if (SET_INSPECTOR == SYS_STEREO_CAL)

	if (NULL == m_hCal_Stereo)
	{
		TRACE(_T("CCal_Stereo::Exec_CalIntrinsic_Setreo() m_hCal_Stereo is NULL\n"));
		return FALSE;
	}

	__try
	{
		dReturn = MRA2_find_stereo_camera_parameter(m_hCal_Stereo, nFlag, &stCalResult);
		stCalResult.streo_rms = dReturn;
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : MRA2_find_stereo_camera_parameter()\n"));
		bReturn = FALSE;
	}

	//if (0.0 < dReturn)
	if (m_hCal_Stereo)
	{
	}
	else
	{
		bReturn = FALSE;
		TRACE(_T("MRA2_find_stereo_camera_parameter() is Failed\n"));
	}

	//Close_CalHandle_Stereo();
#endif
	return bReturn;
}

//=============================================================================
// Method		: Close_CalHandle_Stereo
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/23 - 14:28
// Desc.		:
//=============================================================================
void CCal_Stereo::Close_CalHandle_Stereo()
{
#if (SET_INSPECTOR == SYS_STEREO_CAL)
	if (NULL != m_hCal_Stereo)
	{
		MRA2_stereo_camera_stop(m_hCal_Stereo);
		m_hCal_Stereo = NULL;
	}
#endif
}