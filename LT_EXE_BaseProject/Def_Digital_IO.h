//*****************************************************************************
// Filename	: 	Def_Digital_IO.h
// Created	:	2018/1/5 - 17:20
// Modified	:	2018/1/5 - 17:20
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#ifndef Def_Digital_IO_h__
#define Def_Digital_IO_h__

#include <afxwin.h>

//-------------------------------------------------------------------
// Default Digital I/O
//-------------------------------------------------------------------
typedef enum enDI_Default
{
	DI_00 = 0,
	DI_01,
	DI_02,
	DI_03,
	DI_04,
	DI_05,
	DI_06,
	DI_07,
	DI_08,
	DI_09,
	DI_10,
	DI_11,
	DI_12,
	DI_13,
	DI_14,
	DI_15,
	DI_MaxEnum,
};

static LPCTSTR g_szDI_Default[] =
{
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	NULL
};

typedef enum enDO_Default
{
	DO_00 = 0,
	DO_01,
	DO_02,
	DO_03,
	DO_04,
	DO_05,
	DO_06,
	DO_07,
	DO_08,
	DO_09,
	DO_10,
	DO_11,
	DO_12,
	DO_13,
	DO_14,
	DO_15,
	DO_MaxEnum,
};

static LPCTSTR g_szDO_Default[] =
{
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	NULL
};

//-------------------------------------------------------------------
// 2D Calibration System
//-------------------------------------------------------------------
typedef enum enDI_2DCal
{
	DI_2D_00_MainPower	= 0,
	DI_2D_01_EMO,
	DI_2D_02_AreaSensor,
	DI_2D_03_DoorSensor,
	DI_2D_04_JIG_CoverCheck,
	DI_2D_05,
	DI_2D_06_Start,
	DI_2D_07_Stop,
	DI_2D_08,
	DI_2D_09,
	DI_2D_10,
	DI_2D_11,
	DI_2D_12,
	DI_2D_13,
	DI_2D_14,
	DI_2D_15,
	DI_2D_MaxEnum,
};

static LPCTSTR g_szDI_2DCal[] =
{
	_T("Main Power"),
	_T("EMO"),
	_T("Area Sensor"),
	_T("Door Sensor"),
	_T("JIG Cover Check"),
	_T(""),
	_T("Start"),
	_T("Stop"),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	NULL
};

typedef enum enDO_2DCal
{
	DO_2D_00_CameraBoardPower_L,
	DO_2D_01_CameraBoardPower_R,
	DO_2D_02,
	DO_2D_03_FluorescentLamp,
	DO_2D_04,
	DO_2D_05,
	DO_2D_06_StartLamp,
	DO_2D_07_StopLamp,
	DO_2D_08,
	DO_2D_09,
	DO_2D_10,
	DO_2D_11,
	DO_2D_12_TowerLamp_Red,
	DO_2D_13_TowerLamp_Yellow,
	DO_2D_14_TowerLamp_Green,
	DO_2D_15_TowerLamp_Buzzer,
	DO_2D_MaxEnum,
};

static LPCTSTR g_szDO_2DCal[] =
{
	_T("Camera Board Power[L]	"),
	_T("Camera Board Power[R]	"),
	_T("						"),
	_T("Fluorescent Lamp"),
	_T(""),
	_T(""),
	_T("Start Lamp"),
	_T("Stop Lamp"),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T("Tower Lamp Red"),
	_T("Tower Lamp Yellow"),
	_T("Tower Lamp Green"),
	_T("Tower Lamp Buzzer"),
	NULL
};

//-------------------------------------------------------------------
// Stereo System
//-------------------------------------------------------------------
typedef enum enDI_Stereo
{
	DI_St_00_MainPower = 0,
	DI_St_01_EMO,
	DI_St_02_AreaSensor,
	DI_St_03_DoorSensor,
	DI_St_04_JIG_CoverCheck,
	DI_St_05,
	DI_St_06_Start,
	DI_St_07_Stop,
	DI_St_08,
	DI_St_09,
	DI_St_10,
	DI_St_11,
	DI_St_12,
	DI_St_13,
	DI_St_14,
	DI_St_15,
	DI_St_MaxEnum,
};

static LPCTSTR g_szDI_Stereo[] =
{
	_T("Main Power"),
	_T("EMO"),
	_T("Area Sensor"),
	_T("Door Sensor"),
	_T("JIG Cover Check"),
	_T(""),
	_T("Start"),
	_T("Stop"),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	NULL
};

typedef enum enDO_Stereo
{
	DO_St_00_CameraBoardPower_L,
	DO_St_01_CameraBoardPower_R,
	DO_St_02_LightBoardPower,
	DO_St_03_FluorescentLamp,
	DO_St_04,
	DO_St_05,
	DO_St_06_StartLamp,
	DO_St_07_StopLamp,
	DO_St_08,
	DO_St_09,
	DO_St_10,
	DO_St_11,
	DO_St_12_TowerLamp_Red,
	DO_St_13_TowerLamp_Yellow,
	DO_St_14_TowerLamp_Green,
	DO_St_15_TowerLamp_Buzzer,
	DO_St_MaxEnum,
};

static LPCTSTR g_szDO_Stereo[] =
{
	_T("Camera Board Power[L]	"),
	_T("Camera Board Power[R]	"),
	_T("Light Board	Power		"),
	_T("Fluorescent Lamp"),
	_T(""),
	_T(""),
	_T("Start Lamp"),
	_T("Stop Lamp"),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T("Tower Lamp Red"),
	_T("Tower Lamp Yellow"),
	_T("Tower Lamp Green"),
	_T("Tower Lamp Buzzer"),
	NULL
};

//-------------------------------------------------------------------
// Focusing System
//-------------------------------------------------------------------
typedef enum enDI_Focusing
{
	DI_Fo_00_MainPower = 0,
	DI_Fo_01_EMO,
	DI_Fo_02_AreaSensor,
	DI_Fo_03_DoorTSensor,
	DI_Fo_04_DoorBSensor,
	DI_Fo_05_ColletUpSensor,
	DI_Fo_06_ColletDnSensor,
	DI_Fo_07,
	DI_Fo_08_StartL,
	DI_Fo_09_StartR,
	DI_Fo_10_Stop,
	DI_Fo_11_Master,
	DI_Fo_12_FixOn,
	DI_Fo_13_FixOff,
	DI_Fo_14,
	DI_Fo_15_FixOnSensorC,
	DI_Fo_16_FixOnSensorL,
	DI_Fo_17_FixOnSensorR,
	DI_Fo_18_FixOffSensorL,
	DI_Fo_19_FixOffSensorR,
	DI_Fo_20_Screw_A,
	DI_Fo_21_Screw_B,
	DI_Fo_22_Screw_D,
	DI_Fo_23_Screw_C,
	DI_Fo_24_DriverUpSensor,
	DI_Fo_25_DriverDownSensor,
	DI_Fo_26_GripperUpSensor,
	DI_Fo_27_GripperDnSensor,
	DI_Fo_28_GripperOnSensor,
	DI_Fo_29_GripperOffSensor,
	DI_Fo_30_ParticleInSensor,
	DI_Fo_31_ParticleOutSensor,
	DI_Fo_MaxEnum,
};

static LPCTSTR g_szDI_Focusing[] =
{
	_T(" Main Power			 "),
	_T(" EMO				 "),
	_T(" Area Sensor		 "),
	_T(" Door T Sensor		 "),
	_T(" Door B Sensor		 "),
	_T(" Collet Up Sensor	 "),
	_T(" Collet Down Sensor	 "),
	_T("					 "),
	_T(" Start BUTTON [L]	 "),
	_T(" Start BUTTON [R]	 "),
	_T(" Stop BUTTON		 "),
	_T(" Master BUTTON		 "),
	_T(" Fix On BUTTON		 "),
	_T(" Fix Off BUTTON		 "),
	_T("					 "),
	_T(" Fix On Sensor [C]	 "),
	_T(" Fix On Sensor [L]	 "),
	_T(" Fix On Sensor [R]	 "),
	_T(" Fix Off Sensor [L]	 "),
	_T(" Fix Off Sensor [R]	 "),
	_T(" Detect Screw [A]  	 "),
	_T(" Detect Screw [B]  	 "),
	_T(" Detect Screw [D] 	 "),
	_T(" Detect Screw [C] 	 "),
	_T(" Driver Up Sensr	 "),
	_T(" Driver Down Sensr   "),
	_T(" Gripper Up Sensor	 "),
	_T(" Gripper Down Sensor "),
	_T(" Gripper On Sensor 	 "),
	_T(" Gripper Off Sensor	 "),
	_T(" Particle In Sensor  "),
	_T(" Particle Out Sensor "),
	NULL
};

typedef enum enDO_Focusing
{
	DO_Fo_00,
	DO_Fo_01_CameraBoard,
	DO_Fo_02_LightBoard,
	DO_Fo_03,
	DO_Fo_04_ColletUpCylinder,
	DO_Fo_05_ColletDnCylinder,
	DO_Fo_06_Light,
	DO_Fo_07,
	DO_Fo_08_StartLempL,
	DO_Fo_09_StartLempR,
	DO_Fo_10_StopLemp,
	DO_Fo_11_MasterLemp,
	DO_Fo_12_FixOnLemp,
	DO_Fo_13_FixOffLemp,
	DO_Fo_14,
	DO_Fo_15,
	DO_Fo_16_FixOnCylinder,
	DO_Fo_17_FixOffCylinder,
	DO_Fo_18_TowerLampRed,
	DO_Fo_19_TowerLampYellow,
	DO_Fo_20_TowerLampGreen,
	DO_Fo_21_TowerLampBuzzer,
	DO_Fo_22,
	DO_Fo_23,
	DO_Fo_24_DriverUpCylinder,
	DO_Fo_25_DriverDownCylinder,
	DO_Fo_26_GripperUpCylinder,
	DO_Fo_27_GripperDnCylinder,
	DO_Fo_28_GripperOnCylinder,
	DO_Fo_29_GripperOffCylinder,
	DO_Fo_30_ParticleInCylinder,
	DO_Fo_31_ParticleOutCylinder,
	DO_Fo_MaxEnum,
};

static LPCTSTR g_szDO_Focusing[] =
{
	_T("						"),
	_T(" Camera Board Power		"),
	_T(" Light Board			"),
	_T("						"),
	_T(" Collet Up Cylinder		"),
	_T(" Collet Down Cylinder	"),
	_T(" Fluorescent Lamp		"),
	_T("						"),
	_T(" Start Lemp [L]			"),
	_T(" Start Lemp [R]			"),
	_T(" Stop Lemp				"),
	_T(" Master Lemp			"),
	_T(" FixOn Lemp				"),
	_T(" FixOff Lemp			"),
	_T("						"),
	_T("						"),
	_T(" FixOn Cylinder [S]		"),
	_T(" FixOff Cylinder [S]	"),
	_T(" TowerLamp Red			"),
	_T(" TowerLamp Yellow		"),
	_T(" TowerLamp Green		"),
	_T(" TowerLamp Buzzer		"),
	_T("						"),
	_T("						"),
	_T(" Driver Up Cylinder		"),
	_T(" Driver Down Cylinder	"),
	_T(" Gripper Up Cylinder	"),
	_T(" Gripper Down Cylinder	"),
	_T(" Gripper On Cylinder	"),
	_T(" Gripper Off Cylinder	"),
	_T(" Particle In Cylinder	"),
	_T(" Particle Out Cylinder	"),
	NULL
};

typedef enum enCYL_Focusing
{
	CYL_Fo_FixOn,
	CYL_Fo_FixOff,
	CYL_Fo_StageUp,
	CYL_Fo_StageDown,
	CYL_Fo_GripperOn,
	CYL_Fo_GripperOff,
	CYL_Fo_ParticleIn,
	CYL_Fo_ParticleOut,
	CYL_Fo_DriveUp,
	CYL_Fo_DriveDown,
	CYL_Fo_ColletUp,
	CYL_Fo_ColletDn,
	CYL_Fo_MaxEnum,
};

static LPCTSTR g_szCYL_Focusing[] =
{
	_T(" Cylinder FIX ON		"),
	_T(" Cylinder FIX OFF		"),
	_T(" Cylinder Stage UP		"),
	_T(" Cylinder Stage DOWN	"),
	_T(" Cylinder Gripper ON	"),
	_T(" Cylinder Gripper OFF	"),
	_T(" Cylinder Particle IN	"),
	_T(" Cylinder Particle OUT	"),
	_T(" Cylinder Driver UP		"),
	_T(" Cylinder Driver DOWN	"),
	_T(" Collet Up							"),
	_T(" Collet Down						"),
	NULL
};

//-------------------------------------------------------------------
// ImageTest System
//-------------------------------------------------------------------
typedef enum enDI_ImageTest
{
	DI_ImgT_00_MainPower,
	DI_ImgT_01_EMO,
	DI_ImgT_02_CoverSensor,
	DI_ImgT_03_AreaSensor,
	DI_ImgT_04_DoorSensor,
	DI_ImgT_05_Start,
	DI_ImgT_06_Stop,
	DI_ImgT_07,
	DI_ImgT_08,
	DI_ImgT_09,
	DI_ImgT_10,
	DI_ImgT_11,
	DI_ImgT_12,
	DI_ImgT_13,
	DI_ImgT_14,
	DI_ImgT_15,
	DI_ImgT_MaxEnum,
};

static LPCTSTR g_szDI_ImageTest[] =
{
	_T(" Main Power			 "),
	_T(" EMO				 "),
	_T(" Cover Sensor		 "),
	_T(" Area Sensor		 "),
	_T(" Door Sensor		 "),
	_T(" Start				 "),
	_T(" Stop				 "),
	_T("					 "),
	_T("					 "),
	_T("					 "),
	_T("					 "),
	_T("					 "),
	_T("					 "),
	_T("					 "),
	_T("					 "),
	_T("					 "),
	NULL
};

typedef enum enDO_ImageTest
{
	DO_ImgT_00_CameraPowerL,
	DO_ImgT_01_CameraPowerR,
	DO_ImgT_02_LightPower,
	DO_ImgT_03,
	DO_ImgT_04,
	DO_ImgT_05_StartLamp,
	DO_ImgT_06_StopLamp,
	DO_ImgT_07,
	DO_ImgT_08_LightTop,
	DO_ImgT_09_Light,
	DO_ImgT_10,
	DO_ImgT_11_TowerLampRed,
	DO_ImgT_12_TowerLampYellow,
	DO_ImgT_13_TowerLampGreen,
	DO_ImgT_14_TowerLampBuzzer,
	DO_ImgT_15,
	DO_ImgT_MaxEnum,
};

static LPCTSTR g_szDO_ImageTest[] =
{
	_T(" Camera Board Power[L]	"),
	_T(" Camera Board Power[R]	"),
	_T(" Light Board Power		"),
	_T("						"),
	_T("						"),
	_T(" Start Lemp 			"),
	_T(" Stop Lemp				"),
	_T("						"),
	_T("						"),
	_T(" Fluorescent Lamp		"),
	_T("						"),
	_T(" TowerLamp Red			"),
	_T(" TowerLamp Yellow		"),
	_T(" TowerLamp Green		"),
	_T(" TowerLamp Buzzer		"),
	_T("						"),
	NULL
};

#endif // Def_Digital_IO_h__
