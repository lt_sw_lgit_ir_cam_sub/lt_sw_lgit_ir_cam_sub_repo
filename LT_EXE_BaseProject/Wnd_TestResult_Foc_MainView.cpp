﻿// Wnd_TestResult_Foc_MainView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "Wnd_TestResult_Foc_MainView.h"


// CWnd_TestResult_Foc_MainView
typedef enum TestResult_ImgID
{
	IDC_BTN_ITEM  = 1001,
	IDC_CMB_ITEM  = 2001,
	IDC_EDT_ITEM  = 3001,
	IDC_LIST_ITEM = 4001,
};

IMPLEMENT_DYNAMIC(CWnd_TestResult_Foc_MainView, CWnd)

CWnd_TestResult_Foc_MainView::CWnd_TestResult_Foc_MainView()
{
	for (UINT nItem = 0; nItem < TI_Foc_MaxEnum; nItem++)
	{
		m_iSelectTest[nItem] = -1;
	}

	m_InspectionType = Sys_Focusing;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_TestResult_Foc_MainView::~CWnd_TestResult_Foc_MainView()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_TestResult_Foc_MainView, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
	ON_CBN_SELENDOK(IDC_CMB_ITEM, OnLbnSelChangeTest)
END_MESSAGE_MAP()

// CWnd_TestResult_Foc_MainView 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_TestResult_Foc_MainView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < STI_TR_Foc_Main_MAX; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdex].Create(g_szTestResult_Foc_Main_Static[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	m_cb_TestItem.Create(dwStyle | CBS_DROPDOWNLIST | WS_VSCROLL, rectDummy, this, IDC_CMB_ITEM);
	m_cb_TestItem.SetFont(&m_font);

	UINT nWndID = IDC_LIST_ITEM;

	m_Wnd_CurrentData.SetOwner(GetOwner());
	m_Wnd_CurrentData.Create(NULL, _T("Current"), dwStyle /*| WS_BORDER*/, rectDummy,this, nWndID++);
	m_Wnd_CurrentData.ShowWindow(SW_HIDE);

	m_Wnd_DefectPixelData.SetOwner(GetOwner());
	m_Wnd_DefectPixelData.Create(NULL, _T("DefectPixel"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	m_Wnd_DefectPixelData.ShowWindow(SW_HIDE);

	m_Wnd_OpticalData.SetOwner(GetOwner());
	m_Wnd_OpticalData.SetDisplayMode(TRUE);
	m_Wnd_OpticalData.Create(NULL, _T("Optical"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	m_Wnd_OpticalData.ShowWindow(SW_HIDE);

	m_Wnd_RotateData.SetOwner(GetOwner());
	m_Wnd_RotateData.SetDisplayMode(TRUE);
	m_Wnd_RotateData.Create(NULL, _T("Rotate"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	m_Wnd_RotateData.ShowWindow(SW_HIDE);

	m_Wnd_SFRData.SetOwner(GetOwner());
	m_Wnd_SFRData.SetDisplayMode(TRUE);
	m_Wnd_SFRData.Create(NULL, _T("SFR"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	m_Wnd_SFRData.ShowWindow(SW_HIDE);

	m_Wnd_ParticleData.SetOwner(GetOwner());
	m_Wnd_ParticleData.Create(NULL, _T("Particle"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	m_Wnd_ParticleData.ShowWindow(SW_HIDE);
	
	m_Wnd_TorqueData.SetOwner(GetOwner());
	m_Wnd_TorqueData.Create(NULL, _T("Torque"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	m_Wnd_TorqueData.ShowWindow(SW_HIDE);

	m_Wnd_ActiveAlignData.SetOwner(GetOwner());
	m_Wnd_ActiveAlignData.Create(NULL, _T("ActiveAlign"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	m_Wnd_ActiveAlignData.ShowWindow(SW_HIDE);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc_MainView::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iMargin  = 10;
	int iSpacing = 5;

	int iLeft	 = 0;
	int iTop	 = 0;
	int iWidth   = cx;
	int iHeight  = cy;
	
	int iStHeight = 25;
	int iHeightTemp	= iHeight / 9;

	m_st_Item[STI_TR_Foc_Main_Title].MoveWindow(iLeft, iTop, iWidth, iStHeight);
	iTop += iStHeight + 2;
	
	m_cb_TestItem.MoveWindow(iLeft, iTop, iWidth, iStHeight);
	iTop += iStHeight + 2;

	switch (m_iSelectTest[m_cb_TestItem.GetCurSel()])
	{
	case TI_Foc_Fn_PreFocus:
		m_Wnd_OpticalData.MoveWindow(iLeft, iTop, iWidth, iHeightTemp * 2);
		break;

	case TI_Foc_Fn_ActiveAlign:
		m_Wnd_ActiveAlignData.MoveWindow(iLeft, iTop, iWidth, iHeightTemp * 3);
		iTop += iHeightTemp * 3;

		m_Wnd_TorqueData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
		break;

	case TI_Foc_Fn_ECurrent:
		m_Wnd_CurrentData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
		break;

	case TI_Foc_Fn_OpticalCenter:
		m_Wnd_OpticalData.MoveWindow(iLeft, iTop, iWidth, iHeightTemp * 2);
		break;

	case TI_Foc_Fn_SFR:
		m_Wnd_SFRData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
		break;

	case TI_Foc_Fn_Rotation:
		m_Wnd_RotateData.MoveWindow(iLeft, iTop, iWidth, iHeightTemp);
		break;

	case TI_Foc_Fn_Stain:
		m_Wnd_ParticleData.MoveWindow(iLeft, iTop, iWidth, iHeightTemp);
		break;

	case TI_Foc_Fn_DefectPixel:
		m_Wnd_DefectPixelData.MoveWindow(iLeft, iTop, iWidth, iHeightTemp);
		break;

	case TI_Foc_Re_TorqueCheck:
		m_Wnd_TorqueData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
		break;

	case TI_Foc_Motion_ReleaseScrew:
		m_Wnd_TorqueData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
		break;

	case TI_Foc_Motion_LockingScrew:
		m_Wnd_TorqueData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_TestResult_Foc_MainView::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: SetShowWindowResult
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/21 - 10:59
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc_MainView::SetShowWindowResult(int iItem)
{
	switch (m_InspectionType)
	{
	case Sys_Focusing:
		break;
	case Sys_2D_Cal:
		break;
	case Sys_Image_Test:
		break;
	case Sys_Stereo_Cal:
		break;
	case Sys_3D_Cal:
		break;
	default:
		break;
	}

}

//=============================================================================
// Method		: MoveWindow_Result
// Access		: protected  
// Returns		: void
// Parameter	: int x
// Parameter	: int y
// Parameter	: int nWidth
// Parameter	: int nHeight
// Qualifier	:
// Last Update	: 2017/10/21 - 11:16
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc_MainView::MoveWindow_Result(int x, int y, int nWidth, int nHeight)
{
	int iHeaderH = 40;
	int iListH = iHeaderH + 3 * 20;
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc_MainView::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (SW_HIDE == bShow)
	{
		SetShowWindowResult(-1);
	}
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc_MainView::OnRangeBtnCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_BTN_ITEM;
}

//=============================================================================
// Method		: SetTestItemResult
// Access		: public  
// Returns		: void
// Parameter	: __in  int iTestItem
// Qualifier	:
// Last Update	: 2017/10/21 - 10:45
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc_MainView::SetTestItem(__in int iTestItem)
{
	SetShowWindowResult(iTestItem);
}

//=============================================================================
// Method		: SetUpdateResult
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_RecipeInfo_Base * pstRecipeInfo
// Parameter	: __in int iTestItem
// Qualifier	:
// Last Update	: 2017/10/21 - 12:44
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc_MainView::SetUpdateResult(__in const ST_RecipeInfo_Base* pstRecipeInfo, __in int iTestItem)
{
	if (pstRecipeInfo == NULL)
		return;

	switch (m_InspectionType)
	{
	case Sys_Focusing:
		break;
	case Sys_2D_Cal:
		break;
	case Sys_Image_Test:
		break;
	case Sys_Stereo_Cal:
		break;
	case Sys_3D_Cal:
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: SetUpdateClear
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/21 - 14:23
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc_MainView::SetUpdateClear()
{
}
//=============================================================================
// Method		: AllDataReset
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/19 - 10:23
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc_MainView::AllDataReset()
{
	m_Wnd_CurrentData.DataReset();
	m_Wnd_OpticalData.DataReset();
	m_Wnd_SFRData.DataReset();
	m_Wnd_RotateData.DataReset();
	m_Wnd_ParticleData.DataReset();
	m_Wnd_DefectPixelData.DataReset();
	m_Wnd_ActiveAlignData.DataReset();
	m_Wnd_TorqueData.DataReset();
}
//=============================================================================
// Method		: SetUIData_Reset
// Access		: public  
// Returns		: void
// Parameter	: int nIdx
// Qualifier	:
// Last Update	: 2018/3/9 - 8:52
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc_MainView::SetUIData_Reset(int nIdx)
{
	switch (nIdx)
	{
	case TI_Foc_Fn_PreFocus:
		m_Wnd_OpticalData.DataReset();
		break;
	case TI_Foc_Fn_ECurrent:
		m_Wnd_CurrentData.DataReset();
		break;
	case TI_Foc_Fn_ActiveAlign:
		m_Wnd_ActiveAlignData.DataReset();
		m_Wnd_TorqueData.DataReset();
		break;
	case  TI_Foc_Fn_OpticalCenter:
		m_Wnd_OpticalData.DataReset();
		break;
	case TI_Foc_Fn_SFR:
		m_Wnd_SFRData.DataReset();
		break;
	case TI_Foc_Fn_Rotation:
		m_Wnd_RotateData.DataReset();
		break;
	case TI_Foc_Fn_Stain:
		m_Wnd_ParticleData.DataReset();
		break;
	case TI_Foc_Fn_DefectPixel:
		m_Wnd_DefectPixelData.DataReset();
		break;
	case TI_Foc_Re_TorqueCheck:
		m_Wnd_TorqueData.DataReset();
		break;
	case TI_Foc_Motion_ReleaseScrew:
		m_Wnd_TorqueData.DataReset();
		break;
	case TI_Foc_Motion_LockingScrew:
		m_Wnd_TorqueData.DataReset();
		break;

	default:
		break;
	}

	SelectItem(nIdx);
}

//=============================================================================
// Method		: SetUIData
// Access		: public  
// Returns		: void
// Parameter	: __in int nIdx
// Parameter	: __in LPVOID pParam
// Parameter	: __in enFocus_AAView enView
// Qualifier	:
// Last Update	: 2018/3/9 - 10:01
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc_MainView::SetUIData(__in int nIdx, __in LPVOID pParam, __in enFocus_AAView enView)
{
	switch (nIdx)
	{
	case TI_Foc_Fn_PreFocus:
	{
		ST_OpticalCenter_Data *pData = (ST_OpticalCenter_Data *)pParam;
		m_Wnd_OpticalData.DetectDisplay(*pData);
	}
	break;

	case TI_Foc_Fn_ActiveAlign:
	{
		if (Foc_AA == enView)
		{
			ST_ActiveAlign_Data *pData = (ST_ActiveAlign_Data *)pParam;
			m_Wnd_ActiveAlignData.DataDisplay(*pData);
		}
		else if (Foc_AA_Torque == enView)
		{
			ST_Torque_Data *pData = (ST_Torque_Data *)pParam;
			m_Wnd_TorqueData.DataDisplay(*pData);
		}
	}
	break;

	case TI_Foc_Fn_ECurrent:
	{
		ST_ECurrent_Data *pData = (ST_ECurrent_Data *)pParam;
		m_Wnd_CurrentData.DataDisplay(*pData);
	}
	break;

	case TI_Foc_Fn_OpticalCenter:
	{
		ST_OpticalCenter_Data *pData = (ST_OpticalCenter_Data *)pParam;
		m_Wnd_OpticalData.DataDisplay(*pData);
	}
	break;

	case TI_Foc_Fn_SFR:
	{
		ST_SFR_Data *pData = (ST_SFR_Data *)pParam;
		m_Wnd_SFRData.DataDisplay(*pData);
	}
	break;

	case TI_Foc_Fn_Rotation:
	{
		ST_Rotate_Data *pData = (ST_Rotate_Data *)pParam;
		m_Wnd_RotateData.DataDisplay(*pData);
	}
	break;

	case TI_Foc_Fn_Stain:
	{
		ST_Particle_Data *pData = (ST_Particle_Data *)pParam;
		m_Wnd_ParticleData.DataDisplay(*pData);
	}
	break;

	case TI_Foc_Fn_DefectPixel:
	{
		ST_DefectPixel_Data *pData = (ST_DefectPixel_Data *)pParam;
		m_Wnd_DefectPixelData.DataDisplay(*pData);
	}
	break;

	case TI_Foc_Re_TorqueCheck:
	{
		ST_Torque_Data *pData = (ST_Torque_Data *)pParam;
		m_Wnd_TorqueData.DataDisplay(*pData);
	}
	break;

	case TI_Foc_Motion_ReleaseScrew:
	{
		ST_Torque_Data *pData = (ST_Torque_Data *)pParam;
		m_Wnd_TorqueData.DataDisplay(*pData);
	}
	break;

	case TI_Foc_Motion_LockingScrew:
	{
		ST_Torque_Data *pData = (ST_Torque_Data *)pParam;
		m_Wnd_TorqueData.DataDisplay(*pData);
	}
	break;

	default:
		break;
	}
}

//=============================================================================
// Method		: SetClearTab
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/9 - 9:07
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc_MainView::SetClearTab()
{
	for (UINT nItem = 0; nItem < TI_Foc_MaxEnum; nItem++)
	{
		m_iSelectTest[nItem] = -1;
	}

	if (m_cb_TestItem.GetCount() > 0)
	{
		m_cb_TestItem.ResetContent();
	}
}

//=============================================================================
// Method		: SetAddTab
// Access		: public  
// Returns		: void
// Parameter	: int nIdx
// Parameter	: int & iItemCnt
// Qualifier	:
// Last Update	: 2018/3/9 - 9:03
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc_MainView::SetAddTab(int nIdx, int &iItemCnt)
{
	// 첫 아이템 등록시 콤보 박스 초기화
	if (iItemCnt == 0)
	{
		m_cb_TestItem.ResetContent();
	}

	// 기존에 항목이 있으면 추가 하지 말고 지나가라.
	for (int iItem = 0; iItem < iItemCnt; iItem++)
	{
		if (nIdx == m_iSelectTest[iItem])
			return;
	}

	switch (nIdx)
  	{
	case TI_Foc_Fn_PreFocus:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("PreFocus"));
		break;
	case TI_Foc_Fn_ActiveAlign:	
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("ActiveAlign"));
		break;
	case TI_Foc_Fn_ECurrent:	
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("Current"));
		break;
	case TI_Foc_Fn_OpticalCenter:	
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("OpticalCenter"));
		break;
	case TI_Foc_Fn_SFR:	
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("SFR"));
		break;
	case TI_Foc_Fn_Rotation:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("Rotate"));
		break;
	case TI_Foc_Fn_Stain:	
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("Stain"));
		break;
	case TI_Foc_Fn_DefectPixel:	
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("DefectPixel"));
		break;
	case TI_Foc_Re_TorqueCheck:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("Torque"));
		break;
	case TI_Foc_Motion_ReleaseScrew:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("Torque"));
		break;
	case TI_Foc_Motion_LockingScrew:
		m_iSelectTest[iItemCnt++] = nIdx;
		m_cb_TestItem.AddString(_T("Torque"));
		break;

  	default:
  		break;
  	}
}

//=============================================================================
// Method		: OnLbnSelChangeTest
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/9 - 9:09
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc_MainView::OnLbnSelChangeTest()
{
	m_Wnd_CurrentData.ShowWindow(SW_HIDE);
	m_Wnd_DefectPixelData.ShowWindow(SW_HIDE);
	m_Wnd_OpticalData.ShowWindow(SW_HIDE);
	m_Wnd_RotateData.ShowWindow(SW_HIDE);
	m_Wnd_SFRData.ShowWindow(SW_HIDE);
	m_Wnd_ParticleData.ShowWindow(SW_HIDE);
	m_Wnd_TorqueData.ShowWindow(SW_HIDE);
	m_Wnd_ActiveAlignData.ShowWindow(SW_HIDE);

	switch (m_iSelectTest[m_cb_TestItem.GetCurSel()])
	{
	case TI_Foc_Fn_PreFocus:
		m_Wnd_OpticalData.ShowWindow(SW_SHOW);
		break;
	case TI_Foc_Fn_ActiveAlign:
		m_Wnd_ActiveAlignData.ShowWindow(SW_SHOW);
		m_Wnd_TorqueData.ShowWindow(SW_SHOW);
		break;
	case TI_Foc_Fn_ECurrent:
		m_Wnd_CurrentData.ShowWindow(SW_SHOW);
		break;
	case TI_Foc_Fn_OpticalCenter:
		m_Wnd_OpticalData.ShowWindow(SW_SHOW);
		break;
	case TI_Foc_Fn_SFR:
		m_Wnd_SFRData.ShowWindow(SW_SHOW);
		break;
	case TI_Foc_Fn_Rotation:
		m_Wnd_RotateData.ShowWindow(SW_SHOW);
		break;
	case TI_Foc_Fn_Stain:
		m_Wnd_ParticleData.ShowWindow(SW_SHOW);
		break;
	case TI_Foc_Fn_DefectPixel:
		m_Wnd_DefectPixelData.ShowWindow(SW_SHOW);
		break;
	case TI_Foc_Re_TorqueCheck:
		m_Wnd_TorqueData.ShowWindow(SW_SHOW);
		break;
	case TI_Foc_Motion_ReleaseScrew:
		m_Wnd_TorqueData.ShowWindow(SW_SHOW);
		break;	
	case TI_Foc_Motion_LockingScrew:
		m_Wnd_TorqueData.ShowWindow(SW_SHOW);
		break;

	default:
		break;
	}

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());
}

//=============================================================================
// Method		: SelectItem
// Access		: public  
// Returns		: void
// Parameter	: UINT nTestItem
// Qualifier	:
// Last Update	: 2018/3/9 - 9:09
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc_MainView::SelectItem(UINT nTestItem)
{
	int iSelect = 0;

	for (int nItem = 0; nItem < m_cb_TestItem.GetCount(); nItem++)
	{
		if (m_iSelectTest[nItem] == nTestItem)
		{
			iSelect = nItem;
			break;
		}
	}

	m_cb_TestItem.SetCurSel(iSelect);
	OnLbnSelChangeTest();
}