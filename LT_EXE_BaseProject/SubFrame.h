﻿//*****************************************************************************
// Filename	: SubFrame.h
// Created	: 2010/11/22
// Modified	: 2010/11/22 - 17:59
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
// SubFrame.h : CSubFrame 클래스의 인터페이스
//

#pragma once

#include "View_MainCtrl.h"
#include "NTCaptionBar.h"
#include "NTBannerBar.h"
#include "NTTabViewBar.h"
#include "NTLinksBar.h"
#include "Pane_CommStatus.h"
#include "Define_MonitorInfoFunc.h"

#ifndef CFrameWnd
#define CFrameWnd CFrameWndEx
#endif

//=============================================================================
//
//=============================================================================
class CSubFrame : public CFrameWndEx
{

public:
	CSubFrame();
protected: 
	DECLARE_DYNAMIC(CSubFrame)

	// 재정의입니다.
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);

	// 구현입니다.
public:
	virtual ~CSubFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // 컨트롤 모음이 포함된 멤버입니다.

	CView_MainCtrl		m_wndView_MainCtrl;	

	ST_MONITORINFO		m_infoMonitor;

	// 생성된 메시지 맵 함수
protected:
	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSetFocus			(CWnd *pOldWnd);

	afx_msg void	OnClose				();
	afx_msg void	OnActivate			(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	afx_msg void	OnGetMinMaxInfo		(MINMAXINFO FAR* lpMMI);
	afx_msg void	OnSettingChange		(UINT uFlags, LPCTSTR lpszSection);

	afx_msg void	OnTabView			(UINT nID);
	afx_msg void	OnUpdateTabView		(CCmdUI* pCmdUI);

	afx_msg	LRESULT	OnLoadComplete		(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnOption			(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT	OnLogMsg			(WPARAM wParam, LPARAM lParam);

	afx_msg	LRESULT	OnWatchExtProcess	(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT	OnOptionChanged		(WPARAM wParam, LPARAM lParam);

	afx_msg	LRESULT	OnTestFunction		(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT	OnChangeView		(WPARAM wParam, LPARAM lParam);

	afx_msg	LRESULT	OnPermissionMode	(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT	OnManualBarcode		(WPARAM wParam, LPARAM lParam);

	DECLARE_MESSAGE_MAP()

	int				MakeTabViewBar		();	
	void			AddLog				(LPCTSTR lpszLog, BOOL bError = FALSE, BYTE bySubLog = 0);
	void			AddLogProgramInfo	();


	HANDLE			m_hThreadStartSetting;
	static UINT WINAPI	ThreadStartSetting	(LPVOID lParam);
	void			LoadStartSetting	();

	BOOL			m_bShowDeviceInfoPane;
	void			ShowPaneByTabIndex	(UINT nTabIndex);

private:

	CNTCaptionBar		m_wndCaptionBar;
	CNTBannerBar		m_wndBannerBar;	
	CNTTabViewBar		m_wndTabViewBar;
	CNTLinksBar			m_wndLinksBar;
	CPane_CommStatus	m_wndDeviceStatus;

	void			SetupMemoryBitmapSize	(const CSize& sz);

	UINT				m_nTabView;

	CString				m_strExecutedAppTime;

	// 프로그램 시작 할 때
	void			InitProgram				();

	// 프로그램 종료 할 때
	void			ExitProgram				();

	void			ShowWindow_2ndView		(__in BOOL bShow);

	// 검사기 종류
	enInsptrSysType		m_InspectionType	= enInsptrSysType::Sys_Focusing;

public:
	
	// 검사기 종류 설정
	virtual void	SetSystemType			(__in enInsptrSysType nSysType);
};


