#ifndef Def_T_MasterMode_h__
#define Def_T_MasterMode_h__

#include <afxwin.h>
#include "Def_Test_Cm.h"
#include "Def_Enum.h"
#include "Def_TestItem_Cm.h"

#pragma pack(push,1)


//=============================================================================
// PreSet
//=============================================================================

static long g_Preset_MasterModeList[] =
{
	TI_Foc_Motion_LockingCheck,
	TI_Foc_Initialize_Test,
	TI_Foc_Fn_ECurrent,
	TI_Foc_Fn_OpticalCenter,
	TI_Foc_Fn_SFR,
	TI_Foc_Fn_Rotation,
	TI_Foc_Motion_ParticleIn,
	TI_Foc_Fn_Stain,
	TI_Foc_Fn_DefectPixel,
	TI_Foc_Motion_ParticleOut,
	TI_Foc_Re_ECurrent,
	TI_Foc_Re_OpticalCenterX,
	TI_Foc_Re_OpticalCenterY,
	TI_Foc_Re_SFR,
	TI_Foc_Re_Group_SFR,
	TI_Foc_Re_Tilt_SFR,
// 	TI_Foc_Re_G0_SFR,
// 	TI_Foc_Re_G1_SFR,
// 	TI_Foc_Re_G2_SFR,
// 	TI_Foc_Re_G3_SFR,
// 	TI_Foc_Re_G4_SFR,
// 	TI_Foc_Re_X1Tilt_SFR,
// 	TI_Foc_Re_X2Tilt_SFR,
// 	TI_Foc_Re_Y1Tilt_SFR,
// 	TI_Foc_Re_Y2Tilt_SFR,
	TI_Foc_Re_Rotation,
	TI_Foc_Re_Stain,
	TI_Foc_Re_DefectPixel,
	TI_Foc_Finalize_Test,
	-1,
};


typedef struct _tag_MasterMode_Para
{


	_tag_MasterMode_Para()
	{
		Reset();
	};

	_tag_MasterMode_Para& operator= (const _tag_MasterMode_Para& ref)
	{


		return *this;
	};

	void Reset()
	{

	};
}ST_MasterMode_Para, *PST_MasterMode_Para;


typedef struct _tag_MasterMode_Result
{
	UINT nMasterOCX;
	UINT nMasterOCY;
	double dbMasterRodate;

	_tag_MasterMode_Result()
	{
		Reset();
	};

	_tag_MasterMode_Result& operator= (const _tag_MasterMode_Result& ref)
	{
		nMasterOCX = ref.nMasterOCX;
		nMasterOCY = ref.nMasterOCY;
		dbMasterRodate = ref.dbMasterRodate;
		return *this;
	};

	void Reset()
	{
		nMasterOCX = 0;
		nMasterOCY = 0;
		dbMasterRodate = 0.0;
	};

}ST_MasterMode_Result, *PST_MasterMode_Result;

typedef struct _tag_MasterMode_Spec
{


	_tag_MasterMode_Spec()
	{
		Reset();
	};

	_tag_MasterMode_Spec& operator= (const _tag_MasterMode_Spec& ref)
	{


		return *this;
	};

	void Reset()
	{

	};

}ST_MasterMode_Spec, *PST_MasterMode_Spec;

//-------------------------------------------------------------------
// 6채널 다운로드 설비에서 사용하는 데이터 구조체
//-------------------------------------------------------------------
typedef struct _tag_MasterMode_Info
{
	ST_MasterMode_Para		Parameter;	// Parameters
	ST_MasterMode_Spec		Spec_Min;	// Results Spec Min
	ST_MasterMode_Spec		Spec_Max;	// Results Spec Max


	_tag_MasterMode_Info()
	{
		Reset();
	};

	_tag_MasterMode_Info& operator= (const _tag_MasterMode_Info& ref)
	{
		Parameter	= ref.Parameter;
		Spec_Min	= ref.Spec_Min;
		Spec_Max	= ref.Spec_Max;
		
		return *this;			
	};

	void Reset()
	{
		Parameter.Reset();
		Spec_Min.Reset();
		Spec_Max.Reset();
	};
}ST_MasterMode_Info, *PST_MasterMode_Info;


#pragma pack(pop)

#endif // Def_T_MasterMode_h__
