﻿#ifndef TestManager_TestMES_h__
#define TestManager_TestMES_h__

#pragma once

#include <windows.h>

#include "Def_DataStruct.h"
#include "File_Mes.h"
#include "Def_Mes_ImgT.h"
#include "Def_Mes_Foc.h"
#include "Def_Mes_EachTest.h"


class CTestManager_TestMES 
{
public:
	CTestManager_TestMES();
	virtual ~CTestManager_TestMES();

	//-모든 정보
	ST_InspectionInfo *m_pInspInfo;

	void	SetPtr_InspInfo(ST_InspectionInfo* pstInfo)
	{
		if (pstInfo == NULL)
			return;

		m_pInspInfo = pstInfo;
	};

	//전류
	void Current_DataSave(int nParaIdx, ST_MES_Data *pMesData);
	//광축
	void OpticalCenter_DataSave(int nParaIdx, ST_MES_Data *pMesData);
	//SFR
	//void SFR_DataSave(int nParaIdx, ST_MES_Data *pMesData);
	void SFR_DataSave(int nParaIdx, ST_MES_Data *pMesData, int nTypeCnt = 0);
	//Rotate
	void Rotate_DataSave(int nParaIdx, ST_MES_Data *pMesData);
	//Distortion
	void Distortion_DataSave(int nParaIdx, ST_MES_Data *pMesData);
	//FOV
	void Fov_DataSave(int nParaIdx, ST_MES_Data *pMesData);
	//Dynamic Range
	void DynamicRange_DataSave(int nParaIdx, ST_MES_Data *pMesData);
	//Intensity
	void Intensity_DataSave(int nParaIdx, ST_MES_Data *pMesData);
	//Stain
	void Stain_DataSave(int nParaIdx, ST_MES_Data *pMesData);
	//DefectPixel
	void Defectpixel_DataSave(int nParaIdx, ST_MES_Data *pMesData);
	//SNR(dB)
	void SNR_Light_DataSave(int nParaIdx, ST_MES_Data *pMesData);
	//Shading
	void Shading_DataSave(int nParaIdx, ST_MES_Data *pMesData);

	// HotPixel
	void HotPixel_DataSave(int nParaIdx, ST_MES_Data *pMesData);

	// FPN
	void FPN_DataSave(int nParaIdx, ST_MES_Data *pMesData);

	// 3D Depth
	void Depth_DataSave(int nParaIdx, ST_MES_Data *pMesData);

	// EEPROM
	void EEPROM_DataSave(int nParaIdx, ST_MES_Data *pMesData);

	//Particle_Entry
	void Particle_Entry_DataSave(int nParaIdx, ST_MES_Data *pMesData);
	// TemperatureSensor
	void TemperatureSensor_DataSave(int nParaIdx, ST_MES_Data *pMesData);

	//전류
	void Current_DataSave_Foc		(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData);

	//광축
	void OpticalCenter_DataSave_Foc	(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData);
	
	//SFR
	void SFR_DataSave_Foc			(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData);
	
	//Rotate
	void Rotate_DataSave_Foc		(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData);
	
	//Stain
	void Stain_DataSave_Foc			(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData);
	
	//DefectPixel
	void Defectpixel_DataSave_Foc	(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData);

	//Torque
	void Torque_DataSave_Foc		(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData);

	//-개별 저장 
	void Current_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult);
	void OpticalCenter_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult);
	//void SFR_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void SFR_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult, int nTypeCnt = 0);
	void Rotate_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult);
	void Distortion_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult);
	void Fov_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void DynamicRange_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult);
	void Intensity_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult);
	void Stain_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult);
	void Defectpixel_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult);
	void SNR_Light_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void Shading_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void HotPixel_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void FPN_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void Depth_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void EEPROM_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void TemperatureSensor_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void Particle_Entry_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);

	//Entry
	//전류
	void Current_Entry_DataSave(int nParaIdx, ST_MES_Entry_Data *pMesData);
	//광축
	void OpticalCenter_Entry_DataSave(int nParaIdx, ST_MES_Entry_Data *pMesData);
	//SFR
	void SFR_Entry_DataSave(int nParaIdx, ST_MES_Entry_Data *pMesData);
	//Rotate
	void Rotate_Entry_DataSave(int nParaIdx, ST_MES_Entry_Data *pMesData);
	//Distortion
	void Distortion_Entry_DataSave(int nParaIdx, ST_MES_Entry_Data *pMesData);
	//FOV
	void Fov_Entry_DataSave(int nParaIdx, ST_MES_Entry_Data *pMesData);
	//Dynamic Range
	void DynamicRange_Entry_DataSave(int nParaIdx, ST_MES_Entry_Data *pMesData);
	//SNR(dB)
	void SNR_Light_Entry_DataSave(int nParaIdx, ST_MES_Entry_Data *pMesData);
	//Shading
	void Shading_Entry_DataSave(int nParaIdx, ST_MES_Entry_Data *pMesData);
	// HotPixel
	void HotPixel_Entry_DataSave(int nParaIdx, ST_MES_Entry_Data *pMesData);
	// FPN
	void FPN_Entry_DataSave(int nParaIdx, ST_MES_Entry_Data *pMesData);
	// 3D Depth
	void Depth_Entry_DataSave(int nParaIdx, ST_MES_Entry_Data *pMesData);
	// EEPROM
	void EEPROM_Entry_DataSave(int nParaIdx, ST_MES_Entry_Data *pMesData);
	//Particle_Entry
	void Particle_Entry_DataSave(int nParaIdx, ST_MES_Entry_Data *pMesData);
	// TemperatureSensor
	void TemperatureSensor_Entry_DataSave(int nParaIdx, ST_MES_Entry_Data *pMesData);
};


#endif // TestManager_TestMES_h__
