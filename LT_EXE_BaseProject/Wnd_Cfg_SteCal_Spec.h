//*****************************************************************************
// Filename	: 	Wnd_Cfg_SteCal_Spec.h
// Created	:	2017/11/10 - 16:04
// Modified	:	2017/11/10 - 16:04
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Cfg_SteCal_Spec_h__
#define Wnd_Cfg_SteCal_Spec_h__

#pragma once

#include "Def_DataStruct_Cm.h"
#include "Def_T_SteCal.h"
#include "VGStatic.h"


//-----------------------------------------------------------------------------
// CWnd_Cfg_SteCal_Spec
//-----------------------------------------------------------------------------
class CWnd_Cfg_SteCal_Spec : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Cfg_SteCal_Spec)

public:
	CWnd_Cfg_SteCal_Spec();
	virtual ~CWnd_Cfg_SteCal_Spec();

protected:
	afx_msg int			OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void		OnSize					(UINT nType, int cx, int cy);
	afx_msg void		OnShowWindow			(BOOL bShow, UINT nStatus);
	virtual BOOL		PreCreateWindow			(CREATESTRUCT& cs);
	afx_msg void		OnBnClickedChkSpecMin	(UINT nID);
	afx_msg void		OnBnClickedChkSpecMax	(UINT nID);

	DECLARE_MESSAGE_MAP()

	CFont				m_font;

	CVGStatic			m_st_CapItem[2];
	CVGStatic			m_st_CapSpecMin[2];
	CVGStatic			m_st_CapSpecMax[2];

	CVGStatic			m_st_Item[Spec_SteCal_MaxNum];
	CMFCMaskedEdit		m_ed_SpecMin[Spec_SteCal_MaxNum];
	CMFCMaskedEdit		m_ed_SpecMax[Spec_SteCal_MaxNum];
	CMFCButton			m_chk_SpecMin[Spec_SteCal_MaxNum];
	CMFCButton			m_chk_SpecMax[Spec_SteCal_MaxNum];

	// Single / Stereo Cal ����
	BOOL				m_bStereoCAL		= FALSE;
public:

	void			Set_TestItemInfo		(__in ST_TestItemInfo* pstTestItemInfo);
	void			Get_TestItemInfo		(__out ST_TestItemInfo& stOutTestItemInfo);

	void			Set_SteCal_Spec			(__in ST_SteCal_Spec* pstSteCal_SpecMin, __in ST_SteCal_Spec* pstSteCal_SpecMax);
	void			Get_SteCal_Spec			(__out ST_SteCal_Spec& stOutSteCal_SpecMin, __out ST_SteCal_Spec& stOutSteCal_SpecMax);

	void			Set_StereoCAL			(__in BOOL bStereoCal);

};

#endif // Wnd_Cfg_SteCal_Spec_h__


