﻿//*****************************************************************************
// Filename	: 	File_Recipe.h
// Created	:	2017/10/10 - 10:53
// Modified	:	2017/10/10 - 10:53
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef File_Recipe_h__
#define File_Recipe_h__

#pragma once

#include "Def_DataStruct.h"
#include "File_Recipe_Cm.h"
#include "File_Config_VI.h"
#include "Def_T_SteCal.h"

//-----------------------------------------------------------------------------
// CFile_Recipe
//-----------------------------------------------------------------------------
class CFile_Recipe : public CFile_Recipe_Cm
{
public:
	CFile_Recipe();
	virtual ~CFile_Recipe();
	
	// 공통 사항
	virtual BOOL	Load_Common			(__in LPCTSTR szPath, __out ST_RecipeInfo& stRecipeInfo);
	virtual BOOL	Save_Common			(__in LPCTSTR szPath, __in const ST_RecipeInfo* pstRecipeInfo);
	
	// 레시피 설정값
	virtual BOOL	Load_RecipeFile		(__in LPCTSTR szPath, __out ST_RecipeInfo& stRecipeInfo);
	virtual BOOL	Save_RecipeFile		(__in LPCTSTR szPath, __in const ST_RecipeInfo* pstRecipeInfo);
	
	// DAQ
	virtual BOOL	Load_LVDS			(__in LPCTSTR szPath, __out ST_LVDSInfo& stLVDSInfo);
	virtual BOOL	Save_LVDS			(__in LPCTSTR szPath, __in const ST_LVDSInfo* pstLVDSInfo);

	// Stereo Calibration -----------------------------------------------------
	BOOL	Load_SteCal_Info			(__in LPCTSTR szPath, __out ST_SteCal_Info& stOutSteCal_Info);
	BOOL	Save_SteCal_Info			(__in LPCTSTR szPath, __in const ST_SteCal_Info* pstSteCal_Info);

	BOOL	Load_SteCal_Para			(__in LPCTSTR szPath, __out ST_SteCal_Para& stOutSteCal_Para);
	BOOL	Save_SteCal_Para			(__in LPCTSTR szPath, __in const ST_SteCal_Para* pstSteCal_Para);

};

#endif // File_Recipe_h__
