﻿//*****************************************************************************
// Filename	: Def_WindowMessage.h
// Created	: 2012/1/16
// Modified	: 2016/08/17
//
// Author	: PiRing
//	
// Purpose	: 윈도우 메세지 정의 (0x0400 ~ 0x7FFF)
//*****************************************************************************
#ifndef Def_WindowMessage_h__
#define Def_WindowMessage_h__

#include "Def_WindowMessage_Cm.h"

//-------------------------------------------------------------------
// 프로그램 운영
//-------------------------------------------------------------------
#define		WM_CHANGE_SITE_VIEW			WM_USER + 400
#define		WM_CHK_USABLE_CH			WM_USER + 401
#define		WM_RECV_MAIN_BRD_ACK		WM_USER + 402	// 제어 보드로부터 데이터 수신

#define		WM_EEPROM_CTRL				WM_USER + 403	// 임시
#define		WM_CAMERA_CTRL				WM_USER + 404	



#define		WM_SELECT_AXIS				WM_USER + 2001
#define		WM_UPDATA_AXIS				WM_USER + 2002
#define		WM_CHANGED_MOTOR			WM_USER + 2003	
#define		WM_CHANGED_MAINTENANCE		WM_USER + 2004
#define		WM_LIGHT_VOLT_CNTROL		WM_USER + 2005
#define		WM_LIGHT_STEP_CNTROL		WM_USER + 2006

#define		WM_MANAUL_SEQUENCE			WM_USER + 3000
#define		WM_MANAUL_TESTITEM			WM_USER + 3001
#define		WM_MANAUL_CANCOMM			WM_USER + 3002
#define		WM_MANAUL_CANCOMMPG2		WM_USER + 3003
#define		WM_MANAUL_CANCOMMPG3		WM_USER + 3004
#define		WM_MANAUL_CANCOMMPG4		WM_USER + 3005

#define		WM_CHANGE_OPTIONPIC			WM_USER + 4000
#define		WM_RECV_TORQUE_ACK			WM_USER + 5000	
#define		WM_CYLINGER_MANUL			WM_USER + 6000

//-------------------------------------------------------------------
//
//-------------------------------------------------------------------



#endif // Def_WindowMessage_h__
