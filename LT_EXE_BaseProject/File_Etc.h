#ifndef File_Etc_h__
#define File_Etc_h__

#pragma once

#include "Def_DataStruct.h"
#include "Def_T_2DCal.h"
#include "Def_T_3DCal.h"

class CFile_Etc
{
public:
	CFile_Etc();
	~CFile_Etc();

protected:

	CString		m_szPath;

public:

	void	SetPtrPath				(__in LPCTSTR szPath)
	{
		m_szPath = szPath;
	}

	void	SaveCornerPt			(__in CString szBarcode, __in ST_2DCal_CornerPt stCorner, __in UINT nIndex);
	void	SaveCornerPtBinFile		(__in CString szBarcode, __in ST_2DCal_CornerPt stCorner, __in UINT nIndex);
	BOOL	LoadCornerPtBinFile		(__in CString szBarcode, __out ST_2DCal_CornerPt& stCorner, __in UINT nIndex);

	void	Save2DCalSendResult		(__in CString szBarcode, __in ST_2DCal_Result stResult);
	void	Save3DCalDisResult		(__in CString szBarcode, __in ST_3DCal_Depth_Result stResult);
	void	Save3DCalEvaluResult	(__in CString szBarcode, __in ST_3DCal_Eval_Result stResult);

	void	DeleteDir				(__in CString szBarcode);

	void	LoadDTCErrorcodeFile	(__out ARR_DTC_DESC& arr);

};

#endif // File_Etc_h__