﻿// Wnd_TeachOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_TeachOp.h"

// CWnd_TeachOp

IMPLEMENT_DYNAMIC(CWnd_TeachOp, CWnd)

typedef enum TeachOp_ID
{
	IDC_TC_BN_SAVE	= 1100,
	IDC_TC_ED_ITEM1	= 1200,
	IDC_TC_ED_ITEM2,
	IDC_TC_ED_ITEM3,
	IDC_TC_ED_ITEM4,
	IDC_TC_ED_ITEM5,
	IDC_TC_ED_ITEM6,
	IDC_TC_ED_ITEM7,
	IDC_TC_ED_ITEM8,
	IDC_TC_ED_ITEM9,
	IDC_TC_ED_ITEM10,
	IDC_TC_ED_ITEM11,
	IDC_TC_ED_ITEM12,
	IDC_TC_ED_ITEM13,
	IDC_TC_ED_ITEM14,
	IDC_TC_MAXNUM,
};

CWnd_TeachOp::CWnd_TeachOp()
{
	m_nItemCnt = 0;

	m_pstMaintenanceInfo = NULL;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_TeachOp::~CWnd_TeachOp()
{
	m_font.DeleteObject		();
}

BEGIN_MESSAGE_MAP(CWnd_TeachOp, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_TC_BN_SAVE, OnBnClickedBnSave)
END_MESSAGE_MAP()

//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/3/28 - 17:02
// Desc.		:
//=============================================================================
int CWnd_TeachOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwEtStyle = WS_VISIBLE | WS_BORDER | ES_CENTER;
	DWORD dwStyle	= WS_VISIBLE | WS_CHILD;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_st_Name.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Name.SetBackColor_COLORREF(RGB(33, 73, 125));
	m_st_Name.SetTextColor(Gdiplus::Color::White, Gdiplus::Color::White);
	m_st_Name.SetFont_Gdip(L"Arial", 12.0F);
	m_st_Name.Create(_T("MOTOR TEACH PARAMETER SETTING"), dwStyle | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

	for (UINT nIdx = 0; nIdx < m_nItemCnt; nIdx++)
	{
		m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Item[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdx].SetTextAlignment(StringAlignmentNear);
		m_st_Item[nIdx].Create(m_szTeacOpName[nIdx], dwStyle | SS_LEFT | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdx = 0; nIdx < BN_TC_MAXNUM; nIdx++)
	{
		m_bn_Item[nIdx].Create(g_szTeachOpButton[nIdx], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_TC_BN_SAVE + nIdx);
		m_bn_Item[nIdx].SetFont(&m_font);
	}

	for (UINT nIdx = 0; nIdx < m_nItemCnt; nIdx++)
	{
		m_ed_Item[nIdx].Create(dwEtStyle, rectDummy, this, IDC_TC_ED_ITEM1 + nIdx);
		m_ed_Item[nIdx].SetWindowText(_T("0"));
		m_ed_Item[nIdx].SetValidChars(_T("0123456789.-"));
		m_ed_Item[nIdx].SetFont(&m_font);
	}

	m_st_Comm.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Comm.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Comm.SetFont_Gdip(L"Arial", 9.0F);
	m_st_Comm.SetTextAlignment(StringAlignmentNear);
	m_st_Comm.Create(_T("COMM"), dwStyle | SS_LEFT | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

	m_st_Fornt.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Fornt.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Fornt.SetFont_Gdip(L"Arial", 9.0F);
	m_st_Fornt.SetTextAlignment(StringAlignmentNear);
	m_st_Fornt.Create(_T("Fornt MODEL"), dwStyle | SS_LEFT | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

	m_st_Ikc.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Ikc.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Ikc.SetFont_Gdip(L"Arial", 9.0F);
	m_st_Ikc.SetTextAlignment(StringAlignmentNear);
	m_st_Ikc.Create(_T("IKC MODEL"), dwStyle | SS_LEFT | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

	m_st_MRA2.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_MRA2.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_MRA2.SetTextAlignment(StringAlignmentNear);
	m_st_MRA2.SetFont_Gdip(L"Arial", 9.0F);
	m_st_MRA2.Create(_T("MRA2 MODEL"), dwStyle | SS_LEFT | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

	m_st_Entry.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Entry.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Entry.SetTextAlignment(StringAlignmentNear);
	m_st_Entry.SetFont_Gdip(L"Arial", 9.0F);
	m_st_Entry.Create(_T("Entry MODEL"), dwStyle | SS_LEFT | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/5/29 - 10:54
// Desc.		:
//=============================================================================
void CWnd_TeachOp::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	int iMargin  = 5;
	int iSpacing = 5;

	int iLeft	 = iMargin;
	int iTop	 = iMargin;
	int iWidth	 = cx - iMargin - iMargin;
	int iHeight  = cy - iMargin - iMargin;
	int iEdWidth = (iWidth - iSpacing * 9) / 8;

	int iEditHeight = (iHeight - iSpacing * 11) / 10; 
	int iBtnWidth = 0;
	int iTitleH = 35;
	
	int iWidthTemp = (iWidth - iSpacing * 3) / 4;
	int iWidthItem = (iWidthTemp - iSpacing) / 2;
	m_st_Name.MoveWindow(0, iTop, cx, iTitleH);

	iLeft = iMargin;
	iTop += iTitleH + iSpacing;

	switch (m_nSysType)
	{
	case Sys_Image_Test:
		m_st_Comm.MoveWindow(iLeft, iTop, iWidthTemp * 2 + iSpacing, iEditHeight);
		
		iLeft += iWidthTemp * 2 + iSpacing + iSpacing;
		m_st_Fornt.MoveWindow(iLeft, iTop, iWidthTemp, iEditHeight);
	
		iLeft += iWidthTemp + iSpacing;
		m_st_MRA2.MoveWindow(iLeft, iTop, iWidthTemp, iEditHeight);

		// COMM
		iLeft = iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Imgt_Load_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Imgt_Load_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Imgt_Load_Y].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Imgt_Load_Y].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Imgt_Load_Z].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Imgt_Load_Z].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Imgt_Load_R].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Imgt_Load_R].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Imgt_Test_R].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Imgt_Test_R].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);
		
		iLeft = iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Imgt_Par_Y].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Imgt_Par_Y].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);
		
		iLeft = iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Imgt_Distance].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Imgt_Distance].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Imgt_Crash_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Imgt_Crash_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = iWidthTemp + iSpacing + iSpacing;
		iTop = iTitleH + iSpacing + iMargin + iEditHeight + iSpacing;
		m_st_Item[TC_Imgt_Shutter].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Imgt_Shutter].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		// ENTRY
		iLeft = iWidthTemp + iSpacing + iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Entry.MoveWindow(iLeft, iTop, iWidthTemp, iEditHeight);

		iLeft = iWidthTemp + iSpacing + iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Imgt_Load_R_Entry].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Imgt_Load_R_Entry].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = iWidthTemp + iSpacing + iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Imgt_Test_X_Entry].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Imgt_Test_X_Entry].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = iWidthTemp + iSpacing + iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Imgt_Test_Z_Entry].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Imgt_Test_Z_Entry].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = iWidthTemp + iSpacing + iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Imgt_Test_R_Entry].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Imgt_Test_R_Entry].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = iWidthTemp + iSpacing + iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Imgt_Par_X_Entry].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Imgt_Par_X_Entry].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = iWidthTemp + iSpacing + iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Imgt_Par_Y_Entry].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Imgt_Par_Y_Entry].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		// FRONT
		iLeft = (iWidthTemp + iSpacing) * 2 + iSpacing;
		iTop   = iTitleH + iSpacing + iMargin + iEditHeight + iSpacing;
		m_st_Item[TC_Imgt_Test_Front_Z].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Imgt_Test_Front_Z].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = (iWidthTemp + iSpacing) * 2 + iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Imgt_Chart_Front_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Imgt_Chart_Front_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = (iWidthTemp + iSpacing) * 2 + iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Imgt_Par_Front_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Imgt_Par_Front_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		// IKC
		iLeft = (iWidthTemp + iSpacing) * 2 + iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Ikc.MoveWindow(iLeft, iTop, iWidthTemp, iEditHeight);

		iLeft = (iWidthTemp + iSpacing) * 2 + iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Imgt_Test_IKC_Z].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Imgt_Test_IKC_Z].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = (iWidthTemp + iSpacing) * 2 + iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Imgt_Chart_IKC_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Imgt_Chart_IKC_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = (iWidthTemp + iSpacing) * 2 + iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Imgt_Par_IKC_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Imgt_Par_IKC_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		// MRA2
		iLeft = (iWidthTemp + iSpacing) * 3 + iSpacing;
		iTop  = iTitleH + iSpacing + iMargin + iEditHeight + iSpacing;
		m_st_Item[TC_Imgt_Test_MRA2_R].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Imgt_Test_MRA2_R].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = (iWidthTemp + iSpacing) * 3 + iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Imgt_Test_MRA2_Z].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Imgt_Test_MRA2_Z].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);
		
		iLeft = (iWidthTemp + iSpacing) * 3 + iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Imgt_Chart_MRA2L_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Imgt_Chart_MRA2L_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = (iWidthTemp + iSpacing) * 3 + iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Imgt_Chart_MRA2R_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Imgt_Chart_MRA2R_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = (iWidthTemp + iSpacing) * 3 + iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Imgt_Par_MRA2L_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Imgt_Par_MRA2L_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = (iWidthTemp + iSpacing) * 3 + iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Imgt_Par_MRA2R_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Imgt_Par_MRA2R_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = (iWidthTemp + iSpacing) * 3 + iSpacing;
		iTop = cy - iEditHeight - iSpacing;
		m_bn_Item[BN_TC_SAVE].MoveWindow(iLeft, iTop, iWidthTemp, iEditHeight);
		break;
	case Sys_Focusing:
		m_st_Ikc.MoveWindow(iLeft, iTop, iWidthTemp, iEditHeight);

		iLeft += iWidthTemp + iSpacing;
		m_st_MRA2.MoveWindow(iLeft, iTop, iWidthTemp, iEditHeight);

		// IKC
		iLeft = iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Focus_IKC_CenterX].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Focus_IKC_CenterX].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Focus_IKC_CenterY].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Focus_IKC_CenterY].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Focus_IKC_CenterR].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Focus_IKC_CenterR].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		// MRA2
		iLeft = (iWidthTemp + iSpacing) * 1 + iSpacing;
		iTop = iTitleH + iSpacing + iMargin + iEditHeight + iSpacing;
		m_st_Item[TC_Focus_MRA_CenterX].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Focus_MRA_CenterX].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = (iWidthTemp + iSpacing) * 1 + iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Focus_MRA_CenterY].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Focus_MRA_CenterY].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = (iWidthTemp + iSpacing) * 1 + iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Focus_MRA_CenterR].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Focus_MRA_CenterR].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = (iWidthTemp + iSpacing) * 3 + iSpacing;
		iTop  = cy -  iEditHeight - iSpacing;
		m_bn_Item[BN_TC_SAVE].MoveWindow(iLeft, iTop, iWidthTemp, iEditHeight);

		break;
	case Sys_2D_Cal:
		for (UINT nIdx = 1; nIdx < m_nItemCnt + 1; nIdx++)
		{
			m_st_Item[nIdx - 1].MoveWindow(iLeft, iTop, iEdWidth + iSpacing * 3, iEditHeight);
			iLeft += iEdWidth + iSpacing * 3 + 2;

			m_ed_Item[nIdx - 1].MoveWindow(iLeft, iTop, iEdWidth - iSpacing * 3, iEditHeight);
			iLeft += iEdWidth - iSpacing * 1;

			if (nIdx % 4 == 0 && nIdx != 0)
			{
				iTop += iEditHeight + iSpacing;
				iLeft = iMargin;
			}
		}

		iBtnWidth = (iEdWidth + iSpacing) * 2;
		iLeft = cx - iSpacing - iBtnWidth;

		m_bn_Item[BN_TC_SAVE].MoveWindow(iLeft, iTop, iBtnWidth, iEditHeight);
		break;

	case Sys_Stereo_Cal:
		m_st_Comm.MoveWindow(iLeft, iTop, iWidthTemp, iEditHeight);

		iLeft += iWidthTemp + iSpacing;
		m_st_Fornt.MoveWindow(iLeft, iTop, iWidthTemp, iEditHeight);

		iLeft += iWidthTemp + iSpacing;
		m_st_Ikc.MoveWindow(iLeft, iTop, iWidthTemp, iEditHeight);

		iLeft += iWidthTemp + iSpacing;
		m_st_MRA2.MoveWindow(iLeft, iTop, iWidthTemp, iEditHeight);

		// COMM
		iLeft = iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Stereo_Load_Y].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Stereo_Load_Y].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Stereo_ShutterOpen].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Stereo_ShutterOpen].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Stereo_ShutterClose].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Stereo_ShutterClose].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		// FRONT
		iLeft = (iWidthTemp + iSpacing) * 1 + iSpacing;
		iTop = iTitleH + iSpacing + iMargin + iEditHeight + iSpacing;
		m_st_Item[TC_Stereo_FRONT_Distance].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Stereo_FRONT_Distance].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = (iWidthTemp + iSpacing) * 1 + iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Stereo_FRONT_Center_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Stereo_FRONT_Center_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = (iWidthTemp + iSpacing) * 1 + iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Stereo_FRONT_Center_Z].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Stereo_FRONT_Center_Z].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		// IKC
		iLeft = (iWidthTemp + iSpacing) * 2 + iSpacing;
		iTop   = iTitleH + iSpacing + iMargin + iEditHeight + iSpacing;
		m_st_Item[TC_Stereo_IKC_Distance].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Stereo_IKC_Distance].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = (iWidthTemp + iSpacing) * 2 + iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Stereo_IKC_Center_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Stereo_IKC_Center_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = (iWidthTemp + iSpacing) * 2 + iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Stereo_IKC_Center_Z].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Stereo_IKC_Center_Z].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		//	MRA2
		iLeft = (iWidthTemp + iSpacing) * 3 + iSpacing;
		iTop = iTitleH + iSpacing + iMargin + iEditHeight + iSpacing;
		m_st_Item[TC_Stereo_MRA_Distance].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Stereo_MRA_Distance].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = (iWidthTemp + iSpacing) * 3 + iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Stereo_MRA_Center_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Stereo_MRA_Center_X].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = (iWidthTemp + iSpacing) * 3 + iSpacing;
		iTop += iEditHeight + iSpacing;
		m_st_Item[TC_Stereo_MRA_Center_Z].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft += iWidthItem + iSpacing;
		m_ed_Item[TC_Stereo_MRA_Center_Z].MoveWindow(iLeft, iTop, iWidthItem, iEditHeight);

		iLeft = (iWidthTemp + iSpacing) * 3 + iSpacing;
		iTop += iEditHeight + iSpacing;
		m_bn_Item[BN_TC_SAVE].MoveWindow(iLeft, iTop, iWidthTemp, iEditHeight);
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: OnBnClickedBnSave
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/30 - 9:56
// Desc.		:
//=============================================================================
void CWnd_TeachOp::OnBnClickedBnSave()
{
	// UI 데이터 수집
	GetUpdateData();

	// 수집된 데이터 저장
	m_pstDevice->SaveMotionInfo();

	CString szMaintenanceFullPath;

	m_fileMaintenance.SaveTeachFile(m_pstMaintenanceInfo->szMaintenanceFullPath, &m_pstMaintenanceInfo->stTeachInfo);
	SetUpdateData();
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/5/29 - 10:54
// Desc.		:
//=============================================================================
BOOL CWnd_TeachOp::PreTranslateMessage(MSG* pMsg)
{
	return CWnd::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/2/5 - 18:30
// Desc.		:
//=============================================================================
BOOL CWnd_TeachOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: SetUpdateItem
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/5 - 19:02
// Desc.		:
//=============================================================================
void CWnd_TeachOp::SetUpdateItem()
{
	switch (m_nSysType)
	{
	case Sys_Focusing:
		m_nItemCnt = TC_ALL_MAX;
		for (UINT nIdx = 0; nIdx < m_nItemCnt; nIdx++)
		{
			m_szTeacOpName[nIdx] = g_szTeachItem_Focus[nIdx];
		}
		break;
	case Sys_2D_Cal:
		m_nItemCnt = TC_ALL_MAX;
		for (UINT nIdx = 0; nIdx < m_nItemCnt; nIdx++)
		{
			m_szTeacOpName[nIdx] = g_szTeachItem_2D_CAL[nIdx];
		}
		break;
	case Sys_Image_Test:
		m_nItemCnt = TC_ALL_MAX;
		for (UINT nIdx = 0; nIdx < m_nItemCnt; nIdx++)
		{
			m_szTeacOpName[nIdx] = g_szTeachItem_Image[nIdx];
		}
		break;
	case Sys_Stereo_Cal:
		m_nItemCnt = TC_ALL_MAX;
		for (UINT nIdx = 0; nIdx < m_nItemCnt; nIdx++)
		{
			m_szTeacOpName[nIdx] = g_szTeachItem_Stereo_CAL[nIdx];
		}
		break;
	case Sys_3D_Cal:
// 		m_nItemCnt = TC_ALL_MAX;
// 		for (UINT nIdx = 0; nIdx < m_nItemCnt; nIdx++)
// 		{
// 			m_szTeacOpName[nIdx] = g_szTeachItem_3D_CAL[nIdx];
// 		}
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: SetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/7 - 16:38
// Desc.		:
//=============================================================================
void CWnd_TeachOp::SetUpdateData()
{

	if (m_pstMaintenanceInfo == NULL)
		return;

	CString szData;

	for (int _a = 0; _a < TC_ALL_MAX; _a++)
	{
		szData.Format(_T("%.1f"), m_pstMaintenanceInfo->stTeachInfo.dbTeachData[_a]);
		m_ed_Item[_a].SetWindowText(szData);
	}
}

//=============================================================================
// Method		: GetUpdateData
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/7 - 16:38
// Desc.		:
//=============================================================================
void CWnd_TeachOp::GetUpdateData()
{
	if (m_pstMaintenanceInfo == NULL)
		return;

	CString szData;

	for (int _a = 0; _a < TC_ALL_MAX; _a++)
	{
		m_ed_Item[_a].GetWindowText(szData);
		m_pstMaintenanceInfo->stTeachInfo.dbTeachData[_a] = _ttof(szData);
	}
}
