﻿//*****************************************************************************
// Filename	: 	TestManager_Test.h
// Created	:	2017/10/14 - 18:01
// Modified	:	2017/10/14
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#ifndef TestManager_Test_h__
#define TestManager_Test_h__

#pragma once

#include "Def_DataStruct.h"
#include "Def_ResultCode_Cm.h"
#include "Def_TestDevice.h"

#include "TI_CircleDetect.h"
#include "TI_SFR.h"
#include "TI_DefectPixel.h"
#include "TI_Particle.h"
#include "TI_Particle_Entry.h"
#include "TI_Shading.h"
#include "TI_SNR.h"
#include "TI_SNR_Light.h"
#include "TI_HotPixel.h"
#include "TI_FPN.h"
#include "TI_T3D_Depth.h"


class CTestManager_Test
{
public:
	CTestManager_Test();
	virtual ~CTestManager_Test();

	void SetPtr_RecipeInfo (__in const ST_RecipeInfo* pstRecipeinfo)
	{
		if (NULL == pstRecipeinfo)
			return;

		m_pstRecipeinfo = pstRecipeinfo;
	};

	void OpticalCenterColor(__in enMarkColor eMarkColor)
	{
		m_eMarkColor = eMarkColor;
	};


	LRESULT CenterPointFunc				(__in LPBYTE pImage8bitBuf, __in int iImgW, __in int iImgH, __in ST_OpticalCenter_Opt stTestOpt, __out ST_OpticalCenter_Data &stTestData, __in int iCenterX = 0, __in int iCenterY = 0, __in UINT nParaID = 0);
	void CamType_CenterPoint			(__in int iImgW, __in int iImgH, __out int &nPosX, __out  int &nPosY);

	LRESULT RotateFunc					(__in LPBYTE pImage8bitBuf, __in int iImgW, __in int iImgH, __in ST_Rotate_Opt stTestOpt, __out ST_Rotate_Data &stTestData);
	double Vertical_GetRotationCheck	(double pt_x1, double pt_y1, double pt_x2, double pt_y2);
	double Horizontal_GetRotationCheck	(double pt_x1, double pt_y1, double pt_x2, double pt_y2);

	LRESULT FovFunc						(__in LPBYTE pImage8bitBuf, __in int iImgW, __in int iImgH, __in ST_FOV_Opt stTestOpt, __out ST_FOV_Data &stTestData);
	double Degree_Sum					(int NUM, double Distance);

	LRESULT DistortionFunc				(__in LPBYTE pImage8bitBuf, __in int iImgW, __in int iImgH, __in ST_Distortion_Opt stTestOpt, __out ST_Distortion_Data &stTestData);
	double GetDistance					(int x1, int y1, int x2, int y2);

	LRESULT SFRFunc						(__in LPBYTE pImage8bitBuf, __in LPWORD pImage12bitBuf, __in int iImgW, __in int iImgH, __in ST_SFR_Opt stTestOpt, __out ST_SFR_Data &stTestData);
	void OnTestProcess_SFRROI			(__in IplImage* pFrameImage, __in ST_SFR_Opt stOpt,  __out ST_SFR_Data &stTestData);

	LRESULT DefectPixelFunc				(__in LPBYTE pImage8bitBuf, __in LPWORD pImage12bitBuf, __in int iImgW, __in int iImgH, __in UINT nLightMode,  __in stDPList& stDefectList, __in ST_DefectPixel_Opt stTestOpt, __out ST_DefectPixel_Data &stTestData);
	LRESULT ParticleFunc				(__in LPBYTE pImage8bitBuf, __in LPWORD pImage12bitBuf, __in int iImgW, __in int iImgH, __in ST_Particle_Opt	stTestOpt, __out ST_Particle_Data	 &stTestData);
	LRESULT IntensityFunc				(__in LPBYTE pImage8bitBuf, __in LPWORD pImage12bitBuf, __in int iImgW, __in int iImgH, __in ST_Intensity_Opt	stTestOpt, __out ST_Intensity_Data	 &stTestData);
	LRESULT ShadingFunc					(__in LPBYTE pImage8bitBuf, __in LPWORD pImage12bitBuf, __in int iImgW, __in int iImgH, __in ST_Shading_Opt		stTestOpt, __out ST_Shading_Data	 &stTestData);
	UINT GetItemIndexNumber				(UINT nType, UINT nRow);
	LRESULT SNR_LightFunc				(__in LPBYTE pImage8bitBuf, __in LPWORD pImage12bitBuf, __in int iImgW, __in int iImgH, __in ST_SNR_Light_Opt	stTestOpt, __out ST_SNR_Light_Data	 &stTestData);
	LRESULT Partiecl_SNRFunc			(__in LPBYTE pImage8bitBuf, __in LPWORD pImage12bitBuf, __in int iImgW, __in int iImgH, __in UINT nLightMode, __in ST_Dynamic_Opt	stTestOpt, __out ST_Dynamic_Data &stTestData);
	LRESULT HotPixelFunc				(__in LPBYTE pImage8bitBuf, __in LPWORD pImage12bitBuf, __in int iImgW, __in int iImgH, __in ST_HotPixel_Opt	stTestOpt, __out ST_HotPixel_Data	 &stTestData);
	LRESULT FPNFunc						(__in LPBYTE pImage8bitBuf, __in LPWORD pImage12bitBuf, __in int iImgW, __in int iImgH, __in ST_FPN_Opt	stTestOpt, __out ST_FPN_Data	 &stTestData);
	LRESULT T3D_depthFunc				(__in LPBYTE pImage8bitBuf, __in LPWORD pImage12bitBuf, __in int iImgW, __in int iImgH, __in ST_3D_Depth_Opt	stTestOpt, __out ST_3D_Depth_Data	 &stTestData);
	LRESULT Particle_EntryFunc			(__in LPBYTE pImage8bitBuf, __in LPWORD pImage12bitBuf, __in int iImgW, __in int iImgH, __in ST_Particle_Entry_Opt	stTestOpt, __out ST_Particle_Entry_Data	 &stTestData);

	BOOL	Get_MeasurmentData			(__in BOOL bMinUse, __in BOOL bMaxUse, __in double dbMinSpec, __in double dbMaxSpe, __in double dbValue);
	BOOL	Get_MeasurmentData			(__in BOOL bMinUse, __in BOOL bMaxUse, __in int iMinSpec, __in int iMaxSpec,__in int iValue);
	
	enMarkColor					m_eMarkColor = MCol_Black;
protected:

	const ST_RecipeInfo*		m_pstRecipeinfo	= NULL;

	//-Test 항목
	//-원을 잡는 Test
	CTI_CircleDetect			m_TI_CircleDetect;
	CTI_SFR						m_TI_SFR;
	CTI_DefectPixel				m_TI_DefectPixel;
	CTI_Particle				m_TI_Particle;
	CTI_Particle_Entry			m_TI_Particle_Entry;
	CTI_Shading					m_TI_Shading;
	CTI_SNR						m_TI_SNR;
	CTI_SNR_Light				m_TI_SNR_Light;
	CTI_HotPixel				m_TI_HotPixel;
	CTI_FPN						m_TI_FPN;
	CTI_T3D_Depth				m_TI_3D_Depth;
};			   

#endif // TestManager_Test_h__
