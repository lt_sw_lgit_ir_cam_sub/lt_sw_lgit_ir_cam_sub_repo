﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_StdInfo.cpp
// Created	:	2017/1/3 - 10:39
// Modified	:	2017/1/3 - 10:39
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "Wnd_Cfg_StdInfo.h"


IMPLEMENT_DYNAMIC(CWnd_Cfg_StdInfo, CWnd_BaseView)

CWnd_Cfg_StdInfo::CWnd_Cfg_StdInfo()
{
	VERIFY(m_font_Default.CreateFont(
		20,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	VERIFY(m_font_Data.CreateFont(
		24,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}


CWnd_Cfg_StdInfo::~CWnd_Cfg_StdInfo()
{
	m_font_Default.DeleteObject();
	m_font_Data.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_StdInfo, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

// CWnd_Cfg_StdInfo message handlers
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
int CWnd_Cfg_StdInfo::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
void CWnd_Cfg_StdInfo::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

// 	int iMagrin = 10;
// 	int iSpacing = 5;
// 	int iCateSpacing = 10;
// 
// 	int iLeft = iMagrin;
// 	int iTop = iMagrin;
// 	int iWidth = cx - iMagrin - iMagrin;
// 	int iHeight = cy - iMagrin - iMagrin;

	

}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_StdInfo::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_BaseView::PreCreateWindow(cs);
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_StdInfo::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class

	return CWnd_BaseView::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: SetRecipeInfo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_RecipeInfo * pRecipeInfo
// Qualifier	:
// Last Update	: 2017/1/4 - 10:07
// Desc.		:
//=============================================================================
void CWnd_Cfg_StdInfo::SetRecipeInfo(__in const ST_RecipeInfo* pRecipeInfo)
{

}

void CWnd_Cfg_StdInfo::GetRecipeInfo(__out ST_RecipeInfo& stRecipeInfo)
{

}
