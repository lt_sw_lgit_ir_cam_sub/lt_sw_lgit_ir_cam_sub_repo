﻿//*****************************************************************************
// Filename	: 	TestManager_EQP_3DCal.h
// Created	:	2016/5/9 - 13:31
// Modified	:	2016/07/22
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef TestManager_EQP_3DCal_h__
#define TestManager_EQP_3DCal_h__

#pragma once

#include "TestManager_EQP.h"

#define DELAY_3DEPTH		200

//=============================================================================
// CTestManager_EQP_3DCal
//=============================================================================
class CTestManager_EQP_3DCal : public CTestManager_EQP
{
public:
	CTestManager_EQP_3DCal();
	virtual ~CTestManager_EQP_3DCal();

protected:

	//-----------------------------------------------------
	// 옵션
	//-----------------------------------------------------
	// 환경설정 데이터 불러오기
	virtual BOOL	OnLoadOption					();	
	virtual void	InitDevicez						(__in HWND hWndOwner = NULL);
	//virtual void	OnInitUISetting					(__in HWND hWndOwner = NULL);


	//-----------------------------------------------------
	// 쓰레드 작업 처리
	//-----------------------------------------------------

	// Load/Unload 쓰레드 시작
	virtual LRESULT	StartOperation_LoadUnload		(__in BOOL bLoad);
	// 검사 쓰레드 시작
	virtual LRESULT	StartOperation_Inspection		(__in UINT nParaIdx = 0);
	// 수동 모드일때 자동화 처리용 함수
	virtual LRESULT	StartOperation_Manual			(__in UINT nStepIdx, __in UINT nParaIdx = 0);

	// 수동 모드일때 검사항목 처리용 함수
	virtual LRESULT	StartOperation_InspManual		(__in UINT nParaIdx = 0);

	// 자동으로 처리되고 있는 쓰레드 강제 종료
	virtual void	StopProcess_LoadUnload			();
	virtual void	StopProcess_Test_All			();
	virtual void	StopProcess_InspManual			(__in UINT nParaIdx = 0);

	// 쓰레드 내에서 처리될 자동 작업 함수
	virtual BOOL	AutomaticProcess_LoadUnload		(__in BOOL bLoad);
	virtual void	AutomaticProcess_Test_All		(__in UINT nParaIdx = 0);
	virtual void	AutomaticProcess_Test_InspManual(__in UINT nParaIdx = 0);

	// 팔레트 로딩/언로딩 초기 세팅
	virtual void	OnInitial_LoadUnload			(__in BOOL bLoad);
	// 팔레트 로딩/언로딩 종료 세팅
	virtual void	OnFinally_LoadUnload			(__in BOOL bLoad);
	// 개별 검사 작업 시작
	virtual LRESULT	OnStart_LoadUnload				(__in BOOL bLoad);

	// 전체 테스트 초기 세팅
	virtual void	OnInitial_Test_All				(__in UINT nParaIdx = 0);
	// 전체 테스트 종료 세팅
	virtual void	OnFinally_Test_All				(__in LRESULT lResult, __in UINT nParaIdx = 0);
	// 전체 검사 작업 시작
	virtual LRESULT	OnStart_Test_All				(__in UINT nParaIdx = 0);
	
	// 개별 테스트 동작항목
	virtual void	OnInitial_Test_InspManual		(__in UINT nTestItemID, __in UINT nParaIdx = 0);
	virtual void	OnFinally_Test_InspManual		(__in UINT nTestItemID, __in LRESULT lResult, __in UINT nParaIdx = 0);
	virtual LRESULT	OnStart_Test_InspManual			(__in UINT nTestItemID, __in UINT nParaIdx = 0);
	
	// 검사
	virtual LRESULT Load_Product					();
	virtual LRESULT Unload_Product					();

	// 검사기별 검사
	virtual LRESULT	StartTest_Equipment				(__in UINT nParaIdx = 0);
	virtual LRESULT	StartTest_3D_CAL				(__in UINT nParaIdx = 0);

	
	// 쓰레드 내에서 처리될 자동 작업 함수
	virtual void	AutomaticProcess_TestItem		(__in UINT nParaIdx, __in UINT nTestItemID, __in UINT nStepIdx, __in UINT nCornerStep = 0);

	virtual LRESULT	StartTest_3D_CAL_TestItem		(__in UINT nTestItemID, __in UINT nStepIdx, __in WORD wAlpha = 0, __in UINT nCornerStep = 0, __in UINT nParaIdx = 0);

	// 공용 검사항목
	virtual	LRESULT _TI_Cm_Initialize				(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual	LRESULT _TI_Cm_Finalize					(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual	LRESULT _TI_Cm_MeasurCurrent			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual	LRESULT _TI_Cm_MeasurVoltage			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual	LRESULT	_TI_Cm_CaptureImage				(__in UINT nStepIdx, __in UINT nParaIdx = 0, __in BOOL bSaveImage = FALSE, __in LPCTSTR szFileName = NULL);
	virtual	LRESULT	_TI_Cm_CameraRegisterSet		(__in UINT nStepIdx, __in UINT nRegisterType, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_Cm_CameraAlphaSet			(__in UINT nStepIdx, __in UINT nAlpha, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_Cm_CameraPowerOnOff			(__in UINT nStepIdx, __in enPowerOnOff nOnOff, __in UINT nParaIdx = 0);
	
	// 3D CAL 검사항목
	virtual	LRESULT	_TI_3DCAL_SetParameter			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual	LRESULT	_TI_3DCAL_CornerExt				(__in UINT nStepIdx, __in UINT nCornerStep, __in UINT nParaIdx = 0);
	virtual	LRESULT	_TI_3DCAL_Depth_SendParameter	(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual	LRESULT	_TI_3DCAL_Depth_AlphaCapture	(__in UINT nStepIdx, __in WORD wAlpha, __in UINT nCornerStep, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_3DCAL_Depth_Result			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_3DCAL_Depth_JudgeResult		(__in UINT nStepIdx, __in UINT nTestItem, __in UINT nParaIdx = 0);
	virtual	LRESULT	_TI_3DCAL_Eval_SendParameter	(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual	LRESULT	_TI_3DCAL_Eval_AlphaCapture		(__in UINT nStepIdx, __in WORD wAlpha, __in UINT nCornerStep, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_3DCAL_Eval_Result			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_3DCAL_Eval_JudgeResult		(__in UINT nStepIdx, __in UINT nTestItem, __in UINT nParaIdx = 0);

	// MES 통신
	virtual LRESULT	MES_CheckBarcodeValidation		(__in UINT nParaIdx = 0);
	virtual LRESULT MES_SendInspectionData			(__in UINT nParaIdx = 0);
	virtual LRESULT MES_MakeInspecData_3D_CAL		(__in UINT nParaIdx = 0);

	// 검사 결과에 따른 메세지 팝업
	virtual void	OnPopupMessageTest_All			(__in enResultCode nResultCode);

	//-----------------------------------------------------
	// Motion
	//-----------------------------------------------------	
	virtual LRESULT	OnMotion_MoveToTestPos			(__in UINT nParaIdx = 0);
	virtual	LRESULT OnMotion_MoveToStandbyPos		(__in UINT nParaIdx = 0);
	virtual LRESULT	OnMotion_MoveToUnloadPos		(__in UINT nParaIdx = 0);

	virtual LRESULT	OnMotion_Origin					();
	virtual LRESULT	OnMotion_LoadProduct			();
	virtual LRESULT	OnMotion_UnloadProduct			();
	virtual LRESULT	OnMotion_3DCAL_Test				(__in double dbDistanceY, __in UINT nParaIdx = 0);
 	virtual LRESULT	OnMotion_3DCAL_Hand				(__in BOOL bHandIn, __in UINT nParaIdx = 0);

	//-----------------------------------------------------
	// Digital I/O
	//-----------------------------------------------------
	// Digital I/O 신호 UI 처리
	virtual void	OnDIO_UpdateDInSignal			(__in BYTE byBitOffset, __in BOOL bOnOff){};
	virtual void	OnDIO_UpdateDOutSignal			(__in BYTE byBitOffset, __in BOOL bOnOff){};
	// I/O 신호 감지 및 신호별 기능 처리
	virtual void	OnDIn_DetectSignal				(__in BYTE byBitOffset, __in BOOL bOnOff);
	virtual void	OnDIn_DetectSignal_SteCal		(__in BYTE byBitOffset, __in BOOL bOnOff);
	// Digital I/O 초기화
	virtual void	OnDIO_InitialSignal				();

	virtual LRESULT	OnDIn_MainPower					();
	virtual LRESULT	OnDIn_EMO						();
	virtual LRESULT	OnDIn_AreaSensor				();
	virtual LRESULT	OnDIn_DoorSensor				();
	virtual LRESULT	OnDIn_JIGCoverCheck				();
	virtual LRESULT	OnDIn_Start						();
	virtual LRESULT	OnDIn_Stop						();
	virtual LRESULT	OnDIn_CheckSafetyIO				();
	virtual LRESULT	OnDIn_CheckJIGCoverStatus		();
		
	virtual LRESULT	OnDOut_BoardPower				(__in BOOL bOn, __in UINT nParaIdx = 0);
	virtual LRESULT	OnDOut_FluorescentLamp			(__in BOOL bOn);
	virtual LRESULT	OnDOut_StartLamp				(__in BOOL bOn);
	virtual LRESULT	OnDOut_StopLamp					(__in BOOL bOn);
	virtual LRESULT	OnDOut_TowerLamp				(__in enLampColor nLampColor, __in BOOL bOn);
	virtual LRESULT	OnDOut_Buzzer					(__in BOOL bOn);

	// DAQ 보드
	virtual LRESULT	OnDAQ_EEPROM_Write				(__in UINT nIdxBrd = 0);
	virtual LRESULT	OnDAQ_EEPROM_OMS_Entry_3D		(__in UINT nIdxBrd = 0);
	virtual LRESULT	OnDAQ_EEPROM_OMS_Front_3D		(__in UINT nIdxBrd = 0);
	virtual LRESULT	OnDAQ_EEPROM_OMS_FrontSet_3D	(__in UINT nIdxBrd = 0);

	//-----------------------------------------------------
	// 카메라 보드 / 광원 제어 보드
	//-----------------------------------------------------
 	

	//-----------------------------------------------------
	// 메뉴얼 테스트
	//-----------------------------------------------------
	virtual LRESULT OnManualSequence				(__in UINT nParaID, __in enParaManual enFunc);
	virtual	LRESULT OnManualTestItem				(__in UINT nParaID, __in UINT nStepIdx);

	//-----------------------------------------------------
	// 테스트
	//-----------------------------------------------------
	// 주변기기 통신 상태 체크
	virtual LRESULT	OnCheck_DeviceComm				();
	// 기구물 안전 체크
	virtual LRESULT	OnCheck_SaftyJIG				();	
	// 모델 설정 데이터가 정상인가 판단
	virtual LRESULT	OnCheck_RecipeInfo				();	
	
	// 제품 결과 판정 및 리포트 처리
	virtual void	OnJugdement_And_Report			(__in UINT nParaIdx = 0);
	virtual void	OnJugdement_And_Report_All		();
	
	//-----------------------------------------------------
	// 타이머 
	//-----------------------------------------------------
	virtual void	CreateTimer_UpdateUI			(__in DWORD DueTime = 5000, __in DWORD Period = 250);
	virtual void	DeleteTimer_UpdateUI			();
	virtual void	OnMonitor_TimeCheck				();
	virtual void	OnMonitor_UpdateUI				();
	
	//-----------------------------------------------------
	// Worklist 처리
	//-----------------------------------------------------
	virtual void	OnSaveWorklist					();
	
	//-----------------------------------------------------
	// 멤버 변수 & 클래스
	//-----------------------------------------------------

public:
	
	// 생성자 처리용 코드
	virtual void		OnInitialize				();
	// 소멸자 처리용 코드
	virtual	void		OnFinalize					();

	// 제어 권한 상태 설정
	virtual void		SetPermissionMode			(__in enPermissionMode nAcessMode);
	
	
};

#endif // TestManager_EQP_3DCal_h__

