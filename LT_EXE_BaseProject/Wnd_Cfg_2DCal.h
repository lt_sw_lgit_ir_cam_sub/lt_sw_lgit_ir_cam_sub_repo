
// CWnd_Cfg_2DCal
#ifndef Wnd_Cfg_2DCal_h__
#define Wnd_Cfg_2DCal_h__

#pragma once

#include "VGStatic.h"
#include "Def_T_2DCal.h"
#include "List_2DCal_ROI.h"

class CWnd_Cfg_2DCal : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Cfg_2DCal)

public:
	CWnd_Cfg_2DCal();
	virtual ~CWnd_Cfg_2DCal();

	typedef enum enParamBtROI
	{
		PROI_2DCal_Test = 0,
		PROI_2DCal_Result,
		PROI_BT_MaxNum
	};

protected:
	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);	
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);

	afx_msg void	OnRangeBtnCtrl		(__in UINT nID);

	DECLARE_MESSAGE_MAP()
	CFont				m_font;

	// 세팅 파라미터 옵션
	CVGStatic			m_st_Parameter[Param_2DCal_MaxNum];
	CMFCMaskedEdit		m_ed_Parameter[Param_2DCal_MaxNum];

	// ROI 영역
	CList_2DCal_ROI		m_lst_2DCalROI;

	// * BPF Version (16 Byte) : 0x6E
	CVGStatic			m_st_BPFVer;
	CMFCMaskedEdit		m_ed_BPFVer;
	// * Gate Driver Version (16 Byte) : 0x7E
	CVGStatic			m_st_GateDrvVer;
	CMFCMaskedEdit		m_ed_GateDrvVer;

	CMFCButton			m_bt_Temperature;
		

	enInsptrSysType		m_InspectionType = enInsptrSysType::Sys_2D_Cal;

public:
	void	Set_TestItemInfo	(__in ST_TestItemInfo* pstTestItemInfo);
	void	Get_TestItemInfo	(__out ST_TestItemInfo& stOutTestItemInfo);

	void	Set_2DCal_Para		(__in ST_2DCal_Para* pst2DCal_Para);
	void	Get_2DCal_Para		(__out ST_2DCal_Para& stOut2DCal_Para);

	void	SetSystemType		(__in enInsptrSysType nSysType);

	afx_msg void OnBnClickedTemperature();
};

#endif // Wnd_Cfg_2DCal_h__

