﻿#include <afxwin.h>
#include "stdafx.h"
#include "TestManager_TestMES.h"



CTestManager_TestMES::CTestManager_TestMES()
{
}

CTestManager_TestMES::~CTestManager_TestMES()
{
}

void CTestManager_TestMES::Current_DataSave(int nParaIdx, ST_MES_Data *pMesData)
{
	//-전류 1.8V, 2.8V
	CString strData;
	CString strFullData;

	strFullData.Empty();

	int nCurrentCnt = 2;

	/*if ((m_pInspInfo->RecipeInfo.ModelType == Model_OMS_Front) || (m_pInspInfo->RecipeInfo.ModelType == Model_OMS_Front) || (m_pInspInfo->RecipeInfo.ModelType == Model_OMS_Entry))
		nCurrentCnt = Spec_ECurrent_Max;
*/
	for (UINT t = 0; t < nCurrentCnt; t++)
	{
		strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stECurrentData.dbValue[t], m_pInspInfo->CamInfo[nParaIdx].stImageQ.stECurrentData.nEachResult[t]);

		if (t < (nCurrentCnt - 1))
		{
			strData += _T(",");
		}
		strFullData += strData;
	}
	pMesData->szMesTestData[nParaIdx][MesDataIdx_ECurrent] = strFullData;
}

void CTestManager_TestMES::OpticalCenter_DataSave(int nParaIdx, ST_MES_Data *pMesData)
{
	//-광축 X, Y
	CString strData;
	CString strFullData;

	strFullData.Empty();
	//@SH _181108: LGIT에서 nResultPosX -> Offset으로 저장해달라고 변경 요청했으나 이미 Offset으로 남김
	strData.Format(_T("%d:%d,"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stOpticalCenterData.iStandPosDevX, m_pInspInfo->CamInfo[nParaIdx].stImageQ.stOpticalCenterData.nResultX);
	strFullData += strData;

	strData.Format(_T("%d:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stOpticalCenterData.iStandPosDevY, m_pInspInfo->CamInfo[nParaIdx].stImageQ.stOpticalCenterData.nResultY);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesDataIdx_OpticalCenter] = strFullData;
}

void CTestManager_TestMES::SFR_DataSave(int nParaIdx, ST_MES_Data *pMesData, int nTypeCnt)
{
	CString strData;
	CString strFullData;

	strFullData.Empty();

	//for (int t = 0; t < ITM_SFR_TiltMax; t++)
	for (int t = 0; t < ROI_SFR_TILT_Max; t++)
	{
		if (m_pInspInfo->RecipeInfo.stImageQ.stSFROpt[nTypeCnt].stInput_Tilt[t].bEnable)
		{
			strData.Format(_T("%.2f:%d,"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbTiltdata[t], m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].nEachTiltResult[t]);
		}
		else{
			strData.Format(_T("Ｘ:1,"));
		}
		strFullData += strData;
	}


	//for (int t = 0; t < ITM_SFR_Max; t++)
	for (int t = 0; t < ROI_SFR_GROUP_Max; t++)
	{
		//strData.Format(_T("%.2f:%d,"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbMinValue[t], m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].nEachResult_G[t]);
		if (m_pInspInfo->RecipeInfo.stImageQ.stSFROpt[nTypeCnt].stInput_Group[t].bEnable)
		{
			strData.Format(_T("%.2f:%d,"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbEachResultGroup[t], m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].nEachResult_G[t]);
		}
		else{
			strData.Format(_T("Ｘ:1,"));
		}
		strFullData += strData;
	}

	for (int t = 0; t < ROI_SFR_Max; t++)
	{

		if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].bUse)
		{
			strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbValue[t], m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].nEachResult[t]);
		}
		else{
			strData.Format(_T(":1"));
		}
		if (t < (ROI_SFR_Max-1))
		{
			strData += _T(",");
		}
		strFullData += strData;
	}
	switch (nTypeCnt)
	{
	case 0:
		pMesData->szMesTestData[nParaIdx][MesDataIdx_SFR_A] = strFullData;
		break;
	case 1:
		pMesData->szMesTestData[nParaIdx][MesDataIdx_SFR_B] = strFullData;
		break;
	case 2:
		pMesData->szMesTestData[nParaIdx][MesDataIdx_SFR_C] = strFullData;
		break;
	default:
		pMesData->szMesTestData[nParaIdx][MesDataIdx_SFR_A] = strFullData;
		break;
	}

}

void CTestManager_TestMES::Rotate_DataSave(int nParaIdx, ST_MES_Data *pMesData)
{
	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stRotateData.dbValue, m_pInspInfo->CamInfo[nParaIdx].stImageQ.stRotateData.nResult);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesDataIdx_Rotation] = strFullData;
}

void CTestManager_TestMES::Distortion_DataSave(int nParaIdx, ST_MES_Data *pMesData)
{
	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDistortionData.dbValue, m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDistortionData.nResult);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesDataIdx_Distortion] = strFullData;
}

void CTestManager_TestMES::Fov_DataSave(int nParaIdx, ST_MES_Data *pMesData)
{
	CString strData;
	CString strFullData;

	strFullData.Empty();
	for (int t = 0; t < 2; t++)
	{
		strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stFovData.dbValue[t], m_pInspInfo->CamInfo[nParaIdx].stImageQ.stFovData.nEachResult[t]);

		if (t < 1)
		{
			strData += _T(",");
		}
		strFullData += strData;
	}

	pMesData->szMesTestData[nParaIdx][MesDataIdx_FOV] = strFullData;
}


void CTestManager_TestMES::DynamicRange_DataSave(int nParaIdx, ST_MES_Data *pMesData)
{
	//-광축 X, Y
	CString strData;
	CString strFullData;

	strFullData.Empty();

	for (int k = 0; k < ROI_Par_SNR_Max; k++)
	{
		if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicData.bUse[k])
		{
			for (int t = 0; t < Light_Par_SNR_MAX; t++)
			{
				strData.Format(_T("%d:1,"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicData.sSignal[t][k]);
				strFullData += strData;
			}
			for (int t = 0; t < Light_Par_SNR_MAX; t++)
			{
				strData.Format(_T("%.2f:1,"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicData.fNoise[t][k]);
				strFullData += strData;
			}

			strData.Format(_T("%.2f:%d,"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicData.dbValueBW[k], m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicData.nEtcResultBW[k]);
			strFullData += strData;
			strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicData.dbValueDR[k], m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicData.nEtcResultDR[k]);


	
		}
		else{

			for (int t = 0; t < Light_Par_SNR_MAX; t++)
			{
				strData = _T(":1,");
				strFullData += strData;
			}
			for (int t = 0; t < Light_Par_SNR_MAX; t++)
			{
				strData = _T(":1,");
				strFullData += strData;
			}

			strData = _T(":1,");
			strFullData += strData;
			strData = _T(":1");
			strFullData += strData;

		}
	
		if (k < (ROI_Par_SNR_Max - 1))
		{
			strData += _T(",");
		}

		strFullData += strData;
	}
	
	

	

	pMesData->szMesTestData[nParaIdx][MesDataIdx_Particle_SNR] = strFullData;

}

void CTestManager_TestMES::Intensity_DataSave(int nParaIdx, ST_MES_Data *pMesData)
{
	CString strData;
	CString strFullData;

	strFullData.Empty();
	int nDataNum[5] = { ROI_Intencity_CC, ROI_Intencity_RT, ROI_Intencity_RB, ROI_Intencity_LT, ROI_Intencity_LB };

	for (int t = 0; t < ROI_Intencity_Max; t++)
	{
		strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stIntensityData.fSignal[nDataNum[t]], m_pInspInfo->CamInfo[nParaIdx].stImageQ.stIntensityData.nEachResult[nDataNum[t]]);
		strData += _T(",");
		strFullData += strData;

		strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stIntensityData.fPercent[nDataNum[t]], m_pInspInfo->CamInfo[nParaIdx].stImageQ.stIntensityData.nEachResult[nDataNum[t]]);
		if (t < (ROI_Intencity_Max-1))
		{
			strData += _T(",");
		}
		strFullData += strData;
	}
	pMesData->szMesTestData[nParaIdx][MesDataIdx_Intensity] = strFullData;
}

void CTestManager_TestMES::Stain_DataSave(int nParaIdx, ST_MES_Data *pMesData)
{
	CString strData;
	CString strFullData;

	strFullData.Empty();
	for (int t = 0; t < ROI_Particle_Max; t++)
	{
		strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stParticleData.dFailTypeConcentration[t], m_pInspInfo->CamInfo[nParaIdx].stImageQ.stParticleData.nResult);
		strData += _T(",");
		strFullData += strData;

		strData.Format(_T("%d:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stParticleData.nFailTypeCount[t], m_pInspInfo->CamInfo[nParaIdx].stImageQ.stParticleData.nResult);
		if (t < (ROI_Particle_Max - 1))
		{
			strData += _T(",");
		}
		strFullData += strData;
	}
	pMesData->szMesTestData[nParaIdx][MesDataIdx_Stain] = strFullData;
}

void CTestManager_TestMES::Defectpixel_DataSave(int nParaIdx, ST_MES_Data *pMesData)
{
	CString strData;
	CString strFullData;
	

	strFullData.Empty();
	for (int t = 0; t < Spec_Defect_Max; t++)
	{
		strData.Format(_T("%d:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDefectPixelData.nFailCount[t], m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDefectPixelData.nEachResult[t]);

		if (t < (Spec_Defect_Max-1))
		{
			strData += _T(",");
		}
		strFullData += strData;
	}

	pMesData->szMesTestData[nParaIdx][MesDataIdx_DefectPixel] = strFullData;
}

//=============================================================================
// Method		: SNR_Light_DataSave
// Access		: public  
// Returns		: void
// Parameter	: int nParaIdx
// Parameter	: ST_MES_Data * pMesData
// Qualifier	:
// Last Update	: 2018/5/12 - 19:47
// Desc.		:
//=============================================================================
void CTestManager_TestMES::SNR_Light_DataSave(int nParaIdx, ST_MES_Data *pMesData)
{
	CString strData;
	CString strFullData;


	strFullData.Empty();
	for (int t = 0; t < ROI_SNR_Light_Max; t++)
	{
		if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSNR_LightData.bUse[t])
		{
			strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSNR_LightData.dbEtcValue[t], m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSNR_LightData.nResult);
		}
		else{
			strData.Format(_T(":1"));
		}

		if (t < (ROI_SNR_Light_Max -1))
		{
			strData += _T(",");
		}
		strFullData += strData;
	}

	pMesData->szMesTestData[nParaIdx][MesDataIdx_SNR_Light] = strFullData;
}

void CTestManager_TestMES::Shading_DataSave(int nParaIdx, ST_MES_Data *pMesData)
{
	CString strData;
	CString strFullData;

// 	IDX_Shading_Ver_CT,
// 		IDX_Shading_Ver_CB,
// 		IDX_Shading_Hov_LC,
// 		IDX_Shading_Hov_RC,
// 		IDX_Shading_Dig_LT,
// 		IDX_Shading_Dig_LB,
// 		IDX_Shading_Dig_RT,
// 		IDX_Shading_Dig_R

	strFullData.Empty();

	CString strShadingData[Index_Shading_Max] = {_T(""),};

	for (UINT nIdx = 0; nIdx < Index_Shading_Max; nIdx++)
	{
		if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.bUse[nIndexVer_CT[nIdx]])
		{
			strFullData.Empty();
			strData.Format(_T("CT_%.2f_"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.fPercent[nIndexVer_CT[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%.2f:"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.sSignal[nIndexVer_CT[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.nEachResult[nIndexVer_CT[nIdx]]);
			if (nIdx < (Index_Shading_Max - 1))
			{
				strData + _T(",");
			}
			strFullData += strData;

			strShadingData[nIdx] += strFullData;
		}
		else{
			strShadingData[nIdx] += _T(":1");
			if (nIdx < (Index_Shading_Max - 1))
			{
				strShadingData[nIdx] + _T(",");
			}
		}
	}
	for (UINT nIdx = 0; nIdx < Index_Shading_Max; nIdx++)
	{
		if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.bUse[nIndexVer_CB[nIdx]])
		{
			strFullData.Empty();
			strData.Format(_T("CB_%.2f_"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.fPercent[nIndexVer_CB[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%.2f:"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.sSignal[nIndexVer_CB[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.nEachResult[nIndexVer_CB[nIdx]]);
			if (nIdx < (Index_Shading_Max - 1))
			{
				strData + _T(",");
			}
			strFullData += strData;

			strShadingData[nIdx] += strFullData;
		}
		else{
			strShadingData[nIdx] += _T(":1");
			if (nIdx < (Index_Shading_Max - 1))
			{
				strShadingData[nIdx] + _T(",");
			}
		}
	}
	for (UINT nIdx = 0; nIdx < Index_Shading_Max; nIdx++)
	{
		if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.bUse[nIndexHov_LC[nIdx]])
		{
			strFullData.Empty();
			strData.Format(_T("LC_%.2f_"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.fPercent[nIndexHov_LC[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%.2f:"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.sSignal[nIndexHov_LC[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.nEachResult[nIndexHov_LC[nIdx]]);
			if (nIdx < (Index_Shading_Max - 1))
			{
				strData + _T(",");
			}
			strFullData += strData;

			strShadingData[nIdx] += strFullData;
		}
		else{
			strShadingData[nIdx] += _T(":1");
			if (nIdx < (Index_Shading_Max - 1))
			{
				strShadingData[nIdx] + _T(",");
			}
		}
	}
	for (UINT nIdx = 0; nIdx < Index_Shading_Max; nIdx++)
	{
		if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.bUse[nIndexHov_RC[nIdx]])
		{
			strFullData.Empty();
			strData.Format(_T("RC_%.2f_"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.fPercent[nIndexHov_RC[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%.2f:"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.sSignal[nIndexHov_RC[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.nEachResult[nIndexHov_RC[nIdx]]);
			if (nIdx < (Index_Shading_Max - 1))
			{
				strData + _T(",");
			}
			strFullData += strData;

			strShadingData[nIdx] += strFullData;
		}
		else{
			strShadingData[nIdx] += _T(":1");
			if (nIdx < (Index_Shading_Max - 1))
			{
				strShadingData[nIdx] + _T(",");
			}
		}
	}
	for (UINT nIdx = 0; nIdx < Index_Shading_Max; nIdx++)
	{
		if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.bUse[nIndexDig_LT[nIdx]])
		{
			strFullData.Empty();
			strData.Format(_T("LT_%.2f_"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.fPercent[nIndexDig_LT[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%.2f:"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.sSignal[nIndexDig_LT[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.nEachResult[nIndexDig_LT[nIdx]]);
			if (nIdx < (Index_Shading_Max - 1))
			{
				strData + _T(",");
			}
			strFullData += strData;

			strShadingData[nIdx] += strFullData;
		}
		else{
			strShadingData[nIdx] += _T(":1");
			if (nIdx < (Index_Shading_Max - 1))
			{
				strShadingData[nIdx] + _T(",");
			}
		}
	}
	for (UINT nIdx = 0; nIdx < Index_Shading_Max; nIdx++)
	{
		if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.bUse[nIndexDig_LB[nIdx]])
		{
			strFullData.Empty();
			strData.Format(_T("LB_%.2f_"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.fPercent[nIndexDig_LB[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%.2f:"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.sSignal[nIndexDig_LB[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.nEachResult[nIndexDig_LB[nIdx]]);
			if (nIdx < (Index_Shading_Max - 1))
			{
				strData + _T(",");
			}
			strFullData += strData;

			strShadingData[nIdx] += strFullData;
		}
		else{
			strShadingData[nIdx] += _T(":1");
			if (nIdx < (Index_Shading_Max - 1))
			{
				strShadingData[nIdx] + _T(",");
			}
		}
	}
	for (UINT nIdx = 0; nIdx < Index_Shading_Max; nIdx++)
	{
		if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.bUse[nIndexDig_RT[nIdx]])
		{
			strFullData.Empty();
			strData.Format(_T("RT_%.2f_"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.fPercent[nIndexDig_RT[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%.2f:"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.sSignal[nIndexDig_RT[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.nEachResult[nIndexDig_RT[nIdx]]);
			if (nIdx < (Index_Shading_Max - 1))
			{
				strData + _T(",");
			}
			strFullData += strData;

			strShadingData[nIdx] += strFullData;
		}
		else{
			strShadingData[nIdx] += _T(":1");
			if (nIdx < (Index_Shading_Max - 1))
			{
				strShadingData[nIdx] + _T(",");
			}
		}

	}
	for (UINT nIdx = 0; nIdx < Index_Shading_Max; nIdx++)
	{
		if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.bUse[nIndexDig_RB[nIdx]])
		{
			strFullData.Empty();
			strData.Format(_T("RB_%.2f_"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.fPercent[nIndexDig_RB[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%.2f:"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.sSignal[nIndexDig_RB[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.nEachResult[nIndexDig_RB[nIdx]]);
			if (nIdx < (Index_Shading_Max - 1))
			{
				strData + _T(",");
			}
			strFullData += strData;

			strShadingData[nIdx] += strFullData;
		}
		else{
			strShadingData[nIdx] += _T(":1");
			if (nIdx < (Index_Shading_Max - 1))
			{
				strShadingData[nIdx] + _T(",");
			}
		}
	}


	for (int t = 0; t < Index_Shading_Max; t++)
	{

		strFullData += strShadingData[t];
	}
	if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.bUse[ROI_Shading_CNT])
	{
		strData.Format(_T("CC_%.2f_"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.fPercent[ROI_Shading_CNT]);
		strFullData += strData;
		strData.Format(_T("%.2f:"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.sSignal[ROI_Shading_CNT]);
		strFullData += strData;
		strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.nEachResult[ROI_Shading_CNT]);
		
		strFullData += strData;

	}
	else{
		strFullData += _T(":1");
	}

// 		stTestData.fPercent[nIndexVer_CT[nIdx]] = ((float)stTestData.sSignal[nIndexVer_CT[nIdx]] / (float)stTestData.sSignal[nStandardIndex[IDX_Shading_Ver_CT]] * 100.0f);
// 		stTestData.fPercent[nIndexVer_CB[nIdx]] = ((float)stTestData.sSignal[nIndexVer_CB[nIdx]] / (float)stTestData.sSignal[nStandardIndex[IDX_Shading_Ver_CB]] * 100.0f);
// 		stTestData.fPercent[nIndexHov_LC[nIdx]] = ((float)stTestData.sSignal[nIndexHov_LC[nIdx]] / (float)stTestData.sSignal[nStandardIndex[IDX_Shading_Hov_LC]] * 100.0f);
// 		stTestData.fPercent[nIndexHov_RC[nIdx]] = ((float)stTestData.sSignal[nIndexHov_RC[nIdx]] / (float)stTestData.sSignal[nStandardIndex[IDX_Shading_Hov_RC]] * 100.0f);
// 		stTestData.fPercent[nIndexDig_LT[nIdx]] = ((float)stTestData.sSignal[nIndexDig_LT[nIdx]] / (float)stTestData.sSignal[nStandardIndex[IDX_Shading_Dig_LT]] * 100.0f);
// 		stTestData.fPercent[nIndexDig_LB[nIdx]] = ((float)stTestData.sSignal[nIndexDig_LB[nIdx]] / (float)stTestData.sSignal[nStandardIndex[IDX_Shading_Dig_LB]] * 100.0f);
// 		stTestData.fPercent[nIndexDig_RT[nIdx]] = ((float)stTestData.sSignal[nIndexDig_RT[nIdx]] / (float)stTestData.sSignal[nStandardIndex[IDX_Shading_Dig_RT]] * 100.0f);
// 		stTestData.fPercent[nIndexDig_RB[nIdx]] = ((float)stTestData.sSignal[nIndexDig_RB[nIdx]] / (float)stTestData.sSignal[nStandardIndex[IDX_Shading_Dig_RB]] * 100.0f);
	
// 	for (int t = 0; t < ROI_SNR_Light_Max; t++)
// 	{
// 		if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.bUse[t])
// 		{
// 			strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSNR_LightData.dbEtcValue[t], m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSNR_LightData.nResult);
// 		}
// 		else{
// 			strData.Format(_T(":1"));
// 		}
// 
// 		if (t < (ROI_SNR_Light_Max - 1))
// 		{
// 			strData += _T(",");
// 		}
// 		strFullData += strData;
// 	}

	pMesData->szMesTestData[nParaIdx][MesDataIdx_SNR_Light] = strFullData;
}

//=============================================================================
// Method		: Current_DataSave_Foc
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in ST_MES_Data_Foc * pMesData
// Qualifier	:
// Last Update	: 2018/3/9 - 20:13
// Desc.		:
//=============================================================================
void CTestManager_TestMES::Current_DataSave_Foc(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData)
{
	if (NULL == pMesData)
		return;

	//-전류 1.8V, 2.8V
	CString strData;
	CString strFullData;

	strFullData.Empty();

	int nCurrentCnt = 2;

	if ((m_pInspInfo->RecipeInfo.ModelType == Model_OMS_Front) || (m_pInspInfo->RecipeInfo.ModelType == Model_OMS_Front) || (m_pInspInfo->RecipeInfo.ModelType == Model_OMS_Entry))
		nCurrentCnt = Spec_ECurrent_Max;

	for (UINT nCh = 0; nCh < nCurrentCnt; nCh++)
	{
		strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stECurrentData.dbValue[nCh], m_pInspInfo->CamInfo[nParaIdx].stFocus.stECurrentData.nEachResult[nCh]);

		if (nCh < (nCurrentCnt - 1))
		{
			strData += _T(",");
		}

		strFullData += strData;
	}

	pMesData->szMesTestData[nParaIdx][MesDataIdx_Foc_ECurrent] = strFullData;
}

//=============================================================================
// Method		: OpticalCenter_DataSave_Foc
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in ST_MES_Data_Foc * pMesData
// Qualifier	:
// Last Update	: 2018/3/9 - 20:14
// Desc.		:
//=============================================================================
void CTestManager_TestMES::OpticalCenter_DataSave_Foc(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData)
{
	if (NULL == pMesData)
		return;

	//-광축 X, Y
	CString strData;
	CString strFullData;

	strFullData.Empty();
	//@SH _181108: LGIT에서 nResultPosX -> Offset으로 저장해달라고 변경 요청했으나 이미 Offset으로 남김
	strData.Format(_T("%d:%d,"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stOpticalCenterData.iStandPosDevX, m_pInspInfo->CamInfo[nParaIdx].stFocus.stOpticalCenterData.nResultX);
	strFullData += strData;

	strData.Format(_T("%d:%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stOpticalCenterData.iStandPosDevY, m_pInspInfo->CamInfo[nParaIdx].stFocus.stOpticalCenterData.nResultY);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesDataIdx_Foc_OpticalCenter] = strFullData;
}

//=============================================================================
// Method		: SFR_DataSave_Foc
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in ST_MES_Data_Foc * pMesData
// Qualifier	:
// Last Update	: 2018/3/9 - 20:14
// Desc.		:
//=============================================================================
void CTestManager_TestMES::SFR_DataSave_Foc(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData)
{
	if (NULL == pMesData)
		return;
	CString strData;
	CString strFullData;

	strFullData.Empty();
	//--------------------------------------------------20181210 SH2 수정
	//for (int t = 0; t < ITM_SFR_TiltMax; t++)
	for (int t = 0; t < ROI_SFR_TILT_Max; t++)
	{
		if (m_pInspInfo->RecipeInfo.stFocus.stSFROpt.stInput_Tilt[t].bEnable)
		{
			strData.Format(_T("%.2f:%d,"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFRData.dbTiltdata[t], m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFRData.nEachTiltResult[t]);
		}
		else{
			strData.Format(_T("Ｘ:1,"));
		}
		strFullData += strData;
	}

	//for (int t = 0; t < ITM_SFR_Max; t++)
	for (int t = 0; t < ROI_SFR_GROUP_Max; t++)
	{
		//strData.Format(_T("%.2f:%d,"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbMinValue[t], m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].nEachResult_G[t]);
		if (m_pInspInfo->RecipeInfo.stFocus.stSFROpt.stInput_Group[t].bEnable)
		{
			strData.Format(_T("%.2f:%d,"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFRData.dbEachResultGroup[t], m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFRData.nEachResult_G[t]);
		}
		else{
			strData.Format(_T("Ｘ:1,"));
		}
		strFullData += strData;
	}
	//----------------------------------------------------------------20181210
	//for (int t = 0; t < ITM_SFR_TiltMax; t++)
	//{
	//	strData.Format(_T("%.2f:%d,"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFRData.dbTiltdata[t], m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFRData.nEachTiltResult[t]);
	//	strFullData += strData;
	//}


	//for (int t = 0; t < ITM_SFR_Max; t++)
	//{
	//	strData.Format(_T("%.2f:%d,"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFRData.dbMinValue[t], m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFRData.nEachResult_G[t]);
	//	strFullData += strData;
	//}


	for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
	{
		if (m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFRData.bUse)
		{
			strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFRData.dbValue[nROI], m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFRData.nEachResult[nROI]);
		}
		else
		{
			strData.Format(_T(":1"));
		}

		if (nROI < (ROI_SFR_Max - 1))
		{
			strData += _T(",");
		}

		strFullData += strData;
	}

	pMesData->szMesTestData[nParaIdx][MesDataIdx_Foc_SFR] = strFullData;
}

//=============================================================================
// Method		: Rotate_DataSave_Foc
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in ST_MES_Data_Foc * pMesData
// Qualifier	:
// Last Update	: 2018/3/9 - 20:14
// Desc.		:
//=============================================================================
void CTestManager_TestMES::Rotate_DataSave_Foc(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData)
{
	if (NULL == pMesData)
		return;

	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stRotateData.dbValue, m_pInspInfo->CamInfo[nParaIdx].stFocus.stRotateData.nResult);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesDataIdx_Foc_Rotation] = strFullData;
}

//=============================================================================
// Method		: Stain_DataSave_Foc
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in ST_MES_Data_Foc * pMesData
// Qualifier	:
// Last Update	: 2018/3/9 - 20:14
// Desc.		:
//=============================================================================
void CTestManager_TestMES::Stain_DataSave_Foc(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData)
{
	if (NULL == pMesData)
		return;

	CString strData;
	CString strFullData;

	strFullData.Empty();

	for (UINT nROI = 0; nROI < ROI_Particle_Max; nROI++)
	{
		strData.Format(_T("%d:%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stParticleData.nFailTypeCount[nROI], m_pInspInfo->CamInfo[nParaIdx].stFocus.stParticleData.nResult);
		strData += _T(",");
		strFullData += strData;

		strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stParticleData.dFailTypeConcentration[nROI], m_pInspInfo->CamInfo[nParaIdx].stFocus.stParticleData.nResult);
		
		if (nROI < (ROI_Particle_Max - 1))
		{
			strData += _T(",");
		}

		strFullData += strData;
	}

	pMesData->szMesTestData[nParaIdx][MesDataIdx_Foc_Stain] = strFullData;
}

//=============================================================================
// Method		: Defectpixel_DataSave_Foc
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in ST_MES_Data_Foc * pMesData
// Qualifier	:
// Last Update	: 2018/3/9 - 20:14
// Desc.		:
//=============================================================================
void CTestManager_TestMES::Defectpixel_DataSave_Foc(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData)
{
	if (NULL == pMesData)
		return;

	CString strData;
	CString strFullData;

	strFullData.Empty();

	for (UINT nItem = 0; nItem < Spec_Defect_Max; nItem++)
	{
		strData.Format(_T("%d:%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stDefectPixelData.nFailCount[nItem], m_pInspInfo->CamInfo[nParaIdx].stFocus.stDefectPixelData.nEachResult[nItem]);

		if (nItem < (Spec_Defect_Max-1))
		{
			strData += _T(",");
		}

		strFullData += strData;
	}

	pMesData->szMesTestData[nParaIdx][MesDataIdx_Foc_DefectPixel] = strFullData;
}




//=============================================================================
// Method		: Torque_DataSave_Foc
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in ST_MES_Data_Foc * pMesData
// Qualifier	:
// Last Update	: 2018/5/9 - 17:52
// Desc.		:
//=============================================================================
void CTestManager_TestMES::Torque_DataSave_Foc(__in UINT nParaIdx, __in ST_MES_Data_Foc *pMesData)
{
	if (NULL == pMesData)
		return;

	CString strData;
	CString strFullData;

	strFullData.Empty();

	for (UINT nItem = 0; nItem < Spec_Tor_Max; nItem++)
	{
		if (nItem <= Spec_Tor_D)
		{
			strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stTorqueData.dbValue[nItem], m_pInspInfo->CamInfo[nParaIdx].stFocus.stTorqueData.nEachResult[nItem]);
		}
		else
		{
			strData.Format(_T("%.f:%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stTorqueData.dbValue[nItem], m_pInspInfo->CamInfo[nParaIdx].stFocus.stTorqueData.nEachResult[nItem]);
		}

		if (nItem < (Spec_Tor_Max - 1))
		{
			strData += _T(",");
		}

		strFullData += strData;
	}

	pMesData->szMesTestData[nParaIdx][MesDataIdx_Foc_Torque] = strFullData;
}

//--------------------------------------------------------------------------------------------개별 검사 Report

void CTestManager_TestMES::Current_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult)
{
	//-전류 1.8V, 2.8V
	CString strData;
	switch (nSysType)
	{
	case Sys_2D_Cal:
		break;
	case Sys_3D_Cal:
		break;
	case Sys_Stereo_Cal:
		break;
	case Sys_Focusing:
	{
						 nResult = m_pInspInfo->CamInfo[nParaIdx].stFocus.stECurrentData.nResult;
						

						 if (m_pInspInfo->RecipeInfo.ModelType == Model_IKC || m_pInspInfo->RecipeInfo.ModelType == Model_MRA2){

							 for (int t = 0; t < MESDataNum_Foc_ECurrent; t++)
							 {
								 pAddHeaderz->Add(g_szMESHeader_Current[t]);
							 }


							 for (int t = 0; t < 2; t++)
							 {
								 strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stECurrentData.dbValue[t]);
								 pAddData->Add(strData);
							 }
							 for (int t = 0; t < 3; t++)
							 {
								 pAddData->Add(_T("X"));
							 }
						 }
						 else{
							 for (int t = 0; t < MESDataNum_Foc_ECurrent; t++)
							 {
								 pAddHeaderz->Add(g_szMESHeader_CurrentEntry[t]);
							 }


							 for (int t = 0; t <5; t++)
							 {
								 strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stECurrentData.dbValue[t]);
								 pAddData->Add(strData);
							 }
						 }
						
						 
	}
		break;
	case Sys_Image_Test:
	{
						   nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stECurrentData.nResult;
						   if (m_pInspInfo->RecipeInfo.ModelType == Model_IKC || m_pInspInfo->RecipeInfo.ModelType == Model_MRA2){
							   for (int t = 0; t < MESDataNum_ECurrent; t++)
							   {
								   pAddHeaderz->Add(g_szMESHeader_Current[t]);
							   }


							   for (int t = 0; t < 2; t++)
							   {
								   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stECurrentData.dbValue[t]);
								   pAddData->Add(strData);
							   }
							   for (int t = 0; t < 3; t++)
							   {
								   pAddData->Add(_T("X"));
							   }
						   }else{
							   for (int t = 0; t < MESDataNum_ECurrent; t++)
							   {
								   pAddHeaderz->Add(g_szMESHeader_CurrentEntry[t]);
							   }


							   for (int t = 0; t < 5; t++)
							   {
								   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stECurrentData.dbValue[t]);
								   pAddData->Add(strData);
							   }
						   }
	}
		break;
	default:
		break;
	}
}

void CTestManager_TestMES::OpticalCenter_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult)
{
	//-광축 X, Y
	CString strData;

	switch (nSysType)
	{
	case Sys_2D_Cal:
		break;
	case Sys_3D_Cal:
		break;
	case Sys_Stereo_Cal:
		break;
	case Sys_Focusing:
	{
						 nResult = m_pInspInfo->CamInfo[nParaIdx].stFocus.stOpticalCenterData.nResult;
						 for (int t = 0; t < MESDataNum_Foc_OpticalCenter; t++)
						 {
							 pAddHeaderz->Add(g_szMESHeader_OC[t]);
						 }
						 //@SH _181108: LGIT에서 nResultPosX -> Offset으로 저장해달라고 변경 요청
						 //strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stOpticalCenterData.nResultPosX);
						 //pAddData->Add(strData);

						 //strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stOpticalCenterData.nResultPosY);
						 //pAddData->Add(strData);
						 strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stOpticalCenterData.iStandPosDevX);
						 pAddData->Add(strData);

						 strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stOpticalCenterData.iStandPosDevY);
						 pAddData->Add(strData);

						 for (int t = 0; t < 3; t++)
						 {
							 pAddData->Add(_T("X"));
						 }

	}
		break;
	case Sys_Image_Test:
	{
						   nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stOpticalCenterData.nResult;
						   for (int t = 0; t < MESDataNum_OpticalCenter; t++)
						   {
							   pAddHeaderz->Add(g_szMESHeader_OC[t]);
						   }
						   //@SH _181108: LGIT에서 nResultPosX -> Offset으로 저장해달라고 변경 요청
						   //strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stOpticalCenterData.nResultPosX);
						   //pAddData->Add(strData);

						   //strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stOpticalCenterData.nResultPosY);
						   //pAddData->Add(strData);

						   strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stOpticalCenterData.iStandPosDevX);
						   pAddData->Add(strData);

						   strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stOpticalCenterData.iStandPosDevY);
						   pAddData->Add(strData);
						 
						   for (int t = 0; t < 3; t++)
						   {
							   pAddData->Add(_T("X"));
						   }
	}
		break;
	default:
		break;
	}



}

void CTestManager_TestMES::SFR_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult, int nTypeCnt)
{
	CString strData;

	switch (nSysType)
	{
	case Sys_2D_Cal:
		break;
	case Sys_3D_Cal:
		break;
	case Sys_Stereo_Cal:
		break;
	case Sys_Focusing:
	{
						 nResult = m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFRData.nResult;
						 for (int t = 0; t < MESDataNum_Foc_SFR; t++)
						 {
							 pAddHeaderz->Add(g_szMESHeader_SFR[t]);
						 }
						 //  for (int t = 0; t < ITM_SFR_TiltMax; t++)
						 for (int t = 0; t < ROI_SFR_TILT_Max; t++)
						 {
							 if (m_pInspInfo->RecipeInfo.stFocus.stSFROpt.stInput_Tilt[t].bEnable)
							 {
								 strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFRData.dbTiltdata[t]);
							 }
							 else
							 {
								 strData.Format(_T("X"));
							 }
							 pAddData->Add(strData);
						 }
						 //for (int t = 0; t < ITM_SFR_TiltMax; t++)
						 //{
							// strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFRData.dbTiltdata[t]);
							// pAddData->Add(strData);
						 //}


						 //for (int t = 0; t < ITM_SFR_Max; t++)
						 //{
							// strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFRData.dbMinValue[t]);
							// pAddData->Add(strData);
						 //}
						 // for (int t = 0; t < ITM_SFR_Max; t++)
						 for (int t = 0; t < ROI_SFR_GROUP_Max; t++)
						 {
							 //   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbMinValue[t]);
							 if (m_pInspInfo->RecipeInfo.stFocus.stSFROpt.stInput_Group[t].bEnable)
							 {
								 strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFRData.dbEachResultGroup[t]);
							 }
							 else
							 {
								 strData.Format(_T("X"));
							 }
							 pAddData->Add(strData);
						 }


						 for (int t = 0; t < ROI_SFR_Max; t++)
						 {
							 if (m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFRData.bUse)
							 {
								 strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFRData.dbValue[t]);
							 }
							 else
							 {
								 strData.Format(_T("X"));
							 }

							 pAddData->Add(strData);
						 }

	}
		break;
	case Sys_Image_Test:
	{
						   nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].nResult;
						   for (int t = 0; t < MESDataNum_SFR; t++)
						   {
							   pAddHeaderz->Add(g_szMESHeader_SFR[t]);
						   }

						 //  for (int t = 0; t < ITM_SFR_TiltMax; t++)
						   for (int t = 0; t < ROI_SFR_TILT_Max; t++)
						   {
							   if (m_pInspInfo->RecipeInfo.stImageQ.stSFROpt[nTypeCnt].stInput_Tilt[t].bEnable)
							   {
								   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbTiltdata[t]);
							   }
							   else
							   {
								   strData.Format(_T("X"));
							   }
							   pAddData->Add(strData);
						   }

						  // for (int t = 0; t < ITM_SFR_Max; t++)
						   for (int t = 0; t < ROI_SFR_GROUP_Max; t++)
						   {
							//   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbMinValue[t]);
							   if (m_pInspInfo->RecipeInfo.stImageQ.stSFROpt[nTypeCnt].stInput_Group[t].bEnable)
							   {
								   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbEachResultGroup[t]);
							   }
							   else
							   {
								   strData.Format(_T("X"));
							   }
							   pAddData->Add(strData);
						   }

						   for (int t = 0; t < ROI_SFR_Max; t++)
						   {

							   if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].bUse)
							   {
								   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbValue[t]);
							   }
							   else
							   {
								   strData.Format(_T("X"));
							   }

							   pAddData->Add(strData);
						   }
	}
		break;
	default:
		break;
	}



//	pMesData->szMesTestData[nParaIdx][MesDataIdx_SFR] = strFullData;
}

void CTestManager_TestMES::Rotate_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult)
{
	CString strData;
	switch (nSysType)
	{
	case Sys_2D_Cal:
		break;
	case Sys_3D_Cal:
		break;
	case Sys_Stereo_Cal:
		break;
	case Sys_Focusing:
	{
						 nResult = m_pInspInfo->CamInfo[nParaIdx].stFocus.stRotateData.nResult;
						 for (int t = 0; t < MESDataNum_Foc_Rotation; t++)
						 {
							 pAddHeaderz->Add(g_szMESHeader_Rotate[t]);
						 }

						 strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stRotateData.dbValue);
						 pAddData->Add(strData);

						 for (int t = 0; t < 2; t++)
						 {
							 pAddHeaderz->Add(_T("X"));
						 }
	}
		break;
	case Sys_Image_Test:
	{
						   nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stRotateData.nResult;
						   for (int t = 0; t < MESDataNum_Rotation; t++)
						   {
							   pAddHeaderz->Add(g_szMESHeader_Rotate[t]);
						   }

						   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stRotateData.dbValue);
						   pAddData->Add(strData);

						   for (int t = 0; t < 2; t++)
						   {
							   pAddHeaderz->Add(_T("X"));
						   }
	}
		break;
	default:
		break;
	}

}

void CTestManager_TestMES::Distortion_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;
	switch (nSysType)
	{
	case Sys_2D_Cal:
		break;
	case Sys_3D_Cal:
		break;
	case Sys_Stereo_Cal:
		break;
	case Sys_Focusing:
	{

	}
		break;
	case Sys_Image_Test:
	{
						   nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDistortionData.nResult;
						   for (int t = 0; t < MESDataNum_Distortion; t++)
						   {
							   pAddHeaderz->Add(g_szMESHeader_Distortion[t]);
						   }

						   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDistortionData.dbValue);
						   pAddData->Add(strData);

						   for (int t = 0; t < 2; t++)
						   {
							   pAddHeaderz->Add(_T("X"));
						   }
	}
		break;
	default:
		break;
	}
}

void CTestManager_TestMES::Fov_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult)
{
	CString strData;
	switch (nSysType)
	{
	case Sys_2D_Cal:
		break;
	case Sys_3D_Cal:
		break;
	case Sys_Stereo_Cal:
		break;
	case Sys_Focusing:
	{

	}
		break;
	case Sys_Image_Test:
	{
						   nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stFovData.nResult;
						   for (int t = 0; t < MESDataNum_FOV; t++)
						   {
							   pAddHeaderz->Add(g_szMESHeader_Fov[t]);
						   }

						   for (int t = 0; t < 2; t++)
						   {
							   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stFovData.dbValue[t]);
							   pAddData->Add(strData);
						   }

						   for (int t = 0; t <1; t++)
						   {
							   pAddData->Add(_T("X"));
						   }
	}
		break;
	default:
		break;
	}

}


void CTestManager_TestMES::DynamicRange_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult)
{
	CString strData;
	switch (nSysType)
	{
	case Sys_2D_Cal:
		break;
	case Sys_3D_Cal:
		break;
	case Sys_Stereo_Cal:
		break;
	case Sys_Focusing:
	{

	}
		break;
	case Sys_Image_Test:
	{
						   nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicData.nResultDR&m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicData.nResultBW;
						   for (int t = 0; t < MESDataNum_Particle_SNR; t++)
						   {
							   pAddHeaderz->Add(g_szMESHeader_Dynamic[t]);
						   }
						   int nCnt = 0;

						   for (int k = 0; k < ROI_Par_SNR_Max; k++)
						   {
							   if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicData.bUse[k])
							   {
								   for (int t = 0; t < Light_Par_SNR_MAX; t++)
								   {
									   strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicData.sSignal[t][k]);
									   pAddData->Add(strData);
									   nCnt++;
								   }
								   for (int t = 0; t < Light_Par_SNR_MAX; t++)
								   {
									   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicData.fNoise[t][k]);
									   pAddData->Add(strData);
									   nCnt++;
								   }

								   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicData.dbValueBW[k]);
								   pAddData->Add(strData);
								   nCnt++;
								   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicData.dbValueDR[k]);
								   pAddData->Add(strData);
								   nCnt++;



							   }
							   else{
								   for (int t = 0; t < Light_Par_SNR_MAX; t++)
								   {
									   pAddData->Add(_T("X"));
									   nCnt++;
								   }
								   for (int t = 0; t < Light_Par_SNR_MAX; t++)
								   {
									   pAddData->Add(_T("X"));
									   nCnt++;
								   }

								   pAddData->Add(_T("X"));
								   nCnt++;
								   pAddData->Add(_T("X"));
								   nCnt++;
								   pAddData->Add(_T("X"));
								   nCnt++;

							   }
						   }

						   for (int t = nCnt; t < MESDataNum_Particle_SNR; t++)
						   {
							   pAddData->Add(_T("X"));
						   }
						   //pMesData->szMesTestData[nParaIdx][MesDataIdx_Particle_SNR] = strFullData;
	}
		break;
	default:
		break;
	}


}

void CTestManager_TestMES::Intensity_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult)
{

	int nDataNum[5] = { ROI_Intencity_CC, ROI_Intencity_RT, ROI_Intencity_RB, ROI_Intencity_LT, ROI_Intencity_LB };
	CString strData;
	switch (nSysType)
	{
	case Sys_2D_Cal:
		break;
	case Sys_3D_Cal:
		break;
	case Sys_Stereo_Cal:
		break;
	case Sys_Focusing:
	{

	}
		break;
	case Sys_Image_Test:
	{
						   nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stIntensityData.nResult;
						   for (int t = 0; t < MESDataNum_Intensity; t++)
						   {
							   CString str;
							   str.Format(_T("%s"), g_szMESHeader_Shading_RI[t]);
							   pAddHeaderz->Add(str);
						   }
						   int nCnt = 0;
						   for (int t = 0; t < ROI_Intencity_Max; t++)
						   {
							   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stIntensityData.fSignal[nDataNum[t]]);
							   pAddData->Add(strData);
							   nCnt++;

							   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stIntensityData.fPercent[nDataNum[t]]);
							   pAddData->Add(strData);
							   nCnt++;
						   }
						   for (int t = nCnt; t < MESDataNum_Intensity; t++)
						   {
							   pAddData->Add(_T("X"));
						   }
						   			   
						   //pMesData->szMesTestData[nParaIdx][MesDataIdx_Particle_SNR] = strFullData;
	}
		break;
	default:
		break;
	}

	//pMesData->szMesTestData[nParaIdx][MesDataIdx_Intensity] = strFullData;
}

void CTestManager_TestMES::Stain_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult)
{
	CString strData;
	switch (nSysType)
	{
	case Sys_2D_Cal:
		break;
	case Sys_3D_Cal:
		break;
	case Sys_Stereo_Cal:
		break;
	case Sys_Focusing:
	{
						 nResult = m_pInspInfo->CamInfo[nParaIdx].stFocus.stParticleData.nResult;
						 for (int t = 0; t < MESDataNum_Foc_Stain; t++)
						 {
							 pAddHeaderz->Add(g_szMESHeader_Stain[t]);
						 }
						 int nCnt = 0;
						 for (int t = 0; t < ROI_Particle_Max; t++)
						 {
							 strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stParticleData.dFailTypeConcentration[t]);
							 pAddData->Add(strData);
							 nCnt++;

							 strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stParticleData.nFailTypeCount[t]);
							 pAddData->Add(strData);
							 nCnt++;

						 }
						 for (int t = nCnt; t < MESDataNum_Foc_Stain; t++)
						 {
							 pAddData->Add(_T("X"));
						 }
	}
		break;
	case Sys_Image_Test:
	{
						   nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stParticleData.nResult;

						   for (int t = 0; t < MESDataNum_Stain; t++)
						   {
							   pAddHeaderz->Add(g_szMESHeader_Stain[t]);
						   }
						   int nCnt = 0;
						   for (int t = 0; t < ROI_Particle_Max; t++)
						   {
							   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stParticleData.dFailTypeConcentration[t]);
							   pAddData->Add(strData);
							   nCnt++;

							   strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stParticleData.nFailTypeCount[t]);
							   pAddData->Add(strData);
							   nCnt++;

						   }
						   for (int t = nCnt; t < MESDataNum_Stain; t++)
						   {
							   pAddData->Add(_T("X"));
						   }
						   //pMesData->szMesTestData[nParaIdx][MesDataIdx_Particle_SNR] = strFullData;
	}
		break;
	default:
		break;
	}
	
//	pMesData->szMesTestData[nParaIdx][MesDataIdx_Stain] = strFullData;
}

void CTestManager_TestMES::Defectpixel_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult)
{
	CString strData;
	switch (nSysType)
	{
	case Sys_2D_Cal:
		break;
	case Sys_3D_Cal:
		break;
	case Sys_Stereo_Cal:
		break;
	case Sys_Focusing:
	{
		
						 nResult = m_pInspInfo->CamInfo[nParaIdx].stFocus.stDefectPixelData.nResult;
						 for (int t = 0; t < MESDataNum_Foc_DefectPixel; t++)
						 {
							 pAddHeaderz->Add(g_szMESHeader_Defectpixel[t]);
						 }
						 int nCnt = 0;
						 for (int t = 0; t < Spec_Defect_Max; t++)
						 {
							 strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stDefectPixelData.nFailCount[t]);
							 pAddData->Add(strData);
							 nCnt++;
						 }
						 for (int t = nCnt; t < MESDataNum_Foc_DefectPixel; t++)
						 {
							 pAddData->Add(_T("X"));
						 }
	}
		break;
	case Sys_Image_Test:
	{
						   nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDefectPixelData.nResult;
						   for (int t = 0; t < MESDataNum_DefectPixel; t++)
						   {
							   pAddHeaderz->Add(g_szMESHeader_Defectpixel[t]);
						   }
						   int nCnt = 0;
						   for (int t = 0; t < Spec_Defect_Max; t++)
						   {
							   strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDefectPixelData.nFailCount[t]);
							   pAddData->Add(strData);
							   nCnt++;
						   }
						   for (int t = nCnt; t < MESDataNum_DefectPixel; t++)
						   {
							   pAddData->Add(_T("X"));
						   }
						   //pMesData->szMesTestData[nParaIdx][MesDataIdx_Particle_SNR] = strFullData;
	}
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: SNR_Light_Each_DataSave
// Access		: public  
// Returns		: void
// Parameter	: enInsptrSysType nSysType
// Parameter	: int nParaIdx
// Parameter	: CStringArray * pAddHeaderz
// Parameter	: CStringArray * pAddData
// Parameter	: UINT & nResult
// Qualifier	:
// Last Update	: 2018/5/12 - 21:32
// Desc.		:
//=============================================================================
void CTestManager_TestMES::SNR_Light_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult)
{
	CString strData;
	switch (nSysType)
	{
	case Sys_2D_Cal:
		break;
	case Sys_3D_Cal:
		break;
	case Sys_Stereo_Cal:
		break;
	case Sys_Focusing:
	{
	}
		break;
	case Sys_Image_Test:
	{
						   nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSNR_LightData.nResult;
						   for (int t = 0; t < MESDataNum_SNR_Light; t++)
						   {
							   pAddHeaderz->Add(g_szMESHeader_SNR_Light[t]);
						   }
						   int nCnt = 0;
						   for (int t = 0; t < MESDataNum_SNR_Light; t++)
						   {
							   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSNR_LightData.dbEtcValue[t]);
							   pAddData->Add(strData);
							   nCnt++;
						   }
// 						   for (int t = nCnt; t < MESDataNum_DefectPixel; t++)
// 						   {
// 							   pAddData->Add(_T("X"));
// 						   }
						   //pMesData->szMesTestData[nParaIdx][MesDataIdx_Particle_SNR] = strFullData;
	}
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: Shading_Each_DataSave
// Access		: public  
// Returns		: void
// Parameter	: enInsptrSysType nSysType
// Parameter	: int nParaIdx
// Parameter	: CStringArray * pAddHeaderz
// Parameter	: CStringArray * pAddData
// Parameter	: UINT & nResult
// Qualifier	:
// Last Update	: 2018/5/12 - 21:32
// Desc.		:
//=============================================================================
void CTestManager_TestMES::Shading_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult)
{

	CString strData;
	switch (nSysType)
	{
	case Sys_2D_Cal:
		break;
	case Sys_3D_Cal:
		break;
	case Sys_Stereo_Cal:
		break;
	case Sys_Focusing:
	{
	}
		break;
	case Sys_Image_Test:
	{
						   nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.nResult;
						   for (int t = 0; t < MESDataNum_Shading; t++)
						   {
							   pAddHeaderz->Add(g_szMESHeader_Shading_SNR[t]);
						   }
						   CString strFullData;
						   for (UINT nIdx = 0; nIdx < Index_Shading_Max; nIdx++)
						   {

							   strFullData.Empty();
							   if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.bUse[nIndexVer_CT[nIdx]])
							   {
								   strData.Format(_T("CT_%.2f_"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.fPercent[nIndexVer_CT[nIdx]]);
								   strFullData += strData;
								   strData.Format(_T("%.2f:"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.sSignal[nIndexVer_CT[nIdx]]);
								   strFullData += strData;
								   strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.nEachResult[nIndexVer_CT[nIdx]]);

								   strFullData += strData;

								   pAddData->Add(strFullData);

							   }
							   else{
								   pAddData->Add(_T("X"));

							   }
						   }
						   for (UINT nIdx = 0; nIdx < Index_Shading_Max; nIdx++)
						   {
							   strFullData.Empty();
							   if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.bUse[nIndexVer_CB[nIdx]])
							   {
								   strFullData.Empty();
								   strData.Format(_T("CB_%.2f_"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.fPercent[nIndexVer_CB[nIdx]]);
								   strFullData += strData;
								   strData.Format(_T("%.2f:"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.sSignal[nIndexVer_CB[nIdx]]);
								   strFullData += strData;
								   strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.nEachResult[nIndexVer_CB[nIdx]]);

								   strFullData += strData;

								   pAddData->Add(strFullData);
							   }
							   else{
								   pAddData->Add(_T("X"));

							   }
						   }
						   for (UINT nIdx = 0; nIdx < Index_Shading_Max; nIdx++)
						   {
							   strFullData.Empty();
							   if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.bUse[nIndexHov_LC[nIdx]])
							   {
								   strFullData.Empty();
								   strData.Format(_T("LC_%.2f_"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.fPercent[nIndexHov_LC[nIdx]]);
								   strFullData += strData;
								   strData.Format(_T("%.2f:"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.sSignal[nIndexHov_LC[nIdx]]);
								   strFullData += strData;
								   strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.nEachResult[nIndexHov_LC[nIdx]]);

								   strFullData += strData;
								   pAddData->Add(strFullData);

							   }
							   else{
								   pAddData->Add(_T("X"));

							   }
						   }
						   for (UINT nIdx = 0; nIdx < Index_Shading_Max; nIdx++)
						   {
							   strFullData.Empty();
							   if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.bUse[nIndexHov_RC[nIdx]])
							   {
								   strFullData.Empty();
								   strData.Format(_T("RC_%.2f_"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.fPercent[nIndexHov_RC[nIdx]]);
								   strFullData += strData;
								   strData.Format(_T("%.2f:"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.sSignal[nIndexHov_RC[nIdx]]);
								   strFullData += strData;
								   strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.nEachResult[nIndexHov_RC[nIdx]]);

								   strFullData += strData;
								   pAddData->Add(strFullData);
							   }
							   else{
								   pAddData->Add(_T("X"));

							   }
						   }
						   for (UINT nIdx = 0; nIdx < Index_Shading_Max; nIdx++)
						   {
							   strFullData.Empty();
							   if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.bUse[nIndexDig_LT[nIdx]])
							   {
								   strFullData.Empty();
								   strData.Format(_T("LT_%.2f_"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.fPercent[nIndexDig_LT[nIdx]]);
								   strFullData += strData;
								   strData.Format(_T("%.2f:"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.sSignal[nIndexDig_LT[nIdx]]);
								   strFullData += strData;
								   strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.nEachResult[nIndexDig_LT[nIdx]]);

								   strFullData += strData;
								   pAddData->Add(strFullData);
							   }
							   else{
								   pAddData->Add(_T("X"));

							   }
						   }
						   for (UINT nIdx = 0; nIdx < Index_Shading_Max; nIdx++)
						   {
							   strFullData.Empty();
							   if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.bUse[nIndexDig_LB[nIdx]])
							   {
								   strFullData.Empty();
								   strData.Format(_T("LB_%.2f_"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.fPercent[nIndexDig_LB[nIdx]]);
								   strFullData += strData;
								   strData.Format(_T("%.2f:"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.sSignal[nIndexDig_LB[nIdx]]);
								   strFullData += strData;
								   strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.nEachResult[nIndexDig_LB[nIdx]]);

								   strFullData += strData;
								   pAddData->Add(strFullData);
							   }
							   else{
								   pAddData->Add(_T("X"));

							   }
						   }
						   for (UINT nIdx = 0; nIdx < Index_Shading_Max; nIdx++)
						   {
							   strFullData.Empty();
							   if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.bUse[nIndexDig_RT[nIdx]])
							   {
								   strFullData.Empty();
								   strData.Format(_T("RT_%.2f_"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.fPercent[nIndexDig_RT[nIdx]]);
								   strFullData += strData;
								   strData.Format(_T("%.2f:"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.sSignal[nIndexDig_RT[nIdx]]);
								   strFullData += strData;
								   strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.nEachResult[nIndexDig_RT[nIdx]]);

								   strFullData += strData;
								   pAddData->Add(strFullData);
							   }
							   else{
								   pAddData->Add(_T("X"));

							   }
						   }
						   for (UINT nIdx = 0; nIdx < Index_Shading_Max; nIdx++)
						   {
							   strFullData.Empty();
							   if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.bUse[nIndexDig_RB[nIdx]])
							   {
								   strFullData.Empty();
								   strData.Format(_T("RB_%.2f_"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.fPercent[nIndexDig_RB[nIdx]]);
								   strFullData += strData;
								   strData.Format(_T("%.2f:"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.sSignal[nIndexDig_RB[nIdx]]);
								   strFullData += strData;
								   strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.nEachResult[nIndexDig_RB[nIdx]]);

								   strFullData += strData;
								   pAddData->Add(strFullData);
							   }
							   else{
								   pAddData->Add(_T("X"));

							   }
						   }						 


						  
// 						   int nCnt = 0;
// 						   for (int t = 0; t < MESDataNum_Shading; t++)
// 						   {
// 							   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.fPercent[t]);
// 							   pAddData->Add(strData);
// 							   nCnt++;
// 						   }

						   if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.bUse[ROI_Shading_CNT])
						   {
							   strFullData.Empty();
							   strData.Format(_T("CC_%.2f_"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.fPercent[ROI_Shading_CNT]);
							   strFullData += strData;
							   strData.Format(_T("%.2f:"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.sSignal[ROI_Shading_CNT]);
							   strFullData += strData;
							   strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.nEachResult[ROI_Shading_CNT]);

							   strFullData += strData;
							   pAddData->Add(strFullData);
						   }
						   else{
							   pAddData->Add(_T("X"));

						   }
				
	}
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: HotPixel_Each_DataSave
// Access		: public  
// Returns		: void
// Parameter	: enInsptrSysType nSysType
// Parameter	: int nParaIdx
// Parameter	: CStringArray * pAddHeaderz
// Parameter	: CStringArray * pAddData
// Parameter	: UINT & nResult
// Qualifier	:
// Last Update	: 2018/5/12 - 21:28
// Desc.		:
//=============================================================================
void CTestManager_TestMES::HotPixel_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;
	switch (nSysType)
	{
	case Sys_2D_Cal:
		break;
	case Sys_3D_Cal:
		break;
	case Sys_Stereo_Cal:
		break;
	case Sys_Focusing:
		break;
	case Sys_Image_Test:
		nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stHotPixelData.nResult;
		for (int t = 0; t < MESDataNum_HotPixel; t++)
		{
			pAddHeaderz->Add(g_szMESHeader_HotPixel[t]);
		}

		strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stHotPixelData.iFailCount);
		pAddData->Add(strData);

		for (int t = 0; t < 2; t++)
		{
			pAddData->Add(_T("X"));
		}
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: FPN_Each_DataSave
// Access		: public  
// Returns		: void
// Parameter	: enInsptrSysType nSysType
// Parameter	: int nParaIdx
// Parameter	: CStringArray * pAddHeaderz
// Parameter	: CStringArray * pAddData
// Parameter	: UINT & nResult
// Qualifier	:
// Last Update	: 2018/5/12 - 21:31
// Desc.		:
//=============================================================================
void CTestManager_TestMES::FPN_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;
	switch (nSysType)
	{
	case Sys_2D_Cal:
		break;
	case Sys_3D_Cal:
		break;
	case Sys_Stereo_Cal:
		break;
	case Sys_Focusing:
		break;
	case Sys_Image_Test:
		nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stFPNData.nResult;
		for (int t = 0; t < MESDataNum_FPN; t++)
		{
			pAddHeaderz->Add(g_szMESHeader_FPN[t]);
		}

		for (int t = 0; t < 2; t++)
		{
			strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stFPNData.iFailCount[t]);
			pAddData->Add(strData);
		}

		for (int t = 0; t < 2; t++)
		{
			pAddData->Add(_T("X"));
		}
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: Depth_Each_DataSave
// Access		: public  
// Returns		: void
// Parameter	: enInsptrSysType nSysType
// Parameter	: int nParaIdx
// Parameter	: CStringArray * pAddHeaderz
// Parameter	: CStringArray * pAddData
// Parameter	: UINT & nResult
// Qualifier	:
// Last Update	: 2018/5/12 - 21:31
// Desc.		:
//=============================================================================
void CTestManager_TestMES::Depth_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;
	switch (nSysType)
	{
	case Sys_2D_Cal:
		break;
	case Sys_3D_Cal:
		break;
	case Sys_Stereo_Cal:
		break;
	case Sys_Focusing:
		break;
	case Sys_Image_Test:
		nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.st3D_DepthData.nResult;
		for (int t = 0; t < MESDataNum_3DDepth; t++)
		{
			pAddHeaderz->Add(g_szMESHeader_3DDepth[t]);
		}

		for (int t = 0; t < 2; t++)
		{
			strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.st3D_DepthData.dbValue[t]);
			pAddData->Add(strData);
		}

		for (int t = 0; t < 2; t++)
		{
			pAddData->Add(_T("X"));
		}
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: EEPROM_Each_DataSave
// Access		: public  
// Returns		: void
// Parameter	: enInsptrSysType nSysType
// Parameter	: int nParaIdx
// Parameter	: CStringArray * pAddHeaderz
// Parameter	: CStringArray * pAddData
// Parameter	: UINT & nResult
// Qualifier	:
// Last Update	: 2018/5/12 - 21:32
// Desc.		:
//=============================================================================
void CTestManager_TestMES::EEPROM_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;
	switch (nSysType)
	{
	case Sys_2D_Cal:
		break;
	case Sys_3D_Cal:
		break;
	case Sys_Stereo_Cal:
		break;
	case Sys_Focusing:
		break;
	case Sys_Image_Test:
		nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.nResult;
		for (int t = 0; t < MESDataNum_EEPROM; t++)
		{
			pAddHeaderz->Add(g_szMESHeader_EEPROM[t]);
		}

		strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.fOC_X);
		pAddData->Add(strData);

		strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.fOC_Y);
		pAddData->Add(strData);

		strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.fCheckSum1);
		pAddData->Add(strData);

		strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.fCheckSum2);
		pAddData->Add(strData);

		for (int t = 0; t < 2; t++)
		{
			pAddData->Add(_T("X"));
		}
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: TemperatureSensor_Each_DataSave
// Access		: public  
// Returns		: void
// Parameter	: enInsptrSysType nSysType
// Parameter	: int nParaIdx
// Parameter	: CStringArray * pAddHeaderz
// Parameter	: CStringArray * pAddData
// Parameter	: UINT & nResult
// Qualifier	:
// Last Update	: 2018/5/12 - 21:32
// Desc.		:
//=============================================================================
void CTestManager_TestMES::TemperatureSensor_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;
	switch (nSysType)
	{
	case Sys_2D_Cal:
		break;
	case Sys_3D_Cal:
		break;
	case Sys_Stereo_Cal:
		break;
	case Sys_Focusing:
		break;
	case Sys_Image_Test:
		nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stTemperatureSensorData.nResult;
		for (int t = 0; t < MESEntryDataNum_TemperatureSensor; t++)
		{
			pAddHeaderz->Add(g_szMESHeader_TemperatureSensor[t]);
		}

		strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stTemperatureSensorData.dbTemperature);
		pAddData->Add(strData);

		break;
	default:
		break;
	}
}


//=============================================================================
// Method		: Particle_Entry_Each_DataSave
// Access		: public  
// Returns		: void
// Parameter	: enInsptrSysType nSysType
// Parameter	: int nParaIdx
// Parameter	: CStringArray * pAddHeaderz
// Parameter	: CStringArray * pAddData
// Parameter	: UINT & nResult
// Qualifier	:
// Last Update	: 2018/5/12 - 21:32
// Desc.		:
//=============================================================================
void CTestManager_TestMES::Particle_Entry_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;
	switch (nSysType)
	{
	case Sys_2D_Cal:
		break;
	case Sys_3D_Cal:
		break;
	case Sys_Stereo_Cal:
		break;
	case Sys_Focusing:
		break;
	case Sys_Image_Test:
		nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stParticle_EntryData.nResult;
		for (int t = 0; t < MESEntryDataNum_Particle; t++)
		{
			pAddHeaderz->Add(g_szMESHeader_Particle_Entry[t]);
		}

		strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stParticle_EntryData.nFailCount);
		pAddData->Add(strData);

		break;
	default:
		break;
	}
}


void CTestManager_TestMES::Current_Entry_DataSave(int nParaIdx, ST_MES_Entry_Data *pMesData)
{
	//-전류 1.8V, 2.8V
	CString strData;
	CString strFullData;

	strFullData.Empty();

	int nCurrentCnt = Spec_ECurrent_Max;

	for (UINT t = 0; t < nCurrentCnt; t++)
	{
		strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stECurrentData.dbValue[t], m_pInspInfo->CamInfo[nParaIdx].stImageQ.stECurrentData.nEachResult[t]);

		if (t < (nCurrentCnt - 1))
		{
			strData += _T(",");
		}
		strFullData += strData;
	}
	pMesData->szMesTestData[nParaIdx][MesEntryDataIdx_ECurrent] = strFullData;
}

void CTestManager_TestMES::OpticalCenter_Entry_DataSave(int nParaIdx, ST_MES_Entry_Data *pMesData)
{
	//-광축 X, Y
	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%d:%d,"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stOpticalCenterData.nResultPosX, m_pInspInfo->CamInfo[nParaIdx].stImageQ.stOpticalCenterData.nResultX);
	strFullData += strData;

	strData.Format(_T("%d:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stOpticalCenterData.nResultPosY, m_pInspInfo->CamInfo[nParaIdx].stImageQ.stOpticalCenterData.nResultY);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesEntryDataIdx_OpticalCenter] = strFullData;
}

void CTestManager_TestMES::SFR_Entry_DataSave(int nParaIdx, ST_MES_Entry_Data *pMesData)
{
	//!SH _ 181122: Entry의 경우 SFR을 하나만 사용하므로, 0으로 Pix 추후 변경 필요시 수정 필요
	CString strData;
	CString strFullData;

	strFullData.Empty();

	//for (int t = 0; t < ITM_SFR_TiltMax; t++)
	//{
	//	strData.Format(_T("%.2f:%d,"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData[0].dbTiltdata[t], m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData[0].nEachTiltResult[t]);
	//	strFullData += strData;
	//}


	//for (int t = 0; t < ITM_SFR_Max; t++)
	//{
	//	strData.Format(_T("%.2f:%d,"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData[0].dbMinValue[t], m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData[0].nEachResult_G[t]);
	//	strFullData += strData;
	//}

	for (int t = 0; t < ROI_SFR_Max; t++)
	{

		if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData[0].bUse)
		{
			strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData[0].dbValue[t], m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData[0].nEachResult[t]);
		}
		else{
			strData.Format(_T(":1"));
		}
		if (t < (ROI_SFR_Max - 1))
		{
			strData += _T(",");
		}
		strFullData += strData;
	}

	pMesData->szMesTestData[nParaIdx][MesEntryDataIdx_SFR] = strFullData;
}

void CTestManager_TestMES::Rotate_Entry_DataSave(int nParaIdx, ST_MES_Entry_Data *pMesData)
{
	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stRotateData.dbValue, m_pInspInfo->CamInfo[nParaIdx].stImageQ.stRotateData.nResult);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesEntryDataIdx_Rotation] = strFullData;
}

void CTestManager_TestMES::Distortion_Entry_DataSave(int nParaIdx, ST_MES_Entry_Data *pMesData)
{
	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDistortionData.dbValue, m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDistortionData.nResult);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesEntryDataIdx_Distortion] = strFullData;
}

void CTestManager_TestMES::Fov_Entry_DataSave(int nParaIdx, ST_MES_Entry_Data *pMesData)
{
	CString strData;
	CString strFullData;

	strFullData.Empty();
	for (int t = 0; t < 2; t++)
	{
		strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stFovData.dbValue[t], m_pInspInfo->CamInfo[nParaIdx].stImageQ.stFovData.nEachResult[t]);

		if (t < 1)
		{
			strData += _T(",");
		}
		strFullData += strData;
	}

	pMesData->szMesTestData[nParaIdx][MesEntryDataIdx_FOV] = strFullData;
}


void CTestManager_TestMES::DynamicRange_Entry_DataSave(int nParaIdx, ST_MES_Entry_Data *pMesData)
{
	//-광축 X, Y
	CString strData;
	CString strFullData;

	strFullData.Empty();

	for (int k = 0; k < ROI_Par_SNR_Max; k++)
	{
		if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicData.bUse[k])
		{
			for (int t = 0; t < Light_Par_SNR_MAX; t++)
			{
				strData.Format(_T("%d:1,"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicData.sSignal[t][k]);
				strFullData += strData;
			}
			for (int t = 0; t < Light_Par_SNR_MAX; t++)
			{
				strData.Format(_T("%.2f:1,"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicData.fNoise[t][k]);
				strFullData += strData;
			}

			strData.Format(_T("%.2f:%d,"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicData.dbValueBW[k], m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicData.nEtcResultBW[k]);
			strFullData += strData;
			strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicData.dbValueDR[k], m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicData.nEtcResultDR[k]);



		}
		else{

			for (int t = 0; t < Light_Par_SNR_MAX; t++)
			{
				strData = _T(":1,");
				strFullData += strData;
			}
			for (int t = 0; t < Light_Par_SNR_MAX; t++)
			{
				strData = _T(":1,");
				strFullData += strData;
			}

			strData = _T(":1,");
			strFullData += strData;
			strData = _T(":1");
			strFullData += strData;

		}

		if (k < (ROI_Par_SNR_Max - 1))
		{
			strData += _T(",");
		}

		strFullData += strData;
	}





	pMesData->szMesTestData[nParaIdx][MesEntryDataIdx_Particle_SNR] = strFullData;

}


void CTestManager_TestMES::SNR_Light_Entry_DataSave(int nParaIdx, ST_MES_Entry_Data *pMesData)
{
	CString strData;
	CString strFullData;


	strFullData.Empty();
	for (int t = 0; t < ROI_SNR_Light_Max; t++)
	{
		if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSNR_LightData.bUse[t])
		{
			strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSNR_LightData.dbEtcValue[t], m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSNR_LightData.nResult);
		}
		else{
			strData.Format(_T(":1"));
		}

		if (t < (ROI_SNR_Light_Max - 1))
		{
			strData += _T(",");
		}
		strFullData += strData;
	}

	pMesData->szMesTestData[nParaIdx][MesEntryDataIdx_SNR_Light] = strFullData;
}

void CTestManager_TestMES::Shading_Entry_DataSave(int nParaIdx, ST_MES_Entry_Data *pMesData)
{
	CString strData;
	CString strFullData;

	// 	IDX_Shading_Ver_CT,
	// 		IDX_Shading_Ver_CB,
	// 		IDX_Shading_Hov_LC,
	// 		IDX_Shading_Hov_RC,
	// 		IDX_Shading_Dig_LT,
	// 		IDX_Shading_Dig_LB,
	// 		IDX_Shading_Dig_RT,
	// 		IDX_Shading_Dig_R

	strFullData.Empty();

	CString strShadingData[Index_Shading_Max] = { _T(""), };

	for (UINT nIdx = 0; nIdx < Index_Shading_Max; nIdx++)
	{
		if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.bUse[nIndexVer_CT[nIdx]])
		{
			strFullData.Empty();
			strData.Format(_T("CT_%.2f_"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.fPercent[nIndexVer_CT[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%.2f:"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.sSignal[nIndexVer_CT[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.nEachResult[nIndexVer_CT[nIdx]]);
			if (nIdx < (Index_Shading_Max - 1))
			{
				strData + _T(",");
			}
			strFullData += strData;

			strShadingData[nIdx] += strFullData;
		}
		else{
			strShadingData[nIdx] += _T(":1");
			if (nIdx < (Index_Shading_Max - 1))
			{
				strShadingData[nIdx] + _T(",");
			}
		}
	}
	for (UINT nIdx = 0; nIdx < Index_Shading_Max; nIdx++)
	{
		if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.bUse[nIndexVer_CB[nIdx]])
		{
			strFullData.Empty();
			strData.Format(_T("CB_%.2f_"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.fPercent[nIndexVer_CB[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%.2f:"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.sSignal[nIndexVer_CB[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.nEachResult[nIndexVer_CB[nIdx]]);
			if (nIdx < (Index_Shading_Max - 1))
			{
				strData + _T(",");
			}
			strFullData += strData;

			strShadingData[nIdx] += strFullData;
		}
		else{
			strShadingData[nIdx] += _T(":1");
			if (nIdx < (Index_Shading_Max - 1))
			{
				strShadingData[nIdx] + _T(",");
			}
		}
	}
	for (UINT nIdx = 0; nIdx < Index_Shading_Max; nIdx++)
	{
		if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.bUse[nIndexHov_LC[nIdx]])
		{
			strFullData.Empty();
			strData.Format(_T("LC_%.2f_"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.fPercent[nIndexHov_LC[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%.2f:"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.sSignal[nIndexHov_LC[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.nEachResult[nIndexHov_LC[nIdx]]);
			if (nIdx < (Index_Shading_Max - 1))
			{
				strData + _T(",");
			}
			strFullData += strData;

			strShadingData[nIdx] += strFullData;
		}
		else{
			strShadingData[nIdx] += _T(":1");
			if (nIdx < (Index_Shading_Max - 1))
			{
				strShadingData[nIdx] + _T(",");
			}
		}
	}
	for (UINT nIdx = 0; nIdx < Index_Shading_Max; nIdx++)
	{
		if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.bUse[nIndexHov_RC[nIdx]])
		{
			strFullData.Empty();
			strData.Format(_T("RC_%.2f_"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.fPercent[nIndexHov_RC[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%.2f:"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.sSignal[nIndexHov_RC[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.nEachResult[nIndexHov_RC[nIdx]]);
			if (nIdx < (Index_Shading_Max - 1))
			{
				strData + _T(",");
			}
			strFullData += strData;

			strShadingData[nIdx] += strFullData;
		}
		else{
			strShadingData[nIdx] += _T(":1");
			if (nIdx < (Index_Shading_Max - 1))
			{
				strShadingData[nIdx] + _T(",");
			}
		}
	}
	for (UINT nIdx = 0; nIdx < Index_Shading_Max; nIdx++)
	{
		if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.bUse[nIndexDig_LT[nIdx]])
		{
			strFullData.Empty();
			strData.Format(_T("LT_%.2f_"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.fPercent[nIndexDig_LT[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%.2f:"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.sSignal[nIndexDig_LT[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.nEachResult[nIndexDig_LT[nIdx]]);
			if (nIdx < (Index_Shading_Max - 1))
			{
				strData + _T(",");
			}
			strFullData += strData;

			strShadingData[nIdx] += strFullData;
		}
		else{
			strShadingData[nIdx] += _T(":1");
			if (nIdx < (Index_Shading_Max - 1))
			{
				strShadingData[nIdx] + _T(",");
			}
		}
	}
	for (UINT nIdx = 0; nIdx < Index_Shading_Max; nIdx++)
	{
		if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.bUse[nIndexDig_LB[nIdx]])
		{
			strFullData.Empty();
			strData.Format(_T("LB_%.2f_"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.fPercent[nIndexDig_LB[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%.2f:"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.sSignal[nIndexDig_LB[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.nEachResult[nIndexDig_LB[nIdx]]);
			if (nIdx < (Index_Shading_Max - 1))
			{
				strData + _T(",");
			}
			strFullData += strData;

			strShadingData[nIdx] += strFullData;
		}
		else{
			strShadingData[nIdx] += _T(":1");
			if (nIdx < (Index_Shading_Max - 1))
			{
				strShadingData[nIdx] + _T(",");
			}
		}
	}
	for (UINT nIdx = 0; nIdx < Index_Shading_Max; nIdx++)
	{
		if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.bUse[nIndexDig_RT[nIdx]])
		{
			strFullData.Empty();
			strData.Format(_T("RT_%.2f_"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.fPercent[nIndexDig_RT[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%.2f:"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.sSignal[nIndexDig_RT[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.nEachResult[nIndexDig_RT[nIdx]]);
			if (nIdx < (Index_Shading_Max - 1))
			{
				strData + _T(",");
			}
			strFullData += strData;

			strShadingData[nIdx] += strFullData;
		}
		else{
			strShadingData[nIdx] += _T(":1");
			if (nIdx < (Index_Shading_Max - 1))
			{
				strShadingData[nIdx] + _T(",");
			}
		}

	}
	for (UINT nIdx = 0; nIdx < Index_Shading_Max; nIdx++)
	{
		if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.bUse[nIndexDig_RB[nIdx]])
		{
			strFullData.Empty();
			strData.Format(_T("RB_%.2f_"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.fPercent[nIndexDig_RB[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%.2f:"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.sSignal[nIndexDig_RB[nIdx]]);
			strFullData += strData;
			strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.nEachResult[nIndexDig_RB[nIdx]]);
			if (nIdx < (Index_Shading_Max - 1))
			{
				strData + _T(",");
			}
			strFullData += strData;

			strShadingData[nIdx] += strFullData;
		}
		else{
			strShadingData[nIdx] += _T(":1");
			if (nIdx < (Index_Shading_Max - 1))
			{
				strShadingData[nIdx] + _T(",");
			}
		}
	}


	for (int t = 0; t < Index_Shading_Max; t++)
	{

		strFullData += strShadingData[t];
	}
	if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.bUse[ROI_Shading_CNT])
	{
		strData.Format(_T("CC_%.2f_"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.fPercent[ROI_Shading_CNT]);
		strFullData += strData;
		strData.Format(_T("%.2f:"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.sSignal[ROI_Shading_CNT]);
		strFullData += strData;
		strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.nEachResult[ROI_Shading_CNT]);

		strFullData += strData;

	}
	else{
		strFullData += _T(":1");
	}

	// 		stTestData.fPercent[nIndexVer_CT[nIdx]] = ((float)stTestData.sSignal[nIndexVer_CT[nIdx]] / (float)stTestData.sSignal[nStandardIndex[IDX_Shading_Ver_CT]] * 100.0f);
	// 		stTestData.fPercent[nIndexVer_CB[nIdx]] = ((float)stTestData.sSignal[nIndexVer_CB[nIdx]] / (float)stTestData.sSignal[nStandardIndex[IDX_Shading_Ver_CB]] * 100.0f);
	// 		stTestData.fPercent[nIndexHov_LC[nIdx]] = ((float)stTestData.sSignal[nIndexHov_LC[nIdx]] / (float)stTestData.sSignal[nStandardIndex[IDX_Shading_Hov_LC]] * 100.0f);
	// 		stTestData.fPercent[nIndexHov_RC[nIdx]] = ((float)stTestData.sSignal[nIndexHov_RC[nIdx]] / (float)stTestData.sSignal[nStandardIndex[IDX_Shading_Hov_RC]] * 100.0f);
	// 		stTestData.fPercent[nIndexDig_LT[nIdx]] = ((float)stTestData.sSignal[nIndexDig_LT[nIdx]] / (float)stTestData.sSignal[nStandardIndex[IDX_Shading_Dig_LT]] * 100.0f);
	// 		stTestData.fPercent[nIndexDig_LB[nIdx]] = ((float)stTestData.sSignal[nIndexDig_LB[nIdx]] / (float)stTestData.sSignal[nStandardIndex[IDX_Shading_Dig_LB]] * 100.0f);
	// 		stTestData.fPercent[nIndexDig_RT[nIdx]] = ((float)stTestData.sSignal[nIndexDig_RT[nIdx]] / (float)stTestData.sSignal[nStandardIndex[IDX_Shading_Dig_RT]] * 100.0f);
	// 		stTestData.fPercent[nIndexDig_RB[nIdx]] = ((float)stTestData.sSignal[nIndexDig_RB[nIdx]] / (float)stTestData.sSignal[nStandardIndex[IDX_Shading_Dig_RB]] * 100.0f);

	// 	for (int t = 0; t < ROI_SNR_Light_Max; t++)
	// 	{
	// 		if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stShadingData.bUse[t])
	// 		{
	// 			strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSNR_LightData.dbEtcValue[t], m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSNR_LightData.nResult);
	// 		}
	// 		else{
	// 			strData.Format(_T(":1"));
	// 		}
	// 
	// 		if (t < (ROI_SNR_Light_Max - 1))
	// 		{
	// 			strData += _T(",");
	// 		}
	// 		strFullData += strData;
	// 	}

	pMesData->szMesTestData[nParaIdx][MesEntryDataIdx_SNR_Light] = strFullData;
}

void CTestManager_TestMES::HotPixel_Entry_DataSave(int nParaIdx, ST_MES_Entry_Data *pMesData)
{
	//#ifdef SET_GWANGJU
	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%d:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stHotPixelData.iFailCount, m_pInspInfo->CamInfo[nParaIdx].stImageQ.stHotPixelData.nResult);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesEntryDataIdx_HotPixel] = strFullData;
	//#endif
}

void CTestManager_TestMES::FPN_Entry_DataSave(int nParaIdx, ST_MES_Entry_Data *pMesData)
{
	//#ifdef SET_GWANGJU
	CString strData;
	CString strFullData;

	strFullData.Empty();
	for (int t = 0; t < 2; t++)
	{
		strData.Format(_T("%d:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stFPNData.iFailCount[t], m_pInspInfo->CamInfo[nParaIdx].stImageQ.stFPNData.nEachResult[t]);

		if (t < 1)
		{
			strData += _T(",");
		}
		strFullData += strData;
	}

	pMesData->szMesTestData[nParaIdx][MesEntryDataIdx_FPN] = strFullData;
	//#endif
}

void CTestManager_TestMES::Depth_Entry_DataSave(int nParaIdx, ST_MES_Entry_Data *pMesData)
{
	//#ifdef SET_GWANGJU
	CString strData;
	CString strFullData;

	strFullData.Empty();
	for (int t = 0; t < 2; t++)
	{
		strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.st3D_DepthData.dbValue[t], m_pInspInfo->CamInfo[nParaIdx].stImageQ.st3D_DepthData.nEachResult[t]);

		if (t < 1)
		{
			strData += _T(",");
		}
		strFullData += strData;
	}

	pMesData->szMesTestData[nParaIdx][MesEntryDataIdx_3DDepth] = strFullData;
	//#endif
}

//=============================================================================
// Method		: EEPROM_DataSave
// Access		: public  
// Returns		: void
// Parameter	: int nParaIdx
// Parameter	: ST_MES_Data * pMesData
// Qualifier	:
// Last Update	: 2018/5/12 - 21:25
// Desc.		:
//=============================================================================
void CTestManager_TestMES::EEPROM_Entry_DataSave(int nParaIdx, ST_MES_Entry_Data *pMesData)
{
	//#ifdef SET_GWANGJU
	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.fOC_X, m_pInspInfo->CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.nEachResult[Spec_EEP_OC_X]);
	strData += _T(",");
	strFullData += strData;

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.fOC_Y, m_pInspInfo->CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.nEachResult[Spec_EEP_OC_Y]);
	strData += _T(",");
	strFullData += strData;

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.fCheckSum1, m_pInspInfo->CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.nEachResult[Spec_EEP_CheckSum1]);
	strData += _T(",");
	strFullData += strData;

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.fCheckSum2, m_pInspInfo->CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.nEachResult[Spec_EEP_CheckSum2]);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesEntryDataIdx_EEPROM] = strFullData;
	//#endif
}

//=============================================================================
// Method		: TemperatureSensor_DataSave
// Access		: public  
// Returns		: void
// Parameter	: int nParaIdx
// Parameter	: ST_MES_Data * pMesData
// Qualifier	:
// Last Update	: 2018/5/12 - 21:25
// Desc.		:
//=============================================================================
void CTestManager_TestMES::TemperatureSensor_Entry_DataSave(int nParaIdx, ST_MES_Entry_Data *pMesData)
{
	//#ifdef SET_GWANGJU
	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stTemperatureSensorData.dbTemperature, m_pInspInfo->CamInfo[nParaIdx].stImageQ.stTemperatureSensorData.nEachResult[Spec_Temp]);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesEntryDataIdx_TemperatureSensor] = strFullData;
	//#endif
}


void CTestManager_TestMES::Particle_Entry_DataSave(int nParaIdx, ST_MES_Entry_Data *pMesData)
{
	CString strData;
	CString strFullData;

	strFullData.Empty();
	strData.Format(_T("%d:%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stParticle_EntryData.nFailCount, m_pInspInfo->CamInfo[nParaIdx].stImageQ.stParticle_EntryData.nResult);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesEntryDataIdx_Particle_Entry] = strFullData;


}