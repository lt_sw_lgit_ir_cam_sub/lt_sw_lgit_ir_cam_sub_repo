﻿//*****************************************************************************
// Filename	: 	File_Report.cpp
// Created	:	2016/8/5 - 17:18
// Modified	:	2016/8/5 - 17:18
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "File_Report.h"
#include "CommonFunction.h"
#include <iostream>

#if (SET_INSPECTOR == SYS_STEREO_CAL)
#include "json/json.h"
#ifdef _WIN64	// 64bit
	#ifdef _DEBUG
		#ifdef _UNICODE
			#pragma comment (lib,"../Lib/64bit/jsoncpp_x64_ud.lib")
		#else
			#pragma comment (lib,"../Lib/64bit/jsoncpp_x64_d.lib")
		#endif
	#else
		#ifdef _UNICODE
			#pragma comment (lib,"../Lib/64bit/jsoncpp_x64_u.lib")
		#else
			#pragma comment (lib,"../Lib/64bit/jsoncpp_x64.lib")
		#endif
	#endif

#else
	#ifdef _DEBUG
		#ifdef _UNICODE
			#pragma comment (lib,"../Lib/32bit/jsoncpp_ud.lib")
		#else
			#pragma comment (lib,"../Lib/32bit/jsoncpp_d.lib")
		#endif
	#else
		#ifdef _UNICODE
			#pragma comment (lib,"../Lib/32bit/jsoncpp_u.lib")
		#else
			#pragma comment (lib,"../Lib/32bit/jsoncpp.lib")
		#endif
	#endif
#endif
#endif



static LPCTSTR g_szHeaderz_2DCal[] = 
{ 
	_T("Date"), 
	_T("Barcode"), 
	_T("Focal Length U"), 
	_T("Focal Length V"), 
	_T("Optical Center X"), 
	_T("Optical Center Y"), 
	_T("Kc[0]"), 
	_T("Kc[1]"), 
	_T("Kc[2]"), 
	_T("Kc[3]"), 
	_T("Kc[4]"),
	_T("DistFov"), 
	_T("Intrinsc Fov"), 
	_T("Reprojection Error"), 
	_T("Eval Error"), 
	_T("Org Offset"),
	_T("Detection FailureCnt"), 
	_T("Invalid PointCnt"), 
	_T("Valid PointCnt"), 
	_T("Date"),
	_T("Camera Init"),
	_T("Corner Point Step"),
	_T("Calibration"),
	_T("EEPROM"),
	NULL 
};

static LPCTSTR g_szHeaderz_SteCal_Mono[] = 
{ 
	_T("Date"), 
	_T("Barcode"), 
	_T("focal_len x"),
	_T("focal_len y"), 
	_T("principal_point x"), 
	_T("principal_point y"), 
	_T("K1"), 
	_T("K2"), 
	_T("K3"), 
	_T("K4"), 
	_T("K5"), 
	_T("rms"), 
	_T("calib_res_width"), 
	_T("calib_res_height"), 
	NULL 
};

static LPCTSTR g_szHeaderz_SteCal_Stereo[] = 
{
	_T("Date"), 
	_T("Barcode"),
	_T("master focal_len x"), 
	_T("master focal_len y"), 
	_T("master principal_point x"), 
	_T("master principal_point y"), 
	_T("master K1"), 
	_T("master K2"), 
	_T("master K3"), 
	_T("master K4"), 
	_T("master K5"), 
	_T("master rms"), 
	_T("master calib_res_width"), 
	_T("master calib_res_height"),
	_T("slave focal_len x"), 
	_T("slave focal_len y"), 
	_T("slave principal_point x"), 
	_T("slave principal_point y"), 
	_T("slave K1"), 
	_T("slave K2"), 
	_T("slave K3"), 
	_T("slave K4"), 
	_T("slave K5"), 
	_T("slave rms"), 
	_T("slave calib_res_width"), 
	_T("slave calib_res_height"),
	_T("rot[1]"), 
	_T("rot[2]"), 
	_T("rot[3]"), 
	_T("trans[1]"), 
	_T("trans[2]"), 
	_T("trans[3]"),
	_T("X"),
	_T("Y"), 
	_T("Z"), 
	_T("pitch"), 
	_T("yaw"), 
	_T("roll"), 
	_T("streo_rms"),
	NULL 
};

CFile_Report::CFile_Report()
{
}


CFile_Report::~CFile_Report()
{
}

//=============================================================================
// Method		: SaveFinalizeResult
// Access		: public  
// Returns		: BOOL
// Parameter	: __in CString szFullPath
// Parameter	: __in CStringArray * pszHeadArray
// Parameter	: __in CStringArray * pszDataArray
// Qualifier	:
// Last Update	: 2017/11/18 - 13:46
// Desc.		:
//=============================================================================
BOOL CFile_Report::SaveFinalizeResult( __in CString szFullPath, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray)
{
	CString szDirPath;
	CString szUNICODE;
	CString szData;

	CFile File;
	CFileException e;
	if (!PathFileExists(szFullPath))
	{
		if (!File.Open(szFullPath, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}

		// Header 추가
		CString szHeader;
		for (int _a = 0; _a < pszHeadArray->GetCount(); _a++)
		{
			szHeader += pszHeadArray->GetAt(_a);
			szHeader += _T(",");
		}

		for (int _a = 0; _a < pszDataArray->GetCount(); _a++)
		{
			szData += pszDataArray->GetAt(_a);
			szData += _T(",");
		}

		szHeader += _T("\r\n");
		szUNICODE = szHeader + szData;
		szUNICODE += _T("\r\n");
	}
	else
	{
		if (!File.Open(szFullPath, CFile::modeWrite | CFile::shareDenyWrite, &e))
		{
			return FALSE;
		}

		for (int _a = 0; _a < pszDataArray->GetCount(); _a++)
		{
			szData += pszDataArray->GetAt(_a);
			szData += _T(",");
		}

		szUNICODE = szData;
		szUNICODE += _T("\r\n");
	}

	CStringA szANSI;
	USES_CONVERSION;
	szANSI = CT2A(szUNICODE.GetBuffer(0));

	File.SeekToEnd();
	//File.Write(szBuff.GetBuffer(0), szBuff.GetLength() * sizeof(TCHAR));
	File.Write(szANSI.GetBuffer(0), szANSI.GetLength());

	File.Flush();
	File.Close();

	return TRUE;
}

//=============================================================================
// Method		: Make2DCalHeadAndData
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in CString szBarcode
// Parameter	: __in const ST_2DCal_Result * pstResult
// Parameter	: __in CStringArray * pszHeadArray
// Parameter	: __in CStringArray * pszDataArray
// Qualifier	:
// Last Update	: 2018/3/5 - 16:19
// Desc.		:
//=============================================================================
BOOL CFile_Report::Make2DCalHeadAndData(__in CString szBarcode, __in const ST_2DCal_Result* pstResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray)
{
	ASSERT(pszHeadArray != NULL);
	ASSERT(pszDataArray != NULL);

	pszHeadArray->RemoveAll();
	pszDataArray->RemoveAll();

	CString szItem;

	//LPCTSTR szHeaderz[] = { _T("Date"), _T("Barcode"), _T("Focal Length U"), _T("Focal Length V"), _T("Optical Center X"), _T("Optical Center Y"), _T("Kc[0]"), _T("Kc[1]"), _T("Kc[2]"), _T("Kc[3]"), _T("Kc[4]"),
	//	_T("DistFov"), _T("Intrinsc Fov"), _T("Reprojection Error"), _T("Eval Error"), _T("Org Offset"), _T("Detection FailureCnt"), _T("Invalid PointCnt"), _T("Valid PointCnt"), _T("Date"), NULL };

	SYSTEMTIME tmLocal;
	GetLocalTime(&tmLocal);

	// Header
	for (int nIdx = 0; NULL != g_szHeaderz_2DCal[nIdx]; nIdx++)
	{
		pszHeadArray->Add(g_szHeaderz_2DCal[nIdx]);
	}
	
	// Date
	szItem.Format(_T("%04d/%02d/%02d[%02d:%02d:%02d]"), tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay, tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
	pszDataArray->Add(szItem);

	// Barcode
	szItem.Format(_T("%s"), szBarcode);
	pszDataArray->Add(szItem);

	for (int nIdx = 0; nIdx < MAX_2DCAL_Re_KK; nIdx++)
	{
		szItem.Format(_T("%f"), pstResult->arfKK[nIdx]);
		pszDataArray->Add(szItem);
	}

	for (int nIdx = 0; nIdx < MAX_2DCAL_Re_Kc; nIdx++)
	{
		szItem.Format(_T("%e"), pstResult->arfKc[nIdx]);
		pszDataArray->Add(szItem);
	}

	szItem.Format(_T("%f"), pstResult->fDistFOV);
	pszDataArray->Add(szItem);

	// fR2Max
	szItem.Format(_T("%f"), pstResult->fR2Max);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), pstResult->fRepError);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), pstResult->fEvalError);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), pstResult->fOrgOffset);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%d"), pstResult->nDetectionFailureCnt);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%d"), pstResult->nInvalidPointCnt);
	pszDataArray->Add(szItem);

	//nValidPointCnt
	szItem.Format(_T("%d"), pstResult->nValidPointCnt);
	pszDataArray->Add(szItem);

	CStringA szTemp;
	for (UINT nIdx = 0; nIdx < 6; nIdx++)
	{
		szTemp.AppendChar(pstResult->arcDate[nIdx]);
	}
	USES_CONVERSION;
	szItem = CA2T(szTemp.GetBuffer(0));
	pszDataArray->Add(szItem);

	return TRUE;
}

//=============================================================================
// Method		: Make2DCalHeadAndData
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in const ST_CamInfo * pstResult
// Parameter	: __in CStringArray * pszHeadArray
// Parameter	: __in CStringArray * pszDataArray
// Qualifier	:
// Last Update	: 2018/3/19 - 10:39
// Desc.		:
//=============================================================================
BOOL CFile_Report::Make2DCalHeadAndData(__in const ST_CamInfo* pstResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray)
{
	ASSERT(pszHeadArray != NULL);
	ASSERT(pszDataArray != NULL);

	pszHeadArray->RemoveAll();
	pszDataArray->RemoveAll();

	CString szItem;
	SYSTEMTIME tmLocal;
	GetLocalTime(&tmLocal);

	// Header
	for (int nIdx = 0; NULL != g_szHeaderz_2DCal[nIdx]; nIdx++)
	{
		pszHeadArray->Add(g_szHeaderz_2DCal[nIdx]);
	}

	// Date
	szItem.Format(_T("%04d/%02d/%02d[%02d:%02d:%02d]"), tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay, tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
	pszDataArray->Add(szItem);

	// Barcode
	szItem.Format(_T("%s"), pstResult->szBarcode);
	pszDataArray->Add(szItem);

	for (int nIdx = 0; nIdx < MAX_2DCAL_Re_KK; nIdx++)
	{
		szItem.Format(_T("%f"), pstResult->st2D_Result.arfKK[nIdx]);
		pszDataArray->Add(szItem);
	}

	for (int nIdx = 0; nIdx < MAX_2DCAL_Re_Kc; nIdx++)
	{
		szItem.Format(_T("%e"), pstResult->st2D_Result.arfKc[nIdx]);
		pszDataArray->Add(szItem);
	}

	szItem.Format(_T("%f"), pstResult->st2D_Result.fDistFOV);
	pszDataArray->Add(szItem);

	// fR2Max
	szItem.Format(_T("%f"), pstResult->st2D_Result.fR2Max);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), pstResult->st2D_Result.fRepError);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), pstResult->st2D_Result.fEvalError);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), pstResult->st2D_Result.fOrgOffset);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%d"), pstResult->st2D_Result.nDetectionFailureCnt);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%d"), pstResult->st2D_Result.nInvalidPointCnt);
	pszDataArray->Add(szItem);

	//nValidPointCnt
	szItem.Format(_T("%d"), pstResult->st2D_Result.nValidPointCnt);
	pszDataArray->Add(szItem);


	// * 카메라 초기화
	//szItem.Format(_T("%d"), pstResult->nInitialize);
	szItem = g_szResultCode[pstResult->nInitialize];
	pszDataArray->Add(szItem);

	// * 코너점 추출
	if (0 == pstResult->nErr_CornerPtStepNo)
	{
		szItem = _T("OK");
	}
	else
	{
		szItem.Format(_T("%d"), pstResult->nErr_CornerPtStepNo);
	}
	pszDataArray->Add(szItem);

	// * Calibration
	//szItem.Format(_T("%d"), pstResult->n2DCAL_Result);
	szItem = g_szResultCode_LG_CalIntr[pstResult->nCAL_ResultCode];
	pszDataArray->Add(szItem);

	// * EEPROM Write
	//szItem.Format(_T("%d"), pstResult->nEEPROM_Result);
	szItem = g_szResultCode_EEPROM[pstResult->nEEPROM_Result];
	pszDataArray->Add(szItem);

	CStringA szTemp;
	for (UINT nIdx = 0; nIdx < 6; nIdx++)
	{
		szTemp.AppendChar(pstResult->st2D_Result.arcDate[nIdx]);
	}
	USES_CONVERSION;
	szItem = CA2T(szTemp.GetBuffer(0));
	pszDataArray->Add(szItem);

	return TRUE;
}

//=============================================================================
// Method		: MakeSteCalHeadAndData_Mono
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in CString szBarcode
// Parameter	: __in const SINGLE_CALIBRATION_RESULT * pstResult
// Parameter	: __in CStringArray * pszHeadArray
// Parameter	: __in CStringArray * pszDataArray
// Qualifier	:
// Last Update	: 2018/3/5 - 15:52
// Desc.		:
//=============================================================================
BOOL CFile_Report::MakeSteCalHeadAndData_Mono(__in CString szBarcode, __in const SINGLE_CALIBRATION_RESULT* pstResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray)
{
	ASSERT(pszHeadArray == NULL);
	ASSERT(pszDataArray == NULL);

	pszHeadArray->RemoveAll();
	pszDataArray->RemoveAll();

	CString szItem;

	//LPCTSTR szHeaderz[] = { _T("Date"), _T("Barcode"), _T("focal_len x"), _T("focal_len y"), _T("principal_point x"), _T("principal_point y"), _T("K1"), _T("K2"), _T("K3"), _T("K4"), _T("K5"), _T("rms"), _T("calib_res_width"), _T("calib_res_height"), NULL };

	SYSTEMTIME tmLocal;
	GetLocalTime(&tmLocal);

	// Header
	for (int nIdx = 0; NULL != g_szHeaderz_SteCal_Mono[nIdx]; nIdx++)
	{
		pszHeadArray->Add(g_szHeaderz_SteCal_Mono[nIdx]);
	}

	// Date
	szItem.Format(_T("%04d/%02d/%02d[%02d:%02d:%02d]"), tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay, tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
	pszDataArray->Add(szItem);

	// Barcode
	szItem.Format(_T("%s"), szBarcode);
	pszDataArray->Add(szItem);


// 	double fx, fy;
// 	double cx, cy;
// 	double distortion_cofficeints[5]; // k1-k5: distortion_cofficeints[0] - distortion_cofficeints[4]
// 	double rms;
// 	int calib_res_width, calib_res_height;

	szItem.Format(_T("%f"), pstResult->fx);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), pstResult->fy);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), pstResult->cx);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), pstResult->cy);
	pszDataArray->Add(szItem);

	for (UINT nIdx = 0; nIdx < 5; nIdx++)
	{
		szItem.Format(_T("%f"), pstResult->distortion_cofficeints[nIdx]);
		pszDataArray->Add(szItem);
	}

	szItem.Format(_T("%f"), pstResult->rms);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%d"), 1280);//pstResult->calib_res_width);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%d"), 724);//pstResult->calib_res_height);
	pszDataArray->Add(szItem);



	return TRUE;
}

//=============================================================================
// Method		: MakeSteCalHeadAndData_Dual
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in CString szBarcode
// Parameter	: __in const STEREO_CALIBRATION_RESULT * pstResult
// Parameter	: __in CStringArray * pszHeadArray
// Parameter	: __in CStringArray * pszDataArray
// Qualifier	:
// Last Update	: 2018/3/5 - 16:39
// Desc.		:
//=============================================================================
BOOL CFile_Report::MakeSteCalHeadAndData_Dual(__in CString szBarcode, __in const STEREO_CALIBRATION_RESULT* pstResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray)
{
	ASSERT(pszHeadArray == NULL);
	ASSERT(pszDataArray == NULL);

	pszHeadArray->RemoveAll();
	pszDataArray->RemoveAll();

	CString szItem;

// 	LPCTSTR szHeaderz[] = { _T("Date"), _T("Barcode"), 
// 	_T("master focal_len x"), _T("master focal_len y"), _T("master principal_point x"), _T("master principal_point y"), _T("master K1"), _T("master K2"), _T("master K3"), _T("master K4"), _T("master K5"), _T("master rms"), _T("master calib_res_width"), _T("master calib_res_height"), 
// 	_T("slave focal_len x"), _T("slave focal_len y"), _T("slave principal_point x"), _T("slave principal_point y"), _T("slave K1"), _T("slave K2"), _T("slave K3"), _T("slave K4"), _T("slave K5"), _T("slave rms"), _T("slave calib_res_width"), _T("slave calib_res_height"), 
// 	_T("rot[1]"), _T("rot[2]"), _T("rot[3]"), _T("trans[1]"), _T("trans[2]"), _T("trans[3]"),
// 	_T("X"), _T("Y"), _T("Z"), _T("pitch"), _T("yaw"), _T("roll"), _T("streo_rms"),
// 	NULL };

	SYSTEMTIME tmLocal;
	GetLocalTime(&tmLocal);

	// Header
	for (int nIdx = 0; NULL != g_szHeaderz_SteCal_Stereo[nIdx]; nIdx++)
	{
		pszHeadArray->Add(g_szHeaderz_SteCal_Stereo[nIdx]);
	}

	// Date
	szItem.Format(_T("%04d/%02d/%02d[%02d:%02d:%02d]"), tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay, tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
	pszDataArray->Add(szItem);

	// Barcode
	szItem.Format(_T("%s"), szBarcode);
	pszDataArray->Add(szItem);

	// * Master
	szItem.Format(_T("%f"), pstResult->master_fx);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), pstResult->master_fy);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), pstResult->master_cx);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), pstResult->master_cy);
	pszDataArray->Add(szItem);

	for (UINT nIdx = 0; nIdx < 5; nIdx++)
	{
		szItem.Format(_T("%f"), pstResult->master_distortion_cofficeints[nIdx]);
		pszDataArray->Add(szItem);
	}

	szItem.Format(_T("%f"), pstResult->master_rms);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%d"), 1280);//pstResult->master_calib_res_width);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%d"), 964);//pstResult->master_calib_res_height);
	pszDataArray->Add(szItem);

	// * Slave
	szItem.Format(_T("%f"), pstResult->slave_fx);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), pstResult->slave_fy);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), pstResult->slave_cx);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), pstResult->slave_cy);
	pszDataArray->Add(szItem);

	for (UINT nIdx = 0; nIdx < 5; nIdx++)
	{
		szItem.Format(_T("%f"), pstResult->slave_distortion_cofficeints[nIdx]);
		pszDataArray->Add(szItem);
	}

	szItem.Format(_T("%f"), pstResult->slave_rms);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%d"), 1280);//pstResult->slave_calib_res_width);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%d"), 964);//pstResult->slave_calib_res_height);
	pszDataArray->Add(szItem);

	// * streo camera
	for (UINT nIdx = 0; nIdx < 3; nIdx++)
	{
		szItem.Format(_T("%f"), pstResult->rotation_vector[nIdx]);
		pszDataArray->Add(szItem);
	}

	for (UINT nIdx = 0; nIdx < 3; nIdx++)
	{
		szItem.Format(_T("%f"), pstResult->translation_vector[nIdx]);
		pszDataArray->Add(szItem);
	}

	szItem.Format(_T("%f"), pstResult->X);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), pstResult->Y);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), pstResult->Z);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), pstResult->pitch);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), pstResult->yaw);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), pstResult->roll);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), pstResult->streo_rms);
	pszDataArray->Add(szItem);


	return TRUE;
}

//=============================================================================
// Method		: Make_HeadAndData_SteCal_Mono_EEPROM
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in CString szBarcode
// Parameter	: __in const SYSTEMTIME * pstTime
// Parameter	: __in const SINGLE_CALIBRATION_RESULT * pstResult
// Parameter	: __in CStringArray * pszHeadArray
// Parameter	: __in CStringArray * pszDataArray
// Qualifier	:
// Last Update	: 2018/3/11 - 13:15
// Desc.		:
//=============================================================================
BOOL CFile_Report::Make_HeadAndData_SteCal_Mono_EEPROM(__in CString szBarcode, __in const SYSTEMTIME* pstTime, __in const SINGLE_CALIBRATION_RESULT* pstResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray)
{
	ASSERT(pszHeadArray == NULL);
	ASSERT(pszDataArray == NULL);

	pszHeadArray->RemoveAll();
	pszDataArray->RemoveAll();

	CString szItem;

	LPCTSTR szHeaderz[] = { _T("Date"), _T("Barcode"), _T("focal_len x"), _T("focal_len y"), _T("principal_point x"), _T("principal_point y"), _T("K1"), _T("K2"), _T("K3"), _T("K4"), _T("K5"), NULL };

	// Header
	for (int nIdx = 0; NULL != szHeaderz[nIdx]; nIdx++)
	{
		pszHeadArray->Add(szHeaderz[nIdx]);
	}

	// Date
	szItem.Format(_T("%04d/%02d/%02d[%02d:%02d]"), pstTime->wYear, pstTime->wMonth, pstTime->wDay, pstTime->wHour, pstTime->wMinute);
	pszDataArray->Add(szItem);

	// Barcode
	szItem.Format(_T("%s"), szBarcode);
	pszDataArray->Add(szItem);

	// 	double fx, fy;
	// 	double cx, cy;
	// 	double distortion_cofficeints[5]; // k1-k5: distortion_cofficeints[0] - distortion_cofficeints[4]

	szItem.Format(_T("%f"), pstResult->fx);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), pstResult->fy);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), pstResult->cx);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), pstResult->cy);
	pszDataArray->Add(szItem);

	for (UINT nIdx = 0; nIdx < 5; nIdx++)
	{
		szItem.Format(_T("%f"), pstResult->distortion_cofficeints[nIdx]);
		pszDataArray->Add(szItem);
	}

	return TRUE;
}

BOOL CFile_Report::Make_HeadAndData_SteCal_Ste_EEPROM(__in BOOL bMaster, __in BOOL bMainPage, __in CString szBarcode, __in const SYSTEMTIME* pstTime, __in const STEREO_CALIBRATION_RESULT* pstResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray)
{
	ASSERT(pszHeadArray != NULL);
	ASSERT(pszDataArray != NULL);

	pszHeadArray->RemoveAll();
	pszDataArray->RemoveAll();

	CString szItem;

	LPCTSTR szHeaderz[] = { _T("Camera"), _T("Date"), _T("Barcode"), _T("focal_len x"), _T("focal_len y"), _T("principal_point x"), _T("principal_point y"), _T("K1"), _T("K2"), _T("K3"), _T("K4"), _T("K5"), _T("Trans X"), _T("Trans Y"), _T("Trans Z"), _T("Rot X"), _T("Rot Y"), _T("Rot Z"), NULL };

	// Header
	for (int nIdx = 0; NULL != szHeaderz[nIdx]; nIdx++)
	{
		pszHeadArray->Add(szHeaderz[nIdx]);
	}

	// Camera : Master/Slave, MainPage/MirrorPage
	if (bMaster)
	{
		if (bMainPage)
			szItem = _T("Master");
		else
			szItem = _T("Master_Mirror");
	}
	else
	{
		if (bMainPage)
			szItem = _T("Slave");
		else
			szItem = _T("Slave_Mirror");
	}
	pszDataArray->Add(szItem);

	// Date
	szItem.Format(_T("%04d/%02d/%02d[%02d:%02d]"), pstTime->wYear, pstTime->wMonth, pstTime->wDay, pstTime->wHour, pstTime->wMinute);
	pszDataArray->Add(szItem);

	// Barcode
	szItem.Format(_T("%s"), szBarcode);
	pszDataArray->Add(szItem);

	// Data
	szItem.Format(_T("%f"), bMaster ? pstResult->master_fx : pstResult->slave_fx);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), bMaster ? pstResult->master_fy : pstResult->slave_fy);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), bMaster ? pstResult->master_cx : pstResult->slave_cx);
	pszDataArray->Add(szItem);

	szItem.Format(_T("%f"), bMaster ? pstResult->master_cy : pstResult->slave_cy);
	pszDataArray->Add(szItem);

	for (UINT nIdx = 0; nIdx < 5; nIdx++)
	{
		szItem.Format(_T("%f"), bMaster ? pstResult->master_distortion_cofficeints[nIdx] : pstResult->slave_distortion_cofficeints[nIdx]);
		pszDataArray->Add(szItem);
	}

	// Trans X, Y, Z
	for (UINT nIdx = 0; nIdx < 3; nIdx++)
	{
		if (bMaster)
			szItem = _T("0");
		else
			szItem.Format(_T("%f"), pstResult->translation_vector[nIdx]);

		pszDataArray->Add(szItem);
	}

	// Rot X
	if (bMaster)
		szItem = _T("0");
	else  // (임시 수정 : 2018.6.12 -> 1, 2, 0 순서에서 0, 1, 2로 바꿈... 임시)
		szItem.Format(_T("%f"), pstResult->roll);	//pstResult->rotation_vector[0]);
	pszDataArray->Add(szItem);

	// Rot Y
	if (bMaster)
		szItem = _T("0");
	else
		szItem.Format(_T("%f"), pstResult->pitch);	//pstResult->rotation_vector[1]);
	pszDataArray->Add(szItem);

	// Rot Z
	if (bMaster)
		szItem = _T("0");
	else
		szItem.Format(_T("%f"), pstResult->yaw);	//pstResult->rotation_vector[2]);
	pszDataArray->Add(szItem);

	return TRUE;
}

//=============================================================================
// Method		: SaveFinalizeResult2DCal
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in UINT nParaIdx
// Parameter	: __in CString szBarcode
// Parameter	: __in const ST_2DCal_Result * pstResult
// Qualifier	:
// Last Update	: 2018/3/5 - 16:19
// Desc.		:
//=============================================================================
BOOL CFile_Report::SaveFinalizeResult2DCal(__in LPCTSTR szPath, __in UINT nParaIdx, __in CString szBarcode, __in const ST_2DCal_Result* pstResult)
{
	CString			szFileName;
	CString			szFullPath;
	SYSTEMTIME		tmLocal;
	GetLocalTime(&tmLocal);

	if (Para_Left == nParaIdx)
	{
		szFileName.Format(_T("%s_Left_%04d%02d%02d_%02d%02d%02d.csv"), szBarcode, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay,
																		tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
	}
	else
	{
		szFileName.Format(_T("%s_Right_%04d%02d%02d_%02d%02d%02d.csv"), szBarcode, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay,
																		tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
	}

	szFullPath.Format(_T("%s\\%04d_%02d_%02d\\"), szPath, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay);
	MakeDirectory(szFullPath);

	szFullPath += szFileName;

	CStringArray arrHeaderz;
	CStringArray arrDataz;

	Make2DCalHeadAndData(szBarcode, pstResult, &arrHeaderz, &arrDataz);

	SaveFinalizeResult(szFullPath, &arrHeaderz, &arrDataz);

	return TRUE;
}

//=============================================================================
// Method		: SaveFinalizeResult2DCal
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in UINT nParaIdx
// Parameter	: __in const ST_CamInfo * pstResult
// Qualifier	:
// Last Update	: 2018/3/19 - 10:40
// Desc.		:
//=============================================================================
BOOL CFile_Report::SaveFinalizeResult2DCal(__in LPCTSTR szPath, __in UINT nParaIdx, __in const ST_CamInfo* pstResult)
{
	CString			szFileName;
	CString			szFullPath;
	SYSTEMTIME		tmLocal;
	GetLocalTime(&tmLocal);

	if (Para_Left == nParaIdx)
	{
		szFileName.Format(_T("%s_Left_%04d%02d%02d_%02d%02d%02d.csv"), pstResult->szBarcode, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay,
			tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
	}
	else
	{
		szFileName.Format(_T("%s_Right_%04d%02d%02d_%02d%02d%02d.csv"), pstResult->szBarcode, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay,
			tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
	}

	szFullPath.Format(_T("%s\\%04d_%02d_%02d\\"), szPath, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay);
	MakeDirectory(szFullPath);

	szFullPath += szFileName;

	CStringArray arrHeaderz;
	CStringArray arrDataz;

	Make2DCalHeadAndData(pstResult, &arrHeaderz, &arrDataz);

	SaveFinalizeResult(szFullPath, &arrHeaderz, &arrDataz);

	return TRUE;
}

//=============================================================================
// Method		: SaveFinalizeResultSteCal_Mono
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in CString szBarcode
// Parameter	: __in const SINGLE_CALIBRATION_RESULT * pstResult
// Qualifier	:
// Last Update	: 2018/3/5 - 15:50
// Desc.		:
//=============================================================================
BOOL CFile_Report::SaveFinalizeResultSteCal_Mono(__in LPCTSTR szPath, __in CString szBarcode, __in const SINGLE_CALIBRATION_RESULT* pstResult)
{
	CString			szFileName;
	CString			szFullPath;
	SYSTEMTIME		tmLocal;
	GetLocalTime(&tmLocal);

	szFileName.Format(_T("%s_Mono_%04d%02d%02d_%02d%02d%02d.csv"), szBarcode, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay,
																tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
	

	szFullPath.Format(_T("%s\\%04d_%02d_%02d\\"), szPath, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay);
	MakeDirectory(szFullPath);

	szFullPath += szFileName;

	CStringArray arrHeaderz;
	CStringArray arrDataz;

	MakeSteCalHeadAndData_Mono(szBarcode, pstResult, &arrHeaderz, &arrDataz);

	SaveFinalizeResult(szFullPath, &arrHeaderz, &arrDataz);

	return TRUE;
}

//=============================================================================
// Method		: SaveFinalizeResultSteCal_Dual
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in CString szBarcode
// Parameter	: __in const STEREO_CALIBRATION_RESULT * pstResult
// Qualifier	:
// Last Update	: 2018/3/5 - 17:04
// Desc.		:
//=============================================================================
BOOL CFile_Report::SaveFinalizeResultSteCal_Dual(__in LPCTSTR szPath, __in CString szBarcode, __in const STEREO_CALIBRATION_RESULT* pstResult)
{
	CString			szFileName;
	CString			szFullPath;
	SYSTEMTIME		tmLocal;
	GetLocalTime(&tmLocal);

	szFileName.Format(_T("%s_Stereo_%04d%02d%02d_%02d%02d%02d.csv"), szBarcode, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay,
		tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);


	szFullPath.Format(_T("%s\\%04d_%02d_%02d\\"), szPath, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay);
	MakeDirectory(szFullPath);

	szFullPath += szFileName;

	CStringArray arrHeaderz;
	CStringArray arrDataz;

	MakeSteCalHeadAndData_Dual(szBarcode, pstResult, &arrHeaderz, &arrDataz);

	SaveFinalizeResult(szFullPath, &arrHeaderz, &arrDataz);

	return TRUE;
}

//=============================================================================
// Method		: Save_SteCal_DetectPattern
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath						-> 파일 저장 경로
// Parameter	: __in CString szBarcode					-> 제품 바코드
// Parameter	: __in BOOL bStereo							-> TRUE : Stereo, FALSE : Mono
// Parameter	: __in const SYSTEMTIME * pInTime			-> 제품 투입시간
// Parameter	: __in UINT nConerStep						-> 코너 좌표 추출 단계
// Parameter	: __in const ST_SteCal_Result * pstResult	-> 결과 데이터
// Parameter	: __in BOOL bError							-> 코너 좌표 추출 성공 여부
// Qualifier	:
// Last Update	: 2018/4/6 - 13:15
// Desc.		:
//=============================================================================
BOOL CFile_Report::Save_SteCal_DetectPattern(__in LPCTSTR szPath, __in CString szBarcode, __in BOOL bStereo, __in const SYSTEMTIME* pInTime, __in UINT nConerStep, __in const ST_SteCal_Result* pstResult, __in BOOL bError /*= FALSE*/)
{
	// 시간, 바코드, 단계, 성공여부, 인식된 데이터 개수, 데이터 (x, y)
	CString			szFileName;
	CString			szFullPath;

	szFileName.Format(_T("%s_DetectPattern.csv"), szBarcode);
	szFullPath.Format(_T("%s\\%04d_%02d_%02d\\"), szPath, pInTime->wYear, pInTime->wMonth, pInTime->wDay);
	MakeDirectory(szFullPath);

	szFullPath += szFileName;

	CStringArray arrHeaderz;
	CStringArray arrDataz;
	CString szItem;

	// Header
	// 시간, 바코드, Master/Slave, 단계, 성공여부, 인식된 데이터 개수, 데이터 (x, y)
	arrHeaderz.Add(_T("Date"));
	arrHeaderz.Add(_T("Barcode"));
	arrHeaderz.Add(_T("Camera"));
	arrHeaderz.Add(_T("Step"));
	arrHeaderz.Add(_T("Result"));
	arrHeaderz.Add(_T("Count"));
	arrHeaderz.Add(_T("x"));
	arrHeaderz.Add(_T("y"));
// 	for (int nIdx = 0; NULL != g_szHeaderz_SteCal_Stereo[nIdx]; nIdx++)
// 	{
// 		arrHeaderz.Add(g_szHeaderz_SteCal_Stereo[nIdx]);
// 	}

	// Date
	szItem.Format(_T("%04d/%02d/%02d[%02d:%02d:%02d]"), pInTime->wYear, pInTime->wMonth, pInTime->wDay, pInTime->wHour, pInTime->wMinute, pInTime->wSecond);
	arrDataz.Add(szItem);

	// Barcode
	szItem.Format(_T("%s"), szBarcode);
	arrDataz.Add(szItem);

	// Master/Slave
	arrDataz.Add(_T("Master"));

	// 단계
	szItem.Format(_T("%d"), nConerStep + 1);
	arrDataz.Add(szItem);

	// 성공여부
	if (bError)
		szItem = _T("Fail");
	else
		szItem = _T("Pass");
	arrDataz.Add(szItem);

	// 인식된 데이터 개수
	szItem.Format(_T("%d"), pstResult->nPtCount_M[nConerStep]);
	arrDataz.Add(szItem);

	// 데이터 (x, y)
	for (INT nIdx = 0; nIdx < pstResult->nPtCount_M[nConerStep]; nIdx++)
	{
		szItem.Format(_T("%d"), pstResult->ptPattern_M[nConerStep][nIdx].x);
		arrDataz.Add(szItem);

		szItem.Format(_T("%d"), pstResult->ptPattern_M[nConerStep][nIdx].y);
		arrDataz.Add(szItem);
	}

	// Master 파일 출력
	SaveFinalizeResult(szFullPath, &arrHeaderz, &arrDataz);

	if (bStereo)
	{
		arrDataz.RemoveAll();

		// Date
		szItem.Format(_T("%04d/%02d/%02d[%02d:%02d:%02d]"), pInTime->wYear, pInTime->wMonth, pInTime->wDay, pInTime->wHour, pInTime->wMinute, pInTime->wSecond);
		arrDataz.Add(szItem);

		// Barcode
		szItem.Format(_T("%s"), szBarcode);
		arrDataz.Add(szItem);

		// Master/Slave
		arrDataz.Add(_T("Slave"));

		// 단계
		szItem.Format(_T("%d"), nConerStep + 1);
		arrDataz.Add(szItem);

		// 성공여부
		if (bError)
			szItem = _T("Fail");
		else
			szItem = _T("Pass");
		arrDataz.Add(szItem);

		// 인식된 데이터 개수
		szItem.Format(_T("%d"), pstResult->nPtCount_S[nConerStep]);
		arrDataz.Add(szItem);

		// 데이터 (x, y)
		for (INT nIdx = 0; nIdx < pstResult->nPtCount_S[nConerStep]; nIdx++)
		{
			szItem.Format(_T("%d"), pstResult->ptPattern_S[nConerStep][nIdx].x);
			arrDataz.Add(szItem);

			szItem.Format(_T("%d"), pstResult->ptPattern_S[nConerStep][nIdx].y);
			arrDataz.Add(szItem);
		}

		// Slave 파일 출력
		SaveFinalizeResult(szFullPath, &arrHeaderz, &arrDataz);
	}

	return TRUE;
}

//=============================================================================
// Method		: SaveJSON_StereoCAL_Mono
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const SINGLE_CALIBRATION_RESULT * pstResult
// Qualifier	:
// Last Update	: 2018/2/26 - 10:14
// Desc.		:
//=============================================================================
BOOL CFile_Report::SaveJSON_StereoCAL_Mono(__in LPCTSTR szPath, __in const SINGLE_CALIBRATION_RESULT* pstResult)
{
#if (SET_INSPECTOR == SYS_STEREO_CAL)
	Json::Value root(Json::objectValue);
	Json::Value master;
	Json::Value lens_params_m;
	Json::Value camera_pose;

	Json::Value focal_len_m;
	Json::Value principal_point_m;
	Json::Value calib_res_m;

	focal_len_m.append(pstResult->fx);	// "focal_len": [%f, %f],\n' % (fx_left[0]*-1, fy_left[0]*-1))
	focal_len_m.append(pstResult->fy);
	principal_point_m.append(pstResult->cx);	// "principal_point": [%f, %f],\n' % (cx_left[0], cy_left[0]-2))
	principal_point_m.append(pstResult->cy);
	calib_res_m.append(1280);//pstResult->calib_res_width);
	calib_res_m.append(724);//pstResult->calib_res_height);

	root["type"] = "Mono Calibration";
	root["version"] = 1.0;

	master["serial"] = 0;
	lens_params_m["focal_len"] = focal_len_m;
	lens_params_m["principal_point"] = principal_point_m;
	lens_params_m["skew"] = 0.0;
	lens_params_m["k1"] = pstResult->distortion_cofficeints[0];
	lens_params_m["k2"] = pstResult->distortion_cofficeints[1];
	lens_params_m["k3"] = pstResult->distortion_cofficeints[2];
	lens_params_m["k4"] = pstResult->distortion_cofficeints[3];
	lens_params_m["k5"] = pstResult->distortion_cofficeints[4];
	lens_params_m["calib_res"] = calib_res_m;
	master["lens_params"] = lens_params_m;

	root["master"] = master;

	Json::StyledWriter writer;
	std::string outputConfig = writer.write(root);

	FILE* fp = nullptr;

	USES_CONVERSION;
	fopen_s(&fp, CT2A(szPath), "wb");

	if (fp == nullptr)
	{
		return false;
	}

	size_t fileSize = fwrite(outputConfig.c_str(), 1, outputConfig.length(), fp);

	fclose(fp);

#endif
	return TRUE;
}

//=============================================================================
// Method		: SaveJSON_StereoCAL_Stereo
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const STEREO_CALIBRATION_RESULT * pstResult
// Qualifier	:
// Last Update	: 2018/2/27 - 15:41
// Desc.		:
//=============================================================================
BOOL CFile_Report::SaveJSON_StereoCAL_Stereo(__in LPCTSTR szPath, __in const STEREO_CALIBRATION_RESULT* pstResult)
{
#if (SET_INSPECTOR == SYS_STEREO_CAL)
	Json::Value root(Json::objectValue);
	Json::Value master;
	Json::Value slave;
	Json::Value lens_params_m;
	Json::Value camera_pose;
	Json::Value lens_params_s;

	Json::Value focal_len_m;
	Json::Value principal_point_m;
	Json::Value calib_res_m;

	Json::Value trans;
	Json::Value rot;
	Json::Value focal_len_s;
	Json::Value principal_point_s;
	Json::Value calib_res_s;

	focal_len_m.append(pstResult->master_fx);	// "focal_len": [%f, %f],\n' % (fx_left[0]*-1, fy_left[0]*-1))
	focal_len_m.append(pstResult->master_fy);
	principal_point_m.append(pstResult->master_cx);	// "principal_point": [%f, %f],\n' % (cx_left[0], cy_left[0]-2))
	principal_point_m.append(pstResult->master_cy);
	calib_res_m.append(1280);//pstResult->master_calib_res_width);
	calib_res_m.append(964);//pstResult->master_calib_res_height);
	
	root["type"] = "Stereo Calibration";
	root["version"] = 1.0;

	master["serial"] = 0;
	lens_params_m["focal_len"] = focal_len_m;
	lens_params_m["principal_point"] = principal_point_m;
	lens_params_m["skew"] = 0.0;
	lens_params_m["k1"] = pstResult->master_distortion_cofficeints[0];
	lens_params_m["k2"] = pstResult->master_distortion_cofficeints[1];
	lens_params_m["k3"] = pstResult->master_distortion_cofficeints[2];
	lens_params_m["k4"] = pstResult->master_distortion_cofficeints[3];
	lens_params_m["k5"] = pstResult->master_distortion_cofficeints[4];
	lens_params_m["calib_res"] = calib_res_m;
	master["lens_params"] = lens_params_m;
	
	// "trans": [%0.5f, %0.5f, %0.5f],\n' % (pos[0]*0.001, pos[1]*0.001, pos[2]*0.001))#(Trans[0][0], Trans[0][1], Trans[0][2]))
	trans.append(pstResult->translation_vector[0]);
	trans.append(pstResult->translation_vector[1]);
	trans.append(pstResult->translation_vector[2]);
	rot.append(pstResult->pitch);	//rot.append(pstResult->rotation_vector[2]);
	rot.append(pstResult->yaw);		//rot.append(pstResult->rotation_vector[0]);
	rot.append(pstResult->roll);	//rot.append(pstResult->rotation_vector[1]);
	focal_len_s.append(pstResult->slave_fx);	// "focal_len": [%f, %f],\n' % (fx_left[0]*-1, fy_left[0]*-1))
	focal_len_s.append(pstResult->slave_fy);
	principal_point_s.append(pstResult->slave_cx);	// "principal_point": [%f, %f],\n' % (cx_left[0], cy_left[0]-2))
	principal_point_s.append(pstResult->slave_cy);
	calib_res_s.append(1280);//pstResult->slave_calib_res_width);
	calib_res_s.append(964);//pstResult->slave_calib_res_height);

	slave["serial"] = 0;
	camera_pose["trans"] = trans;
	camera_pose["rot"] = rot;
	slave["camera_pose"] = camera_pose;
	lens_params_s["focal_len"] = focal_len_s;
	lens_params_s["principal_point"] = principal_point_s;
	lens_params_s["skew"] = 0.0;
	lens_params_s["k1"] = pstResult->slave_distortion_cofficeints[0];
	lens_params_s["k2"] = pstResult->slave_distortion_cofficeints[1];
	lens_params_s["k3"] = pstResult->slave_distortion_cofficeints[2];
	lens_params_s["k4"] = pstResult->slave_distortion_cofficeints[3];
	lens_params_s["k5"] = pstResult->slave_distortion_cofficeints[4];
	lens_params_s["calib_res"] = calib_res_s;
	slave["lens_params_s"] = lens_params_s;

	root["master"] = master;
	root["slave"] = slave;
	
	Json::StyledWriter writer;	
	std::string outputConfig = writer.write(root);	

	FILE* fp = nullptr;

	USES_CONVERSION;
	fopen_s(&fp, CT2A(szPath), "wb");

	if (fp == nullptr)
	{
		return false;
	}

	size_t fileSize = fwrite(outputConfig.c_str(), 1, outputConfig.length(), fp);

	fclose(fp);

#endif
	return TRUE;
}

//=============================================================================
// Method		: SaveJSON_StereoCAL_Mono_Raw
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const SINGLE_CALIBRATION_RESULT * pstResult
// Qualifier	:
// Last Update	: 2018/3/6 - 17:40
// Desc.		:
//=============================================================================
BOOL CFile_Report::SaveJSON_StereoCAL_Mono_Raw(__in LPCTSTR szPath, __in const SINGLE_CALIBRATION_RESULT* pstResult)
{
	// ** 데이터 보정
	//(fx_left[0] * -1, fy_left[0] * -1))
	//(cx_left[0], cy_left[0] - 2))

	CStringA szJSON;
	CStringA szTemp;

	szJSON = "{\r\n";
	szJSON += "	\"type\": \"Mono Calibration\",\r\n";
	szJSON += "	\"version\" : 1.0,\r\n";
	szJSON += "	\"master\" : {\r\n";
	szJSON += "		\"serial\": 0,\r\n";
	szJSON += "		\"lens_params\" : {\r\n";
	szTemp.Format("			\"focal_len\":[%.6f, %.6f],\r\n", pstResult->fx, pstResult->fy);
	szJSON += szTemp;
	szTemp.Format("			\"principal_point\" : [%.6f, %.6f],\r\n", pstResult->cx, pstResult->cy);
	szJSON += szTemp;
	szJSON += "			\"skew\" : 0.0,\r\n";
	szTemp.Format("			\"k1\" : %.5f,\r\n", pstResult->distortion_cofficeints[0]);
	szJSON += szTemp;
	szTemp.Format("			\"k2\" : %.5f,\r\n", pstResult->distortion_cofficeints[1]);
	szJSON += szTemp;
	szTemp.Format("			\"k3\" : %.5f,\r\n", pstResult->distortion_cofficeints[2]);
	szJSON += szTemp;
	szTemp.Format("			\"k4\" : %.5f,\r\n", pstResult->distortion_cofficeints[3]);
	szJSON += szTemp;
	szTemp.Format("			\"k5\" : %.5f,\r\n", pstResult->distortion_cofficeints[4]);
	szJSON += szTemp;
	szTemp.Format("			\"calib_res\" : [%d, %d]\r\n", 1280, 724);//pstResult->calib_res_width, pstResult->calib_res_height);
	szJSON += szTemp;
	szJSON += "		}\r\n";
	szJSON += "	}\r\n";
	szJSON += "}\r\n";

	FILE* fp = nullptr;

	USES_CONVERSION;
	fopen_s(&fp, CT2A(szPath), "wb");

	if (fp == nullptr)
	{
		return false;
	}

	size_t fileSize = fwrite(szJSON.GetBuffer(0), 1, szJSON.GetLength(), fp);

	fclose(fp);

	return TRUE;
}

//=============================================================================
// Method		: SaveJSON_StereoCAL_Stereo_Raw
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const STEREO_CALIBRATION_RESULT * pstResult
// Qualifier	:
// Last Update	: 2018/3/8 - 14:28
// Desc.		:
//=============================================================================
BOOL CFile_Report::SaveJSON_StereoCAL_Stereo_Raw(__in LPCTSTR szPath, __in const STEREO_CALIBRATION_RESULT* pstResult)
{
	// ** 데이터 보정
	//(fx_left[0] * -1, fy_left[0] * -1))
	//(cx_left[0], cy_left[0] - 2))
	//(pos[0] * 0.001, pos[1] * 0.001, pos[2] * 0.001))#(Trans[0][0], Trans[0][1], Trans[0][2]))


	CStringA szJSON;
	CStringA szTemp;
	
	szJSON = "{\r\n";
	szJSON +="	\"type\": \"Stereo Calibration\",\r\n";
	szJSON +="	\"version\" : 1.0,\r\n";
	szJSON +="	\"master\" : {\r\n";
	szJSON +="		\"serial\": 0,\r\n";
	szJSON +="		\"lens_params\" : {\r\n";
	szTemp.Format("			\"focal_len\":[%.6f, %.6f],\r\n", pstResult->master_fx, pstResult->master_fy);
	szJSON += szTemp;
	szTemp.Format("			\"principal_point\" : [%.6f, %.6f],\r\n", pstResult->master_cx, pstResult->master_cy);
	szJSON += szTemp;
	szJSON +="			\"skew\" : 0.0,\r\n";
	szTemp.Format("			\"k1\" : %.5f,\r\n", pstResult->master_distortion_cofficeints[0]);
	szJSON += szTemp;
	szTemp.Format("			\"k2\" : %.5f,\r\n", pstResult->master_distortion_cofficeints[1]);
	szJSON += szTemp;
	szTemp.Format("			\"k3\" : %.5f,\r\n", pstResult->master_distortion_cofficeints[2]);
	szJSON += szTemp;
	szTemp.Format("			\"k4\" : %.5f,\r\n", pstResult->master_distortion_cofficeints[3]);
	szJSON += szTemp;
	szTemp.Format("			\"k5\" : %.5f,\r\n", pstResult->master_distortion_cofficeints[4]);
	szJSON += szTemp;
	szTemp.Format("			\"calib_res\" : [%d, %d]\r\n", 1280, 964);//pstResult->master_calib_res_width, pstResult->master_calib_res_height);
	szJSON += szTemp;
	szJSON +="		}\r\n";
	szJSON +="	},\r\n";
	szJSON +="	\"slave\": {\r\n";
	szJSON +="		\"serial\": 0,\r\n";
	szJSON +="		\"camera_pose\" : {\r\n";
	szTemp.Format("			\"trans\":[%.5f, %.5f, %.5f],\r\n", pstResult->translation_vector[0], pstResult->translation_vector[1], pstResult->translation_vector[2]); //pstResult->X, pstResult->Y, pstResult->Z); //
	szJSON += szTemp;
	//szTemp.Format("			\"rot\" : [%.5f, %.5f, %.5f]\r\n", pstResult->pitch, pstResult->yaw, pstResult->roll);//pstResult->rotation_vector[2], pstResult->rotation_vector[0], pstResult->rotation_vector[1]);
	szTemp.Format("			\"rot\" : [%.5f, %.5f, %.5f]\r\n", pstResult->pitch, pstResult->yaw, pstResult->roll);//pstResult->rotation_vector[2], pstResult->rotation_vector[0], pstResult->rotation_vector[1]);
	szJSON += szTemp;
	szJSON +="		},\r\n";
	szJSON +="		\"lens_params\" : {\r\n";
	szTemp.Format("			\"focal_len\":[%.6f, %.6f],\r\n", pstResult->slave_fx, pstResult->slave_fy);
	szJSON += szTemp;
	szTemp.Format("			\"principal_point\" : [%.6f, %.6f],\r\n", pstResult->slave_cx, pstResult->slave_cy);
	szJSON += szTemp;
	szJSON +="			\"skew\" : 0.0,\r\n";
	szTemp.Format("			\"k1\" : %.5f,\r\n", pstResult->slave_distortion_cofficeints[0]);
	szJSON += szTemp;
	szTemp.Format("			\"k2\" : %.5f,\r\n", pstResult->slave_distortion_cofficeints[1]);
	szJSON += szTemp;
	szTemp.Format("			\"k3\" : %.5f,\r\n", pstResult->slave_distortion_cofficeints[2]);
	szJSON += szTemp;
	szTemp.Format("			\"k4\" : %.5f,\r\n", pstResult->slave_distortion_cofficeints[3]);
	szJSON += szTemp;
	szTemp.Format("			\"k5\" : %.5f,\r\n", pstResult->slave_distortion_cofficeints[4]);
	szJSON += szTemp;
	szTemp.Format("			\"calib_res\" : [%d, %d]\r\n", 1280, 964);//pstResult->slave_calib_res_width, pstResult->slave_calib_res_height);
	szJSON += szTemp;
	szJSON +="		}\r\n";
	szJSON +="	}\r\n";
	szJSON +="}\r\n";


	FILE* fp = nullptr;

	USES_CONVERSION;
	fopen_s(&fp, CT2A(szPath), "wb");

	if (fp == nullptr)
	{
		return false;
	}

	size_t fileSize = fwrite(szJSON.GetBuffer(0), 1, szJSON.GetLength(), fp);

	fclose(fp);

	return TRUE;
}

//=============================================================================
// Method		: Save_SteCal_Mono_EEPROM
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in LPCTSTR szBarcode
// Parameter	: __in const SYSTEMTIME * stTime
// Parameter	: __in const SINGLE_CALIBRATION_RESULT * pstResult
// Qualifier	:
// Last Update	: 2018/3/11 - 13:13
// Desc.		:
//=============================================================================
BOOL CFile_Report::Save_SteCal_Mono_EEPROM(__in LPCTSTR szPath, __in LPCTSTR szBarcode, __in const SYSTEMTIME* pstTime,  __in const SINGLE_CALIBRATION_RESULT* pstResult)
{
	CString			szFileName;
	CString			szFullPath;
	SYSTEMTIME		tmLocal;
	GetLocalTime(&tmLocal);

	szFileName.Format(_T("Mono_EEPROM_%s_%04d%02d%02d_%02d%02d%02d.csv"), szBarcode, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay,
		tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);


	szFullPath.Format(_T("%s\\EEPROM\\%04d_%02d_%02d\\"), szPath, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay);
	MakeDirectory(szFullPath);

	szFullPath += szFileName;

	CStringArray arrHeaderz;
	CStringArray arrDataz;

	Make_HeadAndData_SteCal_Mono_EEPROM(szBarcode, pstTime, pstResult, &arrHeaderz, &arrDataz);

	SaveFinalizeResult(szFullPath, &arrHeaderz, &arrDataz);

	return TRUE;
}

//=============================================================================
// Method		: Save_SteCal_Mono_EEPROM_Bin
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in LPCTSTR szBarcode
// Parameter	: __in const SYSTEMTIME * pstTime
// Parameter	: __in const LPBYTE parbyEEPROM
// Parameter	: __in UINT nDataLen
// Qualifier	:
// Last Update	: 2018/3/11 - 13:53
// Desc.		:
//=============================================================================
BOOL CFile_Report::Save_SteCal_Mono_EEPROM_Bin(__in LPCTSTR szPath, __in LPCTSTR szBarcode, __in const SYSTEMTIME* pstTime, __in const LPBYTE parbyEEPROM, __in UINT nDataLen)
{
	CString			szFileName;
	CString			szFullPath;
	SYSTEMTIME		tmLocal;
	GetLocalTime(&tmLocal);

	szFileName.Format(_T("Mono_EEPROM_%s_%04d%02d%02d_%02d%02d%02d.bin"), szBarcode, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay,
		tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);


	szFullPath.Format(_T("%s\\EEPROM\\%04d_%02d_%02d\\"), szPath, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay);
	MakeDirectory(szFullPath);

	szFullPath += szFileName;


	CFile	fileBin;
	CFileException ex;

	try
	{
		if (!fileBin.Open(szFullPath, CFile::modeWrite | CFile::shareExclusive | CFile::modeCreate, &ex))
		{
			return FALSE;
		}

		fileBin.Write(parbyEEPROM, nDataLen);
		fileBin.Flush();
	}
	catch (CFileException* pEx)
	{
		pEx->ReportError();
		pEx->Delete();
	}
	catch (CMemoryException* pEx)
	{
		pEx->ReportError();
		pEx->Delete();
		AfxAbort();
	}

	fileBin.Close();


	return TRUE;
}

//=============================================================================
// Method		: Save_SteCal_Stereo_EEPROM
// Access		: public  
// Returns		: BOOL
// Parameter	: __in BOOL bMaster
// Parameter	: __in BOOL bMainPage
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in LPCTSTR szBarcode
// Parameter	: __in const SYSTEMTIME * pstTime
// Parameter	: __in const STEREO_CALIBRATION_RESULT * pstResult
// Qualifier	:
// Last Update	: 2018/5/31 - 15:36
// Desc.		:
//=============================================================================
BOOL CFile_Report::Save_SteCal_Stereo_EEPROM(__in BOOL bMaster, __in BOOL bMainPage, __in LPCTSTR szPath, __in LPCTSTR szBarcode, __in const SYSTEMTIME* pstTime, __in const STEREO_CALIBRATION_RESULT* pstResult)
{
	CString			szFileName;
	CString			szFullPath;
// 	SYSTEMTIME		tmLocal;
// 	GetLocalTime(&tmLocal);

	szFileName.Format(_T("Stereo_EEPROM_%s_%04d%02d%02d_%02d%02d%02d.csv"), szBarcode, pstTime->wYear, pstTime->wMonth, pstTime->wDay,
																			pstTime->wHour, pstTime->wMinute, pstTime->wSecond);

	szFullPath.Format(_T("%s\\EEPROM\\%04d_%02d_%02d\\"), szPath, pstTime->wYear, pstTime->wMonth, pstTime->wDay);
	MakeDirectory(szFullPath);

	szFullPath += szFileName;

	CStringArray arrHeaderz;
	CStringArray arrDataz;

	Make_HeadAndData_SteCal_Ste_EEPROM(bMaster, bMainPage, szBarcode, pstTime, pstResult, &arrHeaderz, &arrDataz);

	SaveFinalizeResult(szFullPath, &arrHeaderz, &arrDataz);

	return TRUE;
}

//=============================================================================
// Method		: Save_SteCal_Stereo_EEPROM_Bin
// Access		: public  
// Returns		: BOOL
// Parameter	: __in BOOL bMaster
// Parameter	: __in BOOL bMainPage
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in LPCTSTR szBarcode
// Parameter	: __in const SYSTEMTIME * pstTime
// Parameter	: __in const LPBYTE parbyEEPROM
// Parameter	: __in UINT nDataLen
// Qualifier	:
// Last Update	: 2018/5/31 - 15:36
// Desc.		:
//=============================================================================
BOOL CFile_Report::Save_SteCal_Stereo_EEPROM_Bin(__in BOOL bMaster, __in BOOL bMainPage, __in LPCTSTR szPath, __in LPCTSTR szBarcode, __in const SYSTEMTIME* pstTime, __in const LPBYTE parbyEEPROM, __in UINT nDataLen)
{
	CString			szFileName;
	CString			szFullPath;
	SYSTEMTIME		tmLocal;
	GetLocalTime(&tmLocal);

	if (bMaster)
	{
		if (bMainPage)
			szFileName.Format(_T("Stereo_EEPROM_Master_%s_%04d%02d%02d_%02d%02d%02d.bin"), szBarcode, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay, tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
		else
			szFileName.Format(_T("Stereo_EEPROM_Master_Mirror_%s_%04d%02d%02d_%02d%02d%02d.bin"), szBarcode, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay, tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
	}
	else
	{
		if (bMainPage)
			szFileName.Format(_T("Stereo_EEPROM_Slave_%s_%04d%02d%02d_%02d%02d%02d.bin"), szBarcode, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay, tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
		else
			szFileName.Format(_T("Stereo_EEPROM_Slave_Mirror_%s_%04d%02d%02d_%02d%02d%02d.bin"), szBarcode, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay, tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
	}

	szFullPath.Format(_T("%s\\EEPROM\\%04d_%02d_%02d\\"), szPath, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay);
	MakeDirectory(szFullPath);

	szFullPath += szFileName;


	CFile	fileBin;
	CFileException ex;

	try
	{
		if (!fileBin.Open(szFullPath, CFile::modeWrite | CFile::shareExclusive | CFile::modeCreate, &ex))
		{
			return FALSE;
		}

		fileBin.Write(parbyEEPROM, nDataLen);
		fileBin.Flush();
	}
	catch (CFileException* pEx)
	{
		pEx->ReportError();
		pEx->Delete();
	}
	catch (CMemoryException* pEx)
	{
		pEx->ReportError();
		pEx->Delete();
		AfxAbort();
	}

	fileBin.Close();

	return TRUE;
}
