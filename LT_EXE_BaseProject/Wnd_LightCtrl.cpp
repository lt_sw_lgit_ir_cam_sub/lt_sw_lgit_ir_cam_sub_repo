﻿//*****************************************************************************
// Filename	: 	Wnd_LightCtrl.cpp
// Created	:	2016/10/31 - 21:02
// Modified	:	2016/10/31 - 21:02
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************

#include "stdafx.h"
#include "Wnd_LightCtrl.h"
#include "Reg_InspInfo.h"

typedef enum Light_ID
{
	IDC_BN_VOLT_APPLY	= 1000,
	IDC_BN_STEP_APPLY,
	IDC_ED_VOLT,
	IDC_ED_STEP,
	IDC_SC_STEP,
	IDC_BN_LIGHT_ON,
	IDC_BN_LIGHT_OFF,
};

IMPLEMENT_DYNAMIC(CWnd_LightCtrl, CWnd)

CWnd_LightCtrl::CWnd_LightCtrl()
{
	VERIFY(m_font.CreateFont(
		18,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_LightCtrl::~CWnd_LightCtrl()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_LightCtrl, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_HSCROLL()
	ON_BN_CLICKED(IDC_BN_VOLT_APPLY,	OnBnClickedVoltApply)
	ON_BN_CLICKED(IDC_BN_STEP_APPLY,	OnBnClickedStepApply)
	ON_BN_CLICKED(IDC_BN_LIGHT_ON,		OnBnClickedBnLightOn)
	ON_BN_CLICKED(IDC_BN_LIGHT_OFF,		OnBnClickedBnLightOff)
END_MESSAGE_MAP()

// CWnd_LightCtrl message handlers
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
int CWnd_LightCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_st_Title.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Title.SetBackColor_COLORREF(RGB(33, 73, 125));
	m_st_Title.SetTextColor(Gdiplus::Color::White, Gdiplus::Color::White);
	m_st_Title.SetFont_Gdip(L"Arial", 10.0F);
	m_st_Title.Create(m_szTitle.GetBuffer(0), dwStyle | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

	m_st_Volt.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Volt.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Volt.SetFont_Gdip(L"Arial", 9.0F);
	m_st_Volt.Create(_T("VOLTAGE (V)"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_ed_Volt.Create(dwStyle | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_VOLT);
	m_ed_Volt.SetFont(&m_font);
	m_ed_Volt.LimitText(5);
	m_ed_Volt.SetValidChars(_T("0123456789."));
	m_ed_Volt.SetWindowText(_T("12.00"));

	m_bn_Volt_Apply.Create(_T("APPLY"), dwStyle | BS_PUSHLIKE, rectDummy, this, IDC_BN_VOLT_APPLY);
	m_bn_Volt_Apply.SetFont(&m_font);

	m_sc_Volt.Create(WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS | WS_BORDER | TBS_BOTH | TBS_NOTICKS, rectDummy, this, IDC_SC_STEP);
	m_sc_Volt.SetRange((int)(m_stLightConfig.fVoltMin * 10.0f), (int)(m_stLightConfig.fVoltMax * 10.0f));
	m_sc_Volt.SetLineSize(1);
	m_sc_Volt.SetTicFreq(1);
	m_sc_Volt.SetPageSize(1);
	m_sc_Volt.SetPos((int)(m_stLightConfig.fVoltMax * 10.0f));
	
	m_st_Step.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Step.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Step.SetFont_Gdip(L"Arial", 9.0F);
	m_st_Step.Create(_T("CURRENT (mA)"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_ed_Step.Create(dwStyle | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_STEP);
	m_ed_Step.SetFont(&m_font);
	m_ed_Step.LimitText(4);
	m_ed_Step.SetValidChars(_T("0123456789"));
	m_ed_Step.SetWindowText(_T("3000"));

	m_bn_Step_Apply.Create(_T("APPLY"), dwStyle | BS_PUSHLIKE, rectDummy, this, IDC_BN_STEP_APPLY);
	m_bn_Step_Apply.SetFont(&m_font);

	m_sc_Step.Create(WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS | WS_BORDER | TBS_BOTH | TBS_NOTICKS, rectDummy, this, IDC_SC_STEP);
	m_sc_Step.SetRange(m_stLightConfig.wStepMin, m_stLightConfig.wStepMax);
	m_sc_Step.SetLineSize(1);
	m_sc_Step.SetTicFreq(1);
	m_sc_Step.SetPageSize(1);
	m_sc_Step.SetPos(m_stLightConfig.wStepMax);

	m_bn_LightOn.Create(_T("Light On"), dwStyle | BS_PUSHLIKE, rectDummy, this, IDC_BN_LIGHT_ON);
	m_bn_LightOn.SetFont(&m_font);
	m_bn_LightOff.Create(_T("Light Off"), dwStyle | BS_PUSHLIKE, rectDummy, this, IDC_BN_LIGHT_OFF);
	m_bn_LightOff.SetFont(&m_font);

	m_st_Frame.Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE | SS_BLACKFRAME, rectDummy, this, IDC_STATIC);
	
	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
void CWnd_LightCtrl::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iSpacing	= 5;
	int iMargin		= 5;
	int iLeft		= 0;
	int iTop		= 0;
	int iWidth		= cx;
	int iHeight		= cy;

	int iCtrlWidth	= (iWidth - iMargin - iMargin - (iSpacing * 3)) / 4;
	int iCtrlHeight = (iHeight - iMargin - (iSpacing * 4)) / 5;
	int iTitleHeight= iCtrlHeight + iCtrlHeight + iSpacing;
	int iScWidth	= iCtrlWidth + iSpacing + iCtrlWidth;

	iTop = 0;
	m_st_Title.MoveWindow(iLeft, iTop, iWidth, iCtrlHeight);

	// Volt
	iLeft = iMargin;
	iTop += iCtrlHeight + iSpacing;
	m_st_Volt.MoveWindow(iLeft, iTop, iCtrlWidth, iTitleHeight);

	iLeft += iCtrlWidth + iSpacing;
	m_ed_Volt.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);

	iLeft += iCtrlWidth + iSpacing;
	m_bn_Volt_Apply.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);

	iTop += iCtrlHeight + iSpacing;
	iLeft = iMargin + iCtrlWidth + iSpacing;
	m_sc_Volt.MoveWindow(iLeft, iTop, iScWidth, iCtrlHeight);

	switch (m_nLightBoardType)
	{
	case LBT_Luritech_01:
		// Step
		iTop += iCtrlHeight + iSpacing;
		iLeft = iMargin;
		m_st_Step.MoveWindow(iLeft, iTop, iCtrlWidth, iTitleHeight);

		iLeft += iCtrlWidth + iSpacing;
		m_ed_Step.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);

		iLeft += iCtrlWidth + iSpacing;
		m_bn_Step_Apply.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);

		iTop += iCtrlHeight + iSpacing;
		iLeft = iMargin + iCtrlWidth + iSpacing;
		m_sc_Step.MoveWindow(iLeft, iTop, iScWidth, iCtrlHeight);
		break;
	}

	// 광원 On/off
	//iLeft = iMargin + (iCtrlWidth * 3) + (iSpacing * 3);
	iLeft = cx - iMargin - iCtrlWidth;
	iTop = iCtrlHeight + iSpacing;
	m_bn_LightOn.MoveWindow(iLeft, iTop, iCtrlWidth, iTitleHeight);
	iTop += iTitleHeight + iSpacing;
	m_bn_LightOff.MoveWindow(iLeft, iTop, iCtrlWidth, iTitleHeight);

	// Border
	m_st_Frame.MoveWindow(0, 0, iWidth, iHeight);
}

//=============================================================================
// Method		: OnHScroll
// Access		: protected  
// Returns		: void
// Parameter	: UINT nSBCode
// Parameter	: UINT nPos
// Parameter	: CScrollBar * pScrollBar
// Qualifier	:
// Last Update	: 2017/6/29 - 15:27
// Desc.		:
//=============================================================================
void CWnd_LightCtrl::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	BOOL bScrollbar = FALSE;
	UINT nSBIndex = 0;
	CString strValue;

	int iPos = 0;

	if (pScrollBar)
	{
		if (pScrollBar == (CScrollBar*)&m_sc_Step)
		{
			bScrollbar = TRUE;
			nSBIndex = 1;
		}
		else if (pScrollBar == (CScrollBar*)&m_sc_Volt)
		{
			bScrollbar = TRUE;
			nSBIndex = 0;
		}

		if (bScrollbar)
		{
			switch (nSBCode)
			{
			case TB_THUMBPOSITION:
			case TB_THUMBTRACK:
			case TB_ENDTRACK:
			{
				if (1 == nSBIndex)
				{
					iPos = m_sc_Step.GetPos();

					strValue.Format(_T("%d"), iPos);
					m_ed_Step.SetWindowText(strValue);
				}
				else if (0 == nSBIndex)
				{
					iPos = m_sc_Volt.GetPos();

					strValue.Format(_T("%.2f"), (float)iPos / 10.0f);
					m_ed_Volt.SetWindowText(strValue);
				}
			}
				break;

			case TB_LINEUP:
			case TB_LINEDOWN:
			case TB_BOTTOM:
			case TB_PAGEDOWN:
			case TB_PAGEUP:
			case TB_TOP:
			default:
				break;
			}
		}
	}

	CWnd::OnHScroll(nSBCode, nPos, pScrollBar);
}

//=============================================================================
// Method		: OnRangeBtnVolt
// Access		: protected  
// Returns		: void
// Parameter	: void
// Qualifier	:
// Last Update	: 2017/6/29 - 14:51
// Desc.		:
//=============================================================================
void CWnd_LightCtrl::OnBnClickedVoltApply()
{
	CString strValue;
	float fVolt = 0;

	m_ed_Volt.GetWindowText(strValue);

	fVolt = (float)_ttof(strValue.GetBuffer(0));
	
	if (fVolt > m_stLightConfig.fVoltMax)
	{
		strValue.Format(_T("%.2f"), m_stLightConfig.fVoltMax);
		fVolt	 = m_stLightConfig.fVoltMax;
	}

	m_ed_Volt.SetWindowText(strValue);
	m_sc_Volt.SetPos((int)(fVolt * 10.0f));

	if (m_pLightDevice)
	{
		LRESULT lCommResult = SRC_OK;

		switch (m_nLightBoardType)
		{
		case LBT_ODA_PT:
			lCommResult = ((CODA_PT*)m_pLightDevice)->Send_Voltage(fVolt);
			break;

		case LBT_Luritech_01:
			lCommResult = ((CLGE_LightBrd*)m_pLightDevice)->Send_AmbientVoltOn_All(fVolt);
			break;

		default:
			lCommResult = SRC_Err_Unknown;
			break;
		}

		if (SRC_OK != lCommResult)
		{
			TRACE(_T("Light Board Error : Code -> %d\n"), lCommResult);
			AfxMessageBox(_T("Light Board Error\n"));
		}
	}
	//GetOwner()->SendNotifyMessage(WM_LIGHT_VOLT_CNTROL, (WPARAM)nIndex, (LPARAM)fVolt);
}

//=============================================================================
// Method		: OnRangeBtnStep
// Access		: protected  
// Returns		: void
// Parameter	: void
// Qualifier	:
// Last Update	: 2017/6/29 - 14:51
// Desc.		:
//=============================================================================
void CWnd_LightCtrl::OnBnClickedStepApply()
{
	CString strValue;
	WORD wStep = 0;

	m_ed_Step.GetWindowText(strValue);

	if (wStep > m_stLightConfig.wStepMax)
	{
		strValue.Format(_T("%d"), m_stLightConfig.wStepMax);
		wStep = m_stLightConfig.wStepMax;
	}
	else
	{
		wStep = (WORD)_ttoi(strValue.GetBuffer(0));
	}

	m_ed_Step.SetWindowText(strValue);
	m_sc_Step.SetPos((int)wStep);

	//GetOwner()->SendNotifyMessage(WM_LIGHT_STEP_CNTROL, (WPARAM)nIndex, nCode);
	if (m_pLightDevice)
	{
		LRESULT lCommResult = SRC_OK;

		switch (m_nLightBoardType)
		{
		case LBT_ODA_PT:
			//lCommResult = ((CODA_PT*)m_pLightDevice)->Send_Apply(enSwitchOnOff::Switch_OFF);
			break;

		case LBT_Luritech_01:
			lCommResult = ((CLGE_LightBrd*)m_pLightDevice)->Send_EtcSetCurrent(Slot_All, wStep);
			break;

		default:
			lCommResult = SRC_Err_Unknown;
			break;
		}
		//lCommResult = m_pstDevice->PCBLightBrd.Send_AmbientLightCurrent(wStep);

		if (SRC_OK != lCommResult)
		{
			TRACE(_T("Light Board Error : Code -> %d\n"), lCommResult);
			AfxMessageBox(_T("Light Board Error\n"));
		}
	}
}

//=============================================================================
// Method		: OnBnClickedBnLightOn
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/1/13 - 12:29
// Desc.		:
//=============================================================================
void CWnd_LightCtrl::OnBnClickedBnLightOn()
{
	LRESULT lCommResult = SRC_OK;

	switch (m_nLightBoardType)
	{
	case LBT_ODA_PT:
		lCommResult = ((CODA_PT*)m_pLightDevice)->Send_Output(enSwitchOnOff::Switch_ON);
		break;

	case LBT_Luritech_01:
		OnBnClickedVoltApply();
		OnBnClickedStepApply();
		break;

	default:
		break;
	}

	if (SRC_OK != lCommResult)
	{
		TRACE(_T("Light Board Light Off Error : Code -> %d\n"), lCommResult);
		AfxMessageBox(_T("Light Board Light Off Error\n"));
	}
}

//=============================================================================
// Method		: OnBnClickedBnLightOff
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/1/13 - 12:29
// Desc.		:
//=============================================================================
void CWnd_LightCtrl::OnBnClickedBnLightOff()
{
	if (m_pLightDevice)
	{
		LRESULT lCommResult = SRC_OK;

		switch (m_nLightBoardType)
		{
		case LBT_ODA_PT:
			lCommResult = ((CODA_PT*)m_pLightDevice)->Send_Output(enSwitchOnOff::Switch_OFF);
			break;

		case LBT_Luritech_01:
			lCommResult = ((CLGE_LightBrd*)m_pLightDevice)->Send_AmbientVoltOff_All();
			break;

		default:
			lCommResult = SRC_Err_Unknown;
			break;
		}

		if (SRC_OK != lCommResult)
		{
			TRACE(_T("Light Board Light Off Error : Code -> %d\n"), lCommResult);
			AfxMessageBox(_T("Light Board Light Off Error\n"));
		}
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
BOOL CWnd_LightCtrl::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
BOOL CWnd_LightCtrl::PreTranslateMessage(MSG* pMsg)
{
	return CWnd::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: SetPtr_Device
// Access		: public  
// Returns		: void
// Parameter	: __in enLightBrdType nCtrlType
// Parameter	: __in PVOID pLightDevice
// Qualifier	:
// Last Update	: 2018/2/5 - 20:31
// Desc.		:
//=============================================================================
void CWnd_LightCtrl::SetPtr_Device(__in enLightBrdType nCtrlType, __in PVOID pLightDevice)
{
	m_nLightBoardType = nCtrlType;
	m_pLightDevice = pLightDevice;
}

//=============================================================================
// Method		: SetLightInfo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_LightCfg * pstLightInfo
// Qualifier	:
// Last Update	: 2018/1/5 - 11:04
// Desc.		:
//=============================================================================
void CWnd_LightCtrl::SetLightInfo(__in const ST_LightCfg* pstLightInfo)
{
	m_stLightConfig = *pstLightInfo;

	CString strValue;

	strValue.Format(_T("%.2f"), pstLightInfo->fVolt);
	m_ed_Volt.SetWindowText(strValue);
	m_sc_Volt.SetPos((int)(pstLightInfo->fVolt * 10.0f));
	 
	strValue.Format(_T("%d"), pstLightInfo->wStep);
	m_ed_Step.SetWindowText(strValue);
	m_sc_Step.SetPos((int)pstLightInfo->wStep);
}

//=============================================================================
// Method		: GetLightInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_LightCfg & stLightInfo
// Qualifier	:
// Last Update	: 2018/1/5 - 11:04
// Desc.		:
//=============================================================================
void CWnd_LightCtrl::GetLightInfo(__out ST_LightCfg& stLightInfo)
{
	CString strValue;

	m_ed_Volt.GetWindowText(strValue);
	stLightInfo.fVolt = (float)_ttof(strValue);
	m_stLightConfig.fVolt = stLightInfo.fVolt;
	 
	m_ed_Step.GetWindowText(strValue);
	stLightInfo.wStep = _ttoi(strValue);
	m_stLightConfig.wStep = stLightInfo.wStep;
	stLightInfo.fCurrent = (float)stLightInfo.wStep;
	m_stLightConfig.fCurrent = stLightInfo.fCurrent;
}

//=============================================================================
// Method		: Set_VoltageMinMax
// Access		: virtual public  
// Returns		: void
// Parameter	: __in float fVoltMin
// Parameter	: __in float fVoltMax
// Qualifier	:
// Last Update	: 2018/2/6 - 11:06
// Desc.		:
//=============================================================================
void CWnd_LightCtrl::Set_VoltageMinMax(__in float fVoltMin, __in float fVoltMax)
{
	m_stLightConfig.fVoltMin	= fVoltMin;
	m_stLightConfig.fVoltMax	= fVoltMax;
}

//=============================================================================
// Method		: Set_CurrentMinMax
// Access		: virtual public  
// Returns		: void
// Parameter	: __in float fCurrMin
// Parameter	: __in float fCurrMax
// Qualifier	:
// Last Update	: 2018/2/6 - 11:15
// Desc.		:
//=============================================================================
void CWnd_LightCtrl::Set_CurrentMinMax(__in float fCurrMin, __in float fCurrMax)
{
	m_stLightConfig.fCurrMin = fCurrMin;
	m_stLightConfig.fCurrMax = fCurrMax;
}

//=============================================================================
// Method		: Set_StepMinMax
// Access		: virtual public  
// Returns		: void
// Parameter	: __in WORD wStepMin
// Parameter	: __in WORD wStepMax
// Qualifier	:
// Last Update	: 2018/2/6 - 11:15
// Desc.		:
//=============================================================================
void CWnd_LightCtrl::Set_StepMinMax(__in WORD wStepMin, __in WORD wStepMax)
{
	m_stLightConfig.wStepMin = wStepMin;
	m_stLightConfig.wStepMax = wStepMax;
}

//=============================================================================
// Method		: SetTitle
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szText
// Qualifier	:
// Last Update	: 2018/2/6 - 11:16
// Desc.		:
//=============================================================================
void CWnd_LightCtrl::SetTitle(__in LPCTSTR szText)
{
	m_szTitle = szText;

	m_st_Title.SetText(szText);
}

