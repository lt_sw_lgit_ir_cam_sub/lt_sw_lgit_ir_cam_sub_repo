﻿//*****************************************************************************
// Filename	: Wnd_RecipeView.h
// Created	: 2015/12/9 - 0:05
// Modified	: 2015/12/9 - 0:05
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#ifndef Wnd_RecipeView_h__
#define Wnd_RecipeView_h__

#pragma once

#include "VGStatic.h"
#include "Def_Enum.h"
#include "Def_TestDevice.h"
#include "File_Recipe.h"
#include "File_WatchList.h"
#include "Wnd_Cfg_Model.h"
#include "Wnd_Cfg_ConsumInfo.h"
#include "Wnd_Cfg_TestStep.h"
#include "Wnd_Cfg_TestItem.h"
#include "Wnd_Cfg_TestItem_ImgT.h"
#include "Wnd_Cfg_TestItem_Foc.h"
#include "Wnd_ImageView.h"
#include "Wnd_TestResult.h"
//#include "Wnd_Cfg_Delay.h"
#include "Wnd_Cfg_ROI_Image.h"
#include "Wnd_Cfg_DAQ.h"
#include "Wnd_ImgProc.h"
#include "Overlay_Proc.h"

#include "Wnd_TestResult_ImgT.h"
#include "Wnd_TestResult_Foc.h"
#ifdef USE_PRESET_MODE
#include "Wnd_Cfg_TestStepPreset.h"
#endif

//=============================================================================
// CWnd_RecipeView
//=============================================================================
class CWnd_RecipeView : public CWnd
{
	DECLARE_DYNAMIC(CWnd_RecipeView)

public:
	CWnd_RecipeView();
	virtual ~CWnd_RecipeView();

	CMFCTabCtrl		m_tc_Option;

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow			(BOOL bShow, UINT nStatus);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);

	afx_msg void	OnBnClickedBnNew		();
	afx_msg void	OnBnClickedBnSave		();
	afx_msg void	OnBnClickedBnSaveAs		();
	afx_msg void	OnBnClickedBnLoad		();
	afx_msg void	OnBnClickedBnImage		();
	afx_msg void	OnBnClickedBnRefresh	();
	afx_msg void	OnLbnSelChangeRecipe	();		

	afx_msg LRESULT	OnFileRecipe			(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnChangeRecipe			(WPARAM wParam, LPARAM lParam);

	afx_msg LRESULT	OnRefreshRecipeList		(WPARAM wParam, LPARAM lParam);

	afx_msg LRESULT	OnUpdateDataRoiList		(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnSelectResultView		(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnSelectItemTest		(WPARAM wParam, LPARAM lParam);

	afx_msg LRESULT	OnChangedModelType		(WPARAM wParam, LPARAM lParam);
	
	afx_msg void	OnBnClickedRbCameraSelLeft();
	afx_msg void	OnBnClickedRbCameraSelRight();
	void			Set_CameraSelect		(__in INT iChIdx, __in BOOL bUpdateRadioButton = TRUE);

	CFont			m_font_Default;
	CFont			m_font_Data;

	// 모델 리스트	
	CVGStatic				m_st_File;
	CVGStatic				m_st_Location;
//	CListBox				m_lst_RecipeList;
	CComboBox		        m_Combo_ModelList;
	CMFCButton				m_bn_Refresh;

	// New, Save, Save As, Load	
	CMFCButton				m_bn_New;
	CMFCButton				m_bn_Save;
	CMFCButton				m_bn_SaveAs;
	CMFCButton				m_bn_Load;
	CMFCButton				m_bn_ImageLoad;

	// Tab Windows
	CWnd_Cfg_Model			m_wnd_ModelCfg;
	CWnd_Cfg_TestStep		m_wnd_TestStepCfg;
	CWnd_Cfg_TestItem		m_wnd_TestItemCfg;
	CWnd_Cfg_TestItem_ImgT	m_wnd_TestItemImgTCfg;
	CWnd_Cfg_TestItem_Foc   m_wnd_Cfg_TestItem_Foc;
	//CWnd_Cfg_Delay		m_wnd_DelayCfg;
	CWnd_Cfg_ConsumInfo		m_wnd_ConsumInfoCfg;
	CWnd_Cfg_DAQ			m_wnd_DAQ;

#ifdef USE_PRESET_MODE
	// * Stereo CAL 임시 (Preset)
	CWnd_Cfg_TestStepPreset	m_wnd_Preset;
#endif
	
	// 이미지
	CVGStatic			m_st_Image;
	CWnd_ImageView		m_wnd_Image;
	CWnd_Cfg_ROI_Image	m_wnd_ROI_Image;
	COverlay_Proc		m_OverlayProc;

	CWnd_ImgProc		m_wnd_ImageLive[USE_CHANNEL_CNT];
	CMFCButton			m_rb_CameraSelect[USE_CHANNEL_CNT];		// 카메라 선택 버튼


	CMFCButton			m_rb_ImageMode[ImageMode_MaxNum];		// 이미지 모드

	CMFCButton			m_bt_ImageLoad;		
	CMFCButton			m_bt_ImageCapture;		

	// 알고리즘 테스트 결과
	CWnd_TestResult			m_wnd_TestResult;

	// 데이터 변수
	ST_RecipeInfo			m_stRecipeInfo;
	CFile_Recipe			m_fileRecipe;
	CFile_WatchList			m_ModelWatch;

	// 장치 제어 구조체 포인터
	ST_Device*				m_pDevice;
	const ST_CamInfo*		m_pCamInfo[USE_CHANNEL_CNT];

	// 검사기 설정
	enInsptrSysType			m_InspectionType = enInsptrSysType::Sys_Focusing;

	// New, Save, Save as, Load
	void		New_Recipe			();
	void		Save_Recipe			();
	void		SaveAs_Recipe		();
	void		Load_Recipe			();
	void		Load_Image			();

	void		GetRecipeInfo		();
	void		SetRecipeInfo		();

	CString		m_strRecipeFullPath;
	CString		m_strRecipePath;
	CString		m_strConsumInfoPath;
	CString		m_strImagePath;

	void		SetFullPath			(__in LPCTSTR szRecipeName);

	UINT nCamParaIdx;

public:
	
	// 검사기 종류 설정
	void		SetSystemType		(__in enInsptrSysType nSysType);

	// 작업 경로 설정
	void		SetPath				(__in LPCTSTR szRecipePath, __in LPCTSTR szPogoPath, __in LPCTSTR szImagePath)
	{
		if (NULL != szRecipePath)
			m_strRecipePath = szRecipePath;

		if (NULL != szPogoPath)
			m_strConsumInfoPath	 = szPogoPath;

		if (NULL != szImagePath)
			m_strImagePath = szImagePath;

		m_wnd_ConsumInfoCfg.SetConsumInfoPath(szPogoPath);

		m_ModelWatch.SetWatchOption(m_strRecipePath, RECIPE_FILE_EXT);
		m_ModelWatch.BeginWatchThrFunc();

		m_wnd_Image.SetImagePath(m_strImagePath);
	};

	// 레시피 파일이 저장되는 패스
	void	ChangePath				(__in LPCTSTR lpszRecipePath);
	
	// 레시피 설정 파일 불러오기
	void	SetRecipeFile			(__in LPCTSTR szRecipe);

	// 레시피 정보 UI에 설정하기
	void	SetRecipeInfo			(__in const ST_RecipeInfo* pstRecipeInfo);

	// 레시피 파일 목록 갱신
	void	RefreshRecipeFileList	(__in const CStringList* pFileList);

	// 장치 제어 구조체 포인터 설정
	void	SetPtr_Device			(__in ST_Device* pDevice)
	{
		m_pDevice = pDevice;
	};
	
	// 카메라 정보 전달~
	void 	SetPtr_CameraInfo		(__in const ST_CamInfo* pCamInfo)
	{
		if (NULL == pCamInfo)
			return;

		for (UINT nCh = 0; nCh < USE_CHANNEL_CNT; nCh++)
		{
			m_pCamInfo[nCh] = &pCamInfo[nCh];
		}
	};

	// Overlay Item 전달
	void	SetPtr_OverInfo			(__in const enOverlayItem enOverlayitem)
	{
		m_stRecipeInfo.nOverlayItem = enOverlayitem;
	};

	ST_ImageMode* m_pstImageMode;
	void	SetPtr_ImageMode(__in ST_ImageMode *stImageMode)
	{
		m_pstImageMode = stImageMode;
	};

	// 탭 컨트롤 탭 인덱스 초기화
	void		InitOptionView			();

	// 영상 뷰어 출력 용도
	void		ShowVideo_Overlay		(__in INT iChIdx, __inout IplImage* lpVideo, __in DWORD dwWidth, __in DWORD dwHeight);
	void		ShowVideo				(__in INT iChIdx, __in LPBYTE lpVideo, __in DWORD dwWidth, __in DWORD dwHeight);
	void		NoSignal_Ch				(__in INT iChIdx);

	afx_msg void OnBnClickedRbLiveMode();
	afx_msg void OnBnClickedRbStillMode();
	void ChangeImageMode(enImageMode eImageMode);
	afx_msg void OnBnClickedImageLoad();
	afx_msg void OnBnClickedImageSave();
	BOOL OnPopImageLoad();
	void OnImageSave();
	void OnImage_LoadDisplay(CString strPath);

	IplImage *m_LoadImage;
	
	HANDLE			m_hExternalExitEvent = NULL;	// 외부의 통신 접속 쓰레드 종료 이벤트

	BOOL			m_bFlag_ImageViewMon = FALSE;
	HANDLE			m_hThr_ImageViewMon = NULL;	// PLC 모니터링 쓰레드 핸들	
	DWORD			m_dwImageViewMonCycle = 50;	// 모니터링 주기
	BOOL			Start_ImageView_Mon();
	BOOL			Stop_ImageView_Mon();
	static UINT WINAPI Thread_ImageViewMon(__in LPVOID lParam);
	void OnLoadImageDisplay();
	afx_msg LRESULT OnChangeOptionPic(WPARAM wParam, LPARAM lParam);
	void TestRect_Setting(int nTestModel);
	CWnd_TestResult_ImgT	m_wnd_TestResult_ImgT[USE_CHANNEL_CNT];
	CWnd_TestResult_Foc		m_wnd_TestResult_Foc[USE_CHANNEL_CNT];

	BOOL *m_bImageCapturemode;
	void SetPtrImageCaptureMode(BOOL *bMode)
	{
		m_bImageCapturemode = bMode;
	};

	CString *m_pszImageCaptureFile;
	void SetPtrImageCaptureFile(CString *szImageCaptureFile)
	{
		m_pszImageCaptureFile = szImageCaptureFile;
	};

};

#endif // Wnd_RecipeView_h__

