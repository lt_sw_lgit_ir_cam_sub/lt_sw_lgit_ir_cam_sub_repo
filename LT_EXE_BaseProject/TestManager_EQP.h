﻿//*****************************************************************************
// Filename	: 	TestManager_EQP.h
// Created	:	2016/5/9 - 13:31
// Modified	:	2016/07/22
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef TestManager_EQP_h__
#define TestManager_EQP_h__

#pragma once

#include "TestManager_Test.h"
#include "TestManager_Device.h"
#include "TestSequence.h"

#include "Def_TestFunc.h"
#include "Reg_InspInfo.h"
#include "File_Report.h"
#include "File_RomBinary.h"
#include "File_Etc.h"
#include "LG_CalIntr.h"
#include "FileMES_LGIT.h"
#include "FileTestLog_LGIT.h"
#include "Test_ResultDataView.h"

#define ERR_RETRY_CNT		3

//-----------------------------------------------------------------------------
// CTestManager_EQP
//-----------------------------------------------------------------------------
class CTestManager_EQP : public CTestManager_Device
{
public:
	CTestManager_EQP();
	virtual ~CTestManager_EQP();

protected:

	//-----------------------------------------------------
	// 옵션
	//-----------------------------------------------------
	// 환경설정 데이터 불러오기
	virtual BOOL	OnLoadOption					();	
	virtual void	InitDevicez						(__in HWND hWndOwner = NULL);
	//virtual void	OnInitUISetting					(__in HWND hWndOwner = NULL);

	//-----------------------------------------------------
	// 이벤트 처리
	//-----------------------------------------------------
	HANDLE			m_hEvent_EjectModule[USE_CHANNEL_CNT];
	void			SetEvent_EjectModule			(__in UINT nParaIdx = 0);
	void			ResetEvent_EjectModule			(__in UINT nParaIdx = 0);

	HANDLE			m_hEvent_ReqCapture;
	void			SetEvent_ReqCapture				();
	void			ResetEvent_ReqCapture			();

	// Start/Stop 버튼 이벤트 처리
	enum enEvent_Button
	{
		Event_Bn_Start,
		Event_Bn_Stop,
		Event_Bn_MaxEnum,
	};

	HANDLE			m_hEvent_Butoon[Event_Bn_MaxEnum];
	void			SetEvent_Button					(__in enEvent_Button nButton);
	void			ResetEvent_Button				(__in enEvent_Button nButton);
	void			ResetEvent_Button_All			();
	LRESULT			Wait_Event_Button				(__out UINT& nOutEventType, __in DWORD dwTimeout = 600000);

	//-----------------------------------------------------
	// 쓰레드 작업 처리
	//-----------------------------------------------------
	typedef struct
	{
		LPVOID		pOwner;
		UINT		nIndex;			// Para Index
		UINT		nCondition;		// Load / Unload
		UINT		nArg_2;			// 
		UINT		nStepIndex;
	}stThreadParam;

	// Start 신호에 의해 구동되는 쓰레드
	HANDLE			m_hThr_LoadUnload;
	HANDLE			m_hThrTest_All;
	HANDLE			m_hThrTest_Unit[MAX_OPERATION_THREAD];

	HANDLE			m_hThrTest_Manual;
	HANDLE			m_hThrTest_Monitor;

	// Load/Unload 쓰레드 시작
	virtual LRESULT	StartOperation_LoadUnload		(__in BOOL bLoad);
	// 검사 쓰레드 시작
	virtual LRESULT	StartOperation_Inspection		(__in UINT nParaIdx = 0);
	// 개별 검사 쓰레드 시작
	virtual LRESULT	StartOperation_Test_Unit		(__in UINT nUnitIdx, __in enThreadTestType nTestType, __in UINT nStepIdx = 0);
	// 수동 모드일때 자동화 처리용 함수
	virtual LRESULT	StartOperation_Manual			(__in UINT nStepIdx, __in UINT nParaIdx = 0);

	// 수동 모드일때 검사항목 처리용 함수
	virtual LRESULT	StartOperation_InspManual		(__in UINT nParaIdx = 0);

	virtual LRESULT	StartThread_Monitoring			();

	// 자동으로 처리되고 있는 쓰레드 강제 종료
	virtual void	StopProcess_LoadUnload			();
	virtual void	StopProcess_Test_All			();
	virtual void	StopProcess_Test_Unit			(__in UINT nUnitIdx);
	virtual void	StopProcess_InspManual			(__in UINT nParaIdx = 0);

	// 검사 쓰레드
	static UINT WINAPI	ThreadLoadUnload			(__in LPVOID lParam);
	static UINT WINAPI	ThreadTest_All				(__in LPVOID lParam);
	static UINT WINAPI	ThreadTest_Unit				(__in LPVOID lParam);
	static UINT WINAPI	ThreadManualTest			(__in LPVOID lParam);

	// 거리, 각도 측정
	static UINT WINAPI	Thread_MotorMonitoring		(__in LPVOID lParam);

	// 쓰레드 내에서 처리될 자동 작업 함수
	virtual BOOL	AutomaticProcess_LoadUnload		(__in BOOL bLoad);
	virtual void	AutomaticProcess_Test_All		(__in UINT nParaIdx = 0);
	virtual BOOL	AutomaticProcess_Test_Unit		(__in UINT nUnitIdx, __in enThreadTestType nTestType, __in UINT nStepIdx);
	virtual void	AutomaticProcess_Test_InspManual(__in UINT nParaIdx = 0);

	// 거리, 각도 측정
	virtual void	AutomaticProcess_Motor			();

	virtual void	SetMoterMoniter					(__in CString szDistance = NULL, __in CString szChartTX = NULL, __in CString szChartTZ = NULL){};

	// 팔레트 로딩/언로딩 초기 세팅
	virtual void	OnInitial_LoadUnload			(__in BOOL bLoad);
	// 팔레트 로딩/언로딩 종료 세팅
	virtual void	OnFinally_LoadUnload			(__in BOOL bLoad);
	// 개별 검사 작업 시작
	virtual LRESULT	OnStart_LoadUnload				(__in BOOL bLoad);

	// 전체 테스트 초기 세팅
	virtual void	OnInitial_Test_All				(__in UINT nParaIdx = 0);
	// 전체 테스트 종료 세팅
	virtual void	OnFinally_Test_All				(__in LRESULT lResult, __in UINT nParaIdx = 0);
	// 전체 검사 작업 시작
	virtual LRESULT	OnStart_Test_All				(__in UINT nParaIdx = 0);
	virtual LRESULT	OnStart_Test_Unit				(__in UINT nUnitIdx, __in enThreadTestType nTestType, __in UINT nStepIdx);
	
	// 개별 테스트 동작항목
	virtual void	OnInitial_Test_InspManual		(__in UINT nTestItemID, __in UINT nParaIdx = 0);
	virtual void	OnFinally_Test_InspManual		(__in UINT nTestItemID, __in LRESULT lResult, __in UINT nParaIdx = 0);
	virtual LRESULT	OnStart_Test_InspManual			(__in UINT nTestItemID, __in UINT nParaIdx = 0);
	
	// 검사
	virtual LRESULT Load_Product					();	// left, right
	virtual LRESULT Unload_Product					();	// left, right

	// 검사기별 검사
	virtual LRESULT	StartTest_Equipment				(__in UINT nParaIdx = 0);
	//virtual LRESULT	StartTest_Eqp				(__in UINT nParaIdx = 0);
	virtual LRESULT	StartTest_PresetMode			(__in UINT nPresetIndex, __in UINT nParaIdx = 0);

	// 개별 검사 항목
	typedef struct
	{
		LPVOID		pOwner;
		UINT		nParaIdx;			// Para Index
		UINT		nTestItemID;		// TestItem
		UINT		nStepIdx;			// StepIdx
		UINT		nArg_1;				// Corner Idx
		UINT		nArg_2;				// 예약
	}stParam_TestItem;
	// Start 신호에 의해 구동되는 쓰레드
	HANDLE			m_hThrTest_Item;
	// 검사 쓰레드 시작
	LRESULT			StartThread_TestItem			(__in UINT nParaIdx, __in UINT nTestItemID, __in UINT nStepIdx, __in UINT nCornerStep = 0);
	// 검사 쓰레드
	static UINT WINAPI	Thread_TestItem				(__in LPVOID lParam);
	// 쓰레드 내에서 처리될 자동 작업 함수
	virtual void	AutomaticProcess_TestItem		(__in UINT nParaIdx, __in UINT nTestItemID, __in UINT nStepIdx, __in UINT nCornerStep = 0);
	LRESULT			Wait_FinishThead_TestItem		();
	LRESULT			Wait_Event_ReqCapture			();


	// 공용 검사항목
	virtual	LRESULT StartThread_TestUnit_All		(__in enThreadTestType nThreadTestType, __in UINT nStepIdx);

	virtual	LRESULT _TI_Cm_Initialize_All			(__in UINT nStepIdx);	// 쓰레드로 전체 초기화
	virtual	LRESULT _TI_Cm_Initialize_All_NoThread	(__in UINT nStepIdx);	// 쓰레드로 전체 초기화
	virtual	LRESULT _TI_Cm_Finalize_All				(__in UINT nStepIdx);	// 쓰레드로 전체 마무리
	virtual	LRESULT _TI_Cm_Finalize_All_NoThread	(__in UINT nStepIdx);	// 쓰레드로 전체 마무리
	virtual LRESULT _TI_Cm_CameraReboot_All			();

	virtual	LRESULT _TI_Cm_Initialize				(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual	LRESULT _TI_Cm_Finalize					(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual	LRESULT _TI_Cm_Finalize_Error			(__in UINT nParaIdx = 0);
	virtual LRESULT _TI_Cm_CameraReboot				(__in BOOL bUseCaptureOn, __in UINT nParaIdx = 0, __in DWORD dwRebootDelay = 500);
	virtual	LRESULT _TI_Cm_MeasurCurrent			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual	LRESULT _TI_Cm_MeasurVoltage			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual	LRESULT	_TI_Cm_CaptureImage				(__in UINT nStepIdx, __in UINT nParaIdx = 0, __in BOOL bSaveImage = FALSE, __in LPCTSTR szFileName = NULL);
	virtual	LRESULT	_TI_Cm_CameraRegisterSet		(__in UINT nStepIdx, __in UINT nRegisterType, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_Cm_CameraAlphaSet			(__in UINT nStepIdx, __in UINT nAlpha, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_Cm_CameraPowerOnOff			(__in UINT nStepIdx, __in enPowerOnOff nOnOff, __in UINT nParaIdx = 0);
	
	
	virtual void	OnImage_AddHistory				(__in UINT nChIdx, __in LPCTSTR szTitle){};
	virtual void	OnImage_SetHistory				(__in UINT nChIdx, __in UINT nHistoryIndex, __in LPCTSTR szTitle){};

	// 검사 최종 판정
	enTestResult	Judgment_Inspection				(__in UINT nParaIdx = 0);
	enTestResult	Judgment_Inspection_All			();
	enTestResult	Judgment_CheckStep				(__in UINT nStepIdx, __in UINT nParaIdx = 0);

	// Library 연동 (2D CAL / Stereo CAL)


	// 체크 스텝별 검사 시간
	virtual DWORD	Check_TimeStepStart				();
	virtual DWORD	Check_TimeStepStop				(__in DWORD dwStart);

	// MES 통신
	virtual LRESULT	MES_CheckBarcodeValidation		(__in UINT nParaIdx = 0);
	virtual LRESULT MES_SendInspectionData			(__in UINT nParaIdx = 0);
	//virtual LRESULT MES_MakeInspecData_Eqp		(__in UINT nParaIdx = 0);

	// 검사 결과에 따른 메세지 팝업
	virtual void	OnPopupMessageTest_All			(__in enResultCode nResultCode);

	//-----------------------------------------------------
	// Motion
	//-----------------------------------------------------	
	virtual void	OnMotion_SetOption_Model		(__in enModelType nModelType);

	virtual LRESULT	OnMotion_MoveToTestPos			(__in UINT nParaIdx = 0);
	virtual	LRESULT OnMotion_MoveToStandbyPos		(__in UINT nParaIdx = 0);
	virtual LRESULT	OnMotion_MoveToUnloadPos		(__in UINT nParaIdx = 0);

	virtual LRESULT	OnMotion_Origin					();
	virtual LRESULT	OnMotion_LoadProduct			();
	virtual LRESULT	OnMotion_UnloadProduct			();
	virtual LRESULT OnMotion_EStop					();

	//-----------------------------------------------------
	// Digital I/O
	//-----------------------------------------------------
	// Digital I/O 신호 UI 처리
	virtual void	OnDIO_UpdateDInSignal			(__in BYTE byBitOffset, __in BOOL bOnOff){};
	virtual void	OnDIO_UpdateDOutSignal			(__in BYTE byBitOffset, __in BOOL bOnOff){};
	// I/O 신호 감지 및 신호별 기능 처리
	virtual void	OnDIn_DetectSignal				(__in BYTE byBitOffset, __in BOOL bOnOff);
	virtual void	OnDIn_DetectSignal_2DCAL		(__in BYTE byBitOffset, __in BOOL bOnOff);
	virtual void	OnDIn_DetectSignal_ImgT			(__in BYTE byBitOffset, __in BOOL bOnOff);
	virtual void	OnDIn_DetectSignal_Foc			(__in BYTE byBitOffset, __in BOOL bOnOff);
	// Digital I/O 초기화
	virtual void	OnDIO_InitialSignal				();
	virtual void	OnSet_BoardPower				(__in BOOL bOnOff);

	virtual LRESULT	OnDIn_MainPower					();
	virtual LRESULT	OnDIn_EMO						();
	virtual LRESULT	OnDIn_AreaSensor				();
	virtual LRESULT	OnDIn_DoorSensor				();
	virtual LRESULT	OnDIn_JIGCoverCheck				();
	virtual LRESULT	OnDIn_Start						();
	virtual LRESULT	OnDIn_Stop						();
	virtual LRESULT	OnDIn_CheckSafetyIO				();
	virtual LRESULT	OnDIn_CheckJIGCoverStatus		();
		
	virtual LRESULT	OnDOut_BoardPower				(__in BOOL bOn, __in UINT nParaIdx = 0);
	virtual LRESULT	OnDOut_FluorescentLamp			(__in BOOL bOn);
	virtual LRESULT	OnDOut_StartLamp				(__in BOOL bOn);
	virtual LRESULT	OnDOut_StopLamp					(__in BOOL bOn);
	virtual LRESULT	OnDOut_TowerLamp				(__in enLampColor nLampColor, __in BOOL bOn);
	virtual LRESULT	OnDOut_Buzzer					(__in BOOL bOn);

	// DAQ 보드
	virtual LRESULT	OnDAQ_CaptureStart				(__in UINT nIdxBrd);
	virtual LRESULT	OnDAQ_CaptureStop				(__in UINT nIdxBrd);
	virtual LRESULT OnDAQ_SensorInit_Unit			(__in UINT nIdxBrd, UINT nRegisterMode = 0);
	virtual LRESULT	OnDAQ_SensorInit_All			();
	virtual LRESULT	OnDAQ_CheckVideoSignal			(__in DWORD dwCheckTime, __in UINT nIdxBrd);
	virtual BOOL	OnDAQ_CheckFrameCount			(__in DWORD dwCheckTime, __in UINT nIdxBrd);
	virtual void	OnDAQ_SetOption					(__in enModelType nModelType);
	virtual void	OnDAQ_SetOption_Model			(__in enModelType nModelType);
	virtual LRESULT	OnDAQ_EEPROM_Write				(__in UINT nIdxBrd = 0);
	virtual LRESULT	OnDAQ_EEPROM_OMS_Entry_2D		(__in UINT nIdxBrd = 0);
	virtual LRESULT	OnDAQ_EEPROM_OMS_Front_2D		(__in UINT nIdxBrd = 0);
	virtual LRESULT	OnDAQ_EEPROM_OMS_FrontSet_2D	(__in UINT nIdxBrd = 0);
	virtual LRESULT	OnDAQ_EEPROM_Verify_OMS_Entry	(__in UINT nIdxBrd = 0);
	virtual LRESULT	OnDAQ_EEPROM_Verify_IKC			(__in UINT nIdxBrd = 0);

	virtual BOOL	OnSaveImage_Gray16				(__in LPCTSTR szInPath, __in LPWORD lpwInImage, __in DWORD dwWidth, __in DWORD dwHeight);
	virtual BOOL	OnLoadImage_Gray16				(__in LPCTSTR szInPath, __out LPWORD lpwOutImage, __out DWORD& dwOutWidth, __out DWORD& dwOutHeight);
	virtual BOOL	OnSaveImage_RGB					(__in LPCTSTR szInPath, __in LPBYTE lpbyInImage, __in DWORD dwWidth, __in DWORD dwHeight);
	virtual BOOL	OnLoadImage_RGB					(__in LPCTSTR szInPath, __out LPBYTE lpbyOutImage, __out DWORD& dwOutWidth, __out DWORD& dwOutHeight);

	//-----------------------------------------------------
	// 카메라 보드 / 광원 제어 보드
	//-----------------------------------------------------
 	virtual LRESULT OnCameraBrd_CheckSync			();
	virtual LRESULT OnCameraBrd_PowerOnOff			(__in enPowerOnOff bOn, __in UINT nParaIdx = 0, __in UINT nRetry = 0);
 	virtual LRESULT OnCameraBrd_Read_Current		(__in UINT nParaIdx = 0);
	virtual LRESULT OnCameraBrd_CheckOverCurrent	(__out BOOL bOverCurrent, __in UINT nParaIdx = 0);
 	
	// 광원 제어 보드
	virtual LRESULT OnLightBrd_CheckSync			();
	virtual LRESULT OnLightBrd_PowerOn				(__in float fVolt, __in WORD wStep);
	virtual LRESULT OnLightBrd_Volt_PowerOn			(__in float fVolt);
	virtual LRESULT OnLightBrd_PowerOff				();

	// ODA Q 광원 파워 서플라이
	virtual LRESULT OnLightPSU_CheckSync			();
	virtual LRESULT OnLightPSU_PowerOnOff			(__in BOOL bOn);
	virtual LRESULT OnLightPSU_Voltage				();
	virtual LRESULT OnLightPSU_Current				();

	// 광원보드 설정값 적용
	LRESULT			OnLightPSU_CheckLightOn			();
	LRESULT			OnLightBrd_CheckLightOn			();

	//-----------------------------------------------------
	// 메뉴얼 테스트
	//-----------------------------------------------------
	virtual LRESULT OnManualSequence				(__in UINT nParaID, __in enParaManual enFunc);
	virtual	LRESULT OnManualTestItem				(__in UINT nParaID, __in UINT nStepIdx);

	//-----------------------------------------------------
	// 테스트
	//-----------------------------------------------------
	virtual LRESULT	OnCheck_Barcode					(__in UINT nParaIdx = 0);

	// 주변기기 통신 상태 체크
	virtual LRESULT	OnCheck_DeviceComm				();
	// 기구물 안전 체크
	virtual LRESULT	OnCheck_SaftyJIG				();	
	// 모델 설정 데이터가 정상인가 판단
	virtual LRESULT	OnCheck_RecipeInfo				();	
	
	// 제품 결과 판정 및 리포트 처리
	virtual void	OnJugdement_And_Report			(__in UINT nParaIdx = 0);
	virtual void	OnJugdement_And_Report_All		();
	// Cycle Time Check
	virtual void	OnSet_TestTime					(__in UINT nParaIdx = 0);
	virtual void	OnSet_CycleTime					();
	
	// 카메라 데이터 초기화
	virtual void	OnReset_CamInfo					(__in UINT nParaIdx = 0);
	virtual void	OnReset_CamInfo_All				();
	// 제품 로딩시 데이터 초기화
	virtual void	OnResetInfo_Loading				();
	// 검사 시작 전 데이터 초기화
	virtual void	OnResetInfo_StartTest			(__in UINT nParaIdx = 0);
	// 제품 배출시 데이터 초기화
	virtual void	OnResetInfo_Unloading			();
	// 제품 재검사를 위해서 측정 데이터 및 UI 초기화
	virtual void	OnResetInfo_Measurment			(__in UINT nParaIdx = 0);
	
	// 알람 상태 추가
	virtual void	OnAddAlarmInfo					(__in enResultCode nResultCode);
	virtual void	OnAddAlarm						(__in LPCTSTR szAlarm);
	virtual void	OnAddAlarm_F					(__in LPCTSTR szAlarm, ...){};

	//virtual void	OnSet_BoardPower				(__in BOOL bOnOff);

	//-----------------------------------------------------
	// 타이머 
	//-----------------------------------------------------
	virtual void	CreateTimer_UpdateUI			(__in DWORD DueTime = 5000, __in DWORD Period = 250);
	virtual void	DeleteTimer_UpdateUI			();
	virtual void	OnMonitor_TimeCheck				();
	virtual void	OnMonitor_UpdateUI				();
	DWORD			m_dwTimeCheck;	// 시간 체크용 임시 변수
	
	//-----------------------------------------------------
	// UI 업데이트
	//-----------------------------------------------------
	virtual void	OnUpdate_EquipmentInfo			(){};
	virtual void	OnUpdate_ElapTime_TestUnit		(__in UINT nParaIdx = 0){};
	virtual void	OnUpdate_ElapTime_LoadAtOnce	(){};
	virtual void	OnUpdate_ElapTime_Cycle			(){};
	//virtual void	OnUpdate_Testing_Barcode		(__in UINT nParaIdx = 0){};
	virtual void	OnUpdate_TestReport				(__in UINT nParaIdx = 0){};

	virtual void	OnSet_TestProgress				(__in enTestProcess nProcess);
	virtual void	OnSet_TestProgress_Unit			(__in enTestProcess nProcess, __in UINT nParaIdx = 0);
	virtual void	OnSet_TestProgressStep			(__in UINT nTotalStep, __in UINT nProgStep){};
	virtual void	OnSet_TestResult				(__in enTestResult nResult);
	virtual void	OnSet_TestResult_Unit			(__in enTestResult nResult, __in UINT nParaIdx = 0);
	virtual void	OnSet_ResultCode_Unit			(__in LRESULT nResultCode, __in UINT nParaIdx = 0);
	virtual void	OnSet_TestItemResult			(__in UINT nItemIdx, __in enTestResult nResult, __in LPCTSTR szMeasureValue, __in DWORD dwDuration);
	virtual void	OnSet_TestStepSelect			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual void	OnSet_TestStepResult			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	
	virtual void	OnSet_InputTime					();
	virtual void	OnSet_InputTime__Unit			(__in UINT nParaIdx = 0);
	virtual void	OnSet_BeginTestTime				(__in UINT nParaIdx = 0);
	virtual void	OnSet_EndTestTime				(__in UINT nParaIdx = 0);
	virtual void	OnSet_OutputTime				();
	virtual void	OnSet_Barcode					(__in LPCTSTR szBarcode, __in UINT nRetryCnt = 0, __in UINT nParaIdx = 0);

	virtual void	OnSet_VCSEL_Status				(__in BOOL bOn, __in UINT nParaIdx = 0);
	virtual void	OnSet_PowerSupplyMeas			(__in float fVoltage, __in float fCurrent){};

	// 팝업된 다이얼로그 닫기
	virtual void	OnHidePopupUI					(){};

	virtual void	OnSetCamerParaSelect			(__in UINT nParaIdx = 0){};

	//-----------------------------------------------------
	// Worklist 처리
	//-----------------------------------------------------
	virtual void	OnInsertWorklist				(){};
	virtual void	OnSaveWorklist					();
	virtual void	OnLoadWorklist					(){};
	
	//-----------------------------------------------------
	// 수율 
	//-----------------------------------------------------
	virtual void	OnUpdateYield					(__in UINT nParaIdx = 0);
	virtual void	OnSaveYield						();
	virtual void	OnLoadYield						();

	//-----------------------------------------------------
	// 소모품 카운트 처리
	//-----------------------------------------------------
	virtual BOOL	Reset_ConsumCount				(__in UINT nItemIdx);
	virtual BOOL	Increase_ConsumCount			(__in UINT nItemIdx);
	virtual BOOL	Increase_ConsumCount			();
	virtual BOOL	Save_ConsumInfo					(__in UINT nItemIdx);
	virtual BOOL	Save_ConsumInfo					();
	virtual BOOL	Load_ConsumInfo					();
	virtual BOOL	Check_ConsumInfo				();
	virtual void	OnSetStatus_ConsumInfo			(){};
	virtual void	OnSetStatus_ConsumInfo			(__in UINT nItemIdx){};

	//-----------------------------------------------------
	// 프리셋 모드
	//-----------------------------------------------------
	virtual BOOL	Load_PresetStepInfo				(__in LPCTSTR szRecipe);

	// 현재 사용중인 레시피 파일  저장/불러오기
	virtual BOOL	OnSave_SelectedRecipe			();
	virtual BOOL	OnLoad_SelectedRecipe			(__out CString& szRecipeFile, __out CString& szRecipe);
	// 레시피별 폴더 구분 기능 사용여부 체크
	virtual void	OnCheck_ForcedModel				();

	//-----------------------------------------------------
	// 멤버 변수 & 클래스
	//-----------------------------------------------------

	// 검사에 관련된 모든 정보가 들어있는 구조체 
	ST_InspectionInfo	m_stInspInfo;

	// 레지스트리 데이터 처리용 클래스
	CReg_InspInfo		m_regInspInfo;

	// 결과 데이터 처리용 클래스
	CFile_Report		m_FileReport;
	CFileMES_LGIT		m_FileMES;
	CFileTestLog_LGIT	m_FileTestLog;

	// 검사 시작 조건이 될때까지 검사 시작 방지
	BOOL				m_bFlag_ReadyTest;

	BOOL				m_bFlag_MotorMonitor;

#ifndef MOTION_NOT_USE
	// 모션 시퀀스 제어
	CTestSequence		m_MotionSequence;
#endif

	// 영상 검사용  영상 프레임 버퍼 구조체
	ST_ImageBuffer		m_stImageBuf[USE_CHANNEL_CNT];
	//-영상 모드/ 이미지 경로
	ST_ImageMode		m_stImageMode;

	CTestManager_Test	m_tm_Test;

	CTest_ResultDataView m_Test_ResultDataView;

	BOOL				m_bPicCaptureMode = FALSE;

	CString				m_szImageFileName;

public:
	
	// 생성자 처리용 코드
	virtual void		OnInitialize				();
	// 소멸자 처리용 코드
	virtual	void		OnFinalize					();

	// 검사기 종류 설정
	virtual void		SetSystemType				(__in enInsptrSysType nSysType);

	//-----------------------------------------------------
	// 검사 상태 확인용 함수
	//-----------------------------------------------------
	// 개별, 전체 검사가 수행 중인지 확인
	BOOL				IsTesting					();
	// 전체 검사가 수행 중인지 확인
	BOOL				IsTesting_All				();
	// Load/Unload가 수행 중인지 확인
	BOOL				IsTesting_LoadUnload		();
	BOOL				IsTesting_Manual			();
	// 개별 검사가 수행 중인지 확인
	BOOL				IsTesting_Unit				(__in UINT nUnitIdx);
	// 입력된 Unit을 제외한 검사가 수행 중인지 확인
	BOOL				IsTesting_Exc				(__in UINT nExcUnitIdx);

	// 관리자 모드 확인 (구버전)
	BOOL				IsManagerMode				();
	// 제어 권한 상태 구하기
	enPermissionMode	GetPermissionMode			();
	// 제어 권한 상태 설정
	virtual void		SetPermissionMode			(__in enPermissionMode nAcessMode);
	// MES Online Mode
	virtual void		SetMESOnlineMode			(__in enMES_Online nOnlineMode);
	// 설비 구동 모드
	virtual void		SetOperateMode				(__in enOperateMode nOperMode);
	
	// 설비 초기화 (원점 수행) : 설비 오류 시 초기화 하기 위해 사용
	virtual void		EquipmentInit				(__in UINT nCondition = 0);

	virtual LRESULT _TI_Cm_RegisterChangeMode(__in UINT nParaIdx, UINT nRegisterMode);
	// EEPROM 제어 (임시)
	//virtual void		OnEEPROM_Ctrl				(__in UINT nCondition = 0){};
	
};

#endif // TestManager_EQP_h__

