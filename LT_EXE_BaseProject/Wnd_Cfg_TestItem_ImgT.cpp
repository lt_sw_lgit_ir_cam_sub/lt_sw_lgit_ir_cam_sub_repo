//*****************************************************************************
// Filename	: 	Wnd_Cfg_TestItem_ImgT.cpp
// Created	:	2017/9/24 - 16:11
// Modified	:	2017/9/24 - 16:11
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_Cfg_TestItem_ImgT.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_Cfg_TestItem_ImgT.h"
#include "Def_WindowMessage.h"

// CWnd_Cfg_TestItem_ImgT

#define IDC_TAB 1000
IMPLEMENT_DYNAMIC(CWnd_Cfg_TestItem_ImgT, CWnd_BaseView)

CWnd_Cfg_TestItem_ImgT::CWnd_Cfg_TestItem_ImgT()
{
	m_InspectionType = enInsptrSysType::Sys_Image_Test;
}

CWnd_Cfg_TestItem_ImgT::~CWnd_Cfg_TestItem_ImgT()
{
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_TestItem_ImgT, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()

//	ON_EN_SETFOCUS(IDC_TAB, &CWnd_Cfg_TestItem_ImgT::OnEnSetfocusTabFocus)

END_MESSAGE_MAP()

//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/9/28 - 17:51
// Desc.		:
//=============================================================================
int CWnd_Cfg_TestItem_ImgT::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();
	
	UINT nID_Index = 0;

	m_tc_Option.Create(CMFCTabCtrl::STYLE_3D, rectDummy, this, IDC_TAB, CMFCTabCtrl::LOCATION_BOTTOM);

	m_Wnd_ECurrent.SetOwner(GetOwner());
	m_Wnd_ECurrent.SetOverlayID(WM_SELECT_OVERLAY);
	m_Wnd_ECurrent.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_ECurrent.Create(NULL, _T("Current"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);


	m_Wnd_OpticalCenter.SetOwner(GetOwner());
	m_Wnd_OpticalCenter.SetOverlayID(WM_SELECT_OVERLAY);
	m_Wnd_OpticalCenter.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_OpticalCenter.Create(NULL, _T("OpticalCenter"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);


	m_Wnd_Rotate.SetOwner(GetOwner());
	m_Wnd_Rotate.SetOverlayID(WM_SELECT_OVERLAY);
	m_Wnd_Rotate.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_Rotate.Create(NULL, _T("Rotate"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);


	m_Wnd_Distortion.SetOwner(GetOwner());
	m_Wnd_Distortion.SetOverlayID(WM_SELECT_OVERLAY);
	m_Wnd_Distortion.SeChangeOption(WM_CHANGE_OPTIONPIC);

	m_Wnd_Distortion.Create(NULL, _T("Distortion"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_Fov.SetOwner(GetOwner());
	m_Wnd_Fov.SetOverlayID(WM_SELECT_OVERLAY);
	m_Wnd_Fov.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_Fov.Create(NULL, _T("FOV"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);


	m_Wnd_Dynamic.SetOwner(GetOwner());
	m_Wnd_Dynamic.SetOverlayID(WM_SELECT_OVERLAY);
	m_Wnd_Dynamic.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_Dynamic.Create(NULL, _T("Dynamic"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);


	m_Wnd_DefectPixel.SetOwner(GetOwner());
	m_Wnd_DefectPixel.SetOverlayID(WM_SELECT_OVERLAY);
	m_Wnd_DefectPixel.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_DefectPixel.Create(NULL, _T("DefectPixel"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	for (int t = 0; t < SFR_TestNum_MaxEnum; t++)
	{
		m_Wnd_SFR[t].SetOwner(GetOwner());
		m_Wnd_SFR[t].SetOverlayID(WM_SELECT_OVERLAY);
		m_Wnd_SFR[t].SeChangeOption(WM_CHANGE_OPTIONPIC);
		m_Wnd_SFR[t].Create(NULL, _T("SFR"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);
		m_Wnd_SFR[t].SetOvr_MessageNum(t);
	}


	m_Wnd_SNR_Light.SetOwner(GetOwner());
	m_Wnd_SNR_Light.SetOverlayID(WM_SELECT_OVERLAY);
	m_Wnd_SNR_Light.SeChangeOption(WM_CHANGE_OPTIONPIC);

	m_Wnd_SNR_Light.Create(NULL, _T("SNR_Light"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_Intensity.SetOwner(GetOwner());
	m_Wnd_Intensity.SetOverlayID(WM_SELECT_OVERLAY);
	m_Wnd_Intensity.SeChangeOption(WM_CHANGE_OPTIONPIC);

	m_Wnd_Intensity.Create(NULL, _T("Intensity"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_Shading.SetOwner(GetOwner());
	m_Wnd_Shading.SetOverlayID(WM_SELECT_OVERLAY);
	m_Wnd_Shading.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_Shading.Create(NULL, _T("Shading"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);


	m_Wnd_Particle.SetOwner(GetOwner());
	m_Wnd_Particle.SetOverlayID(WM_SELECT_OVERLAY);
	m_Wnd_Particle.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_Particle.Create(NULL, _T("Particle"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	

//#ifdef SET_GWANGJU
	m_Wnd_HotPixel.SetOwner(GetOwner());
	m_Wnd_HotPixel.SetOverlayID(WM_SELECT_OVERLAY);
	m_Wnd_HotPixel.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_HotPixel.Create(NULL, _T("HotPixel"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_3D_Depth.SetOwner(GetOwner());
	m_Wnd_3D_Depth.SetOverlayID(WM_SELECT_OVERLAY);
	m_Wnd_3D_Depth.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_3D_Depth.Create(NULL, _T("3D_Depth"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_FPN.SetOwner(GetOwner());
	m_Wnd_FPN.SetOverlayID(WM_SELECT_OVERLAY);
	m_Wnd_FPN.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_FPN.Create(NULL, _T("FPN"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_EEPROM_Verify.SetOwner(GetOwner());
	m_Wnd_EEPROM_Verify.SetOverlayID(WM_SELECT_OVERLAY);
	m_Wnd_EEPROM_Verify.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_EEPROM_Verify.Create(NULL, _T("EEPROM_Verify"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

//#endif
	m_Wnd_TemperatureSensor.SetOwner(GetOwner());
	m_Wnd_TemperatureSensor.SetOverlayID(WM_SELECT_OVERLAY);
	m_Wnd_TemperatureSensor.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_TemperatureSensor.Create(NULL, _T("TemperatureSensor"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_Particle_Entry.SetOwner(GetOwner());
	m_Wnd_Particle_Entry.SetOverlayID(WM_SELECT_OVERLAY);
	m_Wnd_Particle_Entry.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_Particle_Entry.Create(NULL, _T("Particle_Entry"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_wnd_TestItem_EachTest.SetCameraParaIdx(m_pnCamParaIdx);

	m_wnd_TestItem_EachTest.Create(NULL, _T("개별"), dwStyle /*| WS_BORDER*/, rectDummy, this, nID_Index++);

	// 검사기 별로 탭 컨트롤 정의
	SetSysAddTabCreate();

	m_tc_Option.SetActiveTab(0);
	m_tc_Option.EnableTabSwap(FALSE);
	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/9/28 - 17:52
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_ImgT::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMargin = 5;
	int iLeft	= iMargin;
	int iTop	= iMargin;
	int iWidth	= cx - iMargin - iMargin;
	int iHeight = cy - iMargin - iMargin;

	int nTabW = iWidth * 4 / 5;
	m_tc_Option.MoveWindow(iLeft, iTop, nTabW, iHeight);

	iLeft += iMargin + nTabW;
	
	m_wnd_TestItem_EachTest.MoveWindow(iLeft, iTop, iWidth - (iMargin + nTabW), iHeight);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: protected  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/10/21 - 11:21
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_ImgT::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (TRUE == bShow)
	{
		m_tc_Option.SetActiveTab(0);
	}
}

//=============================================================================
// Method		: OnNMClickTestItem
// Access		: protected  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/9/28 - 18:36
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_ImgT::OnNMClickTestItem(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	*pResult = 0;
}

//=============================================================================
// Method		: SetSysAddTabCreate
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/15 - 10:43
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_ImgT::SetSysAddTabCreate()
{
	UINT nItemCnt = 0;

	m_tc_Option.AddTab(&m_Wnd_ECurrent, _T("Current"), nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_Wnd_OpticalCenter, _T("OpticalCenter"), nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_Wnd_Rotate, _T("Rotate"), nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_Wnd_Distortion, _T("Distortion"), nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_Wnd_Fov, _T("FOV"), nItemCnt++, TRUE);
	for (int t = 0; t < SFR_TestNum_MaxEnum; t++)
	{
		m_tc_Option.AddTab(&m_Wnd_SFR[t], g_szSFR_TestNum[t], nItemCnt++, TRUE);
	}
	m_tc_Option.AddTab(&m_Wnd_Particle, _T("Stain"), nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_Wnd_DefectPixel, _T("DefectPixel"), nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_Wnd_Dynamic, _T("Dynamic"), nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_Wnd_Intensity, _T("Shading(RI)"), nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_Wnd_SNR_Light, _T("SNR Light"), nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_Wnd_Shading, _T("Shading(SNR)"), nItemCnt++, TRUE);

//#ifdef SET_GWANGJU
	m_tc_Option.AddTab(&m_Wnd_HotPixel, _T("Hot Pixel"), nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_Wnd_FPN, _T("FPN"), nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_Wnd_3D_Depth, _T("3D_Depth"), nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_Wnd_EEPROM_Verify, _T("EEPROM_Verify"), nItemCnt++, TRUE);
//#endif
	m_tc_Option.AddTab(&m_Wnd_TemperatureSensor, _T("TemperatureSensor"), nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_Wnd_Particle_Entry, _T("Particle_Entry"), nItemCnt++, TRUE);
	

}

//=============================================================================
// Method		: SetInitListCtrl
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/28 - 18:20
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_ImgT::SetInitListCtrl()
{
}

//=============================================================================
// Method		: SetSystemType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2017/9/26 - 14:12
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_ImgT::SetSystemType(__in enInsptrSysType nSysType)
{
	m_InspectionType = nSysType;
	m_wnd_TestItem_EachTest.SetSystemType(nSysType);
}

//=============================================================================
// Method		: Set_RecipeInfo
// Access		: public  
// Returns		: void
// Parameter	: __in ST_RecipeInfo * pstRecipeInfo
// Qualifier	:
// Last Update	: 2017/11/10 - 20:50
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_ImgT::Set_RecipeInfo(__in ST_RecipeInfo* pstRecipeInfo)
{

	switch (pstRecipeInfo->ModelType)
	{
	case Model_MRA2:
		m_Wnd_ECurrent.OnSetCameraModelType(0);
		m_Wnd_ECurrent.OnSetModelType(1);
		break;

	case Model_IKC:
		m_Wnd_ECurrent.OnSetCameraModelType(0);
		m_Wnd_ECurrent.OnSetModelType(0);
		break;

	case Model_OMS_Entry:
	case Model_OMS_Front:
	case Model_OMS_Front_Set:
		m_Wnd_ECurrent.OnSetCameraModelType(1);
		m_Wnd_ECurrent.OnSetModelType(0);
		break;

	default:
		break;
	}

	m_Wnd_ECurrent.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stECurrentOpt);
	m_Wnd_ECurrent.SetUpdateData();

	m_Wnd_OpticalCenter.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stOpticalCenterOpt);
	m_Wnd_OpticalCenter.SetUpdateData();

	m_Wnd_Rotate.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stRotateOpt);
	m_Wnd_Rotate.SetUpdateData();

	m_Wnd_Distortion.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stDistortionOpt);
	m_Wnd_Distortion.SetUpdateData();

	m_Wnd_Fov.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stFovOpt);
	m_Wnd_Fov.SetUpdateData();

	m_Wnd_Dynamic.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stDynamicOpt);
	m_Wnd_Dynamic.SetUpdateData();

	m_Wnd_DefectPixel.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stDefectPixelOpt);
	m_Wnd_DefectPixel.SetUpdateData();

	for (int t = 0; t < SFR_TestNum_MaxEnum; t++)
	{
		m_Wnd_SFR[t].SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stSFROpt[t]);
		m_Wnd_SFR[t].SetUpdateData();
	}

	m_Wnd_SNR_Light.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stSNR_LightOpt);
	m_Wnd_SNR_Light.SetUpdateData();

	m_Wnd_Intensity.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stIntensityOpt);
	m_Wnd_Intensity.SetUpdateData();

	m_Wnd_Shading.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stShadingOpt);
	m_Wnd_Shading.SetUpdateData();

	m_Wnd_Particle.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stParticleOpt);
	m_Wnd_Particle.SetUpdateData();

//#ifdef SET_GWANGJU
	m_Wnd_HotPixel.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stHotPixelOpt);
	m_Wnd_HotPixel.SetUpdateData();

	m_Wnd_3D_Depth.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.st3D_DepthOpt);
	m_Wnd_3D_Depth.SetUpdateData();

	m_Wnd_FPN.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stFPNOpt);
	m_Wnd_FPN.SetUpdateData();

	m_Wnd_EEPROM_Verify.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stEEPROM_VerifyOpt);
	m_Wnd_EEPROM_Verify.SetUpdateData();
//#endif
	m_Wnd_TemperatureSensor.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stTemperatureSensorOpt);
	m_Wnd_TemperatureSensor.SetUpdateData();
	
	m_Wnd_Particle_Entry.SetPtr_RecipeInfo(&pstRecipeInfo->stImageQ.stParticle_EntryOpt);
	m_Wnd_Particle_Entry.SetUpdateData();

	m_wnd_TestItem_EachTest.Set_RecipeInfo(pstRecipeInfo);
}

//=============================================================================
// Method		: Get_RecipeInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_RecipeInfo & stOutRecipInfo
// Qualifier	:
// Last Update	: 2017/11/10 - 20:50
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_ImgT::Get_RecipeInfo(__out ST_RecipeInfo& stOutRecipInfo)
{
	m_Wnd_ECurrent.GetUpdateData();
	m_Wnd_OpticalCenter.GetUpdateData();
	m_Wnd_Rotate.GetUpdateData();
	m_Wnd_Distortion.GetUpdateData();
	m_Wnd_Fov.GetUpdateData();
	m_Wnd_Dynamic.GetUpdateData();
	m_Wnd_DefectPixel.GetUpdateData();
	for (int t = 0; t < SFR_TestNum_MaxEnum; t++)
	{
		m_Wnd_SFR[t].GetUpdateData();
	}
	m_Wnd_SNR_Light.GetUpdateData();
	m_Wnd_Intensity.GetUpdateData();
	m_Wnd_Shading.GetUpdateData();
	m_Wnd_Particle.GetUpdateData();
//#ifdef SET_GWANGJU
	m_Wnd_HotPixel.GetUpdateData();
	m_Wnd_3D_Depth.GetUpdateData();
	m_Wnd_FPN.GetUpdateData();
	m_Wnd_EEPROM_Verify.GetUpdateData();
//#endif
	m_Wnd_TemperatureSensor.GetUpdateData();
	m_Wnd_Particle_Entry.GetUpdateData();
}

//=============================================================================
// Method		: Get_TestItemInfo
// Access		: public  
// Returns		: void
// Parameter	: __in ST_RecipeInfo & stRecipeInfo
// Parameter	: __out ST_TestItemInfo & stOutTestItemInfo
// Qualifier	:
// Last Update	: 2018/2/25 - 17:21
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_ImgT::Get_TestItemInfo(__in ST_RecipeInfo& stRecipeInfo, __out ST_TestItemInfo& stOutTestItemInfo)
{
	ST_TestItemSpec* pSpec = NULL;
	CString szText;
	UINT nCtrlIdx = 0;

//#ifdef SET_GWANGJU
	for (UINT nIdx = enTestItem_ImgT::TI_ImgT_Re_ECurrent; nIdx <= enTestItem_ImgT::TI_ImgT_Re_Particle_Entry; nIdx++)
//#else
	//for (UINT nIdx = enTestItem_ImgT::TI_ImgT_Re_ECurrent; nIdx <= enTestItem_ImgT::TI_ImgT_Re_Shading; nIdx++)
//#endif
	{
		pSpec = &stOutTestItemInfo.TestItemList.GetAt(nIdx);
		ASSERT(NULL != pSpec);

		// 결과 데이터가 1개 초과이고, 개별 Min Max 스펙을 가지는 경우
		if ((1 < pSpec->nResultCount) && (pSpec->bUseMultiSpec))
		{
			pSpec->bUseMinMaxSpec = FALSE;

			for (UINT nArIdx = 0; nArIdx < pSpec->nResultCount; nArIdx++)
			{
				Get_TestItemMinSpec(stRecipeInfo, (enTestItem_ImgT)nIdx, nArIdx, pSpec->Spec[nArIdx].bUseSpecMin, szText);

				if (pSpec->Spec[nArIdx].bUseSpecMin)
				{
					pSpec->bUseMinMaxSpec = TRUE;

					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Min.intVal = _ttoi(szText.GetBuffer(0));
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Min.dblVal = _ttof(szText.GetBuffer(0));
					}
				}
				else
				{
					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Min.intVal = 0;
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Min.dblVal = 0.0f;
					}
				}

				Get_TestItemMaxSpec(stRecipeInfo, (enTestItem_ImgT)nIdx, nArIdx, pSpec->Spec[nArIdx].bUseSpecMax, szText);

				if (pSpec->Spec[nArIdx].bUseSpecMax)
				{
					pSpec->bUseMinMaxSpec = TRUE;

					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Max.intVal = _ttoi(szText.GetBuffer(0));
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Max.dblVal = _ttof(szText.GetBuffer(0));
					}
				}
				else
				{
					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Max.intVal = 0;
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Max.dblVal = 0.0f;
					}
				}

				// 항목 인덱스 증가
				++nCtrlIdx;
			}
		}
		else // 결과 데이터가 동일한 MinMax 스펙을 가지는 경우
		{
			pSpec->bUseMinMaxSpec = FALSE;

			for (UINT nArIdx = 0; nArIdx < pSpec->nResultCount; nArIdx++)
			{
				Get_TestItemMinSpec(stRecipeInfo, (enTestItem_ImgT)nIdx, nArIdx, pSpec->Spec[nArIdx].bUseSpecMin, szText);

				if (pSpec->Spec[nArIdx].bUseSpecMin)
				{
					pSpec->bUseMinMaxSpec = TRUE;

					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Min.intVal = _ttoi(szText.GetBuffer(0));
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Min.dblVal = _ttof(szText.GetBuffer(0));
					}
				}
				else
				{
					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Min.intVal = 0;
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Min.dblVal = 0.0f;
					}
				}

				Get_TestItemMaxSpec(stRecipeInfo, (enTestItem_ImgT)nIdx, nArIdx, pSpec->Spec[nArIdx].bUseSpecMax, szText);

				if (pSpec->Spec[nArIdx].bUseSpecMax)
				{
					pSpec->bUseMinMaxSpec = TRUE;

					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Max.intVal = _ttoi(szText.GetBuffer(0));
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Max.dblVal = _ttof(szText.GetBuffer(0));
					}
				}
				else
				{
					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Max.intVal = 0;
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Max.dblVal = 0.0f;
					}
				}
			}

			// 항목 인덱스 증가
			++nCtrlIdx;
		}
	}
}

//=============================================================================
// Method		: Get_TestItemMinSpec
// Access		: public  
// Returns		: void
// Parameter	: __in ST_RecipeInfo & stRecipeInfo
// Parameter	: __in enTestItem_ImgT enTestItem
// Parameter	: __in UINT nArIdx
// Parameter	: __out BOOL &bUseSpec
// Parameter	: __out CString &szSpec
// Qualifier	:
// Last Update	: 2018/2/25 - 17:22
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_ImgT::Get_TestItemMinSpec(__in ST_RecipeInfo& stRecipeInfo, __in enTestItem_ImgT enTestItem, __in UINT nArIdx, __out BOOL &bUseSpec, __out CString &szSpec)
{
	switch (enTestItem)
	{
	case TI_ImgT_Re_ECurrent:
		bUseSpec = stRecipeInfo.stImageQ.stECurrentOpt.stSpec_Min[nArIdx].bEnable;
		szSpec.Format(_T("%.1f"), stRecipeInfo.stImageQ.stECurrentOpt.stSpec_Min[nArIdx].dbValue);
		break;

	case TI_ImgT_Re_OpticalCenterX:
		bUseSpec = stRecipeInfo.stImageQ.stOpticalCenterOpt.stSpec_Min[Spec_OC_X].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stOpticalCenterOpt.stSpec_Min[Spec_OC_X].iValue);
		break;

	case TI_ImgT_Re_OpticalCenterY:
		bUseSpec = stRecipeInfo.stImageQ.stOpticalCenterOpt.stSpec_Min[Spec_OC_Y].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stOpticalCenterOpt.stSpec_Min[Spec_OC_Y].iValue);
		break;

	case TI_ImgT_Re_FOV_Hor:
		bUseSpec = stRecipeInfo.stImageQ.stFovOpt.stSpec_Min[Spec_Fov_Hor].bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stFovOpt.stSpec_Min[Spec_Fov_Hor].dbValue);
		break;

	case TI_ImgT_Re_FOV_Ver:
		bUseSpec = stRecipeInfo.stImageQ.stFovOpt.stSpec_Min[Spec_Fov_Ver].bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stFovOpt.stSpec_Min[Spec_Fov_Ver].dbValue);
		break;

	case TI_ImgT_Re_SFR:
		bUseSpec = stRecipeInfo.stImageQ.stSFROpt[0].stInput[nArIdx].stSpecMin.bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[0].stInput[nArIdx].stSpecMin.dbValue);
		break;

	case TI_ImgT_Re_Group_SFR:
		bUseSpec = stRecipeInfo.stImageQ.stSFROpt[0].stInput_Group[nArIdx].stSpecMin.bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[0].stInput_Group[nArIdx].stSpecMin.dbValue);
		break;

	case TI_ImgT_Re_Tilt_SFR:
		//bUseSpec = stRecipeInfo.stImageQ.stSFROpt[0].stInput_Tilt[nArIdx].stSpecMax.bEnable;
		//szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[0].stInput_Tilt[nArIdx].stSpecMin.dbValue);
		break;
	//case TI_ImgT_Re_G0_SFR:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[0].stSpec_F_Min[ITM_SFR_G0].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[0].stSpec_F_Min[ITM_SFR_G0].dbValue);
	//	break;

	//case TI_ImgT_Re_G1_SFR:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[0].stSpec_F_Min[ITM_SFR_G1].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[0].stSpec_F_Min[ITM_SFR_G1].dbValue);
	//	break;

	//case TI_ImgT_Re_G2_SFR:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[0].stSpec_F_Min[ITM_SFR_G2].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[0].stSpec_F_Min[ITM_SFR_G2].dbValue);
	//	break;

	//case TI_ImgT_Re_G3_SFR:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[0].stSpec_F_Min[ITM_SFR_G3].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[0].stSpec_F_Min[ITM_SFR_G3].dbValue);
	//	break;

	//case TI_ImgT_Re_G4_SFR:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[0].stSpec_F_Min[ITM_SFR_G4].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[0].stSpec_F_Min[ITM_SFR_G4].dbValue);
	//	break;

	//case TI_ImgT_Re_X1Tilt_SFR:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[0].stSpec_Tilt_Min[ITM_SFR_X1_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[0].stSpec_Tilt_Min[ITM_SFR_X1_Tilt_UpperLimit].dbValue);
	//	break;

	//case TI_ImgT_Re_X2Tilt_SFR:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[0].stSpec_Tilt_Min[ITM_SFR_X2_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[0].stSpec_Tilt_Min[ITM_SFR_X2_Tilt_UpperLimit].dbValue);
	//	break;

	//case TI_ImgT_Re_Y1Tilt_SFR:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[0].stSpec_Tilt_Min[ITM_SFR_Y1_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[0].stSpec_Tilt_Min[ITM_SFR_Y1_Tilt_UpperLimit].dbValue);
	//	break;

	//case TI_ImgT_Re_Y2Tilt_SFR:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[0].stSpec_Tilt_Min[ITM_SFR_Y2_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[0].stSpec_Tilt_Min[ITM_SFR_Y2_Tilt_UpperLimit].dbValue);
	//	break;

	case TI_ImgT_Re_SFR_2:
		bUseSpec = stRecipeInfo.stImageQ.stSFROpt[1].stInput[nArIdx].stSpecMin.bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[1].stInput[nArIdx].stSpecMin.dbValue);
		break;

	case TI_ImgT_Re_Group_SFR_2:
		bUseSpec = stRecipeInfo.stImageQ.stSFROpt[1].stInput_Group[nArIdx].stSpecMin.bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[1].stInput_Group[nArIdx].stSpecMin.dbValue);
		break;

	case TI_ImgT_Re_Tilt_SFR_2:
		//bUseSpec = stRecipeInfo.stImageQ.stSFROpt[0].stInput_Tilt[nArIdx].stSpecMax.bEnable;
		//szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[0].stInput_Tilt[nArIdx].stSpecMin.dbValue);
		break;
	//case TI_ImgT_Re_G0_SFR_2:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[1].stSpec_F_Min[ITM_SFR_G0].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[1].stSpec_F_Min[ITM_SFR_G0].dbValue);
	//	break;

	//case TI_ImgT_Re_G1_SFR_2:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[1].stSpec_F_Min[ITM_SFR_G1].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[1].stSpec_F_Min[ITM_SFR_G1].dbValue);
	//	break;

	//case TI_ImgT_Re_G2_SFR_2:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[1].stSpec_F_Min[ITM_SFR_G2].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[1].stSpec_F_Min[ITM_SFR_G2].dbValue);
	//	break;

	//case TI_ImgT_Re_G3_SFR_2:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[1].stSpec_F_Min[ITM_SFR_G3].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[1].stSpec_F_Min[ITM_SFR_G3].dbValue);
	//	break;

	//case TI_ImgT_Re_G4_SFR_2:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[1].stSpec_F_Min[ITM_SFR_G4].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[1].stSpec_F_Min[ITM_SFR_G4].dbValue);
	//	break;

	//case TI_ImgT_Re_X1Tilt_SFR_2:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[1].stSpec_Tilt_Min[ITM_SFR_X1_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[1].stSpec_Tilt_Min[ITM_SFR_X1_Tilt_UpperLimit].dbValue);
	//	break;

	//case TI_ImgT_Re_X2Tilt_SFR_2:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[1].stSpec_Tilt_Min[ITM_SFR_X2_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[1].stSpec_Tilt_Min[ITM_SFR_X2_Tilt_UpperLimit].dbValue);
	//	break;

	//case TI_ImgT_Re_Y1Tilt_SFR_2:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[1].stSpec_Tilt_Min[ITM_SFR_Y1_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[1].stSpec_Tilt_Min[ITM_SFR_Y1_Tilt_UpperLimit].dbValue);
	//	break;

	//case TI_ImgT_Re_Y2Tilt_SFR_2:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[1].stSpec_Tilt_Min[ITM_SFR_Y2_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[1].stSpec_Tilt_Min[ITM_SFR_Y2_Tilt_UpperLimit].dbValue);
	//	break;

	case TI_ImgT_Re_SFR_3:
		bUseSpec = stRecipeInfo.stImageQ.stSFROpt[2].stInput[nArIdx].stSpecMin.bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[2].stInput[nArIdx].stSpecMin.dbValue);
		break;

	case TI_ImgT_Re_Group_SFR_3:
		bUseSpec = stRecipeInfo.stImageQ.stSFROpt[2].stInput_Group[nArIdx].stSpecMin.bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[2].stInput_Group[nArIdx].stSpecMin.dbValue);
		break;

	case TI_ImgT_Re_Tilt_SFR_3:
		//bUseSpec = stRecipeInfo.stImageQ.stSFROpt[0].stInput_Tilt[nArIdx].stSpecMax.bEnable;
		//szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[0].stInput_Tilt[nArIdx].stSpecMin.dbValue);
		break;

	//case TI_ImgT_Re_G0_SFR_3:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[2].stSpec_F_Min[ITM_SFR_G0].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[2].stSpec_F_Min[ITM_SFR_G0].dbValue);
	//	break;

	//case TI_ImgT_Re_G1_SFR_3:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[2].stSpec_F_Min[ITM_SFR_G1].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[2].stSpec_F_Min[ITM_SFR_G1].dbValue);
	//	break;

	//case TI_ImgT_Re_G2_SFR_3:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[2].stSpec_F_Min[ITM_SFR_G2].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[2].stSpec_F_Min[ITM_SFR_G2].dbValue);
	//	break;

	//case TI_ImgT_Re_G3_SFR_3:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[2].stSpec_F_Min[ITM_SFR_G3].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[2].stSpec_F_Min[ITM_SFR_G3].dbValue);
	//	break;

	//case TI_ImgT_Re_G4_SFR_3:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[2].stSpec_F_Min[ITM_SFR_G4].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[2].stSpec_F_Min[ITM_SFR_G4].dbValue);
	//	break;

	//case TI_ImgT_Re_X1Tilt_SFR_3:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[2].stSpec_Tilt_Min[ITM_SFR_X1_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[2].stSpec_Tilt_Min[ITM_SFR_X1_Tilt_UpperLimit].dbValue);
	//	break;

	//case TI_ImgT_Re_X2Tilt_SFR_3:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[2].stSpec_Tilt_Min[ITM_SFR_X2_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[2].stSpec_Tilt_Min[ITM_SFR_X2_Tilt_UpperLimit].dbValue);
	//	break;

	//case TI_ImgT_Re_Y1Tilt_SFR_3:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[2].stSpec_Tilt_Min[ITM_SFR_Y1_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[2].stSpec_Tilt_Min[ITM_SFR_Y1_Tilt_UpperLimit].dbValue);
	//	break;

	//case TI_ImgT_Re_Y2Tilt_SFR_3:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[2].stSpec_Tilt_Min[ITM_SFR_Y2_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[2].stSpec_Tilt_Min[ITM_SFR_Y2_Tilt_UpperLimit].dbValue);
	//	break;

	case TI_ImgT_Re_Distortion:
		bUseSpec = stRecipeInfo.stImageQ.stDistortionOpt.stSpec_Min.bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stDistortionOpt.stSpec_Min.dbValue);
		break;

	case TI_ImgT_Re_Rotation:
		bUseSpec = stRecipeInfo.stImageQ.stRotateOpt.stSpec_Min.bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stRotateOpt.stSpec_Min.dbValue);
		break;

	case TI_ImgT_Re_DynamicRange:
		if (stRecipeInfo.stImageQ.stDynamicOpt.stRegion[nArIdx].bEnable == TRUE)
		{
			bUseSpec = stRecipeInfo.stImageQ.stDynamicOpt.stSpec_Min[Spec_Par_SNR_Dynamic].bEnable;
			szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stDynamicOpt.stSpec_Min[Spec_Par_SNR_Dynamic].dbValue);
			break;
		}
		else
		{
			bUseSpec = FALSE;
			szSpec.Format(_T("0.00"));
		}

	case TI_ImgT_Re_SNR_BW:
		if (stRecipeInfo.stImageQ.stDynamicOpt.stRegion[nArIdx].bEnable == TRUE)
		{
			bUseSpec = stRecipeInfo.stImageQ.stDynamicOpt.stSpec_Min[Spec_Par_SNR_SNRBW].bEnable;
			szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stDynamicOpt.stSpec_Min[Spec_Par_SNR_SNRBW].dbValue);
			break;
		}
		else
		{
			bUseSpec = FALSE;
			szSpec.Format(_T("0.00"));
		}

	case TI_ImgT_Re_Stain:
		bUseSpec = stRecipeInfo.stImageQ.stParticleOpt.stSpec_Min.bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stParticleOpt.stSpec_Min.iValue);
		break;

	case TI_ImgT_Re_DefectPixel:
		bUseSpec = stRecipeInfo.stImageQ.stDefectPixelOpt.stSpec_Min[nArIdx].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stDefectPixelOpt.stSpec_Min[nArIdx].iValue);
		break;

	case TI_ImgT_Re_Intencity:
		bUseSpec = stRecipeInfo.stImageQ.stIntensityOpt.stSpec_Min[nArIdx].bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stIntensityOpt.stSpec_Min[nArIdx].dbValue);
		break;

	case TI_ImgT_Re_Shading:
		bUseSpec = stRecipeInfo.stImageQ.stShadingOpt.stSpec_Min[nArIdx].bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stShadingOpt.stSpec_Min[nArIdx].dbValue);
		break;

	case TI_ImgT_Re_SNR_Light:
		bUseSpec = stRecipeInfo.stImageQ.stSNR_LightOpt.stSpec_Min.bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSNR_LightOpt.stSpec_Min.dbValue);
		break;

//#ifdef SET_GWANGJU
	case TI_ImgT_Re_Hot_Pixel:
		bUseSpec = stRecipeInfo.stImageQ.stHotPixelOpt.stSpec_Min.bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stHotPixelOpt.stSpec_Min.iValue);
		break;

	case TI_ImgT_Re_Fixed_Pattern:
		bUseSpec = stRecipeInfo.stImageQ.stFPNOpt.stSpec_Min[nArIdx].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stFPNOpt.stSpec_Min[nArIdx].iValue);
		break;

	case TI_ImgT_Re_3D_Depth:
		bUseSpec = stRecipeInfo.stImageQ.st3D_DepthOpt.stSpec_Min[nArIdx].bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.st3D_DepthOpt.stSpec_Min[nArIdx].dbValue);
		break;

	case TI_ImgT_Re_EEPROM_Verify:
		bUseSpec = stRecipeInfo.stImageQ.stEEPROM_VerifyOpt.stSpec_Min[nArIdx].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stEEPROM_VerifyOpt.stSpec_Min[nArIdx].iValue);
		break;
//#endif
	case TI_ImgT_Re_TemperatureSensor:
		bUseSpec = stRecipeInfo.stImageQ.stTemperatureSensorOpt.stSpec_Min[nArIdx].bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stTemperatureSensorOpt.stSpec_Min[nArIdx].dbValue);
		break;

	case TI_ImgT_Re_Particle_Entry:
		bUseSpec = stRecipeInfo.stImageQ.stParticle_EntryOpt.stSpec_Min.bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stParticle_EntryOpt.stSpec_Min.iValue);
		break;
		
	default:
		break;
	}
}

//=============================================================================
// Method		: Get_TestItemMaxSpec
// Access		: public  
// Returns		: void
// Parameter	: __in ST_RecipeInfo & stRecipeInfo
// Parameter	: __in enTestItem_ImgT enTestItem
// Parameter	: __in UINT nArIdx
// Parameter	: __out BOOL &bUseSpec
// Parameter	: __out CString &szSpec
// Qualifier	:
// Last Update	: 2018/2/25 - 17:22
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_ImgT::Get_TestItemMaxSpec(__in ST_RecipeInfo& stRecipeInfo, __in enTestItem_ImgT enTestItem, __in UINT nArIdx, __out BOOL &bUseSpec, __out CString &szSpec)
{
	switch (enTestItem)
	{
	case TI_ImgT_Re_ECurrent:
		bUseSpec = stRecipeInfo.stImageQ.stECurrentOpt.stSpec_Max[nArIdx].bEnable;
		szSpec.Format(_T("%.1f"), stRecipeInfo.stImageQ.stECurrentOpt.stSpec_Max[nArIdx].dbValue);
		break;

	case TI_ImgT_Re_OpticalCenterX:
		bUseSpec = stRecipeInfo.stImageQ.stOpticalCenterOpt.stSpec_Max[Spec_OC_X].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stOpticalCenterOpt.stSpec_Max[Spec_OC_X].iValue);
		break;

	case TI_ImgT_Re_OpticalCenterY:
		bUseSpec = stRecipeInfo.stImageQ.stOpticalCenterOpt.stSpec_Max[Spec_OC_Y].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stOpticalCenterOpt.stSpec_Max[Spec_OC_Y].iValue);
		break;

	case TI_ImgT_Re_FOV_Hor:
		bUseSpec = stRecipeInfo.stImageQ.stFovOpt.stSpec_Max[Spec_Fov_Hor].bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stFovOpt.stSpec_Max[Spec_Fov_Hor].dbValue);
		break;

	case TI_ImgT_Re_FOV_Ver:
		bUseSpec = stRecipeInfo.stImageQ.stFovOpt.stSpec_Max[Spec_Fov_Ver].bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stFovOpt.stSpec_Max[Spec_Fov_Ver].dbValue);
		break;

	case TI_ImgT_Re_SFR:
		bUseSpec = stRecipeInfo.stImageQ.stSFROpt[0].stInput[nArIdx].stSpecMax.bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[0].stInput[nArIdx].stSpecMax.dbValue);
		break;

	case TI_ImgT_Re_Group_SFR:
		bUseSpec = stRecipeInfo.stImageQ.stSFROpt[0].stInput_Group[nArIdx].stSpecMax.bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[0].stInput_Group[nArIdx].stSpecMax.dbValue);
		break;

	case TI_ImgT_Re_Tilt_SFR:
		bUseSpec = stRecipeInfo.stImageQ.stSFROpt[0].stInput_Tilt[nArIdx].bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[0].stInput_Tilt[nArIdx].dbValue);
		break;

	//case TI_ImgT_Re_G0_SFR:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[0].stSpec_F_Max[ITM_SFR_G0].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[0].stSpec_F_Max[ITM_SFR_G0].dbValue);
	//	break;

	//case TI_ImgT_Re_G1_SFR:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[0].stSpec_F_Max[ITM_SFR_G1].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[0].stSpec_F_Max[ITM_SFR_G1].dbValue);
	//	break;

	//case TI_ImgT_Re_G2_SFR:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[0].stSpec_F_Max[ITM_SFR_G2].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[0].stSpec_F_Max[ITM_SFR_G2].dbValue);
	//	break;

	//case TI_ImgT_Re_G3_SFR:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[0].stSpec_F_Max[ITM_SFR_G3].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[0].stSpec_F_Max[ITM_SFR_G3].dbValue);
	//	break;

	//case TI_ImgT_Re_G4_SFR:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[0].stSpec_F_Max[ITM_SFR_G4].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[0].stSpec_F_Max[ITM_SFR_G4].dbValue);
	//	break;
	//case TI_ImgT_Re_X1Tilt_SFR:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[0].stSpec_Tilt_Max[ITM_SFR_X1_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[0].stSpec_Tilt_Max[ITM_SFR_X1_Tilt_UpperLimit].dbValue);
	//	break;

	//case TI_ImgT_Re_X2Tilt_SFR:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[0].stSpec_Tilt_Max[ITM_SFR_X2_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[0].stSpec_Tilt_Max[ITM_SFR_X2_Tilt_UpperLimit].dbValue);
	//	break;

	//case TI_ImgT_Re_Y1Tilt_SFR:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[0].stSpec_Tilt_Max[ITM_SFR_Y1_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[0].stSpec_Tilt_Max[ITM_SFR_Y1_Tilt_UpperLimit].dbValue);
	//	break;

	//case TI_ImgT_Re_Y2Tilt_SFR:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[0].stSpec_Tilt_Max[ITM_SFR_Y2_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[0].stSpec_Tilt_Max[ITM_SFR_Y2_Tilt_UpperLimit].dbValue);
	//	break;

	case TI_ImgT_Re_SFR_2:
		bUseSpec = stRecipeInfo.stImageQ.stSFROpt[1].stInput[nArIdx].stSpecMax.bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[1].stInput[nArIdx].stSpecMax.dbValue);
		break;

	case TI_ImgT_Re_Group_SFR_2:
		bUseSpec = stRecipeInfo.stImageQ.stSFROpt[1].stInput_Group[nArIdx].stSpecMax.bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[1].stInput_Group[nArIdx].stSpecMax.dbValue);
		break;

	case TI_ImgT_Re_Tilt_SFR_2:
		bUseSpec = stRecipeInfo.stImageQ.stSFROpt[1].stInput_Tilt[nArIdx].bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[1].stInput_Tilt[nArIdx].dbValue);
		break;
	//case TI_ImgT_Re_G0_SFR_2:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[1].stSpec_F_Max[ITM_SFR_G0].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[1].stSpec_F_Max[ITM_SFR_G0].dbValue);
	//	break;

	//case TI_ImgT_Re_G1_SFR_2:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[1].stSpec_F_Max[ITM_SFR_G1].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[1].stSpec_F_Max[ITM_SFR_G1].dbValue);
	//	break;

	//case TI_ImgT_Re_G2_SFR_2:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[1].stSpec_F_Max[ITM_SFR_G2].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[1].stSpec_F_Max[ITM_SFR_G2].dbValue);
	//	break;

	//case TI_ImgT_Re_G3_SFR_2:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[1].stSpec_F_Max[ITM_SFR_G3].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[1].stSpec_F_Max[ITM_SFR_G3].dbValue);
	//	break;

	//case TI_ImgT_Re_G4_SFR_2:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[1].stSpec_F_Max[ITM_SFR_G4].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[1].stSpec_F_Max[ITM_SFR_G4].dbValue);
	//	break;
	//case TI_ImgT_Re_X1Tilt_SFR_2:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[1].stSpec_Tilt_Max[ITM_SFR_X1_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[1].stSpec_Tilt_Max[ITM_SFR_X1_Tilt_UpperLimit].dbValue);
	//	break;

	//case TI_ImgT_Re_X2Tilt_SFR_2:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[1].stSpec_Tilt_Max[ITM_SFR_X2_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[1].stSpec_Tilt_Max[ITM_SFR_X2_Tilt_UpperLimit].dbValue);
	//	break;

	//case TI_ImgT_Re_Y1Tilt_SFR_2:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[1].stSpec_Tilt_Max[ITM_SFR_Y1_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[1].stSpec_Tilt_Max[ITM_SFR_Y1_Tilt_UpperLimit].dbValue);
	//	break;

	//case TI_ImgT_Re_Y2Tilt_SFR_2:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[1].stSpec_Tilt_Max[ITM_SFR_Y2_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[1].stSpec_Tilt_Max[ITM_SFR_Y2_Tilt_UpperLimit].dbValue);
	//	break;

	case TI_ImgT_Re_SFR_3:
		bUseSpec = stRecipeInfo.stImageQ.stSFROpt[2].stInput[nArIdx].stSpecMax.bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[2].stInput[nArIdx].stSpecMax.dbValue);
		break;

	case TI_ImgT_Re_Group_SFR_3:
		bUseSpec = stRecipeInfo.stImageQ.stSFROpt[2].stInput_Group[nArIdx].stSpecMax.bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[2].stInput_Group[nArIdx].stSpecMax.dbValue);
		break;

	case TI_ImgT_Re_Tilt_SFR_3:
		bUseSpec = stRecipeInfo.stImageQ.stSFROpt[2].stInput_Tilt[nArIdx].bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[2].stInput_Tilt[nArIdx].dbValue);
		break;

	//case TI_ImgT_Re_G0_SFR_3:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[2].stSpec_F_Max[ITM_SFR_G0].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[2].stSpec_F_Max[ITM_SFR_G0].dbValue);
	//	break;

	//case TI_ImgT_Re_G1_SFR_3:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[2].stSpec_F_Max[ITM_SFR_G1].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[2].stSpec_F_Max[ITM_SFR_G1].dbValue);
	//	break;

	//case TI_ImgT_Re_G2_SFR_3:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[2].stSpec_F_Max[ITM_SFR_G2].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[2].stSpec_F_Max[ITM_SFR_G2].dbValue);
	//	break;

	//case TI_ImgT_Re_G3_SFR_3:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[2].stSpec_F_Max[ITM_SFR_G3].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[2].stSpec_F_Max[ITM_SFR_G3].dbValue);
	//	break;

	//case TI_ImgT_Re_G4_SFR_3:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[2].stSpec_F_Max[ITM_SFR_G4].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[2].stSpec_F_Max[ITM_SFR_G4].dbValue);
	//	break;
	//case TI_ImgT_Re_X1Tilt_SFR_3:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[2].stSpec_Tilt_Max[ITM_SFR_X1_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[2].stSpec_Tilt_Max[ITM_SFR_X1_Tilt_UpperLimit].dbValue);
	//	break;

	//case TI_ImgT_Re_X2Tilt_SFR_3:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[2].stSpec_Tilt_Max[ITM_SFR_X2_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[2].stSpec_Tilt_Max[ITM_SFR_X2_Tilt_UpperLimit].dbValue);
	//	break;

	//case TI_ImgT_Re_Y1Tilt_SFR_3:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[2].stSpec_Tilt_Max[ITM_SFR_Y1_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[2].stSpec_Tilt_Max[ITM_SFR_Y1_Tilt_UpperLimit].dbValue);
	//	break;

	//case TI_ImgT_Re_Y2Tilt_SFR_3:
	//	bUseSpec = stRecipeInfo.stImageQ.stSFROpt[2].stSpec_Tilt_Max[ITM_SFR_Y2_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSFROpt[2].stSpec_Tilt_Max[ITM_SFR_Y2_Tilt_UpperLimit].dbValue);
	//	break;

	case TI_ImgT_Re_Distortion:
		bUseSpec = stRecipeInfo.stImageQ.stDistortionOpt.stSpec_Max.bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stDistortionOpt.stSpec_Max.dbValue);
		break;

	case TI_ImgT_Re_Rotation:
		bUseSpec = stRecipeInfo.stImageQ.stRotateOpt.stSpec_Max.bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stRotateOpt.stSpec_Max.dbValue);
		break;

	case TI_ImgT_Re_DynamicRange:
		if (stRecipeInfo.stImageQ.stDynamicOpt.stRegion[nArIdx].bEnable == TRUE)
		{
			bUseSpec = stRecipeInfo.stImageQ.stDynamicOpt.stSpec_Max[Spec_Par_SNR_Dynamic].bEnable;
			szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stDynamicOpt.stSpec_Max[Spec_Par_SNR_Dynamic].dbValue);
		}
		else
		{
			bUseSpec = FALSE;
			szSpec.Format(_T("0.00"));
		}
		break;

	case TI_ImgT_Re_SNR_BW:
		if (stRecipeInfo.stImageQ.stDynamicOpt.stRegion[nArIdx].bEnable == TRUE)
		{
			bUseSpec = stRecipeInfo.stImageQ.stDynamicOpt.stSpec_Max[Spec_Par_SNR_SNRBW].bEnable;
			szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stDynamicOpt.stSpec_Max[Spec_Par_SNR_SNRBW].dbValue);
			break;
		}
		else
		{
			bUseSpec = FALSE;
			szSpec.Format(_T("0.00"));
		}

	case TI_ImgT_Re_Stain:
		bUseSpec = stRecipeInfo.stImageQ.stParticleOpt.stSpec_Max.bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stParticleOpt.stSpec_Max.iValue);
		break;

	case TI_ImgT_Re_DefectPixel:
		bUseSpec = stRecipeInfo.stImageQ.stDefectPixelOpt.stSpec_Max[nArIdx].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stDefectPixelOpt.stSpec_Max[nArIdx].iValue);
		break;

	case TI_ImgT_Re_Intencity:
		bUseSpec = stRecipeInfo.stImageQ.stIntensityOpt.stSpec_Max[nArIdx].bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stIntensityOpt.stSpec_Max[nArIdx].dbValue);
		break;

	case TI_ImgT_Re_Shading:
		bUseSpec = stRecipeInfo.stImageQ.stShadingOpt.stSpec_Max[nArIdx].bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stShadingOpt.stSpec_Max[nArIdx].dbValue);
		break;

	case TI_ImgT_Re_SNR_Light:
		bUseSpec = stRecipeInfo.stImageQ.stSNR_LightOpt.stSpec_Max.bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stSNR_LightOpt.stSpec_Max.dbValue);
		break;

//#ifdef SET_GWANGJU
	case TI_ImgT_Re_Hot_Pixel:
		bUseSpec = stRecipeInfo.stImageQ.stHotPixelOpt.stSpec_Max.bEnable;
		szSpec.Format(_T("%.d"), stRecipeInfo.stImageQ.stHotPixelOpt.stSpec_Max.iValue);
		break;

	case TI_ImgT_Re_Fixed_Pattern:
		bUseSpec = stRecipeInfo.stImageQ.stFPNOpt.stSpec_Max[nArIdx].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stFPNOpt.stSpec_Max[nArIdx].iValue);
		break;

	case TI_ImgT_Re_3D_Depth:
		bUseSpec = stRecipeInfo.stImageQ.st3D_DepthOpt.stSpec_Max[nArIdx].bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.st3D_DepthOpt.stSpec_Max[nArIdx].dbValue);
		break;

	case TI_ImgT_Re_EEPROM_Verify:
		bUseSpec = stRecipeInfo.stImageQ.stEEPROM_VerifyOpt.stSpec_Max[nArIdx].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stEEPROM_VerifyOpt.stSpec_Max[nArIdx].iValue);
		break;
//#endif
	case TI_ImgT_Re_TemperatureSensor:
		bUseSpec = stRecipeInfo.stImageQ.stTemperatureSensorOpt.stSpec_Max[nArIdx].bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stImageQ.stTemperatureSensorOpt.stSpec_Max[nArIdx].dbValue);
		break;
	case TI_ImgT_Re_Particle_Entry:
		bUseSpec = stRecipeInfo.stImageQ.stParticle_EntryOpt.stSpec_Max.bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stImageQ.stParticle_EntryOpt.stSpec_Max.iValue);
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: OnEnSetfocusTabFocus
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/7 - 19:16
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_ImgT::OnEnSetfocusTabFocus()
{
	GetOwner()->SendNotifyMessage(WM_CHANGE_OPTIONPIC,0, 0);
}