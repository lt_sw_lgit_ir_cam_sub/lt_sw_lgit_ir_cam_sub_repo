﻿
// LT MainExecution.cpp : 응용 프로그램에 대한 클래스 동작을 정의합니다.
//

#include "stdafx.h"
#include "afxwinappex.h"
#include "LT MainExecution.h"
#include "MainFrm.h"
#include "SubFrame.h"
#include "LimitSingleInstance.h"
#include "Rockey.h"

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//#define DEBUG_NORMALBLOCK new (_NORMAL_BLOCK, __FILE__, __LINE__)
//#ifdef new
//#undef new
//#endif
//#define new DEBUG_NORMALBLOCK

//=============================================================================
// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.
//=============================================================================
class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

//=============================================================================
// CNTInspectionSystemApp
//=============================================================================
BEGIN_MESSAGE_MAP(CNTInspectionSystemApp, CWinAppEx)
	ON_COMMAND(ID_APP_ABOUT, &CNTInspectionSystemApp::OnAppAbout)
END_MESSAGE_MAP()


// CNTInspectionSystemApp 생성
//=============================================================================
//
//=============================================================================
CNTInspectionSystemApp::CNTInspectionSystemApp()
{
	// Registry의 workspace에 데이터 저장 안함
	m_bSaveState = FALSE;

	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	_CrtSetReportMode(_CRT_WARN, _CRTDBG_MODE_DEBUG);
	_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_DEBUG);
	_CrtSetReportMode(_CRT_ASSERT, _CRTDBG_MODE_DEBUG);
	_CrtDumpMemoryLeaks();
		
	//_CrtSetBreakAlloc(1063);	
	//_CrtSetBreakAlloc(1061);
	_CrtMemDumpAllObjectsSince(0);

	m_bHiColorIcons = TRUE;

	Gdiplus::GdiplusStartup(&m_gdiplusToken, &m_gdiplusStartupInput, NULL);
}

//=============================================================================
//
//=============================================================================
CNTInspectionSystemApp::~CNTInspectionSystemApp()
{
	TRACE(_T("<<< Start ~CNTInspectionSystemApp >>> \n"));

	Gdiplus::GdiplusShutdown(m_gdiplusToken);

	//_CrtSetBreakAlloc(1063);
	_CrtDumpMemoryLeaks();

	TRACE(_T("<<< End ~CNTInspectionSystemApp >>> \n"));
}

// 유일한 CNTInspectionSystemApp 개체입니다.

CNTInspectionSystemApp theApp;
CLimitSingleInstance g_SingleInstanceObj(TEXT("Global\\{5A5AFC2E-D2DC-4D38-89B0-AEBF8B95D67F}"));

// CNTInspectionSystemApp 초기화

//=============================================================================
// Method		: InitInstance
// Access		: virtual public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/9/22 - 14:32
// Desc.		:
//=============================================================================
BOOL CNTInspectionSystemApp::InitInstance()
{
#ifndef USE_ANOTHER_INSTANCE_APP
	if (g_SingleInstanceObj.IsAnotherInstanceRunning())
		return FALSE; 
#endif

#ifdef USE_HW_LOCK_KEY
	// 락키 체크용 루틴
	CRockey Rockey;
	if (!Rockey.CheckRockey(ROCKEY_MODE0))
	{
	 	return FALSE;
	}
#endif

	SetLFH(); // Low fragmentation Heap 적용

	// 응용 프로그램 매니페스트가 ComCtl32.dll 버전 6 이상을 사용하여 비주얼 스타일을
	// 사용하도록 지정하는 경우, Windows XP 상에서 반드시 InitCommonControlsEx()가 필요합니다. 
	// InitCommonControlsEx()를 사용하지 않으면 창을 만들 수 없습니다.
// 	INITCOMMONCONTROLSEX InitCtrls;
// 	InitCtrls.dwSize = sizeof(InitCtrls);
	// 응용 프로그램에서 사용할 모든 공용 컨트롤 클래스를 포함하도록
	// 이 항목을 설정하십시오.
// 	InitCtrls.dwICC = ICC_WIN95_CLASSES;
// 	InitCommonControlsEx(&InitCtrls);

	CWinAppEx::InitInstance();

 	if (!AfxSocketInit())
 	{
 		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
 		return FALSE;
 	}

	// OLE 라이브러리를 초기화합니다.
// 	if (!AfxOleInit())
// 	{
// 		AfxMessageBox(IDP_OLE_INIT_FAILED);
// 		return FALSE;
// 	}
	AfxEnableControlContainer();
	// 표준 초기화
	// 이들 기능을 사용하지 않고 최종 실행 파일의 크기를 줄이려면
	// 아래에서 필요 없는 특정 초기화
	// 루틴을 제거해야 합니다.
	// 해당 설정이 저장된 레지스트리 키를 변경하십시오.
	// TODO: 이 문자열을 회사 또는 조직의 이름과 같은
	// 적절한 내용으로 수정해야 합니다.
	SetRegistryKey(_T("Luritech"));

//	InitContextMenuManager();

//	InitKeyboardManager();

// 	InitTooltipManager();
// 	CMFCToolTipInfo ttParams;
// 	ttParams.m_bVislManagerTheme	= TRUE;
// 	ttParams.m_bBoldLabel			= TRUE;
// 	ttParams.m_bDrawDescription		= TRUE;
// 	ttParams.m_bDrawIcon			= TRUE;
// 	ttParams.m_bRoundedCorners		= TRUE;
// 	ttParams.m_bDrawSeparator		= TRUE;
// 	ttParams.m_clrFill				= RGB (255, 255, 255);
// 	ttParams.m_clrFillGradient		= RGB (228, 228, 240);
// 	ttParams.m_clrText				= RGB (61, 83, 80);
// 	ttParams.m_clrBorder			= RGB (144, 149, 168);
// 	theApp.GetTooltipManager()->SetTooltipParams(AFX_TOOLTIP_TYPE_ALL, RUNTIME_CLASS(CMFCToolTipCtrl), &ttParams);

	// 주 창을 만들기 위해 이 코드에서는 새 프레임 창 개체를
	// 만든 다음 이를 응용 프로그램의 주 창 개체로 설정합니다.
	CMainFrame* pFrame = new CMainFrame;
	if (!pFrame)
		return FALSE;
	m_pMainWnd = pFrame;

	//검사기 종류 설정
	pFrame->SetSystemType((enInsptrSysType)SET_INSPECTOR);

	// 프레임을 만들어 리소스와 함께 로드합니다.
	//pFrame->LoadFrame(IDR_MAINFRAME, WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, NULL, NULL);
	pFrame->LoadFrame(IDR_MAINFRAME, WS_OVERLAPPED | WS_SYSMENU | WS_THICKFRAME | WS_BORDER | WS_MINIMIZEBOX | WS_MAXIMIZEBOX & ~FWS_ADDTOTITLE & ~WS_CAPTION, NULL, NULL);
	
	// 창 하나만 초기화되었으므로 이를 표시하고 업데이트합니다.
	pFrame->ShowWindow(SW_SHOWMAXIMIZED);
	pFrame->UpdateWindow();
	// 접미사가 있을 경우에만 DragAcceptFiles를 호출합니다.
	//  SDI 응용 프로그램에서는 ProcessShellCommand 후에 이러한 호출이 발생해야 합니다.

	// 프로그램 로딩이 끝났을을 메세지로 알림??
	pFrame->PostMessage(WM_LOAD_COMPLETE, 0, 0);

// #if (SET_INSPECTOR == SYS_IMAGE_TEST)
// 	CSubFrame* pSubFrame = new CSubFrame;
// 	if (!pSubFrame)
// 		return FALSE;	
// 
// 	//검사기 종류 설정
// 	pSubFrame->SetSystemType(enInsptrSysType::Sys_Stereo_Cal);
// 
// 	// 프레임을 만들어 리소스와 함께 로드합니다.
// 	pSubFrame->LoadFrame(IDR_MAINFRAME, WS_OVERLAPPED | WS_SYSMENU | WS_THICKFRAME | WS_BORDER | WS_MINIMIZEBOX | WS_MAXIMIZEBOX & ~FWS_ADDTOTITLE & ~WS_CAPTION, NULL, NULL);
// 
// 	// 창 하나만 초기화되었으므로 이를 표시하고 업데이트합니다.
// 	pSubFrame->ShowWindow(SW_SHOWMAXIMIZED);
// 	pSubFrame->UpdateWindow();	
// 
// 	// 프로그램 로딩이 끝났을을 메세지로 알림??
// 	pSubFrame->PostMessage(WM_LOAD_COMPLETE, 0, 0);
// #endif

	return TRUE;
}

// CNTInspectionSystemApp 메시지 처리기

// 대화 상자를 실행하기 위한 응용 프로그램 명령입니다.
void CNTInspectionSystemApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

// CNTInspectionSystemApp 사용자 지정 로드/저장 메서드

void CNTInspectionSystemApp::PreLoadState()
{
	/*BOOL bNameValid;
	CString strName;
	bNameValid = strName.LoadString(IDS_EDIT_MENU);
	ASSERT(bNameValid);
	GetContextMenuManager()->AddMenu(strName, IDR_POPUP_EDIT);*/
}

void CNTInspectionSystemApp::LoadCustomState()
{
}

void CNTInspectionSystemApp::SaveCustomState()
{
}

void CNTInspectionSystemApp::SetLFH()
{
	HANDLE Heaps[1024];
	DWORD Count = GetProcessHeaps( 1024, Heaps );
	for( DWORD i = 0; i < Count; ++i )
	{
		ULONG  HeapFragValue = 2;

		if( HeapSetInformation( Heaps[i], HeapCompatibilityInformation, &HeapFragValue, sizeof(HeapFragValue) ) )
		{
			//.............
		}
		else 
		{
			//...................
		}
	}
}
// CNTInspectionSystemApp 메시지 처리기



