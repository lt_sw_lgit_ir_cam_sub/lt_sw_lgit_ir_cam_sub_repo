﻿#ifndef Wnd_Progress_h__
#define Wnd_Progress_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "VGProgressBar_Variable.h"

#include "Def_T_Focus.h"

//-----------------------------------------------------------------------------
// CWnd_Progress
//-----------------------------------------------------------------------------
class CWnd_Progress : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Progress)

public:
	CWnd_Progress();
	virtual ~CWnd_Progress();
	

	enum enProgStatic
	{
		STI_PG_NAME_P1 = 0,
		STI_PG_NAME_P2,
		STI_PG_MAX,
	};
	enum enProgstateStatic
	{
		STI_PG_MAX_P1 = 0,
		STI_PG_MAX_P2,
		STI_PG_Current_P1,
		STI_PG_Current_P2,
		STI_PG_stateMAX,
	};

	enum enProgProgress
	{
		PRG_PG_P1,
		PRG_PG_P2,
		PRG_PG_MAX,
	};
	enum enProgResultStatic
	{
		STI_PG_MAX_P1_Result,
		STI_PG_MAX_P2_Result,
		STI_PG_Current_P1_Result,
		STI_PG_Current_P2_Result,
		STI_PG_Result_MAX,
	};
protected:
	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	CFont						m_font;

	CVGStatic					m_st_Item[STI_PG_MAX];
	CVGProgressBar_Variable		m_pg_Item[PRG_PG_MAX];
	CVGStatic					m_st_Result_Item[STI_PG_Result_MAX];
	CVGStatic					m_st_stateItem[STI_PG_stateMAX];

	ST_Focus_Opt*				m_pstConfigInfo = NULL;

public:

	void	SetPtr_RecipeInfo(__in ST_Focus_Opt* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstConfigInfo = pstRecipeInfo;
	};

	void	SetUpdateMax	(__in ST_Focus_Data stData);
	void	SetUpdateBest	(__in ST_Focus_Data stData);
	void	SetUpdateSpec	();
	void	SetResetData	();
};

#endif // Wnd_Leak_h__
