﻿#ifndef Wnd_TestResult_Foc_h__
#define Wnd_TestResult_Foc_h__

#pragma once

#include "VGStatic.h"
#include "CommonFunction.h"
#include "Def_DataStruct_Cm.h"

#include "Wnd_CurrentData.h"
#include "Wnd_DefectPixelData.h"
#include "Wnd_OpticalData.h"
#include "Wnd_RotateData.h"
#include "Wnd_SFRData.h"
#include "Wnd_ParticleData.h"
#include "Wnd_TorqueData.h"
#include "Wnd_ActiveAlignData.h"

// CWnd_TestResult_Foc
enum enTestResult_Foc_Static
{
	STI_TR_Foc_Title,
	STI_TR_Foc_MAX,
};

static LPCTSTR	g_szTestResult_Foc_Static[] =
{
	_T("TEST RESULT"),
	NULL
};
enum enTestResult_Foc_Item
{
	TR_Foc_ECurrent,
	TR_Foc_OpticalCenter,
	TR_Foc_SFR,
	TR_Foc_Rotation,
	TR_Foc_DefectPixel,
	TR_Foc_Particle,
	TR_Foc_Torque,
	TR_Foc_ActiveAlign,
	TR_Foc_MAX,
};

class CWnd_TestResult_Foc : public CWnd
{
	DECLARE_DYNAMIC(CWnd_TestResult_Foc)

public:
	CWnd_TestResult_Foc();
	virtual ~CWnd_TestResult_Foc();
	CMFCTabCtrl		m_tc_Option;

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	enInsptrSysType				m_InspectionType;

	CFont						m_font;
	
	CVGStatic					m_st_default;
	CVGStatic					m_st_Item[STI_TR_Foc_MAX];

	CWnd_ActiveAlignData		m_Wnd_ActiveAlignData;
	CWnd_CurrentData			m_Wnd_CurrentData;
	CWnd_DefectPixelData		m_Wnd_DefectPixelData;
	CWnd_OpticalData			m_Wnd_OpticalData;
	CWnd_RotateData				m_Wnd_RotateData;
	CWnd_SFRData				m_Wnd_SFRData;
	CWnd_ParticleData			m_Wnd_ParticleData;
	CWnd_TorqueData				m_Wnd_TorqueData;

	void	SetShowWindowResult (int iItem);
	void	MoveWindow_Result	(int x, int y, int nWidth, int nHeight);

public:
	int m_iSelectTest[TR_Foc_MAX];
	CComboBox		m_cb_TestItem;

	// 검사기 종류 설정
	void	SetSystemType (__in enInsptrSysType nSysType)
	{
		m_InspectionType = nSysType;
	};

	void SetTestItem		(__in int iTestItem);
	void SetUpdateResult	(__in const ST_RecipeInfo_Base* pstRecipeInfo, __in int iTestItem);
	void SetUpdateClear		();

	void AllDataReset();
	void SetUIData_Reset(int nIdx);
	void SetUIData(int nIdx, LPVOID pParam);
	void SetClearTab();
	void SetAddTab(int nIdx, int iItemCnt);
	afx_msg void OnLbnSelChangeTest();
	void SelectNum(UINT nSelect);
};
#endif // Wnd_TestResult_Foc_h__
