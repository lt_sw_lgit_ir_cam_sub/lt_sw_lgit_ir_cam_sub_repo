﻿#ifndef List_LightBrd_Op_h__
#define List_LightBrd_Op_h__

#pragma once

#include "List_Cfg_VIBase.h"
#include "Def_Enum_Cm.h"
#include "Def_DataStruct.h"
#include "Def_TestDevice.h"

typedef enum enListItemNum_LightBrdOp
{
	LightOp_ItemNum = Light_I_MaxEnum,
};

//-----------------------------------------------------------------------------
// List_SFRInfo
//-----------------------------------------------------------------------------
class CList_LightBrd_Op : public CList_Cfg_VIBase
{
	DECLARE_DYNAMIC(CList_LightBrd_Op)

public:
	CList_LightBrd_Op();
	virtual ~CList_LightBrd_Op();
	UINT m_nMessage;
	void SetWindowMessage(UINT nMeg){
		m_nMessage = nMeg;
	};
protected:
	// 광원 제어 종류
	typedef enum enLightBrdType
	{
		LBT_ODA_PT,			// ODA PT 시리즈
		LBT_Luritech_01,	// 루리텍 광원 보드
	};
 
	DECLARE_MESSAGE_MAP()
	enLightBrdType	m_nLightBoardType = LBT_ODA_PT;
	PVOID			m_pLightDevice = NULL;

	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg void	OnNMClick				(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk				(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL	OnMouseWheel			(UINT nFlags, short zDelta, CPoint pt);

	afx_msg void OnRangeCheckBtnOn (UINT nID);
	afx_msg void	OnRangeCheckBtnOff	(UINT nID);

	afx_msg void	OnEnKillFocusEdit();

	BOOL			UpdateCellData			(UINT nRow, UINT nCol, int iValue);
	BOOL			UpdateCelldbData		(UINT nRow, UINT nCol, double dValue);

	CFont				m_Font;
	CEdit				m_ed_CellEdit;
	UINT				m_nEditCol;
	UINT				m_nEditRow;
	CButton				m_bt_On[LightOp_ItemNum];
	CButton				m_bt_Off[LightOp_ItemNum];

	

	//ST_SFR_Opt*			m_pstConfigInfo = NULL;
	ST_LightInfo *m_pstLightInfo = NULL;
public:

	virtual void SetPtr_Device(__in UINT nCtrlType, __in PVOID pLightDevice);
	void SetPtr_LightInfo(ST_LightInfo* pstLightInfo)
	{
	

		m_pstLightInfo = pstLightInfo;
	};

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();
	
};

#endif // List_SFRInfo_h__
