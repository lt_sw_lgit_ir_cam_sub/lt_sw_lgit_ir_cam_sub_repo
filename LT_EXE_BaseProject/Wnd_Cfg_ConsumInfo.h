﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_ConsumInfo.h
// Created	:	2016/10/31 - 20:17
// Modified	:	2016/10/31 - 20:17
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Cfg_ConsumInfo_h__
#define Wnd_Cfg_ConsumInfo_h__

#pragma once

#include <afxwin.h>
#include "Wnd_BaseView.h"
#include "Def_DataStruct.h"
#include "VGStatic.h"
#include "File_Recipe.h"
#include "File_WatchList.h"

class CWnd_Cfg_ConsumInfo : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_Cfg_ConsumInfo)

public:
	CWnd_Cfg_ConsumInfo();
	virtual ~CWnd_Cfg_ConsumInfo();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);
	virtual BOOL	PreTranslateMessage		(MSG* pMsg);
	afx_msg void	OnShowWindow			(BOOL bShow, UINT nStatus);

	afx_msg void	OnCbnSelendokFile		();
	afx_msg void	OnBnClickedBnSave		();
	afx_msg void	OnBnClickedReset		(UINT nID);	
	afx_msg void	OnBnClickedBnApply		();
	afx_msg void	OnEnKillFocusEdMaxCnt	();

	afx_msg LRESULT	OnRefreshFileList		(WPARAM wParam, LPARAM lParam);

	CFont			m_font_Default;
	CFont			m_font_Data;

	CVGStatic		m_st_File;
	CComboBox		m_cb_File;
	CButton			m_bn_NewFile;
	
	CVGStatic		m_stCount_Max[MAX_CONSUMABLES_ITEM_CNT];
	CVGStatic		m_stCount[MAX_CONSUMABLES_ITEM_CNT];
	CEdit			m_ed_Count_Max[MAX_CONSUMABLES_ITEM_CNT];
	CEdit			m_ed_Count[MAX_CONSUMABLES_ITEM_CNT];
	CButton			m_bn_ResetCount[MAX_CONSUMABLES_ITEM_CNT];

	CButton			m_bn_Apply;
	CButton			m_bn_Cancel;

	CString				m_strConsumInfoPath;
	ST_ConsumablesInfo	m_stConsumInfo;
	CFile_Recipe		m_fileRecipe;

	// .ini 파일 변화 감지를 위한 클래스
	CFile_WatchList		m_IniWatch;

	// 선택한 소모품 설정 파일
	CString				m_szConsumInfoFile;

	// 검사기 설정
	enInsptrSysType		m_InspectionType = enInsptrSysType::Sys_Focusing;

	// 소모품 설정 데이터 저장 및 가져오기
	BOOL		LoadIniFile				(__in LPCTSTR szFile);
	BOOL		SaveIniFile				(__in LPCTSTR szFile);

	// 콤보 박스의 소모품 설정 파일 선택 완료시 사용할 함수
	void		SelectEndConsumInfoFile	(__in LPCTSTR szFile);

public:	

	// 검사기 종류 설정
	void		SetSystemType			(__in enInsptrSysType nSysType);

	// 소모품 설정 파일이 있는 경로
	void		SetConsumInfoPath		(__in LPCTSTR szPath)
	{
		m_strConsumInfoPath = szPath;
		m_IniWatch.SetWatchOption(m_strConsumInfoPath, CONSUM_FILE_EXT);
		m_IniWatch.BeginWatchThrFunc();
	};

	// 콤보 박스의 소모품 설정 파일 목록 갱신
	void		RefreshFileList			(__in const CStringList* pFileList);

	// 콤보 박스의 소모품 설정 파일 선택
	void		SetConsumInfoFile		(__in LPCTSTR szFile);
	ST_ConsumablesInfo	GetConsumInfo	();

	CString		GetConsumInfoFile		();
	BOOL		SaveConsumInfoSetting	();
};

#endif // Wnd_Cfg_ConsumInfo_h__
