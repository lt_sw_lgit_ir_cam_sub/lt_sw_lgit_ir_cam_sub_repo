#ifndef Def_T_SteCal_h__
#define Def_T_SteCal_h__

#include <afxwin.h>
#include "MRA2_stereo_calibration.h"

#pragma pack(push,1)

typedef enum enParam_SteCal
{
	Param_SteCal_Board_W,
	Param_SteCal_Board_H,
	Param_SteCal_MaxNum,
};

static LPCTSTR	g_Param_SteCal[] =
{
	_T("Number of Horizontal Patterns"),	// 가로 패턴의 개수
	_T("Number of Vertical Patterns"),		// 세로 패턴의 개수
	NULL
};

typedef enum enSpec_SteCal_Single
{
	Spec_SteCalS_Focal_Len_X,	// fx
	Spec_SteCalS_Focal_Len_Y,	// fy
	Spec_SteCalS_Principle_X,	// cx
	Spec_SteCalS_Principle_Y,	// cy
	Spec_SteCalS_K1,
	Spec_SteCalS_K2,
	Spec_SteCalS_K3,
	Spec_SteCalS_K4,
	Spec_SteCalS_K5,
	Spec_SteCalS_RMS,
	Spec_SteCalS_Calib_Res_Width,
	Spec_SteCalS_Calib_Res_Height,
	Spec_SinCalS_MaxNum
};

static LPCTSTR	g_szSpecSteCal_Single[] =
{
	_T("Focal Len X"),
	_T("Focal Len Y"),
	_T("Principle X"),
	_T("Principle Y"),
	_T("K1"),
	_T("K2"),
	_T("K3"),
	_T("K4"),
	_T("K5"),
	_T("RMS"),
	_T("Calib Res Width"),
	_T("Calib Res Height"),
	NULL
};

typedef enum enSpec_SteCal_Stereo
{
// 	Spec_SteCalD_M_Focal_Len_X,	// fx	// master camera
// 	Spec_SteCalD_M_Focal_Len_Y,	// fy
// 	Spec_SteCalD_M_Principle_X,	// cx
// 	Spec_SteCalD_M_Principle_Y,	// cy
// 	Spec_SteCalD_M_K1,
// 	Spec_SteCalD_M_K2,
// 	Spec_SteCalD_M_K3,
// 	Spec_SteCalD_M_K4,
// 	Spec_SteCalD_M_K5,
// 	Spec_SteCalD_M_RMS,
// 	Spec_SteCalD_M_Calib_Res_Width,
// 	Spec_SteCalD_M_Calib_Res_Height,
// 	Spec_SteCalD_S_Focal_Len_X,	// fx	// slave camera
// 	Spec_SteCalD_S_Focal_Len_Y,	// fy
// 	Spec_SteCalD_S_Principle_X,	// cx
// 	Spec_SteCalD_S_Principle_Y,	// cy
// 	Spec_SteCalD_S_K1,
// 	Spec_SteCalD_S_K2,
// 	Spec_SteCalD_S_K3,
// 	Spec_SteCalD_S_K4,
// 	Spec_SteCalD_S_K5,
// 	Spec_SteCalD_S_RMS,
// 	Spec_SteCalD_S_Calib_Res_Width,
// 	Spec_SteCalD_S_Calib_Res_Height,
// 	Spec_SteCalD_RotationVector_1,	// streo camera
// 	Spec_SteCalD_RotationVector_2,
// 	Spec_SteCalD_RotationVector_3,
// 	Spec_SteCalD_TranslationVector_1,
// 	Spec_SteCalD_TranslationVector_2,
// 	Spec_SteCalD_TranslationVector_3,
	Spec_SteCalD_X,
	Spec_SteCalD_Y,
	Spec_SteCalD_Z,
	Spec_SteCalD_Roll,
	Spec_SteCalD_Pitch,
	Spec_SteCalD_Yaw,
	Spec_SteCalD_StereoRMS,

	Spec_SteCalD_MaxNum
};

static LPCTSTR	g_szSpecSteCal_Stereo[] =
{
// 	_T("Master Focal Len X"),
// 	_T("Master Focal Len Y"),
// 	_T("Master Principle X"),
// 	_T("Master Principle Y"),
// 	_T("Master K1"),
// 	_T("Master K2"),
// 	_T("Master K3"),
// 	_T("Master K4"),
// 	_T("Master K5"),
// 	_T("Master RMS"),
// 	_T("Master Calib Res Width"),
// 	_T("Master Calib Res Height"),
// 	_T("Slave Focal Len X"),
// 	_T("Slave Focal Len Y"),
// 	_T("Slave Principle X"),
// 	_T("Slave Principle Y"),
// 	_T("Slave K1"),
// 	_T("Slave K2"),
// 	_T("Slave K3"),
// 	_T("Slave K4"),
// 	_T("Slave K5"),
// 	_T("Slave RMS"),
// 	_T("Slave Calib Res Width"),
// 	_T("Slave Calib Res Height"),
// 	_T("Stereo Rotation Vector 1"),
// 	_T("Stereo Rotation Vector 2"),
// 	_T("Stereo Rotation Vector 3"),
// 	_T("Stereo Translation Vector 1"),
// 	_T("Stereo Translation Vector 2"),
// 	_T("Stereo Translation Vector 3"),
	_T("Stereo X"),
	_T("Stereo Y"),
	_T("Stereo Z"),
	_T("Stereo Roll"),
	_T("Stereo Pitch"),
	_T("Stereo Yaw"),
	_T("Stereo Stereo RMS"),
	NULL
};

 typedef enum enSpec_SteCal
 {
 	Spec_SteCal_M_Focal_Len_X,
 	Spec_SteCal_M_Focal_Len_Y,
 	Spec_SteCal_M_Principle_X,
 	Spec_SteCal_M_Principle_Y,
 	Spec_SteCal_M_K1,
 	Spec_SteCal_M_K2,
 	Spec_SteCal_M_K3,
 	Spec_SteCal_M_K4,
 	Spec_SteCal_M_K5,
 	Spec_SteCal_M_RMS,
	Spec_SteCal_S_Focal_Len_X,
	Spec_SteCal_S_Focal_Len_Y,
	Spec_SteCal_S_Principle_X,
	Spec_SteCal_S_Principle_Y,
	Spec_SteCal_S_K1,
	Spec_SteCal_S_K2,
	Spec_SteCal_S_K3,
	Spec_SteCal_S_K4,
	Spec_SteCal_S_K5,
	Spec_SteCal_S_RMS,
	Spec_SteCalD_RotationVector_1,	// streo camera
	Spec_SteCalD_RotationVector_2,
	Spec_SteCalD_RotationVector_3,
	Spec_SteCalD_TranslationVector_1,
	Spec_SteCalD_TranslationVector_2,
	Spec_SteCalD_TranslationVector_3,
 	Spec_SteCal_X,
 	Spec_SteCal_Y,
 	Spec_SteCal_Z,
 	Spec_SteCal_Roll,
 	Spec_SteCal_Pitch,
 	Spec_SteCal_Yaw,
 	Spec_SteCal_StereoRMS,
 
 	Spec_SteCal_MaxNum
 };
 
 static LPCTSTR	g_szSpecSteCal[] =
 {
 	_T("Master Focal Len X"),
 	_T("Master Focal Len Y"),
 	_T("Master Principle X"),
 	_T("Master Principle Y"),
 	_T("Master K1"),
 	_T("Master K2"),
 	_T("Master K3"),
 	_T("Master K4"),
 	_T("Master K5"),
 	_T("Master RMS"),
	_T("Slave Focal Len X"),
	_T("Slave Focal Len Y"),
	_T("Slave Principle X"),
	_T("Slave Principle Y"),
	_T("Slave K1"),
	_T("Slave K2"),
	_T("Slave K3"),
	_T("Slave K4"),
	_T("Slave K5"),
	_T("Slave RMS"),
	_T("Rotation Vector 1"),
	_T("Rotation Vector 2"),
	_T("Rotation Vector 3"),
	_T("Translation Vector 1"),
	_T("Translation Vector 2"),
	_T("Translation Vector 3"),
 	_T("X"),
 	_T("Y"),
 	_T("Z"),
 	_T("Roll"),
 	_T("Pitch"),
 	_T("Yaw"),
 	_T("Stereo RMS"),
 	NULL
 };

typedef struct _tag_SteCal_Para
{
	int		iBoard_Width;
	int		iBoard_Height;

	_tag_SteCal_Para()
	{
		Reset();
	};

	_tag_SteCal_Para& operator= (const _tag_SteCal_Para& ref)
	{
		iBoard_Width	= ref.iBoard_Width;
		iBoard_Height	= ref.iBoard_Height;

		return *this;
	};

	void Reset()
	{
		iBoard_Width	= 5;
		iBoard_Height	= 5;
	};
}ST_SteCal_Para, *PST_SteCal_Para;

// SteCal 세팅 구조체
#define MAX_PTN_POINT_CNT			255

typedef struct _tag_SteCal_Result
{
	double		dMasterItem[Spec_SinCalS_MaxNum];
	double		dSlaveItem[Spec_SinCalS_MaxNum];
	double		dStereoItem[Spec_SteCal_MaxNum];

	SINGLE_CALIBRATION_RESULT	MonoCal;
	STEREO_CALIBRATION_RESULT	StereoCal;

	INT			nPtCount_M[MAX_STEP_COUNT];
	MRA2_POINT	ptPattern_M[MAX_STEP_COUNT][MAX_PTN_POINT_CNT];
	
	INT			nPtCount_S[MAX_STEP_COUNT];
	MRA2_POINT	ptPattern_S[MAX_STEP_COUNT][MAX_PTN_POINT_CNT];
	

	_tag_SteCal_Result()
	{
		Reset();
	};

	_tag_SteCal_Result& operator= (_tag_SteCal_Result& ref)
	{
		for (UINT nIdx = 0; nIdx < Spec_SinCalS_MaxNum; nIdx++)
		{
			dMasterItem[nIdx] = ref.dMasterItem[nIdx];
			dSlaveItem[nIdx] = ref.dSlaveItem[nIdx];
		}

		for (UINT nIdx = 0; nIdx < Spec_SteCal_MaxNum; nIdx++)
		{
			dStereoItem[nIdx]	= ref.dStereoItem[nIdx];
		}

		MonoCal = ref.MonoCal;
		StereoCal = ref.StereoCal;

		memcpy(nPtCount_M,	ref.nPtCount_M,		sizeof(INT) * MAX_STEP_COUNT);
		memcpy(ptPattern_M, ref.ptPattern_M,	sizeof(MRA2_POINT) * MAX_STEP_COUNT * MAX_PTN_POINT_CNT);
		
		memcpy(nPtCount_S,	ref.nPtCount_S,		sizeof(INT) * MAX_STEP_COUNT);
		memcpy(ptPattern_S, ref.ptPattern_S,	sizeof(MRA2_POINT) * MAX_STEP_COUNT * MAX_PTN_POINT_CNT);

		return *this;
	};

	void Reset()
	{
		memset(dMasterItem, 0, sizeof(double) * Spec_SinCalS_MaxNum);
		memset(dSlaveItem,	0, sizeof(double) * Spec_SinCalS_MaxNum);
		memset(dStereoItem, 0, sizeof(double) * Spec_SteCal_MaxNum);

		ZeroMemory(&MonoCal,	sizeof(SINGLE_CALIBRATION_RESULT));
		ZeroMemory(&StereoCal,	sizeof(STEREO_CALIBRATION_RESULT));

		ZeroMemory(&nPtCount_M,	sizeof(INT) * MAX_STEP_COUNT);
		ZeroMemory(&ptPattern_M,sizeof(MRA2_POINT) * MAX_STEP_COUNT * MAX_PTN_POINT_CNT);
		
		ZeroMemory(&nPtCount_S, sizeof(INT) * MAX_STEP_COUNT);
		ZeroMemory(&ptPattern_S,sizeof(MRA2_POINT) * MAX_STEP_COUNT * MAX_PTN_POINT_CNT);
	};

}ST_SteCal_Result, *PST_SteCal_Result;


typedef struct _tag_SteCal_Spec
{
	double		dMasterItem[Spec_SinCalS_MaxNum];
	double		dSlaveItem[Spec_SinCalS_MaxNum];
	double		dStereoItem[Spec_SteCal_MaxNum];

	SINGLE_CALIBRATION_RESULT	MonoCal;
	STEREO_CALIBRATION_RESULT	StereoCal;

	_tag_SteCal_Spec()
	{
		Reset();
	};

	_tag_SteCal_Spec& operator= (_tag_SteCal_Spec& ref)
	{
		for (UINT nIdx = 0; nIdx < Spec_SinCalS_MaxNum; nIdx++)
		{
			dMasterItem[nIdx] = ref.dMasterItem[nIdx];
			dSlaveItem[nIdx] = ref.dSlaveItem[nIdx];
		}

		for (UINT nIdx = 0; nIdx < Spec_SteCal_MaxNum; nIdx++)
		{
			dStereoItem[nIdx] = ref.dStereoItem[nIdx];
		}

		MonoCal = ref.MonoCal;
		StereoCal = ref.StereoCal;

		return *this;
	};

	void Reset()
	{
		memset(dMasterItem, 0, sizeof(double) * Spec_SinCalS_MaxNum);
		memset(dSlaveItem,	0, sizeof(double) * Spec_SinCalS_MaxNum);
		memset(dStereoItem, 0, sizeof(double) * Spec_SteCal_MaxNum);

		ZeroMemory(&MonoCal, sizeof(SINGLE_CALIBRATION_RESULT));
		ZeroMemory(&StereoCal, sizeof(STEREO_CALIBRATION_RESULT));
	};

}ST_SteCal_Spec, *PST_SteCal_Spec;


typedef struct _tag_SteCal_Info
{
	ST_SteCal_Para		Parameter;
	ST_SteCal_Spec		SpecMin;
	ST_SteCal_Spec		SpecMax;

	_tag_SteCal_Info()
	{
		Reset();
	};

	_tag_SteCal_Info& operator= (_tag_SteCal_Info& ref)
	{
		Parameter	= ref.Parameter;
		SpecMin		= ref.SpecMin;
		SpecMax		= ref.SpecMax;

		return *this;
	};

	void Reset()
	{
		Parameter.Reset();
		SpecMin.Reset();
		SpecMax.Reset();
	};

}ST_SteCal_Info, *PST_SteCal_Info;

#pragma pack(pop)

#endif // Def_T_SteCal_h__
