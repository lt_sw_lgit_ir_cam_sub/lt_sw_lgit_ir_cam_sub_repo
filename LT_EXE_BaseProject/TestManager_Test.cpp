﻿//*****************************************************************************
// Filename	: 	TestManager_Test.cpp
// Created	:	2017/10/14 - 18:01
// Modified	:	2017/10/14
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "TestManager_Test.h"
#include "CommonFunction.h"

CTestManager_Test::CTestManager_Test()
{
}

CTestManager_Test::~CTestManager_Test()
{
}

//=============================================================================
// Method		: CenterPointFunc
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImage8bitBuf
// Parameter	: __in int iImgW
// Parameter	: __in int iImgH
// Parameter	: __in ST_OpticalCenter_Opt stTestOpt
// Parameter	: __out ST_OpticalCenter_Data & stTestData
// Parameter	: __in int iCenterX
// Parameter	: __in int iCenterY
// Parameter	: __in UINT nParaID
// Qualifier	:
// Last Update	: 2018/3/15 - 11:53
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::CenterPointFunc(__in LPBYTE pImage8bitBuf, __in int iImgW, __in int iImgH, __in ST_OpticalCenter_Opt stTestOpt, __out ST_OpticalCenter_Data &stTestData, __in int iCenterX /*= 0*/, __in int iCenterY /*= 0*/, __in UINT nParaID /*= 0*/)
{
	if (NULL == m_pstRecipeinfo)
		return RC_Invalid_Handle;

	if (NULL == pImage8bitBuf || iImgW < 1 || iImgH < 1)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	//Test 영역과 이진화 밝기
	int iPosX = 0;
	int iPosY = 0;

	int iThresBright = stTestOpt.stRegion.nBrightness;
	CRect TestRect = stTestOpt.stRegion.rtROI;

	//원을 찾는 다.
	BOOL bResult = m_TI_CircleDetect.CircleDetect_Test(pImage8bitBuf, iImgW, iImgH, TestRect, iThresBright, iPosX, iPosY, stTestOpt.stRegion.nMarkColor, stTestOpt.stRegion.nSharpness);

	//광축 Pixel값이 나옴. [ nPosX, nPosY ]
	if (TRUE == bResult)
	{
		//-영상에서 그려질 위치
		stTestData.nPixX = iPosX;
		stTestData.nPixY = iPosY;

		//카메라 상태 별로 값 변경.
		CamType_CenterPoint(iImgW, iImgH, iPosX, iPosY);

		stTestData.nResultPosX = iPosX + stTestOpt.iOffsetX;
		stTestData.nResultPosY = iPosY + stTestOpt.iOffsetY;
		
		stTestData.iStandPosDevX = stTestData.nResultPosX - stTestOpt.nStandarX;
		stTestData.iStandPosDevY = stTestData.nResultPosY - stTestOpt.nStandarY;

		//// 기준 점 대비 편차 X, Y
		//if (Para_Right == nParaID)
		//{
		//	stTestData.nResultPosX -= iCenterX;
		//	stTestData.nResultPosY -= iCenterY;

		//	stTestData.iStandPosDevX -= iCenterX;
		//	stTestData.iStandPosDevY -= iCenterY;
		//}

		stTestData.nDetectResult = TR_Pass;
	}
	else
	{
		stTestData.nResultPosX = iPosX;
		stTestData.nResultPosY = iPosY;

		stTestData.iStandPosDevX = 0;
		stTestData.iStandPosDevY = 0;

		stTestData.nDetectResult = TR_Fail;

		lReturn = RC_Detect;
	}

	BOOL bMinUseX = stTestOpt.stSpec_Min[Spec_OC_X].bEnable;
	BOOL bMinUseY = stTestOpt.stSpec_Min[Spec_OC_Y].bEnable;
	BOOL bMaxUseX = stTestOpt.stSpec_Max[Spec_OC_X].bEnable;
	BOOL bMaxUseY = stTestOpt.stSpec_Max[Spec_OC_Y].bEnable;

	int iMinDevX = stTestOpt.stSpec_Min[Spec_OC_X].iValue;
	int iMinDevY = stTestOpt.stSpec_Min[Spec_OC_Y].iValue;
	int iMaxDevX = stTestOpt.stSpec_Max[Spec_OC_X].iValue;
	int iMaxDevY = stTestOpt.stSpec_Max[Spec_OC_Y].iValue;

	// 판정
	stTestData.nResultX = Get_MeasurmentData(bMinUseX, bMaxUseX, iMinDevX, iMaxDevX, stTestData.iStandPosDevX);
	stTestData.nResultY = Get_MeasurmentData(bMinUseY, bMaxUseY, iMinDevY, iMaxDevY, stTestData.iStandPosDevY);

	stTestData.nResult = stTestData.nResultX & stTestData.nResultY & stTestData.nDetectResult;

	return lReturn;
}

//=============================================================================
// Method		: CamType_CenterPoint
// Access		: public  
// Returns		: void
// Parameter	: __in int iImgW
// Parameter	: __in int iImgH
// Parameter	: __out int & nPosX
// Parameter	: __out int & nPosY
// Qualifier	:
// Last Update	: 2018/2/27 - 9:23
// Desc.		:
//=============================================================================
void CTestManager_Test::CamType_CenterPoint(__in int iImgW, __in int iImgH, __out int &nPosX, __out  int &nPosY)
{
	//카메라 상태에 따라 값 변경.
	int ICamType	= m_pstRecipeinfo->nCamImageType;
	int IChangePosX = 0;
	int IChangePosY = 0;
	
	switch (ICamType)
	{
		// 좌우반전
		case CAM_STATE_MIRROR:
			IChangePosX = nPosX;
			IChangePosY = nPosY;
			break;

		// 상하반전
		case CAM_STATE_FLIP:
			IChangePosX = iImgW - nPosX;
			IChangePosY = iImgH - nPosY;
			break;
			
		// 오리지널
		case CAM_STATE_ORIGINAL:
			IChangePosX = iImgW - nPosX;
			IChangePosY = nPosY;
			break;

		// 로테이트
		case CAM_STATE_ROTATE:
			IChangePosX = nPosX;
			IChangePosY = iImgH - nPosY;
			break;

		default:
			break;
	}

	nPosX = IChangePosX;
	nPosY = IChangePosY;
}

//=============================================================================
// Method		: RotateFunc
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImage8bitBuf
// Parameter	: __in int iImgW
// Parameter	: __in int iImgH
// Parameter	: __in ST_Rotate_Opt stTestOpt
// Parameter	: __out ST_Rotate_Data & stTestData
// Qualifier	:
// Last Update	: 2018/2/27 - 9:24
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::RotateFunc(__in LPBYTE pImage8bitBuf, __in int iImgW, __in int iImgH, __in ST_Rotate_Opt stTestOpt, __out ST_Rotate_Data &stTestData)
{
	if (NULL == m_pstRecipeinfo)
		return RC_Invalid_Handle;

	if (NULL == pImage8bitBuf || iImgW < 1 || iImgH <1)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;
	int nCenterPosX = 0;
	int nCenterPosY = 0;
	int nGapOffsetX = 0;
	int nGapOffsetY = 0;
	int nPosX = 0;
	int nPosY = 0;

	CRect TestRect;
	int   nThresBright = 0;

	TestRect.left	= iImgW / 4 ;
	TestRect.right	= iImgW - (iImgW / 4);
	TestRect.top	= iImgH / 4;
	TestRect.bottom = iImgH - (iImgH / 4);

	//-1. 차트의 중앙점을 찾는다.
	BOOL bResult = m_TI_CircleDetect.CircleDetect_Test(pImage8bitBuf, iImgW, iImgH, TestRect, nThresBright, nCenterPosX, nCenterPosY, m_eMarkColor);

	if (bResult)
	{
		nGapOffsetX = nCenterPosX - iImgW / 2;
		nGapOffsetY = nCenterPosY - iImgH / 2;
	}

	//-2. 차트의 중앙점과 영상 중심의 Gap만큼 ROI를 움직인다.
	int nRectCnt = 0;

	for (int t = 0; t < ROI_Rot_Max; t++)
	{
		if (stTestOpt.stRegion[t].rtROI.Width() == 0 && stTestOpt.stRegion[t].rtROI.Height() == 0)
		{
			break;
		}

		stTestData.rtTestPicROI[t] = stTestOpt.stRegion[t].rtROI;

		stTestData.rtTestPicROI[t].left		+= nGapOffsetX;
		stTestData.rtTestPicROI[t].right	+= nGapOffsetX;
		stTestData.rtTestPicROI[t].top		+= nGapOffsetY;
		stTestData.rtTestPicROI[t].bottom	+= nGapOffsetY;

		nRectCnt++;
	}

	stTestData.nDetectResult = TRUE;

	//-3. 움직인 ROI에서 차트의 중심 좌표를 찾는다.
	for (int t = 0; t < nRectCnt; t++)
	{
		nThresBright = stTestOpt.stRegion[t].nBrightness;

		bResult = m_TI_CircleDetect.CircleDetect_Test(pImage8bitBuf, iImgW, iImgH, stTestData.rtTestPicROI[t], nThresBright, nPosX, nPosY, stTestOpt.stRegion[t].nMarkColor, stTestOpt.stRegion[t].nSharpness);

		if (FALSE == bResult)
		{
			stTestData.nDetectResult = FALSE;
			return RC_Detect;
		}

		stTestData.ptCenter[t].x = nPosX;
		stTestData.ptCenter[t].y = nPosY;
	}

	//-4. 위 좌표들로 Rotate를 산출한다.
	//-LT , LB
	double dHoripointY1 = ((stTestData.ptCenter[ROI_Rot_LB].y + stTestData.ptCenter[ROI_Rot_LT].y) / 2);
	double dHoripointX1 = ((stTestData.ptCenter[ROI_Rot_LB].x + stTestData.ptCenter[ROI_Rot_LT].x) / 2);

	//-RT , RB
	double dHoripointY2 = ((stTestData.ptCenter[ROI_Rot_RB].y + stTestData.ptCenter[ROI_Rot_RT].y) / 2);
	double dHoripointX2 = ((stTestData.ptCenter[ROI_Rot_RB].x + stTestData.ptCenter[ROI_Rot_RT].x) / 2);

	double	dHor_Degree = Horizontal_GetRotationCheck(dHoripointX2, dHoripointY2, dHoripointX1, dHoripointY1);
		
	//-LT , RT
	double dVetipointY1 = ((stTestData.ptCenter[ROI_Rot_RT].y + stTestData.ptCenter[ROI_Rot_LT].y) / 2);
	double dVetipointX1 = ((stTestData.ptCenter[ROI_Rot_RT].x + stTestData.ptCenter[ROI_Rot_LT].x) / 2);

	//-LB , RB
	double dVetipointY2 = ((stTestData.ptCenter[ROI_Rot_RB].y + stTestData.ptCenter[ROI_Rot_LB].y) / 2);
	double dVetipointX2 = ((stTestData.ptCenter[ROI_Rot_RB].x + stTestData.ptCenter[ROI_Rot_LB].x) / 2);

	double dbVer_Degree = Vertical_GetRotationCheck(dVetipointX2, dVetipointY2, dVetipointX1, dVetipointY1);

	double dbDegree = (((dHor_Degree) + (dbVer_Degree)) / 2.0);

	dbDegree = dbDegree - stTestOpt.dbStandard;

	dbDegree *= stTestOpt.dbOffset;

	stTestData.dbValue = dbDegree;

	BOOL	bMinUse = stTestOpt.stSpec_Min.bEnable;
	BOOL	bMaxUse = stTestOpt.stSpec_Max.bEnable;
	double dbMinDev = stTestOpt.stSpec_Min.dbValue;
	double dbMaxDev = stTestOpt.stSpec_Max.dbValue;

	// 판정
	stTestData.nResult = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stTestData.dbValue);

	return lReturn;
}

//=============================================================================
// Method		: Vertical_GetRotationCheck
// Access		: public  
// Returns		: double
// Parameter	: double pt_x1
// Parameter	: double pt_y1
// Parameter	: double pt_x2
// Parameter	: double pt_y2
// Qualifier	:
// Last Update	: 2018/3/1 - 12:27
// Desc.		:
//=============================================================================
double CTestManager_Test::Vertical_GetRotationCheck(double pt_x1, double pt_y1, double pt_x2, double pt_y2)
{
	double degree = atan2((pt_y1 - pt_y2), (pt_x1 - pt_x2));

	degree = degree * 180.0 / 3.14 - 90.0;

	return degree;
}

//=============================================================================
// Method		: Horizontal_GetRotationCheck
// Access		: public  
// Returns		: double
// Parameter	: double pt_x1
// Parameter	: double pt_y1
// Parameter	: double pt_x2
// Parameter	: double pt_y2
// Qualifier	:
// Last Update	: 2018/3/1 - 12:27
// Desc.		:
//=============================================================================
double CTestManager_Test::Horizontal_GetRotationCheck(double pt_x1, double pt_y1, double pt_x2, double pt_y2)
{
	double degree = atan2((pt_y1 - pt_y2), (pt_x1 - pt_x2));

	degree = degree * 180.0 / 3.14;

	return degree;
}

//=============================================================================
// Method		: FovFunc
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImage8bitBuf
// Parameter	: __in int iImgW
// Parameter	: __in int iImgH
// Parameter	: __in ST_FOV_Opt stTestOpt
// Parameter	: __out ST_FOV_Data & stTestData
// Qualifier	:
// Last Update	: 2018/3/1 - 12:27
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::FovFunc(__in LPBYTE pImage8bitBuf, __in int iImgW, __in int iImgH, __in ST_FOV_Opt stTestOpt, __out ST_FOV_Data &stTestData)
{
	if (NULL == m_pstRecipeinfo)
		return RC_Invalid_Handle;

	if (NULL == pImage8bitBuf || iImgW < 1 || iImgH < 1)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;
	int nCenterPosX = 0;
	int nCenterPosY = 0;
	int nGapOffsetX = 0;
	int nGapOffsetY = 0;
	int nPosX = 0;
	int nPosY = 0;

	CRect TestRect;
	int   nThresBright = 0;

	TestRect = stTestOpt.stRegion[ROI_Fov_CC].rtROI;
	nThresBright = stTestOpt.stRegion[ROI_Fov_CC].nBrightness;

	for (int t = 0; t < ROI_Fov_Max; t++)
	{
		stTestData.rtTestPicROI[t] = stTestOpt.stRegion[t].rtROI;
	}
	//-1. 차트의 중앙점을 찾는다.
	BOOL bResult = m_TI_CircleDetect.CircleDetect_Test(pImage8bitBuf, iImgW, iImgH, TestRect, nThresBright, nCenterPosX, nCenterPosY, m_eMarkColor);
	if (bResult)
	{
		nGapOffsetX = nCenterPosX - iImgW / 2;
		nGapOffsetY = nCenterPosY - iImgH / 2;
	}
	
	stTestData.ptCenter[ROI_Fov_CC].x = nCenterPosX;
	stTestData.ptCenter[ROI_Fov_CC].y = nCenterPosY;
	//-2. 차트의 중앙점과 영상 중심의 Gap만큼 ROI를 움직인다.
	int nRectCnt = 0;

	for (int t = ROI_Fov_CT; t < ROI_Fov_Max; t++)
	{
		if (stTestOpt.stRegion[t].rtROI.Width() == 0 && stTestOpt.stRegion[t].rtROI.Height() == 0)
		{
			break;
		}
		stTestData.rtTestPicROI[t] = stTestOpt.stRegion[t].rtROI;

		stTestData.rtTestPicROI[t].left += nGapOffsetX;
		stTestData.rtTestPicROI[t].right += nGapOffsetX;
		stTestData.rtTestPicROI[t].top += nGapOffsetY;
		stTestData.rtTestPicROI[t].bottom += nGapOffsetY;
		nRectCnt++;
	}

	//-3. 움직인 ROI에서 차트의 중심 좌표를 찾는다.
	for (int t = ROI_Fov_CT; t < ROI_Fov_CC; t++)
	{
		nThresBright = stTestOpt.stRegion[t].nBrightness;

		bResult = m_TI_CircleDetect.CircleDetect_Test(pImage8bitBuf, iImgW, iImgH, stTestData.rtTestPicROI[t], nThresBright, nPosX, nPosY, stTestOpt.stRegion[t].nMarkColor, stTestOpt.stRegion[t].nSharpness);

		stTestData.ptCenter[t].x = nPosX;
		stTestData.ptCenter[t].y = nPosY;
	}

	double dDis = 0;
	double dDegree[4] = { 0, };

	//거리 계산
	for (int t = ROI_Fov_CT; t < ROI_Fov_CC; t++)
	{
		dDis = sqrt(pow(stTestData.ptCenter[ROI_Fov_CC].x - stTestData.ptCenter[t].x, 2) + pow(stTestData.ptCenter[ROI_Fov_CC].y - stTestData.ptCenter[t].y, 2));
		dDegree[t] = Degree_Sum( t, dDis);
	}

	double dHorDegree = dDegree[ROI_Fov_CL] + dDegree[ROI_Fov_CR];
	double dVerDegree = dDegree[ROI_Fov_CT] + dDegree[ROI_Fov_CB];

	stTestData.dbValue[Spec_Fov_Hor] = dHorDegree * stTestOpt.dbOffset[Spec_Fov_Hor];
	stTestData.dbValue[Spec_Fov_Ver] = dVerDegree * stTestOpt.dbOffset[Spec_Fov_Ver];


	BOOL	bMinUseHor = stTestOpt.stSpec_Min[Spec_Fov_Hor].bEnable;
	BOOL	bMaxUseHor = stTestOpt.stSpec_Max[Spec_Fov_Hor].bEnable;

	double dbMinDevHor = stTestOpt.stSpec_Min[Spec_Fov_Hor].dbValue;
	double dbMaxDevHor = stTestOpt.stSpec_Max[Spec_Fov_Hor].dbValue;

	BOOL	bMinUseVer = stTestOpt.stSpec_Min[Spec_Fov_Ver].bEnable;
	BOOL	bMaxUseVer = stTestOpt.stSpec_Max[Spec_Fov_Ver].bEnable;

	double dbMinDevVer = stTestOpt.stSpec_Min[Spec_Fov_Ver].dbValue;
	double dbMaxDevVer = stTestOpt.stSpec_Max[Spec_Fov_Ver].dbValue;

	// 판정
	stTestData.nEachResult[Spec_Fov_Hor] = Get_MeasurmentData(bMinUseHor, bMaxUseHor, dbMinDevHor, dbMaxDevHor, stTestData.dbValue[Spec_Fov_Hor]);
	stTestData.nEachResult[Spec_Fov_Ver] = Get_MeasurmentData(bMinUseVer, bMaxUseVer, dbMinDevVer, dbMaxDevVer, stTestData.dbValue[Spec_Fov_Ver]);

	stTestData.nResult = stTestData.nEachResult[Spec_Fov_Hor] & stTestData.nEachResult[Spec_Fov_Ver];

	return lReturn;
}

//=============================================================================
// Method		: Degree_Sum
// Access		: public  
// Returns		: double
// Parameter	: int NUM
// Parameter	: double Distance
// Qualifier	:
// Last Update	: 2018/3/1 - 12:27
// Desc.		:
//=============================================================================
double CTestManager_Test::Degree_Sum(int NUM , double Distance)
{
	double Degree = 0;

	double m_Distance = (m_pstRecipeinfo->fPixelSize *Distance) / 1.0;
	Degree = atan((m_Distance / 1000.0) / m_pstRecipeinfo->fFocalLength) * 180.0 / 3.141592;

	return Degree;
}

//=============================================================================
// Method		: DistortionFunc
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImage8bitBuf
// Parameter	: __in int iImgW
// Parameter	: __in int iImgH
// Parameter	: __in ST_Distortion_Opt stTestOpt
// Parameter	: __out ST_Distortion_Data & stTestData
// Qualifier	:
// Last Update	: 2018/3/1 - 12:27
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::DistortionFunc(__in LPBYTE pImage8bitBuf, __in int iImgW, __in int iImgH, __in ST_Distortion_Opt stTestOpt, __out ST_Distortion_Data &stTestData)
{
	if (NULL == m_pstRecipeinfo)
		return RC_Invalid_Handle;

	if (NULL == pImage8bitBuf || iImgW < 1 || iImgH < 1)
	{
		return RC_Invalid_Handle;
	}

	LRESULT	lReturn = RC_OK;
	int nCenterPosX = 0;
	int nCenterPosY = 0;
	int nGapOffsetX = 0;
	int nGapOffsetY = 0;
	int nPosX = 0;
	int nPosY = 0;

	CRect TestRect;
	int   nThresBright = 0;

	TestRect.left = iImgW / 4;
	TestRect.right = iImgW - (iImgW / 4);

	TestRect.top = iImgH / 4;
	TestRect.bottom = iImgH - (iImgH / 4);


	//-1. 차트의 중앙점을 찾는다.
	BOOL bResult = m_TI_CircleDetect.CircleDetect_Test(pImage8bitBuf, iImgW, iImgH, TestRect, nThresBright, nCenterPosX, nCenterPosY, m_eMarkColor);
	
	if (bResult)
	{
		nGapOffsetX = nCenterPosX - iImgW / 2;
		nGapOffsetY = nCenterPosY - iImgH / 2;
	}
	
	//-2. 차트의 중앙점과 영상 중심의 Gap만큼 ROI를 움직인다.
	int nRectCnt = 0;

	for (int t = 0; t < ROI_Dis_Max; t++)
	{

		if (stTestOpt.stRegion[t].rtROI.Width() == 0 && stTestOpt.stRegion[t].rtROI.Height() == 0)
		{
			break;
		}
		stTestData.rtTestPicROI[t] = stTestOpt.stRegion[t].rtROI;

		stTestData.rtTestPicROI[t].left += nGapOffsetX;
		stTestData.rtTestPicROI[t].right += nGapOffsetX;
		stTestData.rtTestPicROI[t].top += nGapOffsetY;
		stTestData.rtTestPicROI[t].bottom += nGapOffsetY;
		nRectCnt++;
	}


	//-3. 움직인 ROI에서 차트의 중심 좌표를 찾는다.
	for (int t = ROI_Dis_LT; t < ROI_Dis_Max; t++)
	{
		nThresBright = stTestOpt.stRegion[t].nBrightness;

		bResult = m_TI_CircleDetect.CircleDetect_Test(pImage8bitBuf, iImgW, iImgH, stTestData.rtTestPicROI[t], nThresBright, nPosX, nPosY, stTestOpt.stRegion[t].nMarkColor, stTestOpt.stRegion[t].nSharpness);

		stTestData.ptCenter[t].x = nPosX;
		stTestData.ptCenter[t].y = nPosY;
	}

	//-4.영역간 거리 산출
	//- LT, LB 거리
	double A1 = GetDistance(stTestData.ptCenter[ROI_Dis_LT].x, stTestData.ptCenter[ROI_Dis_LT].y, stTestData.ptCenter[ROI_Dis_LB].x, stTestData.ptCenter[ROI_Dis_LB].y);
	//- RT, RB 거리
	double A2 = GetDistance(stTestData.ptCenter[ROI_Dis_RT].x, stTestData.ptCenter[ROI_Dis_RT].y, stTestData.ptCenter[ROI_Dis_RB].x, stTestData.ptCenter[ROI_Dis_RB].y);

	double A = (A1 + A2) / 2.0;
	//- CT, CB 거리
	double B = GetDistance(stTestData.ptCenter[ROI_Dis_CT].x, stTestData.ptCenter[ROI_Dis_CT].y, stTestData.ptCenter[ROI_Dis_CB].x, stTestData.ptCenter[ROI_Dis_CB].y);

	double d_Distortion = 0;

	if (A == 0 || B ==0)
	{
		d_Distortion =-999;
	}
	else{
		d_Distortion = (100.0 * (A - B) / B) * stTestOpt.dbOffset;
	}


	stTestData.dbValue = d_Distortion;

	BOOL	bMinUse = stTestOpt.stSpec_Min.bEnable;
	BOOL	bMaxUse = stTestOpt.stSpec_Max.bEnable;

	double dbMinDev = stTestOpt.stSpec_Min.dbValue;
	double dbMaxDev = stTestOpt.stSpec_Max.dbValue;

	// 판정
	stTestData.nResult = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stTestData.dbValue);

	TRACE(_T("KHO:::Distortion : %.2f \n"), stTestData.dbValue);
	return lReturn;
}
//=============================================================================
// Method		: GetDistance
// Access		: public  
// Returns		: double
// Parameter	: int x1
// Parameter	: int y1
// Parameter	: int x2
// Parameter	: int y2
// Qualifier	:
// Last Update	: 2018/3/1 - 12:27
// Desc.		:
//=============================================================================
double CTestManager_Test::GetDistance(int x1, int y1, int x2, int y2)
{
	double result;

	result = sqrt( (double)((x2-x1)*(x2-x1)) +  ((y2-y1)*(y2-y1)));

	return result;
}

//=============================================================================
// Method		: SFRFunc
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImage8bitBuf
// Parameter	: __in LPWORD pImage12bitBuf
// Parameter	: __in int iImgW
// Parameter	: __in int iImgH
// Parameter	: __in ST_SFR_Opt stTestOpt
// Parameter	: __out ST_SFR_Data & stTestData
// Qualifier	:
// Last Update	: 2018/3/1 - 12:26
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::SFRFunc(__in LPBYTE pImage8bitBuf, __in LPWORD pImage12bitBuf, __in int iImgW, __in int iImgH, __in ST_SFR_Opt stTestOpt, __out ST_SFR_Data &stTestData)
{
	if (NULL == m_pstRecipeinfo)
		return RC_Invalid_Handle;

	if (NULL == pImage8bitBuf || iImgW < 1 || iImgH < 1)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;
	int nCenterPosX = 0;
	int nCenterPosY = 0;
	int nGapOffsetX = 0;
	int nGapOffsetY = 0;
	int nPosX = 0;
	int nPosY = 0;

	CRect TestRect;
	int   nThresBright = 0;

	UINT nEachResultGroupCnt[ROI_SFR_GROUP_Max] = { 0, };
	//!SH _181204: Reset 되는지 확인 필요
	stTestData.Reset();
	//영상 기준으로 자동 ROI 설정.
	IplImage *TestRectImage = cvCreateImage(cvSize(iImgW, iImgH), IPL_DEPTH_8U, 3);
	memcpy(TestRectImage->imageData, pImage8bitBuf, TestRectImage->widthStep * TestRectImage->height);
	OnTestProcess_SFRROI(TestRectImage, stTestOpt, stTestData);

	//자동으로 ROI를 설정하면
	//ROI 별로 SFR 값 측정
	double dSFRValue = 0;

	for (int t = 0; t <ROI_SFR_Max; t++)
	{
		dSFRValue = 0;
		stTestData.bUse[t] = stTestOpt.stRegion[t].bEnable;

		if (stTestOpt.stRegion[t].bEnable)
		{
			m_TI_SFR.SFR_Test(pImage12bitBuf, iImgW, iImgH, stTestData.rtTestPicROI[t], stTestOpt.stRegion[t].dbLinePair, m_pstRecipeinfo->fPixelSize, dSFRValue, stTestOpt.nResultmode);
		}
		else
		{
			dSFRValue = 0;
		}

		if (dSFRValue == 0) //만약 값이 0이면
		{
			stTestData.dbValue[t] = dSFRValue;

		}
		else
		{
			if (stTestOpt.nResultmode == 1 || stTestOpt.nResultmode == 2)
			{
				stTestData.dbValue[t] = floor(dSFRValue + stTestOpt.stRegion[t].dbOffset);
			}
			else
				stTestData.dbValue[t] = dSFRValue + stTestOpt.stRegion[t].dbOffset;
		}
	}

	// 판정
	stTestData.nResult = TR_Pass;

	for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
	{
		BOOL	bMinUse = stTestOpt.stInput[nROI].stSpecMin.bEnable;
		BOOL	bMaxUse = stTestOpt.stInput[nROI].stSpecMax.bEnable;

		double dbMinDev = stTestOpt.stInput[nROI].stSpecMin.dbValue * 1000.0;
		double dbMaxDev = stTestOpt.stInput[nROI].stSpecMax.dbValue * 1000.0;
		double dbValue = stTestData.dbValue[nROI] * 1000.0;

		// 판정
		stTestData.nEachResult[nROI] = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, dbValue);

		stTestData.nResult &= stTestData.nEachResult[nROI];
	}
	/*
	// GROUP
	for (UINT nIdx = 0; nIdx < ITM_SFR_Max; nIdx++)
	{
		UINT nGroupCnt = 0;

		// 01F
		for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
		{
			if (TRUE == stTestOpt.stRegion[nROI].bEnable && nIdx == stTestOpt.stRegion[nROI].nSFR_Group)
			{
				nGroupCnt++;
			}
		}

		if (nGroupCnt > 0)
		{
			double	*dbMinValue = new double[nGroupCnt];
			UINT nCnt = 0;

			// 01F
			for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
			{
				if (TRUE == stTestOpt.stRegion[nROI].bEnable && nIdx == stTestOpt.stRegion[nROI].nSFR_Group)
				{
					dbMinValue[nCnt] = stTestData.dbValue[nROI];
					nCnt++;
				}
			}

			stTestData.dbMinValue[nIdx] = GetMin(dbMinValue, nGroupCnt);

			delete[] dbMinValue;
		}

		BOOL	bMinUse = stTestOpt.stSpec_F_Min[nIdx].bEnable;
		BOOL	bMaxUse = stTestOpt.stSpec_F_Max[nIdx].bEnable;

		double	dbMinDev = stTestOpt.stSpec_F_Min[nIdx].dbValue;
		double	dbMaxDev = stTestOpt.stSpec_F_Max[nIdx].dbValue;
		double	dbValue  = stTestData.dbMinValue[nIdx];

		// 판정
		stTestData.nEachResult_G[nIdx] = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, dbValue);
	}
	*/


	for (UINT nIdx = 0; nIdx < ROI_SFR_Max; nIdx++)
	{
		if (TRUE == stTestOpt.stRegion[nIdx].bEnable)
		{
			//!SH _181204: Enable 빼야 되는 지
			if (stTestData.dbValue[nIdx] > 0 && stTestData.dbValue[nIdx] < 1000) //!SH _181206: 일단 음수빼기, 100 또는 1 보다 작을때만? 
			{
				stTestData.dbEachResultGroup[stTestOpt.stRegion[nIdx].nSFR_Group] += stTestData.dbValue[nIdx];
				nEachResultGroupCnt[stTestOpt.stRegion[nIdx].nSFR_Group] ++;
			}
		}
	}

	for (UINT nIdx = 0; nIdx < ROI_SFR_GROUP_Max; nIdx++)
	{
		//for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
		//{
		
		if (TRUE == stTestOpt.stInput_Group[nIdx].bEnable)
		{
			if (stTestOpt.nResultmode == 1 || stTestOpt.nResultmode == 2)
			{
				stTestData.dbEachResultGroup[nIdx] = floor(stTestData.dbEachResultGroup[nIdx] / nEachResultGroupCnt[nIdx]);
			}
			else
				stTestData.dbEachResultGroup[nIdx] /=  nEachResultGroupCnt[nIdx];

			BOOL	bMinUse = stTestOpt.stInput_Group[nIdx].stSpecMin.bEnable;
			BOOL	bMaxUse = stTestOpt.stInput_Group[nIdx].stSpecMax.bEnable;

			double dbMinDev = stTestOpt.stInput_Group[nIdx].stSpecMin.dbValue * 1000.0;
			double dbMaxDev = stTestOpt.stInput_Group[nIdx].stSpecMax.dbValue * 1000.0;
			double dbValue = stTestData.dbEachResultGroup[nIdx] * 1000.0;

			// 판정
			stTestData.nEachResult_G[nIdx] = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, dbValue);
			//		stTestData.nTiltResult &= stTestData.nEachTiltResult[nIdx];
		}
	}


	for (UINT nIdx = 0; nIdx < ROI_SFR_TILT_Max; nIdx++)
	{
		if (TRUE == stTestOpt.stInput_Tilt[nIdx].bEnable)
		{
			stTestData.dbTiltdata[nIdx] = abs(stTestData.dbEachResultGroup[stTestOpt.stInput_Tilt[nIdx].nStandardGroup] - stTestData.dbEachResultGroup[stTestOpt.stInput_Tilt[nIdx].nSubtractGroup]);
		}

		BOOL	bMinUse = FALSE;// stTestOpt.stInput_Tilt[nIdx].bEnable;
		BOOL	bMaxUse = stTestOpt.stInput_Tilt[nIdx].bEnable;

		double	dbMinDev = FALSE;// stTestOpt.stInput_Tilt[nIdx].dbValue;
		double	dbMaxDev = stTestOpt.stInput_Tilt[nIdx].dbValue;
		double	dbValue = stTestData.dbTiltdata[nIdx];

		// 판정
		stTestData.nEachTiltResult[nIdx] = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, dbValue);

		stTestData.nTiltResult &= stTestData.nEachTiltResult[nIdx];
	}
	/*
	for (UINT nIdx = 0; nIdx < ROI_SFR_GROUP_Max; nIdx++)
	{
		UINT nGroupCnt = 0;

		// 01F
		for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
		{
			if (TRUE == stTestOpt.stRegion[nROI].bEnable && nIdx == stTestOpt.stRegion[nROI].nSFR_Group)
			{
				nGroupCnt++;
			}
		}

		if (nGroupCnt > 0)
		{
			double	*dbMinValue = new double[nGroupCnt];
			UINT nCnt = 0;

			// 01F
			for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
			{
				if (TRUE == stTestOpt.stRegion[nROI].bEnable && nIdx == stTestOpt.stRegion[nROI].nSFR_Group)
				{
					dbMinValue[nCnt] = stTestData.dbValue[nROI];
					nCnt++;
				}
			}

			stTestData.dbMinValue[nIdx] = GetMin(dbMinValue, nGroupCnt);

			delete[] dbMinValue;
		}

		BOOL	bMinUse = stTestOpt.stSpec_F_Min[nIdx].bEnable;
		BOOL	bMaxUse = stTestOpt.stSpec_F_Max[nIdx].bEnable;

		double	dbMinDev = stTestOpt.stSpec_F_Min[nIdx].dbValue;
		double	dbMaxDev = stTestOpt.stSpec_F_Max[nIdx].dbValue;
		double	dbValue = stTestData.dbMinValue[nIdx];

		// 판정
		stTestData.nEachResult_G[nIdx] = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, dbValue);
	}

	double dbX1Tiltdata = 0.0;
	double dbX2Tiltdata = 0.0;
	double dbY1Tiltdata = 0.0;
	double dbY2Tiltdata = 0.0;

	stTestData.nTiltResult = TR_Pass;

	//Tilt X1, X2, Y1, Y2 데이터 넘기기(절대값)
	dbX1Tiltdata = abs(stTestData.dbValue[stTestOpt.nX1_Tilt_A] - stTestData.dbValue[stTestOpt.nX1_Tilt_B]);
	dbX2Tiltdata = abs(stTestData.dbValue[stTestOpt.nX2_Tilt_A] - stTestData.dbValue[stTestOpt.nX2_Tilt_B]);
	dbY1Tiltdata = abs(stTestData.dbValue[stTestOpt.nY1_Tilt_A] - stTestData.dbValue[stTestOpt.nY1_Tilt_B]);
	dbY2Tiltdata = abs(stTestData.dbValue[stTestOpt.nY2_Tilt_A] - stTestData.dbValue[stTestOpt.nY2_Tilt_B]);

	stTestData.dbTiltdata[0] = dbX1Tiltdata;
	stTestData.dbTiltdata[1] = dbX2Tiltdata;
	stTestData.dbTiltdata[2] = dbY1Tiltdata;
	stTestData.dbTiltdata[3] = dbY2Tiltdata;

	for (UINT nIdx = 0; nIdx < ITM_SFR_TiltMax; nIdx++)
	{
		BOOL	bMinUse = stTestOpt.stSpec_Tilt_Min[nIdx].bEnable;
		BOOL	bMaxUse = stTestOpt.stSpec_Tilt_Max[nIdx].bEnable;

		double	dbMinDev = stTestOpt.stSpec_Tilt_Min[nIdx].dbValue;
		double	dbMaxDev = stTestOpt.stSpec_Tilt_Max[nIdx].dbValue;
		double	dbValue = stTestData.dbTiltdata[nIdx];

		// 판정
		stTestData.nEachTiltResult[nIdx] = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, dbValue);

		stTestData.nTiltResult &= stTestData.nEachTiltResult[nIdx];
	}
	*/
	cvReleaseImage(&TestRectImage);

	return lReturn;
}

//=============================================================================
// Method		: OnTestProcess_SFRROI
// Access		: public  
// Returns		: void
// Parameter	: __in IplImage * pFrameImage
// Parameter	: __in ST_SFR_Opt stOpt
// Parameter	: __out ST_SFR_Data & stTestData
// Qualifier	:
// Last Update	: 2018/3/1 - 12:26
// Desc.		:
//=============================================================================
void CTestManager_Test::OnTestProcess_SFRROI(__in IplImage* pFrameImage, __in ST_SFR_Opt stOpt, __out ST_SFR_Data &stTestData)
{
	if (pFrameImage == NULL)
		return;
	
	CvMemStorage* pContourStorage = cvCreateMemStorage(0);
	CvSeq *pContour = 0;
	CvSeq *pContour_Center = 0;
	IplImage* pSrcImage = cvCreateImage(cvGetSize(pFrameImage), IPL_DEPTH_8U, 1);
	IplImage* pBinaryImage = cvCreateImage(cvGetSize(pFrameImage), IPL_DEPTH_8U, 1);
	IplImage* pContoresImage = NULL;
	IplImage* eig_image = NULL;
	IplImage* temp_image = NULL;

	// 광축을 구해서 Offset 추가
	CvPoint Offset_for_Auto;
	Offset_for_Auto.x = 0;
	Offset_for_Auto.y = 0;

	cvCvtColor(pFrameImage, pSrcImage, CV_RGB2GRAY);

	//cvThreshold(pSrcImage, pBinaryImage, 0, 255, CV_THRESH_OTSU); // 20180305 HTH Modify
	cvAdaptiveThreshold(pSrcImage, pBinaryImage, 255, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 71, 5);

	cvNot(pBinaryImage, pBinaryImage);
	cvRectangle(pBinaryImage, cvPoint(0, 0), cvPoint(pBinaryImage->width - 3, pBinaryImage->height - 3), CV_RGB(0, 0, 0), 6);
	cvFindContours(pBinaryImage, pContourStorage, &pContour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
	//cvFindContours(pBinaryImage, pContourStorage, &pContour_Center, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	pContour_Center = pContour;

	//광축 구하기
	CvRect rect;
	double area = 0, arcCount = 0;
	CvPoint nCenterPoint;

	nCenterPoint.x = 0;
	nCenterPoint.y = 0;
	double dbMin_Dist = 99999;
	double dbDist = 0;
	double circularity = 0;
	int rtCenterX = 0;
	int rtCenterY = 0;
	CvPoint StartPoint;
	CvPoint EndPoint;

	int iStartX = pSrcImage->width / 4;
	int iStartY = pSrcImage->height / 4;

	int iEndX = pSrcImage->width * 3/ 4;
	int iEndY = pSrcImage->height * 3/ 4;

	for (; pContour_Center != 0; pContour_Center = pContour_Center->h_next)
	{
		rect = cvContourBoundingRect(pContour_Center, 1);
		area = cvContourArea(pContour_Center, CV_WHOLE_SEQ);
		arcCount = cvArcLength(pContour_Center, CV_WHOLE_SEQ, -1);
		circularity = (4.0 * 3.14 * area) / (arcCount*arcCount);

		rtCenterX = rect.x + rect.width / 2;
		rtCenterY = rect.y + rect.height / 2;

		StartPoint.x = rect.x;
		StartPoint.y = rect.y;

		EndPoint.x = rect.x + rect.width;
		EndPoint.y = rect.y + rect.height;

		if (rect.x > iStartX && rect.x + rect.width < iEndX
			&& rect.y > iStartY && rect.y + rect.height < iEndY)
		{
			dbDist = GetDistance(pSrcImage->width / 2, pSrcImage->height / 2, rtCenterX, rtCenterY);

			//원에 가깝고
			if (circularity > 0.7)
			{
				// 이미지 중심에서 제일 가까운 오브젝트 탐색
				if (dbMin_Dist > dbDist)
				{
					dbMin_Dist = dbDist;

					nCenterPoint.x = rtCenterX;
					nCenterPoint.y = rtCenterY;

					StartPoint.x = nCenterPoint.x - rect.width / 2;
					StartPoint.y = nCenterPoint.y - rect.height / 2;

					EndPoint.x = nCenterPoint.x + rect.width / 2;
					EndPoint.y = nCenterPoint.y + rect.height / 2;
					
				}
			}
		}
	}

	if (pSrcImage->width < nCenterPoint.x || 0 > nCenterPoint.x)
	{
		nCenterPoint.x = pSrcImage->width / 2;
	}

	if (pSrcImage->height < nCenterPoint.y || 0 > nCenterPoint.y)
	{
		nCenterPoint.y = pSrcImage->height / 2;
	}

	Offset_for_Auto.x = nCenterPoint.x - (pSrcImage->width / 2);
	Offset_for_Auto.y = nCenterPoint.y - (pSrcImage->height / 2);

	if (m_pstRecipeinfo->ModelType == Model_OMS_Entry) //entry 일 때
	{
		for (int i = 0; i < ROI_SFR_Max; i++)
		{
			if (stOpt.stRegion[i].bEnable)
			{
				stTestData.rtTestPicROI[i].left		= stOpt.stRegion[i].rtROI.left + Offset_for_Auto.x;
				stTestData.rtTestPicROI[i].top		= stOpt.stRegion[i].rtROI.top + Offset_for_Auto.y;
				stTestData.rtTestPicROI[i].right	= stOpt.stRegion[i].rtROI.right + Offset_for_Auto.x;
				stTestData.rtTestPicROI[i].bottom	= stOpt.stRegion[i].rtROI.bottom + Offset_for_Auto.y;
			}
		}
	}
	else
	{
		// ROI 마다 자동으로 찾아버리기
		CvRect rcContour;
		CRect rtROI[ROI_SFR_Max];
		for (int i = 0; i < ROI_SFR_Max; i++)
		{
			CvPoint Corr_Center;
			if (stOpt.stRegion[i].bEnable)
			{
				int _W = stOpt.stRegion[i].rtROI.right - stOpt.stRegion[i].rtROI.left;
				int _H = stOpt.stRegion[i].rtROI.bottom - stOpt.stRegion[i].rtROI.top;
				CvSeq *pContour_Tmp = pContour;
				rtROI[i] = stOpt.stRegion[i].rtROI;
				CvPoint ROICenterPoint;
				ROICenterPoint.x = (rtROI[i].left + rtROI[i].right) / 2 + Offset_for_Auto.x;
				ROICenterPoint.y = (rtROI[i].top + rtROI[i].bottom) / 2 + Offset_for_Auto.y;
				// ROI에 근접한 Contours 구하기
				double Old_Dist = 99999;
				double Max_Size = 0;

				CvRect Near_ContP;
				BOOL TRUE_Flag = FALSE;
				for (; pContour_Tmp != 0; pContour_Tmp = pContour_Tmp->h_next)
				{
					rcContour = cvBoundingRect(pContour_Tmp, 1);
					double Size_Rect = rcContour.width * rcContour.height;
					double Dist_Contour2ROI = GetDistance(rcContour.x + (rcContour.width / 2), rcContour.y + (rcContour.height / 2), ROICenterPoint.x, ROICenterPoint.y);
					if (rcContour.height > _H && rcContour.width > _W)
					{
						if (Old_Dist > Dist_Contour2ROI && rcContour.width < pFrameImage->width / 2 && rcContour.height < pFrameImage->height / 2)
						{

							Old_Dist = Dist_Contour2ROI;
							Near_ContP.x = rcContour.x - 5; // 여유 +-5 pixel
							Near_ContP.y = rcContour.y - 5; // 여유 +-5 pixel
							Near_ContP.width = rcContour.width + 10;
							Near_ContP.height = rcContour.height + 10;
							TRUE_Flag = TRUE;
						}
					}

				}
				if (TRUE_Flag == FALSE)
				{
					TRACE(_T("Near_Point가 없습니다.\n"));

					// KHO 태현아... 여기서 안지우고 마지막에 해도 되는거 맞지??? 180514
					// 				cvReleaseMemStorage(&pContourStorage);
					// 				cvReleaseImage(&pSrcImage);
					// 				cvReleaseImage(&pBinaryImage);

					stTestData.rtTestPicROI[i].left = stOpt.stRegion[i].rtROI.left;
					stTestData.rtTestPicROI[i].top = stOpt.stRegion[i].rtROI.top;
					stTestData.rtTestPicROI[i].right = stOpt.stRegion[i].rtROI.right;
					stTestData.rtTestPicROI[i].bottom = stOpt.stRegion[i].rtROI.bottom;


				}
				else
				{
					// 근접한 Contours 이미지 따기
					pContoresImage = cvCreateImage(cvSize(Near_ContP.width, Near_ContP.height), IPL_DEPTH_8U, 1);
					eig_image = cvCreateImage(cvSize(Near_ContP.width, Near_ContP.height), IPL_DEPTH_32F, 1);
					temp_image = cvCreateImage(cvSize(Near_ContP.width, Near_ContP.height), IPL_DEPTH_32F, 1);
					if (Near_ContP.x < 0)
						Near_ContP.x = 0;
					if (Near_ContP.y < 0)
						Near_ContP.y = 0;
					if (Near_ContP.x + Near_ContP.width > pFrameImage->width - 1)
						Near_ContP.x = pFrameImage->width - Near_ContP.width - 1;
					if (Near_ContP.y + Near_ContP.height > pFrameImage->height - 1)
						Near_ContP.y = pFrameImage->height - Near_ContP.height - 1;
					cvSetImageROI(pSrcImage, Near_ContP);
					cvCopy(pSrcImage, pContoresImage);
					cvResetImageROI(pSrcImage);
					// 스무딩 처리
					cvDilate(pContoresImage, pContoresImage, 0, 3);
					cvErode(pContoresImage, pContoresImage, 0, 3);
					//cvSmooth(pContoresImage, pContoresImage);
					cvNormalize(pContoresImage, pContoresImage, 0, 255, CV_MINMAX);
					cvDilate(pContoresImage, pContoresImage, 0, 3);
					cvErode(pContoresImage, pContoresImage, 0, 3);

					int MAX_CORNERS = 2000;
					CvPoint2D32f* corners = new CvPoint2D32f[MAX_CORNERS];
					CvPoint2D32f* corners_Final = new CvPoint2D32f[MAX_CORNERS];

					int Corners_Cnt = 0;
					// 코너 찾기
					cvGoodFeaturesToTrack(pContoresImage, eig_image, temp_image, corners, &MAX_CORNERS, 0.12, 30.0, 0, 7, 0, 0.04);
					for (int j = 0; j < MAX_CORNERS; j++)
					{
						if (!(corners[j].x > pContoresImage->width / 2 - 10 && corners[j].x < pContoresImage->width / 2 + 10 &&
							corners[j].y > pContoresImage->height / 2 - 10 && corners[j].y < pContoresImage->height / 2 + 10))
						{
							corners_Final[Corners_Cnt] = corners[j];
							cvCircle(pContoresImage, cvPoint((int)corners[j].x, (int)corners[j].y), 4, CV_RGB(255, 0, 0), 1);
							Corners_Cnt++;
						}
					}

					// 월드좌표 변환
					for (int j = 0; j < Corners_Cnt; j++)
					{
						corners_Final[j].x += Near_ContP.x;
						corners_Final[j].y += Near_ContP.y;
					}
					double Dist_1st = 0.0;
					CvPoint Dist_1stPoint;
					double Dist_2rd = 0.0;
					CvPoint Dist_2rdPoint;

					// W가 큰 ROI 일때 (가로SFR) => X축, Y축에 대한 거리 추출 가중치가 달라짐
					if (rtROI[i].Width() > rtROI[i].Height())
					{
						// 1st
						double Max_Tmp = 99999;
						for (int k = 0; k < Corners_Cnt; k++)
						{
							int Pointy_Mul = (int)((corners_Final[k].y - (float)ROICenterPoint.y) * 20);
							Dist_1st = GetDistance(ROICenterPoint.x, ROICenterPoint.y, (int)corners_Final[k].x, (int)(corners_Final[k].y /*- (float)Pointy_Mul*/));
							if (Max_Tmp > Dist_1st)
							{
								Max_Tmp = Dist_1st;
								Dist_1stPoint.x = (int)corners_Final[k].x;
								Dist_1stPoint.y = (int)corners_Final[k].y;
							}
						}
						// 2rd
						Max_Tmp = 99999;
						for (int k = 0; k < Corners_Cnt; k++)
						{
							// 1st Point 를 제외한 가장 가까운 Point = 2rd
							if (corners_Final[k].x != Dist_1stPoint.x && corners_Final[k].y != Dist_1stPoint.y && (corners_Final[k].y < Dist_1stPoint.y + _H && corners_Final[k].y > Dist_1stPoint.y - _H))
							{
								// 가중치를 적용한 거리 계산
								int Pointy_Mul = (int)((corners_Final[k].y - (float)ROICenterPoint.y) * 20);
								Dist_2rd = GetDistance(ROICenterPoint.x, ROICenterPoint.y, (int)corners_Final[k].x, (int)(corners_Final[k].y) /*- (float)Pointy_Mul)*/);
								if (Max_Tmp > Dist_2rd)
								{
									Max_Tmp = Dist_2rd;
									Dist_2rdPoint.x = (int)corners_Final[k].x;
									Dist_2rdPoint.y = (int)corners_Final[k].y;
								}
							}
						}
					}
					// H가 큰 ROI 일때 (세로SFR)
					else if (rtROI[i].Width() < rtROI[i].Height())
					{
						// 1st
						double Max_Tmp = 99999;
						for (int k = 0; k < Corners_Cnt; k++)
						{
							int Pointy_Mul = (int)((corners_Final[k].x - (float)ROICenterPoint.x) * 20);
							Dist_1st = GetDistance(ROICenterPoint.x, ROICenterPoint.y, (int)(corners_Final[k].x)/*+ (float)Pointy_Mul)*/, (int)corners_Final[k].y);
							if (Max_Tmp > Dist_1st)
							{
								Max_Tmp = Dist_1st;
								Dist_1stPoint.x = (int)corners_Final[k].x;
								Dist_1stPoint.y = (int)corners_Final[k].y;
							}
						}
						// 2rd
						Max_Tmp = 99999;
						for (int k = 0; k < Corners_Cnt; k++)
						{
							// 1st Point 를 제외한 가장 가까운 Point = 2rd
							if (corners_Final[k].x != Dist_1stPoint.x && corners_Final[k].y != Dist_1stPoint.y && (corners_Final[k].x <= Dist_1stPoint.x + _W && corners_Final[k].x >= Dist_1stPoint.x - _W))
							{
								// 가중치를 적용한 거리 계산
								int Pointy_Mul = (int)((corners_Final[k].x - (float)ROICenterPoint.x) * 20);
								Dist_2rd = GetDistance(ROICenterPoint.x, ROICenterPoint.y, (int)(corners_Final[k].x /*+ Pointy_Mul*/), (int)corners_Final[k].y);
								if (Max_Tmp > Dist_2rd)
								{
									Max_Tmp = Dist_2rd;
									Dist_2rdPoint.x = (int)corners_Final[k].x;
									Dist_2rdPoint.y = (int)corners_Final[k].y;
								}
							}
						}
					}
					Corr_Center.x = (int)((Dist_1stPoint.x + Dist_2rdPoint.x) / 2.0);
					Corr_Center.y = (int)((Dist_1stPoint.y + Dist_2rdPoint.y) / 2.0);
					//cvCircle(pContoresImage, Corr_Center, 4, CV_RGB(255, 255, 255), 1);

					// 보정값 넣기
					stTestData.rtTestPicROI[i].left = Corr_Center.x - (_W / 2);
					stTestData.rtTestPicROI[i].top = Corr_Center.y - (_H / 2);
					stTestData.rtTestPicROI[i].right = stTestData.rtTestPicROI[i].left + _W;
					stTestData.rtTestPicROI[i].bottom = stTestData.rtTestPicROI[i].top + _H;

					delete[] corners;
					delete[] corners_Final;

					cvReleaseImage(&eig_image);
					cvReleaseImage(&temp_image);
					cvReleaseImage(&pContoresImage);
					//cvRectangle(pSrcImage, cvPoint(stTestData.rtTestPicROI[i].left, stTestData.rtTestPicROI[i].top), cvPoint(stTestData.rtTestPicROI[i].right, stTestData.rtTestPicROI[i].bottom),CV_RGB(200,200,200),2 );
				}
			}
		}
	}

	cvReleaseMemStorage(&pContourStorage);
	cvReleaseImage(&pSrcImage);
	cvReleaseImage(&pBinaryImage);

}

//=============================================================================
// Method		: DefectPixelFunc
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImage8bitBuf
// Parameter	: __in LPWORD pImage12bitBuf
// Parameter	: __in int iImgW
// Parameter	: __in int iImgH
// Parameter	: __in UINT nLightMode
// Parameter	: __in stDPList stDefectList
// Parameter	: __in ST_DefectPixel_Opt stTestOpt
// Parameter	: __out ST_DefectPixel_Data & stTestData
// Qualifier	:
// Last Update	: 2018/3/3 - 9:41
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::DefectPixelFunc(__in LPBYTE pImage8bitBuf, __in LPWORD pImage12bitBuf, __in int iImgW, __in int iImgH, __in UINT nLightMode, __in stDPList& stDefectList, __in ST_DefectPixel_Opt stTestOpt, __out ST_DefectPixel_Data &stTestData)
{
	if (NULL == m_pstRecipeinfo)
		return RC_Invalid_Handle;

	if (NULL == pImage8bitBuf || iImgW < 1 || iImgH < 1)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	IplImage *TestRectImage = cvCreateImage(cvSize(iImgW, iImgH), IPL_DEPTH_16U, 1);
	memcpy(TestRectImage->imageData, pImage12bitBuf, TestRectImage->width * TestRectImage->height * 2);

	int iVHP_Th = stTestOpt.iThreshold[Spec_Defect_VeryHotPixel];
	int iHP_Th	= stTestOpt.iThreshold[Spec_Defect_HotPixel];
	int iVBP_Th = stTestOpt.iThreshold[Spec_Defect_VeryBrightPixel];
	int iBP_Th	= stTestOpt.iThreshold[Spec_Defect_BrightPixel];
	int iVDP_Th = stTestOpt.iThreshold[Spec_Defect_VeryDeadPixel];
	int iDP_Th	= stTestOpt.iThreshold[Spec_Defect_DeadPixel];
	int iRC_Th	= stTestOpt.iThreshold[Spec_Defect_RowCol];

 	m_TI_DefectPixel.DPDetection(TestRectImage, stDefectList, nLightMode, iVHP_Th, iHP_Th, iVBP_Th, iBP_Th, iVDP_Th, iDP_Th, iRC_Th, 4095, stTestOpt.iBlockSize);
 
 	// 판정
//  	for (UINT nIdx = 0; nIdx < Spec_Defect_Max; nIdx++)
//  	{
//  		BOOL	bMinUse = stTestOpt.stSpec_Min[nIdx].bEnable;
//  		BOOL	bMaxUse = stTestOpt.stSpec_Max[nIdx].bEnable;
//  
//  		double dbMinDev = stTestOpt.stSpec_Min[nIdx].dbValue;
//  		double dbMaxDev = stTestOpt.stSpec_Max[nIdx].dbValue;
//  
//  		// 판정
//  		stTestData.nEachResult[nIdx] = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stTestData.nFailCount[nIdx]);
//  
//  		stTestData.nResult &= stTestData.nEachResult[nIdx];
//  	}

	cvReleaseImage(&TestRectImage);

	return lReturn;
}

//=============================================================================
// Method		: ParticleFunc
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImage8bitBuf
// Parameter	: __in LPWORD pImage12bitBuf
// Parameter	: __in int iImgW
// Parameter	: __in int iImgH
// Parameter	: __in ST_Particle_Opt stTestOpt
// Parameter	: __out ST_Particle_Data & stTestData
// Qualifier	:
// Last Update	: 2018/2/27 - 9:32
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::ParticleFunc(__in LPBYTE pImage8bitBuf, __in LPWORD pImage12bitBuf, __in int iImgW, __in int iImgH, __in ST_Particle_Opt stTestOpt, __out ST_Particle_Data &stTestData)
{
	if (NULL == m_pstRecipeinfo)
		return RC_Invalid_Handle;

	if (NULL == pImage8bitBuf || iImgW < 1 || iImgH < 1)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	ST_CAN_Particle_Opt		stOption;
	ST_CAN_Particle_Result	stResult;

	stOption.fSensitivity = (float)stTestOpt.dbDustDis;
	stOption.iEgdeW = stTestOpt.iEdgeW;
	stOption.iEgdeH = stTestOpt.iEdgeH;

	for (UINT nIdx = 0; nIdx < ROI_Particle_Max; nIdx++)
	{
		stOption.bEllipse[nIdx]				= stTestOpt.stRegion[nIdx].bEllipse ? true : false;
		stOption.fThreConcentration[nIdx]	= (float)stTestOpt.stRegion[nIdx].dbBruiseConc;
		stOption.fThreSize[nIdx]			= (float)stTestOpt.stRegion[nIdx].dbBruiseSize;

		stOption.stROI[nIdx].sLeft			= (unsigned short)stTestOpt.stRegion[nIdx].rtROI.left;
		stOption.stROI[nIdx].sTop			= (unsigned short)stTestOpt.stRegion[nIdx].rtROI.top;
		stOption.stROI[nIdx].sWidth			= (unsigned short)stTestOpt.stRegion[nIdx].rtROI.Width();
		stOption.stROI[nIdx].sHeight		= (unsigned short)stTestOpt.stRegion[nIdx].rtROI.Height();
	}

	m_TI_Particle.Particle_Test(pImage12bitBuf, iImgW, iImgH, stOption, stResult);

	int Region_Stain = 0;
	// 영역별 카운트
	for (int i = 0; i < stResult.byRoiCnt; i++)
	{
		Region_Stain = stResult.byType[i];
		stTestData.nFailTypeCount[Region_Stain]++;
	}

	if (stResult.byRoiCnt > 0)
	{
		double Max_ConcValue = 0.0;
		// 이물 파트별 Max 값만 저장
		for (int i = 0; i < ROI_Particle_Max; i++)
		{
			Max_ConcValue = 0.0;
			for (int j = 0; j < stResult.byRoiCnt; j++)
			{
				// 이물의 위치는??
				Region_Stain = stResult.byType[j];
				if (Region_Stain == i)
				{
					if (stResult.fConcentration[j] > Max_ConcValue)
						Max_ConcValue = stResult.fConcentration[j];
				}
			}
			// 영역에서 가장 어두운 곳의 값
			stTestData.dFailTypeConcentration[i] = Max_ConcValue;
		}
	}
	// 검출된 이물 갯수
	stTestData.nFailCount = 0;

	for (UINT nIdx = 0; nIdx < stResult.byRoiCnt; nIdx++)
	{
		stTestData.nFailCount++;
		stTestData.nFailType[nIdx]			= (UINT)stResult.byType[nIdx];
		stTestData.dbConcentration[nIdx]	= (double)stResult.fConcentration[nIdx];
		stTestData.rtFailROI[nIdx]			= stResult.stROI[nIdx];
	}

	// 양불 판정
	if (stTestOpt.stSpec_Min.dbValue <= stTestData.nFailCount && stTestOpt.stSpec_Max.dbValue >= stTestData.nFailCount)
	{
		stTestData.nResult = TR_Pass;
	}
	else
	{
		stTestData.nResult = TR_Fail;
	}

	return lReturn;
}

//=============================================================================
// Method		: IntensityFunc
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImage8bitBuf
// Parameter	: __in LPWORD pImage12bitBuf
// Parameter	: __in int iImgW
// Parameter	: __in int iImgH
// Parameter	: __in ST_Intensity_Opt stTestOpt
// Parameter	: __out ST_Intensity_Data & stTestData
// Qualifier	:
// Last Update	: 2018/2/27 - 9:32
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::IntensityFunc(__in LPBYTE pImage8bitBuf, __in LPWORD pImage12bitBuf, __in int iImgW, __in int iImgH, __in ST_Intensity_Opt stTestOpt, __out ST_Intensity_Data &stTestData)
{
	if (NULL == m_pstRecipeinfo)
		return RC_Invalid_Handle;

	if (NULL == pImage8bitBuf || iImgW < 1 || iImgH < 1)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	for (UINT nIdx = 0; nIdx < ROI_Intencity_Max; nIdx++)
	{
		stTestData.bUse[nIdx] = stTestOpt.stRegion[nIdx].bEnable;

		if (TRUE == stTestOpt.stRegion[nIdx].bEnable)
		{
			ST_ROI stROI;

			stROI.sLeft		= (unsigned short)stTestOpt.stRegion[nIdx].rtROI.left;
			stROI.sTop		= (unsigned short)stTestOpt.stRegion[nIdx].rtROI.top;
			stROI.sWidth	= (unsigned short)stTestOpt.stRegion[nIdx].rtROI.Width();
			stROI.sHeight	= (unsigned short)stTestOpt.stRegion[nIdx].rtROI.Height();
			
			lReturn = m_TI_SNR.SNR_Test(pImage12bitBuf, iImgW, iImgH, stROI, stTestData.fSignal[nIdx], stTestData.fNoise[nIdx]);
		}
	}

	// 측정값 산출
	for (UINT nIdx = 0; nIdx < ROI_Intencity_Max; nIdx++)
	{
		stTestData.fPercent[nIdx] = ((float)stTestData.fSignal[nIdx] / (float)stTestData.fSignal[ROI_Intencity_CC] * 100.0f);
	}

	// 판정
	for (UINT nIdx = 0; nIdx < ROI_Intencity_Max; nIdx++)
	{
		BOOL	bMinUse = stTestOpt.stSpec_Min[nIdx].bEnable;
		BOOL	bMaxUse = stTestOpt.stSpec_Max[nIdx].bEnable;

		double dbMinDev = stTestOpt.stSpec_Min[nIdx].dbValue;
		double dbMaxDev = stTestOpt.stSpec_Max[nIdx].dbValue;

		// 판정
		stTestData.nEachResult[nIdx] = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stTestData.fPercent[nIdx]);

		stTestData.nResult &= stTestData.nEachResult[nIdx];
	}

	return lReturn;
}

//=============================================================================
// Method		: ShadingFunc
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImage8bitBuf
// Parameter	: __in LPWORD pImage12bitBuf
// Parameter	: __in int iImgW
// Parameter	: __in int iImgH
// Parameter	: __in ST_Shading_Opt stTestOpt
// Parameter	: __out ST_Shading_Data & stTestData
// Qualifier	:
// Last Update	: 2018/2/27 - 9:32
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::ShadingFunc(__in LPBYTE pImage8bitBuf, __in LPWORD pImage12bitBuf, __in int iImgW, __in int iImgH, __in ST_Shading_Opt stTestOpt, __out ST_Shading_Data &stTestData)
{
	if (NULL == m_pstRecipeinfo)
		return RC_Invalid_Handle;

	if (NULL == pImage8bitBuf || iImgW < 1 || iImgH < 1)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	stTestData.nResult = TR_Pass;

	UINT nCount = 0;

	stTestData.Reset();
	for (UINT nIdx = 0; nIdx < ROI_Shading_Max; nIdx++)
	{
		stTestData.bUse[nIdx] = stTestOpt.bUseIndex[nIdx];
		if (Type_Shading_NotUes != stTestOpt.nType[nIdx])
		{
			nCount++;
		}
	}


	lReturn = m_TI_Shading.Shading_Test_New(pImage12bitBuf, iImgW, iImgH, stTestOpt.stShading_Rect.rt_TestSectionFullRect, stTestOpt.bUseIndex, stTestData.sSignal, stTestData.fNoise);

	UINT nStandardIndex[Index_Shading_Max] = { 0, };

	// 측정 기준 값 찾기
	for (UINT nIdx = 0; nIdx < Index_Shading_Max; nIdx++)
	{
		if (Type_Shading_Standard == stTestOpt.nType[nIndexVer_CT[nIdx]])
		{
			nStandardIndex[IDX_Shading_Ver_CT] = nIndexVer_CT[nIdx];
		}
		
		if (Type_Shading_Standard == stTestOpt.nType[nIndexVer_CB[nIdx]])
		{
			nStandardIndex[IDX_Shading_Ver_CB] = nIndexVer_CB[nIdx];
		}

		if (Type_Shading_Standard == stTestOpt.nType[nIndexHov_LC[nIdx]])
		{
			nStandardIndex[IDX_Shading_Hov_LC] = nIndexHov_LC[nIdx];
		}

		if (Type_Shading_Standard == stTestOpt.nType[nIndexHov_RC[nIdx]])
		{
			nStandardIndex[IDX_Shading_Hov_RC] = nIndexHov_RC[nIdx];
		}

		if (Type_Shading_Standard == stTestOpt.nType[nIndexDig_LT[nIdx]])
		{
			nStandardIndex[IDX_Shading_Dig_LT] = nIndexDig_LT[nIdx];
		}

		if (Type_Shading_Standard == stTestOpt.nType[nIndexDig_LB[nIdx]])
		{
			nStandardIndex[IDX_Shading_Dig_LB] = nIndexDig_LB[nIdx];
		}

		if (Type_Shading_Standard == stTestOpt.nType[nIndexDig_RT[nIdx]])
		{
			nStandardIndex[IDX_Shading_Dig_RT] = nIndexDig_RT[nIdx];
		}

		if (Type_Shading_Standard == stTestOpt.nType[nIndexDig_RB[nIdx]])
		{
			nStandardIndex[IDX_Shading_Dig_RB] = nIndexDig_RB[nIdx];
		}
	}

	// 측정값 산출
	for (UINT nIdx = 0; nIdx < Index_Shading_Max; nIdx++)
	{
		stTestData.fPercent[nIndexVer_CT[nIdx]] = ((float)stTestData.sSignal[nIndexVer_CT[nIdx]] / (float)stTestData.sSignal[nStandardIndex[IDX_Shading_Ver_CT]] * 100.0f);
		stTestData.fPercent[nIndexVer_CB[nIdx]] = ((float)stTestData.sSignal[nIndexVer_CB[nIdx]] / (float)stTestData.sSignal[nStandardIndex[IDX_Shading_Ver_CB]] * 100.0f);
		stTestData.fPercent[nIndexHov_LC[nIdx]] = ((float)stTestData.sSignal[nIndexHov_LC[nIdx]] / (float)stTestData.sSignal[nStandardIndex[IDX_Shading_Hov_LC]] * 100.0f);
		stTestData.fPercent[nIndexHov_RC[nIdx]] = ((float)stTestData.sSignal[nIndexHov_RC[nIdx]] / (float)stTestData.sSignal[nStandardIndex[IDX_Shading_Hov_RC]] * 100.0f);
		stTestData.fPercent[nIndexDig_LT[nIdx]] = ((float)stTestData.sSignal[nIndexDig_LT[nIdx]] / (float)stTestData.sSignal[nStandardIndex[IDX_Shading_Dig_LT]] * 100.0f);
		stTestData.fPercent[nIndexDig_LB[nIdx]] = ((float)stTestData.sSignal[nIndexDig_LB[nIdx]] / (float)stTestData.sSignal[nStandardIndex[IDX_Shading_Dig_LB]] * 100.0f);
		stTestData.fPercent[nIndexDig_RT[nIdx]] = ((float)stTestData.sSignal[nIndexDig_RT[nIdx]] / (float)stTestData.sSignal[nStandardIndex[IDX_Shading_Dig_RT]] * 100.0f);
		stTestData.fPercent[nIndexDig_RB[nIdx]] = ((float)stTestData.sSignal[nIndexDig_RB[nIdx]] / (float)stTestData.sSignal[nStandardIndex[IDX_Shading_Dig_RB]] * 100.0f);
	}

	BOOL	bMinUse;
	BOOL	bMaxUse;
	double  dbMinDev;
	double  dbMaxDev;
	// 판정
	for (UINT nIdx = 0; nIdx < IDX_Shading_Max; nIdx++)
	{
		for (int t = 0; t < 9; t++)
		{
			bMinUse = stTestOpt.stSpec_Min[GetItemIndexNumber(nIdx, t)].bEnable;
			bMaxUse = stTestOpt.stSpec_Max[GetItemIndexNumber(nIdx, t)].bEnable;

			dbMinDev = stTestOpt.stSpec_Min[GetItemIndexNumber(nIdx, t)].dbValue;
			dbMaxDev = stTestOpt.stSpec_Max[GetItemIndexNumber(nIdx, t)].dbValue;
			// 판정
			stTestData.nEachResult[GetItemIndexNumber(nIdx, t)] = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stTestData.fPercent[GetItemIndexNumber(nIdx, t)]);
			stTestData.nResult &= stTestData.nEachResult[GetItemIndexNumber(nIdx, t)];
		}
	}
	
	bMinUse = stTestOpt.stSpec_Min[ROI_Shading_CNT].bEnable;
	bMaxUse = stTestOpt.stSpec_Max[ROI_Shading_CNT].bEnable;

	dbMinDev = stTestOpt.stSpec_Min[ROI_Shading_CNT].dbValue;
	dbMaxDev = stTestOpt.stSpec_Max[ROI_Shading_CNT].dbValue;
	// 판정
	stTestData.nEachResult[ROI_Shading_CNT] = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, (double)stTestData.sSignal[ROI_Shading_CNT]);
	stTestData.nResult &= stTestData.nEachResult[ROI_Shading_CNT];
	

	return lReturn;
}
UINT CTestManager_Test::GetItemIndexNumber(UINT nType, UINT nRow)
{
	switch (nType)
	{
	case IDX_Shading_Ver_CT:
		return	nIndexVer_CT[nRow];
		break;
	case IDX_Shading_Ver_CB:
		return	nIndexVer_CB[nRow];
		break;
	case IDX_Shading_Hov_LC:
		return	nIndexHov_LC[nRow];
		break;
	case IDX_Shading_Hov_RC:
		return	nIndexHov_RC[nRow];
		break;
	case IDX_Shading_Dig_LT:
		return	nIndexDig_LT[nRow];
		break;
	case IDX_Shading_Dig_LB:
		return	nIndexDig_LB[nRow];
		break;
	case IDX_Shading_Dig_RT:
		return	nIndexDig_RT[nRow];
		break;
	case IDX_Shading_Dig_RB:
		return	nIndexDig_RB[nRow];
		break;
	default:
		break;
	}

	return	0;
}
//=============================================================================
// Method		: SNR_LightFunc
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImage8bitBuf
// Parameter	: __in LPWORD pImage12bitBuf
// Parameter	: __in int iImgW
// Parameter	: __in int iImgH
// Parameter	: __in ST_SNR_Light_Opt stTestOpt
// Parameter	: __out ST_SNR_Light_Data & stTestData
// Qualifier	:
// Last Update	: 2018/2/27 - 9:32
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::SNR_LightFunc(__in LPBYTE pImage8bitBuf, __in LPWORD pImage12bitBuf, __in int iImgW, __in int iImgH, __in ST_SNR_Light_Opt stTestOpt, __out ST_SNR_Light_Data &stTestData)
{
	if (NULL == m_pstRecipeinfo)
		return RC_Invalid_Handle;

	if (NULL == pImage8bitBuf || iImgW < 1 || iImgH < 1)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	UINT nCount = 0;

	stTestData.nResult = TR_Pass;

	for (UINT nIdx = 0; nIdx < ROI_SNR_Light_Max; nIdx++)
	{
		if (TRUE == stTestOpt.bIndex[nIdx])
		{
			nCount++;
		}
	}

	lReturn = m_TI_SNR_Light.SNR_Light_Test_New(pImage12bitBuf, iImgW, iImgH, stTestOpt.st_SNR_LightRect.rt_TestSectionFullRect, stTestOpt.bIndex, stTestData.sSignal, stTestData.fNoise);

	nCount	= 0;

	for (UINT nIdx = 0; nIdx < ROI_SNR_Light_Max; nIdx++)
	{
		stTestData.bUse[nIdx] = stTestOpt.bIndex[nIdx];

		if (TRUE == stTestOpt.bIndex[nIdx])
		{
			stTestData.dbEtcValue[nIdx] = 20.0 * log10((float)(stTestData.sSignal[nIdx] / stTestData.fNoise[nIdx]));

			if (0 == nCount)
			{
				stTestData.dbFinalValue = stTestData.dbEtcValue[nIdx];
			}
			else
			{
				// 최소값
				if (stTestData.dbFinalValue > stTestData.dbEtcValue[nIdx])
				{
					stTestData.dbFinalValue = stTestData.dbEtcValue[nIdx];
					stTestData.nFinalIndx	= nIdx;
				}
			}

			nCount++;
		}
	}

	BOOL	bMinUse = stTestOpt.stSpec_Min.bEnable;
	BOOL	bMaxUse = stTestOpt.stSpec_Max.bEnable;

	double dbMinDev = stTestOpt.stSpec_Min.dbValue;
	double dbMaxDev = stTestOpt.stSpec_Max.dbValue;

	// 판정
	stTestData.nResult = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stTestData.dbFinalValue);

	return lReturn;
}

//=============================================================================
// Method		: Partiecl_SNRFunc
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImage8bitBuf
// Parameter	: __in LPWORD pImage12bitBuf
// Parameter	: __in int iImgW
// Parameter	: __in int iImgH
// Parameter	: __in UINT nLightMode
// Parameter	: __in ST_Dynamic_Opt stTestOpt
// Parameter	: __out ST_Dynamic_Data & stTestData
// Qualifier	:
// Last Update	: 2018/3/3 - 9:05
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::Partiecl_SNRFunc(__in LPBYTE pImage8bitBuf, __in LPWORD pImage12bitBuf, __in int iImgW, __in int iImgH, __in UINT nLightMode, __in ST_Dynamic_Opt stTestOpt, __out ST_Dynamic_Data &stTestData)
{
	if (NULL == m_pstRecipeinfo)
		return RC_Invalid_Handle;

	if (NULL == pImage8bitBuf || iImgW < 1 || iImgH < 1)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	stTestData.nResultDR = TR_Pass;
	stTestData.nResultBW = TR_Pass;

	for (UINT nIdx = 0; nIdx < ROI_Par_SNR_Max; nIdx++)
	{
		stTestData.bUse[nIdx] = stTestOpt.stRegion[nIdx].bEnable;

		ST_ROI stROI;

		stROI.sLeft		= (unsigned short)stTestOpt.stRegion[nIdx].rtROI.left;
		stROI.sTop		= (unsigned short)stTestOpt.stRegion[nIdx].rtROI.top;
		stROI.sWidth	= (unsigned short)stTestOpt.stRegion[nIdx].rtROI.Width();
		stROI.sHeight	= (unsigned short)stTestOpt.stRegion[nIdx].rtROI.Height();

		lReturn = m_TI_SNR.SNR_Test(pImage12bitBuf, iImgW, iImgH, stROI, stTestData.sSignal[nLightMode][nIdx], stTestData.fNoise[nLightMode][nIdx]);
	}

	return lReturn;
}

//=============================================================================
// Method		: HotPixelFunc
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImage8bitBuf
// Parameter	: __in LPWORD pImage12bitBuf
// Parameter	: __in int iImgW
// Parameter	: __in int iImgH
// Parameter	: __in ST_HotPixel_Opt stTestOpt
// Parameter	: __out ST_HotPixel_Data & stTestData
// Qualifier	:
// Last Update	: 2018/2/27 - 9:32
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::HotPixelFunc(__in LPBYTE pImage8bitBuf, __in LPWORD pImage12bitBuf, __in int iImgW, __in int iImgH, __in ST_HotPixel_Opt	stTestOpt, __out ST_HotPixel_Data	 &stTestData)
{
	if (NULL == m_pstRecipeinfo)
		return RC_Invalid_Handle;

	if (NULL == pImage8bitBuf || iImgW < 1 || iImgH < 1)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	stTestData.Reset();//처음 데이터 값 초기화

	WORD wPointX[ROI_F_HO_Max] = { 0, };
	WORD wPointY[ROI_F_HO_Max] = { 0, };

	m_TI_HotPixel.HotPixel_Test(pImage12bitBuf, iImgW, iImgH, stTestOpt.dbThreshold, stTestData.iFailCount, wPointX, wPointY, stTestData.fConcen);

	for (UINT nIdx = 0; nIdx < (UINT)stTestData.iFailCount; nIdx++)
	{
		stTestData.rtFailROI[nIdx].left		= wPointX[nIdx] - 2;
		stTestData.rtFailROI[nIdx].right	= wPointX[nIdx] + 2;
		stTestData.rtFailROI[nIdx].top		= wPointY[nIdx] - 2;
		stTestData.rtFailROI[nIdx].bottom	= wPointY[nIdx] + 2;
	}

	BOOL bMinUsePassCount = stTestOpt.stSpec_Min.bEnable;
	BOOL bMaxUsePassCount = stTestOpt.stSpec_Max.bEnable;

	int iMinDevPassCount = stTestOpt.stSpec_Min.iValue;
	int iMaxDevPassCount = stTestOpt.stSpec_Max.iValue;

	// 판정
	stTestData.nResult = Get_MeasurmentData(bMinUsePassCount, bMaxUsePassCount, iMinDevPassCount, iMaxDevPassCount, stTestData.iFailCount);

	return lReturn;
}
//=============================================================================
// Method		: FPFunc
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImage8bitBuf
// Parameter	: __in LPWORD pImage12bitBuf
// Parameter	: __in int iImgW
// Parameter	: __in int iImgH
// Parameter	: __in ST_FPN_Opt stTestOpt
// Parameter	: __out ST_FPN_Data & stTestData
// Qualifier	:
// Last Update	: 2018/2/27 - 9:32
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::FPNFunc(__in LPBYTE pImage8bitBuf, __in LPWORD pImage12bitBuf, __in int iImgW, __in int iImgH, __in ST_FPN_Opt	stTestOpt, __out ST_FPN_Data	 &stTestData)
{
	if (NULL == m_pstRecipeinfo)
		return RC_Invalid_Handle;

	if (NULL == pImage8bitBuf || iImgW < 1 || iImgH < 1)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	stTestData.Reset();//처음 데이터 값 초기화


	m_TI_FPN.FPN_Test(pImage12bitBuf, iImgW, iImgH, stTestOpt.stRegion[ROI_FPN].rtROI, stTestOpt.dbThresholdX, stTestOpt.dbThresholdY, stTestData.iFailCount[Spec_FPN_X], stTestData.iFailCount[Spec_FPN_Y]);


	BOOL	bMinUsePassCountX	= stTestOpt.stSpec_Min[Spec_FPN_X].bEnable;
	BOOL	bMaxUsePassCountX	= stTestOpt.stSpec_Max[Spec_FPN_X].bEnable;

	int iMinDevPassCountX		= stTestOpt.stSpec_Min[Spec_FPN_X].iValue;
	int iMaxDevPassCountX		= stTestOpt.stSpec_Max[Spec_FPN_X].iValue;

	BOOL	bMinUsePassCountY	= stTestOpt.stSpec_Min[Spec_FPN_Y].bEnable;
	BOOL	bMaxUsePassCountY	= stTestOpt.stSpec_Max[Spec_FPN_Y].bEnable;

	int iMinDevPassCountY		= stTestOpt.stSpec_Min[Spec_FPN_Y].iValue;
	int iMaxDevPassCountY		= stTestOpt.stSpec_Max[Spec_FPN_Y].iValue;

	// 판정
	stTestData.nEachResult[Spec_FPN_X] = Get_MeasurmentData(bMinUsePassCountX, bMaxUsePassCountX, iMinDevPassCountX, iMaxDevPassCountX, stTestData.iFailCount[Spec_FPN_X]);
	stTestData.nEachResult[Spec_FPN_Y] = Get_MeasurmentData(bMinUsePassCountY, bMaxUsePassCountY, iMinDevPassCountY, iMaxDevPassCountY, stTestData.iFailCount[Spec_FPN_Y]);

	stTestData.nResult = stTestData.nEachResult[Spec_FPN_X] & stTestData.nEachResult[Spec_FPN_Y];

	return lReturn;
}

//=============================================================================
// Method		: T3D_depthFunc
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImage8bitBuf
// Parameter	: __in LPWORD pImage12bitBuf
// Parameter	: __in int iImgW
// Parameter	: __in int iImgH
// Parameter	: __in ST_3D_Depth_Opt stTestOpt
// Parameter	: __out ST_3D_Depth_Data & stTestData
// Qualifier	:
// Last Update	: 2018/5/12 - 18:10
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::T3D_depthFunc(__in LPBYTE pImage8bitBuf, __in LPWORD pImage12bitBuf, __in int iImgW, __in int iImgH, __in ST_3D_Depth_Opt	stTestOpt, __out ST_3D_Depth_Data	 &stTestData)
{
	if (NULL == m_pstRecipeinfo)
		return RC_Invalid_Handle;

	if (NULL == pImage8bitBuf || iImgW < 1 || iImgH < 1)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	stTestData.Reset();//처음 데이터 값 초기화

	m_TI_3D_Depth.T3D_Depth_Test(pImage12bitBuf, iImgW, iImgH, &stTestData.dbValue[Spec_BrightnessAvg], &stTestData.dbValue[Spec_Standard_deviation]);

	BOOL	bMinBrightnessAvg = stTestOpt.stSpec_Min[Spec_BrightnessAvg].bEnable;
	BOOL	bMaxBrightnessAvg = stTestOpt.stSpec_Max[Spec_BrightnessAvg].bEnable;

	double dbMinDevBrightnessAvg = stTestOpt.stSpec_Min[Spec_BrightnessAvg].dbValue;
	double dbMaxDevBrightnessAvg = stTestOpt.stSpec_Max[Spec_BrightnessAvg].dbValue;

	BOOL	bMinUseStandard_deviation = stTestOpt.stSpec_Min[Spec_Standard_deviation].bEnable;
	BOOL	bMaxUseStandard_deviation = stTestOpt.stSpec_Max[Spec_Standard_deviation].bEnable;

	double dbMinDevStandard_deviation = stTestOpt.stSpec_Min[Spec_Standard_deviation].dbValue;
	double dbMaxDevStandard_deviation = stTestOpt.stSpec_Max[Spec_Standard_deviation].dbValue;

	// 판정
	stTestData.nEachResult[Spec_BrightnessAvg] = Get_MeasurmentData(bMinBrightnessAvg, bMaxBrightnessAvg, dbMinDevBrightnessAvg, dbMaxDevBrightnessAvg, stTestData.dbValue[Spec_BrightnessAvg]);
	stTestData.nEachResult[Spec_Standard_deviation] = Get_MeasurmentData(bMinUseStandard_deviation, bMaxUseStandard_deviation, dbMinDevStandard_deviation, dbMaxDevStandard_deviation, stTestData.dbValue[Spec_Standard_deviation]);

	stTestData.nResult = stTestData.nEachResult[Spec_BrightnessAvg] & stTestData.nEachResult[Spec_Standard_deviation];

	return lReturn;
}


//=============================================================================
// Method		: Particle_EntryFunc
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImage8bitBuf
// Parameter	: __in LPWORD pImage12bitBuf
// Parameter	: __in int iImgW
// Parameter	: __in int iImgH
// Parameter	: __in ST_Particle_Opt stTestOpt
// Parameter	: __out ST_Particle_Data & stTestData
// Qualifier	:
// Last Update	: 2018/2/27 - 9:32
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::Particle_EntryFunc(__in LPBYTE pImage8bitBuf, __in LPWORD pImage12bitBuf, __in int iImgW, __in int iImgH, __in ST_Particle_Entry_Opt stTestOpt, __out ST_Particle_Entry_Data &stTestData)
{
	if (NULL == m_pstRecipeinfo)
		return RC_Invalid_Handle;

	if (NULL == pImage8bitBuf || iImgW < 1 || iImgH < 1)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	ST_CAN_Particle_Entry_Opt		stOption;
	ST_CAN_Particle_Entry_Result	stResult;

	stOption.fSensitivity = (float)stTestOpt.dbDustDis;

	for (UINT nIdx = 0; nIdx < ROI_Particle_Max; nIdx++)
	{
		stOption.bEllipse[nIdx] = stTestOpt.stRegion[nIdx].bEllipse ? true : false;
		stOption.fThreConcentration[nIdx] = (float)stTestOpt.stRegion[nIdx].dbBruiseConc;
		stOption.fThreSize[nIdx] = (float)stTestOpt.stRegion[nIdx].dbBruiseSize;

		stOption.stROI[nIdx].sLeft = (unsigned short)stTestOpt.stRegion[nIdx].rtROI.left;
		stOption.stROI[nIdx].sTop = (unsigned short)stTestOpt.stRegion[nIdx].rtROI.top;
		stOption.stROI[nIdx].sWidth = (unsigned short)stTestOpt.stRegion[nIdx].rtROI.Width();
		stOption.stROI[nIdx].sHeight = (unsigned short)stTestOpt.stRegion[nIdx].rtROI.Height();
	}

	m_TI_Particle_Entry.Particle_Test(pImage12bitBuf, iImgW, iImgH, stOption, stResult);

	// 검출된 이물 갯수
	stTestData.nFailCount = 0;

	for (UINT nIdx = 0; nIdx < stResult.byRoiCnt; nIdx++)
	{
		stTestData.nFailCount++;
		stTestData.ErrRegionList[nIdx].RegionList.left = stResult.stROI[nIdx].sLeft;
		stTestData.ErrRegionList[nIdx].RegionList.right = stResult.stROI[nIdx].sLeft + stResult.stROI[nIdx].sWidth;
		stTestData.ErrRegionList[nIdx].RegionList.top = stResult.stROI[nIdx].sTop;
		stTestData.ErrRegionList[nIdx].RegionList.bottom = stResult.stROI[nIdx].sTop + stResult.stROI[nIdx].sHeight;
	}

	// 양불 판정
	if (stTestOpt.stSpec_Min.iValue <= stTestData.nFailCount && stTestOpt.stSpec_Max.iValue >= stTestData.nFailCount)
	{
		stTestData.nResult = TR_Pass;
	}
	else
	{
		stTestData.nResult = TR_Fail;
	}

	return lReturn;
}



//=============================================================================
// Method		: Get_MeasurmentData
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in BOOL bMinUse
// Parameter	: __in BOOL bMaxUse
// Parameter	: __in double dbMinSpec
// Parameter	: __in double dbMaxSpec
// Parameter	: __in double dbValue
// Qualifier	:
// Last Update	: 2018/3/6 - 17:05
// Desc.		:
//=============================================================================
BOOL CTestManager_Test::Get_MeasurmentData(__in BOOL bMinUse, __in BOOL bMaxUse, __in double dbMinSpec, __in double dbMaxSpec, __in double dbValue)
{
	BOOL bMinResult = TRUE;
	BOOL bMaxResult = TRUE;

	if (TRUE == bMinUse)
	{
		bMinResult = (dbMinSpec <= dbValue) ? TRUE : FALSE;
	}

	if (TRUE == bMaxUse)
	{
		bMaxResult = (dbMaxSpec >= dbValue) ? TRUE : FALSE;
	}

	return bMinResult & bMaxResult;
}

//=============================================================================
// Method		: Get_MeasurmentData
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in BOOL bMinUse
// Parameter	: __in BOOL bMaxUse
// Parameter	: __in int iMinSpec
// Parameter	: __in int iMaxSpec
// Parameter	: __in int iValue
// Qualifier	:
// Last Update	: 2018/3/6 - 17:06
// Desc.		:
//=============================================================================
BOOL CTestManager_Test::Get_MeasurmentData(__in BOOL bMinUse, __in BOOL bMaxUse, __in int iMinSpec, __in int iMaxSpec, __in int iValue)
{
	BOOL bMinResult = TRUE;
	BOOL bMaxResult = TRUE;

	if (TRUE == bMinUse)
	{
		bMinResult = (iMinSpec <= iValue) ? TRUE : FALSE;
	}

	if (TRUE == bMaxUse)
	{
		bMaxResult = (iMaxSpec >= iValue) ? TRUE : FALSE;
	}

	return bMinResult & bMaxResult;

}
