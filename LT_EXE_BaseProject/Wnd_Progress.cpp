﻿// Wnd_Progress.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_Progress.h"
#include "resource.h"

static LPCTSTR	g_szState_Static[] =
{
	_T("Max SFR"),
	_T("Max SFR"),
	_T("SFR"),
	_T("SFR"),
	NULL
};

// CWnd_Progress
typedef enum LeakOptID
{
	IDC_BTN_ITEM		= 1001,
	IDC_CMB_ITEM		= 2001,
	IDC_EDT_ITEM		= 3001,
	IDC_LIST_ITEM		= 4001,

	IDC_ED_SPEC_MIN		= 5001,
	IDC_ED_SPEC_MAX		= 6001,
 	IDC_PROGRESSCTRL	= 7001,
};

IMPLEMENT_DYNAMIC(CWnd_Progress, CWnd_Cfg_VIBase)

CWnd_Progress::CWnd_Progress()
{
	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Progress::~CWnd_Progress()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Progress, CWnd_Cfg_VIBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()

// CWnd_Progress 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_Progress::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_Cfg_VIBase::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdx = 0; nIdx < STI_PG_MAX; nIdx++)
	{
		m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdx].SetFont_Gdip(L"Arial", 10.0f);
		m_st_Item[nIdx].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdx = 0; nIdx < PRG_PG_MAX; nIdx++)
	{
		m_pg_Item[nIdx].SetForegroundColorRef(RGB(200, 200, 0), RGB(100, 100, 0));
		m_pg_Item[nIdx].Create(NULL, _T(""), WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_PROGRESSCTRL + nIdx);
	}

	for (UINT nIdx = 0; nIdx < STI_PG_stateMAX; nIdx++)
	{
		m_st_stateItem[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_stateItem[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_stateItem[nIdx].SetFont_Gdip(L"Arial", 8.0f);
		m_st_stateItem[nIdx].Create(g_szState_Static[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdx = 0; nIdx < STI_PG_Result_MAX; nIdx++)
	{
		m_st_Result_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Result_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Result_Item[nIdx].SetFont_Gdip(L"Arial", 15.0f);
		m_st_Result_Item[nIdx].Create(_T("0"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_Progress::OnSize(UINT nType, int cx, int cy)
{
	CWnd_Cfg_VIBase::OnSize(nType, cx, cy);

	int iIdxCnt = 0;
	int iMargin = 0;
	int iSpacing = 5;
	int iSpacingstate = 3;
	int iLeft = iMargin;
	int iTop = iMargin;
	int iWidth = cx - iMargin - iMargin;
	int iHeight = cy - iMargin - iMargin;

	int iSTWidth = iWidth * 4 / 5;
	int iSTHeight = 25;

	int iSTstateWidth = iWidth / 10;

	int iPGHeight = (iHeight - iSTHeight - iSTHeight - iSpacing) / PRG_PG_MAX;

	m_st_Item[STI_PG_NAME_P1].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iTop += iSTHeight + iMargin + 2;
	m_pg_Item[PRG_PG_P1].MoveWindow(iLeft, iTop, iSTWidth, iPGHeight);

	iTop += iPGHeight + iSpacing;
	m_st_Item[STI_PG_NAME_P2].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iTop += iSTHeight + iMargin + 2;
	m_pg_Item[PRG_PG_P2].MoveWindow(iLeft, iTop, iSTWidth, iPGHeight);

	iLeft = iMargin;
	iLeft += iSTWidth + iSpacingstate;
	iTop = iMargin;
	m_st_stateItem[STI_PG_MAX_P1].MoveWindow(iLeft, iTop, iSTstateWidth, iSTHeight);

	iTop += iSTHeight + iMargin + 2;
	m_st_Result_Item[STI_PG_MAX_P1_Result].MoveWindow(iLeft, iTop, iSTstateWidth, iPGHeight);

	iLeft += iSTstateWidth;
	iTop = iMargin;
	m_st_stateItem[STI_PG_Current_P1].MoveWindow(iLeft, iTop, iSTstateWidth, iSTHeight);

	iTop += iSTHeight + iMargin + 2;
	m_st_Result_Item[STI_PG_Current_P1_Result].MoveWindow(iLeft, iTop, iSTstateWidth, iPGHeight);

	iLeft = iMargin;
	iLeft += iSTWidth + iSpacingstate;
	iTop += iPGHeight + iSpacing;
	m_st_stateItem[STI_PG_MAX_P2].MoveWindow(iLeft, iTop, iSTstateWidth, iSTHeight);

	iLeft += iSTstateWidth;
	m_st_stateItem[STI_PG_Current_P2].MoveWindow(iLeft, iTop, iSTstateWidth, iSTHeight);

	iLeft = iMargin;
	iLeft += iSTWidth + iSpacingstate;
	iTop += iSTHeight + iMargin + 2;
	m_st_Result_Item[STI_PG_MAX_P2_Result].MoveWindow(iLeft, iTop, iSTstateWidth, iPGHeight);

	iLeft += iSTstateWidth;
	m_st_Result_Item[STI_PG_Current_P2_Result].MoveWindow(iLeft, iTop, iSTstateWidth, iPGHeight);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_Progress::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_Cfg_VIBase::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_Progress::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd_Cfg_VIBase::OnShowWindow(bShow, nStatus);
}

//=============================================================================
// Method		: SetUpdateMax
// Access		: public  
// Returns		: void
// Parameter	: __in ST_Focus_Data stData
// Qualifier	:
// Last Update	: 2018/8/24 - 11:17
// Desc.		:
//=============================================================================
void CWnd_Progress::SetUpdateMax(__in ST_Focus_Data stData)
{
	double dbCurrentSFR[ROI_Focus_Max] = { 0.0, };

	BOOL bMaxSFR = TRUE;
	CString szValue;
	CString szResultValue;

	for (UINT nROI = 0; nROI < ROI_Focus_Max; nROI++)
	{
		// UI 표시
		float fValue = (float)(stData.dbCurrentValue[nROI] * 100);

		m_pg_Item[nROI].SetPosition(fValue);
		dbCurrentSFR[nROI] = stData.dbCurrentValue[nROI];

		szValue.Format(_T("%.2f"), stData.dbCurrentValue[nROI]);
		m_st_Result_Item[nROI + 2].SetText(szValue);

		szResultValue.Format(_T("%.2f"), stData.dbMaxValue[nROI]);
		m_st_Result_Item[nROI].SetText(szResultValue);

		// MAX SFR 양불 확인
		if (FALSE == stData.bMaxSFR[nROI])
		{
			bMaxSFR = FALSE;
			m_pg_Item[nROI].SetForegroundColorRef(RGB(255, 255, 179), RGB(255, 255, 233));
			m_st_Result_Item[nROI + 2].SetStaticStyle(CVGStatic::StaticStyle_Default);
			m_st_Result_Item[nROI + 2].SetColorStyle(CVGStatic::ColorStyle_Red);
		}
		else
		{
			m_pg_Item[nROI].SetForegroundColorRef(RGB(29, 219, 22), RGB(191, 255, 184));
			m_st_Result_Item[nROI + 2].SetStaticStyle(CVGStatic::StaticStyle_Default);
			m_st_Result_Item[nROI + 2].SetColorStyle(CVGStatic::ColorStyle_Default);
		}
	}
}

//=============================================================================
// Method		: SetUpdateBest
// Access		: public  
// Returns		: void
// Parameter	: __in ST_Focus_Data stData
// Qualifier	:
// Last Update	: 2018/8/24 - 11:17
// Desc.		:
//=============================================================================
void CWnd_Progress::SetUpdateBest(__in ST_Focus_Data stData)
{
	double dbCurrentSFR[ROI_Focus_Max] = { 0.0, };

	CString szValue;
	CString szResultValue;

	for (UINT nROI = 0; nROI < ROI_Focus_Max; nROI++)
	{
		// Best 값으로 Max
		UINT nMaxRang = (UINT)(stData.dbMaxValue[nROI] * 100);

		m_pg_Item[nROI].SetRange(0, nMaxRang);

		// UI 표시
		float fValue = (float)(stData.dbCurrentValue[nROI] * 100);

		m_pg_Item[nROI].SetPosition(fValue);

		dbCurrentSFR[nROI] = stData.dbCurrentValue[nROI];

		szValue.Format(_T("%.2f"), stData.dbCurrentValue[nROI]);
		m_st_Result_Item[nROI + 2].SetText(szValue);

		szResultValue.Format(_T("%.2f"), stData.dbMaxValue[nROI]);
		m_st_Result_Item[nROI].SetText(szResultValue);

		// 최종 MAX SFR 양불 표시
		if (FALSE == stData.bBestSFR[nROI])
		{
			m_pg_Item[nROI].SetForegroundColorRef(RGB(255, 255, 72), RGB(255, 255, 233));
			m_st_Result_Item[nROI + 2].SetStaticStyle(CVGStatic::StaticStyle_Default);
			m_st_Result_Item[nROI + 2].SetColorStyle(CVGStatic::ColorStyle_Red);
		}
		else
		{
			m_pg_Item[nROI].SetForegroundColorRef(RGB(29, 219, 22), RGB(191, 255, 184));
			m_st_Result_Item[nROI + 2].SetStaticStyle(CVGStatic::StaticStyle_Default);
			m_st_Result_Item[nROI + 2].SetColorStyle(CVGStatic::ColorStyle_Default);
		}
	}
}

//=============================================================================
// Method		: SetUpdateSpec
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/8/24 - 10:45
// Desc.		:
//=============================================================================
void CWnd_Progress::SetUpdateSpec()
{
	CString szValue;

	// 프로그래스 명칭 표시
	for (UINT nROI = 0; nROI < ROI_Focus_Max; nROI++)
	{
		szValue.Format(_T("ROI[%d]"), m_pstConfigInfo->nSelectROI[nROI]);
		m_st_Item[nROI].SetText(szValue);

		UINT nMaxRang = (UINT)(m_pstConfigInfo->dbMaxSFR[nROI] * 100);

		m_pg_Item[nROI].SetRange(0, nMaxRang);
		m_pg_Item[nROI].SetType(PRGB_LEFT);
	}
}

//=============================================================================
// Method		: SetResetData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/8/24 - 11:17
// Desc.		:
//=============================================================================
void CWnd_Progress::SetResetData()
{
	CString szValue;
	CString szValueResult;
	for (UINT nROI = 0; nROI < ROI_Focus_Max; nROI++)
	{
		m_pg_Item[nROI].SetPosition(0.0);

		szValue.Format(_T("0"));
		m_st_Result_Item[nROI].SetText(szValue);

		szValueResult.Format(_T("0"));
		m_st_Result_Item[nROI + 2].SetText(szValueResult);
	}
}