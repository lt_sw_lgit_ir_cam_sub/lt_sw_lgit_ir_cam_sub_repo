//*****************************************************************************
// Filename	: 	Wnd_Torque.cpp
// Created	:	2017/9/18 - 9:49
// Modified	:	2017/9/18 - 9:49
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
// Wnd_Torque.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_Torque.h"

// CWnd_Torque
static LPCTSTR g_szTorqueItem[] =
{
	_T("Torque (L)"),	// Pogo Count Left
	_T("Torque (R)"),	// Pogo Count Right
	NULL
};

static LPCTSTR g_szTorqueHeader[] =
{
	_T("Name"),
	_T("A"),
	_T("B"),
	_T("C"),
	_T("D"),
	NULL
};

static int g_iCell_Width[] =
{
	200,	// TRI_X_ItemName,
	150,	// TRI_X_Spec,		
	150,	// TRI_X_Usage,		
	150,	// TRI_X_Remain,	
	120,	// TRI_X_Reset,	
};

IMPLEMENT_DYNAMIC(CWnd_Torque, CWnd)

CWnd_Torque::CWnd_Torque()
{
	m_iTotalWidthRate = 0;
	for (UINT nIdx = 0; nIdx < TRI_X_MaxEnum; nIdx++)
	{
		m_iTotalWidthRate += g_iCell_Width[nIdx];
		m_iCtrlWidth[nIdx] = 0;
	}
}

CWnd_Torque::~CWnd_Torque()
{
}

BEGIN_MESSAGE_MAP(CWnd_Torque, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

// CWnd_Torque message handlers
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/9/18 - 9:50
// Desc.		:
//=============================================================================
int CWnd_Torque::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nColIdx = 0; nColIdx < TRI_X_MaxEnum; nColIdx++)
	{
		m_st_Caption[nColIdx].SetColorStyle(CVGStatic::ColorStyle_Black);
		m_st_Caption[nColIdx].SetFont_Gdip(L"Arial", 8.0F);
		m_st_Caption[nColIdx].Create(g_szTorqueHeader[nColIdx], WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN, rectDummy, this, IDC_STATIC);
	}

	for (UINT nRowIdx = 0; nRowIdx < TRI_Y_MaxEnum; nRowIdx++)
	{
		for (UINT nColIdx = 0; nColIdx < TRI_X_MaxEnum; nColIdx++)
		{
			m_st_Cell[nRowIdx][nColIdx].SetColorStyle(CVGStatic::ColorStyle_White);
			m_st_Cell[nRowIdx][nColIdx].SetFont_Gdip(L"Arial", 8.0F);
			m_st_Cell[nRowIdx][nColIdx].Create(_T(""), WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN, rectDummy, this, IDC_STATIC);
		}
	}

	for (UINT nRowIdx = 0; nRowIdx < TRI_Y_MaxEnum; nRowIdx++)
	{
		m_st_Cell[nRowIdx][TRI_X_ItemName].SetText(g_szTorqueItem[nRowIdx]);
	}

	return 1;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/9/18 - 9:50
// Desc.		:
//=============================================================================
void CWnd_Torque::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iSpacing	= 5;
	int iLeft		= 0;
	int iTop		= 0;
	int iWidth		= cx + (TRI_X_MaxEnum - 1);
	int iHeight		= cy;
	int iRemind		= 0;

	// 너비
	for (UINT nIdx = 0; nIdx < TRI_X_MaxEnum; nIdx++)
	{
		m_iCtrlWidth[nIdx] = (iWidth * g_iCell_Width[nIdx]) / m_iTotalWidthRate;
		iRemind += m_iCtrlWidth[nIdx];
	}
	iRemind = iWidth - iRemind;

	for (INT iIdx = iRemind; 0 < iIdx; iIdx--)
	{
		m_iCtrlWidth[iIdx - 1] += 1;
	}

	// 높이
	int iHeaderH = 40;
	int iCellHeight = iHeight + (m_nItemCount) -iHeaderH;
	int iCtrlHeight[TRI_Y_MaxEnum] = { 0, };
	iRemind = 0;

	for (UINT nIdx = 0; nIdx < (m_nItemCount); nIdx++)
	{
		iCtrlHeight[nIdx] = (iCellHeight)/ (m_nItemCount);
		iRemind += iCtrlHeight[nIdx];
	}
	iRemind = iCellHeight - iRemind;

	for (int iIdx = iRemind; 0 < iIdx; iIdx--)
	{
		iCtrlHeight[iIdx - 1] += 1;
	}
	

	// 컨트롤 이동
	for (UINT nColIdx = 0; nColIdx < TRI_X_MaxEnum; nColIdx++)
	{
		m_st_Caption[nColIdx].MoveWindow(iLeft, iTop, m_iCtrlWidth[nColIdx], iHeaderH);
		iLeft += (m_iCtrlWidth[nColIdx] - 1);
	}

	iLeft = 0;
	iTop += (iHeaderH - 1);

	for (UINT nRowIdx = 0; nRowIdx < m_nItemCount; nRowIdx++)
	{
		for (UINT nColIdx = 0; nColIdx < TRI_X_MaxEnum; nColIdx++)
		{
			m_st_Cell[nRowIdx][nColIdx].MoveWindow(iLeft, iTop, m_iCtrlWidth[nColIdx], iCtrlHeight[nRowIdx]);
			iLeft += (m_iCtrlWidth[nColIdx] - 1);
		}
		
		iLeft = 0;
		iTop += (iCtrlHeight[nRowIdx] - 1);
	}

	// 안쓰는 컨트롤 감추기
	for (UINT nRowIdx = m_nItemCount; nRowIdx < TRI_Y_MaxEnum; nRowIdx++)
	{
		for (UINT nColIdx = 0; nColIdx < TRI_X_MaxEnum; nColIdx++)
		{
			m_st_Cell[nRowIdx][nColIdx].MoveWindow(0, 0, 0, 0);
		}
	}

	if (1 == m_nItemCount)
	{
		m_st_Cell[0][TRI_X_ItemName].SetText(L"Torque");
	}
	else
	{
		m_st_Cell[0][TRI_X_ItemName].SetText(g_szTorqueItem[TRI_Y_Torque_Left]);
	}
}

//=============================================================================
// Method		: ResetAllItemData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/18 - 17:08
// Desc.		:
//=============================================================================
void CWnd_Torque::ResetAllItemData()
{
	for (UINT nRowIdx = 0; nRowIdx < TRI_Y_MaxEnum; nRowIdx++)
	{
		for (UINT nColIdx = 0; nColIdx < TRI_X_MaxEnum; nColIdx++)
		{
			m_st_Cell[nRowIdx][nColIdx].SetText(L"");
		}
	}
}

//=============================================================================
// Method		: SetItemData
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nItemIndex
// Parameter	: __in UINT nSpec
// Parameter	: __in UINT nUsage
// Qualifier	:
// Last Update	: 2017/9/18 - 17:04
// Desc.		:
//=============================================================================
void CWnd_Torque::SetItemData(__in UINT nItemIndex, __in UINT nSpec, __in UINT nUsage)
{
// 	UINT nRemain = nSpec - nUsage;
// 
// 	//m_st_Cell[TRI_Y_MaxEnum][TRI_X_MaxEnum];
// 
// 	if (nItemIndex < m_nItemCount)
// 	{
// 		CStringW szText;
// 		
// 		szText.Format(L"%d", nSpec);
// 		m_st_Cell[nItemIndex][TRI_X_Spec].SetText(szText.GetBuffer(0));
// 		
// 		szText.Format(L"%d", nUsage);
// 		m_st_Cell[nItemIndex][TRI_X_Usage].SetText(szText.GetBuffer(0));
// 		
// 		szText.Format(L"%d", nRemain);
// 		m_st_Cell[nItemIndex][TRI_X_Remain].SetText(szText.GetBuffer(0));
// 	}
}

//=============================================================================
// Method		: SetItemCount
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nCount
// Qualifier	:
// Last Update	: 2017/9/21 - 16:28
// Desc.		:
//=============================================================================
void CWnd_Torque::SetItemCount(__in UINT nCount)
{
	if (nCount <= TRI_Y_MaxEnum)
	{
		if (m_nItemCount != nCount)
		{
			m_nItemCount = nCount;			

			if (GetSafeHwnd())
			{
				CRect rc;
				GetClientRect(rc);
				OnSize(SIZE_RESTORED, rc.Width(), rc.Height());
			}
		}
	}
}
