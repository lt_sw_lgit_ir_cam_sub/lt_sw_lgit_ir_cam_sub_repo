//*****************************************************************************
// Filename	: 	Wnd_Cfg_TestItem_Foc.cpp
// Created	:	2017/9/24 - 16:11
// Modified	:	2017/9/24 - 16:11
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_Cfg_TestItem_Foc.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_Cfg_TestItem_Foc.h"

// CWnd_Cfg_TestItem_Foc

IMPLEMENT_DYNAMIC(CWnd_Cfg_TestItem_Foc, CWnd_BaseView)

CWnd_Cfg_TestItem_Foc::CWnd_Cfg_TestItem_Foc()
{
	m_InspectionType = enInsptrSysType::Sys_Focusing;
}

CWnd_Cfg_TestItem_Foc::~CWnd_Cfg_TestItem_Foc()
{
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_TestItem_Foc, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()

//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/9/28 - 17:51
// Desc.		:
//=============================================================================
int CWnd_Cfg_TestItem_Foc::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();
	
	UINT nID_Index = 0;

	m_tc_Option.Create(CMFCTabCtrl::STYLE_3D, rectDummy, this, 1, CMFCTabCtrl::LOCATION_BOTTOM);

	m_Wnd_ECurrent.SetOwner(GetOwner());
	m_Wnd_ECurrent.SetOverlayID(WM_SELECT_OVERLAY);
	m_Wnd_ECurrent.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_ECurrent.Create(NULL, _T("ECurrent"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_OpticalCenter.SetOwner(GetOwner());
	m_Wnd_OpticalCenter.SetOverlayID(WM_SELECT_OVERLAY);
	m_Wnd_OpticalCenter.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_OpticalCenter.Create(NULL, _T("OpticalCenter"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_Rotate.SetOwner(GetOwner());
	m_Wnd_Rotate.SetOverlayID(WM_SELECT_OVERLAY);
	m_Wnd_Rotate.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_Rotate.Create(NULL, _T("Rotate"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_DefectPixel.SetOwner(GetOwner());
	m_Wnd_DefectPixel.SetOverlayID(WM_SELECT_OVERLAY);
	m_Wnd_DefectPixel.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_DefectPixel.Create(NULL, _T("DefectPixel"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_SFR.SetOwner(GetOwner());
	m_Wnd_SFR.SetOverlayID(WM_SELECT_OVERLAY);
	m_Wnd_SFR.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_SFR.Create(NULL, _T("SFR"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_Particle.SetOwner(GetOwner());
	m_Wnd_Particle.SetOverlayID(WM_SELECT_OVERLAY);
	m_Wnd_Particle.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_Particle.Create(NULL, _T("Stain"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_ActiveAlign.SetOwner(GetOwner());
	m_Wnd_ActiveAlign.SetOverlayID(WM_SELECT_OVERLAY);
	m_Wnd_ActiveAlign.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_ActiveAlign.Create(NULL, _T("ActiveAlign"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_Torque.SetOwner(GetOwner());
	m_Wnd_Torque.SetOverlayID(WM_SELECT_OVERLAY);
	m_Wnd_Torque.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_Torque.Create(NULL, _T("Torque"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_Focus.SetOwner(GetOwner());
	m_Wnd_Focus.SetOverlayID(WM_SELECT_OVERLAY);
	m_Wnd_Focus.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_Focus.Create(NULL, _T("Focus"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_wnd_TestItem_EachTest.SetCameraParaIdx(m_pnCamParaIdx);

	m_wnd_TestItem_EachTest.Create(NULL, _T("개별"), dwStyle /*| WS_BORDER*/, rectDummy, this, nID_Index++);

	// 검사기 별로 탭 컨트롤 정의
	SetSysAddTabCreate();

	m_tc_Option.SetActiveTab(0);
	m_tc_Option.EnableTabSwap(FALSE);
	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/9/28 - 17:52
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_Foc::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMargin = 5;
	int iLeft	= iMargin;
	int iTop	= iMargin;
	int iWidth	= cx - iMargin - iMargin;
	int iHeight = cy - iMargin - iMargin;

	int nTabW = iWidth * 4 / 5;
	m_tc_Option.MoveWindow(iLeft, iTop, nTabW, iHeight);

	iLeft += iMargin + nTabW;
	
	m_wnd_TestItem_EachTest.MoveWindow(iLeft, iTop, iWidth - (iMargin + nTabW), iHeight);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: protected  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/10/21 - 11:21
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_Foc::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (TRUE == bShow)
	{
		m_tc_Option.SetActiveTab(0);
	}
}

//=============================================================================
// Method		: OnNMClickTestItem
// Access		: protected  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/9/28 - 18:36
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_Foc::OnNMClickTestItem(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	*pResult = 0;
}

//=============================================================================
// Method		: SetSysAddTabCreate
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/15 - 10:43
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_Foc::SetSysAddTabCreate()
{
	UINT nItemCnt = 0;

	m_tc_Option.AddTab(&m_Wnd_ECurrent,				_T("ECurrent"),			nItemCnt++,		TRUE);
	m_tc_Option.AddTab(&m_Wnd_OpticalCenter,		_T("OpticalCenter"),	nItemCnt++,		TRUE);
	m_tc_Option.AddTab(&m_Wnd_Rotate,				_T("Rotate"),			nItemCnt++,		TRUE);
	m_tc_Option.AddTab(&m_Wnd_SFR,					_T("SFR"),				nItemCnt++,		TRUE);
	m_tc_Option.AddTab(&m_Wnd_DefectPixel,			_T("DefectPixel"),		nItemCnt++,		TRUE);
	m_tc_Option.AddTab(&m_Wnd_Particle,				_T("Stain"),			nItemCnt++,		TRUE);
	m_tc_Option.AddTab(&m_Wnd_ActiveAlign,			_T("ActiveAlign"),		nItemCnt++,		TRUE);
	m_tc_Option.AddTab(&m_Wnd_Torque,				_T("Torque"),			nItemCnt++,		TRUE);
	m_tc_Option.AddTab(&m_Wnd_Focus,				_T("Focus"),			nItemCnt++,		TRUE);
}

//=============================================================================
// Method		: SetInitListCtrl
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/28 - 18:20
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_Foc::SetInitListCtrl()
{
}

//=============================================================================
// Method		: SetSystemType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2017/9/26 - 14:12
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_Foc::SetSystemType(__in enInsptrSysType nSysType)
{
	m_InspectionType = nSysType;
	m_wnd_TestItem_EachTest.SetSystemType(nSysType);
}

//=============================================================================
// Method		: Set_RecipeInfo
// Access		: public  
// Returns		: void
// Parameter	: __in ST_RecipeInfo * pstRecipeInfo
// Qualifier	:
// Last Update	: 2017/11/10 - 20:50
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_Foc::Set_RecipeInfo(__in ST_RecipeInfo* pstRecipeInfo)
{

	switch (pstRecipeInfo->ModelType)
	{
	case Model_MRA2:
		m_Wnd_ECurrent.OnSetCameraModelType(0);
		m_Wnd_ECurrent.OnSetModelType(1);
		break;

	case Model_IKC:
		m_Wnd_ECurrent.OnSetCameraModelType(0);
		m_Wnd_ECurrent.OnSetModelType(0);
		break;

	case Model_OMS_Entry:
	case Model_OMS_Front:
	case Model_OMS_Front_Set:
		m_Wnd_ECurrent.OnSetCameraModelType(1);
		m_Wnd_ECurrent.OnSetModelType(0);
		break;
	
	default:
		break;
	}


	if (Model_MRA2 == pstRecipeInfo->ModelType)
	{
		m_Wnd_ActiveAlign.OnSetModelType(1);
	}
	else
	{
		m_Wnd_ActiveAlign.OnSetModelType(0);
	}

	m_Wnd_ECurrent.SetPtr_RecipeInfo(&pstRecipeInfo->stFocus.stECurrentOpt);
	m_Wnd_ECurrent.SetUpdateData();

	m_Wnd_OpticalCenter.SetPtr_RecipeInfo(&pstRecipeInfo->stFocus.stOpticalCenterOpt);
	m_Wnd_OpticalCenter.SetUpdateData();

	m_Wnd_Rotate.SetPtr_RecipeInfo(&pstRecipeInfo->stFocus.stRotateOpt);
	m_Wnd_Rotate.SetUpdateData();

	m_Wnd_DefectPixel.SetPtr_RecipeInfo(&pstRecipeInfo->stFocus.stDefectPixelOpt);
	m_Wnd_DefectPixel.SetUpdateData();

	m_Wnd_SFR.SetPtr_RecipeInfo(&pstRecipeInfo->stFocus.stSFROpt);
	m_Wnd_SFR.SetUpdateData();

	m_Wnd_Particle.SetPtr_RecipeInfo(&pstRecipeInfo->stFocus.stParticleOpt);
	m_Wnd_Particle.SetUpdateData();

	m_Wnd_ActiveAlign.SetPtr_RecipeInfo(&pstRecipeInfo->stFocus.stActiveAlignOpt);
	m_Wnd_ActiveAlign.SetUpdateData();

	m_Wnd_Torque.SetPtr_RecipeInfo(&pstRecipeInfo->stFocus.stTorqueOpt);
	m_Wnd_Torque.SetUpdateData();

	m_Wnd_Focus.SetPtr_RecipeInfo(&pstRecipeInfo->stFocus.stFocusOpt);
	m_Wnd_Focus.SetUpdateData();

	m_wnd_TestItem_EachTest.Set_RecipeInfo(pstRecipeInfo);
}

//=============================================================================
// Method		: Get_RecipeInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_RecipeInfo & stOutRecipInfo
// Qualifier	:
// Last Update	: 2017/11/10 - 20:50
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_Foc::Get_RecipeInfo(__out ST_RecipeInfo& stOutRecipInfo)
{
	m_Wnd_ECurrent.GetUpdateData();
	m_Wnd_OpticalCenter.GetUpdateData();
	m_Wnd_Rotate.GetUpdateData();
	m_Wnd_DefectPixel.GetUpdateData();
	m_Wnd_SFR.GetUpdateData();
	m_Wnd_Particle.GetUpdateData();
	m_Wnd_ActiveAlign.GetUpdateData();
	m_Wnd_Torque.GetUpdateData();
	m_Wnd_Focus.GetUpdateData();
}

//=============================================================================
// Method		: Get_TestItemInfo
// Access		: public  
// Returns		: void
// Parameter	: __in ST_RecipeInfo & stRecipeInfo
// Parameter	: __out ST_TestItemInfo & stOutTestItemInfo
// Qualifier	:
// Last Update	: 2018/2/25 - 17:21
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_Foc::Get_TestItemInfo(__in ST_RecipeInfo& stRecipeInfo, __out ST_TestItemInfo& stOutTestItemInfo)
{
	ST_TestItemSpec* pSpec = NULL;
	CString szText;
	UINT nCtrlIdx = 0;

	for (UINT nIdx = enTestItem_Focusing::TI_Foc_Re_TorqueCheck; nIdx <= enTestItem_Focusing::TI_Foc_Re_DefectPixel; nIdx++)
	{
		pSpec = &stOutTestItemInfo.TestItemList.GetAt(nIdx);
		ASSERT(NULL != pSpec);

		// 결과 데이터가 1개 초과이고, 개별 Min Max 스펙을 가지는 경우
		if ((1 < pSpec->nResultCount) && (pSpec->bUseMultiSpec))
		{
			pSpec->bUseMinMaxSpec = FALSE;

			for (UINT nArIdx = 0; nArIdx < pSpec->nResultCount; nArIdx++)
			{
				Get_TestItemMinSpec(stRecipeInfo, (enTestItem_Focusing)nIdx, nArIdx, pSpec->Spec[nArIdx].bUseSpecMin, szText);

				if (pSpec->Spec[nArIdx].bUseSpecMin)
				{
					pSpec->bUseMinMaxSpec = TRUE;

					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Min.intVal = _ttoi(szText.GetBuffer(0));
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Min.dblVal = _ttof(szText.GetBuffer(0));
					}
				}
				else
				{
					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Min.intVal = 0;
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Min.dblVal = 0.0f;
					}
				}

				Get_TestItemMaxSpec(stRecipeInfo, (enTestItem_Focusing)nIdx, nArIdx, pSpec->Spec[nArIdx].bUseSpecMax, szText);

				if (pSpec->Spec[nArIdx].bUseSpecMax)
				{
					pSpec->bUseMinMaxSpec = TRUE;

					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Max.intVal = _ttoi(szText.GetBuffer(0));
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Max.dblVal = _ttof(szText.GetBuffer(0));
					}
				}
				else
				{
					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Max.intVal = 0;
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Max.dblVal = 0.0f;
					}
				}

				// 항목 인덱스 증가
				++nCtrlIdx;
			}
		}
		else // 결과 데이터가 동일한 MinMax 스펙을 가지는 경우
		{
			pSpec->bUseMinMaxSpec = FALSE;

			for (UINT nArIdx = 0; nArIdx < pSpec->nResultCount; nArIdx++)
			{
				Get_TestItemMinSpec(stRecipeInfo, (enTestItem_Focusing)nIdx, nArIdx, pSpec->Spec[nArIdx].bUseSpecMin, szText);

				if (pSpec->Spec[nArIdx].bUseSpecMin)
				{
					pSpec->bUseMinMaxSpec = TRUE;

					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Min.intVal = _ttoi(szText.GetBuffer(0));
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Min.dblVal = _ttof(szText.GetBuffer(0));
					}
				}
				else
				{
					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Min.intVal = 0;
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Min.dblVal = 0.0f;
					}
				}

				Get_TestItemMaxSpec(stRecipeInfo, (enTestItem_Focusing)nIdx, nArIdx, pSpec->Spec[nArIdx].bUseSpecMax, szText);

				if (pSpec->Spec[nArIdx].bUseSpecMax)
				{
					pSpec->bUseMinMaxSpec = TRUE;

					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Max.intVal = _ttoi(szText.GetBuffer(0));
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Max.dblVal = _ttof(szText.GetBuffer(0));
					}
				}
				else
				{
					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Max.intVal = 0;
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Max.dblVal = 0.0f;
					}
				}
			}

			// 항목 인덱스 증가
			++nCtrlIdx;
		}
	}
}

//=============================================================================
// Method		: Get_TestItemMinSpec
// Access		: public  
// Returns		: void
// Parameter	: __in ST_RecipeInfo & stRecipeInfo
// Parameter	: __in enTestItem_Focusing enTestItem
// Parameter	: __in UINT nArIdx
// Parameter	: __out BOOL & bUseSpec
// Parameter	: __out CString & szSpec
// Qualifier	:
// Last Update	: 2018/3/3 - 20:12
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_Foc::Get_TestItemMinSpec(__in ST_RecipeInfo& stRecipeInfo, __in enTestItem_Focusing enTestItem, __in UINT nArIdx, __out BOOL &bUseSpec, __out CString &szSpec)
{
	switch (enTestItem)
	{
	case TI_Foc_Re_ECurrent:
		bUseSpec = stRecipeInfo.stFocus.stECurrentOpt.stSpec_Min[nArIdx].bEnable;
		szSpec.Format(_T("%.1f"), stRecipeInfo.stFocus.stECurrentOpt.stSpec_Min[nArIdx].dbValue);
		break;

	case TI_Foc_Re_OpticalCenterX:
		bUseSpec = stRecipeInfo.stFocus.stOpticalCenterOpt.stSpec_Min[Spec_OC_X].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stFocus.stOpticalCenterOpt.stSpec_Min[Spec_OC_X].iValue);
		break;

	case TI_Foc_Re_OpticalCenterY:
		bUseSpec = stRecipeInfo.stFocus.stOpticalCenterOpt.stSpec_Min[Spec_OC_Y].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stFocus.stOpticalCenterOpt.stSpec_Min[Spec_OC_Y].iValue);
		break;

	case TI_Foc_Re_SFR:
		bUseSpec = stRecipeInfo.stFocus.stSFROpt.stInput[nArIdx].stSpecMin.bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stFocus.stSFROpt.stInput[nArIdx].stSpecMin.dbValue);
		break;
	//------------------------------------------ 20181210 SH2 수정
	case TI_Foc_Re_Group_SFR:
		bUseSpec = stRecipeInfo.stFocus.stSFROpt.stInput_Group[nArIdx].stSpecMin.bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stFocus.stSFROpt.stInput_Group[nArIdx].stSpecMin.dbValue);
		break;
	case TI_Foc_Re_Tilt_SFR:
// 		bUseSpec = stRecipeInfo.stFocus.stSFROpt.stInput_Tilt[nArIdx].bEnable;
// 		szSpec.Format(_T("%.2f"), stRecipeInfo.stFocus.stSFROpt.stInput_Tilt[nArIdx].dbValue);
		break;
	//-------------------------------------------
	//case TI_Foc_Re_G0_SFR:
	//	bUseSpec = stRecipeInfo.stFocus.stSFROpt.stSpec_F_Min[ITM_SFR_G0].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stFocus.stSFROpt.stSpec_F_Min[ITM_SFR_G0].dbValue);
	//	break;

	//case TI_Foc_Re_G1_SFR:
	//	bUseSpec = stRecipeInfo.stFocus.stSFROpt.stSpec_F_Min[ITM_SFR_G1].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stFocus.stSFROpt.stSpec_F_Min[ITM_SFR_G1].dbValue);
	//	break;

	//case TI_Foc_Re_G2_SFR:
	//	bUseSpec = stRecipeInfo.stFocus.stSFROpt.stSpec_F_Min[ITM_SFR_G2].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stFocus.stSFROpt.stSpec_F_Min[ITM_SFR_G2].dbValue);
	//	break;

	//case TI_Foc_Re_G3_SFR:
	//	bUseSpec = stRecipeInfo.stFocus.stSFROpt.stSpec_F_Min[ITM_SFR_G3].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stFocus.stSFROpt.stSpec_F_Min[ITM_SFR_G3].dbValue);
	//	break;

	//case TI_Foc_Re_G4_SFR:
	//	bUseSpec = stRecipeInfo.stFocus.stSFROpt.stSpec_F_Min[ITM_SFR_G4].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stFocus.stSFROpt.stSpec_F_Min[ITM_SFR_G4].dbValue);
	//	break;

	//case TI_Foc_Re_X1Tilt_SFR:
	//	bUseSpec = stRecipeInfo.stFocus.stSFROpt.stSpec_Tilt_Min[ITM_SFR_X1_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stFocus.stSFROpt.stSpec_Tilt_Min[ITM_SFR_X1_Tilt_UpperLimit].dbValue);
	//	break;

	//case TI_Foc_Re_X2Tilt_SFR:
	//	bUseSpec = stRecipeInfo.stFocus.stSFROpt.stSpec_Tilt_Min[ITM_SFR_X2_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stFocus.stSFROpt.stSpec_Tilt_Min[ITM_SFR_X2_Tilt_UpperLimit].dbValue);
	//	break;

	//case TI_Foc_Re_Y1Tilt_SFR:
	//	bUseSpec = stRecipeInfo.stFocus.stSFROpt.stSpec_Tilt_Min[ITM_SFR_Y1_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stFocus.stSFROpt.stSpec_Tilt_Min[ITM_SFR_Y1_Tilt_UpperLimit].dbValue);
	//	break;

	//case TI_Foc_Re_Y2Tilt_SFR:
	//	bUseSpec = stRecipeInfo.stFocus.stSFROpt.stSpec_Tilt_Min[ITM_SFR_Y2_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stFocus.stSFROpt.stSpec_Tilt_Min[ITM_SFR_Y2_Tilt_UpperLimit].dbValue);
	//	break;

	case TI_Foc_Re_Rotation:
		bUseSpec = stRecipeInfo.stFocus.stRotateOpt.stSpec_Min.bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stFocus.stRotateOpt.stSpec_Min.dbValue);
		break;

	case TI_Foc_Re_Stain:
		bUseSpec = stRecipeInfo.stFocus.stParticleOpt.stSpec_Min.bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stFocus.stParticleOpt.stSpec_Min.iValue);
		break;

	case TI_Foc_Re_DefectPixel:
		bUseSpec = stRecipeInfo.stFocus.stDefectPixelOpt.stSpec_Min[nArIdx].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stFocus.stDefectPixelOpt.stSpec_Min[nArIdx].iValue);
		break;

	case TI_Foc_Re_TorqueCheck:
		bUseSpec = stRecipeInfo.stFocus.stTorqueOpt.stSpec_Min[nArIdx].bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stFocus.stTorqueOpt.stSpec_Min[nArIdx].dbValue);
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: Get_TestItemMaxSpec
// Access		: public  
// Returns		: void
// Parameter	: __in ST_RecipeInfo & stRecipeInfo
// Parameter	: __in enTestItem_Focusing enTestItem
// Parameter	: __in UINT nArIdx
// Parameter	: __out BOOL & bUseSpec
// Parameter	: __out CString & szSpec
// Qualifier	:
// Last Update	: 2018/3/3 - 20:09
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_Foc::Get_TestItemMaxSpec(__in ST_RecipeInfo& stRecipeInfo, __in enTestItem_Focusing enTestItem, __in UINT nArIdx, __out BOOL &bUseSpec, __out CString &szSpec)
{
	switch (enTestItem)
	{
	case TI_Foc_Re_ECurrent:
		bUseSpec = stRecipeInfo.stFocus.stECurrentOpt.stSpec_Max[nArIdx].bEnable;
		szSpec.Format(_T("%.1f"), stRecipeInfo.stFocus.stECurrentOpt.stSpec_Max[nArIdx].dbValue);
		break;

	case TI_Foc_Re_OpticalCenterX:
		bUseSpec = stRecipeInfo.stFocus.stOpticalCenterOpt.stSpec_Max[Spec_OC_X].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stFocus.stOpticalCenterOpt.stSpec_Max[Spec_OC_X].iValue);
		break;

	case TI_Foc_Re_OpticalCenterY:
		bUseSpec = stRecipeInfo.stFocus.stOpticalCenterOpt.stSpec_Max[Spec_OC_Y].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stFocus.stOpticalCenterOpt.stSpec_Max[Spec_OC_Y].iValue);
		break;

	case TI_Foc_Re_SFR:
		bUseSpec = stRecipeInfo.stFocus.stSFROpt.stInput[nArIdx].stSpecMax.bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stFocus.stSFROpt.stInput[nArIdx].stSpecMax.dbValue);
		break;
//----------------------------------------------------20181211 SH2 수정
	case TI_Foc_Re_Group_SFR:
		bUseSpec = stRecipeInfo.stFocus.stSFROpt.stInput_Group[nArIdx].stSpecMax.bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stFocus.stSFROpt.stInput_Group[nArIdx].stSpecMax.dbValue);
		break;
	case TI_Foc_Re_Tilt_SFR:
		bUseSpec = stRecipeInfo.stFocus.stSFROpt.stInput_Tilt[nArIdx].bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stFocus.stSFROpt.stInput_Tilt[nArIdx].dbValue);
		break;
//------------------------------------------------------
	//case TI_Foc_Re_G0_SFR:
	//	bUseSpec = stRecipeInfo.stFocus.stSFROpt.stSpec_F_Max[ITM_SFR_G0].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stFocus.stSFROpt.stSpec_F_Max[ITM_SFR_G0].dbValue);
	//	break;

	//case TI_Foc_Re_G1_SFR:
	//	bUseSpec = stRecipeInfo.stFocus.stSFROpt.stSpec_F_Max[ITM_SFR_G1].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stFocus.stSFROpt.stSpec_F_Max[ITM_SFR_G1].dbValue);
	//	break;

	//case TI_Foc_Re_G2_SFR:
	//	bUseSpec = stRecipeInfo.stFocus.stSFROpt.stSpec_F_Max[ITM_SFR_G2].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stFocus.stSFROpt.stSpec_F_Max[ITM_SFR_G2].dbValue);
	//	break;

	//case TI_Foc_Re_G3_SFR:
	//	bUseSpec = stRecipeInfo.stFocus.stSFROpt.stSpec_F_Max[ITM_SFR_G3].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stFocus.stSFROpt.stSpec_F_Max[ITM_SFR_G3].dbValue);
	//	break;

	//case TI_Foc_Re_G4_SFR:
	//	bUseSpec = stRecipeInfo.stFocus.stSFROpt.stSpec_F_Max[ITM_SFR_G4].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stFocus.stSFROpt.stSpec_F_Max[ITM_SFR_G4].dbValue);
	//	break;

	//case TI_Foc_Re_X1Tilt_SFR:
	//	bUseSpec = stRecipeInfo.stFocus.stSFROpt.stSpec_Tilt_Max[ITM_SFR_X1_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stFocus.stSFROpt.stSpec_Tilt_Max[ITM_SFR_X1_Tilt_UpperLimit].dbValue);
	//	break;

	//case TI_Foc_Re_X2Tilt_SFR:
	//	bUseSpec = stRecipeInfo.stFocus.stSFROpt.stSpec_Tilt_Max[ITM_SFR_X2_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stFocus.stSFROpt.stSpec_Tilt_Max[ITM_SFR_X2_Tilt_UpperLimit].dbValue);
	//	break;

	//case TI_Foc_Re_Y1Tilt_SFR:
	//	bUseSpec = stRecipeInfo.stFocus.stSFROpt.stSpec_Tilt_Max[ITM_SFR_Y1_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stFocus.stSFROpt.stSpec_Tilt_Max[ITM_SFR_Y1_Tilt_UpperLimit].dbValue);
	//	break;

	//case TI_Foc_Re_Y2Tilt_SFR:
	//	bUseSpec = stRecipeInfo.stFocus.stSFROpt.stSpec_Tilt_Max[ITM_SFR_Y2_Tilt_UpperLimit].bEnable;
	//	szSpec.Format(_T("%.2f"), stRecipeInfo.stFocus.stSFROpt.stSpec_Tilt_Max[ITM_SFR_Y2_Tilt_UpperLimit].dbValue);
	//	break;

	case TI_Foc_Re_Rotation:
		bUseSpec = stRecipeInfo.stFocus.stRotateOpt.stSpec_Max.bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stFocus.stRotateOpt.stSpec_Max.dbValue);
		break;

	case TI_Foc_Re_Stain:
		bUseSpec = stRecipeInfo.stFocus.stParticleOpt.stSpec_Max.bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stFocus.stParticleOpt.stSpec_Max.iValue);
		break;

	case TI_Foc_Re_DefectPixel:
		bUseSpec = stRecipeInfo.stFocus.stDefectPixelOpt.stSpec_Max[nArIdx].bEnable;
		szSpec.Format(_T("%d"), stRecipeInfo.stFocus.stDefectPixelOpt.stSpec_Max[nArIdx].iValue);
		break;

	case TI_Foc_Re_TorqueCheck:
		bUseSpec = stRecipeInfo.stFocus.stTorqueOpt.stSpec_Max[nArIdx].bEnable;
		szSpec.Format(_T("%.2f"), stRecipeInfo.stFocus.stTorqueOpt.stSpec_Max[nArIdx].dbValue);
		break;

	default:
		break;
	}
}
