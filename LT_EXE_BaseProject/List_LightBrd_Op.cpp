﻿// List_LightBrd_Op.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_LightBrd_Op.h"

// CList_LightBrd_Op

typedef enum enListNumLightBrdOp
{
	LightOp_Object = 0,
	LightOp_Voltage,
	LightOp_Current,
	LightOp_On,
	LightOp_Off,
	LightOp_MaxCol,
};

static LPCTSTR	g_lpszHeaderLightBrdOp[] =
{
	_T(""),						 //LightOp_Object = 0,
	_T("Voltage"),				 //LightOp_Voltage,
	_T("Current"),				 //LightOp_Current,
	_T(""),				 //LightOp_On,
	_T(""),				 //LightOp_Off,
	NULL
};

static LPCTSTR	g_lpszItemLightBrdOp[] =
{
	NULL
};

const int	iListAglinLightBrdOp[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidthLightBrdOp[] =
{
	150,
	100,
	100,
	100,
	100,
};

#define IDC_EDT_CELLEDIT			1000
#define IDC_COM_CELLCOMBO_TYPE		2000
#define IDC_COM_CELLCOMBO_COLOR		3000
#define IDC_COM_CELLCOMBO_ITEM		4000
#define IDC_BTN_ON			5000
#define IDC_BTN_OFF			6000

IMPLEMENT_DYNAMIC(CList_LightBrd_Op, CList_Cfg_VIBase)

CList_LightBrd_Op::CList_LightBrd_Op()
{
	VERIFY(m_Font.CreateFont(
		25,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
	m_nEditCol = 0;
	m_nEditRow = 0;
}

CList_LightBrd_Op::~CList_LightBrd_Op()
{
	m_Font.DeleteObject();
}
BEGIN_MESSAGE_MAP(CList_LightBrd_Op, CList_Cfg_VIBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT	(NM_CLICK,					&CList_LightBrd_Op::OnNMClick					)
	ON_NOTIFY_REFLECT	(NM_DBLCLK,					&CList_LightBrd_Op::OnNMDblclk				)
	ON_EN_KILLFOCUS		(IDC_EDT_CELLEDIT,			&CList_LightBrd_Op::OnEnKillFocusEdit			)
	ON_COMMAND_RANGE(IDC_BTN_ON, IDC_BTN_ON + 999, OnRangeCheckBtnOn)
	ON_COMMAND_RANGE(IDC_BTN_OFF, IDC_BTN_OFF + 999, OnRangeCheckBtnOff)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_LightBrd_Op 메시지 처리기입니다.
int CList_LightBrd_Op::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CList_Cfg_VIBase::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	SetFont(&m_Font);
	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader(); 
	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER | ES_NUMBER, CRect(0, 0, 0, 0), this, IDC_EDT_CELLEDIT);

	for (UINT nIdx = 0; nIdx < LightOp_ItemNum; nIdx++)
	{
		GetSubItemRect(nIdx, LightOp_On, LVIR_BOUNDS, rectDummy);
		ClientToScreen(rectDummy);
		ScreenToClient(rectDummy);

		m_bt_On[nIdx].Create(_T("ON"), WS_VISIBLE | WS_BORDER | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_ON + nIdx);
		m_bt_On[nIdx].SetWindowPos(NULL, rectDummy.left, rectDummy.top, rectDummy.Width(), rectDummy.Height(), SWP_SHOWWINDOW);
	
		GetSubItemRect(nIdx, LightOp_Off, LVIR_BOUNDS, rectDummy);
		ClientToScreen(rectDummy);
		ScreenToClient(rectDummy);

		m_bt_Off[nIdx].Create(_T("OFF"), WS_VISIBLE | WS_BORDER | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_OFF + nIdx);
		m_bt_Off[nIdx].SetWindowPos(NULL, rectDummy.left, rectDummy.top, rectDummy.Width(), rectDummy.Height(), SWP_SHOWWINDOW);
	}

	this->GetHeaderCtrl()->EnableWindow(FALSE);
	


	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_LightBrd_Op::OnSize(UINT nType, int cx, int cy)
{
	CList_Cfg_VIBase::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_LightBrd_Op::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CList_Cfg_VIBase::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_LightBrd_Op::InitHeader()
{
	for (int nCol = 0; nCol < LightOp_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeaderLightBrdOp[nCol], iListAglinLightBrdOp[nCol], iHeaderWidthLightBrdOp[nCol]);
	}

	for (int nCol = 0; nCol < LightOp_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidthLightBrdOp[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 17:25
// Desc.		:
//=============================================================================
void CList_LightBrd_Op::InsertFullData()
{
	if (NULL == m_pstLightInfo)
		return;

 	DeleteAllItems();
 
	for (UINT nIdx = 0; nIdx < LightOp_ItemNum; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
 	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/6/26 - 14:26
// Desc.		:
//=============================================================================
void CList_LightBrd_Op::SetRectRow(UINT nRow)
{
	if (NULL == m_pstLightInfo)
		return;

	CString strValue;

	strValue.Format(_T("%s"), g_szLightItem[nRow]);
	SetItemText(nRow, LightOp_Object, strValue);

	strValue.Format(_T("%.2f"), m_pstLightInfo->stLightBrd[nRow].fVolt);
	SetItemText(nRow, LightOp_Voltage, strValue);

	strValue.Format(_T("%d"), (int)m_pstLightInfo->stLightBrd[nRow].wStep);
	SetItemText(nRow, LightOp_Current, strValue);
}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_LightBrd_Op::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	LVHITTESTINFO HitInfo;
	HitInfo.pt = pNMItemActivate->ptAction;
	
	HitTest(&HitInfo);

	// Check Ctrl Event
// 	if (HitInfo.flags == LVHT_ONITEMSTATEICON)
// 	{
// 		UINT nBuffer;
// 
// 		nBuffer = GetItemState(pNMItemActivate->iItem, LVIS_STATEIMAGEMASK);
// 
// 		if (nBuffer == 0x2000)
// 			m_pstLightInfo->stRegion[pNMItemActivate->iItem].bEnable = FALSE;
// 
// 		if (nBuffer == 0x1000)
// 			m_pstLightInfo->stRegion[pNMItemActivate->iItem].bEnable = TRUE;
// 	}

	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_LightBrd_Op::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem == LightOp_Object)
		{
			LVHITTESTINFO HitInfo;
			HitInfo.pt = pNMItemActivate->ptAction;

			HitTest(&HitInfo);

// 			// Check Ctrl Event
// 			if (HitInfo.flags == LVHT_ONITEMSTATEICON)
// 			{
// 				UINT nBuffer;
// 
// 				nBuffer = GetItemState(pNMItemActivate->iItem, LVIS_STATEIMAGEMASK);
// 
// 				if (nBuffer == 0x2000)
// 					m_pstLightInfo->stRegion[pNMItemActivate->iItem].bEnable = FALSE;
// 
// 				if (nBuffer == 0x1000)
// 					m_pstLightInfo->stRegion[pNMItemActivate->iItem].bEnable = TRUE;
// 			}
		}

		if (pNMItemActivate->iSubItem < LightOp_MaxCol && pNMItemActivate->iSubItem > 0)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);
			
			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);


			m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
			m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
			m_ed_CellEdit.SetFocus();

		}
//		GetOwner()->SendNotifyMessage(m_nMessage, 0, 0);
	}

	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
void CList_LightBrd_Op::OnEnKillFocusEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	if (m_nEditCol == LightOp_Voltage || m_nEditCol == LightOp_Current)
	{
		UpdateCelldbData(m_nEditRow, m_nEditCol, _ttof(strText));
	}

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
BOOL CList_LightBrd_Op::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	if ( NULL == m_pstLightInfo)
		return FALSE;
// 
// 	if (iValue < 0)
// 		iValue = 0;
// 
// 	CRect rtTemp = m_pstLightInfo->stRegion[nRow].rtROI;
// 
// 	switch (nCol)
// 	{
// 	case LightOp_PosX:
// 		m_pstLightInfo->stRegion[nRow].RectPosXY(iValue, rtTemp.CenterPoint().y);
// 		break;
// 	case LightOp_PosY:
// 		m_pstLightInfo->stRegion[nRow].RectPosXY(rtTemp.CenterPoint().x, iValue);
// 		break;
// 	case LightOp_Width:
// 		m_pstLightInfo->stRegion[nRow].RectPosWH(iValue, rtTemp.Height());
// 		break;
// 	case LightOp_Height:
// 		m_pstLightInfo->stRegion[nRow].RectPosWH(rtTemp.Width(), iValue);
// 		break;
// 	default:
// 		break;
// 	}
// 
// 	if (m_pstLightInfo->stRegion[nRow].rtROI.left < 0)
// 	{
// 		CRect rtTemp = m_pstLightInfo->stRegion[nRow].rtROI;
// 
// 		m_pstLightInfo->stRegion[nRow].rtROI.left = 0;
// 		m_pstLightInfo->stRegion[nRow].rtROI.right = m_pstLightInfo->stRegion[nRow].rtROI.left + rtTemp.Width();
// 	}
// 
// 	if (m_pstLightInfo->stRegion[nRow].rtROI.right >(LONG)m_dwImage_Width)
// 	{
// 		CRect rtTemp = m_pstLightInfo->stRegion[nRow].rtROI;
// 
// 		m_pstLightInfo->stRegion[nRow].rtROI.right = (LONG)m_dwImage_Width;
// 		m_pstLightInfo->stRegion[nRow].rtROI.left = m_pstLightInfo->stRegion[nRow].rtROI.right - rtTemp.Width();
// 	}
// 
// 	if (m_pstLightInfo->stRegion[nRow].rtROI.top < 0)
// 	{
// 		CRect rtTemp = m_pstLightInfo->stRegion[nRow].rtROI;
// 
// 		m_pstLightInfo->stRegion[nRow].rtROI.top = 0;
// 		m_pstLightInfo->stRegion[nRow].rtROI.bottom = rtTemp.Height() + m_pstLightInfo->stRegion[nRow].rtROI.top;
// 	}
// 
// 	if (m_pstLightInfo->stRegion[nRow].rtROI.bottom >(LONG)m_dwImage_Height)
// 	{
// 		CRect rtTemp = m_pstLightInfo->stRegion[nRow].rtROI;
// 
// 		m_pstLightInfo->stRegion[nRow].rtROI.bottom = (LONG)m_dwImage_Height;
// 		m_pstLightInfo->stRegion[nRow].rtROI.top = m_pstLightInfo->stRegion[nRow].rtROI.bottom - rtTemp.Height();
// 	}
// 
// 	if (m_pstLightInfo->stRegion[nRow].rtROI.Height() <= 0)
// 		m_pstLightInfo->stRegion[nRow].RectPosWH(m_pstLightInfo->stRegion[nRow].rtROI.Width(), 1);
// 
// 	if (m_pstLightInfo->stRegion[nRow].rtROI.Height() >= (LONG)m_dwImage_Height)
// 		m_pstLightInfo->stRegion[nRow].RectPosWH(m_pstLightInfo->stRegion[nRow].rtROI.Width(), m_dwImage_Height);
// 
// 	if (m_pstLightInfo->stRegion[nRow].rtROI.Width() <= 0)
// 		m_pstLightInfo->stRegion[nRow].RectPosWH(1, m_pstLightInfo->stRegion[nRow].rtROI.Height());
// 
// 	if (m_pstLightInfo->stRegion[nRow].rtROI.Width() >= (LONG)m_dwImage_Width)
// 		m_pstLightInfo->stRegion[nRow].RectPosWH(m_dwImage_Width, m_pstLightInfo->stRegion[nRow].rtROI.Height());
// 
// 	CString strValue;
// 
// 	switch (nCol)
// 	{
// 	case LightOp_PosX:
// 		strValue.Format(_T("%d"), m_pstLightInfo->stRegion[nRow].rtROI.CenterPoint().x);
// 		break;
// 	case LightOp_PosY:
// 		strValue.Format(_T("%d"), m_pstLightInfo->stRegion[nRow].rtROI.CenterPoint().y);
// 		break;
// 	case LightOp_Width:
// 		strValue.Format(_T("%d"), m_pstLightInfo->stRegion[nRow].rtROI.Width());
// 		break;
// 	case LightOp_Height:
// 		strValue.Format(_T("%d"), m_pstLightInfo->stRegion[nRow].rtROI.Height());
// 		break;
// 	default:
// 		break;
// 	}
// 
// 	m_ed_CellEdit.SetWindowText(strValue);
// 	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: UpdateCelldbData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dbValue
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_LightBrd_Op::UpdateCelldbData(UINT nRow, UINT nCol, double dbValue)
{
	CString str;
	switch (nCol)
	{
	case LightOp_Voltage:

		if (dbValue > 12)
		{
			dbValue = 12.0;
		}
		m_pstLightInfo->stLightBrd[nRow].fVolt = (float)dbValue;
		str.Format(_T("%.2f"), dbValue);
		break;
	case LightOp_Current:
		m_pstLightInfo->stLightBrd[nRow].wStep = (WORD)dbValue;
		str.Format(_T("%d"), (int)dbValue);
		break;
	default:
		break;
	}


	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_LightBrd_Op::GetCellData()
{
	if ( NULL == m_pstLightInfo)
		return;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_LightBrd_Op::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue		= _ttoi(strText);
		double dbValue	= _ttof(strText);

		if (0 < zDelta)
		{
			iValue	= iValue + ((zDelta / 120));
			dbValue = dbValue + ((zDelta / 120) * 0.01);
		}
		else
		{
			if (0 < iValue)
				iValue = iValue + ((zDelta / 120));

			if (0 < dbValue)
				dbValue = dbValue + ((zDelta / 120) * 0.01);
		}

		if (iValue < 0)
			iValue = 0;

		if (iValue > 2000)
			iValue = 2000;

		if (dbValue < 0.0)
			dbValue = 0.0;

		if (m_nEditCol == LightOp_Voltage || m_nEditCol == LightOp_Current)
		{
			UpdateCelldbData(m_nEditRow, m_nEditCol, dbValue);
		}
	}

	return CList_Cfg_VIBase::OnMouseWheel(nFlags, zDelta, pt);
}

//=============================================================================
// Method		: OnRangeCheckBtnMinCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2018/2/20 - 9:32
// Desc.		:
//=============================================================================
afx_msg void CList_LightBrd_Op::OnRangeCheckBtnOn(UINT nID)
{
	UINT nIndex = nID - IDC_BTN_ON;



	switch (m_nLightBoardType)
	{
	case LBT_ODA_PT:
		((CODA_PT*)m_pLightDevice)->Send_Output(enSwitchOnOff::Switch_ON);
		((CODA_PT*)m_pLightDevice)->Send_Voltage(m_pstLightInfo->stLightBrd[nIndex].fVolt);
		break;

	case LBT_Luritech_01:
		((CLGE_LightBrd*)m_pLightDevice)->Send_AmbientVoltOn_All(m_pstLightInfo->stLightBrd[nIndex].fVolt);
		((CLGE_LightBrd*)m_pLightDevice)->Send_EtcSetCurrent(Slot_All, m_pstLightInfo->stLightBrd[nIndex].wStep);

		break;

	default:
		break;
	}


}

//=============================================================================
// Method		: OnRangeCheckBtnMaxCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2018/2/20 - 9:32
// Desc.		:
//=============================================================================
void CList_LightBrd_Op::OnRangeCheckBtnOff(UINT nID)
{
	UINT nIndex = nID - IDC_BTN_OFF;

	switch (m_nLightBoardType)
	{
	case LBT_ODA_PT:
		((CODA_PT*)m_pLightDevice)->Send_Output(enSwitchOnOff::Switch_OFF);
		break;

	case LBT_Luritech_01:
		((CLGE_LightBrd*)m_pLightDevice)->Send_AmbientVoltOff_All();
		break;

	default:
		break;
	}



}


void CList_LightBrd_Op::SetPtr_Device(__in UINT nCtrlType, __in PVOID pLightDevice)
{
	m_nLightBoardType = (enLightBrdType)nCtrlType;
	m_pLightDevice = pLightDevice;
}