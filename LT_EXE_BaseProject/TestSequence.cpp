#include "stdafx.h"
#include "TestSequence.h"
#include "CommonFunction.h"

typedef enum enumMotorSequence
{
	MotorOriginWaitTime		= 50000,
	DigitalIoWaitTime		= 5000,
};

CTestSequence::CTestSequence()
{
	m_pDevice		= NULL;
	m_pOption		= NULL;
	m_pstTeachInfo	= NULL;
}

CTestSequence::~CTestSequence()
{
}

//=============================================================================
// Method		: MotorAllEStop
// Access		: public  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/22 - 9:38
// Desc.		:
//=============================================================================
LRESULT CTestSequence::MotorAllEStop()
{
	LRESULT	lReturn = RC_OK;

	// Handle 확인
	if (NULL == m_pDevice)
		return RC_Invalid_Handle;

	// 보드 OPEN 상태 확인
	if (FALSE == m_pDevice->GetBoardOpen())
		return RC_Motion_Err_BoardOpen;

	// 모터 알람 여부 확인
	for (UINT nIdx = 0; nIdx < (UINT)m_pDevice->m_AllMotorData.MotorInfo.lMaxAxisCnt; nIdx++)
	{
		if (TRUE == m_pDevice->m_AllMotorData.pMotionParam->bAxisUse)
		{
			if (FALSE == m_pDevice->SetMotorEStop(nIdx))
			{
				m_Log.LogMsg_Err(_T("%s, AXIS[%d]"), g_szResultCode[RC_Motor_Err_EStop], nIdx);
				return RC_Motor_Err_EStop;
			}
		}
	}

	return RC_OK;
}

//=============================================================================
// Method		: MotorGetStatus
// Access		: public  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/10/14 - 19:29
// Desc.		:
//=============================================================================
LRESULT CTestSequence::MotorGetStatus()
{
	// Handle 확인
	if (NULL == m_pDevice)
		return RC_Invalid_Handle;

	// 보드 OPEN 상태 확인
	if (FALSE == m_pDevice->GetBoardOpen())
		return RC_Motion_Err_BoardOpen;

	// 모터 알람 여부 확인
	for (UINT nIdx = 0; nIdx < (UINT)m_pDevice->m_AllMotorData.MotorInfo.lMaxAxisCnt; nIdx++)
	{
		if (TRUE == m_pDevice->m_AllMotorData.pMotionParam[nIdx].bAxisUse)
		{
			if (TRUE == m_pDevice->GetAlarmSenorStatus(nIdx))
			{
				m_Log.LogMsg_Err(_T("%s, AXIS[%d]"), g_szResultCode[RC_Motor_Err_Alarm], nIdx);
				return RC_Motion_Err_Alarm;
			}
		}
	}

	return RC_OK;
}

//=============================================================================
// Method		: MotorManualOrigin
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in int iAxis
// Qualifier	:
// Last Update	: 2017/10/14 - 20:24
// Desc.		:
//=============================================================================
LRESULT CTestSequence::MotorManualOrigin(__in int iAxis)
{
	// Handle 확인
	if (NULL == m_pDevice)
		return RC_Invalid_Handle;

	// 보드 OPEN 상태 확인
	if (FALSE == m_pDevice->GetBoardOpen())
		return RC_Motion_Err_BoardOpen;

	// 모터 Amp On
	if (FALSE == m_pDevice->SetAmpCtr(iAxis, ON))
		return RC_Motion_Err_Amp;

	if (FALSE == m_pDevice->SetAlarmClear(iAxis))
		return RC_Motion_Err_Alarm_Clear;

	CMotionManager MotionManager = *m_pDevice;

	// 동작 중인 모터 강제중지
	MotionManager.SetMotorEStop(iAxis);
	Sleep(200);

	// 원점 수행
	MotionManager.SetMotorOrigin(iAxis);

	clock_t start_tm = clock();

	do
	{
		// 타임 아웃
		if ((DWORD)clock() - start_tm > MotorOriginWaitTime)
		{
			MotionManager.SetOriginStop(iAxis);
			return RC_Motion_Err_OriginTimeOut;
		}

		// 강제 정지
		if (MotionManager.GetOriginStopStatus(iAxis))
			return RC_Motion_Err_OriginUserStop;

		DoEvents(1);

	} while (!MotionManager.GetOriginStatus(iAxis));

	return RC_OK;
}

//=============================================================================
// Method		: MotorAllOriginStart
// Access		: public  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/10/14 - 19:17
// Desc.		:
//=============================================================================
LRESULT CTestSequence::MotorAllOriginStart()
{
	LRESULT	lReturn = RC_OK;

	// Handle 확인
	if (NULL == m_pDevice)
		return RC_Invalid_Handle;

	// 보드 OPEN 상태 확인
	if (FALSE == m_pDevice->GetBoardOpen())
		return RC_Motion_Err_BoardOpen;
	
	if (FALSE == m_pDevice->SetAllAmpCtr(ON))
		return RC_Motion_Err_Amp;

	if (FALSE == m_pDevice->SetAllAlarmClear())
		return RC_Motion_Err_Alarm_Clear;

	CMotionManager* MotionManager = m_pDevice;

	for (UINT nAxis = 0; nAxis < (UINT)m_pDevice->m_AllMotorData.MotorInfo.lMaxAxisCnt; nAxis++)
	{
		if (TRUE == MotionManager->GetAxisUseStatus(nAxis))
		{
			if (FALSE == MotionManager->SetOriginReset(nAxis))
			{
				m_Log.LogMsg_Err(_T("%s, Origin AXIS[%d]"), g_szResultCode[RC_Motor_Err_Reset], nAxis);
				return RC_Motor_Err_Reset;
			}

			if (FALSE == MotionManager->SetMotorEStop(nAxis))
			{
				m_Log.LogMsg_Err(_T("%s, Origin AXIS[%d]"), g_szResultCode[RC_Motor_Err_EStop], nAxis);
				return RC_Motor_Err_EStop;
			}
		}
	}

	Sleep(200);

	lReturn = MotorSysOriginSeq(m_SysType);
	
	return lReturn;
}

//=============================================================================
// Method		: MotorSysOriginSeq
// Access		: public  
// Returns		: LRESULT
// Parameter	: enInsptrSysType SysType
// Qualifier	:
// Last Update	: 2017/10/14 - 19:36
// Desc.		:
//=============================================================================
LRESULT CTestSequence::MotorSysOriginSeq(enInsptrSysType SysType)
{
	LRESULT	lReturn = RC_OK;

	if (NULL == m_pDevice)
		return RC_Invalid_Handle;

	CMotionManager* MotionManager = m_pDevice;

	switch (m_SysType)
	{
	case Sys_2D_Cal:
		lReturn = MotorOrigin_2DCal();
		break;
	case Sys_Image_Test:
		lReturn = MotorOrigin_ImgT();
		break;
	case Sys_3D_Cal:
		break;
	case Sys_Stereo_Cal:
		lReturn = MotorOrigin_Stereo();
		break;
	case Sys_Focusing:
		lReturn = MotorOrigin_Foc();
		break;
	default:
		break;
	}

	return lReturn;
}

//=============================================================================
// Method		: MotorOrigin_2DCal
// Access		: public  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/10/14 - 19:52
// Desc.		:
//=============================================================================
LRESULT CTestSequence::MotorOrigin_2DCal()
{
	LRESULT	lReturn = RC_OK;

	CMotionManager* MotionManager = m_pDevice;

	BOOL bOrigin = TRUE;

	bOrigin = MotionManager->SetMotorOrigin(AX_2D_Shutter);

	if (FALSE == bOrigin)
	{
		m_Log.LogMsg_Err(_T("%s, Origin AXIS[%d]"), g_szResultCode[RC_Motor_Err_Origin], AX_2D_Shutter);
		return RC_Motion_Err_Origin_Start;
	}

	clock_t start_tm = clock();

	do
	{
		// 타임 아웃
		if ((DWORD)clock() - start_tm > MotorOriginWaitTime)
		{
			MotionManager->SetOriginStop(AX_2D_Shutter);
			m_Log.LogMsg_Err(_T("%s, Origin AXIS[%d]"), g_szResultCode[RC_Motor_Err_Time], AX_2D_Shutter);
			return RC_Motor_Err_Time;
		}

		// 강제 정지
		if (MotionManager->GetOriginStopStatus(AX_2D_Shutter))
		{
			return RC_Motion_Err_OriginUserStop;
		}

		DoEvents(1);

	} while (!MotionManager->GetOriginStatus(AX_2D_Shutter));

	// 1. AXIS X, Y, Z
	bOrigin  = MotionManager->SetMotorOrigin(AX_2D_StageX);
	bOrigin &= MotionManager->SetMotorOrigin(AX_2D_StageY);
	bOrigin &= MotionManager->SetMotorOrigin(AX_2D_StageZ);

	if (FALSE == bOrigin)
		return RC_Motion_Err_Origin_Start;

	start_tm = clock();

	do
	{
		// 타임 아웃
		if ((DWORD)clock() - start_tm > MotorOriginWaitTime)
		{
			MotionManager->SetOriginStop(AX_2D_StageX);
			MotionManager->SetOriginStop(AX_2D_StageY);
			MotionManager->SetOriginStop(AX_2D_StageZ);
			return RC_Motion_Err_OriginTimeOut;
		}

		// 강제 정지
		if (MotionManager->GetOriginStopStatus(AX_2D_StageX) || MotionManager->GetOriginStopStatus(AX_2D_StageY) || MotionManager->GetOriginStopStatus(AX_2D_StageZ))
		{
			return RC_Motion_Err_OriginUserStop;
		}

		DoEvents(1);

	} while (!MotionManager->GetOriginStatus(AX_2D_StageX) || !MotionManager->GetOriginStatus(AX_2D_StageY) || !MotionManager->GetOriginStatus(AX_2D_StageZ));

	// 2. AXIS R, CHART R, SHUTTER
	bOrigin  = MotionManager->SetMotorOrigin(AX_2D_StageR);
	bOrigin &= MotionManager->SetMotorOrigin(AX_2D_ChartR);
	
	if (FALSE == bOrigin)
		return RC_Motion_Err_Origin_Start;

	start_tm = clock();

	do
	{
		// 타임 아웃
		if ((DWORD)clock() - start_tm > MotorOriginWaitTime)
		{
			MotionManager->SetOriginStop(AX_2D_StageR);
			MotionManager->SetOriginStop(AX_2D_ChartR);
			return RC_Motion_Err_OriginTimeOut;
		}

		// 강제 정지
		if (MotionManager->GetOriginStopStatus(AX_2D_StageR) || MotionManager->GetOriginStopStatus(AX_2D_ChartR))
		{
			return RC_Motion_Err_OriginUserStop;
		}

		DoEvents(1);

	} while (!MotionManager->GetOriginStatus(AX_2D_StageR) || !MotionManager->GetOriginStatus(AX_2D_ChartR));

	return RC_OK;
}

//=============================================================================
// Method		: MotorOrigin_Stereo
// Access		: public  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/10/14 - 19:52
// Desc.		:
//=============================================================================
LRESULT CTestSequence::MotorOrigin_Stereo()
{
	LRESULT	lReturn = RC_OK;

	CMotionManager* MotionManager = m_pDevice;

	BOOL bOrigin = TRUE;

	bOrigin = MotionManager->SetMotorOrigin(AX_Stereo_Shutter);

	if (FALSE == bOrigin)
		return RC_Motion_Err_Origin_Start;

	clock_t start_tm = clock();

	do
	{
		// 타임 아웃
		if ((DWORD)clock() - start_tm > MotorOriginWaitTime)
		{
			MotionManager->SetOriginStop(AX_Stereo_Shutter);
			return RC_Motion_Err_OriginTimeOut;
		}

		// 강제 정지
		if (MotionManager->GetOriginStopStatus(AX_Stereo_Shutter))
		{
			return RC_Motion_Err_OriginUserStop;
		}

		DoEvents(1);

	} while (!MotionManager->GetOriginStatus(AX_Stereo_Shutter));

	bOrigin  = MotionManager->SetMotorOrigin(AX_Stereo_StageY);
	bOrigin &= MotionManager->SetMotorOrigin(AX_Stereo_ChartX);
	bOrigin &= MotionManager->SetMotorOrigin(AX_Stereo_ChartTX);
	bOrigin &= MotionManager->SetMotorOrigin(AX_Stereo_ChartTZ);
	bOrigin &= MotionManager->SetMotorOrigin(AX_Stereo_ChartZ);

	if (FALSE == bOrigin)
		return RC_Motion_Err_Origin_Start;

	start_tm = clock();

	do
	{
		// 타임 아웃
		if ((DWORD)clock() - start_tm > MotorOriginWaitTime)
		{
			MotionManager->SetOriginStop(AX_Stereo_StageY);
			MotionManager->SetOriginStop(AX_Stereo_ChartX);
			MotionManager->SetOriginStop(AX_Stereo_ChartTX);
			MotionManager->SetOriginStop(AX_Stereo_ChartTZ);
			MotionManager->SetOriginStop(AX_Stereo_ChartZ);
			return RC_Motion_Err_OriginTimeOut;
		}

		// 강제 정지
		if (MotionManager->GetOriginStopStatus(AX_Stereo_StageY) || MotionManager->GetOriginStopStatus(AX_Stereo_ChartX) || MotionManager->GetOriginStopStatus(AX_Stereo_ChartTX)
			|| MotionManager->GetOriginStopStatus(AX_Stereo_ChartTZ) || MotionManager->GetOriginStopStatus(AX_Stereo_ChartZ))
		{
			return RC_Motion_Err_OriginUserStop;
		}

		DoEvents(1);

	} while (!MotionManager->GetOriginStatus(AX_Stereo_StageY) || !MotionManager->GetOriginStatus(AX_Stereo_ChartX)	|| !MotionManager->GetOriginStatus(AX_Stereo_ChartTX) 
		|| !MotionManager->GetOriginStatus(AX_Stereo_ChartTZ) || !MotionManager->GetOriginStatus(AX_Stereo_ChartZ));

	lReturn = OnActionLoad_Stereo();

	return lReturn;
}

//=============================================================================
// Method		: MotorOrigin_ImgT
// Access		: public  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/22 - 9:03
// Desc.		:
//=============================================================================
LRESULT CTestSequence::MotorOrigin_ImgT()
{
	LRESULT	lReturn = RC_OK;

	CMotionManager* MotionManager = m_pDevice;

	BOOL bOrigin = TRUE;

	bOrigin = MotionManager->SetMotorOrigin(AX_Image_Shutter);
	bOrigin = MotionManager->SetMotorOrigin(AX_Image_StageX);

	if (FALSE == bOrigin)
		return RC_Motion_Err_Origin_Start;

	clock_t start_tm = clock();

	do
	{
		// 타임 아웃
		if ((DWORD)clock() - start_tm > MotorOriginWaitTime)
		{
			MotionManager->SetOriginStop(AX_Image_Shutter);
			MotionManager->SetOriginStop(AX_Image_StageX);
			return RC_Motion_Err_OriginTimeOut;
		}

		// 강제 정지
		if (MotionManager->GetOriginStopStatus(AX_Image_Shutter) || MotionManager->GetOriginStopStatus(AX_Image_StageX))
		{
			return RC_Motion_Err_OriginUserStop;
		}

		DoEvents(1);

	} while (!MotionManager->GetOriginStatus(AX_Image_Shutter) || !MotionManager->GetOriginStatus(AX_Image_StageX));

	bOrigin  = MotionManager->SetMotorOrigin(AX_Image_StageY);
	bOrigin &= MotionManager->SetMotorOrigin(AX_Image_StageZ);
	bOrigin &= MotionManager->SetMotorOrigin(AX_Image_StageR);

	if (FALSE == bOrigin)
		return RC_Motion_Err_Origin_Start;

	start_tm = clock();

	do
	{
		// 타임 아웃
		if ((DWORD)clock() - start_tm > MotorOriginWaitTime)
		{
			MotionManager->SetOriginStop(AX_Image_StageY);
			MotionManager->SetOriginStop(AX_Image_StageZ);
			MotionManager->SetOriginStop(AX_Image_StageR);
			return RC_Motion_Err_OriginTimeOut;
		}

		// 강제 정지
		if (MotionManager->GetOriginStopStatus(AX_Image_StageY) || MotionManager->GetOriginStopStatus(AX_Image_StageZ) || MotionManager->GetOriginStopStatus(AX_Image_StageR))
		{
			return RC_Motion_Err_OriginUserStop;
		}

		DoEvents(1);

	} while (!MotionManager->GetOriginStatus(AX_Image_StageY) || !MotionManager->GetOriginStatus(AX_Image_StageZ) || !MotionManager->GetOriginStatus(AX_Image_StageR));

	lReturn = OnActionLoad_IgmT();

	return RC_OK;
}

//=============================================================================
// Method		: MotorOrigin_Foc
// Access		: public  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/3/15 - 10:23
// Desc.		:
//=============================================================================
LRESULT CTestSequence::MotorOrigin_Foc()
{
	LRESULT	lReturn = RC_OK;

	CMotionManager* MotionManager = m_pDevice;

	BOOL bOrigin = TRUE;

	lReturn = OnActionTesting_Foc_Fix(OFF);

	if (RC_OK != lReturn)
		return lReturn;

	Sleep(200);
	lReturn = OnActionTesting_Foc_Gripper(OFF);
	
	if (RC_OK != lReturn)
		return lReturn; 
	
	Sleep(200);
	lReturn = OnActionTesting_Foc_Driver(UP);

	if (RC_OK != lReturn)
		return lReturn;

	Sleep(200);

	lReturn = OnActionTesting_Foc_Collet(DN);

	if (lReturn != RC_OK)
		return lReturn;

	Sleep(200);
	lReturn = OnActionTesting_Foc_Stage(UP);
	
	if (RC_OK != lReturn)
		return lReturn;
	

	Sleep(200);
	lReturn = OnActionTesting_Foc_Particle(OFF);

	if (RC_OK != lReturn)
		return lReturn;

	bOrigin = MotionManager->SetMotorOrigin(AX_Focus_StageX);
	bOrigin &= MotionManager->SetMotorOrigin(AX_Focus_StageY);
	bOrigin &= MotionManager->SetMotorOrigin(AX_Focus_StageR);

	if (FALSE == bOrigin)
		return RC_Motion_Err_Origin_Start;

	clock_t start_tm = clock();

	do
	{
		// 타임 아웃
		if ((DWORD)clock() - start_tm > MotorOriginWaitTime)
		{
			MotionManager->SetOriginStop(AX_Focus_StageX);
			MotionManager->SetOriginStop(AX_Focus_StageY);
			MotionManager->SetOriginStop(AX_Focus_StageR);
			return RC_Motion_Err_OriginTimeOut;
		}

		// 강제 정지
		if (MotionManager->GetOriginStopStatus(AX_Focus_StageX) || MotionManager->GetOriginStopStatus(AX_Focus_StageY) || MotionManager->GetOriginStopStatus(AX_Focus_StageR))
		{
			return RC_Motion_Err_OriginUserStop;
		}

		DoEvents(1);

	} while (!MotionManager->GetOriginStatus(AX_Focus_StageX) || !MotionManager->GetOriginStatus(AX_Focus_StageY) || !MotionManager->GetOriginStatus(AX_Focus_StageR));

	// 센터로 이동
	lReturn = OnActionInit_Foc();

	return RC_OK;
}

//=============================================================================
// Method		: MotorAllOriginStop
// Access		: public  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/10/14 - 20:16
// Desc.		:
//=============================================================================
LRESULT CTestSequence::MotorAllOriginStop()
{
	// Handle 확인
	if (NULL == m_pDevice)
		return RC_Invalid_Handle;

	// 보드 OPEN 상태 확인
	if (FALSE == m_pDevice->GetBoardOpen())
		return RC_Motion_Err_BoardOpen;

	if (FALSE == m_pDevice->SetAllOriginStop())
		return RC_Motion_Err_OriginStop;

	return RC_OK;
}

//=============================================================================
// Method		: MotorMoveAxis
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in int iAxis
// Parameter	: __in double dbPos
// Qualifier	:
// Last Update	: 2017/10/14 - 20:42
// Desc.		:
//=============================================================================
LRESULT CTestSequence::MotorMoveAxis(__in int iAxis, __in double dbPos)
{
	LRESULT lReturn = RC_OK;

	lReturn = MotorGetStatus();

	if (lReturn != RC_OK)
		return lReturn;

	if (FALSE == m_pDevice->MotorAxisMove(POS_ABS_MODE, iAxis, dbPos))
		return RC_Motion_Err_MoveAxis;

	return lReturn;
}

//=============================================================================
// Method		: IsReadyOk
// Access		: private  
// Returns		: LRESULT
// Parameter	: UINT nPort
// Parameter	: BOOL bStatus
// Qualifier	:
// Last Update	: 2017/10/14 - 20:46
// Desc.		:
//=============================================================================
LRESULT CTestSequence::IsReadyOk(UINT nPort, BOOL bStatus /*= TRUE*/)
{
	// 보드 연결 상태 확인
	if (NULL == m_pDevice)
		return RC_Invalid_Handle;

	// 보드 연결 상태 확인
	if (FALSE == m_pDIO->AXTState())
		return RC_Motion_Err_BoardOpen;

	clock_t	start_tm = clock();
	do
	{
		if (m_pDIO->Get_DI_Status(nPort) == bStatus)
		{
			return RC_OK;
			break;
		}
		
		DoEvents(1);

	} while (((DWORD)(clock() - start_tm) < DigitalIoWaitTime));

	return RC_Motion_Err_Port_TimeOut;
}

//=============================================================================
// Method		: OnActionTesting
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in double dbDistanceX
// Parameter	: __in double dbDistanceY
// Parameter	: __in double dbDegreeR
// Qualifier	:
// Last Update	: 2018/2/20 - 18:47
// Desc.		:
//=============================================================================
LRESULT CTestSequence::OnActionTesting(__in double dbDistanceX /*= 0*/, __in double dbDistanceY /*= 0*/, __in double dbDegreeR /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	if (m_pstTeachInfo == NULL)
		return RC_Motion_Err_EmptyTeach;

	switch (m_SysType)
	{
	case Sys_2D_Cal:
		lReturn = OnActionTesting_2D_CAL(dbDistanceX, dbDistanceY, dbDegreeR);
		break;
	case Sys_Image_Test:
		break;
	case Sys_3D_Cal:
		break;
	case Sys_Stereo_Cal:
		break;
	case Sys_Focusing:
		break;
	default:
		break;
	}

	return lReturn;
}

//=============================================================================
// Method		: OnActionTesting_2D_CAL
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in double dbDistanceX
// Parameter	: __in double dbDistanceY
// Parameter	: __in double dbDegreeR
// Qualifier	:
// Last Update	: 2018/2/20 - 18:44
// Desc.		:
//=============================================================================
LRESULT CTestSequence::OnActionTesting_2D_CAL(__in double dbDistanceX, __in double dbDistanceY, __in double dbDegreeR)
{
	LRESULT lReturn = RC_OK;

	double dbTestingY = 0.0;

	lReturn = MotorGetStatus();

	if (lReturn != RC_OK)
		return lReturn;

	dbTestingY = m_pstTeachInfo->dbTeachData[TC_2D_Center_Y] + (m_pstTeachInfo->dbTeachData[TC_2D_Distance] - dbDistanceY);

	// chart R
	if (RC_OK != MotorMoveAxis(AX_2D_ChartR, dbDegreeR))
		return RC_Motion_Err_Chart;

	// move Y
	if (RC_OK != MotorMoveAxis(AX_2D_StageY, dbTestingY))
		return RC_Motion_Err_Left_StageY;

	// move X
	if (RC_OK != MotorMoveAxis(AX_2D_StageX, dbDistanceX))
		return RC_Motion_Err_Left_StageX;

	return lReturn;
}

//=============================================================================
// Method		: OnActionTesting_Stereo
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in double dbDistanceX
// Parameter	: __in double dbDistanceY
// Parameter	: __in double dbDistanceZ
// Parameter	: __in double dbChartTX
// Parameter	: __in double dbChartTZ
// Parameter	: __in BOOL bUseX
// Parameter	: __in BOOL bUseY
// Parameter	: __in BOOL bUseZ
// Parameter	: __in BOOL bUseTX
// Parameter	: __in BOOL bUseTZ
// Qualifier	:
// Last Update	: 2018/3/15 - 10:22
// Desc.		:
//=============================================================================
LRESULT CTestSequence::OnActionTesting_Stereo(__in double dbDistanceX, __in double dbDistanceY, __in double dbDistanceZ, __in double dbChartTX, __in double dbChartTZ, __in BOOL bUseX, __in BOOL bUseY, __in BOOL bUseZ, __in BOOL bUseTX, __in BOOL bUseTZ)
{
	LRESULT lReturn = RC_OK;

	lReturn = MotorGetStatus();

	if (lReturn != RC_OK)
		return lReturn;

	double dbMoveX = 0;
	double dbMoveY = 0;
	double dbMoveZ = 0;

	switch (m_ModelType)
	{
	case Model_OMS_Front:
	case Model_OMS_Front_Set:
		dbMoveX = dbDistanceX + m_pstTeachInfo->dbTeachData[TC_Stereo_FRONT_Center_X];
		dbMoveY = dbDistanceY - m_pstTeachInfo->dbTeachData[TC_Stereo_FRONT_Distance];
		dbMoveZ = dbDistanceZ + m_pstTeachInfo->dbTeachData[TC_Stereo_FRONT_Center_Z];
		break;
	case Model_MRA2:
		dbMoveX = dbDistanceX + m_pstTeachInfo->dbTeachData[TC_Stereo_MRA_Center_X];
		dbMoveY = dbDistanceY - m_pstTeachInfo->dbTeachData[TC_Stereo_MRA_Distance];
		dbMoveZ = dbDistanceZ + m_pstTeachInfo->dbTeachData[TC_Stereo_MRA_Center_Z];
		break;
	case Model_IKC:
		dbMoveX = dbDistanceX + m_pstTeachInfo->dbTeachData[TC_Stereo_IKC_Center_X];
		dbMoveY = dbDistanceY - m_pstTeachInfo->dbTeachData[TC_Stereo_IKC_Distance];
		dbMoveZ = dbDistanceZ + m_pstTeachInfo->dbTeachData[TC_Stereo_IKC_Center_Z];
		break;
	default:
		break;
	}

	if (FALSE == bUseX)
		dbMoveX = m_pDevice->GetCurrentPos(AX_Stereo_ChartX);

	if (FALSE == bUseY)
		dbMoveY = m_pDevice->GetCurrentPos(AX_Stereo_StageY);

	if (FALSE == bUseZ)
		dbMoveZ = m_pDevice->GetCurrentPos(AX_Stereo_ChartZ);

	if (FALSE == bUseTX)
		dbChartTX = m_pDevice->GetCurrentPos(AX_Stereo_ChartTX);

	if (FALSE == bUseTZ)
		dbChartTZ = m_pDevice->GetCurrentPos(AX_Stereo_ChartTZ);

	if (RC_OK != MotorMoveAxis(AX_Stereo_ChartZ, dbMoveZ))
		return RC_Motion_Err_Left_StageX;

	// 멀티축 구조체
	ST_AxisMoveParam stAxisParam[4];

	stAxisParam[0].dbPos = dbMoveX;
	stAxisParam[1].dbPos = dbMoveY;
	stAxisParam[2].dbPos = dbChartTX;
	stAxisParam[3].dbPos = dbChartTZ;

	for (UINT nAxis = AX_Stereo_ChartX; nAxis <= AX_Stereo_ChartTZ; nAxis++)
	{
		stAxisParam[nAxis - AX_Stereo_ChartX].lAxisNum = m_pDevice->m_AllMotorData.pMotionParam[nAxis].iAxisNum;
		stAxisParam[nAxis - AX_Stereo_ChartX].dbVel = m_pDevice->m_AllMotorData.pMotionParam[nAxis].dbVel;
		stAxisParam[nAxis - AX_Stereo_ChartX].dbAcc = m_pDevice->m_AllMotorData.pMotionParam[nAxis].dbAcc;
		stAxisParam[nAxis - AX_Stereo_ChartX].dbDec = m_pDevice->m_AllMotorData.pMotionParam[nAxis].dbAcc;
	}

	lReturn = m_pDevice->MotorMultiMove(POS_ABS_MODE, 4, stAxisParam);

	return lReturn;
}

//=============================================================================
// Method		: OnActionTesting_ImgT_Change
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/5/11 - 13:56
// Desc.		:
//=============================================================================
LRESULT CTestSequence::OnActionTesting_ImgT_Change(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = MotorGetStatus();

	if (lReturn != RC_OK)
		return lReturn;

	double dbStageR	= 0.0;

	switch (m_ModelType)
	{
	case Model_OMS_Entry:
		dbStageR = m_pstTeachInfo->dbTeachData[TC_Imgt_Load_R_Entry];
		break;
	case Model_OMS_Front:
	case Model_OMS_Front_Set:
	case Model_IKC:
	case Model_MRA2:
		dbStageR = m_pstTeachInfo->dbTeachData[TC_Imgt_Load_R];
		break;
	default:
		break;
	}
	
	if (IDYES == AfxMessageBox(_T("Are you sure you want to change the stage R Axis??"), MB_YESNO))
	{
		// 1. X축 센터로 이동
		if (RC_OK != MotorMoveAxis(AX_Image_StageR, dbStageR))
			return RC_Motion_Err_Left_StageR;
	}

	return lReturn;
}

//=============================================================================
// Method		: OnActionTesting_Chart_ImgT
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/22 - 9:23
// Desc.		:
//=============================================================================
LRESULT CTestSequence::OnActionTesting_ImgT_Chart(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = MotorGetStatus();

	if (lReturn != RC_OK)
		return lReturn;

	double dbCenterX = 0.0;

	switch (m_ModelType)
	{
	case Model_OMS_Entry:
		dbCenterX = m_pstTeachInfo->dbTeachData[TC_Imgt_Test_X_Entry];
		break;

	case Model_OMS_Front:
		dbCenterX = m_pstTeachInfo->dbTeachData[TC_Imgt_Chart_Front_X];
		break;

	case Model_OMS_Front_Set:
		dbCenterX = m_pstTeachInfo->dbTeachData[TC_Imgt_Chart_Front_X];
		break;

	case Model_MRA2:
		if (Para_Left == nParaIdx)
		{
			dbCenterX = m_pstTeachInfo->dbTeachData[TC_Imgt_Chart_MRA2L_X];
		}
		else
		{
			dbCenterX = m_pstTeachInfo->dbTeachData[TC_Imgt_Chart_MRA2R_X];
		}
		break;

	case Model_IKC:
		dbCenterX = m_pstTeachInfo->dbTeachData[TC_Imgt_Chart_IKC_X];
		break;

	default:
		break;
	}


	// 1. X축 이물 광원 충돌 확인
	double dbGetPuls = m_pDevice->GetCurrentPos(AX_Image_StageX);

	if (m_pstTeachInfo->dbTeachData[TC_Imgt_Crash_X] <= dbGetPuls)
	{
		if (RC_OK != MotorMoveAxis(AX_Image_StageX, m_pstTeachInfo->dbTeachData[TC_Imgt_Load_X]))
			return RC_Motion_Err_MotorCrash;
	}

	// 1. X축 센터로 이동
	if (RC_OK != MotorMoveAxis(AX_Image_StageX, dbCenterX))
		return RC_Motion_Err_Left_StageR;

	return lReturn;
}

//=============================================================================
// Method		: OnActionTesting_Par_ImgT
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/22 - 12:50
// Desc.		:
//=============================================================================
LRESULT CTestSequence::OnActionTesting_ImgT_Par(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = MotorGetStatus();

	if (lReturn != RC_OK)
		return lReturn;

	double dbStageX = 0.0;
	double dbStageY = 0.0;

	switch (m_ModelType)
	{
	case Model_OMS_Entry:
		dbStageX = m_pstTeachInfo->dbTeachData[TC_Imgt_Par_X_Entry];
		dbStageY = m_pstTeachInfo->dbTeachData[TC_Imgt_Par_Y_Entry];
		break;

	case Model_OMS_Front:
		dbStageX = m_pstTeachInfo->dbTeachData[TC_Imgt_Par_Front_X];
		dbStageY = m_pstTeachInfo->dbTeachData[TC_Imgt_Par_Y];
		break;

	case Model_OMS_Front_Set:
		dbStageX = m_pstTeachInfo->dbTeachData[TC_Imgt_Par_Front_X];
		dbStageY = m_pstTeachInfo->dbTeachData[TC_Imgt_Par_Y];
		break;

	case Model_MRA2:
		if (Para_Left == nParaIdx)
		{
			dbStageX = m_pstTeachInfo->dbTeachData[TC_Imgt_Par_MRA2L_X];
			dbStageY = m_pstTeachInfo->dbTeachData[TC_Imgt_Par_Y];
		}
		else
		{
			dbStageX = m_pstTeachInfo->dbTeachData[TC_Imgt_Par_MRA2R_X];
			dbStageY = m_pstTeachInfo->dbTeachData[TC_Imgt_Par_Y];
		}
		break;

	case Model_IKC:
		dbStageX = m_pstTeachInfo->dbTeachData[TC_Imgt_Par_IKC_X];
		dbStageY = m_pstTeachInfo->dbTeachData[TC_Imgt_Par_Y];
		break;

	default:
		break;
	}

	// 1. y축 이물 광원 이동 위치 인지 확인
	double dbGetPuls = m_pDevice->GetCurrentPos(AX_Image_StageY);
	
	if (dbStageY != dbGetPuls)
	{
		// 1. X축 센터로 이동
		if (RC_OK != MotorMoveAxis(AX_Image_StageX, m_pstTeachInfo->dbTeachData[TC_Imgt_Load_X]))
			return RC_Motion_Err_Left_StageR;
	}

	if (RC_OK != MotorMoveAxis(AX_Image_StageY, dbStageY))
		return RC_Motion_Err_Left_StageR;

	// 1. X축 센터로 이동
	if (RC_OK != MotorMoveAxis(AX_Image_StageX, dbStageX))
		return RC_Motion_Err_Left_StageR;

	return lReturn;
}

//=============================================================================
// Method		: OnActionTesting_ImgT_Y
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in double dbDistanceY
// Qualifier	:
// Last Update	: 2018/2/22 - 12:55
// Desc.		:
//=============================================================================
LRESULT CTestSequence::OnActionTesting_ImgT_Y(__in double dbDistanceY, __in BOOL bUseShutter /*= TRUE*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = MotorGetStatus();

	double dbStageY = (dbDistanceY - m_pstTeachInfo->dbTeachData[TC_Imgt_Distance]) * 1000;

	if (lReturn != RC_OK)
		return lReturn;

	// 1. y축 이물 광원 이동 위치 인지 확인
	double dbGetPuls = m_pDevice->GetCurrentPos(AX_Image_StageX);

	// shutter
	if (bUseShutter)
	{
		if (RC_OK != MotorMoveAxis(AX_Image_Shutter, m_pstTeachInfo->dbTeachData[TC_Imgt_Shutter]))
			return RC_Motion_Err_Left_StageR;
	}

	if (m_pstTeachInfo->dbTeachData[TC_Imgt_Crash_X] <= dbGetPuls)
	{
		if (RC_OK != MotorMoveAxis(AX_Image_StageX, m_pstTeachInfo->dbTeachData[TC_Imgt_Load_X]))
			return RC_Motion_Err_MotorCrash;
	}

	if (RC_OK != MotorMoveAxis(AX_Image_StageY, dbStageY))
		return RC_Motion_Err_Left_StageX;

	return lReturn;
}

//=============================================================================
// Method		: OnActionTesting_Foc_Fix
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in BOOL bOnOff
// Parameter	: __in UINT nParaID
// Qualifier	:
// Last Update	: 2018/3/15 - 11:24
// Desc.		:
//=============================================================================
LRESULT CTestSequence::OnActionTesting_Foc_Fix(__in BOOL bOnOff, __in UINT nParaID /*= 0*/)
{
	LRESULT lReturn = RC_OK;
	UINT nPort = 0;

	// 보드 연결 상태 확인
	lReturn = m_pDIO->AXTState();

	if (RC_OK != lReturn)
		return lReturn;

	if (bOnOff == ON)
	{
		m_pDIO->Set_DO_Status(DO_Fo_17_FixOffCylinder, IO_SignalT_SetOff);
		m_pDIO->Set_DO_Status(DO_Fo_16_FixOnCylinder, IO_SignalT_SetOn);

		switch (m_ModelType)
		{
		case Model_OMS_Entry:
		case Model_OMS_Front:
		case Model_OMS_Front_Set:
		case Model_IKC:
			lReturn = IsReadyOk(DI_Fo_16_FixOnSensorL);

			if (RC_OK != lReturn)
			{
				m_Log.LogMsg_Err(_T("[CYL] CHECK FIX ON SENSER[L]"));
				return lReturn;
			}

			lReturn = IsReadyOk(DI_Fo_17_FixOnSensorR);

			if (RC_OK != lReturn)
			{
				m_Log.LogMsg_Err(_T("[CYL] CHECK FIX ON SENSER[R]"));
				return lReturn;
			}

			break;
		case Model_MRA2:

			if (Para_Left == nParaID)
				nPort = DI_Fo_17_FixOnSensorR;
			else
				nPort = DI_Fo_16_FixOnSensorL;

			lReturn = IsReadyOk(nPort);

			if (RC_OK != lReturn)
			{
			
				if (Para_Left == nParaID)
					m_Log.LogMsg_Err(_T("[CYL] CHECK FIX ON SENSER[L]"));
				else
					m_Log.LogMsg_Err(_T("[CYL] CHECK FIX ON SENSER[R]"));
				
				return lReturn;
			}

			break;
		default:
			break;
		}

		if (RC_OK != lReturn)
			return lReturn;
	}

	if (bOnOff == OFF)
	{
		m_pDIO->Set_DO_Status(DO_Fo_16_FixOnCylinder, IO_SignalT_SetOff);
		m_pDIO->Set_DO_Status(DO_Fo_17_FixOffCylinder, IO_SignalT_SetOn);

		lReturn = IsReadyOk(DI_Fo_18_FixOffSensorL);

		if (RC_OK != lReturn)
		{
			m_Log.LogMsg_Err(_T("[CYL] CHECK FIX OFF SENSER[L]"));
			return lReturn;
		}

		lReturn = IsReadyOk(DI_Fo_19_FixOffSensorR);

		if (RC_OK != lReturn)
		{
			m_Log.LogMsg_Err(_T("[CYL] CHECK FIX OFF SENSER[R]"));
			return lReturn;
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: OnActionTesting_Foc_Gripper
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2018/3/4 - 18:49
// Desc.		:
//=============================================================================
LRESULT CTestSequence::OnActionTesting_Foc_Gripper(__in BOOL bOnOff)
{
	LRESULT lReturn = RC_OK;

	// 보드 연결 상태 확인
	lReturn = m_pDIO->AXTState();

	if (RC_OK != lReturn)
		return lReturn;

	if (bOnOff == ON)
	{
		m_pDIO->Set_DO_Status(DO_Fo_29_GripperOffCylinder, IO_SignalT_SetOff);
		m_pDIO->Set_DO_Status(DO_Fo_28_GripperOnCylinder, IO_SignalT_SetOn);

		lReturn = IsReadyOk(DI_Fo_28_GripperOnSensor);

		if (RC_OK != lReturn)
		{
			m_Log.LogMsg_Err(_T("[CYL] CHECK GRIPPER ON SENSER"));
			return lReturn;
		}
	}

	if (bOnOff == OFF)
	{
		m_pDIO->Set_DO_Status(DO_Fo_28_GripperOnCylinder, IO_SignalT_SetOff);
		m_pDIO->Set_DO_Status(DO_Fo_29_GripperOffCylinder, IO_SignalT_SetOn);

		lReturn = IsReadyOk(DI_Fo_29_GripperOffSensor);

		if (RC_OK != lReturn)
		{
			m_Log.LogMsg_Err(_T("[CYL] CHECK GRIPPER OFF SENSER"));
			return lReturn;
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: OnActionTesting_Foc_Stage
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in BOOL bUpDn
// Qualifier	:
// Last Update	: 2018/3/4 - 18:49
// Desc.		:
//=============================================================================
LRESULT CTestSequence::OnActionTesting_Foc_Stage(__in BOOL bUpDn)
{
	LRESULT lReturn = RC_OK;

	// 보드 연결 상태 확인
	lReturn = m_pDIO->AXTState();

	if (RC_OK != lReturn)
		return lReturn;

	if (bUpDn == UP)
	{
		m_pDIO->Set_DO_Status(DO_Fo_27_GripperDnCylinder, IO_SignalT_SetOff);
		m_pDIO->Set_DO_Status(DO_Fo_26_GripperUpCylinder, IO_SignalT_SetOn);

		lReturn = IsReadyOk(DI_Fo_26_GripperUpSensor);
		
		if (RC_OK != lReturn)
		{
			m_Log.LogMsg_Err(_T("[CYL] CHECK STAGE UP SENSER"));
			return lReturn;
		}
	}

	if (bUpDn == DN)
	{
		m_pDIO->Set_DO_Status(DO_Fo_26_GripperUpCylinder, IO_SignalT_SetOff);
		m_pDIO->Set_DO_Status(DO_Fo_27_GripperDnCylinder, IO_SignalT_SetOn);

		lReturn = IsReadyOk(DI_Fo_27_GripperDnSensor);

		if (RC_OK != lReturn)
		{
			m_Log.LogMsg_Err(_T("[CYL] CHECK STAGE DOWN SENSER"));
			return lReturn;
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: OnActionTesting_Foc_Particle
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in BOOL bInOut
// Qualifier	:
// Last Update	: 2018/3/4 - 18:38
// Desc.		:
//=============================================================================
LRESULT CTestSequence::OnActionTesting_Foc_Particle(__in BOOL bInOut)
{
	LRESULT lReturn = RC_OK;

	// 보드 연결 상태 확인
	lReturn = m_pDIO->AXTState();

	if (RC_OK != lReturn)
		return lReturn;

	if (bInOut == ON)
	{
		m_pDIO->Set_DO_Status(DO_Fo_31_ParticleOutCylinder, IO_SignalT_SetOff);
		m_pDIO->Set_DO_Status(DO_Fo_30_ParticleInCylinder, IO_SignalT_SetOn);

		lReturn = IsReadyOk(DI_Fo_30_ParticleInSensor);

		if (RC_OK != lReturn)
		{
			m_Log.LogMsg_Err(_T("[CYL] CHECK PARTICLE IN SENSER"));
			return lReturn;
		}
	}

	if (bInOut == OFF)
	{
		m_pDIO->Set_DO_Status(DO_Fo_30_ParticleInCylinder, IO_SignalT_SetOff);
		m_pDIO->Set_DO_Status(DO_Fo_31_ParticleOutCylinder, IO_SignalT_SetOn);

		lReturn = IsReadyOk(DI_Fo_31_ParticleOutSensor);

		if (RC_OK != lReturn)
		{
			m_Log.LogMsg_Err(_T("[CYL] CHECK PARTICLE OUT SENSER"));
			return lReturn;
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: OnActionTesting_Foc_Driver
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in BOOL bUpDn
// Qualifier	:
// Last Update	: 2018/3/12 - 9:09
// Desc.		:
//=============================================================================
LRESULT CTestSequence::OnActionTesting_Foc_Driver(__in BOOL bUpDn)
{
	LRESULT lReturn = RC_OK;

	// 보드 연결 상태 확인
	lReturn = m_pDIO->AXTState();

	if (RC_OK != lReturn)
		return lReturn;

	if (bUpDn == UP)
	{
		m_pDIO->Set_DO_Status(DO_Fo_25_DriverDownCylinder, IO_SignalT_SetOff);
		m_pDIO->Set_DO_Status(DO_Fo_24_DriverUpCylinder, IO_SignalT_SetOn);

		lReturn = IsReadyOk(DI_Fo_24_DriverUpSensor);

		if (RC_OK != lReturn)
		{
			m_Log.LogMsg_Err(_T("[CYL] CHECK DRIVER UP SENSER"));
			return lReturn;
		}
	}

	if (bUpDn == DN)
	{
		m_pDIO->Set_DO_Status(DO_Fo_24_DriverUpCylinder, IO_SignalT_SetOff);
		m_pDIO->Set_DO_Status(DO_Fo_25_DriverDownCylinder, IO_SignalT_SetOn);

		lReturn = IsReadyOk(DI_Fo_25_DriverDownSensor);

		if (RC_OK != lReturn)
		{
			m_Log.LogMsg_Err(_T("[CYL] CHECK DRIVER DOWN SENSER"));
			return lReturn;
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: OnActionTesting_Foc_Collet
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in BOOL bUpDn
// Qualifier	:
// Last Update	: 2018/10/16 - 11:16
// Desc.		:
//=============================================================================
LRESULT CTestSequence::OnActionTesting_Foc_Collet(__in BOOL bUpDn)
{
	LRESULT lReturn = RC_OK;

	// 보드 연결 상태 확인
	lReturn = m_pDIO->AXTState();

	if (RC_OK != lReturn)
		return lReturn;

	if (bUpDn == UP)
	{
		m_pDIO->Set_DO_Status(DO_Fo_05_ColletDnCylinder, IO_SignalT_SetOff);
		m_pDIO->Set_DO_Status(DO_Fo_04_ColletUpCylinder, IO_SignalT_SetOn);

		lReturn = IsReadyOk(DI_Fo_05_ColletUpSensor);

		if (RC_OK != lReturn)
		{
			m_Log.LogMsg_Err(_T("[CYL] CHECK STAGE UP SENSER"));
			return lReturn;
		}
	}

	if (bUpDn == DN)
	{
		m_pDIO->Set_DO_Status(DO_Fo_04_ColletUpCylinder, IO_SignalT_SetOff);
		m_pDIO->Set_DO_Status(DO_Fo_05_ColletDnCylinder, IO_SignalT_SetOn);

		lReturn = IsReadyOk(DI_Fo_06_ColletDnSensor);

		if (RC_OK != lReturn)
		{
			m_Log.LogMsg_Err(_T("[CYL] CHECK STAGE DOWN SENSER"));
			return lReturn;
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: OnActionStandby
// Access		: public  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/20 - 18:35
// Desc.		:
//=============================================================================
LRESULT CTestSequence::OnActionStandby()
{
	LRESULT lReturn = RC_OK;

	if (m_pstTeachInfo == NULL)
		return RC_Motion_Err_EmptyTeach;

	switch (m_SysType)
	{
	case Sys_2D_Cal:
		lReturn = OnActionStandby_2D_CAL();
		break;

	case Sys_3D_Cal:
		break;

	case Sys_Stereo_Cal:
		lReturn = OnActionStandby_Stereo();
		break;

	case Sys_Focusing:
		break;

	case Sys_Image_Test:
		break;

	default:
		break;
	}
	return lReturn;
}

//=============================================================================
// Method		: OnActionStandby_2D_CAL
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in BOOL bUseShutter
// Qualifier	:
// Last Update	: 2018/3/13 - 10:23
// Desc.		:
//=============================================================================
LRESULT CTestSequence::OnActionStandby_2D_CAL(__in BOOL bUseShutter /*= TRUE*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = MotorGetStatus();

	if (lReturn != RC_OK)
		return lReturn;

	lReturn = MotorGetStatus();
	if (lReturn != RC_OK)
		return lReturn;
	
	// shutter
	if (bUseShutter)
	{
		if (RC_OK != MotorMoveAxis(AX_2D_Shutter, m_pstTeachInfo->dbTeachData[TC_2D_ShutterClose]))
			return RC_Motion_Err_Left_StageR;
	}

	// stage y
	if (RC_OK != MotorMoveAxis(AX_2D_StageY, m_pstTeachInfo->dbTeachData[TC_2D_Center_Y]))
		return RC_Motion_Err_Left_StageY;

	// stage x
	if (RC_OK != MotorMoveAxis(AX_2D_StageX, m_pstTeachInfo->dbTeachData[TC_2D_Center_X]))
		return RC_Motion_Err_Left_StageX;

	// stage r
	if (RC_OK != MotorMoveAxis(AX_2D_StageR, m_pstTeachInfo->dbTeachData[TC_2D_Center_R]))
		return RC_Motion_Err_Left_StageR;

	return lReturn;
}

//=============================================================================
// Method		: OnActionStandby_Stereo
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in BOOL bUseShutter
// Qualifier	:
// Last Update	: 2018/4/16 - 16:34
// Desc.		:
//=============================================================================
LRESULT CTestSequence::OnActionStandby_Stereo(__in BOOL bUseShutter /*= TRUE*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = MotorGetStatus();

	if (lReturn != RC_OK)
		return lReturn;

	lReturn = MotorGetStatus();

	if (lReturn != RC_OK)
		return lReturn;

	double dbStageX = 0;
	double dbStageZ = 0;

	switch (m_ModelType)
	{
	case Model_OMS_Front:
	case Model_OMS_Front_Set:
		dbStageX = m_pstTeachInfo->dbTeachData[TC_Stereo_FRONT_Center_X];
		dbStageZ = m_pstTeachInfo->dbTeachData[TC_Stereo_FRONT_Center_Z];
		break;
	case Model_IKC:
		dbStageX = m_pstTeachInfo->dbTeachData[TC_Stereo_IKC_Center_X];
		dbStageZ = m_pstTeachInfo->dbTeachData[TC_Stereo_IKC_Center_Z];
		break;
	case Model_MRA2:
		dbStageX = m_pstTeachInfo->dbTeachData[TC_Stereo_MRA_Center_X];
		dbStageZ = m_pstTeachInfo->dbTeachData[TC_Stereo_MRA_Center_Z];
		break;
	default:
		break;
	}

	if (TRUE == bUseShutter)
	{
		if (RC_OK != MotorMoveAxis(AX_Stereo_Shutter, m_pstTeachInfo->dbTeachData[TC_Stereo_ShutterClose]))
			return RC_Motion_Err_Left_StageR;
	}
	
	if (RC_OK != MotorMoveAxis(AX_Stereo_ChartZ, dbStageZ))
		return RC_Motion_Err_Left_StageX;

	// 멀티축 구조체
	ST_AxisMoveParam stAxisParam[3];

	stAxisParam[0].lAxisNum = m_pDevice->m_AllMotorData.pMotionParam[AX_Stereo_ChartX].iAxisNum;
	stAxisParam[0].dbPos = dbStageX;

	stAxisParam[1].lAxisNum = m_pDevice->m_AllMotorData.pMotionParam[AX_Stereo_ChartTX].iAxisNum;
	stAxisParam[1].dbPos = 0;

	stAxisParam[2].lAxisNum = m_pDevice->m_AllMotorData.pMotionParam[AX_Stereo_ChartTZ].iAxisNum;
	stAxisParam[2].dbPos = 0;

	stAxisParam[0].dbVel = m_pDevice->m_AllMotorData.pMotionParam[AX_Stereo_ChartX].dbVel;
	stAxisParam[0].dbAcc = m_pDevice->m_AllMotorData.pMotionParam[AX_Stereo_ChartX].dbAcc;
	stAxisParam[0].dbDec = m_pDevice->m_AllMotorData.pMotionParam[AX_Stereo_ChartX].dbAcc;

	stAxisParam[1].dbVel = m_pDevice->m_AllMotorData.pMotionParam[AX_Stereo_ChartTX].dbVel;
	stAxisParam[1].dbAcc = m_pDevice->m_AllMotorData.pMotionParam[AX_Stereo_ChartTX].dbAcc;
	stAxisParam[1].dbDec = m_pDevice->m_AllMotorData.pMotionParam[AX_Stereo_ChartTX].dbAcc;

	stAxisParam[2].dbVel = m_pDevice->m_AllMotorData.pMotionParam[AX_Stereo_ChartTZ].dbVel;
	stAxisParam[2].dbAcc = m_pDevice->m_AllMotorData.pMotionParam[AX_Stereo_ChartTZ].dbAcc;
	stAxisParam[2].dbDec = m_pDevice->m_AllMotorData.pMotionParam[AX_Stereo_ChartTZ].dbAcc;

	lReturn = m_pDevice->MotorMultiMove(POS_ABS_MODE, 3, stAxisParam);

	return lReturn;
}

//=============================================================================
// Method		: OnActionStandby_ImgT
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in BOOL bUseShutter
// Qualifier	:
// Last Update	: 2018/4/10 - 18:13
// Desc.		:
//=============================================================================
LRESULT CTestSequence::OnActionStandby_ImgT(__in BOOL bUseShutter /*= TRUE*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = MotorGetStatus();

	if (lReturn != RC_OK)
		return lReturn;

	lReturn = MotorGetStatus();

	if (lReturn != RC_OK)
		return lReturn;

	// shutter
	if (bUseShutter)
	{
		if (RC_OK != MotorMoveAxis(AX_Image_Shutter, m_pstTeachInfo->dbTeachData[TC_Imgt_Shutter]))
			return RC_Motion_Err_Left_StageR;
	}

	double dbStageZ = 0;
	double dbStageR = 0;

	switch (m_ModelType)
	{
	case Model_OMS_Front:
		dbStageZ = m_pstTeachInfo->dbTeachData[TC_Imgt_Test_Front_Z];
		dbStageR = m_pstTeachInfo->dbTeachData[TC_Imgt_Test_R];
		break;

	case Model_OMS_Front_Set:
		dbStageZ = m_pstTeachInfo->dbTeachData[TC_Imgt_Test_Front_Z];
		dbStageR = m_pstTeachInfo->dbTeachData[TC_Imgt_Test_R];
		break;

	case Model_MRA2:
		dbStageZ = m_pstTeachInfo->dbTeachData[TC_Imgt_Test_MRA2_Z];
		dbStageR = m_pstTeachInfo->dbTeachData[TC_Imgt_Test_MRA2_R];
		break;

	case Model_IKC:
		dbStageZ = m_pstTeachInfo->dbTeachData[TC_Imgt_Test_IKC_Z];
		dbStageR = m_pstTeachInfo->dbTeachData[TC_Imgt_Test_R];
		break;

	case Model_OMS_Entry:
		dbStageZ = m_pstTeachInfo->dbTeachData[TC_Imgt_Test_Z_Entry];
		dbStageR = m_pstTeachInfo->dbTeachData[TC_Imgt_Test_R_Entry];
		break;

	default:
		break;
	}
	

	// 멀티축 구조체
	ST_AxisMoveParam stAxisParam[2];

	stAxisParam[0].lAxisNum = AX_Image_StageZ;
	stAxisParam[1].lAxisNum = AX_Image_StageR;

	stAxisParam[0].dbPos = dbStageZ;
	stAxisParam[1].dbPos = dbStageR;

	for (UINT nAxis = 0; nAxis < 2; nAxis++)
	{
		stAxisParam[nAxis].dbVel = m_pDevice->m_AllMotorData.pMotionParam[stAxisParam[nAxis].lAxisNum].dbVel;
		stAxisParam[nAxis].dbAcc = m_pDevice->m_AllMotorData.pMotionParam[stAxisParam[nAxis].lAxisNum].dbAcc;
		stAxisParam[nAxis].dbDec = m_pDevice->m_AllMotorData.pMotionParam[stAxisParam[nAxis].lAxisNum].dbAcc;
	}

	lReturn = m_pDevice->MotorMultiMove(POS_ABS_MODE, 2, stAxisParam);

	return lReturn;
}

//=============================================================================
// Method		: OnActionLoad
// Access		: public  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/20 - 18:40
// Desc.		:
//=============================================================================
LRESULT CTestSequence::OnActionLoad()
{
	LRESULT lReturn = RC_OK;

	if (m_pstTeachInfo == NULL)
		return RC_Motion_Err_EmptyTeach;

	switch (m_SysType)
	{
	case Sys_2D_Cal:
		lReturn = OnActionLoad_2D_CAL();
		break;

	case Sys_Stereo_Cal:
		lReturn = OnActionLoad_Stereo();
		break;

	case Sys_Focusing:
		//lReturn = OnActionLoad_Foc();
		break;

	case Sys_Image_Test:
		lReturn = OnActionLoad_IgmT();
		break;

	default:
		break;
	}
	return lReturn;
}

//=============================================================================
// Method		: OnActionLoad_2D_CAL
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in BOOL bUseShutter
// Qualifier	:
// Last Update	: 2018/3/13 - 10:48
// Desc.		:
//=============================================================================
LRESULT CTestSequence::OnActionLoad_2D_CAL(__in BOOL bUseShutter /*= TRUE*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = MotorGetStatus();

	if (lReturn != RC_OK)
		return lReturn;

	lReturn = MotorGetStatus();
	if (lReturn != RC_OK)
		return lReturn;

	// stage y
	if (RC_OK != MotorMoveAxis(AX_2D_StageY, m_pstTeachInfo->dbTeachData[TC_2D_Load_Y]))
		return RC_Motion_Err_Left_StageY;

	// stage x
	if (RC_OK != MotorMoveAxis(AX_2D_StageX, m_pstTeachInfo->dbTeachData[TC_2D_Load_X]))
		return RC_Motion_Err_Left_StageX;

	// stage z
	if (RC_OK != MotorMoveAxis(AX_2D_StageZ, m_pstTeachInfo->dbTeachData[TC_2D_Load_Z]))
		return RC_Motion_Err_Left_StageZ;

	// stage R
	if (RC_OK != MotorMoveAxis(AX_2D_StageR, m_pstTeachInfo->dbTeachData[TC_2D_Load_R]))
		return RC_Motion_Err_Left_StageR;

	// Shutter
	if (bUseShutter)
	{
		if (RC_OK != MotorMoveAxis(AX_2D_Shutter, m_pstTeachInfo->dbTeachData[TC_2D_ShutterOpen]))
			return RC_Motion_Err_Left_StageR;
	}

	return lReturn;
}

//=============================================================================
// Method		: OnActionLoad_Stereo
// Access		: public  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/20 - 18:42
// Desc.		:
//=============================================================================
LRESULT CTestSequence::OnActionLoad_Stereo()
{
	LRESULT lReturn = RC_OK;

	lReturn = MotorGetStatus();

	if (lReturn != RC_OK)
		return lReturn;

	lReturn = MotorGetStatus();

	if (lReturn != RC_OK)
		return lReturn;

	if (RC_OK != MotorMoveAxis(AX_Stereo_StageY, m_pstTeachInfo->dbTeachData[TC_Stereo_Load_Y]))
		return RC_Motion_Err_Left_StageY;

	if (RC_OK != MotorMoveAxis(AX_Stereo_Shutter, m_pstTeachInfo->dbTeachData[TC_Stereo_ShutterOpen]))
		return RC_Motion_Err_Left_StageR;

	return lReturn;
}

//=============================================================================
// Method		: OnActionLoad_Igmt
// Access		: public  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/21 - 19:35
// Desc.		:
//=============================================================================
LRESULT CTestSequence::OnActionLoad_IgmT(__in BOOL bUseShutter /*= TRUE*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = MotorGetStatus();

	if (lReturn != RC_OK)
		return lReturn;

	lReturn = MotorGetStatus();

	if (lReturn != RC_OK)
		return lReturn;

	if (RC_OK != MotorMoveAxis(AX_Image_StageX, m_pstTeachInfo->dbTeachData[TC_Imgt_Load_X]))
		return RC_Motion_Err_Left_StageY;

	if (RC_OK != MotorMoveAxis(AX_Image_StageY, m_pstTeachInfo->dbTeachData[TC_Imgt_Load_Y]))
		return RC_Motion_Err_Left_StageY;

	// Shutter
	if (bUseShutter)
	{
		if (RC_OK != MotorMoveAxis(AX_Image_Shutter, 0))
		{
			return RC_Motion_Err_Left_StageR;
		}
	}

	// 멀티축 구조체
	ST_AxisMoveParam stAxisParam[2];

	stAxisParam[0].lAxisNum = AX_Image_StageZ;
	stAxisParam[1].lAxisNum = AX_Image_StageR;

	stAxisParam[0].dbPos = m_pstTeachInfo->dbTeachData[TC_Imgt_Load_Z];

	switch (m_ModelType)
	{
	case Model_OMS_Entry:
		stAxisParam[1].dbPos = m_pstTeachInfo->dbTeachData[TC_Imgt_Load_R_Entry];
		break;
	case Model_OMS_Front:
	case Model_OMS_Front_Set:
	case Model_MRA2:
	case Model_IKC:
		stAxisParam[1].dbPos = m_pstTeachInfo->dbTeachData[TC_Imgt_Load_R];
		break;
		break;
	default:
		break;
	}

	for (UINT nAxis = 0; nAxis < 2; nAxis++)
	{
		stAxisParam[nAxis].dbVel = m_pDevice->m_AllMotorData.pMotionParam[stAxisParam[nAxis].lAxisNum].dbVel;
		stAxisParam[nAxis].dbAcc = m_pDevice->m_AllMotorData.pMotionParam[stAxisParam[nAxis].lAxisNum].dbAcc;
		stAxisParam[nAxis].dbDec = m_pDevice->m_AllMotorData.pMotionParam[stAxisParam[nAxis].lAxisNum].dbAcc;
	}

	lReturn = m_pDevice->MotorMultiMove(POS_ABS_MODE, 2, stAxisParam);

	return lReturn;
}

//=============================================================================
// Method		: OnActionInit_Foc
// Access		: public  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/3/16 - 13:48
// Desc.		:
//=============================================================================
LRESULT CTestSequence::OnActionInit_Foc()
{
	LRESULT lReturn = RC_OK;

	if (RC_OK != MotorMoveAxis(AX_Focus_StageR, m_pstTeachInfo->dbTeachData[TC_Focus_MRA_CenterR]))
		return RC_Motion_Err_Left_StageR;

	// 멀티축 구조체
	ST_AxisMoveParam stAxisParam[2];

	switch (m_ModelType)
	{
	case Model_MRA2:
		stAxisParam[AX_Focus_StageX].dbPos = m_pstTeachInfo->dbTeachData[TC_Focus_MRA_CenterX];
		stAxisParam[AX_Focus_StageY].dbPos = m_pstTeachInfo->dbTeachData[TC_Focus_MRA_CenterY];
		break;
	case Model_IKC:
		stAxisParam[AX_Focus_StageX].dbPos = m_pstTeachInfo->dbTeachData[TC_Focus_IKC_CenterX];
		stAxisParam[AX_Focus_StageY].dbPos = m_pstTeachInfo->dbTeachData[TC_Focus_IKC_CenterY];
		break;
	default:
		break;
	}

	for (UINT nAxis = 0; nAxis < 2; nAxis++)
	{
		stAxisParam[nAxis].lAxisNum = nAxis;
		stAxisParam[nAxis].dbVel = m_pDevice->m_AllMotorData.pMotionParam[stAxisParam[nAxis].lAxisNum].dbVel;
		stAxisParam[nAxis].dbAcc = m_pDevice->m_AllMotorData.pMotionParam[stAxisParam[nAxis].lAxisNum].dbAcc;
		stAxisParam[nAxis].dbDec = m_pDevice->m_AllMotorData.pMotionParam[stAxisParam[nAxis].lAxisNum].dbAcc;
	}

	if (TRUE != m_pDevice->MotorMultiMove(POS_ABS_MODE, 2, stAxisParam))
	{
		m_Log.LogMsg_Err(_T("Stage Init Err"));
		lReturn = RC_Motor_Err_MultiMove;
	}

	return lReturn;
}

//=============================================================================
// Method		: OnActionLoad_Foc
// Access		: public  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/3/5 - 11:10
// Desc.		:
//=============================================================================
LRESULT CTestSequence::OnActionLoad_Foc()
{
	LRESULT lReturn = RC_OK;

	// 모터 센터로 이동
	lReturn = OnActionInit_Foc();

	if (lReturn != RC_OK)
		return lReturn;

	// Stage Down
	lReturn = OnActionTesting_Foc_Stage(DN);

	if (lReturn != RC_OK)
		return lReturn;

	Sleep(500);

	// Gripper On
	lReturn = OnActionTesting_Foc_Gripper(ON);

	if (lReturn != RC_OK)
		return lReturn;

	return lReturn;
}

//=============================================================================
// Method		: OnActionUnLoad_Foc
// Access		: public  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/3/5 - 11:12
// Desc.		:
//=============================================================================
LRESULT CTestSequence::OnActionUnLoad_Foc()
{
	LRESULT lReturn = RC_OK;
	
	// Driver Up
	lReturn = OnActionTesting_Foc_Driver(UP);

	if (lReturn != RC_OK)
		return lReturn;

	// Gripper Off
	lReturn = OnActionTesting_Foc_Gripper(OFF);

	if (lReturn != RC_OK)
		return lReturn;

	Sleep(500);

	// Stage Up
	lReturn = OnActionTesting_Foc_Stage(UP);

	if (lReturn != RC_OK)
		return lReturn;

	Sleep(500);

	lReturn = OnActionTesting_Foc_Collet(DN);

	if (lReturn != RC_OK)
		return lReturn;

	// Fix Off
// 	lReturn = OnActionTesting_Foc_Fix(OFF);
// 
// 	if (lReturn != RC_OK)
// 		return lReturn;
	
	// 모터 센터로 이동
	lReturn = OnActionInit_Foc();

	return lReturn;
}

//=============================================================================
// Method		: OnActionLoad_Master_Foc
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in UINT nParaID
// Qualifier	:
// Last Update	: 2018/3/16 - 16:33
// Desc.		:
//=============================================================================
LRESULT CTestSequence::OnActionLoad_Master_Foc(__in UINT nParaID /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	UINT nPort = 0;

	switch (m_ModelType)
	{
	case Model_OMS_Entry:
	case Model_OMS_Front:
	case Model_OMS_Front_Set:
	case Model_IKC:
		lReturn = IsReadyOk(DI_Fo_16_FixOnSensorL);

		if (RC_OK != lReturn)
			return lReturn; 
		
		lReturn = IsReadyOk(DI_Fo_17_FixOnSensorR);
		break;
	case Model_MRA2:

		if (Para_Left == nParaID)
			nPort = DI_Fo_17_FixOnSensorR;
		else
			nPort = DI_Fo_16_FixOnSensorL;

		lReturn = IsReadyOk(nPort);

		break;
	default:
		break;
	}

	if (lReturn != RC_OK)
		return lReturn;

	// Stage Down
	lReturn = OnActionTesting_Foc_Stage(DN);

	if (lReturn != RC_OK)
		return lReturn;

	return lReturn;
}

//=============================================================================
// Method		: OnActionLampControl
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in long lPort
// Parameter	: __in enum_IO_SignalType enType
// Qualifier	:
// Last Update	: 2018/3/9 - 12:10
// Desc.		:
//=============================================================================
LRESULT CTestSequence::OnActionLampControl(__in long lPort, __in enum_IO_SignalType enType)
{
	LRESULT lReturn = RC_OK;

	// 보드 연결 상태 확인
	lReturn = m_pDIO->AXTState();

	if (RC_OK != lReturn)
		return lReturn;

	m_pDIO->Set_DO_Status(lPort, enType);

	return lReturn;
}

//=============================================================================
// Method		: OnActionTesting_OC_Adj
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in int iOffsetX
// Parameter	: __in int iOffsetY
// Parameter	: __in double dbPixelSensor
// Qualifier	:
// Last Update	: 2018/3/5 - 17:35
// Desc.		:
//=============================================================================
LRESULT CTestSequence::OnActionTesting_OC_Adj(__in int iOffsetX, __in int iOffsetY, __in double dbPixelSensor)
{
	LRESULT lReturn = RC_OK;

	double dbOffsetX = double(iOffsetX) * dbPixelSensor * 10;
	double dbOffsetY = double(iOffsetY) * dbPixelSensor * 10 * (-1);

	// 멀티축 구조체
	ST_AxisMoveParam stAxisParam[AX_Focus_MAX];

	stAxisParam[AX_Focus_StageX].lAxisNum = AX_Focus_StageX;
	stAxisParam[AX_Focus_StageX].dbPos = dbOffsetX;
	stAxisParam[AX_Focus_StageX].dbVel = m_pDevice->m_AllMotorData.pMotionParam[stAxisParam[AX_Focus_StageX].lAxisNum].dbVel;
	stAxisParam[AX_Focus_StageX].dbAcc = m_pDevice->m_AllMotorData.pMotionParam[stAxisParam[AX_Focus_StageX].lAxisNum].dbAcc;
	stAxisParam[AX_Focus_StageX].dbDec = m_pDevice->m_AllMotorData.pMotionParam[stAxisParam[AX_Focus_StageX].lAxisNum].dbAcc;

	stAxisParam[AX_Focus_StageY].lAxisNum = AX_Focus_StageY;
	stAxisParam[AX_Focus_StageY].dbPos = dbOffsetY;
	stAxisParam[AX_Focus_StageY].dbVel = m_pDevice->m_AllMotorData.pMotionParam[stAxisParam[AX_Focus_StageY].lAxisNum].dbVel;
	stAxisParam[AX_Focus_StageY].dbAcc = m_pDevice->m_AllMotorData.pMotionParam[stAxisParam[AX_Focus_StageY].lAxisNum].dbAcc;
	stAxisParam[AX_Focus_StageY].dbDec = m_pDevice->m_AllMotorData.pMotionParam[stAxisParam[AX_Focus_StageY].lAxisNum].dbAcc;

	lReturn = m_pDevice->MotorMultiMove(POS_REL_MODE, 2, stAxisParam);

	return lReturn;
}

//=============================================================================
// Method		: OnActionTesting_Rotate_Adj
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in double dbDegree
// Parameter	: __in double dbPixelSensor
// Qualifier	:
// Last Update	: 2018/3/5 - 17:38
// Desc.		:
//=============================================================================
LRESULT CTestSequence::OnActionTesting_Rotate_Adj(__in double dbDegree, __in double dbPixelSensor)
{
	LRESULT lReturn = RC_OK;

	double dbOffsetR = dbDegree * dbPixelSensor * 300;

	lReturn = m_pDevice->MotorAxisMove(POS_REL_MODE, AX_Focus_StageR, dbOffsetR);

	return lReturn;
}
