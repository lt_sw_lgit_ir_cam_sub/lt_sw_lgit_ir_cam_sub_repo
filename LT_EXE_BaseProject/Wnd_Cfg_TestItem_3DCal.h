//*****************************************************************************
// Filename	: 	Wnd_Cfg_TestItem_3DCal.h
// Created	:	2017/9/24 - 16:11
// Modified	:	2017/9/24 - 16:11
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Cfg_TestItem_3DCal_h__
#define Wnd_Cfg_TestItem_3DCal_h__

#pragma once


#include "Wnd_BaseView.h"
#include "VGStatic.h"
#include "Def_TestItem_Cm.h"
#include "Def_DataStruct.h"

#include "Wnd_Cfg_2DCal.h"

#include "Wnd_Cfg_3DCal.h"
#include "Wnd_Cfg_3DCal_Evalu.h"
#include "Wnd_Cfg_3DCal_Spec.h"
#include "Wnd_Cfg_3DCal_SpecEval.h"


//-----------------------------------------------------------------------------
// CWnd_Cfg_TestItem_3DCal
//-----------------------------------------------------------------------------
class CWnd_Cfg_TestItem_3DCal : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_Cfg_TestItem_3DCal)

public:
	CWnd_Cfg_TestItem_3DCal();
	virtual ~CWnd_Cfg_TestItem_3DCal();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow		(BOOL bShow, UINT nStatus);
	afx_msg void	OnNMClickTestItem	(NMHDR *pNMHDR, LRESULT *pResult);

	// 검사기 설정
	enInsptrSysType		m_InspectionType = enInsptrSysType::Sys_Focusing;

	CMFCTabCtrl				m_tc_Option;
	
	CWnd_Cfg_2DCal			m_wnd_Cfg2DCal;

	CWnd_Cfg_3D_Depth		m_wnd_Cfg3D_Depth;
	CWnd_Cfg_3D_Eval		m_wnd_Cfg3D_Eval;
	CWnd_Cfg_3DCal_Spec		m_wnd_Cfg3DCal_Spec;
	CWnd_Cfg_3DCal_SpecEval	m_wnd_Cfg3DCal_SpecEval;
	

	void		SetSysAddTabCreate		();
	void		SetInitListCtrl			();

public:
	// 검사기 종류 설정
	void		SetSystemType			(__in enInsptrSysType nSysType);

	// 저장된 Test Item Info 데이터 불러오기
	void		Set_RecipeInfo			(__in  ST_RecipeInfo* pstRecipeInfo);
	void		Get_RecipeInfo			(__out ST_RecipeInfo& stOutRecipInfo);

	//void		Set_TestItemInfo		(__in const ST_TestItemInfo* pstTestItemInfo, __in const ST_TestItemOpt* pstInTestItemOpt);
	//void		Get_TestItemInfo		(__out ST_TestItemInfo& stOutTestItemInfo, __out ST_TestItemOpt& stOutTestItemOpt);

};
#endif // Wnd_Cfg_TestItem_3DCal_h__


