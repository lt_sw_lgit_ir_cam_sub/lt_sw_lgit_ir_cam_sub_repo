﻿#ifndef Wnd_TestResult_Foc_MainView_h__
#define Wnd_TestResult_Foc_MainView_h__

#pragma once

#include "VGStatic.h"
#include "Def_DataStruct_Cm.h"
#include "Def_Enum.h"

#include "Wnd_CurrentData.h"
#include "Wnd_DefectPixelData.h"
#include "Wnd_OpticalData.h"
#include "Wnd_RotateData.h"
#include "Wnd_SFRData.h"
#include "Wnd_ParticleData.h"
#include "Wnd_TorqueData.h"
#include "Wnd_ActiveAlignData.h"

// CWnd_TestResult_Foc_MainView
enum enTestResult_Foc_Main_Static
{
	STI_TR_Foc_Main_Title,
	STI_TR_Foc_Main_MAX,
};

static LPCTSTR	g_szTestResult_Foc_Main_Static[] =
{
	_T("TEST RESULT"),
	NULL
};

class CWnd_TestResult_Foc_MainView : public CWnd
{
	DECLARE_DYNAMIC(CWnd_TestResult_Foc_MainView)

public:
	CWnd_TestResult_Foc_MainView();
	virtual ~CWnd_TestResult_Foc_MainView();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int	 OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize				(UINT nType, int cx, int cy);
	afx_msg void OnShowWindow		(BOOL bShow, UINT nStatus);
	afx_msg void OnRangeBtnCtrl		(UINT nID);
	afx_msg void OnLbnSelChangeTest	();
	virtual BOOL PreCreateWindow	(CREATESTRUCT& cs);

	enInsptrSysType			m_InspectionType;

	CFont					m_font;
	
	CVGStatic				m_st_Item[STI_TR_Foc_Main_MAX];

	CComboBox				m_cb_TestItem;

	int						m_iSelectTest[TI_Foc_MaxEnum];

	CWnd_CurrentData		m_Wnd_CurrentData;
	CWnd_DefectPixelData	m_Wnd_DefectPixelData;
	CWnd_OpticalData		m_Wnd_OpticalData;
	CWnd_RotateData			m_Wnd_RotateData;
	CWnd_SFRData			m_Wnd_SFRData;
	CWnd_ParticleData		m_Wnd_ParticleData;
	CWnd_TorqueData			m_Wnd_TorqueData;
	CWnd_ActiveAlignData	m_Wnd_ActiveAlignData;

	void	SetShowWindowResult (int iItem);
	void	MoveWindow_Result	(int x, int y, int nWidth, int nHeight);

public:

	void SelectItem				(UINT nTestItem);

	// 검사기 종류 설정
	void	SetSystemType	(__in enInsptrSysType nSysType)
	{
		m_InspectionType = nSysType;
	};

	void SetTestItem		(__in int iTestItem);
	void SetUpdateResult	(__in const ST_RecipeInfo_Base* pstRecipeInfo, __in int iTestItem);
	void SetUpdateClear		();

	void AllDataReset();
	void SetUIData_Reset(int nIdx);
	void SetUIData			(__in int nIdx, __in LPVOID pParam, __in enFocus_AAView enView);
	void SetClearTab		();
	void SetAddTab			(int nIdx, int &iItemCnt);
};
#endif // Wnd_TestResult_Foc_MainView_h__
