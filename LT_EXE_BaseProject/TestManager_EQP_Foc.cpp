﻿//*****************************************************************************
// Filename	: 	TestManager_EQP_Foc.cpp
// Created	:	2016/5/9 - 13:32
// Modified	:	2016/08/10
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "TestManager_EQP_Foc.h"
#include "CommonFunction.h"
#include "File_Recipe.h"
#include "Def_Digital_IO.h"
#include "CRC16.h"
#include "math.h"

#include <Mmsystem.h>
#pragma comment (lib,"winmm.lib")

CTestManager_EQP_Foc::CTestManager_EQP_Foc()
{
	OnInitialize();

	for (int iCnt = 0; iCnt < TI_Foc_MaxEnum; iCnt++)
	{
		m_bFlag_LoopItem[iCnt] = FALSE;
	}
}

CTestManager_EQP_Foc::~CTestManager_EQP_Foc()
{
	TRACE(_T("<<< Start ~CTestManager_EQP_Foc >>> \n"));

	this->OnFinalize();

	TRACE(_T("<<< End ~CTestManager_EQP_Foc >>> \n"));	
}


//=============================================================================
// Method		: OnLoadOption
// Access		: virtual protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/9/28 - 20:04
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP_Foc::OnLoadOption()
{
	BOOL bReturn = CTestManager_EQP::OnLoadOption();

	return bReturn;
}

//=============================================================================
// Method		: InitDevicez
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in HWND hWndOwner
// Qualifier	:
// Last Update	: 2017/11/11 - 21:37
// Desc.		:
//=============================================================================
void CTestManager_EQP_Foc::InitDevicez(__in HWND hWndOwner /*= NULL*/)
{
	CTestManager_EQP::InitDevicez(hWndOwner);

}

//=============================================================================
// Method		: StartOperation_LoadUnload
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:19
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::StartOperation_LoadUnload(__in BOOL bLoad)
{
	LRESULT lResult = RC_OK;
	lResult = CTestManager_EQP::StartOperation_LoadUnload(bLoad);

	return lResult;
}

//=============================================================================
// Method		: StartOperation_Inspection
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:21
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::StartOperation_Inspection(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lResult = RC_OK;
	lResult = CTestManager_EQP::StartOperation_Inspection(nParaIdx);

	return lResult;
}

//=============================================================================
// Method		: StartOperation_Manual
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bLoad
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:22
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::StartOperation_Manual(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lResult = RC_OK;
	
	//lResult = CTestManager_EQP::StartOperation_Manual(nStepIdx, nParaIdx);

	if (IsTesting_Manual())
	{
		AfxMessageBox(_T("Inspection is in Manual progress.\r\nTry it after the Test is finished."), MB_SYSTEMMODAL);
		TRACE(_T("개별 TEST 진행중.\n"));
		OnLog_Err(_T("Manual Test is Already."));
		return RC_AlreadyTesting;
	}

	if (NULL != m_hThrTest_Manual)
	{
		CloseHandle(m_hThrTest_Manual);
		m_hThrTest_Manual = NULL;
	}

	stThreadParam* pParam	= new stThreadParam;
	pParam->pOwner			= this;
	pParam->nIndex			= nParaIdx;
	pParam->nStepIndex		= nStepIdx;
	pParam->nArg_2			= 0;

	m_hThrTest_Manual = HANDLE(_beginthreadex(NULL, 0, ThreadManualTest, pParam, 0, NULL));

	return lResult;
}

//=============================================================================
// Method		: StartOperation_InspManual
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/20 - 11:15
// Desc.		: 이미지 검사 개별 테스트용
//=============================================================================
LRESULT CTestManager_EQP_Foc::StartOperation_InspManual(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lResult = RC_OK;
	lResult = CTestManager_EQP::StartOperation_InspManual(nParaIdx);



	return lResult;
}

//=============================================================================
// Method		: StopProcess_LoadUnload
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/19 - 19:35
// Desc.		:
//=============================================================================
void CTestManager_EQP_Foc::StopProcess_LoadUnload()
{
	CTestManager_EQP::StopProcess_LoadUnload();

}

//=============================================================================
// Method		: StopProcess_Test_All
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/10 - 21:14
// Desc.		:
//=============================================================================
void CTestManager_EQP_Foc::StopProcess_Test_All()
{
// 	CTestManager_EQP::StopProcess_Test_All();

	// While 해제
	for (UINT nItem = 0; nItem < TI_Foc_MaxEnum; nItem++)
	{
		SetLoopItem_Info(nItem, TestLoop_NotUse);
	}

	m_bFlag_UserStop = TRUE;

#ifndef MOTION_NOT_USE
	m_MotionSequence.OnActionUnLoad_Foc();
#endif
	_TI_Cm_Finalize_Error(Para_Left);

	// * 최종 판정 CHECK 처리
	OnSet_TestResult_Unit(TR_Stop, Para_Left);
	OnSet_TestResult_Unit(TR_Stop, Para_Right);
}

//=============================================================================
// Method		: StopProcess_InspManual
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/20 - 11:15
// Desc.		:
//=============================================================================
void CTestManager_EQP_Foc::StopProcess_InspManual(__in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::StopProcess_InspManual(nParaIdx);
}

//=============================================================================
// Method		: AutomaticProcess_LoadUnload
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:23
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP_Foc::AutomaticProcess_LoadUnload(__in BOOL bLoad)
{
	BOOL bResult = TRUE;

	bResult = CTestManager_EQP::AutomaticProcess_LoadUnload(bLoad);

	return bResult;
}

//=============================================================================
// Method		: AutomaticProcess_Test_All
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:26
// Desc.		:
//=============================================================================
void CTestManager_EQP_Foc::AutomaticProcess_Test_All(__in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::AutomaticProcess_Test_All(nParaIdx);
}

//=============================================================================
// Method		: AutomaticProcess_Test_InspManual
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/15 - 3:06
// Desc.		:
//=============================================================================
void CTestManager_EQP_Foc::AutomaticProcess_Test_InspManual(__in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::AutomaticProcess_Test_InspManual(nParaIdx);
}

//=============================================================================
// Method		: OnInitial_LoadUnload
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:28
// Desc.		:
//=============================================================================
void CTestManager_EQP_Foc::OnInitial_LoadUnload(__in BOOL bLoad)
{
	//CTestManager_EQP::OnInitial_LoadUnload(bLoad);

	if (bLoad)	// Loading
	{
		// * Reset Alarm
		//OnResetAlarm();

		// * 데이터 초기화
		OnResetInfo_Loading();

		// * 투입 시간 설정
		OnSet_InputTime__Unit();

		// * Status : Loading
		OnSet_TestResult_Unit(TR_Ready);
		OnSet_TestProgress_Unit(TP_Loading);

		// * 경광등 검사 중 표시
		OnDOut_TowerLamp(enLampColor::Lamp_Yellow, Bit_Clear);
		OnDOut_TowerLamp(enLampColor::Lamp_Green, Bit_Set);

		OnDOut_StartLamp(FALSE);
		OnDOut_StopLamp(TRUE);

		OnSet_GraphReset();
	}
	else	// Unloading
	{
		OnSet_TestProgress(TP_Unloading);
	}
}

//=============================================================================
// Method		: OnFinally_LoadUnload
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:29
// Desc.		:
//=============================================================================
void CTestManager_EQP_Foc::OnFinally_LoadUnload(__in BOOL bLoad)
{
	CTestManager_EQP::OnFinally_LoadUnload(bLoad);

	if (FALSE == bLoad)
	{
		OnDOut_StartLamp(TRUE);
		OnDOut_StopLamp(FALSE);
	}
}

//=============================================================================
// Method		: OnStart_LoadUnload
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:30
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnStart_LoadUnload(__in BOOL bLoad)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnStart_LoadUnload(bLoad);

	return lReturn;
}

//=============================================================================
// Method		: OnInitial_Test_All
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 19:36
// Desc.		:
//=============================================================================
void CTestManager_EQP_Foc::OnInitial_Test_All(__in UINT nParaIdx /*= 0*/)
{
	//CTestManager_EQP::OnInitial_Test_All(nParaIdx);

	// 테스트 상태 : Run	
	m_stInspInfo.nTestPara  = nParaIdx;
	m_bFlag_CameraChange	= FALSE;

	OnSet_TestResult_Unit(TR_Testing, nParaIdx);
	OnSet_TestProgress_Unit(TP_Testing, nParaIdx);

	// 검사 시작 시간 설정
	OnSet_BeginTestTime(nParaIdx);

	// 채널 카메라 데이터 초기화
	m_stInspInfo.WorklistInfo.Reset();

}

//=============================================================================
// Method		: OnFinally_Test_All
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in LRESULT lResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/23 - 13:05
// Desc.		:
//=============================================================================
void CTestManager_EQP_Foc::OnFinally_Test_All(__in LRESULT lResult, __in UINT nParaIdx /*= 0*/)
{
//	CTestManager_EQP::OnFinally_Test_All(lResult, nParaIdx);

	OnSet_EndTestTime(nParaIdx);

	OnJugdement_And_Report(nParaIdx);

	m_stInspInfo.nTestPara = Para_Left;
}

//=============================================================================
// Method		: OnStart_Test_All
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:30
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnStart_Test_All(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	m_bFlag_UserStop = FALSE;

	// * 안전 기능 체크
	if (TR_Pass != m_stInspInfo.CamInfo[nParaIdx].nJudgeLoading)
	{
		if (RC_OK == m_stInspInfo.CamInfo[nParaIdx].ResultCode)
			lReturn = RC_LoadingFailed;
		else
			lReturn = m_stInspInfo.CamInfo[nParaIdx].ResultCode;

		// * 임시 : 안전하게 배출하기 위하여
		// KHO  이거 지워도 되는 건지? (A:지우면 언로드가 안될때 있음 )
		Sleep(3000);

		return lReturn;
	}
	
	// * 검사 시작 위치로 제품 이동
	if (g_InspectorTable[m_InspectionType].UseMotion)
	{
		lReturn = OnMotion_MoveToTestPos(nParaIdx);
		if (RC_OK != lReturn)
		{
			// 에러 상황

			// 알람
			OnAddAlarm_F(_T("Motion : Move to Test Position [Para -> %s]"), g_szParaName[nParaIdx]);
			//return lReturn;
		}
	}

	// * Step별로 검사 진행
	enTestResult nResult = TR_Pass;

	for (UINT nCamPara = 0; nCamPara < g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt; nCamPara++)
	{
		m_stInspInfo.nTestPara = nCamPara;

		OnSetCamerParaSelect(nCamPara);
		m_Test_ResultDataView.TestResultView_AllReset_Foc(nCamPara);

		if (m_InspectionType == Sys_Focusing && nCamPara == Para_Right)
		{
			OnSet_InputTime__Unit(nCamPara);
			OnInitial_Test_All(nCamPara);
			OnChangeTheCamera(TRUE, nCamPara); //화면 전환 및 카운트 시작

			m_MotionSequence.OnActionLampControl(DO_Fo_08_StartLempL, IO_SignalT_ToggleStart);
			m_MotionSequence.OnActionLampControl(DO_Fo_09_StartLempR, IO_SignalT_ToggleStart);

			while (TRUE)
			{
				if (TRUE == m_bFlag_UserStop)
				{
					break;
				}

				// 변경 완료 변수
				if (TRUE == m_bFlag_CameraChange)
				{
					OnChangeTheCameraStop();
					break;
				}

				//카운트가 끝난 경우
				if (FALSE == OnFoc_ChangeCamFlag() && FALSE == m_bFlag_CameraChange)
				{
					// 강제 종료 루틴 수행,,
					OnChangeTheCameraStop();
					break;
				}

				Sleep(100);
			}

			m_MotionSequence.OnActionLampControl(DO_Fo_08_StartLempL, IO_SignalT_ToggleStop);
			m_MotionSequence.OnActionLampControl(DO_Fo_09_StartLempR, IO_SignalT_ToggleStop);

			OnChangeTheCamera(FALSE, nCamPara); //화면 전환 및 카운트 시작
		}

		if (FALSE == m_bFlag_CameraChange && Para_Right == nCamPara)
		{
			m_stInspInfo.CamInfo[nCamPara].nJudgment = TR_Check;
			break;
		}

		if (TRUE == m_bFlag_UserStop)
		{
			m_stInspInfo.CamInfo[nCamPara].nJudgment = TR_Stop;
			break;
		}

		for (UINT nTryCnt = 0; nTryCnt <= m_stInspInfo.RecipeInfo.nTestRetryCount; nTryCnt++)
		{
			m_stInspInfo.CamInfo[nCamPara].stFocus.Reset();

			// 검사기에 맞추어 검사 시작
			lReturn = StartTest_Equipment(nCamPara);

			// * 검사 결과 판정
			nResult = Judgment_Inspection(nCamPara);

			if (TRUE == m_bFlag_UserStop)
			{
				nResult = TR_Stop;
				break;
			}

			if ((TR_Pass == nResult))
			{
				break;
			}
		}

		if (TRUE == m_bFlag_UserStop)
		{
			nResult = TR_Stop;
			break;
		}
	
		OnSet_TestResult_Unit(nResult, nCamPara);
		
		// * 검사 결과 판정 정보 세팅 및 UI표시
		if ((m_stInspInfo.RecipeInfo.ModelType == Model_MRA2) && (nCamPara == Para_Left))
		{
			OnSet_TestProgress_Unit(TP_Ready, nCamPara);
		}
		else
		{
			OnUpdate_TestReport(nCamPara);
		}

		if (TR_Check == lReturn)
		{
			m_stInspInfo.CamInfo[nCamPara].nJudgment = TR_Check;
			break;
		}

		if (TR_Pass != nResult)
		{
			m_stInspInfo.CamInfo[nCamPara].nJudgment = TR_Stop;
			break;
		}
	}

	Judgment_Inspection_All();

	// * MES 검사 결과 보고
	if (MES_Online == m_stInspInfo.MESOnlineMode && OpMode_Master != m_stInspInfo.OperateMode)
	{
		lReturn = MES_SendInspectionData(nParaIdx);
		if (RC_OK != lReturn)
		{
			// 에러 상황
		}
	}

	// * Unload Product
	lReturn = StartOperation_LoadUnload(Product_Unload);
	if (RC_OK != lReturn)
	{
		// 에러 상황
	}

	return lReturn;
}

//=============================================================================
// Method		: OnInitial_Test_InspManual
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/8 - 9:11
// Desc.		:
//=============================================================================
void CTestManager_EQP_Foc::OnInitial_Test_InspManual(__in UINT nTestItemID, __in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::OnInitial_Test_InspManual(nTestItemID, nParaIdx);
}

//=============================================================================
// Method		: OnFinally_Test_InspManual
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nTestItemID
// Parameter	: __in LRESULT lResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/8 - 9:12
// Desc.		:
//=============================================================================
void CTestManager_EQP_Foc::OnFinally_Test_InspManual(__in UINT nTestItemID, __in LRESULT lResult, __in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::OnFinally_Test_InspManual(nTestItemID, lResult, nParaIdx);
}

//=============================================================================
// Method		: OnStart_Test_InspManual
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/15 - 3:05
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnStart_Test_InspManual(__in UINT nTestItemID, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnStart_Test_InspManual(nTestItemID, nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: Load_Product
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::Load_Product()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::Load_Product();

	return lReturn;
}

//=============================================================================
// Method		: Unload_Product
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/23 - 14:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::Unload_Product()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::Unload_Product();

	return lReturn;
}

//=============================================================================
// Method		: StartTest_Equipment
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:21
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::StartTest_Equipment(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	switch (m_stInspInfo.OperateMode)
	{
	case OpMode_Production:
		lReturn = StartTest_Focusing(nParaIdx);
		break;
	case OpMode_Master:
		lReturn = StartMaster_Focusing(nParaIdx);
		break;
	case OpMode_StartUp_Check:
		break;
	case OpMode_DryRun:
		break;
	default:
		break;
	}

	return lReturn;
}

//=============================================================================
// Method		: StartTest_Focusing
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/9 - 16:18
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::StartTest_Focusing(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	// * 검사 결과 처리 --------------------------------------------------------

	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;
	INT_PTR iStepCnt = m_stInspInfo.RecipeInfo.StepInfo.GetCount();
	DWORD dwElapTime = 0;

	BOOL bError = FALSE;

	// * 설정된 스텝 진행
	for (INT nStepIdx = 0; nStepIdx < iStepCnt; nStepIdx++)
	{
		// 스텝 진행 시간 체크
		dwElapTime = Check_TimeStepStart();

		// UI 스텝 선택
		OnSet_TestStepSelect(nStepIdx);

		// * 검사 시작
		if (TRUE == pStepInfo->StepList[nStepIdx].bTest)
		{
			lReturn = StartTest_Focusing_TestItem(pStepInfo->StepList[nStepIdx].nTestItem, nStepIdx, nParaIdx);
		}
		else
		{
			if (RC_OK == lReturn)
			{
				m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
			}
			else
			{
				m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
			}
		}


		// 스텝 딜레이
		if (0 < pStepInfo->StepList[nStepIdx].dwDelay)
		{
			Sleep(pStepInfo->StepList[nStepIdx].dwDelay);
		}

		// 스텝 진행 시간 체크
		dwElapTime = Check_TimeStepStop(dwElapTime);

		// 진행 시간 설정
		m_stInspInfo.Set_ElapTime(nParaIdx, nStepIdx, dwElapTime);

		// 진행 결과 설정 및 UI에 표시
		OnSet_TestStepResult(nStepIdx, nParaIdx);

		// * 검사 프로그레스		
		OnSet_TestProgressStep((UINT)iStepCnt, nStepIdx + 1);

		// UI 갱신 딜레이
		Sleep(50);


		if (TRUE == m_bFlag_UserStop)
		{
			_TI_Cm_Finalize((UINT)(iStepCnt - 1), Para_Left);
			break;
		}

		// * 검사 중단 여부 판단
		if (RC_OK != lReturn)
		{
			// 에러 상황
			switch (pStepInfo->StepList[nStepIdx].nTestItem)
			{
			case TI_Foc_Motion_LockingCheck:
			case TI_Foc_Motion_LockingScrew:
			case TI_Foc_Motion_ReleaseScrew:
			case TI_Foc_Motion_StageLoad:
			case TI_Foc_Initialize_Test:
			case TI_Foc_Finalize_Test:
			case TI_Foc_Fn_PreFocus:
			case TI_Foc_Fn_ActiveAlign:
				nStepIdx = (INT)iStepCnt;
				bError	 = TRUE;
				break;
			default:
				break;
			}
		}
	}

	// 검사 중단시 Finalize 수행
	if (bError)
	{
		_TI_Cm_Finalize_Error(Para_Left);

		m_MotionSequence.OnActionUnLoad_Foc();

		lReturn = TR_Check;

		// * 최종 판정 CHECK 처리
		OnSet_TestResult_Unit(TR_Check, nParaIdx);
	}

	return lReturn;
}

//=============================================================================
// Method		: StartMaster_Focusing
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/8/21 - 10:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::StartMaster_Focusing(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	// * 검사 결과 처리 --------------------------------------------------------

	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.MasterInfo;
	INT_PTR iStepCnt = m_stInspInfo.RecipeInfo.MasterInfo.GetCount();
	DWORD dwElapTime = 0;

	BOOL bError = FALSE;

	// * 설정된 스텝 진행
	for (INT nStepIdx = 0; nStepIdx < iStepCnt; nStepIdx++)
	{
		// 스텝 진행 시간 체크
		dwElapTime = Check_TimeStepStart();

		// UI 스텝 선택
		OnSet_TestStepSelect(nStepIdx);

		// * 검사 시작
		if (TRUE == pStepInfo->StepList[nStepIdx].bTest)
		{
			lReturn = StartTest_Focusing_TestItem(pStepInfo->StepList[nStepIdx].nTestItem, nStepIdx, nParaIdx);
		}
		else
		{
			if (RC_OK == lReturn)
			{
				m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
			}
			else
			{
				m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
			}
		}


		// 스텝 딜레이
		if (0 < pStepInfo->StepList[nStepIdx].dwDelay)
		{
			Sleep(pStepInfo->StepList[nStepIdx].dwDelay);
		}

		// 스텝 진행 시간 체크
		dwElapTime = Check_TimeStepStop(dwElapTime);

		// 진행 시간 설정
		m_stInspInfo.Set_ElapTime(nParaIdx, nStepIdx, dwElapTime);

		// 진행 결과 설정 및 UI에 표시
		OnSet_TestStepResult(nStepIdx, nParaIdx);

		// * 검사 프로그레스		
		OnSet_TestProgressStep((UINT)iStepCnt, nStepIdx + 1);

		// UI 갱신 딜레이
		Sleep(50);

		if (TRUE == m_bFlag_UserStop)
		{
			_TI_Cm_Finalize((UINT)(iStepCnt - 1), Para_Left);
			break;
		}

		// * 검사 중단 여부 판단
		if (RC_OK != lReturn)
		{
			// 에러 상황
			switch (pStepInfo->StepList[nStepIdx].nTestItem)
			{
			case TI_Foc_Motion_LockingCheck:
			case TI_Foc_Motion_LockingScrew:
			case TI_Foc_Motion_ReleaseScrew:
			case TI_Foc_Initialize_Test:
			case TI_Foc_Finalize_Test:
				nStepIdx = (INT)iStepCnt;
				bError = TRUE;
				break;
			default:
				break;
			}
		}
			}

	// 검사 중단시 Finalize 수행
	if (bError)
	{
		_TI_Cm_Finalize_Error(Para_Left);

		m_MotionSequence.OnActionUnLoad_Foc();

		lReturn = TR_Check;

		// * 최종 판정 CHECK 처리
		OnSet_TestResult_Unit(TR_Check, nParaIdx);
	}

	return lReturn;
}

//=============================================================================
// Method		: AutomaticProcess_TestItem
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nCornerStep
// Qualifier	:
// Last Update	: 2017/12/12 - 13:31
// Desc.		:
//=============================================================================
void CTestManager_EQP_Foc::AutomaticProcess_TestItem(__in UINT nParaIdx, __in UINT nTestItemID, __in UINT nStepIdx, __in UINT nCornerStep /*= 0*/)
{
// 	if (TI_2D_Detect_CornerExt == nTestItemID)
// 	{
// 		LRESULT lReturn = _TI_2DCAL_CornerExt(nStepIdx, nCornerStep, nParaIdx);
// 	}
}

//=============================================================================
// Method		: StartTest_Focusing_TestItem
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/20 - 11:16
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::StartTest_Focusing_TestItem(__in UINT nTestItemID, __in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	// Overlay Draw
	OnSetOverlayInfo_Foc(nTestItemID, nParaIdx);

	// 결과 초기화
	TestResultView_Reset(nTestItemID, nParaIdx);
	
	switch (nTestItemID)
	{
	case TI_Foc_CaptureImage:
		lReturn = _TI_Cm_CaptureImage(nStepIdx, nParaIdx, TRUE);
		break;

	case TI_Foc_Motion_LockingCheck:
		lReturn = _TI_Foc_Mot_LockingCheck(nStepIdx, nParaIdx);
		break;

	case TI_Foc_Motion_StageLoad:
		lReturn = _TI_Foc_Mot_StageLoad(nStepIdx, nParaIdx);
		break;

	case TI_Foc_Motion_StageUnLoad:
		lReturn = _TI_Foc_Mot_StageUnLoad(nStepIdx, nParaIdx);
		break;

	case TI_Foc_Motion_ReleaseScrew:
		lReturn = _TI_Foc_Mot_ReleaseTorque(nStepIdx, nParaIdx);
		break;

	case TI_Foc_Motion_LockingScrew:
		lReturn = _TI_Foc_Mot_LockingTorque(nStepIdx, nParaIdx);
		break;

	case TI_Foc_Motion_ParticleIn:
		lReturn = _TI_Foc_Mot_ParticleLenzIn(nStepIdx, nParaIdx);
		break;

	case TI_Foc_Motion_ParticleOut:
		lReturn = _TI_Foc_Mot_ParticleLenzOut(nStepIdx, nParaIdx);
		break;

	case TI_Foc_Motion_ColletUp:
		lReturn = _TI_Foc_Mot_ColletUp(nStepIdx, nParaIdx);
		break;

	case TI_Foc_Initialize_Test:
		lReturn = _TI_Cm_Initialize(nStepIdx, nParaIdx);
		break;

	case TI_Foc_Finalize_Test:
		lReturn = _TI_Cm_Finalize(nStepIdx, nParaIdx);
		break;

	case TI_Foc_Fn_PreFocus:
		lReturn = _TI_Foc_Pro_PreFocus(nStepIdx, nParaIdx);
		break;

	case TI_Foc_Fn_ActiveAlign:
		lReturn = _TI_Foc_Pro_ActiveAlign(nStepIdx, nParaIdx);
		break;

	case TI_Foc_Fn_ECurrent:
		lReturn = _TI_Foc_Pro_Current(nStepIdx, nParaIdx);
		break;

	case TI_Foc_Fn_OpticalCenter:
		lReturn = _TI_Foc_Pro_OpticalCenter(nStepIdx, nParaIdx);
		break;

	case TI_Foc_Fn_SFR:
		lReturn = _TI_Foc_Pro_SFR(nStepIdx, nParaIdx);
		break;

	case TI_Foc_Fn_Rotation:
		lReturn = _TI_Foc_Pro_Rotation(nStepIdx, nParaIdx);
		break;

	case TI_Foc_Fn_Stain:
		lReturn = _TI_Foc_Pro_Particle(nStepIdx, nParaIdx);
		break;

	case TI_Foc_Fn_DefectPixel:
		lReturn = _TI_Foc_Pro_DefectPixel(nStepIdx, nParaIdx);
		break;
	

// 	case TI_Foc_Re_TorqueCheck:
// 		lReturn = _TI_Foc_Pro_Torque(nStepIdx, nParaIdx);
// 		break;

	case TI_Foc_Re_TorqueCheck:
	case TI_Foc_Re_SFR:
	case TI_Foc_Re_Group_SFR:
	case TI_Foc_Re_Tilt_SFR:
// 	case TI_Foc_Re_G0_SFR:
// 	case TI_Foc_Re_G1_SFR:
// 	case TI_Foc_Re_G2_SFR:
// 	case TI_Foc_Re_G3_SFR:
// 	case TI_Foc_Re_G4_SFR:
// 	case TI_Foc_Re_X1Tilt_SFR:
// 	case TI_Foc_Re_X2Tilt_SFR:
// 	case TI_Foc_Re_Y1Tilt_SFR:
// 	case TI_Foc_Re_Y2Tilt_SFR:
	case TI_Foc_Re_ECurrent:
	case TI_Foc_Re_OpticalCenterX:
	case TI_Foc_Re_OpticalCenterY:
	case TI_Foc_Re_Rotation:
	case TI_Foc_Re_Stain:
	case TI_Foc_Re_DefectPixel:
		lReturn = _TI_Foc_JudgeResult(nStepIdx, nTestItemID, nParaIdx);
		break;

	default:
		break;
	}

	switch (nTestItemID)
	{
	case TI_Foc_Fn_PreFocus:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stFocus.stOpticalCenterData);
		break;

	case TI_Foc_Fn_ActiveAlign:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlignData, Foc_AA);
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stFocus.stTorqueData, Foc_AA_Torque);
		break;

	case TI_Foc_Fn_ECurrent:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stFocus.stECurrentData);
		break;

	case TI_Foc_Fn_OpticalCenter:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stFocus.stOpticalCenterData);
		break;

	case TI_Foc_Fn_SFR:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stFocus.stSFRData);
		break;

	case TI_Foc_Fn_Rotation:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stFocus.stRotateData);
		break;

	case TI_Foc_Fn_Stain:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stFocus.stParticleData);
		break;

	case TI_Foc_Fn_DefectPixel:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stFocus.stDefectPixelData);
		break;

	case TI_Foc_Motion_ReleaseScrew:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stFocus.stTorqueData);
		break;

	case TI_Foc_Motion_LockingScrew:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stFocus.stTorqueData);
		break;

	case TI_Foc_Re_TorqueCheck:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stFocus.stTorqueData);
		break;

	default:
		break;
	}
	if (m_stOption.Inspector.bSaveImage_RGB)
	{
		switch (nTestItemID)
		{
		case TI_Foc_Fn_OpticalCenter:
		case TI_Foc_Fn_SFR:
		case TI_Foc_Fn_Rotation:
		case TI_Foc_Fn_Stain:
			//	case TI_Foc_Fn_DefectPixel:
			m_bPicCaptureMode = TRUE;
			for (int t = 0; t < 100; t++)
			{
				Sleep(50);
				if (m_bPicCaptureMode == FALSE)
				{
					break;
				}
			}
			break;
		}
	}
	switch (nTestItemID)
	{
	case TI_Foc_Fn_OpticalCenter:
	case TI_Foc_Fn_SFR:
	case TI_Foc_Fn_Rotation:
	case TI_Foc_Fn_Stain:

		EachTest_ResultDataSave(nTestItemID, nParaIdx);

		break;
	default:
		break;
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_Initialize
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/6 - 21:56
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Cm_Initialize(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_Initialize(nStepIdx, Para_Left);

	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_Foc_Initialize_Test].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}
	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_Finalize
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/13 - 13:54
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Cm_Finalize(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_Finalize(nStepIdx, Para_Left);

	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_Foc_Finalize_Test].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}
	
	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_Finalize_Error
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/6/10 - 10:49
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Cm_Finalize_Error(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_Finalize_Error(nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_MeasurCurrent
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/13 - 13:54
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Cm_MeasurCurrent(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_MeasurCurrent(nStepIdx, nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_MeasurVoltage
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:23
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Cm_MeasurVoltage(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_MeasurVoltage(nStepIdx, nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_CaptureImage
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Parameter	: __in BOOL bSaveImage
// Parameter	: __in LPCTSTR szFileName
// Qualifier	:
// Last Update	: 2018/3/3 - 20:23
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Cm_CaptureImage(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/, __in BOOL bSaveImage /*= FALSE*/, __in LPCTSTR szFileName /*= NULL*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_CaptureImage(nStepIdx, nParaIdx, bSaveImage, szFileName);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_CameraRegisterSet
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nRegisterType
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:23
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Cm_CameraRegisterSet(__in UINT nStepIdx, __in UINT nRegisterType, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_CameraRegisterSet(nStepIdx, nRegisterType, nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_CameraAlphaSet
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nAlpha
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:23
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Cm_CameraAlphaSet(__in UINT nStepIdx, __in UINT nAlpha, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_CameraAlphaSet(nStepIdx, nAlpha, nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_CameraPowerOnOff
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in enPowerOnOff nOnOff
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:23
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Cm_CameraPowerOnOff(__in UINT nStepIdx, __in enPowerOnOff nOnOff, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_CameraPowerOnOff(nStepIdx, nOnOff, nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Get_ImageBuffer_CaptureImage
// Access		: protected  
// Returns		: BOOL
// Parameter	: enImageMode eImageMode
// Parameter	: __in UINT nParaIdx
// Parameter	: __out UINT & nImgW
// Parameter	: __out UINT & nImgH
// Parameter	: __in BOOL bSaveImage
// Parameter	: __in LPCTSTR szFileName
// Qualifier	:
// Last Update	: 2018/3/3 - 20:30
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP_Foc::_TI_Get_ImageBuffer_CaptureImage(enImageMode eImageMode, __in UINT nParaIdx, __out UINT &nImgW, __out UINT &nImgH, __out CString &strTestFileName, __in BOOL bSaveImage /*= FALSE*/, __in LPCTSTR szFileName /*= NULL*/)
{
	int nWidth = 0;
	int nHeight = 0;

	// 이미지 캡쳐 (Request Capture)
	if (ImageMode_LiveCam == eImageMode)
	{
		UINT nPara = Para_Left;

		if (Sys_Focusing == m_InspectionType)
			nPara = Para_Left;
		else
			nPara = nParaIdx;

		if (!m_Device.DAQ_LVDS.GetSignalStatus(nPara))
		{
			nImgW = 0;
			nImgH = 0;
			return FALSE;
		}

		ST_VideoRGB* pstVideo = m_Device.DAQ_LVDS.GetRecvVideoRGB(nPara);
		LPWORD lpwImage = (LPWORD)m_Device.DAQ_LVDS.GetSourceFrameData(nPara);
		LPBYTE lpbyRGBImage = m_Device.DAQ_LVDS.GetRecvRGBData(nPara);

		nWidth = pstVideo->m_dwWidth;
		nHeight = pstVideo->m_dwHeight;

		// 이미지 버퍼에 복사
		memcpy(m_stImageBuf[nParaIdx].lpwImage_16bit, lpwImage, nWidth * nHeight * sizeof(WORD));
		memcpy(m_stImageBuf[nParaIdx].lpbyImage_8bit, lpbyRGBImage, nWidth * nHeight * 3);
	}
	else{

		if (m_stImageMode.szImagePath.IsEmpty())
		{
			nImgW = 0;
			nImgH = 0;
			return FALSE;
		}
		IplImage *LoadImage = cvLoadImage((CStringA)m_stImageMode.szImagePath, -1);

		nWidth = LoadImage->width;
		nHeight = LoadImage->height;

		if (nWidth < 1 || nHeight < 1)
		{
			nImgW = 0;
			nImgH = 0;
			return FALSE;
		}
		m_stImageBuf[nParaIdx].AssignMem(nWidth, nHeight);
		memcpy(m_stImageBuf[nParaIdx].lpwImage_16bit, LoadImage->imageData, LoadImage->width*LoadImage->height * 2);

		IplImage *LoadImage_8bit = cvCreateImage(cvSize(nWidth, nHeight), IPL_DEPTH_8U, 3);

		cv::Mat Bit16Mat(LoadImage->height, LoadImage->width, CV_16UC1, LoadImage->imageData);
		Bit16Mat.convertTo(Bit16Mat, CV_8UC1, 0.2, 0);

		cv::Mat rgb8BitMat(LoadImage_8bit->height, LoadImage_8bit->width, CV_8UC3, LoadImage_8bit->imageData);
		cv::cvtColor(Bit16Mat, rgb8BitMat, CV_GRAY2BGR);

		memcpy(m_stImageBuf[nParaIdx].lpbyImage_8bit, LoadImage_8bit->imageData, LoadImage->width*LoadImage->height * 3);

		cvReleaseImage(&LoadImage);
		cvReleaseImage(&LoadImage_8bit);
	}

	// 이미지 저장
	if (bSaveImage)
	{
		CString szPath;
		CString szImgFileName;

		SYSTEMTIME tmLocal;
		GetLocalTime(&tmLocal);
		if (szFileName)
		{
			CString strDate;
			strDate.Format(_T("_%02d%02d_%02d%02d%02d"), tmLocal.wMonth, tmLocal.wDay, tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);

			szImgFileName = (szFileName + strDate);
		}
		else
		{
			szImgFileName.Format(_T("%02d%02d_%02d%02d%02d"), tmLocal.wMonth, tmLocal.wDay, tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
		}

		CString strPath;
		//-바코드
		if (FALSE == m_stInspInfo.CamInfo[nParaIdx].szBarcode.IsEmpty())
		{
			strPath.Format(_T("%s/%s"), m_stInspInfo.Path.szImage, m_stInspInfo.CamInfo[nParaIdx].szBarcode);
		}
		else{
			strPath.Format(_T("%s/No Barcode"), m_stInspInfo.Path.szImage);
		}
		MakeDirectory(strPath);

		if (1 < g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt)
			szPath.Format(_T("%s/Capatue_%s_%s"), strPath, g_szParaName[nParaIdx], szImgFileName);
		else
			szPath.Format(_T("%s/Capatue_%s"), strPath, szImgFileName);


		strTestFileName = szPath;
		if (m_stOption.Inspector.bSaveImage_RAW)
		{
			CString szFullPath;
			szFullPath.Format(_T("%s\\%s.raw"), szPath, szImgFileName);
			m_Device.DAQ_LVDS.SaveRawImage(nParaIdx, szFullPath.GetBuffer(0));
		}


		szPath += _T(".png");
		if (m_stOption.Inspector.bSaveImage_Gray12)
		{
			OnSaveImage_Gray16(szPath.GetBuffer(0), m_stImageBuf[nParaIdx].lpwImage_16bit, nWidth, nHeight);
		}
	}

	nImgW = nWidth;
	nImgH = nHeight;

	m_szImageFileName = strTestFileName;

	return TRUE;
}

//=============================================================================
// Method		: _TI_Foc_Pro_StageLoad
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/5 - 11:05
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Foc_Mot_StageLoad(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

#ifndef MOTION_NOT_USE
	lReturn = m_MotionSequence.OnActionLoad_Foc();
#endif

	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_Foc_Motion_StageLoad].vt);
	
	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		varResult.SetString(_T("OK"), VT_BSTR);
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		varResult.SetString(_T("NG"), VT_BSTR);
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		OnAddAlarm_F(_T("Motion : Move to Stage Load [Para -> %s]"), g_szParaName[nParaIdx]);
	}
	
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Foc_Pro_StageUnLoad
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/5 - 11:06
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Foc_Mot_StageUnLoad(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

#ifndef MOTION_NOT_USE	
	lReturn = m_MotionSequence.OnActionUnLoad_Foc();
#endif

	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_Foc_Motion_StageUnLoad].vt);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		varResult.SetString(_T("OK"), VT_BSTR);
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		varResult.SetString(_T("NG"), VT_BSTR);
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		OnAddAlarm_F(_T("Motion : Move to Stage UnLoad [Para -> %s]"), g_szParaName[nParaIdx]);
	}

	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Foc_Mot_LockingCheck
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/7 - 16:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Foc_Mot_LockingCheck(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;
	BOOL bStatus = TRUE;

	switch (m_stInspInfo.RecipeInfo.ModelType)
	{
	case Model_MRA2:
		if (Para_Left == nParaIdx)
		{
			bStatus = m_Device.DigitalIOCtrl.Get_DI_Status(DI_Fo_17_FixOnSensorR);
		}
		else
		{
			bStatus = m_Device.DigitalIOCtrl.Get_DI_Status(DI_Fo_16_FixOnSensorL);
		}
		break;
	case Model_IKC:
		bStatus = m_Device.DigitalIOCtrl.Get_DI_Status(DI_Fo_16_FixOnSensorL) & m_Device.DigitalIOCtrl.Get_DI_Status(DI_Fo_17_FixOnSensorR);
		break;
	default:
		break;
	}

	if (FALSE == bStatus)
	{
		lReturn = RC_Rution_FixLocking_Err;
	}

	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_Foc_Motion_LockingCheck].vt);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		varResult.SetString(_T("OK"), VT_BSTR);
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		varResult.SetString(_T("NG"), VT_BSTR);
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		OnAddAlarm_F(_T("Motion : Check Fix Senser Locking [Para -> %s]"), g_szParaName[nParaIdx]);
		Sleep(2000);
	}

	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Foc_Mot_ReleaseTorque
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/7 - 15:50
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Foc_Mot_ReleaseTorque(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;
	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;

	lReturn = m_MotionSequence.OnActionLoad_Master_Foc(nParaIdx);

	if (RC_OK == lReturn)
	{
		lReturn = m_MotionSequence.OnActionTesting_Foc_Driver(DN);

		for (UINT nCnt = 0; nCnt <= nRetryCnt; nCnt++)
		{
			// Loop 중인 Item 선정
			SetLoopItem_Info(pStepInfo->StepList[nStepIdx].nTestItem, pStepInfo->StepList[nStepIdx].nTestLoop);

			m_MotionSequence.OnActionLampControl(DO_Fo_08_StartLempL, IO_SignalT_ToggleStart);

			while (GetLoopItem_Info(pStepInfo->StepList[nStepIdx].nTestItem))
			{
				TestResultView(pStepInfo->StepList[nStepIdx].nTestItem, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stFocus.stTorqueData);

				// 스크류 토크 값 확인 필요  & 카운팅 
				DoEvents(50);
			}

			if (Torque_Rele == m_stInspInfo.CamInfo[nParaIdx].stFocus.stTorqueData.enStatus[enTorData_Header_A]
				&& Torque_Rele == m_stInspInfo.CamInfo[nParaIdx].stFocus.stTorqueData.enStatus[enTorData_Header_B]
				&& Torque_Rele == m_stInspInfo.CamInfo[nParaIdx].stFocus.stTorqueData.enStatus[enTorData_Header_C]
				&& Torque_Rele == m_stInspInfo.CamInfo[nParaIdx].stFocus.stTorqueData.enStatus[enTorData_Header_D])
			{
				lReturn = RC_OK;
				break;
			}
			else
			{
				lReturn = RC_Motor_Err_ReleaseTorque;
			}

			// 진짜 PASS 일 경우 통과
			if (RC_OK == lReturn)
			{
				break;
			}

			m_MotionSequence.OnActionLampControl(DO_Fo_08_StartLempL, IO_SignalT_ToggleStop);
		}
	}

	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_Foc_Motion_ReleaseScrew].vt);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		varResult.SetString(_T("OK"), VT_BSTR);
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		varResult.SetString(_T("NG"), VT_BSTR);
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Foc_Mot_ParticleLenzIn
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/9 - 12:15
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Foc_Mot_ParticleLenzIn(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

#ifndef MOTION_NOT_USE
	lReturn = m_MotionSequence.OnActionTesting_Foc_Particle(ON);
#endif

	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_Foc_Motion_ParticleIn].vt);

	if (RC_OK == lReturn)
	{
		varResult.SetString(_T("OK"), VT_BSTR);
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		varResult.SetString(_T("NG"), VT_BSTR);
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		OnAddAlarm_F(_T("Motion : Move to Particle Lenz In [Para -> %s]"), g_szParaName[nParaIdx]);
	}

	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Foc_Mot_ParticleLenzOut
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/9 - 12:15
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Foc_Mot_ParticleLenzOut(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

#ifndef MOTION_NOT_USE
	lReturn = m_MotionSequence.OnActionTesting_Foc_Particle(OFF);
#endif

	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_Foc_Motion_ParticleOut].vt);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		varResult.SetString(_T("OK"), VT_BSTR);
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		varResult.SetString(_T("NG"), VT_BSTR);
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		OnAddAlarm_F(_T("Motion : Move to Particle Lenz Out [Para -> %s]"), g_szParaName[nParaIdx]);
	}

	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Foc_Mot_LockingTorque
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/6/21 - 13:32
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Foc_Mot_LockingTorque(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;

	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;

	lReturn = m_MotionSequence.OnActionLoad_Master_Foc(nParaIdx);

	if (RC_OK == lReturn)
	{
		lReturn = m_MotionSequence.OnActionTesting_Foc_Driver(DN);

		if (TRUE == m_Device.DigitalIOCtrl.Get_DI_Status(DI_Fo_06_ColletDnSensor))
		{
			// 2018.10.16 콜렛 UP/DOWN 기능 추가
			if (RC_OK == lReturn)
			{
				// 1. UnFix
				Sleep(200);
				lReturn = m_MotionSequence.OnActionTesting_Foc_Fix(OFF, nParaIdx);

				if (RC_OK == lReturn)
				{
					// 2. Fix
					Sleep(200);
					lReturn = m_MotionSequence.OnActionTesting_Foc_Fix(ON, nParaIdx);

					if (RC_OK == lReturn)
					{
						// 3. Collet Up
						Sleep(400);
						lReturn = m_MotionSequence.OnActionTesting_Foc_Collet(UP);
					}
				}
			}
		}

		if (RC_OK == lReturn)
		{
			for (UINT nTry = 0; nTry <= nRetryCnt; nTry++)
			{
				// Loop 중인 Item 선정
				SetLoopItem_Info(pStepInfo->StepList[nStepIdx].nTestItem, pStepInfo->StepList[nStepIdx].nTestLoop);

				m_MotionSequence.OnActionLampControl(DO_Fo_08_StartLempL, IO_SignalT_ToggleStart);

				while (GetLoopItem_Info(pStepInfo->StepList[nStepIdx].nTestItem))
				{
					TestResultView(pStepInfo->StepList[nStepIdx].nTestItem, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stFocus.stTorqueData);

					// 스크류 토크 값 확인 필요  & 카운팅 
					DoEvents(50);

					_TI_Foc_Pro_Torque(nParaIdx);
				}

				if (TR_Pass == m_stInspInfo.CamInfo[nParaIdx].stFocus.stTorqueData.nResult)
				{
					lReturn = RC_OK;
					break;
				}
				else
				{
					lReturn = RC_Motor_Err_LockingTorque;
				}
			}
		}

		m_MotionSequence.OnActionLampControl(DO_Fo_08_StartLempL, IO_SignalT_ToggleStop);
	}

	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_Foc_Motion_LockingScrew].vt);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		varResult.SetString(_T("OK"), VT_BSTR);
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		varResult.SetString(_T("NG"), VT_BSTR);
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	return lReturn;
}
//=============================================================================
// Method		: _TI_Foc_Mot_ColletUp
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/12/12 - 14:52
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Foc_Mot_ColletUp(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

//	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;

//	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;

	lReturn = m_MotionSequence.OnActionLoad_Master_Foc(nParaIdx);

	if (RC_OK == lReturn)
	{
		lReturn = m_MotionSequence.OnActionTesting_Foc_Driver(DN);

		if (TRUE == m_Device.DigitalIOCtrl.Get_DI_Status(DI_Fo_06_ColletDnSensor))
		{
			// 2018.10.16 콜렛 UP/DOWN 기능 추가
			if (RC_OK == lReturn)
			{
				// 1. UnFix
				Sleep(200);
				lReturn = m_MotionSequence.OnActionTesting_Foc_Fix(OFF, nParaIdx);

				if (RC_OK == lReturn)
				{
					// 2. Fix
					Sleep(200);
					lReturn = m_MotionSequence.OnActionTesting_Foc_Fix(ON, nParaIdx);

					if (RC_OK == lReturn)
					{
						// 3. Collet Up
						Sleep(400);
						lReturn = m_MotionSequence.OnActionTesting_Foc_Collet(UP);
					}
				}
			}
		}
	}

	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_Foc_Motion_ColletUp].vt);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		varResult.SetString(_T("OK"), VT_BSTR);
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		varResult.SetString(_T("NG"), VT_BSTR);
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);
	return lReturn;
}
//=============================================================================
// Method		: _TI_Foc_Pro_PreFocus
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/5 - 12:10
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Foc_Pro_PreFocus(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//*** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;
	UINT nImgW = 0, nImgH = 0;

	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;

	// 이미지 버퍼에 복사
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stFocus.szTestFileName, FALSE, _T("PreFocus")))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_NoImage;
	}

	if (RC_OK == lReturn)
	{
		// Loop 중인 Item 선정
		SetLoopItem_Info(pStepInfo->StepList[nStepIdx].nTestItem, pStepInfo->StepList[nStepIdx].nTestLoop);

		m_MotionSequence.OnActionLampControl(DO_Fo_08_StartLempL, IO_SignalT_ToggleStart);

		while (GetLoopItem_Info(pStepInfo->StepList[nStepIdx].nTestItem))
		{
			m_tm_Test.CenterPointFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stFocus.stOpticalCenterOpt, m_stInspInfo.CamInfo[nParaIdx].stFocus.stOpticalCenterData);
		
			DoEvents(33);

			// 이미지 버퍼에 복사
			if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stFocus.szTestFileName, FALSE, _T("PreFocus")))
			{
				m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
					lReturn = RC_NoImage;

					break;
			}

			TestResultView(pStepInfo->StepList[nStepIdx].nTestItem, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stFocus.stOpticalCenterData);
		}
	}

	if (RC_OK == lReturn)
	{
		m_MotionSequence.OnActionLampControl(DO_Fo_08_StartLempL, IO_SignalT_ToggleStop);

		// 최종 측정
		m_tm_Test.CenterPointFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stFocus.stOpticalCenterOpt, m_stInspInfo.CamInfo[nParaIdx].stFocus.stOpticalCenterData);
	}

	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_Foc_Fn_PreFocus].vt);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		varResult.SetString(_T("OK"), VT_BSTR);
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		varResult.SetString(_T("NG"), VT_BSTR);
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		OnAddAlarm_F(_T("TEST : No Detect Optical Center Point [Para -> %s]"), g_szParaName[nParaIdx]);
	}

	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Foc_Pro_ActiveAlign
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/4 - 18:18
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Foc_Pro_ActiveAlign(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;
	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;

	// 1. 기본 광축 잡기
	lReturn = _TI_Foc_AA_Routine(nStepIdx, nParaIdx);

	if (RC_OK != lReturn)
	{
		CString szText;
		szText.Format(_T("Base AA Routine Err"));
		OnAddAlarm(szText);
		m_Log_ErrLog.LogWrite(szText);
	}

	
	// 2. 렌즈 전체 검사 MAX SFR 찾기 위한 행위
	lReturn = _TI_Foc_AA_LensScan(nStepIdx, nParaIdx);
	
	if (RC_OK != lReturn)
	{
		CString szText;
		szText.Format(_T("Lens Scan Routine Err"));
		OnAddAlarm(szText);
		m_Log_ErrLog.LogWrite(szText);
	}

	if (RC_OK == lReturn)
	{
		// 3. AA루틴 동작
		for (UINT nCnt = 0; nCnt <= nRetryCnt; nCnt++)
		{
			// Loop 중인 Item 선정
			SetLoopItem_Info(pStepInfo->StepList[nStepIdx].nTestItem, pStepInfo->StepList[nStepIdx].nTestLoop);

			for (UINT nAACnt = 0; nAACnt < m_stInspInfo.RecipeInfo.stFocus.stActiveAlignOpt.nTargetCnt; nAACnt++)
			{
				DoEvents(33);

				lReturn = _TI_Foc_AA_Routine(nStepIdx, nParaIdx);

				if (RC_OK != lReturn)
				{
					CString szText;
					szText.Format(_T("[Try : %d] AA Routine Err"), nAACnt);
					OnAddAlarm(szText);
					m_Log_ErrLog.LogWrite(szText);

						nCnt = nRetryCnt + 1;
						break;
				}
			}

			m_MotionSequence.OnActionLampControl(DO_Fo_08_StartLempL, IO_SignalT_ToggleStart);

			while (GetLoopItem_Info(pStepInfo->StepList[nStepIdx].nTestItem)) 
			{
				// AA 데이터 
				lReturn = _TI_Foc_AA_Data(nStepIdx, nParaIdx);

				if (RC_OK != lReturn)
				{
						nCnt = nRetryCnt + 1;
						break;
				}

				
				// 토크 판정 
				lReturn = _TI_Foc_Pro_Torque(nParaIdx);
				if (RC_OK != lReturn)
				{
						nCnt = nRetryCnt + 1;
						break;
				}

				// Best Focus
				lReturn = _TI_Foc_AA_LensBest(nStepIdx, nParaIdx);

				if (RC_OK != lReturn)
				{
						nCnt = nRetryCnt + 1;
						break;
				}

			}

			m_MotionSequence.OnActionLampControl(DO_Fo_08_StartLempL, IO_SignalT_ToggleStop);

			
			// 최종 판정
			if (TR_Pass == m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlignData.nResult && TR_Pass == m_stInspInfo.CamInfo[nParaIdx].stFocus.stSFRData.nResult)
			{
				// 토크 && Best SFR 합격일 경우만 다음 진행
				if (TR_Pass == m_stInspInfo.CamInfo[nParaIdx].stFocus.stTorqueData.nResult && TR_Pass == m_stInspInfo.CamInfo[nParaIdx].stFocus.stFocusData.nResult)
				{
					lReturn = RC_OK;
					break;
				}
				else
				{
					if (nCnt <=0)
					{
						nCnt--;
					}
					else
					{
						nCnt = 0;
					}
				}
			}
			else
			{
				// 에러 문구 작성
			}
			
		}
	}

	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_Foc_Fn_ActiveAlign].vt);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		varResult.SetString(_T("OK"), VT_BSTR);
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		varResult.SetString(_T("NG"), VT_BSTR);
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		OnAddAlarm_F(_T("TEST : No Detect Optical Center Point & SFR [Para -> %s]"), g_szParaName[nParaIdx]);
	}

	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Foc_Pro_OpticalCenter
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:30
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Foc_Pro_OpticalCenter(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//*** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;

	UINT nImgW = 0, nImgH = 0;

	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;

	// 이미지 버퍼에 복사
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stFocus.szTestFileName, FALSE, _T("OpticalCenter")))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_NoImage;
	}

	if (RC_OK == lReturn)
	{
		// Loop 중인 Item 선정
		SetLoopItem_Info(pStepInfo->StepList[nStepIdx].nTestItem, pStepInfo->StepList[nStepIdx].nTestLoop);

		m_MotionSequence.OnActionLampControl(DO_Fo_08_StartLempL, IO_SignalT_ToggleStart);

		while (GetLoopItem_Info(pStepInfo->StepList[nStepIdx].nTestItem))
		{
			m_tm_Test.CenterPointFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stFocus.stOpticalCenterOpt, m_stInspInfo.CamInfo[nParaIdx].stFocus.stOpticalCenterData);

			// 이미지 버퍼에 복사
			if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stFocus.szTestFileName, FALSE, _T("OpticalCenter")))
			{
				m_MotionSequence.OnActionLampControl(DO_Fo_08_StartLempL, IO_SignalT_ToggleStop);
				m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
				lReturn = RC_NoImage;
			}

			DoEvents(33);
		}
	}

	if (RC_OK == lReturn)
	{
		m_MotionSequence.OnActionLampControl(DO_Fo_08_StartLempL, IO_SignalT_ToggleStop);
		if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stFocus.szTestFileName, TRUE, _T("OpticalCenter")))
		{
			m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
			lReturn = RC_NoImage;
		}

		DoEvents(33);

		m_tm_Test.CenterPointFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stFocus.stOpticalCenterOpt, m_stInspInfo.CamInfo[nParaIdx].stFocus.stOpticalCenterData);
	}

	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_Foc_Fn_OpticalCenter].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}


	return lReturn;
}

//=============================================================================
// Method		: _TI_Foc_Pro_Current
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:30
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Foc_Pro_Current(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//*** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;

	UINT nImgW = 0, nImgH = 0;

	ST_CamBrdCurrent stCurrent;

	lReturn = m_Device.PCBCamBrd[0].Send_GetCurrent(stCurrent);

	if (RC_OK == lReturn)
	{
		m_stInspInfo.CamInfo[nParaIdx].stFocus.stECurrentData.nResult = TR_Pass;

		int nCurrentCnt = 2;

		if ((m_stInspInfo.RecipeInfo.ModelType == Model_OMS_Front_Set) || (m_stInspInfo.RecipeInfo.ModelType == Model_OMS_Front) || (m_stInspInfo.RecipeInfo.ModelType == Model_OMS_Entry))
			nCurrentCnt = Spec_ECurrent_Max;

		for (int nCh = 0; nCh < nCurrentCnt; nCh++)
		{
			m_stInspInfo.CamInfo[nParaIdx].stFocus.stECurrentData.dbValue[nCh]		 = stCurrent.fOutCurrent[nCh] * m_stInspInfo.RecipeInfo.stFocus.stECurrentOpt.dbOffset[Para_Left][nCh];
			m_stInspInfo.CamInfo[nParaIdx].stFocus.stECurrentData.nEachResult[nCh]	 = m_tm_Test.Get_MeasurmentData(m_stInspInfo.RecipeInfo.stFocus.stECurrentOpt.stSpec_Min[nCh].bEnable, m_stInspInfo.RecipeInfo.stFocus.stECurrentOpt.stSpec_Max[nCh].bEnable, m_stInspInfo.RecipeInfo.stFocus.stECurrentOpt.stSpec_Min[nCh].dbValue, m_stInspInfo.RecipeInfo.stFocus.stECurrentOpt.stSpec_Max[nCh].dbValue, m_stInspInfo.CamInfo[nParaIdx].stFocus.stECurrentData.dbValue[nCh]);
			m_stInspInfo.CamInfo[nParaIdx].stFocus.stECurrentData.nResult			&= m_stInspInfo.CamInfo[nParaIdx].stFocus.stECurrentData.nEachResult[nCh];
		}
	}

	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_Foc_Fn_ECurrent].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_Foc_Pro_SFR
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:30
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Foc_Pro_SFR(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//*** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;

	UINT nImgW = 0, nImgH = 0;

	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;

	// 이미지 버퍼에 복사
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stFocus.szTestFileName, FALSE, _T("SFR")))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_NoImage;
	}

	if (RC_OK == lReturn)
	{
		// Loop 중인 Item 선정
		SetLoopItem_Info(pStepInfo->StepList[nStepIdx].nTestItem, pStepInfo->StepList[nStepIdx].nTestLoop);

		m_MotionSequence.OnActionLampControl(DO_Fo_08_StartLempL, IO_SignalT_ToggleStart);

		while (GetLoopItem_Info(pStepInfo->StepList[nStepIdx].nTestItem))
		{
			lReturn = m_tm_Test.SFRFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stImageBuf[nParaIdx].lpwImage_16bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stFocus.stSFROpt, m_stInspInfo.CamInfo[nParaIdx].stFocus.stSFRData);

			// 이미지 버퍼에 복사
			if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stFocus.szTestFileName, FALSE, _T("SFR")))
			{
				m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
				lReturn = RC_NoImage;
					break;
			}
	
			DoEvents(33);
		}

		m_MotionSequence.OnActionLampControl(DO_Fo_08_StartLempL, IO_SignalT_ToggleStop);
	}

	if (RC_OK == lReturn)
	{
		if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stFocus.szTestFileName, TRUE, _T("SFR")))
		{
			m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
			lReturn = RC_NoImage;
		}

		lReturn = m_tm_Test.SFRFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stImageBuf[nParaIdx].lpwImage_16bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stFocus.stSFROpt, m_stInspInfo.CamInfo[nParaIdx].stFocus.stSFRData);
	}

	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_Foc_Fn_SFR].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_Foc_Pro_Rotation
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:30
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Foc_Pro_Rotation(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//*** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;
	UINT nImgW = 0, nImgH = 0;
	
	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;

	// 이미지 버퍼에 복사
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stFocus.szTestFileName, FALSE, _T("Rotate")))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_NoImage;
	}

	if (RC_OK == lReturn)
	{
		// Loop 중인 Item 선정
		SetLoopItem_Info(pStepInfo->StepList[nStepIdx].nTestItem, pStepInfo->StepList[nStepIdx].nTestLoop);

		m_MotionSequence.OnActionLampControl(DO_Fo_08_StartLempL, IO_SignalT_ToggleStart);

		while (GetLoopItem_Info(pStepInfo->StepList[nStepIdx].nTestItem))
		{
			lReturn = m_tm_Test.RotateFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stFocus.stRotateOpt, m_stInspInfo.CamInfo[nParaIdx].stFocus.stRotateData);

			// 이미지 버퍼에 복사
			if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stFocus.szTestFileName, FALSE, _T("Rotate")))
			{
				m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
				lReturn = RC_NoImage;
					break;
			}

			DoEvents(33);
		}

		m_MotionSequence.OnActionLampControl(DO_Fo_08_StartLempL, IO_SignalT_ToggleStop);
	}

	if (RC_OK == lReturn)
	{
		if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stFocus.szTestFileName, TRUE, _T("Rotate")))
		{
			m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
			lReturn = RC_NoImage;
		}

		DoEvents(33);

		lReturn = m_tm_Test.RotateFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stFocus.stRotateOpt, m_stInspInfo.CamInfo[nParaIdx].stFocus.stRotateData);
	}

	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_Foc_Fn_Rotation].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);
	
	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_Foc_Pro_Particle
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:30
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Foc_Pro_Particle(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//*** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;
	UINT nImgW = 0, nImgH = 0;

	OnLightBrd_PowerOn(m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_Particle].fVolt, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_Particle].wStep);

	Sleep(m_stInspInfo.RecipeInfo.nLightDelay);

	// 이미지 버퍼에 복사
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stFocus.szTestFileName, TRUE, _T("Stain")))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_NoImage;
	}

	if (RC_OK == lReturn)
	{
		lReturn = m_tm_Test.ParticleFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stImageBuf[nParaIdx].lpwImage_16bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stFocus.stParticleOpt, m_stInspInfo.CamInfo[nParaIdx].stFocus.stParticleData);
	}

	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_Foc_Fn_Stain].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_Foc_Pro_DefectPixel
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:30
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Foc_Pro_DefectPixel(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//*** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;

	stDPList stDefectList;
	CString strCaptureCnt;

	for (UINT nMode = 0; nMode < Light_Defect_MAX; nMode++)
	{
		UINT nImgW = 0, nImgH = 0;

		OnLightBrd_PowerOn(m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[nMode + Light_I_Defect_B].fVolt, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[nMode + Light_I_Defect_B].wStep);

		Sleep(m_stInspInfo.RecipeInfo.nLightDelay);
		strCaptureCnt.Format(_T("DefectPixel_%d"), nMode + 1);

		// 이미지 버퍼에 복사
		if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stFocus.szTestFileName, TRUE, strCaptureCnt))
		{
			m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
			lReturn = RC_NoImage;
			break;
		}

		if (RC_OK == lReturn)
		{
			lReturn = m_tm_Test.DefectPixelFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stImageBuf[nParaIdx].lpwImage_16bit, nImgW, nImgH, nMode, stDefectList, m_stInspInfo.RecipeInfo.stFocus.stDefectPixelOpt, m_stInspInfo.CamInfo[nParaIdx].stFocus.stDefectPixelData);

			if (nMode < (Light_Defect_MAX - 1))
			{
				if (m_stOption.Inspector.bSaveImage_RGB)
				{
					m_bPicCaptureMode = TRUE;
					for (int t = 0; t < 100; t++)
					{
						Sleep(50);
						if (m_bPicCaptureMode == FALSE)
						{
							break;
						}
					}
				}
			}
		}
	}

	for (UINT nIdx = 0; nIdx < Spec_Defect_Max; nIdx++)
	{
		m_stInspInfo.CamInfo[nParaIdx].stFocus.stDefectPixelData.nFailCount[nIdx] = 0;
	}

	UINT nCnt[Spec_Defect_Max] = { 0, };

	for (INT nIdx = 0; nIdx < stDefectList.DefectPixe_Cnt; nIdx++)
	{
		// 유형별 카운팅
		UINT nItem = stDefectList.DP_List[nIdx].nType;

		m_stInspInfo.CamInfo[nParaIdx].stFocus.stDefectPixelData.nFailType[nItem][nCnt[nItem]] = stDefectList.DP_List[nIdx].nType;
		m_stInspInfo.CamInfo[nParaIdx].stFocus.stDefectPixelData.nFailCount[nItem]++;
		m_stInspInfo.CamInfo[nParaIdx].stFocus.stDefectPixelData.rtFailROI[nItem][nCnt[nItem]].left = stDefectList.DP_List[nIdx].x;
		m_stInspInfo.CamInfo[nParaIdx].stFocus.stDefectPixelData.rtFailROI[nItem][nCnt[nItem]].top = stDefectList.DP_List[nIdx].y;
		m_stInspInfo.CamInfo[nParaIdx].stFocus.stDefectPixelData.rtFailROI[nItem][nCnt[nItem]].right = stDefectList.DP_List[nIdx].x + stDefectList.DP_List[nIdx].W;
		m_stInspInfo.CamInfo[nParaIdx].stFocus.stDefectPixelData.rtFailROI[nItem][nCnt[nItem]].bottom = stDefectList.DP_List[nIdx].y + stDefectList.DP_List[nIdx].H;

		nCnt[nItem]++; //HTH 추가 0305
	}

	// 양불 판정
	for (UINT nIdx = 0; nIdx < Spec_Defect_Max; nIdx++)
	{
		if (m_stInspInfo.RecipeInfo.stFocus.stDefectPixelOpt.stSpec_Min[nIdx].iValue <= (int)m_stInspInfo.CamInfo[nParaIdx].stFocus.stDefectPixelData.nFailCount[nIdx] && m_stInspInfo.RecipeInfo.stFocus.stDefectPixelOpt.stSpec_Max[nIdx].iValue >= (int)m_stInspInfo.CamInfo[nParaIdx].stFocus.stDefectPixelData.nFailCount[nIdx])
		{
			m_stInspInfo.CamInfo[nParaIdx].stFocus.stDefectPixelData.nEachResult[nIdx] = TR_Pass;
		}
		else
		{
			m_stInspInfo.CamInfo[nParaIdx].stFocus.stDefectPixelData.nEachResult[nIdx] = TR_Fail;
			m_stInspInfo.CamInfo[nParaIdx].stFocus.stDefectPixelData.nResult = TR_Fail;
		}
	}
	if (m_stOption.Inspector.bSaveImage_RGB)
	{

		m_bPicCaptureMode = TRUE;
		for (int t = 0; t < 100; t++)
		{
			Sleep(50);
			if (m_bPicCaptureMode == FALSE)
			{
				break;
			}
		}
	}

	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_Foc_Fn_DefectPixel].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Foc_JudgeResult
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nTestItem
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:30
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Foc_JudgeResult(__in UINT nStepIdx, __in UINT nTestItem, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;
	CArray <COleVariant, COleVariant&> VarArray;
	COleVariant varResult;

	for (UINT nIdx = 0; nIdx < m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList.GetAt(nTestItem).nResultCount; nIdx++)
	{
		switch (nTestItem)
		{
		case TI_Foc_Re_TorqueCheck:
			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stFocus.stTorqueData.dbValue[nIdx];
			VarArray.Add(varResult);
			break;
		case TI_Foc_Re_ECurrent:
			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stFocus.stECurrentData.dbValue[nIdx];
			VarArray.Add(varResult);
			break;
		case TI_Foc_Re_OpticalCenterX:
			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stFocus.stOpticalCenterData.iStandPosDevX;
			VarArray.Add(varResult);
			break;
		case TI_Foc_Re_OpticalCenterY:
			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stFocus.stOpticalCenterData.iStandPosDevY;
			VarArray.Add(varResult);
			break;
		case TI_Foc_Re_SFR:
			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stFocus.stSFRData.dbValue[nIdx];
			VarArray.Add(varResult);
			break;
		case TI_Foc_Re_Group_SFR:
			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stFocus.stSFRData.dbEachResultGroup[nIdx];
			VarArray.Add(varResult);
			break;
		case TI_Foc_Re_Tilt_SFR:
			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stFocus.stSFRData.dbTiltdata[nIdx];
			VarArray.Add(varResult);
			break;
		case TI_Foc_Re_Rotation:
			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stFocus.stRotateData.dbValue;
			VarArray.Add(varResult);
			break;
		case TI_Foc_Re_Stain:
			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stFocus.stParticleData.nFailCount;
			VarArray.Add(varResult);
			break;
		case TI_Foc_Re_DefectPixel:
			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stFocus.stDefectPixelData.nFailCount[nIdx];
			VarArray.Add(varResult);
			break;
			
		//case TI_Foc_Re_G0_SFR:
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stFocus.stSFRData.dbMinValue[ITM_SFR_G0];
		//	VarArray.Add(varResult);
		//	break;

		//case TI_Foc_Re_G1_SFR:
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stFocus.stSFRData.dbMinValue[ITM_SFR_G1];
		//	VarArray.Add(varResult);
		//	break;

		//case TI_Foc_Re_G2_SFR:
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stFocus.stSFRData.dbMinValue[ITM_SFR_G2];
		//	VarArray.Add(varResult);
		//	break;

		//case TI_Foc_Re_G3_SFR:
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stFocus.stSFRData.dbMinValue[ITM_SFR_G3];
		//	VarArray.Add(varResult);
		//	break;

		//case TI_Foc_Re_G4_SFR:
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stFocus.stSFRData.dbMinValue[ITM_SFR_G4];
		//	VarArray.Add(varResult);
		//	break;

		//case TI_Foc_Re_X1Tilt_SFR:
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stFocus.stSFRData.dbTiltdata[ITM_SFR_X1_Tilt_UpperLimit];
		//	VarArray.Add(varResult);
		//	break;

		//case TI_Foc_Re_X2Tilt_SFR:
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stFocus.stSFRData.dbTiltdata[ITM_SFR_X2_Tilt_UpperLimit];
		//	VarArray.Add(varResult);
		//	break;

		//case TI_Foc_Re_Y1Tilt_SFR:
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stFocus.stSFRData.dbTiltdata[ITM_SFR_Y1_Tilt_UpperLimit];
		//	VarArray.Add(varResult);
		//	break;

		//case TI_Foc_Re_Y2Tilt_SFR:
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stFocus.stSFRData.dbTiltdata[ITM_SFR_Y2_Tilt_UpperLimit];
		//	VarArray.Add(varResult);
		//	break;
		default:
			break;
		}
	}

	// 측정값 입력 및 판정
	m_stInspInfo.Set_Measurement(nParaIdx, nStepIdx, (UINT)VarArray.GetCount(), VarArray.GetData());

	return lReturn;
}

//=============================================================================
// Method		: _TI_Foc_AA_Routine
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/8/20 - 10:07
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Foc_AA_Routine(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	for (UINT nAACnt = 0; nAACnt < m_stInspInfo.RecipeInfo.stFocus.stActiveAlignOpt.nTargetCnt; nAACnt++)
	{
		DoEvents(33);

		// 1차 광축 조정
		lReturn = _TI_Foc_AA_OpticalCenter(nStepIdx, nParaIdx);

		if (RC_OK != lReturn)
		{
			CString szText;
			szText.Format(_T("[Try : %d] AA Roution 1st Optical Center Err"), nAACnt);
			OnAddAlarm(szText);
			m_Log_ErrLog.LogWrite(szText);
			return lReturn;
		}

		DoEvents(33);

		// 2차 광축 조정
		lReturn = _TI_Foc_AA_OpticalCenter(nStepIdx, nParaIdx);

		if (RC_OK != lReturn)
		{
			CString szText;
			szText.Format(_T("[Try : %d] AA Roution 2nd Optical Center Err"), nAACnt);
			OnAddAlarm(szText);
			m_Log_ErrLog.LogWrite(szText);
			return lReturn;
		}

		DoEvents(33);

		// 1차 로테이트 조정
		lReturn = _TI_Foc_AA_Rotate(nStepIdx, nParaIdx);

		if (RC_OK != lReturn)
		{
			CString szText;
			szText.Format(_T("[Try : %d] AA Roution 1st Rotation Err"), nAACnt);
			OnAddAlarm(szText);
			m_Log_ErrLog.LogWrite(szText);
			return lReturn;
		}

		DoEvents(33);

		// 2차 로테이트 조정
		lReturn = _TI_Foc_AA_Rotate(nStepIdx, nParaIdx);

		if (RC_OK != lReturn)
		{
			CString szText;
			szText.Format(_T("[Try : %d] AA Roution 2nd Rotation Err"), nAACnt);
			OnAddAlarm(szText);
			m_Log_ErrLog.LogWrite(szText);
			return lReturn;
		}

		DoEvents(33);

		// 3차 광축 조정 
		lReturn = _TI_Foc_AA_OpticalCenter(nStepIdx, nParaIdx);

		if (RC_OK != lReturn)
		{
			CString szText;
			szText.Format(_T("[Try : %d] AA Roution 3nd Optical Center Err"), nAACnt);
			OnAddAlarm(szText);
			m_Log_ErrLog.LogWrite(szText);
			return lReturn;
		}

		DoEvents(33);

		// AA 데이터 
		lReturn = _TI_Foc_AA_Data(nStepIdx, nParaIdx);
		if (RC_OK != lReturn)
		{
			return lReturn;
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_Foc_AA_LensScan
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/8/20 - 10:11
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Foc_AA_LensScan(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;
	
	m_bFlag_LensScan = TRUE;
	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;


	UINT nImgW = 0;
	UINT nImgH = 0;

	// Loop 중인 Item 선정
	SetLoopItem_Info(pStepInfo->StepList[nStepIdx].nTestItem, pStepInfo->StepList[nStepIdx].nTestLoop);

	// MAX SFR값을 찾았는지 확인 하는 변수
	BOOL bFindMaxSFR = FALSE;

	while (FALSE == bFindMaxSFR)
	{
		// SFR 데이터 확인 감은쪽 최하단 부터 시작
		DoEvents(33);

		// SFR 루틴
		if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stFocus.szTestFileName, FALSE, _T("SFR")))
		{
			lReturn = RC_NoImage;
			return lReturn;
		}

		lReturn = m_tm_Test.SFRFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stImageBuf[nParaIdx].lpwImage_16bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stFocus.stSFROpt, m_stInspInfo.CamInfo[nParaIdx].stFocus.stSFRData);

		if (RC_OK != lReturn)
		{
			return lReturn;
		}

		// 그래프에 표시 될 내용 정리
		bFindMaxSFR = TRUE;

		for (UINT nROI = 0; nROI < ROI_Focus_Max; nROI++)
		{
			// SFR 값 전달
			m_stInspInfo.CamInfo[nParaIdx].stFocus.stFocusData.dbCurrentValue[nROI] = m_stInspInfo.CamInfo[nParaIdx].stFocus.stSFRData.dbValue[m_stInspInfo.RecipeInfo.stFocus.stFocusOpt.nSelectROI[nROI]];

			// CURRENT와 MAX 비교
			if (m_stInspInfo.CamInfo[nParaIdx].stFocus.stFocusData.dbMaxValue[nROI] < m_stInspInfo.CamInfo[nParaIdx].stFocus.stFocusData.dbCurrentValue[nROI])
			{
				m_stInspInfo.CamInfo[nParaIdx].stFocus.stFocusData.dbMaxValue[nROI] = m_stInspInfo.CamInfo[nParaIdx].stFocus.stFocusData.dbCurrentValue[nROI];
			}
	
			// MAX - CURRENT 값이 SFR GAP 보다 작은 경우 BEST를 찾지 못함
			if (m_stInspInfo.RecipeInfo.stFocus.stFocusOpt.dbMinSFR[nROI] > m_stInspInfo.CamInfo[nParaIdx].stFocus.stFocusData.dbCurrentValue[nROI])
			{
				bFindMaxSFR = FALSE;

				m_stInspInfo.CamInfo[nParaIdx].stFocus.stFocusData.bMaxSFR[nROI] = FALSE;
			}
			else
			{
				m_stInspInfo.CamInfo[nParaIdx].stFocus.stFocusData.bMaxSFR[nROI] = TRUE;
			}
		}

		// 그래프 표시
		OnSet_GraphMax(m_stInspInfo.CamInfo[nParaIdx].stFocus.stFocusData);

		// START 버튼을 누를수 있도록 램프 가이드
		if (TRUE == bFindMaxSFR)
		{
			m_MotionSequence.OnActionLampControl(DO_Fo_08_StartLempL, IO_SignalT_ToggleStart);
		}
		else
		{
			m_MotionSequence.OnActionLampControl(DO_Fo_08_StartLempL, IO_SignalT_ToggleStop);

		}

		// START 버튼이 눌리고, MAX SFR 값을 찾은 경우
		if (FALSE == GetLoopItem_Info(pStepInfo->StepList[nStepIdx].nTestItem) && TRUE == bFindMaxSFR)
		{
			bFindMaxSFR = TRUE;
		}
		else
		{
			bFindMaxSFR = FALSE;
		}
	}

	// START 버튼 램프 끄기
	m_MotionSequence.OnActionLampControl(DO_Fo_08_StartLempL, IO_SignalT_ToggleStop);
	m_bFlag_LensScan = FALSE;

	return lReturn;
}

//=============================================================================
// Method		: _TI_Foc_AA_LensBest
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/8/20 - 11:04
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Foc_AA_LensBest(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;
	m_bFlag_Findbest = TRUE;

	m_stInspInfo.CamInfo[nParaIdx].stFocus.stFocusData.nResult = TR_Pass;


	for (UINT nROI = 0; nROI < ROI_Focus_Max; nROI++)
	{
		// SFR 값 전달
		m_stInspInfo.CamInfo[nParaIdx].stFocus.stFocusData.dbCurrentValue[nROI] = m_stInspInfo.CamInfo[nParaIdx].stFocus.stSFRData.dbValue[m_stInspInfo.RecipeInfo.stFocus.stFocusOpt.nSelectROI[nROI]];

		// CURRENT와 MAX 비교
		if (m_stInspInfo.CamInfo[nParaIdx].stFocus.stFocusData.dbMaxValue[nROI] < m_stInspInfo.CamInfo[nParaIdx].stFocus.stFocusData.dbCurrentValue[nROI])
		{
			m_stInspInfo.CamInfo[nParaIdx].stFocus.stFocusData.dbMaxValue[nROI] = m_stInspInfo.CamInfo[nParaIdx].stFocus.stFocusData.dbCurrentValue[nROI];
		}

	}
	// Graph
	for (UINT nROI = 0; nROI < ROI_Focus_Max; nROI++)
	{
		m_stInspInfo.CamInfo[nParaIdx].stFocus.stFocusData.dbCurrentValue[nROI] = m_stInspInfo.CamInfo[nParaIdx].stFocus.stSFRData.dbValue[m_stInspInfo.RecipeInfo.stFocus.stFocusOpt.nSelectROI[nROI]];

		BOOL bMinUseR = m_stInspInfo.RecipeInfo.stFocus.stFocusOpt.stSpec_Min[nROI].bEnable;
		BOOL bMaxUseR = m_stInspInfo.RecipeInfo.stFocus.stFocusOpt.stSpec_Max[nROI].bEnable;

		double dbMinDevR = m_stInspInfo.CamInfo[nParaIdx].stFocus.stFocusData.dbMaxValue[nROI] - m_stInspInfo.RecipeInfo.stFocus.stFocusOpt.stSpec_Min[nROI].dbValue;
		double dbMaxDevR = m_stInspInfo.CamInfo[nParaIdx].stFocus.stFocusData.dbMaxValue[nROI] + m_stInspInfo.RecipeInfo.stFocus.stFocusOpt.stSpec_Max[nROI].dbValue;

		double dbCurrent = m_stInspInfo.CamInfo[nParaIdx].stFocus.stFocusData.dbCurrentValue[nROI];

		// 판정
		m_stInspInfo.CamInfo[nParaIdx].stFocus.stFocusData.bBestSFR[nROI] = m_tm_Test.Get_MeasurmentData(bMinUseR, bMaxUseR, dbMinDevR, dbMaxDevR, dbCurrent);

		if (FALSE == m_stInspInfo.CamInfo[nParaIdx].stFocus.stFocusData.bBestSFR[nROI])
		{
			m_stInspInfo.CamInfo[nParaIdx].stFocus.stFocusData.nResult = TR_Fail;
		}
	}

	OnSet_GraphBest(m_stInspInfo.CamInfo[nParaIdx].stFocus.stFocusData);
	m_bFlag_Findbest = FALSE;
	return lReturn;
}

//=============================================================================
// Method		: _TI_Foc_AA_OpticalCenter
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/4/23 - 15:33
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Foc_AA_OpticalCenter(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;
	UINT nImgW = 0, nImgH = 0;

	// 이미지 버퍼에 복사
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stFocus.szTestFileName, FALSE, _T("AA_OpticalCenter")))
	{
		CString szText = _T("AA Optical Center Image Empty Err");
		OnAddAlarm(szText);
		m_Log_ErrLog.LogWrite(szText);
		return RC_NoImage;
	}

	// 광축 측정
	lReturn = m_tm_Test.CenterPointFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stFocus.stOpticalCenterOpt, m_stInspInfo.CamInfo[nParaIdx].stFocus.stOpticalCenterData);

	if (RC_OK != lReturn)
	{
		CString szText = _T("AA Optical Center No Detect Err");
		OnAddAlarm(szText);
		m_Log_ErrLog.LogWrite(szText);

		return lReturn;
	}

	// 광축 조정
	int iTargetX = m_stInspInfo.CamInfo[nParaIdx].stFocus.stOpticalCenterData.iStandPosDevX - m_stInspInfo.RecipeInfo.stFocus.stActiveAlignOpt.iTargetX[nParaIdx];
	int iTargetY = m_stInspInfo.CamInfo[nParaIdx].stFocus.stOpticalCenterData.iStandPosDevY - m_stInspInfo.RecipeInfo.stFocus.stActiveAlignOpt.iTargetY[nParaIdx];

	lReturn = m_MotionSequence.OnActionTesting_OC_Adj(iTargetX, iTargetY, (double)m_stInspInfo.RecipeInfo.fPixelSize);

	if (RC_OK != lReturn)
	{
		CString szText = _T("AA Optical Center Move Err");
		OnAddAlarm(szText);
		m_Log_ErrLog.LogWrite(szText);
		return RC_NoImage;
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_Foc_AA_Rotate
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/4/23 - 15:32
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Foc_AA_Rotate(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;
	UINT nImgW = 0, nImgH = 0;

	// 이미지 버퍼에 복사
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stFocus.szTestFileName, FALSE, _T("AA_Rotate")))
	{
		CString szText = _T("AA Rotate Image Empty Err");
		OnAddAlarm(szText);
		m_Log_ErrLog.LogWrite(szText);
		return RC_NoImage;
	}

	// 로테이트 측정
	lReturn = m_tm_Test.RotateFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stFocus.stRotateOpt, m_stInspInfo.CamInfo[nParaIdx].stFocus.stRotateData);

	double dbTargetR = m_stInspInfo.CamInfo[nParaIdx].stFocus.stRotateData.dbValue - m_stInspInfo.RecipeInfo.stFocus.stActiveAlignOpt.dbTargetR;

	if (RC_OK != lReturn)
	{
		CString szText = _T("AA Rotate No Detect Err");
		OnAddAlarm(szText);
		m_Log_ErrLog.LogWrite(szText);

		return lReturn;
	}

	// 로테이트 조정
	lReturn = m_MotionSequence.OnActionTesting_Rotate_Adj(dbTargetR, (double)m_stInspInfo.RecipeInfo.fPixelSize);

	if (RC_OK != lReturn)
	{
		CString szText = _T("AA Rotate Move Err");
		OnAddAlarm(szText);
		m_Log_ErrLog.LogWrite(szText);
		return RC_NoImage;
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_Foc_AA_ScrewLocking
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/9 - 15:58
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Foc_AA_ScrewLocking(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;

	// Loop 중인 Item 선정
	SetLoopItem_Info(pStepInfo->StepList[nStepIdx].nTestItem, pStepInfo->StepList[nStepIdx].nTestLoop);

	m_MotionSequence.OnActionLampControl(DO_Fo_08_StartLempL, IO_SignalT_ToggleStart);

	while (GetLoopItem_Info(pStepInfo->StepList[nStepIdx].nTestItem))
	{
		// 스크류 토크 값 확인 필요  & 카운팅 
		DoEvents(50);
	}

	m_MotionSequence.OnActionLampControl(DO_Fo_08_StartLempL, IO_SignalT_ToggleStop);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Foc_AA_Testing
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/9 - 16:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Foc_AA_Data(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;
	UINT nImgW = 0, nImgH = 0;
	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;

	// 이미지 버퍼에 복사
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stFocus.szTestFileName, FALSE, _T("AA Data")))
	{
		return RC_NoImage;
	}

	// 광축
	m_tm_Test.CenterPointFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stFocus.stOpticalCenterOpt, m_stInspInfo.CamInfo[nParaIdx].stFocus.stOpticalCenterData);

	// 로테이트
	m_tm_Test.RotateFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stFocus.stRotateOpt, m_stInspInfo.CamInfo[nParaIdx].stFocus.stRotateData);

	// AA 결과 판정
	BOOL bMinUseX = m_stInspInfo.RecipeInfo.stFocus.stOpticalCenterOpt.stSpec_Min[Spec_OC_X].bEnable;
	BOOL bMaxUseX = m_stInspInfo.RecipeInfo.stFocus.stOpticalCenterOpt.stSpec_Max[Spec_OC_X].bEnable;
	
	int iMinDevX  = m_stInspInfo.RecipeInfo.stFocus.stOpticalCenterOpt.stSpec_Min[Spec_OC_X].iValue;
	int iMaxDevX  = m_stInspInfo.RecipeInfo.stFocus.stOpticalCenterOpt.stSpec_Max[Spec_OC_X].iValue;

	BOOL bMinUseY = m_stInspInfo.RecipeInfo.stFocus.stOpticalCenterOpt.stSpec_Min[Spec_OC_Y].bEnable;
	BOOL bMaxUseY = m_stInspInfo.RecipeInfo.stFocus.stOpticalCenterOpt.stSpec_Max[Spec_OC_Y].bEnable;

	int iMinDevY  = m_stInspInfo.RecipeInfo.stFocus.stOpticalCenterOpt.stSpec_Min[Spec_OC_Y].iValue;
	int iMaxDevY  = m_stInspInfo.RecipeInfo.stFocus.stOpticalCenterOpt.stSpec_Max[Spec_OC_Y].iValue;


	m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlignData.iOC_X = m_stInspInfo.CamInfo[nParaIdx].stFocus.stOpticalCenterData.iStandPosDevX;
	m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlignData.iOC_Y = m_stInspInfo.CamInfo[nParaIdx].stFocus.stOpticalCenterData.iStandPosDevY;

	// 광축 판정
	m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlignData.nResultX = m_tm_Test.Get_MeasurmentData(bMinUseX, bMaxUseX, iMinDevX, iMaxDevX, m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlignData.iOC_X);
	m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlignData.nResultY = m_tm_Test.Get_MeasurmentData(bMinUseY, bMaxUseY, iMinDevY, iMaxDevY, m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlignData.iOC_Y);

	// 로테이트
	BOOL bMinUseR = m_stInspInfo.RecipeInfo.stFocus.stRotateOpt.stSpec_Min.bEnable;
	BOOL bMaxUseR = m_stInspInfo.RecipeInfo.stFocus.stRotateOpt.stSpec_Max.bEnable;

	double dbMinDevR = m_stInspInfo.RecipeInfo.stFocus.stRotateOpt.stSpec_Min.dbValue;
	double dbMaxDevR = m_stInspInfo.RecipeInfo.stFocus.stRotateOpt.stSpec_Max.dbValue;

	m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlignData.dbRoatae = m_stInspInfo.CamInfo[nParaIdx].stFocus.stRotateData.dbValue;

	// 로테이트 판정
	m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlignData.nResultR = m_tm_Test.Get_MeasurmentData(bMinUseR, bMaxUseR, dbMinDevR, dbMaxDevR, m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlignData.dbRoatae);

	// 최종 판정
	m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlignData.nResult = m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlignData.nResultX & m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlignData.nResultY & m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlignData.nResultR;

	if (TR_Fail == m_stInspInfo.CamInfo[nParaIdx].stFocus.stOpticalCenterData.nDetectResult || TR_Fail == m_stInspInfo.CamInfo[nParaIdx].stFocus.stRotateData.nDetectResult)
	{
		m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlignData.nResult = TR_Fail;
	}

	TestResultView(pStepInfo->StepList[nStepIdx].nTestItem, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stFocus.stActiveAlignData, Foc_AA);
	
	// SFR
	m_tm_Test.SFRFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stImageBuf[nParaIdx].lpwImage_16bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stFocus.stSFROpt, m_stInspInfo.CamInfo[nParaIdx].stFocus.stSFRData);
	
	TestResultView(pStepInfo->StepList[nStepIdx].nTestItem, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stFocus.stTorqueData, Foc_AA_Torque);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Foc_Pro_Torque
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/7/5 - 10:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::_TI_Foc_Pro_Torque(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;
	
	m_stInspInfo.CamInfo[nParaIdx].stFocus.stTorqueData.nResult = TR_Pass;

	for (UINT nIdx = 0; nIdx <= Spec_Tor_D; nIdx++)
	{
		BOOL bMinUseX = m_stInspInfo.RecipeInfo.stFocus.stTorqueOpt.stSpec_Min[nIdx].bEnable;
		BOOL bMaxUseX = m_stInspInfo.RecipeInfo.stFocus.stTorqueOpt.stSpec_Max[nIdx].bEnable;

		double dbMinDevX = m_stInspInfo.RecipeInfo.stFocus.stTorqueOpt.stSpec_Min[nIdx].dbValue;
		double dbMaxDevX = m_stInspInfo.RecipeInfo.stFocus.stTorqueOpt.stSpec_Max[nIdx].dbValue;

		m_stInspInfo.CamInfo[nParaIdx].stFocus.stTorqueData.nEachResult[nIdx] = m_tm_Test.Get_MeasurmentData(bMinUseX, bMaxUseX, dbMinDevX, dbMaxDevX, m_stInspInfo.CamInfo[nParaIdx].stFocus.stTorqueData.dbValue[nIdx]);

		if (TR_Fail == m_stInspInfo.CamInfo[nParaIdx].stFocus.stTorqueData.nEachResult[nIdx])
		{
			m_stInspInfo.CamInfo[nParaIdx].stFocus.stTorqueData.nResult = TR_Fail;
			m_stInspInfo.CamInfo[nParaIdx].stFocus.stTorqueData.enStatus[nIdx] = Torque_Init;
		}
		else
		{
			m_stInspInfo.CamInfo[nParaIdx].stFocus.stTorqueData.enStatus[nIdx] = Torque_Pass;
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: MES_CheckBarcodeValidation
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/4 - 16:25
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::MES_CheckBarcodeValidation(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::MES_CheckBarcodeValidation(nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: MES_SendInspectionData
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/4 - 16:39
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::MES_SendInspectionData(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	if (enOperateMode::OpMode_Production == m_stInspInfo.OperateMode)
	{
		lReturn = MES_MakeInspecData_Foc(nParaIdx);

		// MES 로그 남기기
		BOOL bJudge = (TR_Pass == m_stInspInfo.Judgment_All) ? TRUE : FALSE;

		SYSTEMTIME tmLocal;
		GetLocalTime(&tmLocal);

		// MES 로그 남기기
		//m_FileMES.SaveMESFile(m_stInspInfo.CamInfo[0].szBarcode, m_stInspInfo.CamInfo[0].nMES_TryCnt, bJudge, &m_stInspInfo.CamInfo[0].TestTime.tmStart_Loading);

		CString szBartcode;

		// MES
		if (Model_MRA2 == m_stInspInfo.RecipeInfo.ModelType)
		{
			szBartcode = m_stInspInfo.CamInfo[Para_Left].szBarcode + _T("|") + m_stInspInfo.CamInfo[Para_Right].szBarcode;
		}
		else
		{
			szBartcode = m_stInspInfo.CamInfo[nParaIdx].szBarcode;
		}

		m_FileMES.SaveMESFile(szBartcode, m_stInspInfo.CamInfo[nParaIdx].nMES_TryCnt, bJudge, &tmLocal);

		// 로그
		m_FileTestLog.SaveLOGFile(m_stInspInfo.Path.szReport, szBartcode, m_stInspInfo.CamInfo[nParaIdx].nMES_TryCnt, bJudge, &tmLocal);
	}

	Sleep(1000);

	return lReturn;
}

//=============================================================================
// Method		: MES_MakeInspecData_Foc
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/9 - 19:16
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::MES_MakeInspecData_Foc(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;

	INT_PTR iStepCnt = m_stInspInfo.RecipeInfo.StepInfo.GetCount();
	DWORD dwElapTime = 0;
	int iCamCount	 = g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt;

	for (int camCnt = 0; camCnt < iCamCount; camCnt++)
	{	
		// * 설정된 스텝 진행
		m_stMesData.Reset(nParaIdx);
		for (INT nStepIdx = 0; nStepIdx < iStepCnt; nStepIdx++)
		{
			switch (pStepInfo->StepList[nStepIdx].nTestItem)
			{
			case TI_Foc_Fn_ECurrent:
				m_TestMgr_TestMes.Current_DataSave_Foc(nParaIdx, &m_stMesData);
				break;
			case TI_Foc_Fn_OpticalCenter:
				m_TestMgr_TestMes.OpticalCenter_DataSave_Foc(nParaIdx, &m_stMesData);
				break;
			case TI_Foc_Fn_SFR:
				m_TestMgr_TestMes.SFR_DataSave_Foc(nParaIdx, &m_stMesData);
				break;
			case TI_Foc_Fn_Rotation:
				m_TestMgr_TestMes.Rotate_DataSave_Foc(nParaIdx, &m_stMesData);
				break;
			case TI_Foc_Fn_Stain:
				m_TestMgr_TestMes.Stain_DataSave_Foc(nParaIdx, &m_stMesData);
				break;
			case TI_Foc_Fn_DefectPixel:
				m_TestMgr_TestMes.Defectpixel_DataSave_Foc(nParaIdx, &m_stMesData);
				break;
			case TI_Foc_Re_TorqueCheck:
				m_TestMgr_TestMes.Torque_DataSave_Foc(nParaIdx, &m_stMesData);
				break;
			default:
				break;
			}
		}
	}

	m_FileMES.Reset();
	m_FileTestLog.Reset();

	BOOL bJudge = (TR_Pass == m_stInspInfo.Judgment_All) ? TRUE : FALSE;

#ifdef USE_BARCODE_SCANNER
	m_FileMES.Set_EquipmnetID(m_stInspInfo.CamInfo[0].szBarcode);
#else
	m_FileMES.Set_EquipmnetID(m_stInspInfo.szEquipmentID);
#endif
	m_FileMES.Set_Path(m_stOption.MES.szPath_MESLog);
	m_FileMES.Set_Barcode(m_stInspInfo.CamInfo[nParaIdx].szBarcode, m_stInspInfo.CamInfo[nParaIdx].nMES_TryCnt, bJudge, &m_stInspInfo.CamInfo[0].TestTime.tmStart_Loading);

	m_FileTestLog.Set_EquipmnetID(m_stInspInfo.szEquipmentID);
	m_FileTestLog.Set_Path(m_stOption.MES.szPath_MESLog);
	m_FileTestLog.Set_Barcode(m_stInspInfo.CamInfo[nParaIdx].szBarcode, m_stInspInfo.CamInfo[nParaIdx].nMES_TryCnt, bJudge, &m_stInspInfo.CamInfo[0].TestTime.tmStart_Loading);


	CString		NAME;				// 항목 명칭	
	CString		VALUE;				// 데이터
	BOOL		JUDGE = TRUE;		// 판정

	int nHeaderCnt = 0;

	for (int camCnt = 0; camCnt < iCamCount; camCnt++)
	{
		nHeaderCnt = 0;
		for (int t = 0; t < MesDataIdx_Foc_MAX; t++)
		{
			if (!m_stMesData.szMesTestData[nParaIdx][t].IsEmpty())
			{
				CString strData;
				CString szBlockData;
				CString szValueData;
				CString szResultData;

				strData = m_stMesData.szMesTestData[nParaIdx][t];
				//- ,를 찾고
				int nNum = 0;
				int nStartNum = 0;
				int nDataNum = 0;

				int nDataCnt = 0;
				while (true)
				{
					nNum = strData.Find(',', nStartNum);
					if (nNum != -1)
					{
						szBlockData = strData.Mid(nStartNum, nNum);

						nDataNum = szBlockData.Find(':', 0);
						szValueData = szBlockData.Mid(0, nDataNum);
						szResultData = szBlockData.Mid(nDataNum + 1, szBlockData.GetLength());

						m_FileMES.Add_ItemData(g_szMESHeader_Foc[nHeaderCnt], szValueData, _ttoi(szResultData));
						m_FileTestLog.Add_ItemData(g_szMESHeader_Foc[nHeaderCnt], szValueData, _ttoi(szResultData));
						nHeaderCnt++;

						nStartNum = nNum + 1;
						nDataCnt++;
					}
					else
					{
						break;
					}
				}

				szBlockData = strData.Mid(nStartNum, strData.GetLength());
				nDataNum = szBlockData.Find(':', 0);
				szValueData = szBlockData.Mid(0, nDataNum);
				szResultData = szBlockData.Mid(nDataNum + 1, szBlockData.GetLength());
				m_FileMES.Add_ItemData(g_szMESHeader_Foc[nHeaderCnt], szValueData, _ttoi(szResultData));
				m_FileTestLog.Add_ItemData(g_szMESHeader_Foc[nHeaderCnt], szValueData, _ttoi(szResultData));
				nHeaderCnt++;
				nDataCnt++;

				for (UINT k = nDataCnt; k < m_stMesData.nMesDataNum[t]; k++)
				{
					m_FileMES.Add_ItemData(m_stMesData.szDataName[t], _T(""), 1);
					m_FileTestLog.Add_ItemData(g_szMESHeader_Foc[nHeaderCnt], _T(""), 1);
					nHeaderCnt++;
				}
			}
			else
			{
				CString strHeader;
				for (UINT k = 0; k < m_stMesData.nMesDataNum[t]; k++)
				{

					m_FileMES.Add_ItemData(g_szMESHeader_Foc[nHeaderCnt], _T(""), 1);

					strHeader.Format(_T("%s"), g_szMESHeader_Foc[nHeaderCnt]);
					if (strHeader == _T("Reserved"))
					{
						m_FileTestLog.Add_ItemData(g_szMESHeader_Foc[nHeaderCnt], _T(""), 1);
					}
					else{
						m_FileTestLog.Add_ItemData(g_szMESHeader_Foc[nHeaderCnt], _T("X"), 1);
					}
					nHeaderCnt++;
				}
			}
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: OnPopupMessageTest_All
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enResultCode nResultCode
// Qualifier	:
// Last Update	: 2016/7/21 - 18:59
// Desc.		:
//=============================================================================
void CTestManager_EQP_Foc::OnPopupMessageTest_All(__in enResultCode nResultCode)
{
	CTestManager_EQP::OnPopupMessageTest_All(nResultCode);
}

//=============================================================================
// Method		: OnMotion_MoveToTestPos
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/10/19 - 20:42
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnMotion_MoveToTestPos(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_MoveToTestPos(nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: OnMotion_MoveToStandbyPos
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/15 - 9:58
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnMotion_MoveToStandbyPos(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_MoveToStandbyPos(nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: OnMotion_MoveToUnloadPos
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:41
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnMotion_MoveToUnloadPos(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_MoveToUnloadPos(nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: OnMotion_Origin
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/11/15 - 3:27
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnMotion_Origin()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_Origin();

	return lReturn;
}

//=============================================================================
// Method		: OnMotion_LoadProduct
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/11/15 - 3:05
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnMotion_LoadProduct()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_LoadProduct();

	return lReturn;
}

//=============================================================================
// Method		: OnMotion_UnloadProduct
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/11/15 - 3:16
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnMotion_UnloadProduct()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_UnloadProduct();

	return lReturn;
}

//=============================================================================
// Method		: OnMotion_SteCal_Test
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/13 - 13:48
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnMotion_SteCal_Test(__in UINT nParaIdx /*= 0*/)
{
	return RC_OK;
}

//=============================================================================
// Method		: OnResetInfo_Measurment
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/6/29 - 19:12
// Desc.		:
//=============================================================================
void CTestManager_EQP_Foc::OnResetInfo_Measurment(__in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::OnResetInfo_Measurment(nParaIdx);
}

//=============================================================================
// Method		: OnDIn_DetectSignal
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BYTE byBitOffset
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2018/2/1 - 17:22
// Desc.		:
//=============================================================================
void CTestManager_EQP_Foc::OnDIn_DetectSignal(__in BYTE byBitOffset, __in BOOL bOnOff)
{
//	CTestManager_EQP::OnDIn_DetectSignal(byBitOffset, bOnOff);

	OnDIn_DetectSignal_Foc(byBitOffset, bOnOff);
}

 //=============================================================================
// Method		: OnDIn_DetectSignal_Foc
 // Access		: virtual protected  
 // Returns		: void
 // Parameter	: __in BYTE byBitOffset
 // Parameter	: __in BOOL bOnOff
 // Qualifier	:
// Last Update	: 2018/3/5 - 12:53
 // Desc.		:
 //=============================================================================
void CTestManager_EQP_Foc::OnDIn_DetectSignal_Foc(__in BYTE byBitOffset, __in BOOL bOnOff)
 {
	enDI_Focusing nDI_Signal = (enDI_Focusing)byBitOffset;
	BOOL bStatus = TRUE;

	switch (nDI_Signal)
	{
	case DI_Fo_00_MainPower:
		if (FALSE == bOnOff)
		{
			OnDIn_MainPower();
		}
		break;

	case DI_Fo_01_EMO:
		if (FALSE == bOnOff)
		{
			OnDIn_EMO();
		}
		break;

	case DI_Fo_02_AreaSensor:
		if (FALSE == bOnOff)
		{
			OnDIn_AreaSensor();
		}
		break;

	case DI_Fo_03_DoorTSensor:
	case DI_Fo_04_DoorBSensor:
		if (FALSE == bOnOff)
		{
			OnDIn_DoorSensor();
		}
		break;

	case DI_Fo_08_StartL:
	case DI_Fo_09_StartR:
		if (bOnOff)
		{
			// 양수 버튼 감지
			if (m_Device.DigitalIOCtrl.Get_DI_Status(DI_Fo_08_StartL) == TRUE && m_Device.DigitalIOCtrl.Get_DI_Status(DI_Fo_09_StartR) == TRUE)
			{
				if ((TRUE == OnFoc_ChangeCamFlag()) && (FALSE == m_bFlag_CameraChange))
				{
					Increase_ConsumCount();
					m_bFlag_CameraChange = TRUE;
				}
				else
				{
					if (Para_Right != m_stInspInfo.nTestPara)
					{
						StartOperation_LoadUnload(Product_Load);
					}
				}
			}

			if (m_Device.DigitalIOCtrl.Get_DI_Status(DI_Fo_08_StartL) == TRUE && m_Device.DigitalIOCtrl.Get_DI_Status(DI_Fo_09_StartR) == FALSE)
			{
				for (UINT nItem = 0; nItem < TI_Foc_MaxEnum; nItem++)
				{
					if (TRUE == GetLoopItem_Info(nItem))
					{
						SetLoopItem_Info(nItem, TestLoop_NotUse);
						break;
					}
				}
			}
		}
		break;

	case DI_Fo_10_Stop:
		if (bOnOff)
		{
			StopProcess_Test_All();
		}
		break;
	case DI_Fo_11_Master:
		if (bOnOff)
		{
			SetOperateMode(OpMode_Master);

			StartOperation_LoadUnload(Product_Load);
		}
		break;
	case DI_Fo_12_FixOn:
		if (bOnOff)
		{
#ifndef MOTION_NOT_USE
			m_MotionSequence.OnActionTesting_Foc_Fix(ON);
#endif
		}
		break;
	case DI_Fo_13_FixOff:
		if (bOnOff)
		{
#ifndef MOTION_NOT_USE
			m_MotionSequence.OnActionTesting_Foc_Fix(OFF);
#endif
		}
		break;
	case DI_Fo_16_FixOnSensorL:
	case DI_Fo_17_FixOnSensorR:

		if (Model_MRA2 == m_stInspInfo.RecipeInfo.ModelType)
			bStatus = m_Device.DigitalIOCtrl.Get_DI_Status(DI_Fo_16_FixOnSensorL) | m_Device.DigitalIOCtrl.Get_DI_Status(DI_Fo_17_FixOnSensorR);
		else
			bStatus = m_Device.DigitalIOCtrl.Get_DI_Status(DI_Fo_16_FixOnSensorL) & m_Device.DigitalIOCtrl.Get_DI_Status(DI_Fo_17_FixOnSensorR);

		if (bOnOff && bStatus)
		{
			m_MotionSequence.OnActionLampControl(DO_Fo_12_FixOnLemp, IO_SignalT_SetOn);
		}
		else
		{
			m_MotionSequence.OnActionLampControl(DO_Fo_12_FixOnLemp, IO_SignalT_SetOff);
		}

		break;
	case DI_Fo_18_FixOffSensorL:
	case DI_Fo_19_FixOffSensorR:
		
		if (Model_MRA2 == m_stInspInfo.RecipeInfo.ModelType)
			bStatus = m_Device.DigitalIOCtrl.Get_DI_Status(DI_Fo_16_FixOnSensorL) | m_Device.DigitalIOCtrl.Get_DI_Status(DI_Fo_17_FixOnSensorR);
		else
			bStatus = m_Device.DigitalIOCtrl.Get_DI_Status(DI_Fo_16_FixOnSensorL) & m_Device.DigitalIOCtrl.Get_DI_Status(DI_Fo_17_FixOnSensorR);
		
		if (bOnOff && bStatus)
		{
			m_MotionSequence.OnActionLampControl(DO_Fo_13_FixOffLemp, IO_SignalT_SetOn);
		}
		else
		{
			m_MotionSequence.OnActionLampControl(DO_Fo_13_FixOffLemp, IO_SignalT_SetOff);
		}

		break;
	default:
		break;
	}
 }

//=============================================================================
// Method		: OnDIO_InitialSignal
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/16 - 14:20
// Desc.		:
//=============================================================================
void CTestManager_EQP_Foc::OnDIO_InitialSignal()
{
	CTestManager_EQP::OnDIO_InitialSignal();


}

//=============================================================================
// Method		: OnDIn_MainPower
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 19:48
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnDIn_MainPower()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_MainPower();



	return lReturn;
}

//=============================================================================
// Method		: OnDIn_EMO
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 20:12
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnDIn_EMO()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_EMO();



	return lReturn;
}

//=============================================================================
// Method		: OnDIn_AreaSensor
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 20:12
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnDIn_AreaSensor()
{
	LRESULT lReturn = RC_OK;

	//lReturn = CTestManager_EQP::OnDIn_AreaSensor();

	if (TRUE == m_stOption.Inspector.bUseAreaSensor_Err)
	{
		if (IsTesting())
		{
			//m_bFlag_ReadyTest = FALSE;

			// 모터 정지??
			OnMotion_EStop();

			OnLog_Err(_T("I/O : Detected Area Sesnor !!"));
			OnAddAlarm_F(_T("I/O : Detected Area Sesnor  !!"));

			// 검사 중지??
			StopProcess_Test_All();
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: OnDIn_DoorSensor
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 20:12
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnDIn_DoorSensor()
{
	LRESULT lReturn = RC_OK;

	// lReturn = CTestManager_EQP::OnDIn_DoorSensor();

	if (m_stOption.Inspector.bUseDoorOpen_Err)
	{
		if (IsTesting())
		{
			//m_bFlag_ReadyTest = FALSE;

			// 모터 정지??
			OnMotion_EStop();

			OnLog_Err(_T("I/O : Detected Area Sesnor !!"));
			OnAddAlarm_F(_T("I/O : Detected Area Sesnor  !!"));

			// 검사 중지??
			StopProcess_Test_All();
		}
	}



	return lReturn;
}

//=============================================================================
// Method		: OnDIn_JIGCoverCheck
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 19:41
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnDIn_JIGCoverCheck()
{
	LRESULT lReturn = RC_OK;

	//lReturn = CTestManager_EQP::OnDIn_JIGCoverCheck();

	return lReturn;
}

//=============================================================================
// Method		: OnDIn_Start
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/3/17 - 10:14
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnDIn_Start()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_Start();



	return lReturn;
}

//=============================================================================
// Method		: OnDIn_Stop
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/3/17 - 10:14
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnDIn_Stop()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_Stop();



	return lReturn;
}

//=============================================================================
// Method		: OnDIn_CheckSafetyIO
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 18:36
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnDIn_CheckSafetyIO()
{
	LRESULT lReturn = RC_OK;

	//lReturn = CTestManager_EQP::OnDIn_CheckSafetyIO();

	// * 안전 기능 체크 (Main Power, EMO, Area Sensor, Door Sensor)

	if (FALSE == m_stInspInfo.byDIO_DI[DI_Fo_00_MainPower])
	{
		lReturn = RC_DIO_Err_MainPower;
	}

	if (FALSE == m_stInspInfo.byDIO_DI[DI_Fo_01_EMO])
	{
		lReturn = RC_DIO_Err_EMO;
	}

	if (FALSE == m_stInspInfo.byDIO_DI[DI_Fo_02_AreaSensor])
	{
		if (TRUE == m_stOption.Inspector.bUseDoorOpen_Err)
		{
			lReturn = RC_DIO_Err_AreaSensor;
		}
	}

	if ((FALSE == m_stInspInfo.byDIO_DI[DI_Fo_03_DoorTSensor]) || (FALSE == m_stInspInfo.byDIO_DI[DI_Fo_04_DoorBSensor]))
	{
		if (TRUE == m_stOption.Inspector.bUseDoorOpen_Err)
		{
			lReturn = RC_DIO_Err_DoorSensor;
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: OnDIn_CheckJIGCoverStatus
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/3/17 - 10:21
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnDIn_CheckJIGCoverStatus()
{
	LRESULT lReturn = RC_OK;

	//lReturn = CTestManager_EQP::OnDIn_CheckJIGCoverStatus();

	// * JIG Cover 체크 (Clear : 커버 덮은 상태, Set : 커버 열린 상태)
	return lReturn;
}

//=============================================================================
// Method		: OnDOut_BoardPower
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bOn
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/6 - 16:55
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnDOut_BoardPower(__in BOOL bOn, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_BoardPower(bOn, nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: OnDOut_FluorescentLamp
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bOn
// Qualifier	:
// Last Update	: 2018/3/17 - 10:22
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnDOut_FluorescentLamp(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_FluorescentLamp(bOn);

	return lReturn;
}

//=============================================================================
// Method		: OnDOut_StartLamp
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bOn
// Qualifier	:
// Last Update	: 2018/3/17 - 10:22
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnDOut_StartLamp(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_StartLamp(bOn);

	return lReturn;
}

//=============================================================================
// Method		: OnDOut_StopLamp
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bOn
// Qualifier	:
// Last Update	: 2018/3/17 - 10:22
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnDOut_StopLamp(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_StopLamp(bOn);

	return lReturn;
}

//=============================================================================
// Method		: OnDOut_TowerLamp
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in enLampColor nLampColor
// Parameter	: __in BOOL bOn
// Qualifier	:
// Last Update	: 2018/3/17 - 10:22
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnDOut_TowerLamp(__in enLampColor nLampColor, __in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	//lReturn = CTestManager_EQP::OnDOut_TowerLamp(nLampColor, bOn);

	enum_IO_SignalType enSignalType;

	if (ON == bOn)
	{
		enSignalType = IO_SignalT_SetOn;
	}
	else
	{
		enSignalType = IO_SignalT_SetOff;
	}

	switch (nLampColor)
	{
	case Lamp_Red:
		lReturn = m_Device.DigitalIOCtrl.Set_DO_Status(DO_Fo_18_TowerLampRed, enSignalType);
		break;
	case Lamp_Yellow:
		lReturn = m_Device.DigitalIOCtrl.Set_DO_Status(DO_Fo_19_TowerLampYellow, enSignalType);
		break;
	case Lamp_Green:
		lReturn = m_Device.DigitalIOCtrl.Set_DO_Status(DO_Fo_20_TowerLampGreen, enSignalType);
		break;
	case Lamp_All:
		for (UINT nIdx = 0; nIdx < 3; nIdx++)
		{
			lReturn = m_Device.DigitalIOCtrl.Set_DO_Status(DO_Fo_18_TowerLampRed + nIdx, enSignalType);

			if (RC_OK != lReturn)
				break;
		}
		break;
	default:
		break;
	}

	return lReturn;
}

//=============================================================================
// Method		: OnDOut_Buzzer
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bOn
// Qualifier	:
// Last Update	: 2018/3/17 - 10:22
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnDOut_Buzzer(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_Buzzer(bOn);

	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_EEPROM_Write
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/2/12 - 14:37
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnDAQ_EEPROM_Write(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	switch (m_stInspInfo.RecipeInfo.ModelType)
	{
	case Model_OMS_Entry:
		lReturn = OnDAQ_EEPROM_OMS_Entry_Foc(nIdxBrd);
		break;

	case Model_OMS_Front:
		lReturn = OnDAQ_EEPROM_OMS_Front_Foc(nIdxBrd);
		break;

	case Model_MRA2:
		lReturn = OnDAQ_EEPROM_MRA2_Foc(nIdxBrd);
		break;

	case Model_IKC:
		lReturn = OnDAQ_EEPROM_IKC_Foc(nIdxBrd);
		break;

 	default:
		break;
 	}

	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_EEPROM_OMS_Entry_Foc
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/2/13 - 17:17
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnDAQ_EEPROM_OMS_Entry_Foc(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;


	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_EEPROM_OMS_Front_Foc
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/3/17 - 10:23
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnDAQ_EEPROM_OMS_Front_Foc(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;


	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_EEPROM_MRA2_Foc
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/3/17 - 10:23
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnDAQ_EEPROM_MRA2_Foc(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;


	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_EEPROM_IKC_Foc
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/3/17 - 10:23
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnDAQ_EEPROM_IKC_Foc(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;


	return lReturn;
}

//=============================================================================
// Method		: OnManualSequence
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaID
// Parameter	: __in enParaManual enFunc
// Qualifier	: 메뉴얼 시퀀스 동작 테스트
// Last Update	: 2017/10/27 - 18:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnManualSequence(__in UINT nParaID, __in enParaManual enFunc)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnManualSequence(nParaID, enFunc);

	return lReturn;
}

//=============================================================================
// Method		: OnManualTestItem
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaID
// Parameter	: __in UINT nStepIdx
// Qualifier	:
// Last Update	: 2017/10/27 - 18:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnManualTestItem(__in UINT nParaID, __in UINT nStepIdx)
{
    LRESULT lReturn = RC_OK;

	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;
	INT_PTR iStepCnt = m_stInspInfo.RecipeInfo.StepInfo.GetCount();

	StartTest_Focusing_TestItem(pStepInfo->StepList[nStepIdx].nTestItem, nStepIdx, nParaID);

    return lReturn;
}

//=============================================================================
// Method		: OnCheck_DeviceComm
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnCheck_DeviceComm()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnCheck_DeviceComm();
	
	return lReturn;
}

//=============================================================================
// Method		: OnCheck_SaftyJIG
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnCheck_SaftyJIG()
{
	LRESULT lReturn = RC_OK;

	//lReturn = CTestManager_EQP::OnCheck_SaftyJIG();

	return lReturn;
}

//=============================================================================
// Method		: OnCheck_RecipeInfo
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_Foc::OnCheck_RecipeInfo()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnCheck_RecipeInfo();

	return lReturn;
}

//=============================================================================
// Method		: OnMakeReport
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 14:25
// Desc.		:
//=============================================================================
void CTestManager_EQP_Foc::OnJugdement_And_Report(__in UINT nParaIdx)
{
	CFile_Report fileReport;

	//fileReport.SaveFinalizeResult2DCal(nParaIdx, m_stInspInfo.CamInfo[nParaIdx].szBarcode, m_stInspInfo.CamInfo[nParaIdx].st2D_Result);
}

//=============================================================================
// Method		: OnJugdement_And_Report_All
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/17 - 10:23
// Desc.		:
//=============================================================================
void CTestManager_EQP_Foc::OnJugdement_And_Report_All()
{
	OnUpdateYield();
	OnSaveWorklist();
}

//=============================================================================
// Method		: CreateTimer_UpdateUI
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in DWORD DueTime
// Parameter	: __in DWORD Period
// Qualifier	:
// Last Update	: 2017/11/8 - 14:17
// Desc.		: 파워 서플라이 모니터링
//=============================================================================
void CTestManager_EQP_Foc::CreateTimer_UpdateUI(__in DWORD DueTime /*= 5000*/, __in DWORD Period /*= 250*/)
{
// 		__super::CreateTimer_UpdateUI(5000, 2500);
}

//=============================================================================
// Method		: DeleteTimer_UpdateUI
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/17 - 10:23
// Desc.		:
//=============================================================================
void CTestManager_EQP_Foc::DeleteTimer_UpdateUI()
{
// 		__super::DeleteTimer_UpdateUI();
}

//=============================================================================
// Method		: OnMonitor_TimeCheck
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 13:28
// Desc.		:
//=============================================================================
void CTestManager_EQP_Foc::OnMonitor_TimeCheck()
{
	//CTestManager_EQP::OnMonitor_TimeCheck();

#ifndef NO_CHECK_ELAP_TIME
	// 현재 시간 체크
	m_dwTimeCheck = timeGetTime();

	// 개별 채널 검사 시간 체크
	for (UINT nUnitIdx = 0; nUnitIdx < USE_CHANNEL_CNT; nUnitIdx++)
	{
		// 검사 중이면
		if (TP_Testing == m_stInspInfo.GetTestProgress(nUnitIdx))
		{
			m_stInspInfo.CamInfo[nUnitIdx].TestTime.Get_Duration_Test(m_dwTimeCheck);

			OnUpdate_ElapTime_TestUnit(nUnitIdx);
		}
	}

	if ((TP_Loading <= m_stInspInfo.GetTestStatus()) && (m_stInspInfo.GetTestStatus() <= TP_Unloading))
	{
		for (UINT nUnitIdx = 0; nUnitIdx < USE_CHANNEL_CNT; nUnitIdx++)
		{
			m_stInspInfo.CamInfo[nUnitIdx].TestTime.Get_Duration_Cycle(m_dwTimeCheck);
		}

		OnUpdate_ElapTime_Cycle();
	}


#endif

}

//=============================================================================
// Method		: OnMonitor_UpdateUI
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 21:19
// Desc.		: Power Supply 모니터링
//=============================================================================
void CTestManager_EQP_Foc::OnMonitor_UpdateUI()
{
	CTestManager_EQP::OnMonitor_UpdateUI();
	
}

//=============================================================================
// Method		: OnSaveWorklist
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/5/10 - 20:24
// Desc.		:
//=============================================================================
void CTestManager_EQP_Foc::OnSaveWorklist()
{
// 	m_stInspInfo.WorklistInfo.Reset();
// 
// 	ST_CamInfo* pstCam = NULL;
// 
// 	CString szText;
// 	CString szTime;
// 	SYSTEMTIME lcTime;
// 
// 	GetLocalTime(&lcTime);
// 	szTime.Format(_T("'%04d-%02d-%02d %02d:%02d:%02d.%03d"), lcTime.wYear, lcTime.wMonth, lcTime.wDay,
// 		lcTime.wHour, lcTime.wMinute, lcTime.wSecond, lcTime.wMilliseconds);
// 
// 	for (UINT nChIdx = 0; nChIdx < USE_CHANNEL_CNT; nChIdx++)
// 	{
// 		if (FALSE == m_stInspInfo.bTestEnable[nChIdx])
// 			continue;
// 
// 		pstCam = &m_stInspInfo.CamInfo[nChIdx];
// 
// 		m_stInspInfo.WorklistInfo.Time = szTime;
// 		m_stInspInfo.WorklistInfo.EqpID = m_stInspInfo.szEquipmentID;	// g_szInsptrSysType[m_InspectionType];
// 		m_stInspInfo.WorklistInfo.SWVersion;
// 		m_stInspInfo.WorklistInfo.Model = m_stInspInfo.RecipeInfo.szModelCode;
// 		m_stInspInfo.WorklistInfo.Barcode = pstCam->szBarcode;
// 		m_stInspInfo.WorklistInfo.Result = g_TestResult[pstCam->nJudgment].szText;
// 
// 		szText.Format(_T("%d"), nChIdx + 1);
// 		m_stInspInfo.WorklistInfo.Socket = szText;
// 
// 
// 		m_stInspInfo.WorklistInfo.MakeItemz();
// 
// 		// 파일 저장
// 		m_Worklist.Save_FinalResult_List(m_stInspInfo.Path.szReport, &pstCam->TestTime.tmStart_Loading, &m_stInspInfo.WorklistInfo);
// 
// 		// UI 표시
// 		OnInsertWorklist();
// 
// 	}// End of for
}

//=============================================================================
// Method		: SetLoopItem_Info
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nTestItemID
// Parameter	: __in enTestLoop enUse
// Qualifier	:
// Last Update	: 2018/3/17 - 10:23
// Desc.		:
//=============================================================================
void CTestManager_EQP_Foc::SetLoopItem_Info(__in UINT nTestItemID, __in enTestLoop enUse)
{
	switch (enUse)
	{
	case TestLoop_NotUse:
		m_bFlag_LoopItem[nTestItemID] = FALSE;
		break;
	case TestLoop_Use:
		m_bFlag_LoopItem[nTestItemID] = TRUE;
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: GetLoopItem_Info
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in UINT nTestItemID
// Qualifier	:
// Last Update	: 2018/3/17 - 10:23
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP_Foc::GetLoopItem_Info(__in UINT nTestItemID)
{
	return m_bFlag_LoopItem[nTestItemID];
}

//=============================================================================
// Method		: OnInitialize
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/26 - 6:03
// Desc.		:
//=============================================================================
void CTestManager_EQP_Foc::OnInitialize()
{
	CTestManager_EQP::OnInitialize();
}

//=============================================================================
// Method		: OnFinalize
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/26 - 6:03
// Desc.		:
//=============================================================================
void CTestManager_EQP_Foc::OnFinalize()
{
	CTestManager_EQP::OnFinalize();

}

//=============================================================================
// Method		: SetPermissionMode
// Access		: public  
// Returns		: void
// Parameter	: __in enPermissionMode nAcessMode
// Qualifier	:
// Last Update	: 2016/11/9 - 18:52
// Desc.		:
//=============================================================================
void CTestManager_EQP_Foc::SetPermissionMode(__in enPermissionMode nAcessMode)
{
	CTestManager_EQP::SetPermissionMode(nAcessMode);
}


//=============================================================================
// Method		: SetOperateMode
// Access		: virtual public  
// Returns		: void
// Parameter	: __in enOperateMode nOperMode
// Qualifier	:
// Last Update	: 2018/8/23 - 11:22
// Desc.		:
//=============================================================================
void CTestManager_EQP_Foc::SetOperateMode(__in enOperateMode nOperMode)
{
	if (FALSE == IsTesting())
	{
		m_stInspInfo.OperateMode = nOperMode;
	}

	// 실제 데이터 세팅
	if (OpMode_Master == m_stInspInfo.OperateMode)
	{
		for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
		{
			m_stInspInfo.CamInfo[nIdx].TestInfo.RemoveAll();
			m_stInspInfo.CamInfo[nIdx].TestInfo.SetStepInfo_TestItemInfo(&m_stInspInfo.RecipeInfo.MasterInfo, &m_stInspInfo.RecipeInfo.TestItemInfo);
		}
	}
	else
	{
		for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
		{
			m_stInspInfo.CamInfo[nIdx].TestInfo.RemoveAll();
			m_stInspInfo.CamInfo[nIdx].TestInfo.SetStepInfo_TestItemInfo(&m_stInspInfo.RecipeInfo.StepInfo, &m_stInspInfo.RecipeInfo.TestItemInfo);
		}
	}
}

//=============================================================================
// Method		: TestResultView
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nTestID
// Parameter	: __in UINT nPara
// Parameter	: __in LPVOID pParam
// Parameter	: __in enFocus_AAView enItem
// Qualifier	:
// Last Update	: 2018/3/9 - 10:04
// Desc.		:
//=============================================================================
void CTestManager_EQP_Foc::TestResultView(__in UINT nTestID, __in UINT nPara, __in LPVOID pParam, __in enFocus_AAView enItem /*= Foc_AA_MaxNum*/)
{
	m_Test_ResultDataView.TestResultView_Foc(nTestID, nPara, pParam, enItem);
}

//=============================================================================
// Method		: TestResultView_Reset
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nTestID
// Parameter	: __in UINT nPara
// Qualifier	:
// Last Update	: 2018/3/9 - 9:56
// Desc.		:
//=============================================================================
void CTestManager_EQP_Foc::TestResultView_Reset(__in UINT nTestID, __in UINT nPara)
{
	m_Test_ResultDataView.TestResultView_Reset_Foc(nTestID, nPara);

}
void CTestManager_EQP_Foc::OnReset_CamInfo(__in UINT nParaIdx /*= 0*/)
{
	m_stInspInfo.ResetCamInfo(nParaIdx);
}

//=============================================================================
// Method		: EachTest_ResultDataSave
// Access		: public  
// Returns		: void
// Parameter	: UINT nTestID
// Parameter	: UINT nPara
// Qualifier	:
// Last Update	: 2018/4/23 - 13:18
// Desc.		:
//=============================================================================
void CTestManager_EQP_Foc::EachTest_ResultDataSave(UINT nTestID, UINT nPara)
{
	ST_MES_TestItemLog m_MesData;
	CString str;
	SYSTEMTIME systime;

	GetLocalTime(&systime);

	//- 시간
	m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&systime);

	//- Model
	m_MesData.Model = m_stInspInfo.szRecipeName;

	//- SW Ver
	m_MesData.SWVersion = m_Worklist.GetSWVersion(g_szProgramName[m_InspectionType]);

	if (m_stInspInfo.CamInfo[nPara].szBarcode.IsEmpty())
	{
		m_MesData.Barcode = _T("No Barcode");
	}
	else
	{
		m_MesData.Barcode = m_stInspInfo.CamInfo[nPara].szBarcode;
	}

	//- Channel
	str.Format(_T("%d"), nPara);
	m_MesData.Socket = str;

	//- Result + Data
	UINT DataNum = 0;
	CString Data[100];

	CString strResult;
	UINT nResult = 0;

	CString strTestName;
	switch (nTestID)
	{
	case TI_Foc_Fn_ECurrent:
		m_TestMgr_TestMes.Current_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("Current");
		break;
	case TI_Foc_Fn_OpticalCenter:
		m_TestMgr_TestMes.OpticalCenter_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("OpticalCenter");
		break;
	case TI_Foc_Fn_SFR:
		m_TestMgr_TestMes.SFR_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("SFR");
		break;
	case TI_Foc_Fn_Rotation:
		m_TestMgr_TestMes.Rotate_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("Rotation");
		break;
	case TI_Foc_Fn_Stain:
		m_TestMgr_TestMes.Stain_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("Statin");
		break;
	case TI_Foc_Fn_DefectPixel:
		m_TestMgr_TestMes.Defectpixel_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("DefectPixel");
		break;
	default:
		break;
	}

	m_MesData.Result.Format(_T("%d"), nResult);

	m_Worklist.Save_TestItemLog_List(m_stInspInfo.Path.szReport, &systime, &m_MesData, strTestName);
}
