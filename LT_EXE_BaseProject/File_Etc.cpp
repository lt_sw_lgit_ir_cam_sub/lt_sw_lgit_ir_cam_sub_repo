#include "stdafx.h"
#include "File_Etc.h"

#include "CommonFunction.h"

CFile_Etc::CFile_Etc()
{
}

CFile_Etc::~CFile_Etc()
{
}

//=============================================================================
// Method		: SaveCornerPt
// Access		: public  
// Returns		: void
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_2DCal_CornerPt stCorner
// Parameter	: __in UINT nIndex
// Qualifier	:
// Last Update	: 2018/3/8 - 9:39
// Desc.		:
//=============================================================================
void CFile_Etc::SaveCornerPt(__in CString szBarcode, __in ST_2DCal_CornerPt stCorner, __in UINT nIndex)
{
	CString szMsg;
	CString szTemp;

	UINT nLine = 10;

	CFile File;
	CFileException e;

	szTemp = _T("Corner[Idx:0]");
	szMsg = szTemp;

	CString szFile;
	CString szDir;

	CString szOut;
	CString szText;
	CString szStatus;

	if (szBarcode.IsEmpty())
		szBarcode = _T("Test");

	szDir.Format(_T("%s\\%s"), m_szPath, szBarcode);
	MakeDirectory(szDir);

	szFile.Format(FNAME_CORNERPT, szDir, nIndex);
	if (!File.Open(szFile, CFile::modeCreate | CFile::modeWrite | CFile::shareDenyWrite, &e))
	{
		return;
	}
	
	szMsg = szStatus + _T("Corner[Idx:0],");
	for (int _a = 0; _a < MAX_2DCAL_Corner_Point; _a++)
	{
		if (_a != 0 && ((_a % nLine) == 0))
		{
			szMsg += _T("\r\n");
			TRACE(szMsg);
			szText.Format(_T("Corner[Idx:%d],"), _a);
			szMsg += szText;
		}

		szTemp.Format(_T("U:%3.1f, V:%3.1f"), stCorner.Position[_a].fU, stCorner.Position[_a].fV);
		szMsg += szTemp;
	}

	szOut += szMsg;

	File.SeekToEnd();
	File.Write(szOut.GetBuffer(), szOut.GetLength() * sizeof(TCHAR));
	File.Flush();
	szOut.ReleaseBuffer();

	File.Close();
}

//=============================================================================
// Method		: SaveCornerPtBinFile
// Access		: public  
// Returns		: void
// Parameter	: __in CString szBarcode
// Parameter	: __in ST_2DCal_CornerPt stCorner
// Parameter	: __in UINT nIndex
// Qualifier	:
// Last Update	: 2018/3/8 - 9:39
// Desc.		:
//=============================================================================
void CFile_Etc::SaveCornerPtBinFile(__in CString szBarcode, __in ST_2DCal_CornerPt stCorner, __in UINT nIndex)
{
	FILE* ft;
	char szfile[MAX_PATH] = { 0, };

	CString szDir;
	CString szBinFile;

	if (szBarcode.IsEmpty())
		szBarcode = _T("Test");

	szDir.Format(_T("%s\\%s"), m_szPath, szBarcode);
	MakeDirectory(szDir);

	szBinFile.Format(FNAME_BIN_CORNERPT, szDir, nIndex);

	USES_CONVERSION;
	strcpy(szfile, T2A(szBinFile));

	ft = fopen(szfile, "wb");

	if (NULL == ft)
	{
		TRACE(_T("Save BinFile Open Fail.... [FileName:%s \r\n"), szBinFile);
		return;
	}

	fwrite(&stCorner.Position, sizeof(ST_LGE_CORNER_PT) * 40, 1, ft);
	fclose(ft);
}

//=============================================================================
// Method		: LoadCornerPtBinFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in CString szBarcode
// Parameter	: __out ST_2DCal_CornerPt & stCorner
// Parameter	: __in UINT nIndex
// Qualifier	:
// Last Update	: 2018/3/8 - 9:39
// Desc.		:
//=============================================================================
BOOL CFile_Etc::LoadCornerPtBinFile(__in CString szBarcode, __out ST_2DCal_CornerPt& stCorner, __in UINT nIndex)
{
	FILE* ft;
	char szfile[MAX_PATH] = { 0, };

	CString szDir;
	CString szBinFile;

	if (szBarcode.IsEmpty())
		szBarcode = _T("Test");

	szDir.Format(_T("%s\\%s"), m_szPath, szBarcode);
	MakeDirectory(szDir);

	szBinFile.Format(FNAME_BIN_CORNERPT, szDir, nIndex);

	USES_CONVERSION;
	strcpy(szfile, T2A(szBinFile));

	ft = fopen(szfile, "rb");

	if (NULL == ft)
	{
		TRACE(_T("Load BinFile Open Fail.... [FileName:%s \r\n"), szBinFile);
		return FALSE;
	}

	fread(&stCorner.Position, sizeof(ST_LGE_CORNER_PT) * 40, 1, ft);
	fclose(ft);
	return TRUE;
}

void CFile_Etc::Save2DCalSendResult(__in CString szBarcode, __in ST_2DCal_Result stResult)
{
	// arfKK
	// arfKc
	// fDistFOV
	// fR2Max
	// fRepError
	// fOrgOffset
	// nDetectionFailureCnt
	// nInvalidPointCnt
	// nValidPointCnt
	// arcDate
}

void CFile_Etc::Save3DCalDisResult(__in CString szBarcode, __in ST_3DCal_Depth_Result stResult)
{
	// arfDistCoef
	// fTotalFittingError
	// arfAccracyMap
	// fInvalidRatio
	// nFailureCnt
}

void CFile_Etc::Save3DCalEvaluResult(__in CString szBarcode, __in ST_3DCal_Eval_Result stResult)
{
	// fChess
	// fTofDist
	// arfError
	// arstShadingH
	// arstShadingV
	// arsShadingD0
	// arsShadingD1
	// nFailureCnt
	// fIntensityC
}

void CFile_Etc::DeleteDir(__in CString szBarcode)
{
	CString szFile;
	CString szDir;

	CString szOut;
	CString szText;
	CString szStatus;

	if (szBarcode.IsEmpty())
		szBarcode = _T("Test");

	szDir.Format(_T("C:\\Log\\%s"), szBarcode);
	::DeleteAllFiles(szDir, 1);
}

void CFile_Etc::LoadDTCErrorcodeFile(__out ARR_DTC_DESC& arr)
{
	CString szFile;
	CString szText;
	CString szPath;

	CStdioFile file;
	CFileException e;

	// 폴더 경로 를 다른곳으로 옮기자..
	szPath = _T("D:\\TestOMS\\DTC_Errorcode.txt");

	BOOL bOpen = file.Open(szPath, CFile::modeRead, &e);
	if (FALSE == bOpen)
	{
		TRACE(_T("[FileErr] DTC File Load Error Path:%s \r\n"), szPath);
		return;
	}

	if (FALSE == bOpen)
		return;

	while (file.ReadString(szText))
	{
		ST_DTC_Desc dtc;

		CString sz1;
		CString sz2;

		AfxExtractSubString(sz1, szText, 0, _T(':'));
		AfxExtractSubString(sz2, szText, 1, _T(':'));

		dtc.szID = sz1;
		dtc.szDesc = sz2;

		TRACE(_T("ID:%s, Description:%s \r\n"), dtc.szID, dtc.szDesc);
		arr.Add(dtc);
	}

	file.Close();
}
