﻿//*****************************************************************************
// Filename	: 	TestManager_Device.cpp
// Created	:	2016/10/6 - 13:46
// Modified	:	2016/10/6 - 13:46
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "TestManager_Device.h"
#include "CommonFunction.h"

CTestManager_Device::CTestManager_Device()
{
	m_InspectionType = (enInsptrSysType)SET_INSPECTOR;
	OnInitialize();									
}


CTestManager_Device::~CTestManager_Device()
{
	TRACE(_T("<<< Start ~CTestManager_Device >>> \n"));

	this->OnFinalize();

	TRACE(_T("<<< End ~CTestManager_Device >>> \n"));
}

//=============================================================================
// Method		: OnLoadOption
// Access		: virtual protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/5/18 - 18:58
// Desc.		:
//=============================================================================
BOOL CTestManager_Device::OnLoadOption()
{
	CLT_Option	stOption;
//#ifdef USE_MODEL_PATH
//	stOption.m_bModelPathUse =TRUE;
//#endif

	stOption.SetInspectorType(m_InspectionType);
	BOOL		bReturn = TRUE;

	stOption.SetRegistryPath(REG_PATH_OPTION_BASE);

	bReturn &= stOption.LoadOption_Inspector(m_stOption.Inspector);
	bReturn &= stOption.LoadOption_BCR(m_stOption.BCR);
	bReturn &= stOption.LoadOption_MES(m_stOption.MES);
	bReturn &= stOption.LoadOption_PCB(m_stOption.PCB);
	bReturn &= stOption.LoadOption_Misc(m_stOption.Misc);
	bReturn &= stOption.LoadOption_Torque(m_stOption.Torque);

	return bReturn;
}

//=============================================================================
// Method		: InitDevicez
// Access		: virtual protected  
// Returns		: void
// Parameter	: HWND hWndOwner
// Qualifier	:
// Last Update	: 2016/5/18 - 18:41
// Desc.		: 주변 장치 초기화
//=============================================================================
void CTestManager_Device::InitDevicez(HWND hWndOwner /*= NULL*/)
{
	CString strText;

#ifdef USE_BARCODE_SCANNER
	if ((Sys_Focusing == m_InspectionType) || (Sys_Image_Test == m_InspectionType) || (Sys_Stereo_Cal == m_InspectionType))
	{
		m_Device.HandyBCR.SetOwnerHwnd(hWndOwner);
		m_Device.HandyBCR.SetAckMsgID(WM_RECV_BARCODE);
	}
#endif

#ifndef MES_NOT_USE
	// MES
	m_Device.MES.MakeSemaphore(_T("Smph_MES"), 500);
	m_Device.MES.SetDeviceType(0);
	m_Device.MES.SetOwnerHWND(hWndOwner);
	m_Device.MES.SetWmLog(WM_LOGMSG_MES);
	m_Device.MES.SetWmCommStatus(WM_MES_COMM_STATUS);
	m_Device.MES.SetWmRecv(WM_MES_RECV_BARCODE);
	m_Device.MES.SetExitEvent(m_hEvent_ProgramExit);
	m_Device.MES.SetAddress(ntohl(m_stOption.MES.Address.dwAddress), m_stOption.MES.Address.dwPort, ntohl(m_stOption.MES.dwNIC_Address));
#endif

	// Camera 보드
	for (UINT nIdxBrd = 0; nIdxBrd < g_InspectorTable[m_InspectionType].CtrlBrd_Cnt; nIdxBrd++)
	{
		strText.Format(_T("Smph_CameraBoard_%02d"), nIdxBrd + 1);
		m_Device.PCBCamBrd[nIdxBrd].SetOwnerHwnd(hWndOwner);
		m_Device.PCBCamBrd[nIdxBrd].SetExitEvent(m_hEvent_ProgramExit);
		m_Device.PCBCamBrd[nIdxBrd].SetSemaphore(strText);
		m_Device.PCBCamBrd[nIdxBrd].SetLogMsgID(WM_LOGMSG_PCB_CAM);
		m_Device.PCBCamBrd[nIdxBrd].SetAckMsgID(WM_RECV_MAIN_BRD_ACK);
		//m_Device.PCBCamBrd[nIdxBrd].SetAckMsgID(WM_RECV_PCB_LIGHT);
	}

	// 광원 보드 or 광원 파워 서플라이
#ifdef USE_LIGHT_PSU_PARTICLE
	strText = _T("Smph_LightPSU_Part");
	m_Device.LightPSU_Part.SetOwnerHwnd(hWndOwner);
	m_Device.LightPSU_Part.SetExitEvent(m_hEvent_ProgramExit);
	m_Device.LightPSU_Part.SetSemaphore(strText);
	m_Device.LightPSU_Part.SetLogMsgID(WM_LOGMSG_PCB_LIGHT);
#else
	for (UINT nIdxBrd = 0; nIdxBrd < g_InspectorTable[m_InspectionType].LightBrd_Cnt; nIdxBrd++)
	{
		strText.Format(_T("Smph_PCB_LIGHT_Brd_%02d"), nIdxBrd + 1);
		m_Device.LightBrd[nIdxBrd].SetOwnerHwnd(hWndOwner);
		m_Device.LightBrd[nIdxBrd].SetExitEvent(m_hEvent_ProgramExit);
		m_Device.LightBrd[nIdxBrd].SetSemaphore(strText);
		m_Device.LightBrd[nIdxBrd].SetLogMsgID(WM_LOGMSG_PCB_LIGHT);
	}
#endif

	// 광원 파워 서플라이
	for (UINT nIdxBrd = 0; nIdxBrd < g_InspectorTable[m_InspectionType].LightPSU_Cnt; nIdxBrd++)
	{
		strText.Format(_T("Smph_LightPSU_%02d"), nIdxBrd + 1);
		m_Device.LightPSU[nIdxBrd].SetOwnerHwnd(hWndOwner);
		m_Device.LightPSU[nIdxBrd].SetExitEvent(m_hEvent_ProgramExit);
		m_Device.LightPSU[nIdxBrd].SetSemaphore(strText);
		m_Device.LightPSU[nIdxBrd].SetLogMsgID(WM_LOGMSG_POWERSUPPLY);
	}

	if (TRUE == g_InspectorTable[m_InspectionType].UseDIO)
	{
		strText.Format(_T("Smph_DigitalIO"));

		m_Device.DigitalIOCtrl.AXTInit();
		m_Device.DigitalIOCtrl.SetOwnerHwnd(hWndOwner);
		m_Device.DigitalIOCtrl.Set_WM_BitChanged(WM_RECV_DIO_BIT);
		m_Device.DigitalIOCtrl.Set_WM_FirstRead(WM_RECV_DIO_FST_READ);
	}

	
	// Indicator 센서
// 	for (UINT nIdxBrd = 0; nIdxBrd < g_InspectorTable[m_InspectionType].nIndigatorCnt; nIdxBrd++)
// 	{
// 		strText.Format(_T("Smph_IndicatorBoard_%02d"), nIdxBrd + 1);
// 		m_Device.Indicator[nIdxBrd].SetOwnerHwnd(hWndOwner);
// 		m_Device.Indicator[nIdxBrd].SetExitEvent(m_hEvent_ProgramExit);
// 		m_Device.Indicator[nIdxBrd].SetSemaphore(strText);
// 		m_Device.Indicator[nIdxBrd].SetLogMsgID(WM_LOGMSG_INDICATOR);
// 		//m_Device.PCBCamBrd[nIdxBrd].SetAckMsgID(WM_RECV_PCB_LIGHT);
// 	}

	// 토크 드라이버
	if (Sys_Focusing == m_InspectionType)
	{
		strText = _T("Smph_Torque");
		m_Device.Torque.SetOwnerHwnd(hWndOwner);
		m_Device.Torque.SetExitEvent(m_hEvent_ProgramExit);
		m_Device.Torque.SetSemaphore(strText);
		m_Device.Torque.SetLogMsgID(WM_LOGMSG_TORQUE);
		m_Device.Torque.SetAckMsgID(WM_RECV_TORQUE_ACK);
	}

	// 프레임 그래버 보드
	m_Device.DAQ_LVDS.SetOwner(hWndOwner);
	m_Device.DAQ_LVDS.SetWM_RecvVideo(WM_CAMERA_RECV_VIDEO);
	m_Device.DAQ_LVDS.SetWM_ChgStatus(WM_CAMERA_CHG_STATUS);
	m_Device.DAQ_LVDS.SetColorModel_All(enColorModel::CM_Bayer_GRBG, enColorModelOut::CMOUT_RGB);
	m_Device.DAQ_LVDS.SetFrameRate_All(30.0f);

	// OMS 테스트 세팅 (1280 x 960 -> 640 x 480) 우/하
	ST_DAQOption stDaqOpt;
	stDaqOpt.dWidthMultiple		= 1.0;
	stDaqOpt.dHeightMultiple	= 0.5;
	//stDaqOpt.dwClock			= 0;
	stDaqOpt.nDataMode			= enDAQDataMode::DAQ_Data_16bit_Mode;
	stDaqOpt.nHsyncPolarity		= enDAQHsyncPolarity::DAQ_HsyncPol_Inverse;
	stDaqOpt.nPClockPolarity	= enDAQPclkPolarity::DAQ_PclkPol_RisingEdge;
	stDaqOpt.nDvalUse			= enDAQDvalUse::DAQ_DeUse_DVAL_Use;
	stDaqOpt.nVideoMode			= enDAQVideoMode::DAQ_Video_MIPI_Mode;
	stDaqOpt.nMIPILane			= enDAQMIPILane::DAQMIPILane_2;
	stDaqOpt.nSensorType		= enImgSensorType::enImgSensorType_Monochrome_12bit;
	stDaqOpt.nConvFormat		= enConvFormat::Conv_YCbYCr_BGGR;
	stDaqOpt.byBitPerPixels		= 12;
	stDaqOpt.nCropFrame			= Crop_OddFrame;//Crop_OddFrame,Crop_EvenFrame;
	//stDaqOpt.dShiftExp			= 0.0f;
	stDaqOpt.SetExposure(0.0f);
	
	m_Device.DAQ_LVDS.SetDAQOption_All(stDaqOpt);
	//m_Device.DAQ_LVDS.SetI2CFilePath_All(_T("C:/Temp/oms_RESET_APPLY.set"));
}

//=============================================================================
// Method		: ConnectDevicez
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/18 - 18:58
// Desc.		:
//=============================================================================
void CTestManager_Device::ConnectDevicez()
{
	OnShowSplashScreen(TRUE, _T("Connect Devices"));

	OnShowSplashScreen(TRUE, _T("Connect I/O Motion"));
	ConnectMotion(TRUE);
	
#ifdef USE_BARCODE_SCANNER
	if ((Sys_Focusing == m_InspectionType) || (Sys_Image_Test == m_InspectionType) || (Sys_Stereo_Cal == m_InspectionType))
	{
		OnShowSplashScreen(TRUE, _T("Connect Barcode Reader"));
		ConnectHandyBCR(TRUE);
	}
#endif

	OnShowSplashScreen(TRUE, _T("Connect Camera Board"));
	ConnectPCB_Camera_Brd(TRUE);

	OnShowSplashScreen(TRUE, _T("Connect Light Board"));
	ConnectPCB_LightBrd(TRUE);


	OnShowSplashScreen(TRUE, _T("Connect Light PowerSupply"));
	ConnectLightPSU(TRUE);

	if (Sys_Focusing == m_InspectionType)
	{
		OnShowSplashScreen(TRUE, _T("Connect Torque Driver"));
		ConnectTorque(TRUE);
	}

#ifndef MES_NOT_USE
	OnShowSplashScreen(TRUE, _T("Connect MES"));
	ConnectMES(TRUE);
#endif

	OnShowSplashScreen(TRUE, _T("Connect Video Grabber"));

	for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InspectionType].Grabber_Cnt; nIdx++)
	{
		ConnectGrabberBrd(TRUE, nIdx);
	}

	//OnShowSplashScreen(FALSE);
}

//=============================================================================
// Method		: DisconnectDevicez
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/31 - 22:11
// Desc.		:
//=============================================================================
void CTestManager_Device::DisconnectDevicez()
{
	OnShowSplashScreen(TRUE, _T("Disconnect Devices"));

	OnShowSplashScreen(TRUE, _T("Disconnect Video Grabber"));

	for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InspectionType].Grabber_Cnt; nIdx++)
	{
		ConnectGrabberBrd(FALSE, nIdx);
	}

	OnShowSplashScreen(TRUE, _T("Disconnect I/O Motion"));
	ConnectMotion(FALSE);
	
#ifndef MES_NOT_USE
	OnShowSplashScreen(TRUE, _T("Disconnect MES"));
	ConnectMES(FALSE);
#endif

	OnShowSplashScreen(TRUE, _T("Disconnect Camera Board"));
	ConnectPCB_Camera_Brd(FALSE);

	OnShowSplashScreen(TRUE, _T("Disconnect Light PowerSupply"));
	ConnectLightPSU(FALSE);

	OnShowSplashScreen(TRUE, _T("Disconnect Light Board"));
	ConnectPCB_LightBrd(FALSE);

	if (Sys_Focusing == m_InspectionType)
	{
		OnShowSplashScreen(TRUE, _T("Disconnect Torque Driver"));
		ConnectTorque(FALSE);
	}

#ifdef USE_BARCODE_SCANNER
	if ((Sys_Focusing == m_InspectionType) || (Sys_Image_Test == m_InspectionType) || (Sys_Stereo_Cal == m_InspectionType))
	{
		OnShowSplashScreen(TRUE, _T("Disconnect Barcode Reader"));
		ConnectHandyBCR(FALSE);
	}
#endif

	OnShowSplashScreen(TRUE, _T("Exit Program "));
	
	//OnShowSplashScreen(FALSE);
}

//=============================================================================
// Method		: ConnectMES
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2016/12/28 - 15:56
// Desc.		:
//=============================================================================
BOOL CTestManager_Device::ConnectMES(__in BOOL bConnect)
{
#ifndef MES_NOT_USE
	if (bConnect)
	{
		TRACE(_T("MES 주소 : %s : %d에 접속 시도\n"), ConvIPAddrToString(ntohl(m_stOption.MES.Address.dwAddress)), m_stOption.MES.Address.dwPort);

		m_Device.MES.ContinuousConnectServer();
	}
	else
	{
		m_Device.MES.DisconnectSever();
	}
#endif

	return TRUE;
}

//=============================================================================
// Method		: ConnectHandyBCR
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2016/10/6 - 19:59
// Desc.		:
//=============================================================================
BOOL CTestManager_Device::ConnectHandyBCR(__in BOOL bConnect)
{
#ifdef USE_BARCODE_SCANNER
	if (bConnect) // 연결
	{
 		CString strComPort;
 		HANDLE hBCR = NULL;
 
 		strComPort.Format(_T("//./COM%d"), m_stOption.BCR.Handy_ComPort.Port);
 
 		hBCR = m_Device.HandyBCR.Connect(strComPort,
 									m_stOption.BCR.Handy_ComPort.BaudRate,
 									m_stOption.BCR.Handy_ComPort.Parity,
 									m_stOption.BCR.Handy_ComPort.StopBits,
 									m_stOption.BCR.Handy_ComPort.ByteSize);
 
 		if (NULL != hBCR)// 접속에 성공
 		{
 			OnLog(_T("Handy BCR: COMM%d Successful communication connection"), m_stOption.BCR.Handy_ComPort.Port);
 			OnSetStatus_HandyBCR(TRUE);
 		}
 		else
 		{
 			TRACE(_T("Handy BCR: COMM%d Communication connection failure\n"), m_stOption.BCR.Handy_ComPort.Port);
 			OnLog_Err(_T("Handy BCR: COMM%d Communication connection failure"), m_stOption.BCR.Handy_ComPort.Port);
 			OnSetStatus_HandyBCR(FALSE);
 		}
	}
	else // 연결 해제
	{
		if (m_Device.HandyBCR.Disconnect())
		{
			OnSetStatus_HandyBCR(TRUE);
		}
		else
		{
			;
		}
	}

#endif

	return TRUE;
}

//=============================================================================
// Method		: ConnectFixedBCR
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2017/9/12 - 20:01
// Desc.		:
//=============================================================================
BOOL CTestManager_Device::ConnectFixedBCR(__in BOOL bConnect)
{
	if (bConnect) // 연결
	{
// 		CString strComPort;
// 		HANDLE hBCR = NULL;
// 
// 		strComPort.Format(_T("//./COM%d"), m_stOption.BCR.Fixed_ComPort.Port);
// 
// 		hBCR = m_Device.FixedBCR.Connect(strComPort,
// 			m_stOption.BCR.Fixed_ComPort.BaudRate,
// 			m_stOption.BCR.Fixed_ComPort.Parity,
// 			m_stOption.BCR.Fixed_ComPort.StopBits,
// 			m_stOption.BCR.Fixed_ComPort.ByteSize);
// 
// 		if (NULL != hBCR)// 접속에 성공
// 		{
// 			OnLog(_T("Fixed BCR: COMM%d Successful communication connection"), m_stOption.BCR.Fixed_ComPort.Port);
// 
// 			// 통신 싱크 확인	
// 			LRESULT lCommResult = SRC_OK;
// 			lCommResult = m_Device.FixedBCR.Send_VersionCheck();
// 
// 			if (SRC_OK == lCommResult)
// 			{
// 				OnSetStatus_FixedBCR(COMM_STATUS_SYNC_OK);
// 			}
// 			else
// 			{
// 				OnSetStatus_FixedBCR(COMM_STATUS_CONNECT);
// 			}
// 		}
// 		else
// 		{
// 			TRACE(_T("Fixed BCR: COMM%d Communication connection failure\n"), m_stOption.BCR.Fixed_ComPort.Port);
// 			OnLog_Err(_T("Fixed BCR: COMM%d Communication connection failure"), m_stOption.BCR.Fixed_ComPort.Port);
// 			OnSetStatus_FixedBCR(COMM_STATUS_NOTCONNECTED);
// 		}
	}
	else // 연결 해제
	{
// 		if (m_Device.FixedBCR.Disconnect())
// 		{
// 			OnSetStatus_FixedBCR(COMM_STATUS_DISCONNECT);
// 		}
// 		else
// 		{
// 			;
// 		}
	}

	return TRUE;
}

//=============================================================================
// Method		: ConnectPCB_Camera_Brd
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2016/10/16 - 16:13
// Desc.		:
//=============================================================================
BOOL CTestManager_Device::ConnectPCB_Camera_Brd(__in BOOL bConnect)
{
	if (bConnect) // 연결
	{
 		CString strComPort;
 		HANDLE hBCR = NULL;
 		LRESULT lCommResult = SRC_OK;
 
 		for (UINT nIdxBrd = 0; nIdxBrd < g_InspectorTable[m_InspectionType].Grabber_Cnt; nIdxBrd++)
 		{
 			strComPort.Format(_T("//./COM%d"), m_stOption.PCB.ComPort_CtrlBrd[nIdxBrd].Port);
 
 			hBCR = m_Device.PCBCamBrd[nIdxBrd].Connect(strComPort,
 				m_stOption.PCB.ComPort_CtrlBrd[nIdxBrd].BaudRate,
 				m_stOption.PCB.ComPort_CtrlBrd[nIdxBrd].Parity,
 				m_stOption.PCB.ComPort_CtrlBrd[nIdxBrd].StopBits,
 				m_stOption.PCB.ComPort_CtrlBrd[nIdxBrd].ByteSize);
 
 			if (NULL != hBCR)// 접속에 성공
 			{
 				OnLog(_T("PCB Camera Brd[%d]: COMM%d Successful communication connection"), nIdxBrd, m_stOption.PCB.ComPort_CtrlBrd[nIdxBrd].Port);				
 
 				// 보드 체크로 통신 싱크 확인	
 				BYTE byBoardNo;
 				lCommResult = m_Device.PCBCamBrd[nIdxBrd].Send_BoardCheck(byBoardNo);
 
 				if (SRC_OK == lCommResult)
 				{
 					OnSetStatus_Camera_Brd(COMM_STATUS_SYNC_OK, nIdxBrd);
 				}
 				else
 				{
 					OnSetStatus_Camera_Brd(COMM_STATUS_CONNECT, nIdxBrd);
 				}
 			}
 			else
 			{
 				TRACE(_T("PCB Camera Brd[%d]: COMM%d Communication connection failure\n"), nIdxBrd, m_stOption.PCB.ComPort_CtrlBrd[nIdxBrd].Port);
 				OnLog_Err(_T("PCB Camera Brd[%d]: COMM%d Communication connection failure"), nIdxBrd, m_stOption.PCB.ComPort_CtrlBrd[nIdxBrd].Port);
 				OnSetStatus_Camera_Brd(COMM_STATUS_NOTCONNECTED, nIdxBrd);
 			}
 		}
	}
	else // 연결 해제
	{
		for (UINT nIdxBrd = 0; nIdxBrd < g_InspectorTable[m_InspectionType].Grabber_Cnt; nIdxBrd++)
		{
			if (m_Device.PCBCamBrd[nIdxBrd].Disconnect())
			{
				OnSetStatus_Camera_Brd(COMM_STATUS_DISCONNECT, nIdxBrd);
			}
			else
			{
				;
			}
		}
	}

	return TRUE;
}

//=============================================================================
// Method		: ConnectPCB_LightBrd
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2016/9/28 - 18:15
// Desc.		:
//=============================================================================
BOOL CTestManager_Device::ConnectPCB_LightBrd(__in BOOL bConnect)
{
#ifdef USE_LIGHT_PSU_PARTICLE
	if (bConnect) // 연결
	{
		CString strComPort;
		HANDLE	hBCR		= NULL;
		LRESULT lCommResult = SRC_OK;

		strComPort.Format(_T("//./COM%d"), m_stOption.PCB.ComPort_LightBrd[0].Port);

		hBCR = m_Device.LightPSU_Part.Connect(strComPort,
											m_stOption.PCB.ComPort_LightBrd[0].BaudRate,
											m_stOption.PCB.ComPort_LightBrd[0].Parity,
											m_stOption.PCB.ComPort_LightBrd[0].StopBits,
											m_stOption.PCB.ComPort_LightBrd[0].ByteSize);

		if (NULL != hBCR)// 접속에 성공
		{
			OnLog(_T("Light PSU Particle: COMM%d Successful communication connection"), m_stOption.PCB.ComPort_LightBrd[0].Port);

			// 보드 체크로 통신 싱크 확인
			lCommResult = m_Device.LightPSU_Part.Send_PortCheck();

			if (SRC_OK == lCommResult)
			{
				OnSetStatus_LightBrd(COMM_STATUS_SYNC_OK, 0);
			}
			else
			{
				OnSetStatus_LightBrd(COMM_STATUS_CONNECT, 0);
			}
		}
		else
		{
			TRACE(_T("Light PSU Particle: COMM%d Communication connection failure\n"), m_stOption.PCB.ComPort_LightBrd[0].Port);
			OnLog_Err(_T("Light PSU Particle: COMM%d Communication connection failure"), m_stOption.PCB.ComPort_LightBrd[0].Port);
			OnSetStatus_LightBrd(COMM_STATUS_NOTCONNECTED, 0);
		}
	}
	else // 연결 해제
	{
		if (m_Device.LightPSU_Part.Disconnect())
		{
			OnSetStatus_LightBrd(COMM_STATUS_DISCONNECT, 0);
		}
		else
		{
			;
		}
	}

#else

	if (bConnect) // 연결
	{
		CString strComPort;
		HANDLE hBCR = NULL;
		LRESULT lCommResult = SRC_OK;

		for (UINT nIdxBrd = 0; nIdxBrd < g_InspectorTable[m_InspectionType].LightBrd_Cnt; nIdxBrd++)
		{
			strComPort.Format(_T("//./COM%d"), m_stOption.PCB.ComPort_LightBrd[nIdxBrd].Port);

			hBCR = m_Device.LightBrd[nIdxBrd].Connect(strComPort,
				m_stOption.PCB.ComPort_LightBrd[nIdxBrd].BaudRate,
				m_stOption.PCB.ComPort_LightBrd[nIdxBrd].Parity,
				m_stOption.PCB.ComPort_LightBrd[nIdxBrd].StopBits,
				m_stOption.PCB.ComPort_LightBrd[nIdxBrd].ByteSize);

			if (NULL != hBCR)// 접속에 성공
			{
				OnLog(_T("PCB Light Brd[%d]: COMM%d Successful communication connection"), nIdxBrd, m_stOption.PCB.ComPort_LightBrd[nIdxBrd].Port);

				// 보드 체크로 통신 싱크 확인
				BYTE byPort = 0;
				lCommResult = m_Device.LightBrd[nIdxBrd].Send_PortCheck(byPort);

				if (SRC_OK == lCommResult)
				{
					OnSetStatus_LightBrd(COMM_STATUS_SYNC_OK, nIdxBrd);
				}
				else
				{
					OnSetStatus_LightBrd(COMM_STATUS_CONNECT, nIdxBrd);
				}
			}
			else
			{
				TRACE(_T("PCB Light Brd[%d]: COMM%d Communication connection failure\n"), nIdxBrd, m_stOption.PCB.ComPort_LightBrd[nIdxBrd].Port);
				OnLog_Err(_T("PCB Light Brd[%d]: COMM%d Communication connection failure"), nIdxBrd, m_stOption.PCB.ComPort_LightBrd[nIdxBrd].Port);
				OnSetStatus_LightBrd(COMM_STATUS_NOTCONNECTED, nIdxBrd);
			}
		}
	}
	else // 연결 해제
	{
		for (UINT nIdxBrd = 0; nIdxBrd < g_InspectorTable[m_InspectionType].LightPSU_Cnt; nIdxBrd++)
		{
			if (m_Device.LightBrd[nIdxBrd].Disconnect())
			{
				OnSetStatus_LightBrd(COMM_STATUS_DISCONNECT, nIdxBrd);
			}
			else
			{
				;
			}
		}
	}
#endif

	return TRUE;
}

//=============================================================================
// Method		: ConnectLightPSU
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2017/11/8 - 10:17
// Desc.		: ODA PT 시리즈 파워 서플라이 연결 및 해제
//=============================================================================
BOOL CTestManager_Device::ConnectLightPSU(__in BOOL bConnect)
{
	if (bConnect) // 연결
	{
		CString strComPort;
		HANDLE hBCR = NULL;
		LRESULT lCommResult = SRC_OK;

		for (UINT nIdxBrd = 0; nIdxBrd < g_InspectorTable[m_InspectionType].LightPSU_Cnt; nIdxBrd++)
		{
			strComPort.Format(_T("//./COM%d"), m_stOption.PCB.ComPort_LightPSU[nIdxBrd].Port);

			hBCR = m_Device.LightPSU[nIdxBrd].Connect(strComPort,
				m_stOption.PCB.ComPort_LightPSU[nIdxBrd].BaudRate,
				m_stOption.PCB.ComPort_LightPSU[nIdxBrd].Parity,
				m_stOption.PCB.ComPort_LightPSU[nIdxBrd].StopBits,
				m_stOption.PCB.ComPort_LightPSU[nIdxBrd].ByteSize);

			if (NULL != hBCR)// 접속에 성공
			{
				OnLog(_T("Light PowerSupply: COMM%d Successful communication connection"), m_stOption.PCB.ComPort_LightPSU[nIdxBrd].Port);

				// 보드 체크로 통신 싱크 확인
				lCommResult = m_Device.LightPSU[nIdxBrd].Send_PortCheck();

				if (SRC_OK == lCommResult)
				{
					OnSetStatus_LightPSU(COMM_STATUS_SYNC_OK, nIdxBrd);
				}
				else
				{
					OnSetStatus_LightPSU(COMM_STATUS_CONNECT, nIdxBrd);
				}
			}
			else
			{
				TRACE(_T("Light PowerSupply: COMM%d Communication connection failure\n"), m_stOption.PCB.ComPort_LightPSU[nIdxBrd].Port);
				OnLog_Err(_T("Light PowerSupply: COMM%d Communication connection failure"), m_stOption.PCB.ComPort_LightPSU[nIdxBrd].Port);
				OnSetStatus_LightPSU(COMM_STATUS_NOTCONNECTED, nIdxBrd);
			}
		}
	}
	else // 연결 해제
	{
		for (UINT nIdxBrd = 0; nIdxBrd < g_InspectorTable[m_InspectionType].LightPSU_Cnt; nIdxBrd++)
		{
			if (m_Device.LightPSU[nIdxBrd].Disconnect())
			{
				OnSetStatus_LightPSU(COMM_STATUS_DISCONNECT, nIdxBrd);
			}
			else
			{
				;
			}
		}
	}

	return TRUE;
}

//=============================================================================
// Method		: ConnectPLC
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2017/9/12 - 20:02
// Desc.		:
//=============================================================================
BOOL CTestManager_Device::ConnectPLC(__in BOOL bConnect)
{
	if (bConnect)
	{
//		TRACE(_T("PLC 주소 : %s : %d에 접속 시도\n"), ConvIPAddrToString(ntohl(m_stOption.PLC.Address.dwAddress)), m_stOption.PLC.Address.dwPort);

// 		m_Device.PLC.ContinuousConnectServer();
// 		m_Device.PLC.Set_UseHearBeat(TRUE);
// 		m_Device.PLC.Start_PLC_Mon();
	}
	else
	{
// 		m_Device.PLC.Stop_PLC_Mon();
// 
// 		m_Device.PLC.DisconnectSever();
	}

	return TRUE;
}

//=============================================================================
// Method		: ConnectMotion
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2017/9/12 - 20:02
// Desc.		:
//=============================================================================
BOOL CTestManager_Device::ConnectMotion(__in BOOL bConnect)
{
	if (bConnect)
	{
		if (m_Device.DigitalIOCtrl.AXTState())
		{
			OnLog(_T("Motor Control Board Connect Succed"));
			OnSetStatus_Motion(TRUE);
			OnSet_BoardPower(TRUE);
		}
		else
		{
			OnLog(_T("Motor Control Board Connect Failed"));
			OnSetStatus_Motion(FALSE);
		}
	}
	else
	{
	}

	return TRUE;
}

//=============================================================================
// Method		: ConnectIndicator
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2018/2/5 - 16:50
// Desc.		:
//=============================================================================
BOOL CTestManager_Device::ConnectIndicator(__in BOOL bConnect)
{
// 	if (bConnect) // 연결
// 	{
// 		CString strComPort;
// 		CStringA strCheck;
// 		HANDLE hBCR = NULL;
// 
// 		for (UINT nIdxBrd = 0; nIdxBrd < g_InspectorTable[m_InspectionType].nIndigatorCnt; nIdxBrd++)
// 		{
// 			strComPort.Format(_T("//./COM%d"), m_stOption.BCR.ComPort_Motor[nIdxBrd].Port);
// 
// 			hBCR = m_Device.Indicator[nIdxBrd].Connect(strComPort,
// 				m_stOption.BCR.ComPort_Motor[nIdxBrd].BaudRate,
// 				m_stOption.BCR.ComPort_Motor[nIdxBrd].Parity,
// 				m_stOption.BCR.ComPort_Motor[nIdxBrd].StopBits,
// 				m_stOption.BCR.ComPort_Motor[nIdxBrd].ByteSize);
// 
// 			if (NULL != hBCR)// 접속에 성공
// 			{
// 				OnLog(_T("Indicator: COMM%d Comm Contact OK"), m_stOption.BCR.ComPort_Motor[nIdxBrd].Port);
// 				OnSetStatus_Indicator(COMM_STATUS_CONNECT, nIdxBrd);
// 
// 				if (m_Device.Indicator[nIdxBrd].Send_PortCheck(strCheck))
// 				{
// 					if (strCheck == _T("ID"))
// 					{
// 						OnSetStatus_Indicator(COMM_STATUS_SYNC_OK, nIdxBrd);
// 						b_IndicatorConnect[nIdxBrd] = TRUE;
// 					}
// 					else
// 					{
// 						b_IndicatorConnect[nIdxBrd] = FALSE;
// 					}
// 				}
// 			}
// 			else
// 			{
// 				OnLog_Err(_T("Indicator: COMM%d Comm Contact Fail"), m_stOption.BCR.ComPort_Motor[nIdxBrd].Port);
// 				OnSetStatus_Indicator(COMM_STATUS_NOTCONNECTED, nIdxBrd);
// 				b_IndicatorConnect[nIdxBrd] = FALSE;
// 			}
// 		}
// 	}
// 	else // 연결 해제
// 	{
// 		for (UINT nIdxBrd = 0; nIdxBrd < g_InspectorTable[m_InspectionType].nIndigatorCnt; nIdxBrd++)
// 		{
// 			if (m_Device.Indicator[nIdxBrd].Disconnect())
// 			{
// 				b_IndicatorConnect[nIdxBrd] = FALSE;
// 				OnSetStatus_Indicator(COMM_STATUS_DISCONNECT, nIdxBrd);
// 			}
// 		}
// 	}

	return TRUE;
}

//=============================================================================
// Method		: ConnectGrabberBrd
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in BOOL bConnect
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/2/1 - 10:09
// Desc.		:
//=============================================================================
BOOL CTestManager_Device::ConnectGrabberBrd(__in BOOL bConnect, __in UINT nIdxBrd /*= 0*/)
{
	BOOL bReturn = TRUE;

	if (bConnect)
	{
		int iBoardCount = 0;

		for (int t = 0; t < 3; t++)
		{
			if (m_Device.DAQ_LVDS.Open_Device(iBoardCount))
			{
				if (m_Device.DAQ_LVDS.LVDS_Init(nIdxBrd))
				{
					OnSetStatus_GrabBoard(TRUE, nIdxBrd);

					OnLog(_T("LVDS Init : Succeed"));
					//PCB_SetDeserialize();
				}
				else
				{
					bReturn = FALSE;
					OnLog(_T("LVDS Init : Failed"));
				}
				return bReturn;
			}
			Sleep(100);
		}
		bReturn = FALSE;
		OnLog(_T("LVDS Open : Failed [%d found]"), iBoardCount);

// 		if (m_Device.DAQ_LVDS.Open_Device(iBoardCount))
// 		{			
// 			OnLog(_T("LVDS Open : Succeed [%d found]"), iBoardCount);
// 
// 			if (m_Device.DAQ_LVDS.LVDS_Init(nIdxBrd))
// 			{
// 				OnSetStatus_GrabBoard(TRUE, nIdxBrd);
// 
// 				OnLog(_T("LVDS Init : Succeed"));
// 				//PCB_SetDeserialize();
// 			}
// 			else
// 			{
// 				bReturn = FALSE;
// 				OnLog(_T("LVDS Init : Failed"));
// 			}
// 		}
// 		else
// 		{
// 			bReturn = FALSE;
// 			OnLog(_T("LVDS Open : Failed [%d found]"), iBoardCount);
// 		}
	}
	else
	{
		m_Device.DAQ_LVDS.Capture_Stop_All();

		if (m_Device.DAQ_LVDS.LVDS_Stop(nIdxBrd))
		{
			OnLog(_T("LVDS Stop : Succeed"));
		}
		else
		{
			OnLog(_T("LVDS Stop : Failed"));
		}

		if (m_Device.DAQ_LVDS.Close_Device())
		{
			OnLog(_T("LVDS Close : Succeed"));
		}
		else
		{
			bReturn = FALSE;
			OnLog(_T("LVDS Close : Failed"));
		}

		OnSetStatus_GrabBoard(FALSE, nIdxBrd);
	}

	return bReturn;
}


//=============================================================================
// Method		: ConnectTorque
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in BOOL bConnect
// Qualifier	:
// Last Update	: 2018/3/8 - 8:25
// Desc.		:
//=============================================================================
BOOL CTestManager_Device::ConnectTorque(__in BOOL bConnect)
{
	if (bConnect) // 연결
	{
		CString	strLog;
		CString strComPort;
		HANDLE hBCR = NULL;

		strComPort.Format(_T("//./COM%d"), m_stOption.Torque.ComPort_Torque.Port);

		hBCR = m_Device.Torque.Connect(strComPort,
			m_stOption.Torque.ComPort_Torque.BaudRate,
			m_stOption.Torque.ComPort_Torque.Parity,
			m_stOption.Torque.ComPort_Torque.StopBits,
			m_stOption.Torque.ComPort_Torque.ByteSize);

		if (NULL != hBCR)// 접속에 성공
		{
			OnLog(_T("Torque Driver : COM %d 통신 연결 성공"), m_stOption.Torque.ComPort_Torque.Port);
			OnSetStatus_Torque(COMM_STATUS_CONNECT);

			// 보드 체크로 통신 싱크 확인
			if (m_Device.Torque.Send_VersionCheck())
			{
				OnSetStatus_Torque(COMM_STATUS_SYNC_OK);
			}
		}
		else
		{
			OnLog_Err(_T("Torque Driver : COM %d 통신 연결 실패"), m_stOption.Torque.ComPort_Torque.Port);
			OnSetStatus_Torque(COMM_STATUS_NOTCONNECTED);
		}
	}
	else // 연결 해제
	{
		if (m_Device.Torque.Disconnect())
		{
			OnSetStatus_Torque(COMM_STATUS_DISCONNECT);
		}
		else
		{
			;
		}
	}

	return TRUE;
}

//=============================================================================
// Method		: OnInitialize
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/9/28 - 20:10
// Desc.		:
//=============================================================================
void CTestManager_Device::OnInitialize()
{
	// 커맨드 라인
	int nArgs = __argc;
	if (nArgs < 3)	// .exe /m E
	{
		TRACE("Argument not found\n");
		return;
	}

	// 강제 모델 설정 사용 여부
	CString szOption	= __targv[1];	// /m
	// 강제 설정된 모델
	CString szModel		= __targv[2];	// E, F, FS, I, M

	szOption.MakeUpper();
	szModel.MakeUpper();

	if (0 == szOption.Compare(_T("/M")))
	{
		for (UINT nIdx = 0; NULL != g_szModelArg[nIdx]; nIdx++)
		{
			if (0 == szModel.Compare(g_szModelArg[nIdx]))
			{
				m_bUseForcedModel = TRUE;
				m_nModelType = (enModelType)nIdx;
				break;
			}
		}
	}

#ifdef _DEBUG
	if (m_bUseForcedModel)
	{
		TRACE(_T("**** Selected Model : %s ****\n"), g_szModelName[m_nModelType]);

		// 장비별 사용 가능 모델 확인
		switch (m_InspectionType)
		{
		case Sys_2D_Cal:		// OMS Entry, OMS Front, OMS Front Set
			switch (m_nModelType)
			{
			case Model_OMS_Entry:
			//case Model_OMS_Front:
			//case Model_OMS_Front_Set:
				break;

			default:
				m_nModelType		= Model_OMS_Entry;
				m_bUseForcedModel	= FALSE;
				break;
			}
			break;

		case Sys_Stereo_Cal:	// IKC, MRA2
			switch (m_nModelType)
			{
			case Model_MRA2:
			case Model_IKC:
				break;

			default:
				m_nModelType		= Model_IKC;
				m_bUseForcedModel	= FALSE;
				break;
			}
			break;

		case Sys_Focusing:		// IKC, MRA2, OMS Front
			switch (m_nModelType)
			{
			case Model_OMS_Front:
			case Model_MRA2:
			case Model_IKC:
				break;

			default:
				m_nModelType		= Model_IKC;
				m_bUseForcedModel	= FALSE;
				break;
			}
			break;

		case Sys_Image_Test:	// IKC, MRA2, OMS Front
//#ifdef SET_GWANGJU				// IKC, MRA2, OMS Entry
//			switch (m_nModelType)
//			{
//			case Model_OMS_Entry:
//			case Model_MRA2:
//			case Model_IKC:
//				break;
//
//			default:
//				m_nModelType		= Model_IKC;
//				m_bUseForcedModel	= FALSE;
//				break;
//			}
//#else							// IKC, MRA2, OMS Front
			switch (m_nModelType)
			{
			case Model_OMS_Entry:
			case Model_MRA2:
			case Model_IKC:
				break;

			default:
				m_nModelType		= Model_IKC;
				m_bUseForcedModel	= FALSE;
				break;
			}
//#endif
			break;

		case Sys_GJ_Image_Test:	// IKC, MRA2, OMS Entry
			switch (m_nModelType)
			{
			case Model_OMS_Entry:
			case Model_MRA2:
			case Model_IKC:
				break;

			default:
				m_nModelType		= Model_IKC;
				m_bUseForcedModel	= FALSE;
				break;
			}
			break;

		default:
			break;
		}
	}

	if (m_bUseForcedModel)
	{
		TRACE(_T("**** Forced Model : %s ****\n"), g_szModelName[m_nModelType]);
	}
#endif //_DEBUG

}

//=============================================================================
// Method		: OnFinalize
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/9/28 - 20:10
// Desc.		:
//=============================================================================
void CTestManager_Device::OnFinalize()
{

}
