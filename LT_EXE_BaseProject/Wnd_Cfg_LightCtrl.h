﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_LightCtrl.h
// Created	:	2016/10/31 - 20:17
// Modified	:	2016/10/31 - 20:17
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Cfg_LightCtrl_h__
#define Wnd_Cfg_LightCtrl_h__

#pragma once

#include "VGStatic.h"
#include "Wnd_BaseView.h"
#include "Wnd_LightCtrl.h"
#include "Def_DataStruct.h"
#include "Def_TestDevice.h"
#include "File_WatchList.h"
#include "File_Maintenance.h"

#include "List_LightBrd_Op.h"

//=============================================================================
// CWnd_Cfg_LightCtrl
//=============================================================================
class CWnd_Cfg_LightCtrl : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_Cfg_LightCtrl)

public:
	CWnd_Cfg_LightCtrl();
	virtual ~CWnd_Cfg_LightCtrl();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg void	OnSize(UINT nType, int cx, int cy);
	afx_msg int		OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnBnClickedBnNew();
	afx_msg void	OnBnClickedBnSave();
	afx_msg void	OnCbnSelendokFile();
	virtual BOOL	PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL	PreTranslateMessage(MSG* pMsg);

	CFont			m_font;
	CString			m_strMaintenancePath;
	CString			m_strMaintenanceFile;

	CVGStatic			m_st_Path;
	CVGStatic			m_st_File;
	CComboBox			m_cb_File;
	CButton				m_bn_NewFile;
	CButton				m_bn_SaveFile;

	// 광원 설정하는 윈도우들
	CVGStatic			m_st_Category;

	CWnd_LightCtrl		m_wnd_LightPSU[Chart_MaxEnum];
	CWnd_LightCtrl		m_wnd_LightBrd[Light_I_MaxEnum];

	ST_Device*			m_pstDevice;
	ST_MaintenanceInfo*	m_pstMaintenanceInfo;
	CFile_Maintenance	m_fileMaintenance;

	CFile_WatchList		m_IniWatch;
	enInsptrSysType		m_nSysType;
	CList_LightBrd_Op	m_List_LightBrd_Op;

	void	SetFullPath				(__in LPCTSTR szMaintenanceName);
	void	RefreshFileList			(__in const CStringList* pFileList);

public:

	void	SetLightInfo			();
	void	GetLightInfo			();
	void	UpdateMaintenanceInfo	(__in CString szMaintenanceFile);

	void	SetPath					(__in LPCTSTR szMaintenancePath)
	{
		if (NULL != szMaintenancePath)
			m_strMaintenancePath = szMaintenancePath;
	};

	void	SetSystemType			(__in enInsptrSysType nSysType)
	{
		m_nSysType = nSysType;
	};

	void	SetPtr_Device			(__in ST_Device* pstDevice)
	{
		if (NULL == pstDevice)
			return;

		m_pstDevice = pstDevice;

		for (UINT nIdx = 0; nIdx < Chart_MaxEnum; nIdx++)
		{
			m_wnd_LightPSU[nIdx].SetPtr_Device(LBT_ODA_PT, &m_pstDevice->LightPSU[nIdx]);
		}

		// 추후 수정 필요 KHO
		for (UINT nIdx = 0; nIdx < Light_I_MaxEnum; nIdx++)
		{
#ifdef USE_LIGHT_PSU_PARTICLE
			m_wnd_LightBrd[nIdx].SetPtr_Device(LBT_ODA_PT, &m_pstDevice->LightPSU_Part);
#else
			m_wnd_LightBrd[nIdx].SetPtr_Device(LBT_Luritech_01, &m_pstDevice->LightBrd[0]);
#endif
		}

		m_List_LightBrd_Op.SetPtr_Device(LBT_Luritech_01, &m_pstDevice->LightBrd[0]);
	};

	void	SetPtr_MaintenanceInfo	(__in ST_MaintenanceInfo* pstMaintenanceInfo)
	{
		if (NULL == pstMaintenanceInfo)
			return;

		m_pstMaintenanceInfo = pstMaintenanceInfo;
	};
};

#endif // Wnd_Cfg_LightCtrl_h__
