//*****************************************************************************
// Filename	: 	TestManager_EQP_ImgT.cpp
// Created	:	2018/1/27 - 13:48
// Modified	:	2018/1/27 - 13:48
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#include "stdafx.h"
#include "TestManager_EQP_ImgT.h"
#include "CRC16.h"

#include <Mmsystem.h>
#pragma comment (lib,"winmm.lib")

CTestManager_EQP_ImgT::CTestManager_EQP_ImgT()
{
	OnInitialize();
}


CTestManager_EQP_ImgT::~CTestManager_EQP_ImgT()
{
	TRACE(_T("<<< Start ~CTestManager_EQP_ImgT >>> \n"));

	this->OnFinalize();

	TRACE(_T("<<< End ~CTestManager_EQP_ImgT >>> \n"));
}

//=============================================================================
// Method		: OnLoadOption
// Access		: virtual protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/9/28 - 20:04
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP_ImgT::OnLoadOption()
{
	BOOL bReturn = CTestManager_EQP::OnLoadOption();



	return bReturn;
}

//=============================================================================
// Method		: InitDevicez
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in HWND hWndOwner
// Qualifier	:
// Last Update	: 2017/11/11 - 21:37
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::InitDevicez(__in HWND hWndOwner /*= NULL*/)
{
	CTestManager_EQP::InitDevicez(hWndOwner);

}

//=============================================================================
// Method		: StartOperation_LoadUnload
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:19
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::StartOperation_LoadUnload(__in BOOL bLoad)
{
	// 이부분은 CTestManager_EQP껄로 처리 했을 때 무방한지 TEST
	// TEST 중간에 안전센서 감지 하였을때 모든 TEST 끝난 후에 정상적으로 TEST 종료 되는 지 확인해야함.

	LRESULT lReturn = RC_OK;
	lReturn = CTestManager_EQP::StartOperation_LoadUnload(bLoad);
// 
// 	LRESULT lReturn = RC_OK;
// 
// 	if (FALSE == m_bFlag_ReadyTest)
// 	{
// 		TRACE(_T("검사가 진행 가능한 상태가 아닙니다.\n"));
// 		OnLog_Err(_T("검사가 진행 가능한 상태가 아닙니다."));
// 		return RC_NotReadyTest;
// 	}
// 	//20180326 원복
//  	lReturn = OnCheck_SaftyJIG();
//  	if (RC_OK != lReturn)
//  	{
//  		if (bLoad == Product_Unload)
//  		{
//  			OnSet_TestProgress(TP_Unloading);
//  
//  			OnFinally_LoadUnload(FALSE);
//  			OnLightBrd_PowerOff();
//  
//  			m_stInspInfo.Set_ResultCode(RC_OK);
//  
//  		}
//  		//	OnFinally_LoadUnload(bLoad);
//  		return lReturn;
//  	}
// 
// 	if (TRUE == bLoad)
// 	{
// 		lReturn = MES_CheckBarcodeValidation();
// 		if (RC_OK != lReturn)
// 		{
// 			if (RC_NoBarcode == lReturn)
// 			{
// 				AfxMessageBox(_T("No Barcode"), MB_SYSTEMMODAL);
// 			}
// 			else if (RC_MES_Err_BarcodeValidation == lReturn)
// 			{
// 				AfxMessageBox(_T("MES : Barcode is not Validation"), MB_SYSTEMMODAL);
// 			}
// 
// 			return lReturn;
// 		}
// 	}
// 
// 	// 테스트 가능 여부 판단
// 	if (TRUE == bLoad)
// 	{
// 		if (IsTesting())
// 		{
// 			TRACE(_T("검사가 진행 가능한 상태가 아닙니다.\n"));
// 			OnLog_Err(_T("Inspection is in progress."));
// 			return RC_AlreadyTesting;
// 		}
// 	}
// 
// 	// 쓰레드 핸들 리셋
// 	if (NULL != m_hThr_LoadUnload)
// 	{
// 		CloseHandle(m_hThr_LoadUnload);
// 		m_hThr_LoadUnload = NULL;
// 	}
// 
// 	stThreadParam* pParam = new stThreadParam;
// 	pParam->pOwner = this;
// 	pParam->nIndex = 0;
// 	pParam->nCondition = bLoad;
// 	pParam->nArg_2 = 0;
// 
// 	m_hThr_LoadUnload = HANDLE(_beginthreadex(NULL, 0, ThreadLoadUnload, pParam, 0, NULL));
// 
// 	::SetThreadPriority(m_hThr_LoadUnload, THREAD_PRIORITY_HIGHEST);
// 

	return lReturn;
}

//=============================================================================
// Method		: StartOperation_Inspection
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:21
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::StartOperation_Inspection(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lResult = RC_OK;
	lResult = CTestManager_EQP::StartOperation_Inspection(nParaIdx);



	return lResult;
}

//=============================================================================
// Method		: StartOperation_Manual
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bLoad
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:22
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::StartOperation_Manual(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lResult = RC_OK;
	lResult = CTestManager_EQP::StartOperation_Manual(nStepIdx, nParaIdx);

	return lResult;
}

//=============================================================================
// Method		: StartOperation_InspManual
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/20 - 11:15
// Desc.		: 이미지 검사 개별 테스트용
//=============================================================================
LRESULT CTestManager_EQP_ImgT::StartOperation_InspManual(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lResult = RC_OK;
//	lResult = CTestManager_EQP::StartOperation_InspManual(nParaIdx);



	return lResult;
}

//=============================================================================
// Method		: StopProcess_LoadUnload
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/19 - 19:35
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::StopProcess_LoadUnload()
{
	CTestManager_EQP::StopProcess_LoadUnload();

}

//=============================================================================
// Method		: StopProcess_Test_All
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/10 - 21:14
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::StopProcess_Test_All()
{
	CTestManager_EQP::StopProcess_Test_All();
	m_bFlag_UserStop = TRUE;
}

//=============================================================================
// Method		: StopProcess_InspManual
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/20 - 11:15
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::StopProcess_InspManual(__in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::StopProcess_InspManual(nParaIdx);

}

//=============================================================================
// Method		: AutomaticProcess_LoadUnload
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:23
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP_ImgT::AutomaticProcess_LoadUnload(__in BOOL bLoad)
{
	BOOL bResult = TRUE;
	bResult = CTestManager_EQP::AutomaticProcess_LoadUnload(bLoad);



	return bResult;
}

//=============================================================================
// Method		: AutomaticProcess_Test_All
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:26
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::AutomaticProcess_Test_All(__in UINT nParaIdx /*= 0*/)
{
	//CTestManager_EQP::AutomaticProcess_Test_All(nParaIdx);
	TRACE(_T("=-=-= [%d] Start Test operation =-=-=\n"), nParaIdx + 1);
	OnLog(_T("=-=-= [%d] Start Test operation =-=-="), nParaIdx + 1);

	LRESULT lReturn = RC_OK;

	__try
	{
		// 초기화
	//	OnInitial_Test_All(nParaIdx);

		lReturn = OnStart_Test_All(nParaIdx);
	}
	__finally
	{
		//for (int t = 0; t < g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt; t++)
		{
			// 작업 종료 처리
			if (RC_OK == lReturn)
			{
				OnFinally_Test_All(TRUE, nParaIdx);
				TRACE(_T("=-=-= [%d] Complete the entire test =-=-=\n"), nParaIdx + 1);
				OnLog(_T("=-=-= [%d] Complete the entire test =-=-="), nParaIdx + 1);
			}
			else
			{
				m_stInspInfo.CamInfo[nParaIdx].ResultCode = lReturn;

				OnFinally_Test_All(FALSE, nParaIdx);
				TRACE(_T("=-=-= [%d] Stop Test activity : Code -> %d =-=-=\n"), nParaIdx + 1, lReturn);
				OnLog(_T("=-=-= [%d] Stop Test activity : Code -> %d =-=-="), nParaIdx + 1), lReturn;

// 
// 				// * 검사 결과 판정
// 				OnSet_TestResult_Unit(TR_Check, nParaIdx);
// 				OnUpdate_TestReport(nParaIdx);

			
			}
		}

		if (RC_OK != lReturn)
		{
			// * Unload Product
			lReturn = StartOperation_LoadUnload(Product_Unload);
			if (RC_OK != lReturn)
			{
				// 에러 상황
			}
		}
	}

}

//=============================================================================
// Method		: AutomaticProcess_Test_InspManual
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/15 - 3:06
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::AutomaticProcess_Test_InspManual(__in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::AutomaticProcess_Test_InspManual(nParaIdx);


}

//=============================================================================
// Method		: OnInitial_LoadUnload
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:28
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnInitial_LoadUnload(__in BOOL bLoad)
{
	CTestManager_EQP::OnInitial_LoadUnload(bLoad);

	if (bLoad)
	{
		OnDOut_StartLamp(FALSE);
		OnDOut_StopLamp(TRUE);
	}
}

//=============================================================================
// Method		: OnFinally_LoadUnload
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:29
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnFinally_LoadUnload(__in BOOL bLoad)
{
	CTestManager_EQP::OnFinally_LoadUnload(bLoad);

	if (FALSE == bLoad)
	{
		OnDOut_StartLamp(TRUE);
		OnDOut_StopLamp(FALSE);
	}
}

//=============================================================================
// Method		: OnStart_LoadUnload
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:30
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnStart_LoadUnload(__in BOOL bLoad)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnStart_LoadUnload(bLoad);



	return lReturn;
}

//=============================================================================
// Method		: OnInitial_Test_All
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 19:36
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnInitial_Test_All(__in UINT nParaIdx /*= 0*/)
{
//	CTestManager_EQP::OnInitial_Test_All(nParaIdx);

	m_stInspInfo.nTestPara = nParaIdx;
	//m_bFlag_CameraChange = FALSE;

	OnSet_TestResult_Unit(TR_Testing, nParaIdx);
	//OnSet_TestResult_Unit(TR_Testing, nParaIdx);
	OnSet_TestProgress_Unit(TP_Testing, nParaIdx);

	// 검사 시작 시간 설정
	OnSet_BeginTestTime(nParaIdx);

	// 채널 카메라 데이터 초기화
	m_stInspInfo.WorklistInfo.Reset();
}

//=============================================================================
// Method		: OnFinally_Test_All
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in LRESULT lResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/23 - 13:05
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnFinally_Test_All(__in LRESULT lResult, __in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::OnFinally_Test_All(lResult, nParaIdx);


}

//=============================================================================
// Method		: OnStart_Test_All
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:30
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnStart_Test_All(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//lReturn = CTestManager_EQP::OnStart_Test_All(nParaIdx);

	// * 안전 기능 체크
	if (TR_Pass != m_stInspInfo.CamInfo[nParaIdx].nJudgeLoading)
	{
		if (RC_OK == m_stInspInfo.CamInfo[nParaIdx].ResultCode)
			lReturn = RC_LoadingFailed;
		else
			lReturn = m_stInspInfo.CamInfo[nParaIdx].ResultCode;

		// * 임시 : 안전하게 배출하기 위하여
		// KHO  이거 지워도 되는 건지? (A:지우면 언로드가 안될때 있음 )
		Sleep(3000);

		return lReturn;
	}
	//
	// * 검사 시작 위치로 제품 이동
	if (g_InspectorTable[m_InspectionType].UseMotion)
	{
		lReturn = OnMotion_MoveToTestPos(nParaIdx);
		if (RC_OK != lReturn)
		{
			// 에러 상황

			// 알람
			OnAddAlarm_F(_T("Motion : Move to Test Position [Para -> %s]"), g_szParaName[nParaIdx]);
			//return lReturn;
		}
	}

	// * Step별로 검사 진행
	enTestResult nResult = TR_Pass;

	for (UINT nCamPara = 0; nCamPara < g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt; nCamPara++)
	{
		OnInitial_Test_All(nCamPara);
	
		if (m_stInspInfo.CamInfo[Para_Left].nJudgment != TR_Pass && nCamPara == Para_Right)
		{

			m_stInspInfo.CamInfo[nCamPara].stImageQ.stECurrentData.FailData();
			m_stInspInfo.CamInfo[nCamPara].stImageQ.stOpticalCenterData.FailData();
			m_stInspInfo.CamInfo[nCamPara].stImageQ.stRotateData.FailData();
			m_stInspInfo.CamInfo[nCamPara].stImageQ.stDistortionData.FailData();
			m_stInspInfo.CamInfo[nCamPara].stImageQ.stFovData.FailData();
			m_stInspInfo.CamInfo[nCamPara].stImageQ.stDynamicData.FailData();
			m_stInspInfo.CamInfo[nCamPara].stImageQ.stDefectPixelData.FailData();
			m_stInspInfo.CamInfo[nCamPara].stImageQ.stSFRData[0].FailData();
			m_stInspInfo.CamInfo[nCamPara].stImageQ.stSFRData[1].FailData();
			m_stInspInfo.CamInfo[nCamPara].stImageQ.stSFRData[2].FailData();
			m_stInspInfo.CamInfo[nCamPara].stImageQ.stSNR_LightData.FailData();
			m_stInspInfo.CamInfo[nCamPara].stImageQ.stIntensityData.FailData();
			m_stInspInfo.CamInfo[nCamPara].stImageQ.stShadingData.FailData();
			m_stInspInfo.CamInfo[nCamPara].stImageQ.stParticleData.FailData();
			m_stInspInfo.CamInfo[Para_Right].nJudgment = TR_Fail;
		}
		else
		{

			m_Test_ResultDataView.TestResultView_AllReset(nCamPara);
			OnSetCamerParaSelect(nCamPara);

			for (UINT nTryCnt = 0; nTryCnt <= m_stInspInfo.RecipeInfo.nTestRetryCount; nTryCnt++)
			{
				// 검사기에 맞추어 검사 시작
				lReturn = StartTest_Equipment(nCamPara);

				// * 검사 결과 판정
				nResult = Judgment_Inspection(nCamPara);

				if (TRUE == m_bFlag_UserStop)
				{
					nResult = TR_Stop;
					break;
				}

				if ((TR_Pass == nResult))
				{
					break;
				}
			}
		}
		
		if (TRUE == m_bFlag_UserStop)
		{
			nResult = TR_Stop;
			break;
		}

		OnSet_TestResult_Unit(nResult, nCamPara);

		// * 검사 결과 판정 정보 세팅 및 UI표시
		if ((m_stInspInfo.RecipeInfo.ModelType == Model_MRA2) && (nCamPara == Para_Left))
		{
			OnSet_TestProgress_Unit(TP_Fin, nCamPara);
		}
		else
		{
			OnUpdate_TestReport(nParaIdx);
		}

		switch (lReturn)
		{
		case TR_Check:
			m_stInspInfo.CamInfo[nCamPara].nJudgment = TR_Check;
			break;
		case TR_Stop:
			m_stInspInfo.CamInfo[nCamPara].nJudgment = TR_Stop;
			break;
		case TR_Fail:
		case TR_Ready:
		case TR_Testing:
		case TR_Empty:
		case TR_Skip:
		case TR_Rework:
		case TR_Timeout:
			m_stInspInfo.CamInfo[nCamPara].nJudgment = TR_Fail;
		default:
			break;
		}
	}

	Judgment_Inspection_All();

	// * MES 검사 결과 보고
	if (MES_Online == m_stInspInfo.MESOnlineMode)
	{
		lReturn = MES_SendInspectionData(nParaIdx);
		if (RC_OK != lReturn)
		{
			// 에러 상황
		}
	}
	for (UINT nCamPara = 0; nCamPara < g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt; nCamPara++)
	{

		OnSet_TestProgress_Unit(TP_Fin, nCamPara);
	}

	// * Unload Product
	lReturn = StartOperation_LoadUnload(Product_Unload);
	if (RC_OK != lReturn)
	{
		// 에러 상황
	}



	return lReturn;
}

//=============================================================================
// Method		: OnInitial_Test_InspManual
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/8 - 9:11
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnInitial_Test_InspManual(__in UINT nTestItemID, __in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::OnInitial_Test_InspManual(nTestItemID, nParaIdx);


}

//=============================================================================
// Method		: OnFinally_Test_InspManual
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nTestItemID
// Parameter	: __in LRESULT lResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/8 - 9:12
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnFinally_Test_InspManual(__in UINT nTestItemID, __in LRESULT lResult, __in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::OnFinally_Test_InspManual(nTestItemID, lResult, nParaIdx);


}

//=============================================================================
// Method		: OnStart_Test_InspManual
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/15 - 3:05
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnStart_Test_InspManual(__in UINT nTestItemID, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnStart_Test_InspManual(nTestItemID, nParaIdx);


	return lReturn;
}

//=============================================================================
// Method		: Load_Product
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::Load_Product()
{
	LRESULT lReturn = RC_OK;

	//20180326 원복
	//lReturn = CTestManager_EQP::Load_Product();

#ifndef USE_TEST_MODE
	// * 안전 기능 체크 (Main Power, EMO, Area Sensor, Door Sensor)
	lReturn = OnDIn_CheckSafetyIO();
	if (RC_OK != lReturn) // 에러 상황
	{
		// 알람
		OnAddAlarm_F(_T("Motion : Safety Error [%s]"), g_szResultCode[lReturn]);
		return lReturn;
	}

	// * JIG Cover 체크 ()
	lReturn = OnDIn_CheckJIGCoverStatus();
	if (RC_OK != lReturn) // 에러 상황
	{
		// 알람
		OnAddAlarm_F(_T("Motion : JIG Cover Opened"));
		return lReturn;
	}
	
	// * 모터 제어	
	lReturn = OnMotion_LoadProduct();
	if (RC_OK != lReturn) // 에러 상황
	{
		// 알람
		OnAddAlarm_F(_T("Motion : Error Loading Product"));
		return lReturn;
	}

	// * POGO PIN 컨택 확인,  POGO PIN 사용 횟수 증가
	Increase_ConsumCount();
#endif

	// * 통신 초기화, 전원 On, 광원 On

	// * 최종 정상이 아니면 CHECK 판정 후 배출한다.
	if (RC_OK == lReturn)
	{
		// * 검사 시작 (쓰레드)
		lReturn = StartOperation_Inspection(0);
		if (RC_OK != lReturn) // 에러 상황
		{
			OnAddAlarm_F(_T("===== Can't Start Inspection ====="));
			return lReturn;
		}
	}
	else	// 에러 상황
	{
		// * 검사 판정 NG or CHECK
		m_stInspInfo.CamInfo[Para_Left].nJudgeLoading = TR_Fail;
		m_stInspInfo.CamInfo[Para_Right].nJudgeLoading = TR_Fail;
		m_stInspInfo.CamInfo[Para_Left].ResultCode = lReturn;
		m_stInspInfo.CamInfo[Para_Right].ResultCode = lReturn;

		// * 검사 결과 판정 정보 세팅 및 UI표시
		OnSet_TestResult_Unit(TR_Check, Para_Left);
		OnSet_TestResult_Unit(TR_Check, Para_Right);
		OnUpdate_TestReport(Para_Left);
		OnUpdate_TestReport(Para_Right);
		Judgment_Inspection_All();

		AutomaticProcess_LoadUnload(Product_Unload);
		return lReturn;//RC_LoadingFailed
	}


	// * 검사 시작 (쓰레드)
//	lReturn = StartOperation_Inspection(0);

	return lReturn;
}

//=============================================================================
// Method		: Unload_Product
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::Unload_Product()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::Unload_Product();

	return lReturn;
}

//=============================================================================
// Method		: StartTest_Equipment
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:21
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::StartTest_Equipment(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = StartTest_ImageTest(nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: StartTest_ImageTest
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/14 - 22:09
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::StartTest_ImageTest(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;
	INT_PTR iStepCnt = m_stInspInfo.RecipeInfo.StepInfo.GetCount();
	DWORD dwElapTime = 0;

	m_stInspInfo.CamInfo[nParaIdx].szBarcode = m_stInspInfo.szBarcodeBuf;
	m_bFlag_UserStop = FALSE;

	BOOL bError = FALSE;

	// * 설정된 스텝 진행
	for (INT nStepIdx = 0; nStepIdx < iStepCnt; nStepIdx++)
	{
		// 스텝 진행 시간 체크
		dwElapTime = Check_TimeStepStart();

		// UI 스텝 선택
		OnSet_TestStepSelect(nStepIdx, nParaIdx);

		// * 기구 이동		
		if (TRUE == pStepInfo->StepList[nStepIdx].bUseMoveY)
		{
#ifndef MOTION_NOT_USE
			lReturn = m_MotionSequence.OnActionTesting_ImgT_Y(pStepInfo->StepList[nStepIdx].nMoveY, m_stOption.Inspector.bUseBlindShutter);
#endif
		}

		// * 검사 시작
		if (OpMode_DryRun != m_stInspInfo.OperateMode)
		{
			if (TRUE == pStepInfo->StepList[nStepIdx].bTest)
			{
				// MRA2 모델의 경우 언로딩 동작을 하지 안도록 한다, 전체 루틴에서만 동작 될 수 있도록 한다. KHO 확인 필요
				if (Model_MRA2 == m_stInspInfo.RecipeInfo.ModelType && TI_ImgT_Motion_Load == pStepInfo->StepList[nStepIdx].nTestItem && nParaIdx == Para_Left)
				{
					lReturn = RC_OK;
				}
				else
				{
					lReturn = StartTest_ImageTest_TestItem(pStepInfo->StepList[nStepIdx].nTestItem, nStepIdx, nParaIdx);
				}
			}
		}

		// 스텝 진행 시간 체크
		dwElapTime = Check_TimeStepStop(dwElapTime);

		// 진행 시간 설정
		m_stInspInfo.Set_ElapTime(nParaIdx, nStepIdx, dwElapTime);

		// 스텝 결과 UI에 표시
		OnSet_TestStepResult(nStepIdx, nParaIdx);

		// * 검사 프로그레스		
		OnSet_TestProgressStep((UINT)iStepCnt, nStepIdx + 1);

		// * 검사 중단 여부 판단
		if (RC_OK != lReturn)
		{
			// 에러 상황
			switch (pStepInfo->StepList[nStepIdx].nTestItem)
			{
			case TI_ImgT_Initialize_Test:
				nStepIdx = (INT)iStepCnt-1;
				bError = TRUE;
				break;
			default:
				break;
			}
		}

		// UI 갱신 딜레이
		Sleep(100);

		// 스텝 딜레이
		if (0 < pStepInfo->StepList[nStepIdx].dwDelay)
		{
			Sleep(pStepInfo->StepList[nStepIdx].dwDelay);
		}

		if (TRUE == m_bFlag_UserStop)
		{
			m_stInspInfo.CamInfo[nParaIdx].nJudgment = TR_Stop;
			bError = TRUE;
			break;
		}
	}

	// 검사 중단시 Finalize 수행
	if (bError)
	{
		if (Model_MRA2 == m_stInspInfo.RecipeInfo.ModelType)
		{
			_TI_Cm_Finalize_Error(Para_Left);
			_TI_Cm_Finalize_Error(Para_Right);
		}
		else
		{
			_TI_Cm_Finalize_Error(Para_Left);
		}

		lReturn = TR_Check;

		// * 최종 판정 CHECK 처리
		OnSet_TestResult_Unit(TR_Check, Para_Left);
	}

	return lReturn;
}


//=============================================================================
// Method		: AutomaticProcess_TestItem
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nCornerStep
// Qualifier	:
// Last Update	: 2017/12/12 - 13:31
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::AutomaticProcess_TestItem(__in UINT nParaIdx, __in UINT nTestItemID, __in UINT nStepIdx, __in UINT nCornerStep /*= 0*/)
{
	// 	if (TI_2D_Detect_CornerExt == nTestItemID)
	// 	{
	// 		LRESULT lReturn = _TI_2DCAL_CornerExt(nStepIdx, nCornerStep, nParaIdx);
	// 	}
}


//=============================================================================
// Method		: StartTest_ImageTest_TestItem
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/4 - 11:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::StartTest_ImageTest_TestItem(__in UINT nTestItemID, __in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;
	
	// Overlay Draw
	OnSetOverlayInfo_ImgT(nTestItemID);

	TestResultView_Reset(nTestItemID, nParaIdx);

//#ifdef SET_GWANGJU
	//광원 Register 변경 
	if (/*(m_stInspInfo.RecipeInfo.ModelType == Model_OMS_Front_Set) || (m_stInspInfo.RecipeInfo.ModelType == Model_OMS_Front) ||*/ (m_stInspInfo.RecipeInfo.ModelType == Model_OMS_Entry))
	{
		UINT nRegisterMode = Reg_Chg_No;
		
		switch (nTestItemID)
		{
		//-원본 Register
		case TI_ImgT_Fn_ECurrent:
		case TI_ImgT_Fn_OpticalCenter:
		case TI_ImgT_Fn_FOV:
		case TI_ImgT_Fn_SFR:
		case TI_ImgT_Fn_Distortion:
		case TI_ImgT_Fn_Rotation:
		case TI_ImgT_Fn_3DDepth:
			nRegisterMode = Reg_Chg_Normal;
			if (m_stInspInfo.RecipeInfo.stLVDSInfo.strI2CFileName.IsEmpty())
			{
				nRegisterMode = Reg_Chg_No;
			}
			break;
			//-이물 광원 Register
		case TI_ImgT_Fn_Stain:
		case TI_ImgT_Fn_Particle_Entry:
		case TI_ImgT_Fn_Particle_SNR:
		case TI_ImgT_Fn_DefectPixel:
		case TI_ImgT_Fn_Intensity:
		case TI_ImgT_Fn_Shading:
		case TI_ImgT_Fn_SNR_Light:
		case TI_ImgT_Fn_HotPixel:
		case TI_ImgT_Fn_FPN:
			nRegisterMode = Reg_Chg_Dust;
			if (m_stInspInfo.RecipeInfo.stLVDSInfo.strI2CFileName_2.IsEmpty())
			{
				nRegisterMode = Reg_Chg_No;
			}
			break;

		default:
			break;
		}
		if (nRegisterMode != Reg_Chg_No)
		{
			if (m_nRegisterChangeMode != nRegisterMode)
			{
				lReturn = _TI_Cm_RegisterChangeMode(nParaIdx, nRegisterMode);

				if (lReturn == RC_OK)
				{
					m_nRegisterChangeMode = nRegisterMode;
				}
			}
		}
	}
//#endif
	lReturn = RC_OK;

	///nTESTItem 3D Depth/ 아닌지 check
	//3D야..
//#ifdef SET_GWANGJU
	UINT n3D_DepthMode = 2;

	if (m_stInspInfo.RecipeInfo.ModelType == Model_OMS_Entry)
	{
		switch (nTestItemID)
		{
		case TI_ImgT_CaptureImage:
		case TI_ImgT_Initialize_Test:
		case TI_ImgT_Fn_ECurrent:
		case TI_ImgT_Fn_OpticalCenter:
		case TI_ImgT_Fn_FOV:
		case TI_ImgT_Fn_SFR:
		case TI_ImgT_Fn_Distortion:
		case TI_ImgT_Fn_Rotation:
		case TI_ImgT_Fn_Stain:
		case TI_ImgT_Fn_Particle_Entry:
		case TI_ImgT_Fn_Particle_SNR:
		case TI_ImgT_Fn_DefectPixel:
		case TI_ImgT_Fn_Intensity:
		case TI_ImgT_Fn_Shading:
		case TI_ImgT_Fn_SNR_Light:
		case TI_ImgT_Fn_TemperatureSensor:
			n3D_DepthMode = 0;
			break;

		case TI_ImgT_Fn_HotPixel:
		case TI_ImgT_Fn_FPN:
			n3D_DepthMode = 0;
			break;
		case TI_ImgT_Fn_3DDepth:
			n3D_DepthMode = 1;
			break;
		case TI_ImgT_Fn_EEPROM_Verify:
		case TI_ImgT_Finalize_Test:
		case TI_ImgT_Re_ECurrent:
		case TI_ImgT_Re_OpticalCenterX:
		case TI_ImgT_Re_OpticalCenterY:
		case TI_ImgT_Re_FOV_Hor:
		case TI_ImgT_Re_FOV_Ver:
		//case TI_ImgT_Re_G0_SFR:
		case TI_ImgT_Re_Distortion:
		case TI_ImgT_Re_Rotation:
		case TI_ImgT_Re_DynamicRange:
		case TI_ImgT_Re_SNR_BW:
		case TI_ImgT_Re_Stain:
		case TI_ImgT_Re_Particle_Entry:
		case TI_ImgT_Re_DefectPixel:
		case TI_ImgT_Re_Intencity:
		case TI_ImgT_Re_Shading:
		case TI_ImgT_Re_SNR_Light:
		case TI_ImgT_Re_Hot_Pixel:
		case TI_ImgT_Re_Fixed_Pattern:
		case TI_ImgT_Re_3D_Depth:
		case TI_ImgT_Re_EEPROM_Verify:
		case TI_ImgT_Re_TemperatureSensor:
			n3D_DepthMode = 2;
			break;
		default:
			break;
		}

		if (n3D_DepthMode != 2)
		{
			int nVideoMode = -1;
			switch (n3D_DepthMode){
			case 1://3D
				nVideoMode = Crop_EvenFrame;
				break;
			default:
				nVideoMode = Crop_OddFrame;
				break;
			}

			ST_DAQOption stDaqOp;

			m_Device.DAQ_LVDS.GetDAQOption(0, stDaqOp);


			if (stDaqOp.nCropFrame != nVideoMode)
			{

				m_Device.DAQ_LVDS.SetVideoChangeMode(0, TRUE);
				DoEvents(300);
				stDaqOp.nCropFrame = (enCropFrame)nVideoMode;
				m_Device.DAQ_LVDS.SetDAQOption_All(stDaqOp);
				m_Device.DAQ_LVDS.SetVideoChangeMode(0, FALSE);
				DoEvents(100);

			}
		}
	}

	
//#endif

	

	//영상 전환
	int nTypeCnt = 0;
	switch (nTestItemID)
	{
	case TI_ImgT_CaptureImage:
		lReturn = _TI_Cm_CaptureImage(nStepIdx, nParaIdx, TRUE);
		break;
	case TI_ImgT_Motion_Load:
		lReturn = _TI_ImgT_MoveStage_Load(nStepIdx, nParaIdx);
		break;
	case TI_ImgT_Motion_Chart:
		lReturn = _TI_ImgT_MoveStage_Chart(nStepIdx, nParaIdx);
		break;
	case TI_ImgT_Motion_Particle:
		lReturn = _TI_ImgT_MoveStage_Particle(nStepIdx, nParaIdx);
		break;
	case TI_ImgT_Initialize_Test:
		lReturn = _TI_Cm_Initialize(nStepIdx, nParaIdx);
		break;
	case TI_ImgT_Finalize_Test:
		lReturn = _TI_Cm_Finalize(nStepIdx, nParaIdx);
		break;
	case TI_ImgT_Fn_ECurrent:
		lReturn = _TI_ImgT_Pro_Current(nStepIdx, nParaIdx);
		break;
	case TI_ImgT_Fn_OpticalCenter:
		lReturn = _TI_ImgT_Pro_OpticalCenter(nStepIdx, nParaIdx);
		break;
	case TI_ImgT_Fn_FOV:
		lReturn = _TI_ImgT_Pro_FOV(nStepIdx, nParaIdx);
		break;
	case TI_ImgT_Fn_SFR:
	case TI_ImgT_Fn_SFR_2:
	case TI_ImgT_Fn_SFR_3:
		nTypeCnt = nTestItemID - TI_ImgT_Fn_SFR;
		lReturn = _TI_ImgT_Pro_SFR(nStepIdx, nParaIdx, nTypeCnt);
		break;
	case TI_ImgT_Fn_Distortion:
		lReturn = _TI_ImgT_Pro_Distortion(nStepIdx, nParaIdx);
		break;
	case TI_ImgT_Fn_Rotation:
		lReturn = _TI_ImgT_Pro_Rotation(nStepIdx, nParaIdx);
		break;
	case TI_ImgT_Fn_Stain:
		lReturn = _TI_ImgT_Pro_Particle(nStepIdx, nParaIdx);
		break;
	case TI_ImgT_Fn_Particle_SNR:
		lReturn = _TI_ImgT_Pro_Particle_SNR(nStepIdx, nParaIdx);
		break;
	case TI_ImgT_Fn_DefectPixel:
		lReturn = _TI_ImgT_Pro_DefectPixel(nStepIdx, nParaIdx);
		break;
	case TI_ImgT_Fn_Intensity:
		lReturn = _TI_ImgT_Pro_Intencity(nStepIdx, nParaIdx);
		break;
	case TI_ImgT_Fn_Shading:
		lReturn = _TI_ImgT_Pro_Shading(nStepIdx, nParaIdx);
		break;
	case TI_ImgT_Fn_SNR_Light:
		lReturn = _TI_ImgT_Pro_SNR_Light(nStepIdx, nParaIdx);
		break;
//#ifdef SET_GWANGJU
	case TI_ImgT_Fn_HotPixel:
		lReturn = _TI_ImgT_HotPixel(nStepIdx, nParaIdx);
		break;
	case TI_ImgT_Fn_FPN:
		lReturn = _TI_ImgT_FPN(nStepIdx, nParaIdx);
		break;
	case TI_ImgT_Fn_3DDepth:
		lReturn = _TI_ImgT_3D_Depth(nStepIdx, nParaIdx);
		break;
	case TI_ImgT_Fn_EEPROM_Verify:
		lReturn = _TI_ImgT_EEPRON_Verify(nStepIdx, nParaIdx);
		break;
//#endif
	case TI_ImgT_Fn_TemperatureSensor:
		lReturn = _TI_ImgT_TemperatureSensor(nStepIdx, nParaIdx);
		break;

	case TI_ImgT_Fn_Particle_Entry:
		lReturn = _TI_ImgT_Pro_Particle_Entry(nStepIdx, nParaIdx);
		break;
		
	case TI_ImgT_Re_ECurrent:
	case TI_ImgT_Re_OpticalCenterX:
	case TI_ImgT_Re_OpticalCenterY:
	case TI_ImgT_Re_FOV_Hor:
	case TI_ImgT_Re_FOV_Ver:
	case TI_ImgT_Re_SFR:
	case TI_ImgT_Re_Group_SFR:
	case TI_ImgT_Re_Tilt_SFR:
	//case TI_ImgT_Re_G0_SFR:
	//case TI_ImgT_Re_G1_SFR:
	//case TI_ImgT_Re_G2_SFR:
	//case TI_ImgT_Re_G3_SFR:
	//case TI_ImgT_Re_G4_SFR:
	//case TI_ImgT_Re_X1Tilt_SFR:
	//case TI_ImgT_Re_X2Tilt_SFR:
	//case TI_ImgT_Re_Y1Tilt_SFR:
	//case TI_ImgT_Re_Y2Tilt_SFR:
	case TI_ImgT_Re_SFR_2:
	case TI_ImgT_Re_Group_SFR_2:
	case TI_ImgT_Re_Tilt_SFR_2:
	//case TI_ImgT_Re_G0_SFR_2:
	//case TI_ImgT_Re_G1_SFR_2:
	//case TI_ImgT_Re_G2_SFR_2:
	//case TI_ImgT_Re_G3_SFR_2:
	//case TI_ImgT_Re_G4_SFR_2:
	//case TI_ImgT_Re_X1Tilt_SFR_2:
	//case TI_ImgT_Re_X2Tilt_SFR_2:
	//case TI_ImgT_Re_Y1Tilt_SFR_2:
	//case TI_ImgT_Re_Y2Tilt_SFR_2:
	case TI_ImgT_Re_SFR_3:
	case TI_ImgT_Re_Group_SFR_3:
	case TI_ImgT_Re_Tilt_SFR_3:
	//case TI_ImgT_Re_G0_SFR_3:
	//case TI_ImgT_Re_G1_SFR_3:
	//case TI_ImgT_Re_G2_SFR_3:
	//case TI_ImgT_Re_G3_SFR_3:
	//case TI_ImgT_Re_G4_SFR_3:
	//case TI_ImgT_Re_X1Tilt_SFR_3:
	//case TI_ImgT_Re_X2Tilt_SFR_3:
	//case TI_ImgT_Re_Y1Tilt_SFR_3:
	//case TI_ImgT_Re_Y2Tilt_SFR_3:
	case TI_ImgT_Re_Distortion:
	case TI_ImgT_Re_Rotation:
	case TI_ImgT_Re_DynamicRange:
	case TI_ImgT_Re_SNR_BW:
	case TI_ImgT_Re_Stain:
	case TI_ImgT_Re_Particle_Entry:
	case TI_ImgT_Re_DefectPixel:
	case TI_ImgT_Re_Intencity:
	case TI_ImgT_Re_Shading:
	case TI_ImgT_Re_SNR_Light:
//#ifdef SET_GWANGJU
	case TI_ImgT_Re_Hot_Pixel:
	case TI_ImgT_Re_Fixed_Pattern:
	case TI_ImgT_Re_3D_Depth:
	case TI_ImgT_Re_EEPROM_Verify:
//#endif
	case TI_ImgT_Re_TemperatureSensor:
		
		lReturn = _TI_ImgT_JudgeResult(nStepIdx, nTestItemID, nParaIdx);
		break;
	default:
		break;
	}

	switch (nTestItemID)
	{
	case TI_ImgT_Fn_ECurrent:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stImageQ.stECurrentData);
		break;
	case TI_ImgT_Fn_OpticalCenter:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stImageQ.stOpticalCenterData);
		break;
	case TI_ImgT_Fn_FOV:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stImageQ.stFovData);
		break;
	case TI_ImgT_Fn_SFR:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[0]);
		break;
	case TI_ImgT_Fn_SFR_2:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[1]);
		break;
	case TI_ImgT_Fn_SFR_3:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[2]);
		break;
	case TI_ImgT_Fn_Distortion:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDistortionData);
		break;
	case TI_ImgT_Fn_Rotation:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stImageQ.stRotateData);
		break;
	case TI_ImgT_Fn_Stain:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stImageQ.stParticleData);
		break;
	case TI_ImgT_Fn_Particle_SNR:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData);
		break;
	case TI_ImgT_Fn_DefectPixel:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDefectPixelData);
		break;
	case TI_ImgT_Fn_Intensity:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stImageQ.stIntensityData);
		break;
	case TI_ImgT_Fn_Shading:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stImageQ.stShadingData);
		break;
	case TI_ImgT_Fn_SNR_Light:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSNR_LightData);
		break;
//#ifdef SET_GWANGJU
	case TI_ImgT_Fn_HotPixel:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stImageQ.stHotPixelData);
		break;
	case TI_ImgT_Fn_FPN:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stImageQ.stFPNData);
		break;
	case TI_ImgT_Fn_3DDepth:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stImageQ.st3D_DepthData);
		break;
	case TI_ImgT_Fn_EEPROM_Verify:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData);
		break;
//#endif
	case TI_ImgT_Fn_TemperatureSensor:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stImageQ.stTemperatureSensorData);
		break;

	case TI_ImgT_Fn_Particle_Entry:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stImageQ.stParticle_EntryData);
		break;
	default:
		break;
	}


	//-이미지 저장

	if (m_stOption.Inspector.bSaveImage_RGB)
	{
		switch (nTestItemID)
		{
		case TI_ImgT_Fn_ECurrent:
		case TI_ImgT_Fn_OpticalCenter:
		case TI_ImgT_Fn_FOV:
		case TI_ImgT_Fn_SFR:
		case TI_ImgT_Fn_SFR_2:
		case TI_ImgT_Fn_SFR_3:
		case TI_ImgT_Fn_Distortion:
		case TI_ImgT_Fn_Rotation:
		case TI_ImgT_Fn_Stain:
			//case TI_ImgT_Fn_Particle_SNR: //사진 찍으면서 진행함.
			//case TI_ImgT_Fn_DefectPixel://사진 찍으면서 진행함.
		case TI_ImgT_Fn_Intensity:
		case TI_ImgT_Fn_Shading:
		case TI_ImgT_Fn_SNR_Light:
//#ifdef SET_GWANGJU
		case TI_ImgT_Fn_HotPixel:
		case TI_ImgT_Fn_FPN:
		case TI_ImgT_Fn_3DDepth:
		case TI_ImgT_Fn_TemperatureSensor:
		case TI_ImgT_Fn_Particle_Entry:
	//	case TI_ImgT_Fn_EEPROM_Verify:
//#endif

			m_bPicCaptureMode = TRUE;
			for (int t = 0; t < 100; t++)
			{
				Sleep(50);
				if (m_bPicCaptureMode == FALSE)
				{
					break;
				}
			}
			break;
		default:
			break;
		}
	}

	switch (nTestItemID)
	{
	case TI_ImgT_Fn_ECurrent:
	case TI_ImgT_Fn_OpticalCenter:
	case TI_ImgT_Fn_FOV:
	case TI_ImgT_Fn_SFR:
	case TI_ImgT_Fn_SFR_2:
	case TI_ImgT_Fn_SFR_3:
	case TI_ImgT_Fn_Distortion:
	case TI_ImgT_Fn_Rotation:
	case TI_ImgT_Fn_Stain:
	case TI_ImgT_Fn_Particle_SNR:
	case TI_ImgT_Fn_DefectPixel:
	case TI_ImgT_Fn_Intensity:
	case TI_ImgT_Fn_Shading:
	case TI_ImgT_Fn_SNR_Light:
//#ifdef SET_GWANGJU
	case TI_ImgT_Fn_HotPixel:
	case TI_ImgT_Fn_FPN:
	case TI_ImgT_Fn_3DDepth:
	case TI_ImgT_Fn_EEPROM_Verify:

//#endif
	case TI_ImgT_Fn_TemperatureSensor:
	case TI_ImgT_Fn_Particle_Entry:

		EachTest_ResultDataSave(nTestItemID, nParaIdx);

		break;
	default:
		break;
	}
	return lReturn;
}

//=============================================================================
// Method		: StartTest_Particle_TestItem
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:32
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::StartTest_Particle_TestItem(__in UINT nTestItemID, __in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

// 	switch (nTestItemID)
// 	{
// 	default:
// 		break;
// 	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_Initialize
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/6 - 21:56
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_Cm_Initialize(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_Initialize(nStepIdx, nParaIdx);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Initialize_Test].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_nRegisterChangeMode = Reg_Chg_Normal;
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_Finalize
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/13 - 13:54
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_Cm_Finalize(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_Finalize(nStepIdx, nParaIdx);

	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Finalize_Test].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}


//=============================================================================
// Method		: _TI_Cm_Finalize_Error
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/6/10 - 10:50
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_Cm_Finalize_Error(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_Finalize_Error(nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_MeasurCurrent
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/13 - 13:54
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_Cm_MeasurCurrent(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_MeasurCurrent(nStepIdx, nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_MeasurVoltage
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:38
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_Cm_MeasurVoltage(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_MeasurVoltage(nStepIdx, nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_CaptureImage
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Parameter	: __in BOOL bSaveImage
// Parameter	: __in LPCTSTR szFileName
// Qualifier	:
// Last Update	: 2018/2/23 - 16:32
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_Cm_CaptureImage(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/, __in BOOL bSaveImage /*= FALSE*/, __in LPCTSTR szFileName /*= NULL*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_CaptureImage(nStepIdx, nParaIdx, bSaveImage, szFileName);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_CameraRegisterSet
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nRegisterType
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:37
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_Cm_CameraRegisterSet(__in UINT nStepIdx, __in UINT nRegisterType, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_CameraRegisterSet(nStepIdx, nRegisterType, nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_CameraAlphaSet
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nAlpha
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:37
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_Cm_CameraAlphaSet(__in UINT nStepIdx, __in UINT nAlpha, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_CameraAlphaSet(nStepIdx, nAlpha, nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_CameraPowerOnOff
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in enPowerOnOff nOnOff
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:37
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_Cm_CameraPowerOnOff(__in UINT nStepIdx, __in enPowerOnOff nOnOff, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_CameraPowerOnOff(nStepIdx, nOnOff, nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Get_ImageBuffer_CaptureImage
// Access		: protected  
// Returns		: BOOL
// Parameter	: enImageMode eImageMode
// Parameter	: __in UINT nParaIdx
// Parameter	: __out UINT & nImgW
// Parameter	: __out UINT & nImgH
// Parameter	: __in BOOL bSaveImage
// Parameter	: __in LPCTSTR szFileName
// Qualifier	:
// Last Update	: 2018/3/1 - 14:16
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP_ImgT::_TI_Get_ImageBuffer_CaptureImage(enImageMode eImageMode, __in UINT nParaIdx, __out UINT &nImgW, __out UINT &nImgH, __out CString &strTestFileName, __in BOOL bSaveImage /*= FALSE*/, __in LPCTSTR szFileName /*= NULL*/)
{
	int nWidth = 0;
	int nHeight = 0;

	// 이미지 캡쳐 (Request Capture)
	if (ImageMode_LiveCam == eImageMode)
	{
		if (!m_Device.DAQ_LVDS.GetSignalStatus(nParaIdx))
		{
			nImgW = 0;
			nImgH = 0;
			return FALSE;
		}

		ST_VideoRGB* pstVideo = m_Device.DAQ_LVDS.GetRecvVideoRGB(nParaIdx);
		LPWORD lpwImage = (LPWORD)m_Device.DAQ_LVDS.GetSourceFrameData(nParaIdx);
		LPBYTE lpbyRGBImage = m_Device.DAQ_LVDS.GetRecvRGBData(nParaIdx);

		nWidth  = pstVideo->m_dwWidth;
		nHeight = pstVideo->m_dwHeight;

		// 이미지 버퍼에 복사
		memcpy(m_stImageBuf[nParaIdx].lpwImage_16bit, lpwImage, nWidth * nHeight * sizeof(WORD));
		memcpy(m_stImageBuf[nParaIdx].lpbyImage_8bit, lpbyRGBImage, nWidth * nHeight * 3);
	}
	else{

		if (m_stImageMode.szImagePath.IsEmpty())
		{
			nImgW = 0;
			nImgH = 0;
			return FALSE;
		}
		IplImage *LoadImage = cvLoadImage((CStringA)m_stImageMode.szImagePath,-1);

		nWidth = LoadImage->width;
		nHeight = LoadImage->height;

		if (nWidth <1 || nHeight < 1)
		{
			nImgW = 0;
			nImgH = 0;
			return FALSE;
		}
		m_stImageBuf[nParaIdx].AssignMem(nWidth, nHeight);
		memcpy(m_stImageBuf[nParaIdx].lpwImage_16bit, LoadImage->imageData, LoadImage->width*LoadImage->height * 2);

		IplImage *LoadImage_8bit = cvCreateImage(cvSize(nWidth, nHeight),IPL_DEPTH_8U, 3);


		cv::Mat Bit16Mat(LoadImage->height, LoadImage->width, CV_16UC1, LoadImage->imageData);
		Bit16Mat.convertTo(Bit16Mat, CV_8UC1, 0.2, 0);

		cv::Mat rgb8BitMat(LoadImage_8bit->height, LoadImage_8bit->width, CV_8UC3, LoadImage_8bit->imageData);
		cv::cvtColor(Bit16Mat, rgb8BitMat, CV_GRAY2BGR);


		memcpy(m_stImageBuf[nParaIdx].lpbyImage_8bit, LoadImage_8bit->imageData, LoadImage->width*LoadImage->height * 3);

		cvReleaseImage(&LoadImage);
		cvReleaseImage(&LoadImage_8bit);
		
	}

	// 이미지 저장
	if (bSaveImage)
	{
		CString szPath;
		CString szImgFileName;


		SYSTEMTIME tmLocal;
		GetLocalTime(&tmLocal);
		if (szFileName)
		{
			CString strDate;
			strDate.Format(_T("_%02d%02d_%02d%02d%02d"), tmLocal.wMonth, tmLocal.wDay, tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);

			szImgFileName = (szFileName + strDate);
		}
		else
		{
			szImgFileName.Format(_T("%02d%02d_%02d%02d%02d"), tmLocal.wMonth, tmLocal.wDay, tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);

		}


		CString strPath;
		//-바코드
		if (FALSE==m_stInspInfo.CamInfo[0].szBarcode.IsEmpty())
		{
			strPath.Format(_T("%s/%s"), m_stInspInfo.Path.szImage, m_stInspInfo.CamInfo[0].szBarcode);
		}
		else{
			strPath.Format(_T("%s/No Barcode"), m_stInspInfo.Path.szImage);
		}
		MakeDirectory(strPath);

		if (1 <  g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt)
			szPath.Format(_T("%s/Capatue_%s_%s"), strPath, g_szParaName[nParaIdx], szImgFileName);
		else
			szPath.Format(_T("%s/Capatue_%s"), strPath, szImgFileName);

		strTestFileName = szPath;
		if (m_stOption.Inspector.bSaveImage_RAW)
		{
			CString szFullPath;
			szFullPath.Format(_T("%s\\%s.raw"), szPath, szImgFileName);
			m_Device.DAQ_LVDS.SaveRawImage(nParaIdx, szFullPath.GetBuffer(0));
		}


		szPath += _T(".png");
		if (m_stOption.Inspector.bSaveImage_Gray12)
		{
			OnSaveImage_Gray16(szPath.GetBuffer(0), m_stImageBuf[nParaIdx].lpwImage_16bit, nWidth, nHeight);
		}
			
	}

	

	nImgW = nWidth;
	nImgH = nHeight;

	m_szImageFileName = strTestFileName;
	return TRUE;

}

//=============================================================================
// Method		: _TI_ImgT_MoveStage_Load
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/7 - 13:46
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_MoveStage_Load(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = m_MotionSequence.OnActionLoad();

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Motion_Load].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	return lReturn;
}

//=============================================================================
// Method		: _TI_ImgT_MoveStage_Chart
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 9:34
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_MoveStage_Chart(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = m_MotionSequence.OnActionStandby_ImgT(m_stOption.Inspector.bUseBlindShutter);

	if (RC_OK != lReturn)
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		return lReturn;
	}

	lReturn = OnMotion_ChartTestProduct(nParaIdx);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Motion_Chart].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		OnAddAlarm_F(_T("Motion : Move to Chart [Para -> %s]"), g_szParaName[nParaIdx]);

	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_ImgT_MoveStage_Particle
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 9:33
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_MoveStage_Particle(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = m_MotionSequence.OnActionStandby_ImgT(m_stOption.Inspector.bUseBlindShutter);

	if (RC_OK != lReturn)
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		return lReturn;
	}

	lReturn = OnMotion_ParticleTestProduct(nParaIdx);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Motion_Particle].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		OnAddAlarm_F(_T("Motion : Move to Particle Test Stage [Para -> %s]"), g_szParaName[nParaIdx]);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_ImgT_GetOpticalCenter
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/3 - 11:55
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_Pro_OpticalCenter(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//*** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;

	UINT nImgW = 0, nImgH= 0;
	
	// 이미지 버퍼에 복사
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szTestFileName,TRUE,_T("OpticalCenter"))){
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stOpticalCenterData.FailData();
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_NoImage;
		return lReturn;
	}

	// 광축
	if (Para_Left == nParaIdx)
	{
		lReturn = m_tm_Test.CenterPointFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stImageQ.stOpticalCenterOpt, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stOpticalCenterData);
	}
	else
	{
		lReturn = m_tm_Test.CenterPointFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stImageQ.stOpticalCenterOpt, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stOpticalCenterData, m_stInspInfo.CamInfo[Para_Left].stImageQ.stOpticalCenterData.iStandPosDevX, m_stInspInfo.CamInfo[Para_Left].stImageQ.stOpticalCenterData.iStandPosDevY, nParaIdx);
	}

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_OpticalCenter].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_ImgT_Pro_Current
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:36
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_Pro_Current(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//*** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;

	UINT nImgW = 0, nImgH = 0;

	ST_CamBrdCurrent stCurrent;

	lReturn = m_Device.PCBCamBrd[nParaIdx].Send_GetCurrent(stCurrent);

	if (RC_OK != lReturn){
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stECurrentData.FailData();
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		return lReturn;
	}

	m_stInspInfo.CamInfo[nParaIdx].stImageQ.stECurrentData.nResult = TR_Pass;

	UINT nCurrentCnt = 2;

	if ((m_stInspInfo.RecipeInfo.ModelType == Model_OMS_Front_Set) || (m_stInspInfo.RecipeInfo.ModelType == Model_OMS_Front) || (m_stInspInfo.RecipeInfo.ModelType == Model_OMS_Entry))
		nCurrentCnt = Spec_ECurrent_Max;

	for (UINT nCh = 0; nCh < nCurrentCnt; nCh++)
	{
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stECurrentData.dbValue[nCh] = stCurrent.fOutCurrent[nCh] * m_stInspInfo.RecipeInfo.stImageQ.stECurrentOpt.dbOffset[nParaIdx][nCh];

		if (m_stInspInfo.RecipeInfo.ModelType == Model_OMS_Entry)
		{
			m_stInspInfo.CamInfo[nParaIdx].stImageQ.stECurrentData.dbValue[nCh] *= 10;
		}

		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stECurrentData.nEachResult[nCh] = m_tm_Test.Get_MeasurmentData(m_stInspInfo.RecipeInfo.stImageQ.stECurrentOpt.stSpec_Min[nCh].bEnable, m_stInspInfo.RecipeInfo.stImageQ.stECurrentOpt.stSpec_Max[nCh].bEnable, m_stInspInfo.RecipeInfo.stImageQ.stECurrentOpt.stSpec_Min[nCh].dbValue, m_stInspInfo.RecipeInfo.stImageQ.stECurrentOpt.stSpec_Max[nCh].dbValue, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stECurrentData.dbValue[nCh]);
	}
	
	m_stInspInfo.CamInfo[nParaIdx].stImageQ.stECurrentData.nResult = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stECurrentData.nEachResult[Spec_ECurrent_CH1] & m_stInspInfo.CamInfo[nParaIdx].stImageQ.stECurrentData.nEachResult[Spec_ECurrent_CH2];

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_ECurrent].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_ImgT_Pro_FOV
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:39
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_Pro_FOV(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//*** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;
	UINT nImgW = 0, nImgH = 0;
	// 이미지 버퍼에 복사
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szTestFileName, TRUE, _T("FOV")))
	{
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stFovData.FailData();
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_NoImage;
		return lReturn;
	}

	lReturn = m_tm_Test.FovFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, nImgW, nImgH,m_stInspInfo.RecipeInfo.stImageQ.stFovOpt, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stFovData);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_FOV].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_ImgT_Pro_SFR
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:39
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_Pro_SFR(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/, __in UINT nTypeCnt)
{
	LRESULT lReturn = RC_OK;

	//*** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;

	UINT nImgW = 0;
	UINT nImgH = 0;
	
	// 이미지 버퍼에 캡쳐
	if (0 < m_stInspInfo.RecipeInfo.stImageQ.stSFROpt[nTypeCnt].nCaptureCnt)
	{
		nImgW = g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Img_Width;
		nImgH = g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Img_Height;

		LPWORD	lpw16bit_Std	= new WORD[nImgW * nImgH];		// 12bit Gray 평균값
		LPWORD	lpw16bit_Sum	= new WORD[nImgW * nImgH];		// 12bit Gray 합계
		LPBYTE	lpby8bit_Std	= new BYTE[nImgW * nImgH * 3];	// 8bit RGB 평균값
		LPWORD	lpw8bit_Sum		= new WORD[nImgW * nImgH * 3];	// 8bit RGB 합계

		memset(lpw16bit_Std,	0x00, nImgW * nImgH * sizeof(WORD));
		memset(lpw16bit_Sum,	0x00, nImgW * nImgH * sizeof(WORD));
		memset(lpby8bit_Std,	0x00, nImgW * nImgH * 3 * sizeof(BYTE));
		memset(lpw8bit_Sum,		0x00, nImgW * nImgH * 3 * sizeof(WORD));
		
		for (UINT nIdx = 0; nIdx < m_stInspInfo.RecipeInfo.stImageQ.stSFROpt[nTypeCnt].nCaptureCnt; nIdx++)
		{
			if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szTestFileName, FALSE, _T("SFR")))
			{
				m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
				m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].FailData();
				lReturn = RC_NoImage;

				delete[]lpw16bit_Sum;
				delete[]lpw8bit_Sum;
				delete[]lpby8bit_Std;
				delete[]lpw16bit_Std;

				return lReturn;
			}

			// 이미지 머지
			Sleep(m_stInspInfo.RecipeInfo.stImageQ.stSFROpt[nTypeCnt].nCaptureDelay);

			// 영상 누적
			for (UINT iCnt = 0; iCnt < nImgW * nImgH * 3; iCnt++)
			{
				lpw8bit_Sum[iCnt] += (WORD)m_stImageBuf[nParaIdx].lpbyImage_8bit[iCnt];
			}

			for (UINT iCnt = 0; iCnt < nImgW * nImgH; iCnt++)
			{
				lpw16bit_Sum[iCnt] += (m_stImageBuf[nParaIdx].lpwImage_16bit[iCnt]);
			}
		}

		// 평균 구하기
		for (UINT iCnt = 0; iCnt < nImgW * nImgH * 3; iCnt++)
		{
			lpby8bit_Std[iCnt] = lpw8bit_Sum[iCnt] / m_stInspInfo.RecipeInfo.stImageQ.stSFROpt[nTypeCnt].nCaptureCnt;
		}

		for (UINT iCnt = 0; iCnt < nImgW * nImgH; iCnt++)
		{
			lpw16bit_Std[iCnt] = lpw16bit_Sum[iCnt] / m_stInspInfo.RecipeInfo.stImageQ.stSFROpt[nTypeCnt].nCaptureCnt;
		}

		CString szFileName = _T("SFR");
		CString szPath;
		CString szImgFileName;

		SYSTEMTIME tmLocal;
		GetLocalTime(&tmLocal);
		if (szFileName)
		{
			CString strDate;
			strDate.Format(_T("_%02d%02d_%02d%02d%02d"), tmLocal.wMonth, tmLocal.wDay, tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);

			szImgFileName = (szFileName + strDate);
		}
		else
		{
			szImgFileName.Format(_T("%02d%02d_%02d%02d%02d"), tmLocal.wMonth, tmLocal.wDay, tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
		}

		CString strPath;
		//-바코드
		if (FALSE == m_stInspInfo.CamInfo[0].szBarcode.IsEmpty())
		{
			strPath.Format(_T("%s%s"), m_stInspInfo.Path.szImage, m_stInspInfo.CamInfo[0].szBarcode);
		}
		else
		{
			strPath.Format(_T("%sNo Barcode"), m_stInspInfo.Path.szImage);
		}

		MakeDirectory(strPath);

		if (1 < g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt)
		{
			szPath.Format(_T("%s\\Capatue_%s_%s"), strPath, g_szParaName[nParaIdx], szImgFileName);
		}
		else
		{
			szPath.Format(_T("%s\\Capatue_%s"), strPath, szImgFileName);
		}

		m_stInspInfo.CamInfo[nParaIdx].stImageQ.szTestFileName  = szPath;

		szPath += _T(".png");
		if (m_stOption.Inspector.bSaveImage_Gray12)
		{
			OnSaveImage_Gray16(szPath.GetBuffer(0), lpw16bit_Std, nImgW, nImgH);
		}
		
		lReturn = m_tm_Test.SFRFunc(lpby8bit_Std, lpw16bit_Std, nImgW, nImgH, m_stInspInfo.RecipeInfo.stImageQ.stSFROpt[nTypeCnt], m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt]);
	
		delete[]lpw16bit_Sum;
		delete[]lpw8bit_Sum;
		delete[]lpby8bit_Std;
		delete[]lpw16bit_Std;
	}
	else
	{
		if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szTestFileName, TRUE, _T("SFR")))
		{
			m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
			m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].FailData();
			lReturn = RC_NoImage;
			return lReturn;
		}

		lReturn = m_tm_Test.SFRFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stImageBuf[nParaIdx].lpwImage_16bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stImageQ.stSFROpt[nTypeCnt], m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt]);
	}

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_SFR + nTypeCnt].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과 
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_ImgT_Pro_Distortion
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:39
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_Pro_Distortion(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//*** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;

	UINT nImgW = 0, nImgH = 0;

	// 이미지 버퍼에 복사
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szTestFileName, TRUE, _T("Distortion")))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDistortionData.FailData();
		lReturn = RC_NoImage;
		return lReturn;
	}

	lReturn = m_tm_Test.DistortionFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stImageQ.stDistortionOpt, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDistortionData);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_Distortion].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_ImgT_Pro_Rotation
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:39
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_Pro_Rotation(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;
	
	//*** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;
	UINT nImgW = 0, nImgH = 0;

	// 이미지 버퍼에 복사
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szTestFileName, TRUE, _T("Rotation"))){
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stRotateData.FailData();
		lReturn = RC_NoImage;
		return lReturn;
	}

	lReturn = m_tm_Test.RotateFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stImageQ.stRotateOpt, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stRotateData);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_Rotation].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_ImgT_Pro_Particle
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:39
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_Pro_Particle(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//*** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;
	UINT nImgW = 0, nImgH = 0;

	if (RC_OK != OnLightBrd_PowerOn(m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_Particle].fVolt, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_Particle].wStep)){
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stParticleData.FailData();
		lReturn = RC_Light_Brd_Err_Ack;
		return lReturn;
	}
	Sleep(m_stInspInfo.RecipeInfo.nLightDelay);

	// 이미지 버퍼에 복사
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szTestFileName, TRUE, _T("Stain"))){
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stParticleData.FailData();
		lReturn = RC_NoImage;
		return lReturn;
	}

	lReturn = m_tm_Test.ParticleFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stImageBuf[nParaIdx].lpwImage_16bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stImageQ.stParticleOpt, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stParticleData);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_Stain].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_ImgT_Pro_Particle_SNR
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:39
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_Pro_Particle_SNR(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//*** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;
	
	CString strCaptureCnt;
	//for (int icnt = 0; icnt < ROI_Par_SNR_Max; icnt++)
	//{
	//	if (m_stInspInfo.RecipeInfo.stImageQ.stDynamicOpt.stRegion[icnt].bEnable == FALSE)
	//	{
	//		m_stInspInfo.RecipeInfo.stImageQ.stDynamicOpt.stSpec_Min[icnt].bEnable = 0;
	//		m_stInspInfo.RecipeInfo.stImageQ.stDynamicOpt.stSpec_Max[icnt].dbValue = 0;
	//	}
	//}
	

	//for (UINT nMode = 0; nMode < Light_Par_SNR_MAX; nMode++)
	//{
	//	UINT nImgW = 0, nImgH = 0;

	//	if (RC_OK != OnLightBrd_PowerOn(m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[nMode].fVolt, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[nMode].wStep)){
	//		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	//		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData.FailData();
	//		lReturn = RC_Light_Brd_Err_Ack;
	//		return lReturn;
	//	}
	//
	//	Sleep(m_stInspInfo.RecipeInfo.nLightDelay);

	//	strCaptureCnt.Format(_T("DynamicRange_%d"), nMode+1);
	//	// 이미지 버퍼에 복사
	//	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szTestFileName, TRUE, strCaptureCnt))
	//	{
	//		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	//		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData.FailData();
	//		lReturn = RC_NoImage;
	//		return lReturn;
	//	}

	//	lReturn = m_tm_Test.Partiecl_SNRFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stImageBuf[nParaIdx].lpwImage_16bit, nImgW, nImgH, nMode, m_stInspInfo.RecipeInfo.stImageQ.stDynamicOpt, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData);

	//	Sleep(50);
	//	if (nMode < (Light_Par_SNR_MAX -1))
	//	{
	//		if (m_stOption.Inspector.bSaveImage_RGB)
	//		{
	//			m_bPicCaptureMode = TRUE;
	//			for (int t = 0; t < 100; t++)
	//			{
	//				Sleep(50);
	//				if (m_bPicCaptureMode == FALSE)
	//				{
	//					break;
	//				}
	//			}
	//		}
	//	}

	//	if (RC_OK != lReturn)
	//		return lReturn;
	//}

#if 1
	UINT nImgW = 0, nImgH = 0;

	strCaptureCnt.Format(_T("DynamicRange_%d"), 0);
	// 이미지 버퍼에 복사
	m_stImageMode.szImagePath = _T("C:\\Users\\Seongho\\Desktop\\Dynamic Image\\Capatue_DynamicRange_White.png");

	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szTestFileName, TRUE, strCaptureCnt))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData.FailData();
		lReturn = RC_NoImage;
		return lReturn;
	}

	lReturn = m_tm_Test.Partiecl_SNRFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stImageBuf[nParaIdx].lpwImage_16bit, nImgW, nImgH, 0, m_stInspInfo.RecipeInfo.stImageQ.stDynamicOpt, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData);

	Sleep(50);
	if (0 < (Light_Par_SNR_MAX - 1))
	{
		if (m_stOption.Inspector.bSaveImage_RGB)
		{
			m_bPicCaptureMode = TRUE;
			for (int t = 0; t < 100; t++)
			{
				Sleep(50);
				if (m_bPicCaptureMode == FALSE)
				{
					break;
				}
			}
		}
	}

	strCaptureCnt.Format(_T("DynamicRange_%d"), 1);
	// 이미지 버퍼에 복사
	m_stImageMode.szImagePath = _T("C:\\Users\\Seongho\\Desktop\\Dynamic Image\\Capatue_DynamicRange_Gray.png");
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szTestFileName, TRUE, strCaptureCnt))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData.FailData();
		lReturn = RC_NoImage;
		return lReturn;
	}

	lReturn = m_tm_Test.Partiecl_SNRFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stImageBuf[nParaIdx].lpwImage_16bit, nImgW, nImgH, 1, m_stInspInfo.RecipeInfo.stImageQ.stDynamicOpt, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData);

	Sleep(50);
	if (1 < (Light_Par_SNR_MAX - 1))
	{
		if (m_stOption.Inspector.bSaveImage_RGB)
		{
			m_bPicCaptureMode = TRUE;
			for (int t = 0; t < 100; t++)
			{
				Sleep(50);
				if (m_bPicCaptureMode == FALSE)
				{
					break;
				}
			}
		}
	}

	strCaptureCnt.Format(_T("DynamicRange_%d"), 2);
	// 이미지 버퍼에 복사
	m_stImageMode.szImagePath = _T("C:\\Users\\Seongho\\Desktop\\Dynamic Image\\Capatue_DynamicRange_Black.png");
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szTestFileName, TRUE, strCaptureCnt))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData.FailData();
		lReturn = RC_NoImage;
		return lReturn;
	}

	lReturn = m_tm_Test.Partiecl_SNRFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stImageBuf[nParaIdx].lpwImage_16bit, nImgW, nImgH, 2, m_stInspInfo.RecipeInfo.stImageQ.stDynamicOpt, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData);

	Sleep(50);
	if (2 < (Light_Par_SNR_MAX - 1))
	{
		if (m_stOption.Inspector.bSaveImage_RGB)
		{
			m_bPicCaptureMode = TRUE;
			for (int t = 0; t < 100; t++)
			{
				Sleep(50);
				if (m_bPicCaptureMode == FALSE)
				{
					break;
				}
			}
		}
	}

	if (RC_OK != lReturn)
		return lReturn;
#endif
//  [12/17/2018 Seongho.Lee] TEST
	for (UINT nIdx = 0; nIdx < ROI_Par_SNR_Max; nIdx++)
	{
		if (TRUE == m_stInspInfo.RecipeInfo.stImageQ.stDynamicOpt.stRegion[nIdx].bEnable)
		{
			m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData.dbValueDR[nIdx] = 20.0 * log10l((double)m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData.sSignal[Light_Par_SNR_White][nIdx] / (double)m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData.sSignal[Light_Par_SNR_Black][nIdx]);
			m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData.dbValueBW[nIdx] = 20.0 * log10f((float)(m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData.sSignal[Light_Par_SNR_White][nIdx] - m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData.sSignal[Light_Par_SNR_Black][nIdx]) / m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData.fNoise[Light_Par_SNR_Gray][nIdx]);
	
			m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData.dbValueDR[nIdx] *= m_stInspInfo.RecipeInfo.stImageQ.stDynamicOpt.stRegion[nIdx].dbOffset;
			m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData.dbValueBW[nIdx] *= m_stInspInfo.RecipeInfo.stImageQ.stDynamicOpt.stRegion[nIdx].dbOffsetSub;
		}
	}

	BOOL	bMinUse = m_stInspInfo.RecipeInfo.stImageQ.stDynamicOpt.stSpec_Min[Spec_Par_SNR_Dynamic].bEnable;
	BOOL	bMaxUse = m_stInspInfo.RecipeInfo.stImageQ.stDynamicOpt.stSpec_Max[Spec_Par_SNR_Dynamic].bEnable;

	double dbMinDev = m_stInspInfo.RecipeInfo.stImageQ.stDynamicOpt.stSpec_Min[Spec_Par_SNR_Dynamic].dbValue;
	double dbMaxDev = m_stInspInfo.RecipeInfo.stImageQ.stDynamicOpt.stSpec_Max[Spec_Par_SNR_Dynamic].dbValue;

	// 판정
	for (UINT nIdx = 0; nIdx < ROI_Par_SNR_Max; nIdx++)
	{
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData.nEtcResultDR[nIdx] = m_tm_Test.Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData.dbValueDR[nIdx]);
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData.nResultDR &= m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData.nEtcResultDR[nIdx];
	}

	bMinUse = m_stInspInfo.RecipeInfo.stImageQ.stDynamicOpt.stSpec_Min[Spec_Par_SNR_SNRBW].bEnable;
	bMaxUse = m_stInspInfo.RecipeInfo.stImageQ.stDynamicOpt.stSpec_Max[Spec_Par_SNR_SNRBW].bEnable;

	dbMinDev = m_stInspInfo.RecipeInfo.stImageQ.stDynamicOpt.stSpec_Min[Spec_Par_SNR_SNRBW].dbValue;
	dbMaxDev = m_stInspInfo.RecipeInfo.stImageQ.stDynamicOpt.stSpec_Max[Spec_Par_SNR_SNRBW].dbValue;

	// 판정
	for (UINT nIdx = 0; nIdx < ROI_Par_SNR_Max; nIdx++)
	{
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData.nEtcResultBW[nIdx] = m_tm_Test.Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData.dbValueBW[nIdx]);
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData.nResultBW &= m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData.nEtcResultBW[nIdx];
	}

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_Particle_SNR].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_ImgT_Pro_DefectPixel
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:39
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_Pro_DefectPixel(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//*** 추가 재시도 횟수
 	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;
 
 	stDPList stDefectList;
	CString strCaptureCnt;

 	for (UINT nMode = 0; nMode < Light_Defect_MAX; nMode++)
 	{
 		UINT nImgW = 0, nImgH = 0;
 
		if (RC_OK != OnLightBrd_PowerOn(m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[nMode + Light_I_Defect_B].fVolt, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[nMode + Light_I_Defect_B].wStep)){
				m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
				m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDefectPixelData.FailData();
				lReturn = RC_Light_Brd_Err_Ack;
				return lReturn;
		}
 
 		Sleep(m_stInspInfo.RecipeInfo.nLightDelay);
		strCaptureCnt.Format(_T("DefectPixel_%d"), nMode + 1);
 		// 이미지 버퍼에 복사
		if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szTestFileName, TRUE, strCaptureCnt)){
 			m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
			m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDefectPixelData.FailData();
			lReturn = RC_NoImage;
 			return lReturn;
 		}
 
 		lReturn = m_tm_Test.DefectPixelFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stImageBuf[nParaIdx].lpwImage_16bit, nImgW, nImgH, nMode, stDefectList, m_stInspInfo.RecipeInfo.stImageQ.stDefectPixelOpt, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDefectPixelData);
		Sleep(50);

		if (nMode < (Light_Defect_MAX - 1))
		{
			if (m_stOption.Inspector.bSaveImage_RGB)
			{
				m_bPicCaptureMode = TRUE;
				for (int t = 0; t < 100; t++)
				{
					Sleep(50);
					if (m_bPicCaptureMode == FALSE)
					{
						break;
					}
				}
			}
	
		}
 		if (RC_OK != lReturn)
 			return lReturn;
 	}
 
 	for (UINT nIdx = 0; nIdx < Spec_Defect_Max; nIdx++)
 	{
 		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDefectPixelData.nFailCount[nIdx] = 0;
 	}
 
 	UINT nCnt[Spec_Defect_Max] = { 0, };
 
 	for (int nIdx = 0; nIdx < stDefectList.DefectPixe_Cnt; nIdx++)
 	{
 		// 유형별 카운팅
 
 		if (nIdx > ROI_F_DefectPixel_Max)
 		{
 			break;
 		}
 		UINT nItem = stDefectList.DP_List[nIdx].nType;
 
 		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDefectPixelData.nFailType[nItem][nCnt[nItem]]			= stDefectList.DP_List[nIdx].nType;
 		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDefectPixelData.nFailCount[nItem]++;
 		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDefectPixelData.rtFailROI[nItem][nCnt[nItem]].left	= stDefectList.DP_List[nIdx].x;
 		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDefectPixelData.rtFailROI[nItem][nCnt[nItem]].top		= stDefectList.DP_List[nIdx].y;
 		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDefectPixelData.rtFailROI[nItem][nCnt[nItem]].right	= stDefectList.DP_List[nIdx].x + stDefectList.DP_List[nIdx].W;
 		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDefectPixelData.rtFailROI[nItem][nCnt[nItem]].bottom	= stDefectList.DP_List[nIdx].y + stDefectList.DP_List[nIdx].H;
 	}
 

	for (UINT nIdx = 0; nIdx < Spec_Defect_Max; nIdx++)
	{
		BOOL	bMinUse = m_stInspInfo.RecipeInfo.stImageQ.stDefectPixelOpt.stSpec_Min[nIdx].bEnable;
		BOOL	bMaxUse = m_stInspInfo.RecipeInfo.stImageQ.stDefectPixelOpt.stSpec_Max[nIdx].bEnable;

		double dbMinDev = m_stInspInfo.RecipeInfo.stImageQ.stDefectPixelOpt.stSpec_Min[nIdx].dbValue;
		double dbMaxDev = m_stInspInfo.RecipeInfo.stImageQ.stDefectPixelOpt.stSpec_Max[nIdx].dbValue;

		// 판정
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDefectPixelData.nEachResult[nIdx] = m_tm_Test.Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDefectPixelData.nFailCount[nIdx]);

		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDefectPixelData.nResult &= m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDefectPixelData.nEachResult[nIdx];
	}

	if (m_stOption.Inspector.bSaveImage_RGB)
	{
		m_bPicCaptureMode = TRUE;
		for (int t = 0; t < 100; t++)
		{
			Sleep(50);
			if (m_bPicCaptureMode == FALSE)
			{
				break;
			}
		}
	}
	
	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_DefectPixel].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);
	
	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_ImgT_Pro_Intencity
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:38
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_Pro_Intencity(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//*** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;
	UINT nImgW = 0, nImgH = 0;

	if (RC_OK != OnLightBrd_PowerOn(m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_Particle].fVolt, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_Particle].wStep)){
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stIntensityData.FailData();
		lReturn = RC_Light_Brd_Err_Ack;
		return lReturn;
	}
	Sleep(m_stInspInfo.RecipeInfo.nLightDelay);
	// 이미지 버퍼에 복사
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szTestFileName, TRUE, _T("Shading_RI"))){
	
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stIntensityData.FailData();
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_NoImage;
		return lReturn;
	}

	lReturn = m_tm_Test.IntensityFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stImageBuf[nParaIdx].lpwImage_16bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stImageQ.stIntensityOpt, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stIntensityData);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_Intensity].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);
	
	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_ImgT_Pro_Shading
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:38
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_Pro_Shading(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//*** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;
	UINT nImgW = 0, nImgH = 0;

	if (RC_OK != OnLightBrd_PowerOn(m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_Particle].fVolt, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_Particle].wStep)){
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stShadingData.FailData();
		lReturn = RC_Light_Brd_Err_Ack;
		return lReturn;
	}
	// 이미지 버퍼에 복사
	Sleep(m_stInspInfo.RecipeInfo.nLightDelay);
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szTestFileName, TRUE, _T("Shading_SNR"))){
	
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stShadingData.FailData();
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_NoImage;
		return lReturn;
	}

	lReturn = m_tm_Test.ShadingFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stImageBuf[nParaIdx].lpwImage_16bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stImageQ.stShadingOpt, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stShadingData);
	
	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_Shading].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_ImgT_Pro_SNR_Light
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:38
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_Pro_SNR_Light(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//*** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;
	UINT nImgW = 0, nImgH = 0;
	if (RC_OK != OnLightBrd_PowerOn(m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_Particle].fVolt, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_Particle].wStep)){
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSNR_LightData.FailData();
		lReturn = RC_Light_Brd_Err_Ack;
		return lReturn;
	}

	// 이미지 버퍼에 복사
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szTestFileName, TRUE, _T("SNR_Light"))){
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSNR_LightData.FailData();
		lReturn = RC_NoImage;
		return lReturn;
	}

	lReturn = m_tm_Test.SNR_LightFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stImageBuf[nParaIdx].lpwImage_16bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stImageQ.stSNR_LightOpt, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSNR_LightData);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_SNR_Light].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_ImgT_JudgeResult
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nTestItem
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_JudgeResult(__in UINT nStepIdx, __in UINT nTestItem, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;
	CArray <COleVariant, COleVariant&> VarArray;
	COleVariant varResult;
	int nTypeCnt = 0;
	for (UINT nIdx = 0; nIdx < m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList.GetAt(nTestItem).nResultCount; nIdx++)
	{
		switch (nTestItem)
		{
		case TI_ImgT_Re_ECurrent:
			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stECurrentData.dbValue[nIdx];
			VarArray.Add(varResult);
			break;
		case TI_ImgT_Re_OpticalCenterX:
			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stOpticalCenterData.iStandPosDevX;
			VarArray.Add(varResult);
			break;
		case TI_ImgT_Re_OpticalCenterY:
			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stOpticalCenterData.iStandPosDevY;
			VarArray.Add(varResult);
			break;
		case TI_ImgT_Re_FOV_Hor:
			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stFovData.dbValue[Spec_Fov_Hor];
			VarArray.Add(varResult);
			break;
		case TI_ImgT_Re_FOV_Ver:
			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stFovData.dbValue[Spec_Fov_Ver];
			VarArray.Add(varResult);
			break;

		case TI_ImgT_Re_SFR:
			nTypeCnt = 0;
			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbValue[nIdx];
			VarArray.Add(varResult);
			break;
		case TI_ImgT_Re_Group_SFR:
			nTypeCnt = 0;
			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbEachResultGroup[nIdx];
			VarArray.Add(varResult);
			break;
		case TI_ImgT_Re_Tilt_SFR:
			nTypeCnt = 0;
			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbTiltdata[nIdx];
			VarArray.Add(varResult);
			break;
		//case TI_ImgT_Re_G0_SFR:
		//	nTypeCnt = 0;
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbMinValue[ITM_SFR_G0];
		//	VarArray.Add(varResult);
		//	break;
		//case TI_ImgT_Re_G1_SFR:
		//	nTypeCnt = 0;
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbMinValue[ITM_SFR_G1];
		//	VarArray.Add(varResult);
		//	break;
		//case TI_ImgT_Re_G2_SFR:
		//	nTypeCnt = 0;
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbMinValue[ITM_SFR_G2];
		//	VarArray.Add(varResult);
		//	break;
		//case TI_ImgT_Re_G3_SFR:
		//	nTypeCnt = 0;
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbMinValue[ITM_SFR_G3];
		//	VarArray.Add(varResult);
		//	break;
		//case TI_ImgT_Re_G4_SFR:
		//	nTypeCnt = 0;
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbMinValue[ITM_SFR_G4];
		//	VarArray.Add(varResult);
		//	break;
		//case TI_ImgT_Re_X1Tilt_SFR:
		//	nTypeCnt = 0;
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbTiltdata[ITM_SFR_X1_Tilt_UpperLimit];
		//	VarArray.Add(varResult);
		//	break;
		//case TI_ImgT_Re_X2Tilt_SFR:
		//	nTypeCnt = 0;
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbTiltdata[ITM_SFR_X2_Tilt_UpperLimit];
		//	VarArray.Add(varResult);
		//	break;
		//case TI_ImgT_Re_Y1Tilt_SFR:
		//	nTypeCnt = 0;
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbTiltdata[ITM_SFR_Y1_Tilt_UpperLimit];
		//	VarArray.Add(varResult);
		//	break;
		//case TI_ImgT_Re_Y2Tilt_SFR:
		//	nTypeCnt = 0;
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbTiltdata[ITM_SFR_Y2_Tilt_UpperLimit];
		//	VarArray.Add(varResult);
		//	break;

		case TI_ImgT_Re_SFR_2:
			nTypeCnt = 1;
			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbValue[nIdx];
			VarArray.Add(varResult);
			break;
		case TI_ImgT_Re_Group_SFR_2:
			nTypeCnt = 1;
			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbEachResultGroup[nIdx];
			VarArray.Add(varResult);
			break;
		case TI_ImgT_Re_Tilt_SFR_2:
			nTypeCnt = 1;
			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbTiltdata[nIdx];
			VarArray.Add(varResult);
			break;
		//case TI_ImgT_Re_G0_SFR_2:
		//	nTypeCnt = 1;
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbMinValue[ITM_SFR_G0];
		//	VarArray.Add(varResult);
		//	break;
		//case TI_ImgT_Re_G1_SFR_2:
		//	nTypeCnt = 1;
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbMinValue[ITM_SFR_G1];
		//	VarArray.Add(varResult);
		//	break;
		//case TI_ImgT_Re_G2_SFR_2:
		//	nTypeCnt = 1;
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbMinValue[ITM_SFR_G2];
		//	VarArray.Add(varResult);
		//	break;
		//case TI_ImgT_Re_G3_SFR_2:
		//	nTypeCnt = 1;
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbMinValue[ITM_SFR_G3];
		//	VarArray.Add(varResult);
		//	break;
		//case TI_ImgT_Re_G4_SFR_2:
		//	nTypeCnt = 1;
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbMinValue[ITM_SFR_G4];
		//	VarArray.Add(varResult);
		//	break;
		//case TI_ImgT_Re_X1Tilt_SFR_2:
		//	nTypeCnt = 1;
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbTiltdata[ITM_SFR_X1_Tilt_UpperLimit];
		//	VarArray.Add(varResult);
		//	break;
		//case TI_ImgT_Re_X2Tilt_SFR_2:
		//	nTypeCnt = 1;
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbTiltdata[ITM_SFR_X2_Tilt_UpperLimit];
		//	VarArray.Add(varResult);
		//	break;
		//case TI_ImgT_Re_Y1Tilt_SFR_2:
		//	nTypeCnt = 1;
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbTiltdata[ITM_SFR_Y1_Tilt_UpperLimit];
		//	VarArray.Add(varResult);
		//	break;
		//case TI_ImgT_Re_Y2Tilt_SFR_2:
		//	nTypeCnt = 1;
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbTiltdata[ITM_SFR_Y2_Tilt_UpperLimit];
		//	VarArray.Add(varResult);
		//	break;
		case TI_ImgT_Re_SFR_3:
			nTypeCnt = 2;
			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbValue[nIdx];
			VarArray.Add(varResult);
			break;
		case TI_ImgT_Re_Group_SFR_3:
			nTypeCnt = 2;
			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbEachResultGroup[nIdx];
			VarArray.Add(varResult);
			break;
		case TI_ImgT_Re_Tilt_SFR_3:
			nTypeCnt = 2;
			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbTiltdata[nIdx];
			VarArray.Add(varResult);
			break;
		//case TI_ImgT_Re_G0_SFR_3:
		//	nTypeCnt = 2;
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbMinValue[ITM_SFR_G0];
		//	VarArray.Add(varResult);
		//	break;
		//case TI_ImgT_Re_G1_SFR_3:
		//	nTypeCnt = 2;
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbMinValue[ITM_SFR_G1];
		//	VarArray.Add(varResult);
		//	break;
		//case TI_ImgT_Re_G2_SFR_3:
		//	nTypeCnt = 2;
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbMinValue[ITM_SFR_G2];
		//	VarArray.Add(varResult);
		//	break;
		//case TI_ImgT_Re_G3_SFR_3:
		//	nTypeCnt = 2;
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbMinValue[ITM_SFR_G3];
		//	VarArray.Add(varResult);
		//	break;
		//case TI_ImgT_Re_G4_SFR_3:
		//	nTypeCnt = 2;
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbMinValue[ITM_SFR_G4];
		//	VarArray.Add(varResult);
		//	break;
		//case TI_ImgT_Re_X1Tilt_SFR_3:
		//	nTypeCnt = 2;
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbTiltdata[ITM_SFR_X1_Tilt_UpperLimit];
		//	VarArray.Add(varResult);
		//	break;
		//case TI_ImgT_Re_X2Tilt_SFR_3:
		//	nTypeCnt = 2;
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbTiltdata[ITM_SFR_X2_Tilt_UpperLimit];
		//	VarArray.Add(varResult);
		//	break;
		//case TI_ImgT_Re_Y1Tilt_SFR_3:
		//	nTypeCnt = 2;
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbTiltdata[ITM_SFR_Y1_Tilt_UpperLimit];
		//	VarArray.Add(varResult);
		//	break;
		//case TI_ImgT_Re_Y2Tilt_SFR_3:
		//	nTypeCnt = 2;
		//	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSFRData[nTypeCnt].dbTiltdata[ITM_SFR_Y2_Tilt_UpperLimit];
		//	VarArray.Add(varResult);
		//	break;

		case TI_ImgT_Re_Rotation:
			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stRotateData.dbValue;
			VarArray.Add(varResult);
			break;
		case TI_ImgT_Re_Distortion:
			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDistortionData.dbValue;
			VarArray.Add(varResult);
			break;
		case TI_ImgT_Re_Stain:
			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stParticleData.nFailCount;
			VarArray.Add(varResult);
			break;
		case TI_ImgT_Re_DefectPixel:
			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDefectPixelData.nFailCount[nIdx];
			VarArray.Add(varResult);
			break;
		case TI_ImgT_Re_Intencity:
			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stIntensityData.fPercent[nIdx];
			VarArray.Add(varResult);
			break;
		case TI_ImgT_Re_Shading:
			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stShadingData.fPercent[nIdx];
			VarArray.Add(varResult);
			break;
		case TI_ImgT_Re_SNR_Light:
			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stSNR_LightData.dbFinalValue;
			VarArray.Add(varResult);
			break;
		case TI_ImgT_Re_DynamicRange:
			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData.dbValueDR[nIdx];
			VarArray.Add(varResult);
			break;
		case TI_ImgT_Re_SNR_BW:
			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stDynamicData.dbValueBW[nIdx];
			VarArray.Add(varResult);
			break;

//#ifdef SET_GWANGJU
		case TI_ImgT_Re_Hot_Pixel:
			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stHotPixelData.iFailCount;
			VarArray.Add(varResult);
			break;
		case TI_ImgT_Re_Fixed_Pattern:
			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stFPNData.iFailCount[nIdx];
			VarArray.Add(varResult);
			break;
		case TI_ImgT_Re_3D_Depth:
			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.st3D_DepthData.dbValue[nIdx];
			VarArray.Add(varResult);
			break;

		case TI_ImgT_Re_EEPROM_Verify:
			varResult.intVal = (int)m_stInspInfo.CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.fEachData[nIdx];
			VarArray.Add(varResult);

			break;
//#endif
		case TI_ImgT_Re_TemperatureSensor:
			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stTemperatureSensorData.dbTemperature;
			VarArray.Add(varResult);

			break;

		case TI_ImgT_Re_Particle_Entry:
			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stParticle_EntryData.nFailCount;
			VarArray.Add(varResult);
			break;
		default:
			break;
		}
	}

	// 측정값 입력 및 판정
	m_stInspInfo.Set_Measurement(nParaIdx, nStepIdx, (UINT)VarArray.GetCount(), VarArray.GetData());

	return lReturn;
}

//=============================================================================
// Method		: Judgment_Inspection_All
// Access		: protected  
// Returns		: enTestResult
// Qualifier	:
// Last Update	: 2018/4/24 - 10:00
// Desc.		:
//=============================================================================
enTestResult CTestManager_EQP_ImgT::Judgment_Inspection_All()
{
	enTestResult lReturn = TR_Pass;
	UINT nTestCount = 1;

	if (1 < g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt)
	{
		if ((Sys_Focusing == m_InspectionType) && (Model_OMS_Front == m_stInspInfo.RecipeInfo.ModelType))
		{
			nTestCount = 1;
		}
		else
		{
			nTestCount = g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt; // 2
		}
	}

	for (UINT nIdx = 0; nIdx < nTestCount; nIdx++)
	{
		if (TR_Pass != m_stInspInfo.CamInfo[nIdx].nJudgment)
		{
			lReturn = m_stInspInfo.CamInfo[nIdx].nJudgment;
			break;
		}
	}

	// 검사 최종 판정
	OnSet_TestResult(lReturn);
	OnSet_TestResult_Total(lReturn);//이 결과가 최종으로 UI에 Display 됨.
	return lReturn;
}

//=============================================================================
// Method		: MES_CheckBarcodeValidation
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/4 - 16:25
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::MES_CheckBarcodeValidation(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::MES_CheckBarcodeValidation(nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: MES_SendInspectionData
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/4 - 16:39
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::MES_SendInspectionData(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	if (enOperateMode::OpMode_Production == m_stInspInfo.OperateMode)
	{
		lReturn = MES_MakeInspecData_ImgT(nParaIdx);


		// MES 로그 남기기
		BOOL bJudge = (TR_Pass == m_stInspInfo.Judgment_All) ? TRUE : FALSE;

		SYSTEMTIME tmLocal;
		GetLocalTime(&tmLocal);
		// MES 로그 남기기

		//m_FileMES.SaveMESFile(m_stInspInfo.CamInfo[0].szBarcode, m_stInspInfo.CamInfo[0].nMES_TryCnt, bJudge, &m_stInspInfo.CamInfo[0].TestTime.tmStart_Loading);

		// MES
		m_FileMES.SaveMESFile(m_stInspInfo.CamInfo[0].szBarcode, m_stInspInfo.CamInfo[0].nMES_TryCnt, bJudge, &tmLocal);

		// 로그
		m_FileTestLog.SaveLOGFile(m_stInspInfo.Path.szReport, m_stInspInfo.CamInfo[0].szBarcode, m_stInspInfo.CamInfo[0].nMES_TryCnt, bJudge, &tmLocal);
	}

	return lReturn;
}

//=============================================================================
// Method		: MES_MakeInspecData_ImgT
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/9 - 19:16
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::MES_MakeInspecData_ImgT(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

// 	m_stInspInfo.CamInfo[0].stImageQ.stECurrentData.dbValue[0] = 3;
// 	m_stInspInfo.CamInfo[0].stImageQ.stECurrentData.dbValue[1] = 4;
// 	m_stInspInfo.CamInfo[1].stImageQ = m_stInspInfo.CamInfo[0].stImageQ;


	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;
	INT_PTR iStepCnt = m_stInspInfo.RecipeInfo.StepInfo.GetCount();
	DWORD dwElapTime = 0;
	int iCamCount = g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt;
	
	// * 설정된 스텝 진행
	if (m_stInspInfo.RecipeInfo.ModelType == Model_OMS_Entry) //Entry 일때
	{
		for (int camCnt = 0; camCnt < iCamCount; camCnt++)
		{
			m_stMesEntryData.Reset(camCnt);
			for (INT nStepIdx = 0; nStepIdx < iStepCnt; nStepIdx++)
			{
				switch (pStepInfo->StepList[nStepIdx].nTestItem)
				{

				case TI_ImgT_Fn_ECurrent:
					m_TestMgr_TestMes.Current_Entry_DataSave(camCnt, &m_stMesEntryData);
					break;
				case TI_ImgT_Fn_OpticalCenter:
					m_TestMgr_TestMes.OpticalCenter_Entry_DataSave(camCnt, &m_stMesEntryData);
					break;
				case TI_ImgT_Fn_FOV:
					m_TestMgr_TestMes.Fov_Entry_DataSave(camCnt, &m_stMesEntryData);
					break;
				case TI_ImgT_Fn_SFR:
					m_TestMgr_TestMes.SFR_Entry_DataSave(camCnt, &m_stMesEntryData);
					break;
				case TI_ImgT_Fn_Distortion:
					m_TestMgr_TestMes.Distortion_Entry_DataSave(camCnt, &m_stMesEntryData);
					break;
				case TI_ImgT_Fn_Rotation:
					m_TestMgr_TestMes.Rotate_Entry_DataSave(camCnt, &m_stMesEntryData);
					break;
				case TI_ImgT_Fn_Particle_SNR:
					m_TestMgr_TestMes.DynamicRange_Entry_DataSave(camCnt, &m_stMesEntryData);
					break;
				case TI_ImgT_Fn_Shading:
					m_TestMgr_TestMes.Shading_Entry_DataSave(camCnt, &m_stMesEntryData);
					break;
				case TI_ImgT_Fn_SNR_Light:
					m_TestMgr_TestMes.SNR_Light_Entry_DataSave(camCnt, &m_stMesEntryData);
					break;

					//#ifdef SET_GWANGJU
				case TI_ImgT_Fn_HotPixel:
					m_TestMgr_TestMes.HotPixel_Entry_DataSave(camCnt, &m_stMesEntryData);
					break;
				case TI_ImgT_Fn_FPN:
					m_TestMgr_TestMes.FPN_Entry_DataSave(camCnt, &m_stMesEntryData);
					break;
				case TI_ImgT_Fn_3DDepth:
					m_TestMgr_TestMes.Depth_Entry_DataSave(camCnt, &m_stMesEntryData);
					break;
				case TI_ImgT_Fn_EEPROM_Verify:
					m_TestMgr_TestMes.EEPROM_Entry_DataSave(camCnt, &m_stMesEntryData);
					break;
					//#endif
				case TI_ImgT_Fn_TemperatureSensor:
					m_TestMgr_TestMes.TemperatureSensor_Entry_DataSave(camCnt, &m_stMesEntryData);
					break;
				case TI_ImgT_Fn_Particle_Entry:
					m_TestMgr_TestMes.Particle_Entry_DataSave(camCnt, &m_stMesEntryData);
					break;
				default:
					break;
				}
			}
		}
	}
	else
	{
		for (int camCnt = 0; camCnt < iCamCount; camCnt++)
		{
			m_stMesData.Reset(camCnt);
			for (INT nStepIdx = 0; nStepIdx < iStepCnt; nStepIdx++)
			{
				switch (pStepInfo->StepList[nStepIdx].nTestItem)
				{

				case TI_ImgT_Fn_ECurrent:
					m_TestMgr_TestMes.Current_DataSave(camCnt, &m_stMesData);
					break;
				case TI_ImgT_Fn_OpticalCenter:
					m_TestMgr_TestMes.OpticalCenter_DataSave(camCnt, &m_stMesData);
					break;
				case TI_ImgT_Fn_FOV:
					m_TestMgr_TestMes.Fov_DataSave(camCnt, &m_stMesData);
					break;
				case TI_ImgT_Fn_SFR:
					m_TestMgr_TestMes.SFR_DataSave(camCnt, &m_stMesData,0);
					break;
				case TI_ImgT_Fn_SFR_2:
					m_TestMgr_TestMes.SFR_DataSave(camCnt, &m_stMesData,1);
					break;
				case TI_ImgT_Fn_SFR_3:
					m_TestMgr_TestMes.SFR_DataSave(camCnt, &m_stMesData, 2);
					break;
				case TI_ImgT_Fn_Distortion:
					m_TestMgr_TestMes.Distortion_DataSave(camCnt, &m_stMesData);
					break;
				case TI_ImgT_Fn_Rotation:
					m_TestMgr_TestMes.Rotate_DataSave(camCnt, &m_stMesData);
					break;
				case TI_ImgT_Fn_Stain:
					m_TestMgr_TestMes.Stain_DataSave(camCnt, &m_stMesData);
					break;
				case TI_ImgT_Fn_Particle_SNR:
					m_TestMgr_TestMes.DynamicRange_DataSave(camCnt, &m_stMesData);
					break;
				case TI_ImgT_Fn_DefectPixel:
					m_TestMgr_TestMes.Defectpixel_DataSave(camCnt, &m_stMesData);
					break;
				case TI_ImgT_Fn_Intensity:
					m_TestMgr_TestMes.Intensity_DataSave(camCnt, &m_stMesData);
					break;
				case TI_ImgT_Fn_Shading:
					m_TestMgr_TestMes.Shading_DataSave(camCnt, &m_stMesData);
					break;
				case TI_ImgT_Fn_SNR_Light:
					m_TestMgr_TestMes.SNR_Light_DataSave(camCnt, &m_stMesData);
					break;

					//#ifdef SET_GWANGJU
				//case TI_ImgT_Fn_HotPixel:
				//	m_TestMgr_TestMes.HotPixel_DataSave(camCnt, &m_stMesData);
				//	break;
				//case TI_ImgT_Fn_FPN:
				//	m_TestMgr_TestMes.FPN_DataSave(camCnt, &m_stMesData);
				//	break;
				//case TI_ImgT_Fn_3DDepth:
				//	m_TestMgr_TestMes.Depth_DataSave(camCnt, &m_stMesData);
				//	break;
				//case TI_ImgT_Fn_EEPROM_Verify:
				//	m_TestMgr_TestMes.EEPROM_DataSave(camCnt, &m_stMesData);
				//	break;
				//	//#endif
				//case TI_ImgT_Fn_TemperatureSensor:
				//	m_TestMgr_TestMes.TemperatureSensor_DataSave(camCnt, &m_stMesData);
				//	break;

				//case TI_ImgT_Fn_Particle_Entry:
				//	m_TestMgr_TestMes.Particle_Entry_DataSave(camCnt, &m_stMesData);
				//	break;
				default:
					break;
				}
			}
		}
	}

	

	m_FileMES.Reset();
	m_FileTestLog.Reset();

	BOOL bJudge = (TR_Pass == m_stInspInfo.Judgment_All) ? TRUE : FALSE;

#ifdef USE_BARCODE_SCANNER
	m_FileMES.Set_EquipmnetID(m_stInspInfo.CamInfo[0].szBarcode);
#else
	m_FileMES.Set_EquipmnetID(m_stInspInfo.szEquipmentID);
#endif
	m_FileMES.Set_Path(m_stOption.MES.szPath_MESLog);
	m_FileMES.Set_Barcode(m_stInspInfo.CamInfo[0].szBarcode, m_stInspInfo.CamInfo[0].nMES_TryCnt, bJudge, &m_stInspInfo.CamInfo[0].TestTime.tmStart_Loading);

	m_FileTestLog.Set_EquipmnetID(m_stInspInfo.szEquipmentID);
	m_FileTestLog.Set_Path(m_stOption.MES.szPath_MESLog);
	m_FileTestLog.Set_Barcode(m_stInspInfo.CamInfo[0].szBarcode, m_stInspInfo.CamInfo[0].nMES_TryCnt, bJudge, &m_stInspInfo.CamInfo[0].TestTime.tmStart_Loading);

	CString		NAME;				// 항목 명칭	
	CString		VALUE;				// 데이터
	BOOL		JUDGE = TRUE;		// 판정

	int nHeaderCnt =0;

	if (m_stInspInfo.RecipeInfo.ModelType == Model_OMS_Entry) //Entry 모델일때
	{
		for (int camCnt = 0; camCnt < iCamCount; camCnt++)
		{
			nHeaderCnt = 0;
			for (int t = 0; t < MesDataIdx_MAX; t++)
			{
				if (!m_stMesEntryData.szMesTestData[camCnt][t].IsEmpty())
				{

					CString strData;
					CString szBlockData;
					CString szValueData;
					CString szResultData;

					strData = m_stMesEntryData.szMesTestData[camCnt][t];

					strData.Remove(_T(' '));
					strData.Remove(_T('\t'));
					strData.Remove(_T('\r'));
					strData.Remove(_T('\n'));
					//- ,를 찾고
					int nNum = 0;
					int nStartNum = 0;
					int nDataNum = 0;

					int nDataCnt = 0;
					while (true)
					{
						nNum = strData.Find(',', nStartNum);
						if (nNum != -1)
						{
							szBlockData = strData.Mid(nStartNum, nNum);

							nDataNum = szBlockData.Find(':', 0);
							szValueData = szBlockData.Mid(0, nDataNum);
							szResultData = szBlockData.Mid(nDataNum + 1, szBlockData.GetLength());

							m_FileMES.Add_ItemData(g_szMESEntryHeader[nHeaderCnt], szValueData, _ttoi(szResultData));
							m_FileTestLog.Add_ItemData(g_szMESEntryHeader[nHeaderCnt], szValueData, _ttoi(szResultData));




							nHeaderCnt++;
							nStartNum = nNum + 1;
							nDataCnt++;
						}
						else{
							break;
						}
					}

					szBlockData = strData.Mid(nStartNum, strData.GetLength());
					nDataNum = szBlockData.Find(':', 0);
					szValueData = szBlockData.Mid(0, nDataNum);
					szResultData = szBlockData.Mid(nDataNum + 1, szBlockData.GetLength());
					m_FileMES.Add_ItemData(g_szMESEntryHeader[nHeaderCnt], szValueData, _ttoi(szResultData));
					m_FileTestLog.Add_ItemData(g_szMESEntryHeader[nHeaderCnt], szValueData, _ttoi(szResultData));

					nHeaderCnt++;
					nDataCnt++;

					for (int k = nDataCnt; k < m_stMesEntryData.nMesDataNum[t]; k++)
					{
						m_FileMES.Add_ItemData(g_szMESEntryHeader[nHeaderCnt], _T(""), 1);
						m_FileTestLog.Add_ItemData(g_szMESEntryHeader[nHeaderCnt], _T(""), 1);
						nHeaderCnt++;
					}
				}
				else{
					CString strHeader;
					for (int k = 0; k < m_stMesEntryData.nMesDataNum[t]; k++)
					{

						m_FileMES.Add_ItemData(g_szMESEntryHeader[nHeaderCnt], _T(""), 1);

						strHeader.Format(_T("%s"), g_szMESEntryHeader[nHeaderCnt]);
						if (strHeader == _T("Reserved"))
						{
							m_FileTestLog.Add_ItemData(g_szMESEntryHeader[nHeaderCnt], _T(""), 1);
						}
						else{
							m_FileTestLog.Add_ItemData(g_szMESEntryHeader[nHeaderCnt], _T("X"), 1);
						}
						nHeaderCnt++;
					}
				}
			}
		}
	}
	else
	{
		for (int camCnt = 0; camCnt < iCamCount; camCnt++)
		{
			nHeaderCnt = 0;
			for (int t = 0; t < MesDataIdx_MAX; t++)
			{
				if (!m_stMesData.szMesTestData[camCnt][t].IsEmpty())
				{

					CString strData;
					CString szBlockData;
					CString szValueData;
					CString szResultData;

					strData = m_stMesData.szMesTestData[camCnt][t];

					strData.Remove(_T(' '));
					strData.Remove(_T('\t'));
					strData.Remove(_T('\r'));
					strData.Remove(_T('\n'));
					//- ,를 찾고
					int nNum = 0;
					int nStartNum = 0;
					int nDataNum = 0;

					int nDataCnt = 0;
					while (true)
					{
						nNum = strData.Find(',', nStartNum);
						if (nNum != -1)
						{
							szBlockData = strData.Mid(nStartNum, nNum);

							nDataNum = szBlockData.Find(':', 0);
							szValueData = szBlockData.Mid(0, nDataNum);
							szResultData = szBlockData.Mid(nDataNum + 1, szBlockData.GetLength());

							m_FileMES.Add_ItemData(g_szMESHeader[nHeaderCnt], szValueData, _ttoi(szResultData));
							m_FileTestLog.Add_ItemData(g_szMESHeader[nHeaderCnt], szValueData, _ttoi(szResultData));




							nHeaderCnt++;
							nStartNum = nNum + 1;
							nDataCnt++;
						}
						else{
							break;
						}
					}

					szBlockData = strData.Mid(nStartNum, strData.GetLength());
					nDataNum = szBlockData.Find(':', 0);
					szValueData = szBlockData.Mid(0, nDataNum);
					szResultData = szBlockData.Mid(nDataNum + 1, szBlockData.GetLength());
					m_FileMES.Add_ItemData(g_szMESHeader[nHeaderCnt], szValueData, _ttoi(szResultData));
					m_FileTestLog.Add_ItemData(g_szMESHeader[nHeaderCnt], szValueData, _ttoi(szResultData));

					nHeaderCnt++;
					nDataCnt++;

					for (int k = nDataCnt; k < m_stMesData.nMesDataNum[t]; k++)
					{
						m_FileMES.Add_ItemData(g_szMESHeader[nHeaderCnt], _T(""), 1);
						m_FileTestLog.Add_ItemData(g_szMESHeader[nHeaderCnt], _T(""), 1);
						nHeaderCnt++;
					}
				}
				else{
					CString strHeader;
					for (int k = 0; k < m_stMesData.nMesDataNum[t]; k++)
					{

						m_FileMES.Add_ItemData(g_szMESHeader[nHeaderCnt], _T(""), 1);

						strHeader.Format(_T("%s"), g_szMESHeader[nHeaderCnt]);
						if (strHeader == _T("Reserved"))
						{
							m_FileTestLog.Add_ItemData(g_szMESHeader[nHeaderCnt], _T(""), 1);
						}
						else{
							m_FileTestLog.Add_ItemData(g_szMESHeader[nHeaderCnt], _T("X"), 1);
						}
						nHeaderCnt++;
					}
				}
			}
		}
	}
	

	return lReturn;
}

//=============================================================================
// Method		: OnPopupMessageTest_All
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enResultCode nResultCode
// Qualifier	:
// Last Update	: 2016/7/21 - 18:59
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnPopupMessageTest_All(__in enResultCode nResultCode)
{
	CTestManager_EQP::OnPopupMessageTest_All(nResultCode);
}

//=============================================================================
// Method		: OnMotion_MoveToTestPos
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/10/19 - 20:42
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnMotion_MoveToTestPos(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

//	lReturn = CTestManager_EQP::OnMotion_MoveToTestPos(nParaIdx);
	lReturn = m_MotionSequence.OnActionStandby_ImgT(m_stOption.Inspector.bUseBlindShutter);


	return lReturn;
}

//=============================================================================
// Method		: OnMotion_MoveToStandbyPos
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/15 - 9:58
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnMotion_MoveToStandbyPos(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_MoveToStandbyPos(nParaIdx);



	return lReturn;
}

//=============================================================================
// Method		: OnMotion_MoveToUnloadPos
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:40
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnMotion_MoveToUnloadPos(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_MoveToUnloadPos(nParaIdx);



	return lReturn;
}

//=============================================================================
// Method		: OnMotion_Origin
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/11/15 - 3:27
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnMotion_Origin()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_Origin();



	return lReturn;
}

//=============================================================================
// Method		: OnMotion_LoadProduct
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/11/15 - 3:05
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnMotion_LoadProduct()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_LoadProduct();



	return lReturn;
}

//=============================================================================
// Method		: OnMotion_UnloadProduct
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/11/15 - 3:16
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnMotion_UnloadProduct()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_UnloadProduct();



	return lReturn;
}


//=============================================================================
// Method		: OnMotion_ChartTestProduct
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:41
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnMotion_ChartTestProduct(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

#ifndef MOTION_NOT_USE
	lReturn = m_MotionSequence.OnActionTesting_ImgT_Chart(nParaIdx);
#endif
	return lReturn;
}

//=============================================================================
// Method		: OnMotion_ParticleTestProduct
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:41
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnMotion_ParticleTestProduct(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

#ifndef MOTION_NOT_USE
	lReturn = m_MotionSequence.OnActionTesting_ImgT_Par(nParaIdx);
#endif
	return lReturn;
}

//=============================================================================
// Method		: OnDIn_DetectSignal
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BYTE byBitOffset
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2018/2/1 - 17:22
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnDIn_DetectSignal(__in BYTE byBitOffset, __in BOOL bOnOff)
{
	//CTestManager_EQP::OnDIn_DetectSignal(byBitOffset, bOnOff);
	OnDIn_DetectSignal_ImgT(byBitOffset, bOnOff);
}

//=============================================================================
// Method		: OnDIn_DetectSignal_SteCal
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BYTE byBitOffset
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2018/3/3 - 20:41
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnDIn_DetectSignal_ImgT(__in BYTE byBitOffset, __in BOOL bOnOff)
{
	enDI_ImageTest nDI_Signal = (enDI_ImageTest)byBitOffset;

	switch (nDI_Signal)
	{
	case DI_ImgT_00_MainPower:
		if (FALSE == bOnOff)
		{
			OnDIn_MainPower();
		}
		break;

	case DI_ImgT_01_EMO:
		if (FALSE == bOnOff)
		{
			OnDIn_EMO();
		}
		break;

	case DI_ImgT_02_CoverSensor:
		if (FALSE == bOnOff)
		{
			OnDIn_AreaSensor();
		}
		break;

	case DI_ImgT_03_AreaSensor:
		if (FALSE == bOnOff)
		{
			OnDIn_AreaSensor();
		}
		break;

	case DI_ImgT_04_DoorSensor:
		if (FALSE == bOnOff)
		{
			OnDIn_DoorSensor();
		}
		break;

	case DI_ImgT_05_Start:
		if (bOnOff)
		{
			StartOperation_LoadUnload(Product_Load);
		}
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: OnDIO_InitialSignal
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/8 - 20:30
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnDIO_InitialSignal()
{
	CTestManager_EQP::OnDIO_InitialSignal();


}

//=============================================================================
// Method		: OnDIn_MainPower
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 19:48
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnDIn_MainPower()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_MainPower();



	return lReturn;
}

//=============================================================================
// Method		: OnDIn_EMO
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 20:12
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnDIn_EMO()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_EMO();



	return lReturn;
}

//=============================================================================
// Method		: OnDIn_AreaSensor
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 20:12
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnDIn_AreaSensor()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_AreaSensor();



	return lReturn;
}

//=============================================================================
// Method		: OnDIn_DoorSensor
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 20:12
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnDIn_DoorSensor()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_DoorSensor();



	return lReturn;
}

//=============================================================================
// Method		: OnDIn_JIGCoverCheck
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 19:41
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnDIn_JIGCoverCheck()
{
	LRESULT lReturn = RC_OK;

	if (m_stOption.Inspector.bUseJIGCoverSensor)
	{
		//lReturn = CTestManager_EQP::OnDIn_JIGCoverCheck();

		switch (m_stInspInfo.RecipeInfo.ModelType)
		{
		case Model_OMS_Entry:
		case Model_OMS_Front:
		case Model_OMS_Front_Set:
		case Model_IKC:
			return RC_OK;
			break;

		case Model_MRA2:
			break;

		default:
			break;
		}

		// * JIG Cover 체크 (Clear : 커버 덮은 상태, Set : 커버 열린 상태)
		// * 검사 진행 중 커버 열리면 오류 처리
		if (IsTesting())
		{
			if (!m_stInspInfo.byDIO_DI[DI_2D_04_JIG_CoverCheck])
			{
				lReturn = RC_DIO_Err_JIGCorverCheck;

				OnLog_Err(_T("I/O : JIG Cover is Opened !!"));
				OnAddAlarm_F(_T("I/O : JIG Cover is Opened !!"));

				StopProcess_Test_All();

				AfxMessageBox(_T("I/O : Opened JIG Cover!!"), MB_SYSTEMMODAL);
			}
		}
	}


	return lReturn;
}

LRESULT CTestManager_EQP_ImgT::OnDIn_Start()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_Start();



	return lReturn;
}

LRESULT CTestManager_EQP_ImgT::OnDIn_Stop()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_Stop();



	return lReturn;
}

//=============================================================================
// Method		: OnDIn_CheckSafetyIO
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 18:36
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnDIn_CheckSafetyIO()
{
	LRESULT lReturn = RC_OK;

	//	lReturn = CTestManager_EQP::OnDIn_CheckSafetyIO();

	if (FALSE == m_stInspInfo.byDIO_DI[DI_ImgT_00_MainPower])
	{
		lReturn = RC_DIO_Err_MainPower;
	}

	if (FALSE == m_stInspInfo.byDIO_DI[DI_ImgT_01_EMO])
	{
		lReturn = RC_DIO_Err_EMO;
	}

	if (FALSE == m_stInspInfo.byDIO_DI[DI_ImgT_03_AreaSensor] && TRUE == m_stOption.Inspector.bUseAreaSensor_Err)
	{
		lReturn = RC_DIO_Err_AreaSensor;
	}

	if (FALSE == m_stInspInfo.byDIO_DI[DI_ImgT_04_DoorSensor] && TRUE == m_stOption.Inspector.bUseDoorOpen_Err)
	{
		lReturn = RC_DIO_Err_DoorSensor;
	}

	return lReturn;
}

//=============================================================================
// Method		: OnDIn_CheckJIGCoverStatus
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/3/19 - 10:25
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnDIn_CheckJIGCoverStatus()
{
	LRESULT lReturn = RC_OK;

	if (m_stOption.Inspector.bUseJIGCoverSensor)
	{
		//lReturn = CTestManager_EQP::OnDIn_CheckJIGCoverStatus();

		// IKC 커버 센서 없음
		if (Model_IKC == m_stInspInfo.RecipeInfo.ModelType)
			return lReturn;

		// * JIG Cover 체크 (Clear : 커버 덮은 상태, Set : 커버 열린 상태)
		if (!m_stInspInfo.byDIO_DI[DI_ImgT_02_CoverSensor])
		{
			lReturn = RC_DIO_Err_JIGCorverCheck;
		}
	}


	return lReturn;
}

//=============================================================================
// Method		: OnDOut_BoardPower
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bOn
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/6 - 16:55
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnDOut_BoardPower(__in BOOL bOn, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_BoardPower(bOn, nParaIdx);



	return lReturn;
}

LRESULT CTestManager_EQP_ImgT::OnDOut_FluorescentLamp(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_FluorescentLamp(bOn);



	return lReturn;
}

LRESULT CTestManager_EQP_ImgT::OnDOut_StartLamp(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_StartLamp(bOn);



	return lReturn;
}

LRESULT CTestManager_EQP_ImgT::OnDOut_StopLamp(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_StopLamp(bOn);



	return lReturn;
}

//=============================================================================
// Method		: OnDOut_TowerLamp
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in enLampColor nLampColor
// Parameter	: __in BOOL bOn
// Qualifier	:
// Last Update	: 2018/3/31 - 11:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnDOut_TowerLamp(__in enLampColor nLampColor, __in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	//lReturn = CTestManager_EQP::OnDOut_TowerLamp(nLampColor, bOn);

	enum_IO_SignalType enSignalType;

	if (ON == bOn)
	{
		enSignalType = IO_SignalT_SetOn;
	}
	else
	{
		enSignalType = IO_SignalT_SetOff;
	}

	switch (nLampColor)
	{
	case Lamp_Red:
		lReturn = m_Device.DigitalIOCtrl.Set_DO_Status(DO_ImgT_11_TowerLampRed, enSignalType);
		break;
	case Lamp_Yellow:
		lReturn = m_Device.DigitalIOCtrl.Set_DO_Status(DO_ImgT_12_TowerLampYellow, enSignalType);
		break;
	case Lamp_Green:
		lReturn = m_Device.DigitalIOCtrl.Set_DO_Status(DO_ImgT_13_TowerLampGreen, enSignalType);
		break;
	case Lamp_All:
		for (UINT nIdx = 0; nIdx < 3; nIdx++)
		{
			lReturn = m_Device.DigitalIOCtrl.Set_DO_Status(DO_ImgT_11_TowerLampRed + nIdx, enSignalType);
		}
		break;
	default:
		break;
	}

	return lReturn;
}

LRESULT CTestManager_EQP_ImgT::OnDOut_Buzzer(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_Buzzer(bOn);

	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_EEPROM_Write
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/2/12 - 14:37
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnDAQ_EEPROM_Write(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	switch (m_stInspInfo.RecipeInfo.ModelType)
	{
	case Model_OMS_Entry:
		lReturn = OnDAQ_EEPROM_OMS_Entry_ImgT(nIdxBrd);
		break;

	case Model_OMS_Front:
	case Model_OMS_Front_Set:
		lReturn = OnDAQ_EEPROM_OMS_Front_ImgT(nIdxBrd);
		break;

	case Model_MRA2:
		lReturn = OnDAQ_EEPROM_MRA2_ImgT(nIdxBrd);
		break;

	case Model_IKC:
		lReturn = OnDAQ_EEPROM_IKC_ImgT(nIdxBrd);
		break;

	default:
		break;
	}

	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_EEPROM_OMS_Entry_ImgT
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/2/13 - 17:19
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnDAQ_EEPROM_OMS_Entry_ImgT(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;


	return lReturn;
}

LRESULT CTestManager_EQP_ImgT::OnDAQ_EEPROM_OMS_Front_ImgT(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;


	return lReturn;
}

LRESULT CTestManager_EQP_ImgT::OnDAQ_EEPROM_MRA2_ImgT(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;


	return lReturn;
}

LRESULT CTestManager_EQP_ImgT::OnDAQ_EEPROM_IKC_ImgT(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;


	return lReturn;
}

//=============================================================================
// Method		: OnManualSequence
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaID
// Parameter	: __in enParaManual enFunc
// Qualifier	: 메뉴얼 시퀀스 동작 테스트
// Last Update	: 2017/10/27 - 18:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnManualSequence(__in UINT nParaID, __in enParaManual enFunc)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnManualSequence(nParaID, enFunc);



	return lReturn;
}

//=============================================================================
// Method		: OnManualTestItem
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaID
// Parameter	: __in UINT nStepIdx
// Qualifier	:
// Last Update	: 2017/10/27 - 18:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnManualTestItem(__in UINT nParaID, __in UINT nStepIdx)
{
	LRESULT lReturn = RC_OK;

	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;
	INT_PTR iStepCnt = m_stInspInfo.RecipeInfo.StepInfo.GetCount();

	// * 기구 이동		
	if (TRUE == pStepInfo->StepList[nStepIdx].bUseMoveY)
	{
#ifndef MOTION_NOT_USE
		lReturn = m_MotionSequence.OnActionTesting_ImgT_Y(pStepInfo->StepList[nStepIdx].nMoveY, m_stOption.Inspector.bUseBlindShutter);
#endif
	}

	// * 검사 시작
	if (TRUE == pStepInfo->StepList[nStepIdx].bTest)
	{
		if (OpMode_DryRun != m_stInspInfo.OperateMode)
		{
			StartTest_ImageTest_TestItem(pStepInfo->StepList[nStepIdx].nTestItem, nStepIdx, nParaID);
		}
	}

	return lReturn;
}
//=============================================================================
// Method		: OnCheck_DeviceComm
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnCheck_DeviceComm()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnCheck_DeviceComm();



	return lReturn;
}

//=============================================================================
// Method		: OnCheck_SaftyJIG
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnCheck_SaftyJIG()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnCheck_SaftyJIG();



	return lReturn;
}

//=============================================================================
// Method		: OnCheck_RecipeInfo
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::OnCheck_RecipeInfo()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnCheck_RecipeInfo();



	return lReturn;
}

//=============================================================================
// Method		: OnMakeReport
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 14:25
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnJugdement_And_Report(__in UINT nParaIdx)
{
	CFile_Report fileReport;


	//fileReport.SaveFinalizeResult2DCal(nParaIdx, m_stInspInfo.CamInfo[nParaIdx].szBarcode, m_stInspInfo.CamInfo[nParaIdx].st2D_Result);


	
}

void CTestManager_EQP_ImgT::OnJugdement_And_Report_All()
{
	OnUpdateYield();
	OnSaveWorklist();
}

//=============================================================================
// Method		: CreateTimer_UpdateUI
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in DWORD DueTime
// Parameter	: __in DWORD Period
// Qualifier	:
// Last Update	: 2017/11/8 - 14:17
// Desc.		: 파워 서플라이 모니터링
//=============================================================================
void CTestManager_EQP_ImgT::CreateTimer_UpdateUI(__in DWORD DueTime /*= 5000*/, __in DWORD Period /*= 250*/)
{
	// 		__super::CreateTimer_UpdateUI(5000, 2500);
}

void CTestManager_EQP_ImgT::DeleteTimer_UpdateUI()
{
	// 		__super::DeleteTimer_UpdateUI();
}

//=============================================================================
// Method		: OnMonitor_TimeCheck
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 13:28
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnMonitor_TimeCheck()
{
	CTestManager_EQP::OnMonitor_TimeCheck();

}

//=============================================================================
// Method		: OnMonitor_UpdateUI
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 21:19
// Desc.		: Power Supply 모니터링
//=============================================================================
void CTestManager_EQP_ImgT::OnMonitor_UpdateUI()
{
	CTestManager_EQP::OnMonitor_UpdateUI();

}

//=============================================================================
// Method		: OnSaveWorklist
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/5/10 - 20:24
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnSaveWorklist()
{
	// 	m_stInspInfo.WorklistInfo.Reset();
	// 
	// 	ST_CamInfo* pstCam = NULL;
	// 
	// 	CString szText;
	// 	CString szTime;
	// 	SYSTEMTIME lcTime;
	// 
	// 	GetLocalTime(&lcTime);
	// 	szTime.Format(_T("'%04d-%02d-%02d %02d:%02d:%02d.%03d"), lcTime.wYear, lcTime.wMonth, lcTime.wDay,
	// 		lcTime.wHour, lcTime.wMinute, lcTime.wSecond, lcTime.wMilliseconds);
	// 
	// 	for (UINT nChIdx = 0; nChIdx < USE_CHANNEL_CNT; nChIdx++)
	// 	{
	// 		if (FALSE == m_stInspInfo.bTestEnable[nChIdx])
	// 			continue;
	// 
	// 		pstCam = &m_stInspInfo.CamInfo[nChIdx];
	// 
	// 		m_stInspInfo.WorklistInfo.Time = szTime;
	// 		m_stInspInfo.WorklistInfo.EqpID = m_stInspInfo.szEquipmentID;	// g_szInsptrSysType[m_InspectionType];
	// 		m_stInspInfo.WorklistInfo.SWVersion;
	// 		m_stInspInfo.WorklistInfo.Model = m_stInspInfo.RecipeInfo.szModelCode;
	// 		m_stInspInfo.WorklistInfo.Barcode = pstCam->szBarcode;
	// 		m_stInspInfo.WorklistInfo.Result = g_TestResult[pstCam->nJudgment].szText;
	// 
	// 		szText.Format(_T("%d"), nChIdx + 1);
	// 		m_stInspInfo.WorklistInfo.Socket = szText;
	// 
	// 
	// 		m_stInspInfo.WorklistInfo.MakeItemz();
	// 
	// 		// 파일 저장
	// 		m_Worklist.Save_FinalResult_List(m_stInspInfo.Path.szReport, &pstCam->TestTime.tmStart_Loading, &m_stInspInfo.WorklistInfo);
	// 
	// 		// UI 표시
	// 		OnInsertWorklist();
	// 
	// 	}// End of for
}

//=============================================================================
// Method		: OnInitialize
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/26 - 6:03
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnInitialize()
{
	CTestManager_EQP::OnInitialize();

}

//=============================================================================
// Method		: OnFinalize
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/26 - 6:03
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnFinalize()
{
	CTestManager_EQP::OnFinalize();

}

//=============================================================================
// Method		: SetPermissionMode
// Access		: public  
// Returns		: void
// Parameter	: __in enPermissionMode nAcessMode
// Qualifier	:
// Last Update	: 2016/11/9 - 18:52
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::SetPermissionMode(__in enPermissionMode nAcessMode)
{
	CTestManager_EQP::SetPermissionMode(nAcessMode);


}


void CTestManager_EQP_ImgT::TestResultView(UINT nTestID, UINT nPara, LPVOID pParam){
	
	m_Test_ResultDataView.TestResultView(nTestID, nPara, pParam);
}
void CTestManager_EQP_ImgT::TestResultView_Reset(UINT nTestID, UINT nPara){

	m_Test_ResultDataView.TestResultView_Reset(nTestID, nPara);
}

// void CTestManager_EQP_ImgT::Mes_EachTest_Save(UINT nTestID, UINT nPara){
//	m_Test_ResultDataView.TestResultView_Reset(nTestID, nPara);
//}

void CTestManager_EQP_ImgT::EachTest_ResultDataSave(UINT nTestID, UINT nPara){


	ST_MES_TestItemLog m_MesData;
	CString str;
	SYSTEMTIME systime;

	GetLocalTime(&systime);

	//- 시간
	m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&systime);

 	//- Model
	m_MesData.Model = m_stInspInfo.RecipeInfo.szRecipeFile;
 	//- SW Ver
	m_MesData.SWVersion = m_Worklist.GetSWVersion(g_szProgramName[m_InspectionType]);
	if (m_stInspInfo.CamInfo[0].szBarcode.IsEmpty())
	{
		m_MesData.Barcode =_T("No Barcode");
	}
	else{
		m_MesData.Barcode = m_stInspInfo.CamInfo[0].szBarcode;

	}
 
 	//- Channel
	str.Format(_T("%d"), nPara);
 	m_MesData.Socket = str;
 
 	//- Result + Data
 	UINT DataNum = 0;
 	CString Data[100];
 
	CString strResult;
	UINT nResult = 0;
 
	CString strTestName;
	switch (nTestID)
	{
	case TI_ImgT_Fn_ECurrent:
		m_TestMgr_TestMes.Current_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("Current");
		break;
	case TI_ImgT_Fn_OpticalCenter:
		m_TestMgr_TestMes.OpticalCenter_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("OpticalCenter");
		break;
	case TI_ImgT_Fn_FOV:
		m_TestMgr_TestMes.Fov_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("FOV");
		break;
	case TI_ImgT_Fn_SFR:
		m_TestMgr_TestMes.SFR_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult, 0);
		strTestName = g_szSFR_TestNum[0];
		break;
	case TI_ImgT_Fn_SFR_2:
		m_TestMgr_TestMes.SFR_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult, 1);
		strTestName = g_szSFR_TestNum[1];
		break;
	case TI_ImgT_Fn_SFR_3:
		m_TestMgr_TestMes.SFR_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult, 2);
		strTestName = g_szSFR_TestNum[2];
		break;
	case TI_ImgT_Fn_Distortion:
		m_TestMgr_TestMes.Distortion_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("DIstortion");
		break;
	case TI_ImgT_Fn_Rotation:
		m_TestMgr_TestMes.Rotate_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("Rotation");
		break;
	case TI_ImgT_Fn_Stain:
		m_TestMgr_TestMes.Stain_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("Statin");
		break;
	case TI_ImgT_Fn_Particle_SNR:
		m_TestMgr_TestMes.DynamicRange_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("DynamicRange");
		break;
	case TI_ImgT_Fn_DefectPixel:
		m_TestMgr_TestMes.Defectpixel_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("DefectPixel");
		break;
	case TI_ImgT_Fn_Intensity:
		m_TestMgr_TestMes.Intensity_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("Shading(RI)");
		break;
	case TI_ImgT_Fn_Shading:
		m_TestMgr_TestMes.Shading_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("Shading(SNR)");
		break;
	case TI_ImgT_Fn_SNR_Light:
		m_TestMgr_TestMes.SNR_Light_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("SNR_Light");
		break;
//#ifdef SET_GWANGJU
	case TI_ImgT_Fn_HotPixel:
		m_TestMgr_TestMes.HotPixel_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("HotPixel");
		break;
	case TI_ImgT_Fn_FPN:
		m_TestMgr_TestMes.FPN_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("FPN");
		break;
	case TI_ImgT_Fn_3DDepth:
		m_TestMgr_TestMes.Depth_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("3D_Depth");
		break;
	case TI_ImgT_Fn_EEPROM_Verify:
		m_TestMgr_TestMes.EEPROM_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("EEPROM_Verify");
		break;
//#endif
	case TI_ImgT_Fn_TemperatureSensor:
		m_TestMgr_TestMes.TemperatureSensor_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("TemperatureSensor");
		break;

	case TI_ImgT_Fn_Particle_Entry:
		m_TestMgr_TestMes.Particle_Entry_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("Particle_Entry");
		break;
	default:
		break;
	}
	m_MesData.Result.Format(_T("%d"), nResult);

	m_Worklist.Save_TestItemLog_List(m_stInspInfo.Path.szReport, &systime, &m_MesData, strTestName);

 	//  	// 	/*결과*/
//  		m_MesData.Itemz.Add(Data[Distortion_W_FullArea + t]);
//  		m_MesData.ItemHeaderz.Add(g_lpszHeader_Distortion_Worklist[Distortion_W_FullArea + t]);
}

//=============================================================================
// Method		: OnReset_CamInfo
// Access		: virtual public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/30 - 19:25
// Desc.		:
//=============================================================================
void CTestManager_EQP_ImgT::OnReset_CamInfo(__in UINT nParaIdx /*= 0*/)
{
	m_stInspInfo.ResetCamInfo(nParaIdx);
}


//=============================================================================
// Method		: _TI_ImgT_HotPixel
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:39
// Desc.		:
//=============================================================================
//#ifdef SET_GWANGJU
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_HotPixel(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//*** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;
	UINT nImgW = 0, nImgH = 0;

	if (RC_OK != OnLightBrd_PowerOn(m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_HotPixel].fVolt, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_HotPixel].wStep)){
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stParticleData.FailData();
		lReturn = RC_Light_Brd_Err_Ack;
		return lReturn;
	}
	Sleep(m_stInspInfo.RecipeInfo.nLightDelay);

	// 이미지 버퍼에 복사
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szTestFileName, TRUE, _T("Hot_Pixel"))){
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stParticleData.FailData();
		lReturn = RC_NoImage;
		return lReturn;
	}

	lReturn = m_tm_Test.HotPixelFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stImageBuf[nParaIdx].lpwImage_16bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stImageQ.stHotPixelOpt, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stHotPixelData);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_HotPixel].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_ImgT_FPN
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:39
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_FPN(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//*** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;
	UINT nImgW = 0, nImgH = 0;

	if (RC_OK != OnLightBrd_PowerOn(m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_FPN].fVolt, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_FPN].wStep)){
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stParticleData.FailData();
		lReturn = RC_Light_Brd_Err_Ack;
		return lReturn;
	}

	Sleep(m_stInspInfo.RecipeInfo.nLightDelay);

	// 이미지 버퍼에 복사
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szTestFileName, TRUE, _T("Fixed_Pattern"))){
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stParticleData.FailData();
		lReturn = RC_NoImage;
		return lReturn;
	}

	lReturn = m_tm_Test.FPNFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stImageBuf[nParaIdx].lpwImage_16bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stImageQ.stFPNOpt, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stFPNData);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_FPN].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_ImgT_3D_Depth
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:39
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_3D_Depth(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//*** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;
	UINT nImgW = 0, nImgH = 0;

	// 이미지 버퍼에 복사
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szTestFileName, TRUE, _T("3D_Depth"))){
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stParticleData.FailData();
		lReturn = RC_NoImage;
		return lReturn;
	}

	lReturn = m_tm_Test.T3D_depthFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stImageBuf[nParaIdx].lpwImage_16bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stImageQ.st3D_DepthOpt, m_stInspInfo.CamInfo[nParaIdx].stImageQ.st3D_DepthData);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_3DDepth].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_ImgT_EEPRON_Verify
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:39
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_EEPRON_Verify(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//*** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;
	UINT nImgW = 0, nImgH = 0;
	BYTE arbyBufRead[256] = { 0, };

	// * Read :  Max 256 Byte
	if (FALSE == m_Device.DAQ_LVDS.I2C_Read_Repeat(0, (BYTE)m_stInspInfo.RecipeInfo.stImageQ.stEEPROM_VerifyOpt.wslaveid, 1, 0x00, 0xFF, arbyBufRead))
	{
		TRACE(_T("EEPROM Read Failed : First Read Failed"));
		return RC_EEPROM_Err_Read;
	}
	
	int nCntX = 0;
	int nCntY = 0;
	float fDataX, fDataY;

	for (int t = 0; t < 4; t++)
	{
		if (arbyBufRead[m_stInspInfo.RecipeInfo.stImageQ.stEEPROM_VerifyOpt.wocxaddress + t] == 0xff)
		{
			nCntX++;
		}
		if (arbyBufRead[m_stInspInfo.RecipeInfo.stImageQ.stEEPROM_VerifyOpt.wocyaddress + t] == 0xff)
		{
			nCntY++;
		}
	}
	if (nCntX == 4)
	{
		fDataX = -1.0;
	}
	else{

		memcpy(&fDataX, &arbyBufRead[m_stInspInfo.RecipeInfo.stImageQ.stEEPROM_VerifyOpt.wocxaddress], 4);
	}
	if (nCntY == 4)
	{
		fDataY = -1.0;
	}
	else{

		//memcpy(&fDataX, &arbyBufRead[m_stEEPROM.wOC_X_Addr], 4);
		memcpy(&fDataY, &arbyBufRead[m_stInspInfo.RecipeInfo.stImageQ.stEEPROM_VerifyOpt.wocyaddress], 4);
	}

	m_stInspInfo.CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.fOC_X = fDataX;
	m_stInspInfo.CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.fOC_Y = fDataY;


	BOOL	bMinOCX = m_stInspInfo.RecipeInfo.stImageQ.stEEPROM_VerifyOpt.stSpec_Min[Spec_EEP_OC_X].bEnable;
	BOOL	bMaxOCX = m_stInspInfo.RecipeInfo.stImageQ.stEEPROM_VerifyOpt.stSpec_Max[Spec_EEP_OC_X].bEnable;

	double dbMinDevOCX = m_stInspInfo.RecipeInfo.stImageQ.stEEPROM_VerifyOpt.stSpec_Min[Spec_EEP_OC_X].dbValue;
	double dbMaxDevOCX = m_stInspInfo.RecipeInfo.stImageQ.stEEPROM_VerifyOpt.stSpec_Max[Spec_EEP_OC_X].dbValue;

	BOOL	bMinUseOCY = m_stInspInfo.RecipeInfo.stImageQ.stEEPROM_VerifyOpt.stSpec_Min[Spec_EEP_OC_Y].bEnable;
	BOOL	bMaxUseOCY = m_stInspInfo.RecipeInfo.stImageQ.stEEPROM_VerifyOpt.stSpec_Max[Spec_EEP_OC_Y].bEnable;

	double dbMinDevOCY = m_stInspInfo.RecipeInfo.stImageQ.stEEPROM_VerifyOpt.stSpec_Min[Spec_EEP_OC_Y].dbValue;
	double dbMaxDevOCY = m_stInspInfo.RecipeInfo.stImageQ.stEEPROM_VerifyOpt.stSpec_Max[Spec_EEP_OC_Y].dbValue;

	// 판정
	m_stInspInfo.CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.nEachResult[Spec_EEP_OC_X] = m_tm_Test.Get_MeasurmentData(bMinOCX, bMaxOCX, dbMinDevOCX, dbMaxDevOCX, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.fOC_X);
	m_stInspInfo.CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.nEachResult[Spec_EEP_OC_Y] = m_tm_Test.Get_MeasurmentData(bMinUseOCY, bMaxUseOCY, dbMinDevOCY, dbMaxDevOCY, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.fOC_Y);

	m_stInspInfo.CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.nResult = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.nEachResult[Spec_EEP_OC_X] & m_stInspInfo.CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.nEachResult[Spec_EEP_OC_Y];

	m_stInspInfo.CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.fEachData[Spec_EEP_OC_X] = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.fOC_X;
	m_stInspInfo.CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.fEachData[Spec_EEP_OC_Y] = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.fOC_Y;

	BYTE Buff[4] = { 0, };
	//-1차 checksum
	UINT16 crc_uint16 = 0;
	UINT8 crc = 0;
	crc_uint16 = crc16(crc_uint16, arbyBufRead, m_stInspInfo.RecipeInfo.stImageQ.stEEPROM_VerifyOpt.waddress);
	Buff[0] = crc_uint16 & 0xff;
	Buff[1] = crc_uint16 / 0x100;

	crc_uint16 = 0;
	crc = 0;
	crc_uint16 = crc16(crc_uint16, arbyBufRead, 0xfc);

	Buff[2] = crc_uint16 & 0xff;
	Buff[3] = crc_uint16 / 0x100;

	if ((arbyBufRead[m_stInspInfo.RecipeInfo.stImageQ.stEEPROM_VerifyOpt.waddress] == Buff[0]) && (arbyBufRead[m_stInspInfo.RecipeInfo.stImageQ.stEEPROM_VerifyOpt.waddress + 1] == Buff[1]))
	{
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.nEachResult[Spec_EEP_CheckSum1] = TRUE;
	}
	else
	{
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.nEachResult[Spec_EEP_CheckSum1] = FALSE;
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.nResult = FALSE;
	}

	if ((arbyBufRead[m_stInspInfo.RecipeInfo.stImageQ.stEEPROM_VerifyOpt.waddress + 2] == Buff[2]) && (arbyBufRead[m_stInspInfo.RecipeInfo.stImageQ.stEEPROM_VerifyOpt.waddress + 3] == Buff[3]))
	{
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.nEachResult[Spec_EEP_CheckSum2] = TRUE;
	}
	else
	{
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.nEachResult[Spec_EEP_CheckSum2] = FALSE;
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.nResult = FALSE;
	}
	m_stInspInfo.CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.fEachData[Spec_EEP_CheckSum1] = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.nEachResult[Spec_EEP_CheckSum1];
	m_stInspInfo.CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.fEachData[Spec_EEP_CheckSum2] = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stEEPROM_VerifyData.nEachResult[Spec_EEP_CheckSum2];
	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_EEPROM_Verify].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}
//#endif

//=============================================================================
// Method		: _TI_ImgT_TemperatureSensor
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:39
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_TemperatureSensor(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//*** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;
	UINT nImgW = 0, nImgH = 0;
	BYTE arbyBufRead[256] = { 0, };
	DWORD dwTemp = NULL;
	double dbTemper = 0.0;

	// * Read :  Max 256 Byte
	CString strData;
	if (FALSE == m_Device.DAQ_LVDS.I2C_Read_Repeat(0, (BYTE)m_stInspInfo.RecipeInfo.stImageQ.stTemperatureSensorOpt.wslaveid, 1, m_stInspInfo.RecipeInfo.stImageQ.stTemperatureSensorOpt.waddress, 2, arbyBufRead))
	{

		TRACE(_T("TemperatureSensor Read Failed : First Read Failed"));
		return RC_Grab_Err_I2C_Read;

	}
	else
	{
		strData.Format(_T("%02x%02x"), arbyBufRead[0], arbyBufRead[1]);

		if (_T("0000") == strData)
		{
			m_stInspInfo.CamInfo[nParaIdx].stImageQ.stTemperatureSensorData.nEachResult[Spec_Temp] = FALSE;
			m_stInspInfo.CamInfo[nParaIdx].stImageQ.stTemperatureSensorData.nResult = FALSE;
			m_stInspInfo.CamInfo[nParaIdx].stImageQ.stTemperatureSensorData.dbTemperature = 0;
		}
		else
		{
			dwTemp = arbyBufRead[0] << 4;
			dwTemp |= arbyBufRead[1] >> 4;
			dbTemper = 0.0625 * dwTemp;

			m_stInspInfo.CamInfo[nParaIdx].stImageQ.stTemperatureSensorData.dbTemperature = dbTemper;

			BOOL	bMinTemp = m_stInspInfo.RecipeInfo.stImageQ.stTemperatureSensorOpt.stSpec_Min[Spec_Temp].bEnable;
			BOOL	bMaxTemp = m_stInspInfo.RecipeInfo.stImageQ.stTemperatureSensorOpt.stSpec_Max[Spec_Temp].bEnable;

			double dbMinTemp = m_stInspInfo.RecipeInfo.stImageQ.stTemperatureSensorOpt.stSpec_Min[Spec_Temp].dbValue;
			double dbMaxTemp = m_stInspInfo.RecipeInfo.stImageQ.stTemperatureSensorOpt.stSpec_Max[Spec_Temp].dbValue;

			// 판정
			m_stInspInfo.CamInfo[nParaIdx].stImageQ.stTemperatureSensorData.nEachResult[Spec_Temp] = m_tm_Test.Get_MeasurmentData(bMinTemp, bMaxTemp, dbMinTemp, dbMaxTemp, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stTemperatureSensorData.dbTemperature);

			m_stInspInfo.CamInfo[nParaIdx].stImageQ.stTemperatureSensorData.nResult = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stTemperatureSensorData.nEachResult[Spec_Temp];
		}
	
	}


	lReturn = m_stInspInfo.CamInfo[nParaIdx].stImageQ.stTemperatureSensorData.nResult;

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_TemperatureSensor].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_ImgT_Pro_Particle_Entry
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:39
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_ImgT::_TI_ImgT_Pro_Particle_Entry(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//*** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;
	UINT nImgW = 0, nImgH = 0;

	if (RC_OK != OnLightBrd_PowerOn(m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_Particle].fVolt, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_Particle].wStep)){
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stParticle_EntryData.FailData();
		lReturn = RC_Light_Brd_Err_Ack;
		return lReturn;
	}
	Sleep(m_stInspInfo.RecipeInfo.nLightDelay);

	// 이미지 버퍼에 복사
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, nImgW, nImgH, m_stInspInfo.CamInfo[nParaIdx].stImageQ.szTestFileName, TRUE, _T("Particle"))){
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		m_stInspInfo.CamInfo[nParaIdx].stImageQ.stParticle_EntryData.FailData();
		lReturn = RC_NoImage;
		return lReturn;
	}

	lReturn = m_tm_Test.Particle_EntryFunc(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stImageBuf[nParaIdx].lpwImage_16bit, nImgW, nImgH, m_stInspInfo.RecipeInfo.stImageQ.stParticle_EntryOpt, m_stInspInfo.CamInfo[nParaIdx].stImageQ.stParticle_EntryData);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_ImgT_Fn_Stain].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}
