﻿//*****************************************************************************
// Filename	: Wnd_LogView.h
// Created	: 2010/12/2
// Modified	: 2015/12/22
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#ifndef Wnd_LogView_h__
#define Wnd_LogView_h__

#pragma once

#include "VGStatic.h"
#include "GroupLogWnd.h"

//=============================================================================
// CWnd_LogView
//=============================================================================
class CWnd_LogView : public CWnd
{
	DECLARE_DYNAMIC(CWnd_LogView)

public:
	CWnd_LogView();
	virtual ~CWnd_LogView();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	
	CFont		m_font_Default;
	CFont		m_font_Data;

	CGroupLogWnd	m_log_All;
	CGroupLogWnd	m_log_Error;

public:
	
	void	AddLog		(LPCTSTR lpszLog, BOOL bError /*= FALSE*/, UINT nLogType /*= 0*/, COLORREF clrTextColor /*= RGB(0, 0, 0)*/);	

};

#endif // Wnd_LogView_h__


