﻿//*****************************************************************************
// Filename	: Wnd_RecipeView.cpp
// Created	: 2016/03/18
// Modified	: 2016/03/18
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#include "stdafx.h"
#include "Wnd_RecipeView.h"
#include "Dlg_ChkPassword.h"
#include "resource.h"

#define		IDC_ED_OUPUTVOLT		1050

#define		IDC_BN_NEW				1001
#define		IDC_BN_SAVE				1002
#define		IDC_BN_SAVE_AS			1003
#define		IDC_BN_LOAD				1004
#define		IDC_BN_IMAGELOAD		1006
#define		IDC_BN_REFRESH			1007
#define		IDC_LSB_MODEL			1008

#define		IDC_RB_TEST_OPT			1021
#define		IDC_RB_MOTOR_OPT		1022

#define		IDC_BN_POGOCNT			1011
#define		IDC_BN_MOTION			1012
#define		IDC_TB_OPTION			1013


#define		IDC_RB_CAMERA_SEL_LEFT		2001
#define		IDC_RB_CAMERA_SEL_RIGHT		2002


#define		IDC_RB_IMAGE_MODE_LIVE	    2101
#define		IDC_RB_IMAGE_MODE_STILL		2102

#define		IDC_BN_IMAGE_LOADIMAGE	    2201
#define		IDC_BN_IMAGE_SAVEIMAGE	    2202
//#define		IDC_BN_TEST_INDEX		3000

//=============================================================================
// CWnd_RecipeView
//=============================================================================
IMPLEMENT_DYNAMIC(CWnd_RecipeView, CWnd)

//=============================================================================
//
//=============================================================================
CWnd_RecipeView::CWnd_RecipeView()
{
	VERIFY(m_font_Default.CreateFont(
		20,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	VERIFY(m_font_Data.CreateFont(
		26,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	m_pDevice = NULL;
	nCamParaIdx = 0;
	SetFullPath	(_T(""));
	m_LoadImage = NULL;
}

CWnd_RecipeView::~CWnd_RecipeView()
{
	TRACE(_T("<<< Start ~CWnd_RecipeView >>> \n"));
	if (m_InspectionType == Sys_Image_Test)
	{
		m_stRecipeInfo.stImageQ.stSNR_LightOpt.st_SNR_LightRect.Release();
		m_stRecipeInfo.stImageQ.stShadingOpt.stShading_Rect.Release();
	}
	m_font_Default.DeleteObject();
	m_font_Data.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_RecipeView, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW	()
	ON_BN_CLICKED		(IDC_BN_NEW,				OnBnClickedBnNew			)
	ON_BN_CLICKED		(IDC_BN_SAVE,				OnBnClickedBnSave			)
	ON_BN_CLICKED		(IDC_BN_SAVE_AS,			OnBnClickedBnSaveAs			)
	ON_BN_CLICKED		(IDC_BN_LOAD,				OnBnClickedBnLoad			)
	ON_BN_CLICKED		(IDC_BN_IMAGELOAD,			OnBnClickedBnImage			)
	ON_BN_CLICKED		(IDC_BN_REFRESH,			OnBnClickedBnRefresh		)
	ON_CBN_SELENDOK(	IDC_LSB_MODEL,				OnLbnSelChangeRecipe		)
	ON_MESSAGE			(WM_FILE_RECIPE,			OnFileRecipe				)
	ON_MESSAGE			(WM_CHANGED_MODEL,			OnChangeRecipe				)
	ON_MESSAGE			(WM_REFESH_MODEL,			OnRefreshRecipeList			)
	ON_MESSAGE			(WM_SELECT_OVERLAY,			OnUpdateDataRoiList			)
	ON_MESSAGE			(WM_SELECT_ITEM_VIEW,		OnSelectResultView			)
	ON_MESSAGE			(WM_SELECT_ITEM_TEST,		OnSelectItemTest			)
	ON_MESSAGE			(WM_CHANGED_MODEL_TYPE,		OnChangedModelType			)
	ON_MESSAGE			(WM_CHANGE_OPTIONPIC,		OnChangeOptionPic			)
	ON_BN_CLICKED		(IDC_RB_CAMERA_SEL_LEFT,	OnBnClickedRbCameraSelLeft	)
	ON_BN_CLICKED		(IDC_RB_CAMERA_SEL_RIGHT,	OnBnClickedRbCameraSelRight	)
	ON_BN_CLICKED		(IDC_RB_IMAGE_MODE_LIVE,	OnBnClickedRbLiveMode		)
	ON_BN_CLICKED		(IDC_RB_IMAGE_MODE_STILL,	OnBnClickedRbStillMode		)
	ON_BN_CLICKED		(IDC_BN_IMAGE_LOADIMAGE,	OnBnClickedImageLoad		)
	ON_BN_CLICKED		(IDC_BN_IMAGE_SAVEIMAGE,	OnBnClickedImageSave		)
	//ON_LBN_SELCHANGE(IDC_LSB_MODEL, OnLbnSelChangeRecipe)
END_MESSAGE_MAP()

//=============================================================================
// CWnd_RecipeView 메시지 처리기입니다.
//=============================================================================
//=============================================================================
// Method		: CWnd_RecipeView::OnCreate
// Access		: protected 
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2015/12/9 - 0:05
// Desc.		:
//=============================================================================
int CWnd_RecipeView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	UINT	nWndID = 10;

	m_st_Image.SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_st_Image.SetColorStyle(CVGStatic::ColorStyle_DarkGray);

	m_st_File.SetStaticStyle(CVGStatic::StaticStyle_Title_Alt);
	m_st_File.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_File.SetFont_Gdip(L"Arial", 12.0F);

	m_st_Location.SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_st_Location.SetColorStyle(CVGStatic::ColorStyle_White);
	m_st_Location.SetFont_Gdip(L"Arial", 12.0F);

	m_st_Image.Create(_T("Image View"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_st_File.Create(_T("Recipe Files"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_st_Location.Create(_T("Location"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
//	m_lst_RecipeList.Create(dwStyle | WS_HSCROLL | LBS_STANDARD | LBS_SORT | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, IDC_LSB_MODEL);
	m_Combo_ModelList.Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_LSB_MODEL);
	m_Combo_ModelList.SetFont(&m_font_Data);

	m_bn_Refresh.Create(_T("Refresh File List"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_REFRESH);
	m_bn_Refresh.SetMouseCursorHand();

	m_bn_New.Create(_T("New"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_NEW);
	m_bn_Save.Create(_T("Save (Apply Changes)"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_SAVE);
	m_bn_SaveAs.Create(_T("Save As"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_SAVE_AS);
	m_bn_Load.Create(_T("Load"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_LOAD);
	m_bn_ImageLoad.Create(_T("Image Load"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_IMAGELOAD);
	m_bn_New.SetMouseCursorHand();
	m_bn_Save.SetMouseCursorHand();
	m_bn_SaveAs.SetMouseCursorHand();
	m_bn_Load.SetMouseCursorHand();
	m_bn_ImageLoad.SetMouseCursorHand();
	
	// 탭 컨트롤
	m_tc_Option.Create(CMFCTabCtrl::STYLE_3D, rectDummy, this, IDC_TB_OPTION, CMFCTabCtrl::LOCATION_TOP);
	
	m_wnd_Image.SetOwner(this);
	m_wnd_Image.Create(NULL, _T(""), dwStyle | WS_BORDER, rectDummy, this, IDC_STATIC);

	m_wnd_TestResult.SetOwner(GetOwner());
	m_wnd_TestResult.Create(NULL, _T("Result"), dwStyle /*| WS_BORDER*/, rectDummy, this, 100);

	//!SH _181121: 검사기 별로 나누어야 될듯
	for (int t = 0; t < USE_CHANNEL_CNT; t++ )
	{
		m_wnd_TestResult_ImgT[t].SetOwner(GetOwner());
		m_wnd_TestResult_ImgT[t].Create(NULL, _T("Result IMGT"), dwStyle /*| WS_BORDER*/, rectDummy, this, 2000 + t);
	}

	m_wnd_TestResult_ImgT[Para_Left].ShowWindow(SW_SHOW);
	m_wnd_TestResult_ImgT[Para_Right].ShowWindow(SW_HIDE);

	for (int t = 0; t < USE_CHANNEL_CNT; t++)
	{
		m_wnd_TestResult_Foc[t].SetOwner(GetOwner());
		m_wnd_TestResult_Foc[t].Create(NULL, _T("Result FOC"), dwStyle /*| WS_BORDER*/, rectDummy, this, 2100 + t);
	}

	m_wnd_TestResult_Foc[Para_Left].ShowWindow(SW_SHOW);
	m_wnd_TestResult_Foc[Para_Right].ShowWindow(SW_HIDE);
	
	m_wnd_ModelCfg.SetOwner(this);
	m_wnd_ModelCfg.Create(NULL, _T("Model"), dwStyle /*| WS_BORDER*/, rectDummy, &m_tc_Option, nWndID++);
	
	m_wnd_TestStepCfg.SetOwner(GetOwner());
	m_wnd_TestStepCfg.Create(NULL, _T("Test Step"), dwStyle /*| WS_BORDER*/, rectDummy, &m_tc_Option, nWndID++);

#ifdef USE_PRESET_MODE
	if (Sys_Stereo_Cal == m_InspectionType)
	{
		m_wnd_Preset.SetOwner(GetOwner());
		m_wnd_Preset.Set_UsePresetCount(MAX_TESTSTEP_PRESET);
		m_wnd_Preset.Create(NULL, _T("Preset"), dwStyle /*| WS_BORDER*/, rectDummy, &m_tc_Option, nWndID++);
	}
#endif

	m_wnd_TestItemCfg.SetOwner(this);
	m_wnd_TestItemCfg.SetCameraParaIdx(&nCamParaIdx);
	m_wnd_TestItemCfg.Create(NULL, _T("Test Item"), dwStyle /*| WS_BORDER*/, rectDummy, &m_tc_Option, nWndID++);

	m_wnd_TestItemImgTCfg.SetOwner(this);
	m_wnd_TestItemImgTCfg.SetCameraParaIdx(&nCamParaIdx);
	m_wnd_TestItemImgTCfg.Create(NULL, _T("Test Item"), dwStyle /*| WS_BORDER*/, rectDummy, &m_tc_Option, nWndID++);

	m_wnd_Cfg_TestItem_Foc.SetOwner(this);
	m_wnd_Cfg_TestItem_Foc.SetCameraParaIdx(&nCamParaIdx);
	m_wnd_Cfg_TestItem_Foc.Create(NULL, _T("Test Itemss"), dwStyle /*| WS_BORDER*/, rectDummy, &m_tc_Option, nWndID++);

// 	m_wnd_DelayCfg.SetOwner(this);
// 	m_wnd_DelayCfg.Create(NULL, _T("Test Item"), dwStyle /*| WS_BORDER*/, rectDummy, &m_tc_Option, nWndID++);

	m_wnd_ConsumInfoCfg.SetOwner(GetOwner());
	m_wnd_ConsumInfoCfg.Create(NULL, _T("POGO"), dwStyle /*| WS_BORDER*/, rectDummy, &m_tc_Option, nWndID++);

	m_wnd_DAQ.SetOwner(this);
	m_wnd_DAQ.Create(NULL, _T("Grabber"), dwStyle /*| WS_BORDER*/, rectDummy, &m_tc_Option, nWndID++);
	
	UINT nTabIndex = 0;
 	m_tc_Option.AddTab(&m_wnd_ModelCfg,			_T("Model"),		nTabIndex++, FALSE);
	m_tc_Option.AddTab(&m_wnd_TestStepCfg,		_T("Test Step"),	nTabIndex++, FALSE);
#ifdef USE_PRESET_MODE
	if (Sys_Stereo_Cal == m_InspectionType)
	{
		m_tc_Option.AddTab(&m_wnd_Preset,		_T("Preset"),		nTabIndex++, FALSE);
	}
#endif

	switch (m_InspectionType)
	{
	case Sys_2D_Cal:
		m_tc_Option.AddTab(&m_wnd_TestItemCfg, _T("Test Item"), nTabIndex++, FALSE);
		break;
	case Sys_3D_Cal:
		m_tc_Option.AddTab(&m_wnd_TestItemCfg, _T("Test Item"), nTabIndex++, FALSE);
		break;
	case Sys_Stereo_Cal:
		m_tc_Option.AddTab(&m_wnd_TestItemCfg, _T("Test Item"), nTabIndex++, FALSE);
		break;
	case Sys_Focusing:
		m_tc_Option.AddTab(&m_wnd_Cfg_TestItem_Foc, _T("Test Item"), nTabIndex++, FALSE);
		break;
	case Sys_Image_Test:
		m_tc_Option.AddTab(&m_wnd_TestItemImgTCfg, _T("Test Item"), nTabIndex++, FALSE);
		break;
	default:
		break;
	}

 	m_tc_Option.AddTab(&m_wnd_ConsumInfoCfg,	_T("Consumables"),	nTabIndex++, FALSE);
	m_tc_Option.AddTab(&m_wnd_DAQ,				_T("Grabber"),		nTabIndex++, FALSE);

	m_tc_Option.SetActiveTab(0);
	m_tc_Option.EnableTabSwap(FALSE);

	// DAQ 설정 불가
	m_wnd_DAQ.Enable_Setting(FALSE);

	// 파일 감시 쓰레드 설정
	m_ModelWatch.SetOwner(GetSafeHwnd(), WM_REFESH_MODEL);
	if (!m_strRecipePath.IsEmpty())
	{
		m_ModelWatch.SetWatchOption(m_strRecipePath, RECIPE_FILE_EXT);
		m_ModelWatch.BeginWatchThrFunc();

		m_ModelWatch.RefreshList();

		RefreshRecipeFileList(m_ModelWatch.GetFileList());
	}

	m_wnd_Image.SetPtr_RecipeInfor(&m_stRecipeInfo);


	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		m_wnd_ImageLive[nIdx].Create(NULL, _T(""), dwStyle | WS_BORDER, rectDummy, this, nWndID++);

	}
	m_wnd_ImageLive[Para_Right].ShowWindow(SW_HIDE);

	// * 좌/우 카메라 선택
	m_rb_CameraSelect[Para_Left].Create(_T("Left Camera"), WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_AUTORADIOBUTTON | WS_GROUP, rectDummy, this, IDC_RB_CAMERA_SEL_LEFT);
	m_rb_CameraSelect[Para_Right].Create(_T("Right Camera"), WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_AUTORADIOBUTTON, rectDummy, this, IDC_RB_CAMERA_SEL_RIGHT);

	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		m_rb_CameraSelect[nIdx].SetFont(&m_font_Default);
		m_rb_CameraSelect[nIdx].m_nFlatStyle = CMFCButton::BUTTONSTYLE_SEMIFLAT;
		m_rb_CameraSelect[nIdx].SetImage(IDB_SELECTNO_16);
		m_rb_CameraSelect[nIdx].SetCheckedImage(IDB_SELECT_16);
		m_rb_CameraSelect[nIdx].SizeToContent();
	}
	m_rb_CameraSelect[Para_Left].SetCheck(TRUE);
	m_rb_CameraSelect[Para_Right].SetCheck(FALSE);

	m_rb_ImageMode[ImageMode_LiveCam].Create(_T(""), WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX, rectDummy, this, IDC_RB_IMAGE_MODE_LIVE);
	m_rb_ImageMode[ImageMode_StillShotImage].Create(_T(""), WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX, rectDummy, this, IDC_RB_IMAGE_MODE_STILL);

	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		m_rb_ImageMode[nIdx].SetFont(&m_font_Default);
		m_rb_ImageMode[nIdx].m_nFlatStyle = CMFCButton::BUTTONSTYLE_SEMIFLAT;
	}
	m_rb_ImageMode[ImageMode_LiveCam].SetImage(IDB_BITMAP_LIVE_1);
	m_rb_ImageMode[ImageMode_LiveCam].SetCheckedImage(IDB_BITMAP_LIVE);
	m_rb_ImageMode[ImageMode_LiveCam].SizeToContent();
	m_rb_ImageMode[ImageMode_LiveCam].SetCheck(TRUE);

	m_rb_ImageMode[ImageMode_StillShotImage].SetImage(IDB_BITMAP_PIC_1);
	m_rb_ImageMode[ImageMode_StillShotImage].SetCheckedImage(IDB_BITMAP_PIC);
	m_rb_ImageMode[ImageMode_StillShotImage].SizeToContent();


	m_bt_ImageLoad.Create(_T(""), WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX, rectDummy, this, IDC_BN_IMAGE_LOADIMAGE);
	m_bt_ImageLoad.SetFont(&m_font_Default);
	m_bt_ImageLoad.m_nFlatStyle = CMFCButton::BUTTONSTYLE_SEMIFLAT;
	m_bt_ImageLoad.SetImage(IDB_BITMAP_OPEN_1);
	m_bt_ImageLoad.SetCheckedImage(IDB_BITMAP_OPEN);
	m_bt_ImageLoad.SizeToContent();
	m_bt_ImageLoad.SetCheck(FALSE);

	m_bt_ImageCapture.Create(_T(""), WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX, rectDummy, this, IDC_BN_IMAGE_SAVEIMAGE);
	m_bt_ImageCapture.SetFont(&m_font_Default);
	m_bt_ImageCapture.m_nFlatStyle = CMFCButton::BUTTONSTYLE_SEMIFLAT;
	m_bt_ImageCapture.SetImage(IDB_BITMAP_CAM_1);
	m_bt_ImageCapture.SetCheckedImage(IDB_BITMAP_CAM);
	m_bt_ImageCapture.SizeToContent();
	m_bt_ImageCapture.SetCheck(FALSE);

	return 0;
}

//=============================================================================
// Method		: CWnd_RecipeView::OnSize
// Access		: protected 
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2015/12/9 - 0:05
// Desc.		:
//=============================================================================
void CWnd_RecipeView::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	int iMagrin = 10;
	int iSpacing = 5;
	int iCateSpacing = 10;

	int iLeft = iMagrin;
	int iTop = iMagrin;
	int iWidth = cx - iMagrin - iMagrin;
	int iHeight = cy - iMagrin - iMagrin;

	int iCtrlHeight = 40;
	int iCtrlWidth  = 147;

	int iCateWidth = (iWidth - (iCateSpacing * 4)) / 5;
	int iHalfWidth = iCateWidth + iCateSpacing + iCateWidth;
	int iLstHeight = iHeight - (iSpacing * 2) - (iCtrlHeight * 2);
	int iTempHeight = iHeight;
	
	// 파일 리스트
	iLeft = iMagrin;
	iTop = iMagrin;	
	m_st_File.MoveWindow(iLeft, iTop, iCateWidth, iCtrlHeight);
	
	iLeft += iCateWidth + iCateSpacing;
	iWidth = iWidth - iCateWidth - iCateSpacing;
	m_st_Location.MoveWindow(iLeft, iTop, iWidth, iCtrlHeight);
	
// 	if (Sys_Focusing == m_InspectionType)
// 	{
// 		// 파일 리스트
// 		iLeft = iMagrin;
// 		iTop += iCtrlHeight + iSpacing;
// 		m_lst_RecipeList.MoveWindow(iLeft, iTop, iCateWidth, iLstHeight);
// 		iTop += iLstHeight + iSpacing;
// 		m_bn_Refresh.MoveWindow(iLeft, iTop, iCateWidth, iCtrlHeight);
// 
// 		// 메뉴
// 		iLeft = cx - iMagrin - iCtrlWidth;
// 		iTop = iMagrin + iCtrlHeight + iCateSpacing;
// 		m_bn_Load.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
// 		iLeft -= (iCtrlWidth + iSpacing);
// 		m_bn_SaveAs.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
// 		iLeft -= (iCtrlWidth + iSpacing);
// 		m_bn_Save.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
// 		iLeft -= (iCtrlWidth + iSpacing);
// 		m_bn_New.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
// 		
// 		m_bn_ImageLoad.MoveWindow(0, 0, 0, 0);
// 
// 		m_st_Image.MoveWindow(0, 0, 0, 0);
// 		m_wnd_TestResult.MoveWindow(0, 0, 0, 0);
// 
// 		// 버튼
// 		iTop += iCtrlHeight + iCateSpacing;
// 
// 		// 탭 컨트롤
// 		iLeft = iMagrin + iCateWidth + iCateSpacing;
// 		iTop += iCtrlHeight + iCateSpacing;
// 		iHeight = cy - iMagrin - iTop;
// 		m_tc_Option.MoveWindow(iLeft, iTop, iWidth, iHeight);
// 	}
// 	else
	{
		int iImageW = 642;
		int iImageH = 480;

		// 메뉴
		iLeft = iMagrin;
		iTop = iMagrin + iCtrlHeight + iSpacing;
		m_Combo_ModelList.MoveWindow(iLeft, iTop, iCateWidth, iCtrlHeight);

		iLeft = cx - iMagrin - iCtrlWidth;
		m_bn_Load.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
		
		iLeft -= (iCtrlWidth + iSpacing);
		m_bn_SaveAs.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
		
		iLeft -= (iCtrlWidth + iSpacing);
		m_bn_Save.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
		
		iLeft -= (iCtrlWidth + iSpacing);
		m_bn_New.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
		
		iLeft -= (iCtrlWidth + iSpacing);
		m_bn_ImageLoad.MoveWindow(0, 0, 0, 0);

		iTop += iCtrlHeight + iSpacing;

		// 이미지 뷰
		int iTempH = 21;
		iLeft = iMagrin;
		m_st_Image.MoveWindow(iLeft, iTop, iImageW, iTempH);

		iTop += iTempH + iSpacing;

		int iCamCount = g_IR_ModelTable[m_stRecipeInfo.ModelType].Camera_Cnt;
		int iCamWidth = g_IR_ModelTable[m_stRecipeInfo.ModelType].Img_Width;
		int iCamHeight = g_IR_ModelTable[m_stRecipeInfo.ModelType].Img_Height;
		if ((Sys_Stereo_Cal == m_InspectionType) && ((Model_IKC == m_stRecipeInfo.ModelType) || (Model_MRA2 == m_stRecipeInfo.ModelType)))
		{
			iCamHeight = g_IR_ModelTable[m_stRecipeInfo.ModelType].Img_Height + 4;
		}

		//int iImgWidth		= 640;
		//int iImgHeight	= 480;
		int iImgWidth = iImageW;
// 		if (iImageW % 2 == 1)
// 		{
// 			iImgWidth = iImageW - 1;
// 		}

		if (0 >= iCamWidth)
			return;

		iImageH = iImgWidth * iCamHeight / iCamWidth;
		m_wnd_ImageLive[0].MoveWindow(iLeft, iTop, iImageW, iImageH);
		m_wnd_ImageLive[1].MoveWindow(iLeft, iTop, iImageW, iImageH);
		
		iTop += iImageH;
		iTempH = cy - iMagrin - iTop;
		int nRbH = (iTempH - iSpacing) / 12 + iSpacing;
		m_rb_CameraSelect[Para_Left].MoveWindow(iLeft, iTop, iCtrlWidth+10, nRbH);
		
		iLeft += iCtrlWidth + 10 + 1;
		m_rb_CameraSelect[Para_Right].MoveWindow(iLeft, iTop, iCtrlWidth + 10, nRbH);
		
		iLeft = iMagrin;
		iLeft += iImageW;
		iLeft -= (nRbH);
		m_bt_ImageCapture.MoveWindow(iLeft, iTop, nRbH, nRbH);
		
		iLeft -= (nRbH - 1);
		m_bt_ImageLoad.MoveWindow(iLeft, iTop, nRbH, nRbH);
		
		iLeft -= (nRbH - 1);
		iLeft -= (nRbH - 1);
		m_rb_ImageMode[ImageMode_StillShotImage].MoveWindow(iLeft, iTop, nRbH, nRbH);
		
		iLeft -= (nRbH-1);
		m_rb_ImageMode[ImageMode_LiveCam].MoveWindow(iLeft, iTop, nRbH, nRbH);

		iTop += nRbH + iSpacing;
		iTempH = iTempH - nRbH - iSpacing;
		iLeft = iMagrin; 
		if (Sys_Focusing == m_InspectionType)
		{
			for (int t = 0; t < USE_CHANNEL_CNT; t++)
			{
				m_wnd_TestResult_ImgT[t].MoveWindow(0, 0, 0, 0);
				m_wnd_TestResult_Foc[t].MoveWindow(iLeft, iTop, iImageW, iTempH);
			}
			m_wnd_TestResult.MoveWindow(0, 0, 0, 0);
		}
		else if( Sys_Image_Test == m_InspectionType)
		{
			for (int t = 0; t < USE_CHANNEL_CNT; t++)
			{
				m_wnd_TestResult_ImgT[t].MoveWindow(iLeft, iTop, iImageW, iTempH);
				m_wnd_TestResult_Foc[t].MoveWindow(0, 0, 0, 0);
			}
			m_wnd_TestResult.MoveWindow(0, 0, 0, 0);
		}
		else
		{
			m_wnd_TestResult.MoveWindow(iLeft, iTop, iImageW, iTempH);
			for (int t = 0; t < USE_CHANNEL_CNT; t++)
			{
				m_wnd_TestResult_ImgT[t].MoveWindow(0, 0, 0, 0);
				m_wnd_TestResult_Foc[t].MoveWindow(0, 0, 0, 0);
			}
		}

		// 탭 컨트롤
		iLeft	= iMagrin + iImageW + iSpacing;
		iWidth  = cx - iMagrin - iMagrin - iImageW - iSpacing;
		iTop	= iMagrin + iCtrlHeight + iSpacing + iCtrlHeight + iSpacing;
		iHeight = cy - iMagrin - iTop;
		m_tc_Option.MoveWindow(iLeft, iTop, iWidth, iHeight);
	}
}

//=============================================================================
// Method		: OnShowWindow
// Access		: protected  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2016/5/28 - 22:31
// Desc.		:
//=============================================================================
void CWnd_RecipeView::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);
	
//	m_stRecipeInfo.TestItemOpt.iPicItem = -1;

	if (TRUE == bShow)
	{
		m_wnd_TestResult.SetTestItem(-1);
	}
	else
	{
		m_wnd_Image.OnUpdataResetImage();
		m_wnd_TestResult.SetUpdateClear();
	}
}

//=============================================================================
// Method		: CWnd_RecipeView::PreCreateWindow
// Access		: virtual protected 
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2015/12/9 - 0:05
// Desc.		:
//=============================================================================
BOOL CWnd_RecipeView::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnBnClickedBnNew
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/6 - 13:10
// Desc.		:
//=============================================================================
void CWnd_RecipeView::OnBnClickedBnNew()
{
	New_Recipe();
}

void CWnd_RecipeView::OnBnClickedBnSave()
{
	Save_Recipe();
}

void CWnd_RecipeView::OnBnClickedBnSaveAs()
{
	SaveAs_Recipe();
}

void CWnd_RecipeView::OnBnClickedBnLoad()
{
	Load_Recipe();
}

//=============================================================================
// Method		: OnBnClickedBnImage
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/15 - 18:52
// Desc.		:
//=============================================================================
void CWnd_RecipeView::OnBnClickedBnImage()
{
	Load_Image();
}

void CWnd_RecipeView::OnBnClickedBnRefresh()
{
	RefreshRecipeFileList(m_ModelWatch.GetFileList());
}

//=============================================================================
// Method		: OnLbnSelChangeRecipe
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/27 - 12:54
// Desc.		:
//=============================================================================
void CWnd_RecipeView::OnLbnSelChangeRecipe()
{
	CString strValue;
	int iSel = -1;
	if (0 <= (iSel = m_Combo_ModelList.GetCurSel()))
	{		
		m_Combo_ModelList.GetLBText(iSel, strValue);

		// 현재 편집중인 모델파일과 같으면 리턴
		if (0 == strValue.Compare(m_stRecipeInfo.szRecipeFile))
			return;

		m_stRecipeInfo.szRecipeFile = strValue;

		CString strFullPath;

		strFullPath.Format(_T("%s%s.%s"), m_strRecipePath, m_stRecipeInfo.szRecipeFile, RECIPE_FILE_EXT);

		// 파일 불러오기
 		if (m_fileRecipe.Load_RecipeFile(strFullPath, m_stRecipeInfo))
 		{
#ifdef USE_PRESET_MODE
			m_fileRecipe.Load_PresetInfo(strFullPath, m_stRecipeInfo.stPresetStepInfo);
			m_fileRecipe.LoadXML_PresetStepInfo(m_strRecipePath, m_stRecipeInfo.stPresetStepInfo);
#endif

 			SetFullPath(m_stRecipeInfo.szRecipeFile);
 
 			// UI에 세팅
 			SetRecipeInfo();
 		}

		// 모델 데이터 불러오기
		//GetOwner()->SendNotifyMessage(WM_CHANGED_MODEL, (WPARAM)m_stRecipeInfo.szRecipeName.GetBuffer(), 0);
		//m_stRecipeInfo.szRecipeName.ReleaseBuffer();
	}
}
		
//=============================================================================
// Method		: OnFileRecipe
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/3/17 - 16:51
// Desc.		:
//=============================================================================
LRESULT CWnd_RecipeView::OnFileRecipe(WPARAM wParam, LPARAM lParam)
{
	UINT nType = (UINT)wParam;

	switch (nType)
	{
	case ID_FILE_NEW:
		New_Recipe();
		break;

	case ID_FILE_SAVE:
		Save_Recipe();
		break;

	case ID_FILE_SAVE_AS:
		SaveAs_Recipe();
		break;

	case ID_FILE_OPEN:
		Load_Recipe();
		break;

	default:
		break;
	}

	return 0;
}

//=============================================================================
// Method		: OnChangeRecipe
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/3/18 - 14:16
// Desc.		:
//=============================================================================
LRESULT CWnd_RecipeView::OnChangeRecipe(WPARAM wParam, LPARAM lParam)
{
	CString strModel = (LPCTSTR)wParam;
	CString strFullPath;

	strFullPath.Format(_T("%s%s.%s"), m_strRecipePath, strModel, RECIPE_FILE_EXT);

	// 파일 불러오기
	if (m_fileRecipe.Load_RecipeFile(strFullPath, m_stRecipeInfo))
	{
#ifdef USE_PRESET_MODE
		m_fileRecipe.Load_PresetInfo(strFullPath, m_stRecipeInfo.stPresetStepInfo);
		m_fileRecipe.LoadXML_PresetStepInfo(m_strRecipePath, m_stRecipeInfo.stPresetStepInfo);
#endif
		//m_stRecipeInfo.szRecipeName = strModel;

		// UI에 세팅
		SetRecipeInfo();
	}

	return 0;
}

//=============================================================================
// Method		: OnRefreshRecipeList
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2016/3/18 - 15:18
// Desc.		:
//=============================================================================
LRESULT CWnd_RecipeView::OnRefreshRecipeList(WPARAM wParam, LPARAM lParam)
{
	RefreshRecipeFileList(m_ModelWatch.GetFileList());

	return 0;
}

//=============================================================================
// Method		: OnUpdataDataRoiList
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/10/17 - 16:44
// Desc.		:
//=============================================================================
LRESULT CWnd_RecipeView::OnUpdateDataRoiList(WPARAM wParam, LPARAM lParam)
{
	m_stRecipeInfo.nOverlayItem = (enOverlayItem)lParam;

	m_wnd_TestItemCfg.Set_RecipeInfo(&m_stRecipeInfo);

	int nTestImgID = 0;
	int nTestFocID = 0;

	switch (m_stRecipeInfo.nOverlayItem)
	{
	case Ovr_LockingScrew:
		nTestFocID = TI_Foc_Motion_LockingScrew;
		break;
	case Ovr_ReleaseScrew:
		nTestFocID = TI_Foc_Motion_ReleaseScrew;
		break;
	case Ovr_PreFocus:
		nTestFocID = TI_Foc_Fn_PreFocus;
		break;
	case Ovr_OpticalCenter:
		nTestImgID = TI_ImgT_Fn_OpticalCenter;
		nTestFocID = TI_Foc_Fn_OpticalCenter;
		break;
	case Ovr_ECurrent:
		nTestImgID = TI_ImgT_Fn_ECurrent;
		nTestFocID = TI_Foc_Fn_ECurrent;
		break;
	case Ovr_FOV:
		nTestImgID = TI_ImgT_Fn_FOV;
		break;
	case Ovr_SFR:
		nTestImgID = TI_ImgT_Fn_SFR;
		nTestFocID = TI_Foc_Fn_SFR;
		break;
	case Ovr_SFR_2:
		nTestImgID = TI_ImgT_Fn_SFR_2;
		break;
	case Ovr_SFR_3:
		nTestImgID = TI_ImgT_Fn_SFR_3;
		break;
	case Ovr_Distortion:
		nTestImgID = TI_ImgT_Fn_Distortion;
		break;
	case Ovr_Rotate:
		nTestImgID = TI_ImgT_Fn_Rotation;
		nTestFocID = TI_Foc_Fn_Rotation;
		break;
	case Ovr_Particle:
		nTestImgID = TI_ImgT_Fn_Stain;
		nTestFocID = TI_Foc_Fn_Stain;
		break;
	case Ovr_Particle_SNR:
		nTestImgID = TI_ImgT_Fn_Particle_SNR;
		break;
	case Ovr_DefectPixel:
		nTestImgID = TI_ImgT_Fn_DefectPixel;
		nTestFocID = TI_Foc_Fn_DefectPixel;
		break;
	case Ovr_Intensity:
		nTestImgID = TI_ImgT_Fn_Intensity;
		break;
	case Ovr_Shading:
		nTestImgID = TI_ImgT_Fn_Shading;
		break;
	case Ovr_SNR_Light:
		nTestImgID = TI_ImgT_Fn_SNR_Light;
		break;
	case Ovr_ActiveAlign:
		nTestFocID = TI_Foc_Fn_ActiveAlign;
		break;

//#ifdef SET_GWANGJU
	case Ovr_HotPixel:
		nTestImgID = TI_ImgT_Fn_HotPixel;
		break;
	case Ovr_FPN:
		nTestImgID = TI_ImgT_Fn_FPN;
		break;
	case Ovr_3D_Depth:
		nTestImgID = TI_ImgT_Fn_3DDepth;
		break;
	case Ovr_EEPROM_Verify:
		nTestImgID = TI_ImgT_Fn_EEPROM_Verify;
		break;
//#endif
	case Ovr_Temperature:
		nTestImgID = TI_ImgT_Fn_TemperatureSensor;
		break;

	case Ovr_Particle_Entry:
		nTestImgID = TI_ImgT_Fn_Particle_Entry;
		break;
		default:
			break;
	}

	switch (m_InspectionType)
	{
	case Sys_2D_Cal:
	case Sys_3D_Cal:
	case Sys_Stereo_Cal:
		break;
	case Sys_Focusing:
		m_wnd_TestResult_Foc[Para_Left].SetUIData(nTestFocID, NULL);
		m_wnd_TestResult_Foc[Para_Right].SetUIData(nTestFocID, NULL);
		break;
	case Sys_Image_Test:
		m_wnd_TestResult_ImgT[Para_Left].SetUIData(nTestImgID, NULL);
		m_wnd_TestResult_ImgT[Para_Right].SetUIData(nTestImgID, NULL);
		break;
	default:
		break;
	}

	return 0;
}

//=============================================================================
// Method		: OnSelectItemTest
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/10/21 - 11:51
// Desc.		:
//=============================================================================
LRESULT CWnd_RecipeView::OnSelectItemTest(WPARAM wParam, LPARAM lParam)
{
	int iItem = (int)lParam;

	if (iItem == -1)
	{
//		m_stRecipeInfo.TestItemOpt.Reset();
		m_wnd_TestResult.SetUpdateClear();
		return 0;
	}

	switch (m_InspectionType)
	{
	case Sys_Focusing:
		break;
	case Sys_2D_Cal:
		break;
	case Sys_Image_Test:
// 		if (iItem == TID_ImgT_OpticalCenter)
// 		{
// 			m_tm_Test.CenterPointManual(m_wnd_Image.GetLoadImage(), &m_stRecipeInfo.TestItemOpt.stCenterPoint);
// 		}
// 		else if (iItem == TID_ImgT_DynamicRange)
// 		{
// 			m_tm_Test.DynamicManual(m_wnd_Image.GetLoadImage(), &m_stRecipeInfo.TestItemOpt.stDynamic);
// 		}
// 		else if (iItem == TID_ImgT_SNR_BW)
// 		{
// 			m_tm_Test.SNR_BWManual(m_wnd_Image.GetLoadImage(), &m_stRecipeInfo.TestItemOpt.stSNR_BW);
// 		}
// 		else if (iItem == TID_ImgT_SNR_IQ)
// 		{
// 			m_tm_Test.SNR_IQManual(m_wnd_Image.GetLoadImage(), &m_stRecipeInfo.TestItemOpt.stSNR_IQ);
// 		}
// 		else if (iItem == TID_ImgT_FOV)
// 		{
// 			m_tm_Test.FOVManual(m_wnd_Image.GetLoadImage(), &m_stRecipeInfo.TestItemOpt.stFOV);
// 		}
// 		else if (iItem == TID_ImgT_SFR)
// 		{
// 			m_tm_Test.SFRManual(m_wnd_Image.GetLoadImage(), &m_stRecipeInfo.TestItemOpt.stSFR);
// 		}
// 		else if (iItem == TID_ImgT_Distortion)
// 		{
// 			m_tm_Test.DistortionManual(m_wnd_Image.GetLoadImage(), &m_stRecipeInfo.TestItemOpt.stDistortion);
// 		}
// 		else if (iItem == TID_ImgT_Tilt)
// 		{
// 			m_tm_Test.TiltManual(m_wnd_Image.GetLoadImage(), &m_stRecipeInfo.TestItemOpt.stTilt);
// 		}
// 		else if (iItem == TID_ImgT_Rotation)
// 		{
// 			m_tm_Test.RotateManual(m_wnd_Image.GetLoadImage(), &m_stRecipeInfo.TestItemOpt.stRotate);
// 		}
// 		else if (iItem == TID_Pat_DefectPixel)
// 		{
// 			m_tm_Test.DefectPixelManual(m_wnd_Image.GetLoadImage(), &m_stRecipeInfo.TestItemOpt.stDefectPixel[m_stRecipeInfo.nTestNum]);
// 		}
// 		else if (iItem == TID_Pat_Rtllumination)
// 		{
// 			m_tm_Test.ShadingManual(m_wnd_Image.GetLoadImage(), &m_stRecipeInfo.TestItemOpt.stShading[m_stRecipeInfo.nTestNum]);
// 		}
// 		else if (iItem == TID_Pat_Light_SNR)
// 		{
// 			m_tm_Test.SNR_LightManual(m_wnd_Image.GetLoadImage(), &m_stRecipeInfo.TestItemOpt.stSNR_Light[m_stRecipeInfo.nTestNum]);
// 		}
// 		else if (iItem == TID_Pat_FPN)
// 		{
// 			m_tm_Test.FPNManual(m_wnd_Image.GetLoadImage(), &m_stRecipeInfo.TestItemOpt.stFPN[m_stRecipeInfo.nTestNum]);
// 		}
// 		else if (iItem == TID_Pat_DeadPixel)
// 		{
// 			m_tm_Test.ParticleManual(m_wnd_Image.GetLoadImage(), &m_stRecipeInfo.TestItemOpt.stParticle[m_stRecipeInfo.nTestNum]);
// 		}
		break;

	case Sys_Stereo_Cal:
		break;

	case Sys_3D_Cal:
		break;

	default:
		break;
	}

	m_wnd_TestResult.SetUpdateResult(&m_stRecipeInfo, iItem);

	return 0;
}

//=============================================================================
// Method		: OnChangedModelType
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2018/2/8 - 17:40
// Desc.		:
//=============================================================================
LRESULT CWnd_RecipeView::OnChangedModelType(WPARAM wParam, LPARAM lParam)
{
	// GetParent()->SendNotifyMessage(WM_CHANGED_MODEL_TYPE, (WPARAM)ModelType, 0);
	enModelType nModelType = (enModelType)wParam;

	m_wnd_DAQ.SetUIData_ByModel(nModelType);
	m_wnd_TestItemCfg.Set_ModelType(nModelType);

	return 0;
}


void CWnd_RecipeView::OnBnClickedRbCameraSelLeft()
{
	Set_CameraSelect(Para_Left);
}


void CWnd_RecipeView::OnBnClickedRbCameraSelRight()
{
	Set_CameraSelect(Para_Right);
}

void CWnd_RecipeView::Set_CameraSelect(__in INT iChIdx, __in BOOL bUpdateRadioButton/* = TRUE*/)
{

	if (iChIdx < USE_CHANNEL_CNT)
	{
		nCamParaIdx = iChIdx;
		if (Para_Left == iChIdx)
		{
			m_wnd_ImageLive[Para_Left].ShowWindow(SW_SHOW);
			m_wnd_ImageLive[Para_Right].ShowWindow(SW_HIDE);

			m_wnd_TestResult_ImgT[Para_Left].ShowWindow(SW_SHOW);
			m_wnd_TestResult_ImgT[Para_Right].ShowWindow(SW_HIDE);

			m_wnd_TestResult_Foc[Para_Left].ShowWindow(SW_SHOW);
			m_wnd_TestResult_Foc[Para_Right].ShowWindow(SW_HIDE);

			if (bUpdateRadioButton)
			{
				m_rb_CameraSelect[Para_Right].SetCheck(BST_UNCHECKED);
				m_rb_CameraSelect[Para_Left].SetCheck(BST_CHECKED);
			}
		}
		else if (Para_Right == iChIdx)
		{

			m_wnd_TestResult_ImgT[Para_Right].ShowWindow(SW_SHOW);
			m_wnd_TestResult_ImgT[Para_Left].ShowWindow(SW_HIDE);

			m_wnd_TestResult_Foc[Para_Right].ShowWindow(SW_SHOW);
			m_wnd_TestResult_Foc[Para_Left].ShowWindow(SW_HIDE);

			m_wnd_ImageLive[Para_Right].ShowWindow(SW_SHOW);
			m_wnd_ImageLive[Para_Left].ShowWindow(SW_HIDE);

			if (bUpdateRadioButton)
			{
				m_rb_CameraSelect[Para_Left].SetCheck(BST_UNCHECKED);
				m_rb_CameraSelect[Para_Right].SetCheck(BST_CHECKED);
			}
		}
	}
}


//=============================================================================
// Method		: OnSelectResultView
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/10/21 - 11:28
// Desc.		:
//=============================================================================
LRESULT CWnd_RecipeView::OnSelectResultView(WPARAM wParam, LPARAM lParam)
{
	int iItem = (int)lParam;
	m_wnd_TestResult.SetTestItem(iItem);

	return 0;
}

//=============================================================================
// Method		: New_Recipe
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/17 - 16:51
// Desc.		:
//=============================================================================
void CWnd_RecipeView::New_Recipe()
{
	if (IDYES == AfxMessageBox(_T("Do you want to create a new one?"), MB_YESNO))
	{
		m_stRecipeInfo.Reset();
	 
	 	SetRecipeInfo();

		//초기화
		SaveAs_Recipe();
	 }
}

//=============================================================================
// Method		: Save_Recipe
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/17 - 17:15
// Desc.		:
//=============================================================================
void CWnd_RecipeView::Save_Recipe()
{
	// UI상의 데이터 얻기
	GetRecipeInfo();
	
	CString strFullPath;
	CString strFileTitle;
	CString strFileExt;
	strFileExt.Format(_T("Model File (*.%s)| *.%s||"), RECIPE_FILE_EXT, RECIPE_FILE_EXT);

 	if (m_stRecipeInfo.szRecipeFile.IsEmpty())
 	{
 	 	CFileDialog fileDlg(FALSE, RECIPE_FILE_EXT, NULL, OFN_OVERWRITEPROMPT, strFileExt);
 	 	fileDlg.m_ofn.lpstrInitialDir = m_strRecipePath;
 	 
 	 	if (fileDlg.DoModal() == IDOK)
 	 	{
 	 		strFullPath = fileDlg.GetPathName();
 	 		strFileTitle = fileDlg.GetFileTitle();	 		
 	 
 	 		// 저장	 		
 			if (m_fileRecipe.Save_RecipeFile(strFullPath, &m_stRecipeInfo))
 			{				
 				// 리스트 모델 갱신
 				m_stRecipeInfo.szRecipeFile = strFileTitle;

				SetFullPath(m_stRecipeInfo.szRecipeFile);
 			}
#ifdef USE_PRESET_MODE
			m_fileRecipe.Save_PresetInfo(strFullPath, &m_stRecipeInfo.stPresetStepInfo, m_InspectionType);
			m_fileRecipe.SaveXML_PresetStepInfo(m_strRecipePath, &m_stRecipeInfo.stPresetStepInfo, m_InspectionType);
#endif

			// 모델 데이터 불러오기
			GetOwner()->SendNotifyMessage(WM_CHANGED_MODEL, (WPARAM)m_stRecipeInfo.szRecipeFile.GetBuffer(), 0);
			m_stRecipeInfo.szRecipeFile.ReleaseBuffer();
 	 	}
 	}
 	else
 	{
 		strFullPath.Format(_T("%s%s.%s"), m_strRecipePath, m_stRecipeInfo.szRecipeFile, RECIPE_FILE_EXT);
 
 	 	// 저장	 	
 	 	if (m_fileRecipe.Save_RecipeFile(strFullPath, &m_stRecipeInfo))
		{
 	 		//AfxMessageBox(_T("저장 되었습니다."));
		}
#ifdef USE_PRESET_MODE
		m_fileRecipe.Save_PresetInfo(strFullPath, &m_stRecipeInfo.stPresetStepInfo, m_InspectionType);
		m_fileRecipe.SaveXML_PresetStepInfo(m_strRecipePath, &m_stRecipeInfo.stPresetStepInfo, m_InspectionType);
#endif

		// 모델 데이터 불러오기
		GetOwner()->SendNotifyMessage(WM_CHANGED_MODEL, (WPARAM)m_stRecipeInfo.szRecipeFile.GetBuffer(), 0);
		SetRecipeInfo();
		
		m_stRecipeInfo.szRecipeFile.ReleaseBuffer();
 	}
}

//=============================================================================
// Method		: SaveAs_Recipe
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/17 - 17:15
// Desc.		:
//=============================================================================
void CWnd_RecipeView::SaveAs_Recipe()
{
	// UI상의 데이터 얻기
	GetRecipeInfo();

	CString strFullPath;
	CString strFileTitle;
	CString strFileExt;
	strFileExt.Format(_T("Model File (*.%s)| *.%s||"), RECIPE_FILE_EXT, RECIPE_FILE_EXT);

	CFileDialog fileDlg(FALSE, RECIPE_FILE_EXT, NULL, OFN_OVERWRITEPROMPT, strFileExt);
	fileDlg.m_ofn.lpstrInitialDir = m_strRecipePath;

 	if (fileDlg.DoModal() == IDOK)
 	{
 		strFullPath = fileDlg.GetPathName();
 		strFileTitle = fileDlg.GetFileTitle();
 
 		// 저장	 		
 		if (m_fileRecipe.Save_RecipeFile(strFullPath, &m_stRecipeInfo))
 		{
 			// 리스트 모델 갱신
 			m_stRecipeInfo.szRecipeFile = strFileTitle; 			
			
			SetFullPath(m_stRecipeInfo.szRecipeFile);
 		}
#ifdef USE_PRESET_MODE
		m_fileRecipe.Save_PresetInfo(strFullPath, &m_stRecipeInfo.stPresetStepInfo, m_InspectionType);
		m_fileRecipe.SaveXML_PresetStepInfo(m_strRecipePath, &m_stRecipeInfo.stPresetStepInfo, m_InspectionType);
#endif

		// 모델 데이터 불러오기
		GetOwner()->SendNotifyMessage(WM_CHANGED_MODEL, (WPARAM)m_stRecipeInfo.szRecipeFile.GetBuffer(), 0);
		m_stRecipeInfo.szRecipeFile.ReleaseBuffer();
 	}
}

//=============================================================================
// Method		: Load_Recipe
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/17 - 17:16
// Desc.		:
//=============================================================================
void CWnd_RecipeView::Load_Recipe()
{
	if (IDNO == AfxMessageBox(_T("The data being edited is deleted.\r\n Do you want to continue?"), MB_YESNO))
	{
		return;
	}

	CString strFullPath;
	CString strFileTitle;
	CString strFileExt;
	strFileExt.Format(_T("Model File (*.%s)| *.%s||"), RECIPE_FILE_EXT, RECIPE_FILE_EXT);
	CString strFileSel;
	strFileSel.Format(_T("*.%s"), RECIPE_FILE_EXT); 

	// 파일 불러오기
	CFileDialog fileDlg(TRUE, RECIPE_FILE_EXT, strFileSel, OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, strFileExt);
	fileDlg.m_ofn.lpstrInitialDir = m_strRecipePath;
	 
	if (fileDlg.DoModal() == IDOK)
	{
	 	strFullPath = fileDlg.GetPathName();
	 	strFileTitle = fileDlg.GetFileTitle();	 	
	 
 		if (m_fileRecipe.Load_RecipeFile(strFullPath, m_stRecipeInfo))	 	
 	 	{
#ifdef USE_PRESET_MODE
			m_fileRecipe.Load_PresetInfo(strFullPath, m_stRecipeInfo.stPresetStepInfo);
			m_fileRecipe.LoadXML_PresetStepInfo(m_strRecipePath, m_stRecipeInfo.stPresetStepInfo);
#endif
 	 		// UI에 세팅
 	 		SetRecipeInfo();

			m_stRecipeInfo.szRecipeFile = strFileTitle;
			SetFullPath(m_stRecipeInfo.szRecipeFile);
 	 	}

		// 모델 데이터 불러오기
		GetOwner()->SendNotifyMessage(WM_CHANGED_MODEL, (WPARAM)m_stRecipeInfo.szRecipeFile.GetBuffer(), 0);
		m_stRecipeInfo.szRecipeFile.ReleaseBuffer();
		m_wnd_TestResult.SetUpdateClear();
	}
}

//=============================================================================
// Method		: Load_Image
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/15 - 18:53
// Desc.		:
//=============================================================================
void CWnd_RecipeView::Load_Image()
{
	m_wnd_TestResult.SetUpdateClear();
	m_wnd_Image.OnUpdataLoadImage();
}

//=============================================================================
// Method		: GetRecipeInfo
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/17 - 16:58
// Desc.		:
//=============================================================================
void CWnd_RecipeView::GetRecipeInfo()
{	
	m_wnd_ModelCfg.GetRecipeInfo(m_stRecipeInfo);

	// 소모품
	m_wnd_ConsumInfoCfg.SaveConsumInfoSetting();
	m_stRecipeInfo.szConsumablesFile = m_wnd_ConsumInfoCfg.GetConsumInfoFile();

	// 검사 스텝
	m_wnd_TestStepCfg.Get_StepInfo(m_stRecipeInfo.StepInfo);

	// 검사 옵션
	switch (m_InspectionType)
	{
	case Sys_2D_Cal:
	case Sys_3D_Cal:
	case Sys_Stereo_Cal:
		m_wnd_TestItemCfg.Get_RecipeInfo(m_stRecipeInfo);
		break;
	case Sys_Focusing:
		m_wnd_Cfg_TestItem_Foc.Get_RecipeInfo(m_stRecipeInfo);
		m_wnd_Cfg_TestItem_Foc.Get_TestItemInfo(m_stRecipeInfo, m_stRecipeInfo.TestItemInfo);
		break;
	case Sys_Image_Test:
		m_wnd_TestItemImgTCfg.Get_RecipeInfo(m_stRecipeInfo);
		m_wnd_TestItemImgTCfg.Get_TestItemInfo(m_stRecipeInfo, m_stRecipeInfo.TestItemInfo);
		break;
	default:
		break;
	}

	// DQA LVDS 설정
	m_wnd_DAQ.GetUIData(m_stRecipeInfo.stLVDSInfo);
//	m_wnd_DelayCfg.Get_RecipeInfo();

#ifdef USE_PRESET_MODE
	if (Sys_Stereo_Cal == m_InspectionType)
	{
		m_wnd_Preset.Get_PresetInfo(m_stRecipeInfo.stPresetStepInfo);
	}
#endif
}

//=============================================================================
// Method		: SetRecipeInfo
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/17 - 16:58
// Desc.		:
//=============================================================================
void CWnd_RecipeView::SetRecipeInfo()
{
	m_wnd_ModelCfg.SetRecipeInfo(&m_stRecipeInfo);

	// 소모품
	m_wnd_ConsumInfoCfg.SetConsumInfoFile(m_stRecipeInfo.szConsumablesFile);

	// 검사 스텝
	m_wnd_TestStepCfg.Set_StepInfo(&m_stRecipeInfo.StepInfo);

	// 검사 옵션
	switch (m_InspectionType)
	{
	case Sys_2D_Cal:
	case Sys_3D_Cal:
	case Sys_Stereo_Cal:
		m_wnd_TestItemCfg.Set_RecipeInfo(&m_stRecipeInfo);
		break;
	case Sys_Focusing:
		m_wnd_Cfg_TestItem_Foc.Set_RecipeInfo(&m_stRecipeInfo);
		break;
	case Sys_Image_Test:
		m_wnd_TestItemImgTCfg.Set_RecipeInfo(&m_stRecipeInfo);
		break;
	default:
		break;
	}

	// DQA LVDS 설정
	m_wnd_DAQ.SetUIData_ByModel(m_stRecipeInfo.ModelType, &m_stRecipeInfo.stLVDSInfo);
	//m_wnd_DAQ.SetUIData(&m_stRecipeInfo.stLVDSInfo);

	m_wnd_TestItemCfg.Set_ModelType(m_stRecipeInfo.ModelType);

//	m_wnd_DelayCfg.SetPtr_RecipeInfo(&m_stRecipeInfo);
//	m_wnd_DelayCfg.Set_RecipeInfo();
#ifdef USE_PRESET_MODE
	if (Sys_Stereo_Cal == m_InspectionType)
	{
		m_wnd_Preset.Set_PresetInfo(&m_stRecipeInfo.stPresetStepInfo);
	}
#endif

	for (int t = 0; t < USE_CHANNEL_CNT; t++)
	{
		m_wnd_TestResult_ImgT[t].SetModelType(m_stRecipeInfo.ModelType);
	}

	m_OverlayProc.SetModelType(m_stRecipeInfo.ModelType);
}

//=============================================================================
// Method		: SetFullPath
// Access		: protected  
// Returns		: void
// Parameter	: __in LPCTSTR szRecipeName
// Qualifier	:
// Last Update	: 2016/5/27 - 11:33
// Desc.		:
//=============================================================================
void CWnd_RecipeView::SetFullPath(__in LPCTSTR szRecipeName)
{
	m_strRecipeFullPath.Format(_T("%s%s.%s"), m_strRecipePath, szRecipeName, RECIPE_FILE_EXT);
	
	m_st_Location.SetText(m_strRecipeFullPath);
}

//=============================================================================
// Method		: SetSystemType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2017/9/26 - 13:54
// Desc.		:
//=============================================================================
void CWnd_RecipeView::SetSystemType(__in enInsptrSysType nSysType)
{
	m_InspectionType = nSysType;
	m_fileRecipe.SetSystemType(m_InspectionType);
	m_stRecipeInfo.SetSystemType(m_InspectionType);

	m_wnd_ModelCfg.SetSystemType(nSysType);
	m_wnd_TestStepCfg.SetSystemType(nSysType);
#ifdef USE_PRESET_MODE
	m_wnd_Preset.SetSystemType(nSysType);
#endif
	m_wnd_TestItemCfg.SetSystemType(nSysType);
	m_wnd_TestItemImgTCfg.SetSystemType(nSysType);
	m_wnd_Cfg_TestItem_Foc.SetSystemType(nSysType);
	m_wnd_TestResult.SetSystemType(nSysType);

	m_wnd_ConsumInfoCfg.SetSystemType(nSysType);
	m_wnd_DAQ.SetSystemType(nSysType);
}

//=============================================================================
// Method		: ChangePath
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR lpszRecipePath
// Qualifier	:
// Last Update	: 2016/6/24 - 13:58
// Desc.		:
//=============================================================================
void CWnd_RecipeView::ChangePath(__in LPCTSTR lpszRecipePath)
{
	if (NULL != lpszRecipePath)
		m_strRecipePath = lpszRecipePath;

	m_ModelWatch.SetWatchOption(m_strRecipePath, RECIPE_FILE_EXT);
	m_ModelWatch.EndWatchThrFunc();
	m_ModelWatch.BeginWatchThrFunc();
}

//=============================================================================
// Method		: SetRecipeFile
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szRecipe
// Qualifier	:
// Last Update	: 2016/3/18 - 16:43
// Desc.		:
//=============================================================================
void CWnd_RecipeView::SetRecipeFile(__in LPCTSTR szRecipe)
{
	CString strFullPath;
	strFullPath.Format(_T("%s%s.%s"), m_strRecipePath, szRecipe, RECIPE_FILE_EXT);

	if (m_fileRecipe.Load_RecipeFile(strFullPath, m_stRecipeInfo))
	{
#ifdef USE_PRESET_MODE
		m_fileRecipe.Load_PresetInfo(strFullPath, m_stRecipeInfo.stPresetStepInfo);
		m_fileRecipe.LoadXML_PresetStepInfo(m_strRecipePath, m_stRecipeInfo.stPresetStepInfo);
#endif

	 	// UI에 세팅
	 	SetRecipeInfo();

		m_stRecipeInfo.szRecipeFile = szRecipe;
		if (!m_stRecipeInfo.szRecipeFile.IsEmpty())
		{
			int iSel = m_Combo_ModelList.FindStringExact(0, m_stRecipeInfo.szRecipeFile);

			if (0 <= iSel)
			{
				m_Combo_ModelList.SetCurSel(iSel);
			}

			SetFullPath(m_stRecipeInfo.szRecipeFile);
		}
	}
}

//=============================================================================
// Method		: SetRecipeInfo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_RecipeInfo * pstRecipeInfo
// Qualifier	:
// Last Update	: 2017/10/14 - 17:52
// Desc.		:
//=============================================================================
void CWnd_RecipeView::SetRecipeInfo(__in const ST_RecipeInfo* pstRecipeInfo)
{
	//m_stRecipeInfo = (ST_RecipeInfo)*pstRecipeInfo;
	//SetRecipeInfo();
}

//=============================================================================
// Method		: RefreshRecipeFileList
// Access		: public  
// Returns		: void
// Parameter	: __in const CStringList * pFileList
// Qualifier	:
// Last Update	: 2016/6/25 - 14:24
// Desc.		:
//=============================================================================
void CWnd_RecipeView::RefreshRecipeFileList(__in const CStringList* pFileList)
{
	m_Combo_ModelList.ResetContent();

	INT_PTR iFileCnt = pFileList->GetCount();

	POSITION pos;
	for (pos = pFileList->GetHeadPosition(); pos != NULL;)
	{
		m_Combo_ModelList.AddString(pFileList->GetNext(pos));
	}

	// 이전에 선택되어있는 파일 다시 선택
	if (!m_stRecipeInfo.szRecipeFile.IsEmpty())
	{
		int iSel = m_Combo_ModelList.FindStringExact(0, m_stRecipeInfo.szRecipeFile);

		if (0 <= iSel)
		{
			m_Combo_ModelList.SetCurSel(iSel);
		}
	}
}

//=============================================================================
// Method		: InitOptionView
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/25 - 14:37
// Desc.		:
//=============================================================================
void CWnd_RecipeView::InitOptionView()
{
	m_tc_Option.SetActiveTab(0);
}

//=============================================================================
// Method		: ShowVideo_Overlay
// Access		: public  
// Returns		: void
// Parameter	: __in INT iChIdx
// Parameter	: __inout IplImage * lpVideo
// Parameter	: __in DWORD dwWidth
// Parameter	: __in DWORD dwHeight
// Qualifier	:
// Last Update	: 2018/2/25 - 9:51
// Desc.		:
//=============================================================================
void CWnd_RecipeView::ShowVideo_Overlay(__in INT iChIdx, __inout IplImage* lpVideo, __in DWORD dwWidth, __in DWORD dwHeight)
{
	enOverlayItem enOverlayItem = m_stRecipeInfo.nOverlayItem;

	switch (enOverlayItem)
	{
	case Ovr_OpticalCenter:
		if (SYS_IMAGE_TEST == m_InspectionType)
			m_OverlayProc.Overlay_OpticalCenter(lpVideo, m_stRecipeInfo.stImageQ.stOpticalCenterOpt, m_pCamInfo[iChIdx]->stImageQ.stOpticalCenterData, Ovr_OpticalCenter);
		else if (SYS_FOCUSING == m_InspectionType)
			m_OverlayProc.Overlay_OpticalCenter(lpVideo, m_stRecipeInfo.stFocus.stOpticalCenterOpt, m_pCamInfo[iChIdx]->stFocus.stOpticalCenterData, Ovr_OpticalCenter);
		break;
	case Ovr_ECurrent:
		if (SYS_IMAGE_TEST == m_InspectionType)
			m_OverlayProc.Overlay_Current(lpVideo, m_stRecipeInfo.stImageQ.stECurrentOpt, m_pCamInfo[iChIdx]->stImageQ.stECurrentData);
		else if (SYS_FOCUSING == m_InspectionType)
			m_OverlayProc.Overlay_Current(lpVideo, m_stRecipeInfo.stFocus.stECurrentOpt, m_pCamInfo[iChIdx]->stFocus.stECurrentData);
		break;
	case Ovr_FOV:
		if (SYS_IMAGE_TEST == m_InspectionType)
			m_OverlayProc.Overlay_FOV(lpVideo, m_stRecipeInfo.stImageQ.stFovOpt, m_pCamInfo[iChIdx]->stImageQ.stFovData);
		break;
	case Ovr_SFR:
		if (SYS_IMAGE_TEST == m_InspectionType)
		{
			m_OverlayProc.Overlay_SFR(lpVideo, m_stRecipeInfo.stImageQ.stSFROpt[0], m_pCamInfo[iChIdx]->stImageQ.stSFRData[0]);
			m_OverlayProc.Overlay_SFR_Group(lpVideo, m_stRecipeInfo.stImageQ.stSFROpt[0], m_pCamInfo[iChIdx]->stImageQ.stSFRData[0]);
			m_OverlayProc.Overlay_SFR_Tilt(lpVideo, m_stRecipeInfo.stImageQ.stSFROpt[0], m_pCamInfo[iChIdx]->stImageQ.stSFRData[0]);
		}
		else if (SYS_FOCUSING == m_InspectionType)
		{
			m_OverlayProc.Overlay_SFR(lpVideo, m_stRecipeInfo.stFocus.stSFROpt, m_pCamInfo[iChIdx]->stFocus.stSFRData);
			m_OverlayProc.Overlay_SFR_Group(lpVideo, m_stRecipeInfo.stFocus.stSFROpt, m_pCamInfo[iChIdx]->stFocus.stSFRData);
			m_OverlayProc.Overlay_SFR_Tilt(lpVideo, m_stRecipeInfo.stFocus.stSFROpt, m_pCamInfo[iChIdx]->stFocus.stSFRData);
		}
		break;	
	case Ovr_SFR_2:
		if (SYS_IMAGE_TEST == m_InspectionType)
		{
			m_OverlayProc.Overlay_SFR(lpVideo, m_stRecipeInfo.stImageQ.stSFROpt[1], m_pCamInfo[iChIdx]->stImageQ.stSFRData[1]);
			m_OverlayProc.Overlay_SFR_Group(lpVideo, m_stRecipeInfo.stImageQ.stSFROpt[1], m_pCamInfo[iChIdx]->stImageQ.stSFRData[1]);
			m_OverlayProc.Overlay_SFR_Tilt(lpVideo, m_stRecipeInfo.stImageQ.stSFROpt[1], m_pCamInfo[iChIdx]->stImageQ.stSFRData[1]);;
		}
		else if (SYS_FOCUSING == m_InspectionType)
			m_OverlayProc.Overlay_SFR(lpVideo, m_stRecipeInfo.stFocus.stSFROpt, m_pCamInfo[iChIdx]->stFocus.stSFRData);
		break;
	case Ovr_SFR_3:
		if (SYS_IMAGE_TEST == m_InspectionType)
		{
			m_OverlayProc.Overlay_SFR(lpVideo, m_stRecipeInfo.stImageQ.stSFROpt[2], m_pCamInfo[iChIdx]->stImageQ.stSFRData[2]);
			m_OverlayProc.Overlay_SFR_Group(lpVideo, m_stRecipeInfo.stImageQ.stSFROpt[2], m_pCamInfo[iChIdx]->stImageQ.stSFRData[2]);
			m_OverlayProc.Overlay_SFR_Tilt(lpVideo, m_stRecipeInfo.stImageQ.stSFROpt[2], m_pCamInfo[iChIdx]->stImageQ.stSFRData[2]);
		}
		else if (SYS_FOCUSING == m_InspectionType)
			m_OverlayProc.Overlay_SFR(lpVideo, m_stRecipeInfo.stFocus.stSFROpt, m_pCamInfo[iChIdx]->stFocus.stSFRData);
		break;

	case Ovr_Distortion:
		if (SYS_IMAGE_TEST == m_InspectionType)
			m_OverlayProc.Overlay_Distortion(lpVideo, m_stRecipeInfo.stImageQ.stDistortionOpt, m_pCamInfo[iChIdx]->stImageQ.stDistortionData);
		break;
	case Ovr_Rotate:
		if (SYS_IMAGE_TEST == m_InspectionType)
			m_OverlayProc.Overlay_Rotation(lpVideo, m_stRecipeInfo.stImageQ.stRotateOpt, m_pCamInfo[iChIdx]->stImageQ.stRotateData, Ovr_Rotate);
		else if (SYS_FOCUSING == m_InspectionType)
			m_OverlayProc.Overlay_Rotation(lpVideo, m_stRecipeInfo.stFocus.stRotateOpt, m_pCamInfo[iChIdx]->stFocus.stRotateData, Ovr_Rotate);
		break;
	case Ovr_Particle:
		if (SYS_IMAGE_TEST == m_InspectionType)
			m_OverlayProc.Overlay_Particle(lpVideo, m_stRecipeInfo.stImageQ.stParticleOpt, m_pCamInfo[iChIdx]->stImageQ.stParticleData);
		else if (SYS_FOCUSING == m_InspectionType)
			m_OverlayProc.Overlay_Particle(lpVideo, m_stRecipeInfo.stFocus.stParticleOpt, m_pCamInfo[iChIdx]->stFocus.stParticleData);
		break;
	case Ovr_Particle_SNR:
		if (SYS_IMAGE_TEST == m_InspectionType)
			m_OverlayProc.Overlay_Particle_SNR(lpVideo, m_stRecipeInfo.stImageQ.stDynamicOpt, m_pCamInfo[iChIdx]->stImageQ.stDynamicData);
		break;
	case Ovr_DefectPixel:
		if (SYS_IMAGE_TEST == m_InspectionType)
			m_OverlayProc.Overlay_DefectPixel(lpVideo, m_stRecipeInfo.stImageQ.stDefectPixelOpt, m_pCamInfo[iChIdx]->stImageQ.stDefectPixelData);
		else if (SYS_FOCUSING == m_InspectionType)
			m_OverlayProc.Overlay_DefectPixel(lpVideo, m_stRecipeInfo.stFocus.stDefectPixelOpt, m_pCamInfo[iChIdx]->stFocus.stDefectPixelData);
		break;
	case Ovr_Intensity:
		if (SYS_IMAGE_TEST == m_InspectionType)
			m_OverlayProc.Overlay_Intencity(lpVideo, m_stRecipeInfo.stImageQ.stIntensityOpt, m_pCamInfo[iChIdx]->stImageQ.stIntensityData);
		break;
	case Ovr_SNR_Light:
		if (SYS_IMAGE_TEST == m_InspectionType)
			m_OverlayProc.Overlay_SNR_Light(lpVideo, m_stRecipeInfo.stImageQ.stSNR_LightOpt, m_pCamInfo[iChIdx]->stImageQ.stSNR_LightData);
		break;
	case Ovr_Shading:
		if (SYS_IMAGE_TEST == m_InspectionType)
			m_OverlayProc.Overlay_SNR_Shading(lpVideo, m_stRecipeInfo.stImageQ.stShadingOpt, m_pCamInfo[iChIdx]->stImageQ.stShadingData);
		break;
	case Ovr_ActiveAlign:
		if (SYS_FOCUSING == m_InspectionType)
		{
			m_OverlayProc.Overlay_OpticalCenter(lpVideo, m_stRecipeInfo.stFocus.stOpticalCenterOpt, m_pCamInfo[iChIdx]->stFocus.stOpticalCenterData, Ovr_ActiveAlign);
			m_OverlayProc.Overlay_Rotation(lpVideo, m_stRecipeInfo.stFocus.stRotateOpt, m_pCamInfo[iChIdx]->stFocus.stRotateData, Ovr_ActiveAlign);
			m_OverlayProc.Overlay_SFR(lpVideo, m_stRecipeInfo.stFocus.stSFROpt, m_pCamInfo[iChIdx]->stFocus.stSFRData);
			m_OverlayProc.Overlay_SFR_Tilt(lpVideo, m_stRecipeInfo.stFocus.stSFROpt, m_pCamInfo[iChIdx]->stFocus.stSFRData);
		}
		break;
	case Ovr_ReleaseScrew:
		if (SYS_FOCUSING == m_InspectionType)
		{
			m_OverlayProc.Overlay_ReleaseScrew(lpVideo, m_stRecipeInfo.stFocus.stTorqueOpt, m_pCamInfo[iChIdx]->stFocus.stTorqueData);
		}
		break;
	case Ovr_LockingScrew:
		if (SYS_FOCUSING == m_InspectionType)
		{
			m_OverlayProc.Overlay_LockingScrew(lpVideo, m_stRecipeInfo.stFocus.stTorqueOpt, m_pCamInfo[iChIdx]->stFocus.stTorqueData);
		}
		break;
	case Ovr_HotPixel:
		if (SYS_IMAGE_TEST == m_InspectionType)
			m_OverlayProc.Overlay_HotPixel(lpVideo, m_stRecipeInfo.stImageQ.stHotPixelOpt, m_pCamInfo[iChIdx]->stImageQ.stHotPixelData);
		break;
	case Ovr_FPN:
		if (SYS_IMAGE_TEST == m_InspectionType)
			m_OverlayProc.Overlay_FPN(lpVideo, m_stRecipeInfo.stImageQ.stFPNOpt, m_pCamInfo[iChIdx]->stImageQ.stFPNData);
		break;
	case Ovr_3D_Depth:
		if (SYS_IMAGE_TEST == m_InspectionType)
			m_OverlayProc.Overlay_3D_Depth(lpVideo, m_stRecipeInfo.stImageQ.st3D_DepthOpt, m_pCamInfo[iChIdx]->stImageQ.st3D_DepthData);
		break;
	case Ovr_EEPROM_Verify:
		if (SYS_IMAGE_TEST == m_InspectionType)
			m_OverlayProc.Overlay_EEPRAM(lpVideo, m_stRecipeInfo.stImageQ.stEEPROM_VerifyOpt, m_pCamInfo[iChIdx]->stImageQ.stEEPROM_VerifyData);
		break;
	case Ovr_Temperature:
		if (SYS_IMAGE_TEST == m_InspectionType)
			m_OverlayProc.Overlay_TemperatureSensor(lpVideo, m_stRecipeInfo.stImageQ.stTemperatureSensorOpt, m_pCamInfo[iChIdx]->stImageQ.stTemperatureSensorData);
		break;
	case Ovr_Particle_Entry:
		if (SYS_IMAGE_TEST == m_InspectionType)
			m_OverlayProc.Overlay_Particle_Entry(lpVideo, m_stRecipeInfo.stImageQ.stParticle_EntryOpt, m_pCamInfo[iChIdx]->stImageQ.stParticle_EntryData);
		break;
		
	case Ovr_SFR_G0:
	case Ovr_SFR_G1:
	case Ovr_SFR_G2:
	case Ovr_SFR_G3:
	case Ovr_SFR_G4:
		if (SYS_IMAGE_TEST == m_InspectionType)
			m_OverlayProc.Overlay_SFR_Group(lpVideo, m_stRecipeInfo.stImageQ.stSFROpt[0], m_pCamInfo[iChIdx]->stImageQ.stSFRData[0]);// , m_stRecipeInfo.nOverlayItem - Ovr_SFR_G0);
		else if (SYS_FOCUSING == m_InspectionType)
			m_OverlayProc.Overlay_SFR_Group(lpVideo, m_stRecipeInfo.stFocus.stSFROpt, m_pCamInfo[iChIdx]->stFocus.stSFRData, m_stRecipeInfo.nOverlayItem - Ovr_SFR_G0);
		break;

	case Ovr_SFR_G0_2:
	case Ovr_SFR_G1_2:
	case Ovr_SFR_G2_2:
	case Ovr_SFR_G3_2:
	case Ovr_SFR_G4_2:
		if (SYS_IMAGE_TEST == m_InspectionType)
			m_OverlayProc.Overlay_SFR_Group(lpVideo, m_stRecipeInfo.stImageQ.stSFROpt[1], m_pCamInfo[iChIdx]->stImageQ.stSFRData[1]);// , m_stRecipeInfo.nOverlayItem - Ovr_SFR_G0);
		else if (SYS_FOCUSING == m_InspectionType)
			m_OverlayProc.Overlay_SFR_Group(lpVideo, m_stRecipeInfo.stFocus.stSFROpt, m_pCamInfo[iChIdx]->stFocus.stSFRData, m_stRecipeInfo.nOverlayItem - Ovr_SFR_G0);
		break;

	case Ovr_SFR_G0_3:
	case Ovr_SFR_G1_3:
	case Ovr_SFR_G2_3:
	case Ovr_SFR_G3_3:
	case Ovr_SFR_G4_3:
		if (SYS_IMAGE_TEST == m_InspectionType)
			m_OverlayProc.Overlay_SFR_Group(lpVideo, m_stRecipeInfo.stImageQ.stSFROpt[2], m_pCamInfo[iChIdx]->stImageQ.stSFRData[2]);// , m_stRecipeInfo.nOverlayItem - Ovr_SFR_G0);
		else if (SYS_FOCUSING == m_InspectionType)
			m_OverlayProc.Overlay_SFR_Group(lpVideo, m_stRecipeInfo.stFocus.stSFROpt, m_pCamInfo[iChIdx]->stFocus.stSFRData, m_stRecipeInfo.nOverlayItem - Ovr_SFR_G0);
		break;


	case Ovr_SFR_X1Tilt:
	case Ovr_SFR_X2Tilt:
	case Ovr_SFR_Y1Tilt:
	case Ovr_SFR_Y2Tilt:
		if (SYS_FOCUSING == m_InspectionType)
			m_OverlayProc.Overlay_SFR_Tilt(lpVideo, m_stRecipeInfo.stFocus.stSFROpt, m_pCamInfo[iChIdx]->stFocus.stSFRData);
		else if (SYS_IMAGE_TEST == m_InspectionType)
			m_OverlayProc.Overlay_SFR_Tilt(lpVideo, m_stRecipeInfo.stImageQ.stSFROpt[0], m_pCamInfo[iChIdx]->stImageQ.stSFRData[0]);
		break;

	case Ovr_SFR_X1Tilt_2:
	case Ovr_SFR_X2Tilt_2:
	case Ovr_SFR_Y1Tilt_2:
	case Ovr_SFR_Y2Tilt_2:
		if (SYS_FOCUSING == m_InspectionType)
			m_OverlayProc.Overlay_SFR_Tilt(lpVideo, m_stRecipeInfo.stFocus.stSFROpt, m_pCamInfo[iChIdx]->stFocus.stSFRData);
		else if (SYS_IMAGE_TEST == m_InspectionType)
			m_OverlayProc.Overlay_SFR_Tilt(lpVideo, m_stRecipeInfo.stImageQ.stSFROpt[1], m_pCamInfo[iChIdx]->stImageQ.stSFRData[1]);
		break;

	case Ovr_SFR_X1Tilt_3:
	case Ovr_SFR_X2Tilt_3:
	case Ovr_SFR_Y1Tilt_3:
	case Ovr_SFR_Y2Tilt_3:
		if (SYS_FOCUSING == m_InspectionType)
			m_OverlayProc.Overlay_SFR_Tilt(lpVideo, m_stRecipeInfo.stFocus.stSFROpt, m_pCamInfo[iChIdx]->stFocus.stSFRData);
		else if (SYS_IMAGE_TEST == m_InspectionType)
			m_OverlayProc.Overlay_SFR_Tilt(lpVideo, m_stRecipeInfo.stImageQ.stSFROpt[2], m_pCamInfo[iChIdx]->stImageQ.stSFRData[2]);
		break;

	default:
		break;
	}

	if (0 <= iChIdx)
	{
		m_wnd_ImageLive[iChIdx].Render((LPBYTE)lpVideo->imageData, dwWidth, dwHeight);
	}
}


//=============================================================================
// Method		: ShowVideo
// Access		: public  
// Returns		: void
// Parameter	: __in INT iChIdx
// Parameter	: __in LPBYTE lpVideo
// Parameter	: __in DWORD dwWidth
// Parameter	: __in DWORD dwHeight
// Qualifier	:
// Last Update	: 2018/2/8 - 15:41
// Desc.		:
//=============================================================================
void CWnd_RecipeView::ShowVideo(__in INT iChIdx, __in LPBYTE lpVideo, __in DWORD dwWidth, __in DWORD dwHeight)
{
	if (0 <= iChIdx)
	{
		m_wnd_ImageLive[iChIdx].Render(lpVideo, dwWidth, dwHeight);
	}
}

void CWnd_RecipeView::NoSignal_Ch(__in INT iChIdx)
{
	if (0 <= iChIdx)
	{
		m_wnd_ImageLive[iChIdx].Render_Empty();
	}
}

void CWnd_RecipeView::OnBnClickedRbLiveMode()
{
	ChangeImageMode(ImageMode_LiveCam);
}

void CWnd_RecipeView::OnBnClickedRbStillMode()
{
	ChangeImageMode(ImageMode_StillShotImage);
}

void CWnd_RecipeView::ChangeImageMode(enImageMode eImageMode){

	if (eImageMode == ImageMode_LiveCam)
	{
		m_rb_ImageMode[ImageMode_LiveCam].SetCheck(BST_CHECKED);
		m_rb_ImageMode[ImageMode_StillShotImage].SetCheck(BST_UNCHECKED);
		Stop_ImageView_Mon();
		m_pstImageMode->szImagePath.Empty();
	}
	else{
		m_rb_ImageMode[ImageMode_LiveCam].SetCheck(BST_UNCHECKED);
		m_rb_ImageMode[ImageMode_StillShotImage].SetCheck(BST_CHECKED);

	}
	m_pstImageMode->eImageMode = eImageMode;
	Set_CameraSelect(Para_Left);
}

void CWnd_RecipeView::OnBnClickedImageLoad()
{
	OnPopImageLoad();
}

void CWnd_RecipeView::OnBnClickedImageSave()
{
	OnImageSave();
}


BOOL CWnd_RecipeView::OnPopImageLoad()
{

	CString strFileExt = _T("Image (*.BMP, *.PNG, *.JPG) | *.BMP;*.PNG;*.JPG,*.bmp, *.png, *.jpg | All Files(*.*) |*.*||");
	CFileDialog fileDlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, strFileExt);

	if (IDOK == fileDlg.DoModal())
	{
		Stop_ImageView_Mon();
		Sleep(100);
		CString strFilePath = fileDlg.GetPathName();

		m_strImagePath = strFilePath;
		m_pstImageMode->szImagePath = strFilePath;
		OnImage_LoadDisplay(strFilePath);
		ChangeImageMode(ImageMode_StillShotImage);
		Start_ImageView_Mon();
		return TRUE;
	}

	return FALSE;
}

//=============================================================================
// Method		: OnImageSave
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/9/20 - 15:21
// Desc.		:
//=============================================================================
void CWnd_RecipeView::OnImageSave()
{
	CString strFileExt = _T("Image (*.BMP, *.PNG, *.JPG) | *.BMP;*.PNG;*.JPG,*.bmp, *.png, *.jpg | All Files(*.*) |*.*||");
	CString strFullPath;
	CString strFileTitle;
}

//=============================================================================
// Method		: OnImage_LoadDisplay
// Access		: public  
// Returns		: void
// Parameter	: CString strPath
// Qualifier	:
// Last Update	: 2018/9/20 - 15:21
// Desc.		:
//=============================================================================
void CWnd_RecipeView::OnImage_LoadDisplay(CString strPath)
{
	if (m_LoadImage != NULL)
	{
		cvReleaseImage(&m_LoadImage);
		m_LoadImage = NULL;
	}

	IplImage *testImage = cvLoadImage((CStringA)strPath, CV_LOAD_IMAGE_UNCHANGED);

	if (testImage == NULL || testImage->width < 1 || testImage->height < 1)
	{
		return;
	}

	int iImageBit = testImage->depth * testImage->nChannels;

	switch (iImageBit)
	{
	case 8 :	// 8bit 영상
	{
		// 컬러로 재로드
		IplImage *TempImage = cvLoadImage((CStringA)strPath, CV_LOAD_IMAGE_COLOR);

		m_LoadImage = cvCreateImage(cvSize(TempImage->width, TempImage->height), IPL_DEPTH_8U, 3);
		cvCopy(TempImage, m_LoadImage);

		cvReleaseImage(&TempImage);
	}
		break;
	case 16 :	// 16bit 영상
	case 24 :	// 24bit 영상
	{
		m_LoadImage = cvCreateImage(cvSize(testImage->width, testImage->height), IPL_DEPTH_8U, 3);

		cv::Mat Bit16Mat(testImage->height, testImage->width, CV_16UC1, testImage->imageData);
		Bit16Mat.convertTo(Bit16Mat, CV_8UC1, 0.1, 0);

		cv::Mat rgb8BitMat(m_LoadImage->height, m_LoadImage->width, CV_8UC3, m_LoadImage->imageData);
		cv::cvtColor(Bit16Mat, rgb8BitMat, CV_GRAY2BGR);
	}
		break;
	default:
		break;
	}

	cvReleaseImage(&testImage);
}

//=============================================================================
// Method		: Start_ImageView_Mon
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2018/9/20 - 15:21
// Desc.		:
//=============================================================================
BOOL CWnd_RecipeView::Start_ImageView_Mon()
{
	if (NULL != m_hThr_ImageViewMon)
	{
		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThr_ImageViewMon, &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			AfxMessageBox(_T("Image 모니터링 쓰레드가 동작 중 입니다."), MB_SYSTEMMODAL);
			return FALSE;
		}
	}

	if (NULL != m_hThr_ImageViewMon)
	{
		CloseHandle(m_hThr_ImageViewMon);
		m_hThr_ImageViewMon = NULL;
	}
	m_bFlag_ImageViewMon = TRUE;
	m_hThr_ImageViewMon = HANDLE(_beginthreadex(NULL, 0, Thread_ImageViewMon, this, 0, NULL));

	return TRUE;
}

//=============================================================================
// Method		: Stop_ImageView_Mon
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2018/9/20 - 15:56
// Desc.		:
//=============================================================================
BOOL CWnd_RecipeView::Stop_ImageView_Mon()
{
	m_bFlag_ImageViewMon = FALSE;

	if (NULL != m_hThr_ImageViewMon)
	{
		WaitForSingleObject(m_hThr_ImageViewMon, m_dwImageViewMonCycle);

		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThr_ImageViewMon, &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			TerminateThread(m_hThr_ImageViewMon, dwExitCode);
			WaitForSingleObject(m_hThr_ImageViewMon, WAIT_ABANDONED);
			CloseHandle(m_hThr_ImageViewMon);
			m_hThr_ImageViewMon = NULL;
		}

		NoSignal_Ch(Para_Left);

	}

	return TRUE;
}

//=============================================================================
// Method		: Thread_ImageViewMon
// Access		: public static  
// Returns		: UINT WINAPI
// Parameter	: __in LPVOID lParam
// Qualifier	:
// Last Update	: 2018/9/20 - 15:56
// Desc.		:
//=============================================================================
UINT WINAPI CWnd_RecipeView::Thread_ImageViewMon(__in LPVOID lParam)
{
	ASSERT(NULL != lParam);

	CWnd_RecipeView* pThis = (CWnd_RecipeView*)lParam;

	DWORD dwEvent = 0;

	__try
	{
		while (pThis->m_bFlag_ImageViewMon)
 		{
// 			// 종료 이벤트, 연결해제 이벤트 체크
 			if (NULL != pThis->m_hExternalExitEvent)
 			{
				dwEvent = WaitForSingleObject(pThis->m_hExternalExitEvent, pThis->m_dwImageViewMonCycle);
 
 				switch (dwEvent)
 				{
 				case WAIT_OBJECT_0:	// Exit Program
 					TRACE(_T(" -- 프로그램 종료 m_hExternalExitEvent 이벤트 감지 \n"));
 					pThis->m_bFlag_ImageViewMon = FALSE;
 					break;
 
 				case WAIT_TIMEOUT:
					pThis->OnLoadImageDisplay();

 					break;
 				}
 			}
 			else
 			{
				pThis->OnLoadImageDisplay();

 				Sleep(pThis->m_dwImageViewMonCycle);
 			}
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CTest_PLC_Process::Thread_PLCMon()\n"));
	}

	TRACE(_T("쓰레드 종료 : CTest_PLC_Process::Thread_PLCMon Loop Exit\n"));
	return TRUE;
}

void CWnd_RecipeView::OnLoadImageDisplay()
{
	if (m_LoadImage != NULL)
	{
		IplImage *TestImage = cvCreateImage(cvSize(m_LoadImage->width, m_LoadImage->height), IPL_DEPTH_8U, 3);

		cvCopy(m_LoadImage, TestImage);
		ShowVideo_Overlay(Para_Left, TestImage, TestImage->width, TestImage->height);
		if (m_InspectionType == Sys_Image_Test || m_InspectionType == Sys_Focusing)
		{
			if (*m_bImageCapturemode)
			{
				CString strFile;
				strFile.Format(_T("%s_Pic.png"), *m_pszImageCaptureFile);
				cvSaveImage(CT2A(strFile), TestImage);
				*m_bImageCapturemode = FALSE;
			}
		}
	
		cvReleaseImage(&TestImage);
		
	}
}


//=============================================================================
// Method		: OnChangeOptionPic
// Access		: public  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2018/3/7 - 19:15
// Desc.		:
//=============================================================================
LRESULT CWnd_RecipeView::OnChangeOptionPic(WPARAM wParam, LPARAM lParam)
{
	m_OverlayProc.m_bTestmode = (BOOL)wParam;
		return TRUE;
}

//=============================================================================
// Method		: TestRect_Setting
// Access		: public  
// Returns		: void
// Parameter	: int nTestModel
// Qualifier	:
// Last Update	: 2018/3/7 - 19:15
// Desc.		:
//=============================================================================
void CWnd_RecipeView::TestRect_Setting(int nTestModel)
{
	m_stRecipeInfo.stImageQ.stSNR_LightOpt.st_SNR_LightRect.Release();
	m_stRecipeInfo.stImageQ.stSNR_LightOpt.st_SNR_LightRect.FullRectCreate(SECTION_NUM_X, SECTION_NUM_Y);
	m_stRecipeInfo.stImageQ.stSNR_LightOpt.st_SNR_LightRect.FullRectMakeSection(g_IR_ModelTable[nTestModel].Img_Width, g_IR_ModelTable[nTestModel].Img_Height);
	m_stRecipeInfo.stImageQ.stSNR_LightOpt.st_SNR_LightRect.TestRectCreate();
	m_stRecipeInfo.stImageQ.stSNR_LightOpt.st_SNR_LightRect.TestRectMakeSection();

	m_stRecipeInfo.stImageQ.stShadingOpt.stShading_Rect.Release();
	m_stRecipeInfo.stImageQ.stShadingOpt.stShading_Rect.FullRectCreate(SECTION_NUM_X, SECTION_NUM_Y);
	m_stRecipeInfo.stImageQ.stShadingOpt.stShading_Rect.FullRectMakeSection(g_IR_ModelTable[nTestModel].Img_Width, g_IR_ModelTable[nTestModel].Img_Height);
	m_stRecipeInfo.stImageQ.stShadingOpt.stShading_Rect.TestRectCreate();
	m_stRecipeInfo.stImageQ.stShadingOpt.stShading_Rect.TestRectMakeSection();
}