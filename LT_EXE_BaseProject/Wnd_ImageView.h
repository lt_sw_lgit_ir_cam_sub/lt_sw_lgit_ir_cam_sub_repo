﻿#ifndef Wnd_ImageView_h__
#define Wnd_ImageView_h__

#pragma once

enum enMouse
{
	Mouse_None,
	Mouse_Edit,
};

typedef enum enPicMode
{
	PICMode_LINE,		// 직선
	PICMode_RECTANGLE,	// 직사각형
	PICMode_CIRCLE,		// 원
	PICMode_TXT,		// 글씨
	PICMode_MaxNum,
};

enum enMouseEdit
{
	MouseEdit_Init,
	MouseEdit_Left_WE,
	MouseEdit_Right_WE,
	MouseEdit_Top_NS,
	MouseEdit_Bottom_NS,
	MouseEdit_Left_NWSE,
	MouseEdit_Right_NWSE,
	MouseEdit_Left_NESW,
	MouseEdit_Right_NESW,
	MouseEdit_ALL,
};

//#ifdef USE_VisionInspection
#include "cv.h"
#include "highgui.h"
//#endif
#include "VGStatic.h"
#include "Def_DataStruct.h"
#include "TestManager_Test.h"

// CWnd_ImageView

class CWnd_ImageView : public CWnd
{
	DECLARE_DYNAMIC(CWnd_ImageView)

public:
	CWnd_ImageView();
	virtual ~CWnd_ImageView();
	
	void	OnUpdataResetImage	();
	void	OnUpdataLoadImage	();

	void	SetPtr_RecipeInfor(__in ST_RecipeInfo* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstRecipeInfo = pstRecipeInfo;
	};

	void	SetImagePath (__in CString szImagePath)
	{
		m_strImagePath = szImagePath;
	};

#ifdef USE_VisionInspection
	IplImage*	GetLoadImage ()
	{
		return m_pLoadImage;
	};
#endif

protected:

	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	afx_msg void	OnLButtonDown		(UINT nFlags, CPoint point);
	afx_msg void	OnLButtonUp			(UINT nFlags, CPoint point);
	afx_msg void	OnMouseMove			(UINT nFlags, CPoint point);
	afx_msg BOOL	OnSetCursor			(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg BOOL	OnEraseBkgnd		(CDC* pDC);
	afx_msg void	OnShowWindow		(BOOL bShow, UINT nStatus);

	ST_RecipeInfo*	m_pstRecipeInfo		= NULL;

	CFont			m_font;

	CVGStatic		m_stImage;
	CString			m_strImagePath;

	UINT			m_nWidth			= 640;
	UINT			m_nHeight			= 480;
	UINT			m_nExposure			= 1;

#ifdef USE_VisionInspection
	IplImage		*m_pLoadImage		= NULL;
	IplImage		*m_pOriginImage		= NULL;
	IplImage		*m_pPicImage		= NULL;
#endif
	BITMAPINFO		m_BitmapInfo;

	BOOL			m_bflg_Timer		= FALSE;
	BOOL			m_bflg_MauseMode	= Mouse_None;
	
	int				m_iROInum			= -1;
	UINT			m_nROICntMax		= 0;
	UINT			m_nMouseEdit		= MouseEdit_Init;
	CPoint			m_ptEdit;
	CPoint			m_ptInit;

	//	타이머
	HANDLE			m_hTimerViewCheck	= NULL;
	HANDLE			m_hTimerQueue		= NULL;

	static VOID CALLBACK TimerRoutineViewCheck(__in PVOID lpParam, __in BOOLEAN TimerOrWaitFired);

	void	CreateTimerQueue_Mon	();
	void	DeleteTimerQueue_Mon	();
	void	CreateTimerViewCheck	();
	void	DeleteTimerViewCheck	();

	void	OnImageView				(__in UINT nOverlayIndex);	// m_pstRecipeInfo->TestItemOpt.iPicItem
	void	SetImageExposure		();


	void	SetROIData				(UINT nROI, CRect rtROI);
	CRect	GetROIData				(UINT nROI);

#ifdef USE_VisionInspection
	void	PicCenterPoint			(IplImage* lpImage);
	void	PicDistortion			(IplImage* lpImage);
	void	PicDynamic				(IplImage* lpImage);
	void	PicRotate				(IplImage* lpImage);
	void	PicSFR					(IplImage* lpImage);
	void	PicSNR_BW				(IplImage* lpImage);
	void	PicSNR_IQ				(IplImage* lpImage);
	void	PicTilt					(IplImage* lpImage);
	void	PicFov					(IplImage* lpImage);
	void	PicDefectPixel				(IplImage* lpImage);
	void	PicParticle				(IplImage* lpImage);
#endif
	void	OnDisplayPIC			(IplImage* lpImage, enPicMode enMode, int iLeft, int iTop, int iRight, int iBottom, int iRed, int iGreen, int iBlue, int iTickness, float fFontSize = 1, CString strText = _T(""));


};


#endif // Wnd_ImageView_h__
