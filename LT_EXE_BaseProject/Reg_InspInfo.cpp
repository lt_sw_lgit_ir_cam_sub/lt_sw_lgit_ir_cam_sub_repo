﻿//*****************************************************************************
// Filename	: 	Reg_InspInfo.cpp
// Created	:	2016/3/31 - 16:33
// Modified	:	2016/3/31 - 16:33
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "Reg_InspInfo.h"

#define  DEF_INI_FILE _T("initmodel.ini")

CReg_InspInfo::CReg_InspInfo()
{
	m_strRegPath.Format(_T("%s\\InspInfo"), REG_PATH_APP_BASE);
}

CReg_InspInfo::CReg_InspInfo(__in LPCTSTR lpszRegPath)
{
	m_strRegPath = lpszRegPath;
}

CReg_InspInfo::~CReg_InspInfo()
{
}

//=============================================================================
// Method		: SaveSelectedModel
// Access		: public  
// Returns		: BOOL
// Parameter	: __in CString szRecipeFile
// Parameter	: __in CString szRecipe
// Qualifier	:
// Last Update	: 2016/11/24 - 15:39
// Desc.		:
//=============================================================================
 BOOL CReg_InspInfo::SaveSelectedModel(__in CString szRecipeFile, __in CString szRecipe)
 {
 	CString		strValue;
 	CString		strRegPath;
 	strRegPath.Format(_T("%s\\InspInfo"), REG_PATH_APP_BASE);
 
 	if (!m_Reg.VerifyKey(HKEY_CURRENT_USER, strRegPath))
 	{
 		m_Reg.CreateKey(HKEY_CURRENT_USER, strRegPath);
 	}
 
 	if (m_Reg.Open(HKEY_CURRENT_USER, strRegPath))
 	{
 		strValue = szRecipeFile;
 		m_Reg.WriteString(_T("SelectedModelFile"), strValue.GetBuffer());
 
 		strValue = szRecipe;
 		m_Reg.WriteString(_T("Set_Model"), strValue.GetBuffer());
 	}
 	else
 	{
 		return FALSE;
 	}
 
 	m_Reg.Close();
 
 	return TRUE;
 }

 BOOL CReg_InspInfo::SaveSelectedModel(__in enModelType nModel, __in CString szRecipeFile, __in CString szRecipe)
 {
	 CString		strValue;
	 CString		strRegPath;
	 strRegPath.Format(_T("%s\\InspInfo\\%s"), REG_PATH_APP_BASE, g_szModelName[nModel]);

	 if (!m_Reg.VerifyKey(HKEY_CURRENT_USER, strRegPath))
	 {
		 m_Reg.CreateKey(HKEY_CURRENT_USER, strRegPath);
	 }

	 if (m_Reg.Open(HKEY_CURRENT_USER, strRegPath))
	 {
		 strValue = szRecipeFile;
		 m_Reg.WriteString(_T("SelectedModelFile"), strValue.GetBuffer());

		 strValue = szRecipe;
		 m_Reg.WriteString(_T("Set_Model"), strValue.GetBuffer());
	 }
	 else
	 {
		 return FALSE;
	 }

	 m_Reg.Close();

	 return TRUE;
 }

//=============================================================================
// Method		: LoadSelectedModel
// Access		: public  
// Returns		: BOOL
// Parameter	: __out CString & szRecipeFile
// Parameter	: __out CString & szRecipe
// Qualifier	:
// Last Update	: 2016/11/24 - 15:38
// Desc.		:
//=============================================================================
 BOOL CReg_InspInfo::LoadSelectedModel(__out CString& szRecipeFile, __out CString& szRecipe)
 {
 	DWORD		dwValue = 0;
 	CString		strValue;
 	CString		strRegPath;
 	strRegPath.Format(_T("%s\\InspInfo"), REG_PATH_APP_BASE);
 
 	if (m_Reg.Open(HKEY_CURRENT_USER, strRegPath))
 	{
 		if (m_Reg.ReadString(_T("SelectedModelFile"), strValue))
 			szRecipeFile = strValue;
 		else
 			szRecipeFile = _T("Default");
 
 		if (m_Reg.ReadString(_T("Set_Model"), strValue))
 			szRecipe = strValue;
 		else
 			szRecipe = _T("Default");//Default
 	}
 	else
 	{
 		return FALSE;
 	}
 
 	m_Reg.Close();
 
 	return TRUE;
 }

 BOOL CReg_InspInfo::LoadSelectedModel(__in enModelType nModel, __out CString& szRecipeFile, __out CString& szRecipe)
 {
	 DWORD		dwValue = 0;
	 CString	strValue;
	 CString	strRegPath;
	 strRegPath.Format(_T("%s\\InspInfo\\%s"), REG_PATH_APP_BASE, g_szModelName[nModel]);

	 if (m_Reg.Open(HKEY_CURRENT_USER, strRegPath))
	 {
		 if (m_Reg.ReadString(_T("SelectedModelFile"), strValue))
			 szRecipeFile = strValue;
		 else
			 szRecipeFile = _T("Default");

		 if (m_Reg.ReadString(_T("Set_Model"), strValue))
			 szRecipe = strValue;
		 else
			 szRecipe = _T("Default");//Default
	 }
	 else
	 {
		 return FALSE;
	 }

	 m_Reg.Close();

	 return TRUE;
 }

//=============================================================================
// Method		: SaveYield
// Access		: public  
// Returns		: BOOL
// Parameter	: __in const ST_Yield * pstYield
// Qualifier	:
// Last Update	: 2016/6/1 - 21:08
// Desc.		:
//=============================================================================
BOOL CReg_InspInfo::SaveYield(__in const ST_Yield* pstYield)
{
	CString		strValue;
	DWORD		dwValue;
	CString		strRegPath;
	strRegPath.Format(_T("%s\\InspInfo"), REG_PATH_APP_BASE);

	if (!m_Reg.VerifyKey(HKEY_CURRENT_USER, strRegPath))
	{
		m_Reg.CreateKey(HKEY_CURRENT_USER, strRegPath);
	}

	if (m_Reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		dwValue = pstYield->dwPass;
		m_Reg.WriteDWORD(_T("Pass"), dwValue);


		dwValue = pstYield->dwFail;
		m_Reg.WriteDWORD(_T("Fail"), dwValue);
	}
	else
	{
		return FALSE;
	}

	m_Reg.Close();

	return TRUE;
}

//=============================================================================
// Method		: LoadYield
// Access		: public  
// Returns		: BOOL
// Parameter	: __out ST_Yield & stYield
// Qualifier	:
// Last Update	: 2017/9/29 - 16:43
// Desc.		:
//=============================================================================
BOOL CReg_InspInfo::LoadYield(__out ST_Yield& stYield)
{
	DWORD		dwValue = 0;
	CString		strValue;
	CString		strRegPath;
	strRegPath.Format(_T("%s\\InspInfo"), REG_PATH_APP_BASE);

	if (m_Reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		if (m_Reg.ReadDWORD(_T("Pass"), dwValue))
			stYield.dwPass = dwValue;
		else
			stYield.dwPass = 0;
		
		if (m_Reg.ReadDWORD(_T("Fail"), dwValue))
			stYield.dwFail = dwValue;
		else
			stYield.dwFail = 0;
	}
	else
	{
		return FALSE;
	}

	m_Reg.Close();

	return TRUE;
}

//=============================================================================
// Method		: SavePassword
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nIndex
// Qualifier	:
// Last Update	: 2016/6/25 - 15:58
// Desc.		:
//=============================================================================
BOOL CReg_InspInfo::SavePassword(__in UINT nIndex, __in CString szPassword)
{
	CRegistry	reg;
	if (reg.Open(HKEY_CURRENT_USER, REG_PATH_OPTION_BASE))
	{
		//Password
		if (0 == nIndex)
			reg.WriteString(_T("Password"), szPassword);
		else
			reg.WriteString(_T("Password_2"), szPassword);

		reg.Close();

		return TRUE;
	}

	return FALSE;
}

//=============================================================================
// Method		: LoadPassword
// Access		: public  
// Returns		: CString
// Parameter	: __in UINT nIndex
// Qualifier	:
// Last Update	: 2016/6/25 - 16:03
// Desc.		:
//=============================================================================
CString CReg_InspInfo::LoadPassword(__in UINT nIndex)
{
	CRegistry	reg;
	CString		strValue;
	if (reg.Open(HKEY_CURRENT_USER, REG_PATH_OPTION_BASE))
	{
		if (0 == nIndex)
			reg.ReadString(_T("Password"), strValue);
		else
			reg.ReadString(_T("Password_2"), strValue);

		reg.Close();
	}

	return strValue;
}

//=============================================================================
// Method		: SavePogoInfo
// Access		: public  
// Returns		: BOOL
// Parameter	: __in const ST_ConsumablesInfo * pstConsumInfo
// Qualifier	:
// Last Update	: 2016/10/31 - 22:44
// Desc.		:
//=============================================================================
BOOL CReg_InspInfo::SavePogoInfo(__in const ST_ConsumablesInfo* pstConsumInfo)
{
	CString		strValue;
	DWORD		dwValue;
	CString		strRegPath;
	strRegPath.Format(_T("%s\\InspInfo"), REG_PATH_APP_BASE);

	if (!m_Reg.VerifyKey(HKEY_CURRENT_USER, strRegPath))
	{
		m_Reg.CreateKey(HKEY_CURRENT_USER, strRegPath);
	}

	CString szKey;
	if (m_Reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		for (UINT nIdx = 0; nIdx < MAX_SITE_CNT; nIdx++)
		{
			szKey.Format(_T("MaxCount_%d"), nIdx + 1);
			dwValue = pstConsumInfo->Item[nIdx].dwCount_Max;
			m_Reg.WriteDWORD(szKey, dwValue);

			szKey.Format(_T("Count_%d"), nIdx + 1);
			dwValue = pstConsumInfo->Item[nIdx].dwCount;
			m_Reg.WriteDWORD(szKey, dwValue);
		}
	}
	else
	{
		return FALSE;
	}

	m_Reg.Close();

	return TRUE;
}

//=============================================================================
// Method		: LoadPogoInfo
// Access		: public  
// Returns		: BOOL
// Parameter	: __out ST_ConsumablesInfo & stConsumInfo
// Qualifier	:
// Last Update	: 2016/10/31 - 22:44
// Desc.		:
//=============================================================================
BOOL CReg_InspInfo::LoadPogoInfo(__out ST_ConsumablesInfo& stConsumInfo)
{
	DWORD		dwValue = 0;
	CString		strValue;
	CString		strRegPath;
	strRegPath.Format(_T("%s\\InspInfo"), REG_PATH_APP_BASE);

	CString szKey;
	if (m_Reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		for (UINT nIdx = 0; nIdx < MAX_SITE_CNT; nIdx++)
		{
			szKey.Format(_T("MaxCount_%d"), nIdx + 1);
			if (m_Reg.ReadDWORD(szKey, dwValue))
				stConsumInfo.Item[nIdx].dwCount_Max = dwValue;

			szKey.Format(_T("Count_%d"), nIdx + 1);
			if (m_Reg.ReadDWORD(szKey, dwValue))
				stConsumInfo.Item[nIdx].dwCount = dwValue;
		}
	}
	else
	{
		return FALSE;
	}

	m_Reg.Close();

	return TRUE;
}

//=============================================================================
// Method		: SaveSelectedCam
// Access		: public  
// Returns		: BOOL
// Parameter	: __in DWORD dwSelection
// Qualifier	:
// Last Update	: 2017/1/24 - 11:23
// Desc.		:
//=============================================================================
BOOL CReg_InspInfo::SaveSelectedCam(__in DWORD dwSelection)
{
	CRegistry	reg;
	CString		strRegPath;
	strRegPath.Format(_T("%s\\InspInfo"), REG_PATH_APP_BASE);

	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		reg.WriteDWORD(_T("SelectedCam"), dwSelection);

		reg.Close();

		return TRUE;
	}

	return FALSE;
}

//=============================================================================
// Method		: LoadSelectedCam
// Access		: public  
// Returns		: BOOL
// Parameter	: __out DWORD & dwSelection
// Qualifier	:
// Last Update	: 2017/1/24 - 11:23
// Desc.		:
//=============================================================================
BOOL CReg_InspInfo::LoadSelectedCam(__out DWORD& dwSelection)
{
	DWORD		dwValue = 0;
	CString		strValue;
	CString		strRegPath;
	strRegPath.Format(_T("%s\\InspInfo"), REG_PATH_APP_BASE);

	if (m_Reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		if (m_Reg.ReadDWORD(_T("SelectedCam"), dwValue))
			dwSelection = dwValue;
		else
			dwSelection = 0xFFFFFFFF;
	}
	else
	{
		return FALSE;
	}

	m_Reg.Close();

	return TRUE;
}

//=============================================================================
// Method		: SaveSelectedMotor
// Access		: public  
// Returns		: BOOL
// Parameter	: __in const CString szMotorFile
// Qualifier	:
// Last Update	: 2017/8/12 - 18:24
// Desc.		:
//=============================================================================
BOOL CReg_InspInfo::SaveSelectedMotor(__in const CString szMotorFile)
{
	CString		strValue;
	CString		strRegPath;
	strRegPath.Format(_T("%s\\InspInfo"), REG_PATH_APP_BASE);

	if (!m_Reg.VerifyKey(HKEY_CURRENT_USER, strRegPath))
	{
		m_Reg.CreateKey(HKEY_CURRENT_USER, strRegPath);
	}

	if (m_Reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		strValue = szMotorFile;
		m_Reg.WriteString(_T("SelectedMotorFile"), strValue.GetBuffer());
	}
	else
	{
		return FALSE;
	}

	m_Reg.Close();
	return TRUE;
}

//=============================================================================
// Method		: LoadSelectedMotor
// Access		: public  
// Returns		: BOOL
// Parameter	: __out CString & szMotorFile
// Qualifier	:
// Last Update	: 2017/8/12 - 18:25
// Desc.		:
//=============================================================================
BOOL CReg_InspInfo::LoadSelectedMotor(__out CString& szMotorFile)
{
	CString		strValue;
	CString		strRegPath;

	strRegPath.Format(_T("%s\\InspInfo"), REG_PATH_APP_BASE);

	if (m_Reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		if (m_Reg.ReadString(_T("SelectedMotorFile"), strValue))
			szMotorFile = strValue;
		else
			szMotorFile = _T("Default");
	}
	else
	{
		return FALSE;
	}

	m_Reg.Close();

	return TRUE;
}

//=============================================================================
// Method		: SaveMaintenance
// Access		: public  
// Returns		: BOOL
// Parameter	: __in CString szMaintenanceFile
// Qualifier	:
// Last Update	: 2017/9/29 - 16:45
// Desc.		:
//=============================================================================
BOOL CReg_InspInfo::SaveSelectMaintenance(__in CString szMaintenanceFile)
{
	CString		strValue;
	CString		strRegPath;

	strRegPath.Format(_T("%s\\InspInfo"), REG_PATH_APP_BASE);

	if (!m_Reg.VerifyKey(HKEY_CURRENT_USER, strRegPath))
	{
		m_Reg.CreateKey(HKEY_CURRENT_USER, strRegPath);
	}

	if (m_Reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		strValue = szMaintenanceFile;
		m_Reg.WriteString(_T("SelectedMaintenanceFile"), strValue.GetBuffer());
	}
	else
	{
		return FALSE;
	}

	m_Reg.Close();

	return TRUE;
}

//=============================================================================
// Method		: LoadMaintenance
// Access		: public  
// Returns		: BOOL
// Parameter	: __out CString & szMaintenanceFile
// Qualifier	:
// Last Update	: 2017/9/29 - 16:46
// Desc.		:
//=============================================================================
BOOL CReg_InspInfo::LoadSelectMaintenance(__out CString& szMaintenanceFile)
{
	CString		strValue;
	CString		strRegPath;

	strRegPath.Format(_T("%s\\InspInfo"), REG_PATH_APP_BASE);

	if (m_Reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		if (m_Reg.ReadString(_T("SelectedMaintenanceFile"), strValue))
			szMaintenanceFile = strValue;
		else
			szMaintenanceFile = _T("Default");
	}
	else
	{
		return FALSE;
	}

	m_Reg.Close();

	return TRUE;
}
