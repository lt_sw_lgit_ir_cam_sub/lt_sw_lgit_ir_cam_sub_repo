//*****************************************************************************
// Filename	: 	Wnd_Torque.h
// Created	:	2017/9/18 - 9:49
// Modified	:	2017/9/18 - 9:49
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Torque_h__
#define Wnd_Torque_h__

#pragma once

#include "VGStatic.h"

//-----------------------------------------------------------------------------
// CWnd_Torque
//-----------------------------------------------------------------------------
class CWnd_Torque : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Torque)

public:
	CWnd_Torque();
	virtual ~CWnd_Torque();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate					(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize						(UINT nType, int cx, int cy);

	enum enTorqueItem
	{
		TRI_Y_Torque_Left,	//  Left
		TRI_Y_Torque_Right,	//  Right
		TRI_Y_MaxEnum,
	};

	enum enTorqueHeader
	{
		TRI_X_ItemName,		// ������ ��
		TRI_X_TorqueA,		//
		TRI_X_TorqueB,		//
		TRI_X_TorqueC,		//
		TRI_X_TorqueD,		//	
		TRI_X_MaxEnum,
	};

	CVGStatic		m_st_Caption[TRI_X_MaxEnum];
	CVGStatic		m_st_Cell[TRI_Y_MaxEnum][TRI_X_MaxEnum];

	int				m_iTotalWidthRate		= 0;
	int				m_iCtrlWidth[TRI_X_MaxEnum];

	UINT			m_nItemCount			= TRI_Y_MaxEnum;

public:

	void		Set_UseResetButton		(__in BOOL bUse);

	void		ResetAllItemData		();

	void		SetItemData				(__in UINT nItemIndex, __in UINT nSpec, __in UINT nUsage);

	void		SetItemCount			(__in UINT nCount);
};

#endif // Wnd_Torque_h__

