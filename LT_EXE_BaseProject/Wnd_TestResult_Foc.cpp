﻿// Wnd_TestResult_Foc.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "Wnd_TestResult_Foc.h"

// CWnd_TestResult_Foc
typedef enum TestResult_ImgID
{
	IDC_BTN_ITEM  = 1001,
	IDC_CMB_ITEM  = 2001,
	IDC_EDT_ITEM  = 3001,
	IDC_LIST_ITEM = 4001,
};

IMPLEMENT_DYNAMIC(CWnd_TestResult_Foc, CWnd)

CWnd_TestResult_Foc::CWnd_TestResult_Foc()
{
	m_InspectionType = Sys_Focusing;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_TestResult_Foc::~CWnd_TestResult_Foc()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_TestResult_Foc, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
	ON_CBN_SELENDOK(IDC_CMB_ITEM, OnLbnSelChangeTest)
END_MESSAGE_MAP()

// CWnd_TestResult_Foc 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_TestResult_Foc::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < STI_TR_Foc_MAX; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdex].Create(g_szTestResult_Foc_Static[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	m_st_default.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_default.SetColorStyle(CVGStatic::ColorStyle_White);
	m_st_default.SetFont_Gdip(L"Arial", 9.0F);
	m_st_default.Create(_T("TEST RESULT"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	UINT	nWndID = IDC_LIST_ITEM;
	m_tc_Option.Create(CMFCTabCtrl::STYLE_3D_SCROLLED, rectDummy, this, nWndID++, CMFCTabCtrl::LOCATION_BOTTOM);

	m_Wnd_CurrentData.SetOwner(GetOwner());
	m_Wnd_CurrentData.Create(NULL, _T("Current"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);

	m_Wnd_DefectPixelData.SetOwner(GetOwner());
	m_Wnd_DefectPixelData.Create(NULL, _T("DefectPixel"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	
	m_Wnd_OpticalData.SetOwner(GetOwner());
	m_Wnd_OpticalData.Create(NULL, _T("Optical"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	
	m_Wnd_RotateData.SetOwner(GetOwner());
	m_Wnd_RotateData.Create(NULL, _T("Rotate"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	
	m_Wnd_SFRData.SetOwner(GetOwner());
	m_Wnd_SFRData.Create(NULL, _T("SFR"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	
	m_Wnd_ParticleData.SetOwner(GetOwner());
	m_Wnd_ParticleData.Create(NULL, _T("Particle"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);

	m_Wnd_TorqueData.SetOwner(GetOwner());
	m_Wnd_TorqueData.Create(NULL, _T("Torque"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);

	m_Wnd_ActiveAlignData.SetOwner(GetOwner());
	m_Wnd_ActiveAlignData.Create(NULL, _T("ActiveAlign"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	
	m_cb_TestItem.Create(dwStyle | CBS_DROPDOWNLIST | WS_VSCROLL, rectDummy, this, IDC_CMB_ITEM);
	m_cb_TestItem.SetFont(&m_font);

	m_cb_TestItem.AddString(_T("Current"));
	m_cb_TestItem.AddString(_T("OpticalCenter"));
	m_cb_TestItem.AddString(_T("SFR"));
	m_cb_TestItem.AddString(_T("Rotate"));
	m_cb_TestItem.AddString(_T("DefectPixel"));
	m_cb_TestItem.AddString(_T("Stain"));
	m_cb_TestItem.AddString(_T("Torque"));
	m_cb_TestItem.AddString(_T("ActiveAlign"));

	m_iSelectTest[TR_Foc_ECurrent]		= TI_Foc_Fn_ECurrent; //들어온 순서대로 ID Input;
	m_iSelectTest[TR_Foc_OpticalCenter] = TI_Foc_Fn_OpticalCenter; //들어온 순서대로 ID Input;
	m_iSelectTest[TR_Foc_SFR]			= TI_Foc_Fn_SFR; //들어온 순서대로 ID Input;
	m_iSelectTest[TR_Foc_Rotation]		= TI_Foc_Fn_Rotation; //들어온 순서대로 ID Input;
	m_iSelectTest[TR_Foc_DefectPixel]	= TI_Foc_Fn_DefectPixel; //들어온 순서대로 ID Input;
	m_iSelectTest[TR_Foc_Particle]		= TI_Foc_Fn_Stain; //들어온 순서대로 ID Input;
	m_iSelectTest[TR_Foc_Torque]		= TI_Foc_Re_TorqueCheck; //들어온 순서대로 ID Input;
	m_iSelectTest[TR_Foc_ActiveAlign]	= TI_Foc_Fn_ActiveAlign; //들어온 순서대로 ID Input;

	m_Wnd_CurrentData.ShowWindow(SW_HIDE);
	m_Wnd_DefectPixelData.ShowWindow(SW_HIDE);
	m_Wnd_OpticalData.ShowWindow(SW_HIDE);
	m_Wnd_RotateData.ShowWindow(SW_HIDE);
	m_Wnd_SFRData.ShowWindow(SW_HIDE);
	m_Wnd_ParticleData.ShowWindow(SW_HIDE);
	m_Wnd_TorqueData.ShowWindow(SW_HIDE);
	m_Wnd_ActiveAlignData.ShowWindow(SW_HIDE);

	SelectNum(0);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iMargin  = 10;
	int iSpacing = 5;

	int iLeft	 = 0;
	int iTop	 = 0;
	int iWidth   = cx;
	int iHeight  = cy;
	
	int iStHeight = 25;

	m_st_Item[STI_TR_Foc_Title].MoveWindow(iLeft, iTop, iWidth, iStHeight);

	iTop += iStHeight + 2;
	m_cb_TestItem.MoveWindow(iLeft, iTop, iWidth, iStHeight);
	
	iTop += iStHeight + 2;
	m_Wnd_CurrentData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_DefectPixelData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_OpticalData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_RotateData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_SFRData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_ParticleData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_TorqueData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_ActiveAlignData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_TestResult_Foc::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: SetShowWindowResult
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/21 - 10:59
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc::SetShowWindowResult(int iItem)
{
	switch (m_InspectionType)
	{
	case Sys_Focusing:
		break;
	case Sys_2D_Cal:
		break;
	case Sys_Image_Test:
		break;
	case Sys_Stereo_Cal:
		break;
	case Sys_3D_Cal:
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: MoveWindow_Result
// Access		: protected  
// Returns		: void
// Parameter	: int x
// Parameter	: int y
// Parameter	: int nWidth
// Parameter	: int nHeight
// Qualifier	:
// Last Update	: 2017/10/21 - 11:16
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc::MoveWindow_Result(int x, int y, int nWidth, int nHeight)
{
	int iHeaderH = 40;
	int iListH = iHeaderH + 3 * 20;
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (SW_HIDE == bShow)
	{
		SetShowWindowResult(-1);
	}
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc::OnRangeBtnCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_BTN_ITEM;
}

//=============================================================================
// Method		: SetTestItemResult
// Access		: public  
// Returns		: void
// Parameter	: __in  int iTestItem
// Qualifier	:
// Last Update	: 2017/10/21 - 10:45
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc::SetTestItem(__in int iTestItem)
{
	SetShowWindowResult(iTestItem);
}

//=============================================================================
// Method		: SetUpdateResult
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_RecipeInfo_Base * pstRecipeInfo
// Parameter	: __in int iTestItem
// Qualifier	:
// Last Update	: 2017/10/21 - 12:44
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc::SetUpdateResult(__in const ST_RecipeInfo_Base* pstRecipeInfo, __in int iTestItem)
{
	if (pstRecipeInfo == NULL)
		return;

	switch (m_InspectionType)
	{
	case Sys_Focusing:
		break;
	case Sys_2D_Cal:
		break;
	case Sys_Image_Test:
		break;
	case Sys_Stereo_Cal:
		break;
	case Sys_3D_Cal:
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: SetUpdateClear
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/21 - 14:23
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc::SetUpdateClear()
{
}
//=============================================================================
// Method		: AllDataReset
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/19 - 10:27
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc::AllDataReset()
{
	m_Wnd_CurrentData.DataReset();
	m_Wnd_OpticalData.DataReset();
	m_Wnd_SFRData.DataReset();
	m_Wnd_RotateData.DataReset();
	m_Wnd_ParticleData.DataReset();
	m_Wnd_DefectPixelData.DataReset();
	m_Wnd_ActiveAlignData.DataReset();
}
//=============================================================================
// Method		: SetUIData_Reset
// Access		: public  
// Returns		: void
// Parameter	: int nIdx
// Qualifier	:
// Last Update	: 2018/3/9 - 10:12
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc::SetUIData_Reset(int nIdx)
{
	switch (nIdx)
	{
	case TI_Foc_Fn_ECurrent:
		m_Wnd_CurrentData.DataReset();
		break;
	
	case TI_Foc_Fn_OpticalCenter:
		m_Wnd_OpticalData.DataReset();
		break;

	case TI_Foc_Fn_SFR:
		m_Wnd_SFRData.DataReset();
		break;

	case TI_Foc_Fn_Rotation:
		m_Wnd_RotateData.DataReset();
		break;
	
	case TI_Foc_Fn_Stain:
		m_Wnd_ParticleData.DataReset();
		break;

	case TI_Foc_Fn_DefectPixel:
		m_Wnd_DefectPixelData.DataReset();
		break;

	case TI_Foc_Re_TorqueCheck:
		m_Wnd_TorqueData.DataReset();
		break;

	case TI_Foc_Fn_ActiveAlign:
		m_Wnd_ActiveAlignData.DataReset();
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: SetUIData
// Access		: public  
// Returns		: void
// Parameter	: int nIdx
// Parameter	: LPVOID pParam
// Qualifier	:
// Last Update	: 2018/3/9 - 10:13
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc::SetUIData(int nIdx, LPVOID pParam)
{
	if (pParam == NULL)
		return;

	for (int iItem = 0; iItem < m_cb_TestItem.GetCount(); iItem++)
	{
		if (m_iSelectTest[iItem] == nIdx)
		{
			SelectNum(iItem);
			break;
		}
	}

	switch (nIdx)
	{
	case TI_Foc_Fn_ECurrent:
	{
		ST_ECurrent_Data *pData = (ST_ECurrent_Data *)pParam;
		m_Wnd_CurrentData.DataDisplay(*pData);
	}
		break;
	case TI_Foc_Fn_OpticalCenter:
	{
		ST_OpticalCenter_Data *pData = (ST_OpticalCenter_Data *)pParam;
		m_Wnd_OpticalData.DataDisplay(*pData);
	}
		break;

	case TI_Foc_Fn_SFR:
	{
		ST_SFR_Data *pData = (ST_SFR_Data *)pParam;
		m_Wnd_SFRData.DataDisplay(*pData);
	}
		break;
	
	case TI_Foc_Fn_Rotation:
	{
		ST_Rotate_Data *pData = (ST_Rotate_Data *)pParam;
		m_Wnd_RotateData.DataDisplay(*pData);
	}
		break;

	case TI_Foc_Fn_Stain:
	{
		ST_Particle_Data *pData = (ST_Particle_Data *)pParam;
		m_Wnd_ParticleData.DataDisplay(*pData);
	}
		break;
	
	case TI_Foc_Fn_DefectPixel:
	{
		ST_DefectPixel_Data *pData = (ST_DefectPixel_Data *)pParam;
		m_Wnd_DefectPixelData.DataDisplay(*pData);
	}
		break;

	case TI_Foc_Re_TorqueCheck:
	{
		ST_Torque_Data *pData = (ST_Torque_Data *)pParam;
		m_Wnd_TorqueData.DataDisplay(*pData);
	}

	break;

	case TI_Foc_Fn_ActiveAlign:
	{
		ST_ActiveAlign_Data *pData = (ST_ActiveAlign_Data *)pParam;
		m_Wnd_ActiveAlignData.DataDisplay(*pData);
	}
		break;

	case TI_Foc_Motion_ReleaseScrew:
	{
	   ST_Torque_Data *pData = (ST_Torque_Data *)pParam;
	   m_Wnd_TorqueData.DataDisplay(*pData);
	}
		break;

	case TI_Foc_Motion_LockingScrew:
	{
		ST_Torque_Data *pData = (ST_Torque_Data *)pParam;
		m_Wnd_TorqueData.DataDisplay(*pData);
	}
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: SetClearTab
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/9 - 10:14
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc::SetClearTab()
{
}

//=============================================================================
// Method		: OnLbnSelChangeTest
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/9 - 10:14
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc::OnLbnSelChangeTest()
{
	m_Wnd_CurrentData.ShowWindow(SW_HIDE);
	m_Wnd_DefectPixelData.ShowWindow(SW_HIDE);
	m_Wnd_OpticalData.ShowWindow(SW_HIDE);
	m_Wnd_RotateData.ShowWindow(SW_HIDE);
	m_Wnd_SFRData.ShowWindow(SW_HIDE);
	m_Wnd_ParticleData.ShowWindow(SW_HIDE);
	m_Wnd_TorqueData.ShowWindow(SW_HIDE);
	m_Wnd_ActiveAlignData.ShowWindow(SW_HIDE);

	switch (m_iSelectTest[m_cb_TestItem.GetCurSel()])
	{
	case TI_Foc_Fn_ECurrent:
		m_Wnd_CurrentData.ShowWindow(SW_SHOW);
		break;

	case TI_Foc_Fn_OpticalCenter:
		m_Wnd_OpticalData.ShowWindow(SW_SHOW);
		break;

	case TI_Foc_Fn_SFR:
		m_Wnd_SFRData.ShowWindow(SW_SHOW);
		break;

	case TI_Foc_Fn_Rotation:
		m_Wnd_RotateData.ShowWindow(SW_SHOW);
		break;

	case TI_Foc_Fn_Stain:
		m_Wnd_ParticleData.ShowWindow(SW_SHOW);
		break;

	case TI_Foc_Fn_DefectPixel:
		m_Wnd_DefectPixelData.ShowWindow(SW_SHOW);
		break;

	case TI_Foc_Re_TorqueCheck:
		m_Wnd_TorqueData.ShowWindow(SW_SHOW);
		break;

	case TI_Foc_Fn_ActiveAlign:
		m_Wnd_ActiveAlignData.ShowWindow(SW_SHOW);
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: SelectNum
// Access		: public  
// Returns		: void
// Parameter	: UINT nSelect
// Qualifier	:
// Last Update	: 2018/3/9 - 10:16
// Desc.		:
//=============================================================================
void CWnd_TestResult_Foc::SelectNum(UINT nSelect)
{
	m_cb_TestItem.SetCurSel(nSelect);
	OnLbnSelChangeTest();
}