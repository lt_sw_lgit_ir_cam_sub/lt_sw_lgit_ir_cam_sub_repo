﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_LightCtrl.cpp
// Created	:	2016/10/31 - 21:02
// Modified	:	2016/10/31 - 21:02
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************

#include "stdafx.h"
#include "Wnd_Cfg_LightCtrl.h"
#include "Reg_InspInfo.h"


typedef enum Light_ID
{
	IDC_CB_FILE	= 1000,
	IDC_BN_NEW	= 1100,
	IDC_BN_SAVE = 1101,
	IDC_LIST_LIGHT = 2000,
};

IMPLEMENT_DYNAMIC(CWnd_Cfg_LightCtrl, CWnd_BaseView)

CWnd_Cfg_LightCtrl::CWnd_Cfg_LightCtrl()
{
	m_pstDevice = NULL;
	m_pstMaintenanceInfo = NULL;
	VERIFY(m_font.CreateFont(
		28,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_LightCtrl::~CWnd_Cfg_LightCtrl()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_LightCtrl, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_CBN_SELENDOK(IDC_CB_FILE, OnCbnSelendokFile	)
	ON_BN_CLICKED(IDC_BN_NEW,	OnBnClickedBnNew	)
	ON_BN_CLICKED(IDC_BN_SAVE,	OnBnClickedBnSave	)
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()

// CWnd_Cfg_LightCtrl message handlers
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
int CWnd_Cfg_LightCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_st_Category.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Category.SetBackColor_COLORREF(RGB(33, 73, 125));
	m_st_Category.SetTextColor(Gdiplus::Color::White, Gdiplus::Color::White);
	m_st_Category.SetFont_Gdip(L"Arial", 12.0F);
	m_st_Category.Create(_T("CHART LIGHT CONTROL"), dwStyle | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

	m_st_File.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_File.SetColorStyle(CVGStatic::ColorStyle_Black);
	m_st_File.SetFont_Gdip(L"Arial", 10.5F);
	m_st_File.Create(_T("Maintenance File"), dwStyle | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

	m_st_Path.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Path.SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_Path.SetFont_Gdip(L"Arial", 11.0F);
	m_st_Path.Create(_T("파일 경로"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_cb_File.Create(dwStyle | CBS_DROPDOWNLIST | CBS_SORT, rectDummy, this, IDC_CB_FILE);
	m_cb_File.SetFont(&m_font);

	m_bn_NewFile.Create(_T("Add New File"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_NEW);
	m_bn_SaveFile.Create(_T("Save File"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_SAVE);
	
	UINT nWndID = 10;
	CString szText;
	// 광원 PSU
	for (UINT nIdx = 0; nIdx < Chart_MaxEnum; nIdx++)
	{
		szText.Format(_T("Light PSU Control : %s"), g_szChartType[nIdx]);
		m_wnd_LightPSU[nIdx].SetTitle(szText.GetBuffer(0));
		m_wnd_LightPSU[nIdx].Create(NULL, _T("Light PSU"), dwStyle | WS_BORDER, rectDummy, this, nWndID++);		
	}

	// 광원 루리텍 보드
	if (m_nSysType == SYS_STEREO_CAL)
	{
		for (UINT nIdx = 0; nIdx < Light_I_MaxEnum; nIdx++)
		{
			m_wnd_LightBrd[nIdx].SetTitle(_T("Light Board : Chart"));
		}
		for (UINT nIdx = 0; nIdx < Light_I_MaxEnum; nIdx++)
		{
			m_wnd_LightBrd[nIdx].Create(NULL, _T("Light Board"), dwStyle | WS_BORDER, rectDummy, this, nWndID++);
		}
	}
	else if (m_nSysType == SYS_IMAGE_TEST )
	{
		m_List_LightBrd_Op.SetOwner(GetOwner());
	//	m_List_LightBrd_Op.SetWindowMessage(m_wm_ChangeOption);
		m_List_LightBrd_Op.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_LIGHT);


// 		for (UINT nIdx = 0; nIdx < Light_I_MaxEnum; nIdx++)
// 		{
// 			szText.Format(_T("Light Board : %s"), g_szLightItem[nIdx]);
// 			m_wnd_LightBrd[nIdx].SetTitle(szText.GetBuffer(0));
// 		}
	}
	else if ( m_nSysType == SYS_FOCUSING)
	{
		


		for (UINT nIdx = 0; nIdx < Light_I_MaxEnum; nIdx++)
		{
		 	szText.Format(_T("Light Board : %s"), g_szLightItem[nIdx]);
		 	m_wnd_LightBrd[nIdx].SetTitle(szText.GetBuffer(0));
		}
		for (UINT nIdx = 0; nIdx < Light_I_MaxEnum; nIdx++)
		{
			m_wnd_LightBrd[nIdx].Create(NULL, _T("Light Board"), dwStyle | WS_BORDER, rectDummy, this, nWndID++);
		}
	}
	else
	{
		for (UINT nIdx = 0; nIdx < Light_I_MaxEnum; nIdx++)
		{
			m_wnd_LightBrd[nIdx].SetTitle(_T("Light Board"));
		}
		for (UINT nIdx = 0; nIdx < Light_I_MaxEnum; nIdx++)
		{
			m_wnd_LightBrd[nIdx].Create(NULL, _T("Light Board"), dwStyle | WS_BORDER, rectDummy, this, nWndID++);
		}
	}



	if (!m_strMaintenancePath.IsEmpty())
	{
		m_IniWatch.SetWatchOption(m_strMaintenancePath, MAINTENANCE_FILE_EXT);
		m_IniWatch.RefreshList();
		RefreshFileList(m_IniWatch.GetFileList());
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
void CWnd_Cfg_LightCtrl::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMagrin			= 10;
	int iSpacing		= 5;
	int iCateSpacing	= 10;

	int iLeft			= iMagrin;
	int iTop			= iMagrin;
	int iWidth			= cx - iMagrin - iMagrin;
	int iHeight			= cy - iMagrin - iMagrin;

	int iCtrlWidth		= 0;
	int iCtrlHeight		= 38;
	int iWndWidth		= (iWidth - iMagrin) / 2;
	int iWndHeight		= (iHeight - iMagrin - iCtrlHeight - iCateSpacing - iSpacing * Light_I_Particle - 1) / (Light_I_Particle);

	int iCapWidth		= 200;
	int iCbWidth		= 280;
	int iBnWidth		= 120;
	iCtrlWidth			= iWidth - iCapWidth - iCbWidth - (iBnWidth * 2) - (iSpacing * 4);

	// 고정
	m_st_File.MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);

	iLeft += iCapWidth + iSpacing;
	m_st_Path.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);

	// 고정
	iLeft += iCtrlWidth + iSpacing;
	m_cb_File.MoveWindow(iLeft, iTop, iCbWidth, iCtrlHeight);

	// 고정
	iLeft += iCbWidth + iSpacing;
	m_bn_NewFile.MoveWindow(iLeft, iTop, iBnWidth, iCtrlHeight);

	// 고정
	iLeft += iBnWidth + iSpacing;
	m_bn_SaveFile.MoveWindow(iLeft, iTop, iBnWidth, iCtrlHeight);

	iLeft = iMagrin;
	iTop += iCtrlHeight + iCateSpacing;

	// 광원 PSU
	for (UINT nIdx = 0; ((nIdx < Chart_MaxEnum) && (nIdx < g_InspectorTable[m_nSysType].LightPSU_Cnt)); nIdx++)
	{
		m_wnd_LightPSU[nIdx].MoveWindow(iLeft, iTop, iWndWidth, iWndHeight);

		iTop += iWndHeight + iCateSpacing;
	}

	// 루리텍 광원 보드
	if (0 < g_InspectorTable[m_nSysType].LightBrd_Cnt)
	{
		if (m_nSysType == SYS_STEREO_CAL)
		{
			for (UINT nIdx = 0; nIdx < g_InspectorTable[m_nSysType].LightBrd_Cnt; nIdx++)
			{
				m_wnd_LightBrd[nIdx].MoveWindow(iLeft, iTop, iWndWidth, iWndHeight);

				iTop += iWndHeight + iCateSpacing;
			}
		}
		else if (m_nSysType == SYS_FOCUSING)
		{
			iLeft += iWndWidth + iMagrin;
			iTop = iMagrin + iCtrlHeight + iCateSpacing;

			for (UINT nIdx = Light_I_Defect_B; nIdx < Light_I_MaxEnum; nIdx++)
			{
				m_wnd_LightBrd[nIdx].MoveWindow(iLeft, iTop, iWndWidth, iWndHeight);

				iTop += iWndHeight + iCateSpacing;
			}

		}
		else if (m_nSysType == SYS_IMAGE_TEST)
		{
			iLeft += iWndWidth + iMagrin;
			iTop = iMagrin + iCtrlHeight + iCateSpacing;

// 			m_wnd_LightBrd[Light_I_Particle].MoveWindow(iLeft, iTop, iWndWidth, iWndHeight);
// 
// 			iLeft += iWndWidth + iMagrin;
// 			iTop   = iMagrin + iCtrlHeight + iCateSpacing;
// 
// 			for (UINT nIdx = 0; nIdx < Light_I_Particle; nIdx++)
// 			{
// 				m_wnd_LightBrd[nIdx].MoveWindow(iLeft, iTop, iWndWidth, iWndHeight);
// 
// 				iTop += iWndHeight + iCateSpacing;
// 			}

			m_List_LightBrd_Op.MoveWindow(iLeft, iTop, iWndWidth, iHeight - iMagrin - iCtrlHeight);

		}
	}
	
}

//=============================================================================
// Method		: OnBnClickedBnNew
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/12 - 13:19
// Desc.		:
//=============================================================================
void CWnd_Cfg_LightCtrl::OnBnClickedBnNew()
{
	m_pstMaintenanceInfo->stLightInfo.Reset();

	if (IDYES == AfxMessageBox(_T("새로 작성 하시겠습니까?"), MB_YESNO))
	{
		CString strFullPath;
		CString strFileTitle;
		CString strFileExt;
		strFileExt.Format(_T("Maintenance File (*.%s)| *.%s||"), MAINTENANCE_FILE_EXT, MAINTENANCE_FILE_EXT);

		CFileDialog fileDlg(FALSE, MAINTENANCE_FILE_EXT, NULL, OFN_OVERWRITEPROMPT, strFileExt);
		fileDlg.m_ofn.lpstrInitialDir = m_strMaintenancePath;

		if (fileDlg.DoModal() == IDOK)
		{
			strFullPath = fileDlg.GetPathName();
			strFileTitle = fileDlg.GetFileTitle();

			// 저장	 		
			if (m_fileMaintenance.SaveMaintenanceFile(strFullPath, m_pstMaintenanceInfo))
 			{
 				// 리스트 모델 갱신
				m_pstMaintenanceInfo->szMaintenanceFile = strFileTitle;
				SetFullPath(m_pstMaintenanceInfo->szMaintenanceFile);
 			}

			// 모델 데이터 불러오기
			GetOwner()->SendNotifyMessage(WM_CHANGED_MAINTENANCE, (WPARAM)m_pstMaintenanceInfo->szMaintenanceFile.GetBuffer(), 0);
			m_pstMaintenanceInfo->szMaintenanceFile.ReleaseBuffer();
		}
	}
}

//=============================================================================
// Method		: OnBnClickedBnSave
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/12 - 14:47
// Desc.		:
//=============================================================================
void CWnd_Cfg_LightCtrl::OnBnClickedBnSave()
{
	GetLightInfo();

	CString strFullPath;
	CString strFileTitle;
	CString strFileExt;
	strFileExt.Format(_T("Maintenance File (*.%s)| *.%s||"), MAINTENANCE_FILE_EXT, MAINTENANCE_FILE_EXT);

	if (m_pstMaintenanceInfo->szMaintenanceFile.IsEmpty())
	{
		CFileDialog fileDlg(FALSE, MAINTENANCE_FILE_EXT, NULL, OFN_OVERWRITEPROMPT, strFileExt);
		fileDlg.m_ofn.lpstrInitialDir = m_strMaintenancePath;

		if (fileDlg.DoModal() == IDOK)
		{
			strFullPath = fileDlg.GetPathName();
			strFileTitle = fileDlg.GetFileTitle();

			// 저장	 		
			if (m_fileMaintenance.SaveMaintenanceFile(strFullPath, m_pstMaintenanceInfo))
			{
				// 리스트 모델 갱신
				m_pstMaintenanceInfo->szMaintenanceFile = strFileTitle;

				SetFullPath(m_pstMaintenanceInfo->szMaintenanceFile);
			}

			// 모델 데이터 불러오기
			GetOwner()->SendNotifyMessage(WM_CHANGED_MAINTENANCE, (WPARAM)m_pstMaintenanceInfo->szMaintenanceFile.GetBuffer(), 0);
			m_pstMaintenanceInfo->szMaintenanceFile.ReleaseBuffer();
		}
	}
	else
	{
		strFullPath.Format(_T("%s%s.%s"), m_strMaintenancePath, m_pstMaintenanceInfo->szMaintenanceFile, MAINTENANCE_FILE_EXT);

		// 저장	 	
		if (m_fileMaintenance.SaveMaintenanceFile(strFullPath, m_pstMaintenanceInfo))
		{
			//AfxMessageBox(_T("저장 되었습니다."));
		}

		// 모델 데이터 불러오기
		GetOwner()->SendNotifyMessage(WM_CHANGED_MAINTENANCE, (WPARAM)m_pstMaintenanceInfo->szMaintenanceFile.GetBuffer(), 0);
		m_pstMaintenanceInfo->szMaintenanceFile.ReleaseBuffer();
	}

}

//=============================================================================
// Method		: OnCbnSelendokFile
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/12 - 13:19
// Desc.		:
//=============================================================================
void CWnd_Cfg_LightCtrl::OnCbnSelendokFile()
{
	// 선택된 파일 로드
	CString szFile;
	CString szMaintenanceFileFull;

	int iSel = m_cb_File.GetCurSel();

	if (0 <= iSel)
	{
		m_cb_File.GetWindowText(szFile);
	}

	szMaintenanceFileFull = m_strMaintenancePath + szFile;
	m_st_Path.SetWindowText(szMaintenanceFileFull + _T(".") + MAINTENANCE_FILE_EXT);

	szMaintenanceFileFull = szFile;
	GetOwner()->SendNotifyMessage(WM_CHANGED_MAINTENANCE, (WPARAM)szFile.GetBuffer(), 0);

	szFile.ReleaseBuffer();
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_LightCtrl::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_BaseView::PreCreateWindow(cs);
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_LightCtrl::PreTranslateMessage(MSG* pMsg)
{
	return CWnd_BaseView::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: SetFullPath
// Access		: protected  
// Returns		: void
// Parameter	: __in LPCTSTR szMaintenanceName
// Qualifier	:
// Last Update	: 2017/10/12 - 14:23
// Desc.		:
//=============================================================================
void CWnd_Cfg_LightCtrl::SetFullPath(__in LPCTSTR szMaintenanceName)
{
	m_strMaintenanceFile = szMaintenanceName;
	m_st_Path.SetText(m_strMaintenancePath + m_strMaintenanceFile + _T(".") + MAINTENANCE_FILE_EXT);
}

//=============================================================================
// Method		: RefreshFileList
// Access		: protected  
// Returns		: void
// Parameter	: __in const CStringList * pFileList
// Qualifier	:
// Last Update	: 2017/10/12 - 13:10
// Desc.		:
//=============================================================================
void CWnd_Cfg_LightCtrl::RefreshFileList(__in const CStringList* pFileList)
{
	m_cb_File.ResetContent();

	INT_PTR iFileCnt = pFileList->GetCount();

	POSITION pos;
	for (pos = pFileList->GetHeadPosition(); pos != NULL;)
	{
		m_cb_File.AddString(pFileList->GetNext(pos));
	}

	// 이전에 선택되어있는 파일 다시 선택
	if (!m_strMaintenancePath.IsEmpty())
	{
		int iSel = m_cb_File.FindStringExact(0, m_strMaintenancePath);

		if (0 <= iSel)
		{
			m_cb_File.SetCurSel(iSel);
		}
	}
}

//=============================================================================
// Method		: SetLightInfo
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/29 - 14:22
// Desc.		:
//=============================================================================
void CWnd_Cfg_LightCtrl::SetLightInfo()
{
	for (UINT nIdx = 0; nIdx < Chart_MaxEnum; nIdx++)
	{
		m_wnd_LightPSU[nIdx].SetLightInfo(&m_pstMaintenanceInfo->stLightInfo.stLightPSU[nIdx]);
	}



	switch (m_nSysType)
	{
	case SYS_IMAGE_TEST:
//	case SYS_FOCUSING:
		m_List_LightBrd_Op.SetPtr_LightInfo(&m_pstMaintenanceInfo->stLightInfo);
		m_List_LightBrd_Op.InsertFullData();
		break;

	default:
	{
			   for (UINT nIdx = 0; nIdx < Light_I_MaxEnum; nIdx++)
			   {
				   m_wnd_LightBrd[nIdx].SetLightInfo(&m_pstMaintenanceInfo->stLightInfo.stLightBrd[nIdx]);
			   }
	}
		break;
	}

}

//=============================================================================
// Method		: GetLightInfo
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/29 - 14:24
// Desc.		:
//=============================================================================
void CWnd_Cfg_LightCtrl::GetLightInfo()
{
	for (UINT nIdx = 0; nIdx < Chart_MaxEnum; nIdx++)
	{
		m_wnd_LightPSU[nIdx].GetLightInfo(m_pstMaintenanceInfo->stLightInfo.stLightPSU[nIdx]);
	}
	switch (m_nSysType)
	{
	case SYS_IMAGE_TEST:
		m_List_LightBrd_Op.SetPtr_LightInfo(&m_pstMaintenanceInfo->stLightInfo);
	//	m_List_LightBrd_Op.GetCellData();
		break;

	default:
	{
			   for (UINT nIdx = 0; nIdx < Light_I_MaxEnum; nIdx++)
			   {
				   m_wnd_LightBrd[nIdx].GetLightInfo(m_pstMaintenanceInfo->stLightInfo.stLightBrd[nIdx]);
			   }
	}
		break;
	}


}

//=============================================================================
// Method		: UpdateMaintenanceInfo
// Access		: public  
// Returns		: void
// Parameter	: __in CString szMaintenanceFile
// Qualifier	:
// Last Update	: 2017/10/12 - 13:07
// Desc.		:
//=============================================================================
void CWnd_Cfg_LightCtrl::UpdateMaintenanceInfo(__in CString szMaintenanceFile)
{
	if (m_pstDevice == NULL)
		return;

	if (!szMaintenanceFile.IsEmpty())
		m_strMaintenanceFile = szMaintenanceFile;

	if (!m_strMaintenancePath.IsEmpty())
	{
		m_IniWatch.SetWatchOption(m_strMaintenancePath, MAINTENANCE_FILE_EXT);
		m_IniWatch.RefreshList();
		RefreshFileList(m_IniWatch.GetFileList());
	}

	if (!m_strMaintenanceFile.IsEmpty())
	{
		int iSel = m_cb_File.FindStringExact(0, m_strMaintenanceFile);

		if (0 <= iSel)
			m_cb_File.SetCurSel(iSel);

		if (!m_strMaintenancePath.IsEmpty())
			m_st_Path.SetText(m_strMaintenancePath + m_strMaintenanceFile + _T(".") + MAINTENANCE_FILE_EXT);
	}

	SetLightInfo();
}
