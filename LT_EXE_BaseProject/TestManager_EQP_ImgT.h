//*****************************************************************************
// Filename	: 	TestManager_EQP_ImgT.h
// Created	:	2018/1/27 - 13:44
// Modified	:	2018/1/27 - 13:44
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#ifndef TestManager_EQP_ImgT_h__
#define TestManager_EQP_ImgT_h__

#pragma once

//#include "Def_Communication.h"
#include "TestManager_EQP.h"
#include "Def_Mes_ImgT.h"
#include "TestManager_TestMES.h"
//=============================================================================
// CTestManager_EQP_ImgT
//=============================================================================
class CTestManager_EQP_ImgT : public CTestManager_EQP
{
public:
	CTestManager_EQP_ImgT();
	virtual ~CTestManager_EQP_ImgT();

protected:

	//-----------------------------------------------------
	// 옵션
	//-----------------------------------------------------
	// 환경설정 데이터 불러오기
	virtual BOOL	OnLoadOption					();	
	virtual void	InitDevicez						(__in HWND hWndOwner = NULL);
	//virtual void	OnInitUISetting					(__in HWND hWndOwner = NULL);


	//-----------------------------------------------------
	// 쓰레드 작업 처리
	//-----------------------------------------------------

	// Load/Unload 쓰레드 시작
	virtual LRESULT	StartOperation_LoadUnload		(__in BOOL bLoad);
	// 검사 쓰레드 시작
	virtual LRESULT	StartOperation_Inspection		(__in UINT nParaIdx = 0);
	// 수동 모드일때 자동화 처리용 함수
	virtual LRESULT	StartOperation_Manual			(__in UINT nStepIdx, __in UINT nParaIdx = 0);

	// 수동 모드일때 검사항목 처리용 함수
	virtual LRESULT	StartOperation_InspManual		(__in UINT nParaIdx = 0);

	// 자동으로 처리되고 있는 쓰레드 강제 종료
	virtual void	StopProcess_LoadUnload			();
	virtual void	StopProcess_Test_All			();
	virtual void	StopProcess_InspManual			(__in UINT nParaIdx = 0);

	// 쓰레드 내에서 처리될 자동 작업 함수
	virtual BOOL	AutomaticProcess_LoadUnload		(__in BOOL bLoad);
	virtual void	AutomaticProcess_Test_All		(__in UINT nParaIdx = 0);
	virtual void	AutomaticProcess_Test_InspManual(__in UINT nParaIdx = 0);

	// 팔레트 로딩/언로딩 초기 세팅
	virtual void	OnInitial_LoadUnload			(__in BOOL bLoad);
	// 팔레트 로딩/언로딩 종료 세팅
	virtual void	OnFinally_LoadUnload			(__in BOOL bLoad);
	// 개별 검사 작업 시작
	virtual LRESULT	OnStart_LoadUnload				(__in BOOL bLoad);

	// 전체 테스트 초기 세팅
	virtual void	OnInitial_Test_All				(__in UINT nParaIdx = 0);
	// 전체 테스트 종료 세팅
	virtual void	OnFinally_Test_All				(__in LRESULT lResult, __in UINT nParaIdx = 0);
	// 전체 검사 작업 시작
	virtual LRESULT	OnStart_Test_All				(__in UINT nParaIdx = 0);

	// 개별 테스트 동작항목
	virtual void	OnInitial_Test_InspManual		(__in UINT nTestItemID, __in UINT nParaIdx = 0);
	virtual void	OnFinally_Test_InspManual		(__in UINT nTestItemID, __in LRESULT lResult, __in UINT nParaIdx = 0);
	virtual LRESULT	OnStart_Test_InspManual			(__in UINT nTestItemID, __in UINT nParaIdx = 0);
	
	// 검사
	virtual LRESULT Load_Product					();
	virtual LRESULT Unload_Product					();

	// 검사기별 검사
	virtual LRESULT	StartTest_Equipment				(__in UINT nParaIdx = 0);
	virtual LRESULT	StartTest_ImageTest				(__in UINT nParaIdx = 0);

	// 쓰레드 내에서 처리될 자동 작업 함수
	virtual void	AutomaticProcess_TestItem		(__in UINT nParaIdx, __in UINT nTestItemID, __in UINT nStepIdx, __in UINT nCornerStep = 0);

	virtual LRESULT	StartTest_ImageTest_TestItem	(__in UINT nTestItemID, __in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	StartTest_Particle_TestItem		(__in UINT nTestItemID, __in UINT nStepIdx, __in UINT nParaIdx = 0);

	// 공용 검사항목
	virtual	LRESULT _TI_Cm_Initialize				(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual	LRESULT _TI_Cm_Finalize					(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual	LRESULT _TI_Cm_Finalize_Error			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual	LRESULT _TI_Cm_MeasurCurrent			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual	LRESULT _TI_Cm_MeasurVoltage			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual	LRESULT _TI_Cm_CaptureImage				(__in UINT nStepIdx, __in UINT nParaIdx = 0, __in BOOL bSaveImage = FALSE, __in LPCTSTR szFileName = NULL);
	virtual	LRESULT _TI_Cm_CameraRegisterSet		(__in UINT nStepIdx, __in UINT nRegisterType, __in UINT nParaIdx = 0);
	virtual LRESULT _TI_Cm_CameraAlphaSet			(__in UINT nStepIdx, __in UINT nAlpha, __in UINT nParaIdx = 0);
	virtual LRESULT _TI_Cm_CameraPowerOnOff			(__in UINT nStepIdx, __in enPowerOnOff nOnOff, __in UINT nParaIdx = 0);
	
	BOOL _TI_Get_ImageBuffer_CaptureImage (enImageMode eImageMode , __in UINT nParaIdx , __out UINT &nImgW, __out UINT &nImgH, __out CString &strTestFileName, __in BOOL bSaveImage = FALSE, __in LPCTSTR szFileName = NULL);

	// IQ 검사 항목 ( Manual )
	virtual LRESULT	_TI_ImgT_MoveStage_Load			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_ImgT_MoveStage_Chart		(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_ImgT_MoveStage_Particle		(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_ImgT_Pro_OpticalCenter		(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_ImgT_Pro_Current			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_ImgT_Pro_FOV				(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	//virtual LRESULT	_TI_ImgT_Pro_SFR				(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_ImgT_Pro_SFR				(__in UINT nStepIdx, __in UINT nParaIdx = 0, __in UINT nTypeCnt = 0);
	virtual LRESULT	_TI_ImgT_Pro_Distortion			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_ImgT_Pro_Rotation			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_ImgT_Pro_Particle			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_ImgT_Pro_Particle_SNR		(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_ImgT_Pro_DefectPixel		(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_ImgT_Pro_Intencity			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_ImgT_Pro_Shading			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_ImgT_Pro_SNR_Light			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT _TI_ImgT_JudgeResult			(__in UINT nStepIdx, __in UINT nTestItem, __in UINT nParaIdx = 0);
//#ifdef SET_GWANGJU

	virtual	LRESULT _TI_ImgT_HotPixel				(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual	LRESULT _TI_ImgT_FPN					(__in UINT nStepIdx, __in UINT nParaIdx =0);
	virtual	LRESULT _TI_ImgT_3D_Depth				(__in UINT nStepIdx, __in UINT nParaIdx =0);
	virtual	LRESULT _TI_ImgT_EEPRON_Verify			(__in UINT nStepIdx, __in UINT nParaIdx =0);
	virtual	LRESULT _TI_ImgT_TemperatureSensor		(__in UINT nStepIdx, __in UINT nParaIdx =0);
	virtual LRESULT	_TI_ImgT_Pro_Particle_Entry			(__in UINT nStepIdx, __in UINT nParaIdx = 0);

//#endif
	// 검사 최종 판정
	enTestResult	Judgment_Inspection_All			();

	// MES 통신
	virtual LRESULT	MES_CheckBarcodeValidation		(__in UINT nParaIdx = 0);
	virtual LRESULT MES_SendInspectionData			(__in UINT nParaIdx = 0);
	virtual LRESULT MES_MakeInspecData_ImgT			(__in UINT nParaIdx = 0);

	// 검사 결과에 따른 메세지 팝업
	virtual void	OnPopupMessageTest_All			(__in enResultCode nResultCode);

	//-----------------------------------------------------
	// Motion
	//-----------------------------------------------------	
	virtual LRESULT	OnMotion_MoveToTestPos			(__in UINT nParaIdx = 0);
	virtual	LRESULT OnMotion_MoveToStandbyPos		(__in UINT nParaIdx = 0);
	virtual LRESULT	OnMotion_MoveToUnloadPos		(__in UINT nParaIdx = 0);

	virtual LRESULT	OnMotion_Origin					();
	virtual LRESULT	OnMotion_LoadProduct			();
	virtual LRESULT	OnMotion_UnloadProduct			();
	virtual LRESULT	OnMotion_ChartTestProduct		(__in UINT nParaIdx = 0);
	virtual LRESULT	OnMotion_ParticleTestProduct	(__in UINT nParaIdx = 0);

	//-----------------------------------------------------
	// Digital I/O
	//-----------------------------------------------------
	// Digital I/O 신호 UI 처리
	virtual void	OnDIO_UpdateDInSignal			(__in BYTE byBitOffset, __in BOOL bOnOff){};
	virtual void	OnDIO_UpdateDOutSignal			(__in BYTE byBitOffset, __in BOOL bOnOff){};
	// I/O 신호 감지 및 신호별 기능 처리
	virtual void	OnDIn_DetectSignal				(__in BYTE byBitOffset, __in BOOL bOnOff);
	virtual void	OnDIn_DetectSignal_ImgT			(__in BYTE byBitOffset, __in BOOL bOnOff);
	// Digital I/O 초기화
	virtual void	OnDIO_InitialSignal				();

	virtual LRESULT	OnDIn_MainPower					();
	virtual LRESULT	OnDIn_EMO						();
	virtual LRESULT	OnDIn_AreaSensor				();
	virtual LRESULT	OnDIn_DoorSensor				();
	virtual LRESULT	OnDIn_JIGCoverCheck				();
	virtual LRESULT	OnDIn_Start						();
	virtual LRESULT	OnDIn_Stop						();
	virtual LRESULT	OnDIn_CheckSafetyIO				();
	virtual LRESULT	OnDIn_CheckJIGCoverStatus		();
		
	virtual LRESULT	OnDOut_BoardPower				(__in BOOL bOn, __in UINT nParaIdx = 0);
	virtual LRESULT	OnDOut_FluorescentLamp			(__in BOOL bOn);
	virtual LRESULT	OnDOut_StartLamp				(__in BOOL bOn);
	virtual LRESULT	OnDOut_StopLamp					(__in BOOL bOn);
	virtual LRESULT	OnDOut_TowerLamp				(__in enLampColor nLampColor, __in BOOL bOn);
	virtual LRESULT	OnDOut_Buzzer					(__in BOOL bOn);

	// DAQ 보드
	virtual LRESULT	OnDAQ_EEPROM_Write				(__in UINT nIdxBrd = 0);
	virtual LRESULT	OnDAQ_EEPROM_OMS_Entry_ImgT		(__in UINT nIdxBrd = 0);
	virtual LRESULT	OnDAQ_EEPROM_OMS_Front_ImgT		(__in UINT nIdxBrd = 0);
	virtual LRESULT	OnDAQ_EEPROM_MRA2_ImgT			(__in UINT nIdxBrd = 0);
	virtual LRESULT	OnDAQ_EEPROM_IKC_ImgT			(__in UINT nIdxBrd = 0);

	//-----------------------------------------------------
	// 카메라 보드 / 광원 제어 보드
	//-----------------------------------------------------
 	

	//-----------------------------------------------------
	// 메뉴얼 테스트
	//-----------------------------------------------------
	virtual LRESULT OnManualSequence				(__in UINT nParaID, __in enParaManual enFunc);
	virtual	LRESULT OnManualTestItem				(__in UINT nParaID, __in UINT nStepIdx);

	//-----------------------------------------------------
	// 테스트
	//-----------------------------------------------------
	// 주변기기 통신 상태 체크
	virtual LRESULT	OnCheck_DeviceComm				();
	// 기구물 안전 체크
	virtual LRESULT	OnCheck_SaftyJIG				();	
	// 모델 설정 데이터가 정상인가 판단
	virtual LRESULT	OnCheck_RecipeInfo				();	
	
	// 제품 결과 판정 및 리포트 처리
	virtual void	OnJugdement_And_Report			(__in UINT nParaIdx = 0);
	virtual void	OnJugdement_And_Report_All		();
	
	//-----------------------------------------------------
	// 타이머 
	//-----------------------------------------------------
	virtual void	CreateTimer_UpdateUI			(__in DWORD DueTime = 5000, __in DWORD Period = 250);
	virtual void	DeleteTimer_UpdateUI			();
	virtual void	OnMonitor_TimeCheck				();
	virtual void	OnMonitor_UpdateUI				();
	
	//-----------------------------------------------------
	// Worklist 처리
	//-----------------------------------------------------
	virtual void	OnSaveWorklist					();
	
	//-----------------------------------------------------
	// Overlay Info
	//-----------------------------------------------------
	virtual void	OnSetOverlayInfo_ImgT			(__in UINT enTestItem){};

	//-----------------------------------------------------
	// 멤버 변수 & 클래스
	//-----------------------------------------------------
	int m_iSelectTest[12];

	// 유저 Stop 처리 변수
	BOOL				m_bFlag_UserStop = FALSE;

public:
	// 생성자 처리용 코드
	virtual void		OnInitialize				();
	// 소멸자 처리용 코드
	virtual	void		OnFinalize					();

	// 제어 권한 상태 설정
	virtual void		SetPermissionMode			(__in enPermissionMode nAcessMode);
	
	
	void TestResultView(UINT nTestID, UINT nPara, LPVOID pParam);
	void TestResultView_Reset(UINT nTestID, UINT nPara);


	virtual void   OnSet_TestResult_Total(__in enTestResult nResult){};


	void EachTest_ResultDataSave(UINT nTestID, UINT nPara);
	virtual void OnReset_CamInfo(__in UINT nParaIdx = 0);
	ST_MES_Data m_stMesData;
	ST_MES_Entry_Data m_stMesEntryData;
	CTestManager_TestMES m_TestMgr_TestMes;

	UINT m_nRegisterChangeMode;
};

#endif // TestManager_EQP_ImgT_h__
