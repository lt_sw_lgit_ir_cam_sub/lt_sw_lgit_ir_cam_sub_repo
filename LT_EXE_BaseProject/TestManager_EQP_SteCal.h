﻿//*****************************************************************************
// Filename	: 	TestManager_EQP_SteCal.h
// Created	:	2016/5/9 - 13:31
// Modified	:	2016/07/22
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef TestManager_EQP_SteCal_h__
#define TestManager_EQP_SteCal_h__

#pragma once

#include "TestManager_EQP.h"
#include "Cal_Stereo.h"

//=============================================================================
// CTestManager_EQP_SteCal
//=============================================================================
class CTestManager_EQP_SteCal : public CTestManager_EQP
{
public:
	CTestManager_EQP_SteCal();
	virtual ~CTestManager_EQP_SteCal();

protected:

	//-----------------------------------------------------
	// 옵션
	//-----------------------------------------------------
	// 환경설정 데이터 불러오기
	virtual BOOL	OnLoadOption					();	
	virtual void	InitDevicez						(__in HWND hWndOwner = NULL);
	//virtual void	OnInitUISetting					(__in HWND hWndOwner = NULL);


	//-----------------------------------------------------
	// 쓰레드 작업 처리
	//-----------------------------------------------------

	// Load/Unload 쓰레드 시작
	virtual LRESULT	StartOperation_LoadUnload		(__in BOOL bLoad);
	// 검사 쓰레드 시작
	virtual LRESULT	StartOperation_Inspection		(__in UINT nParaIdx = 0);
	// 수동 모드일때 자동화 처리용 함수
	virtual LRESULT	StartOperation_Manual			(__in UINT nStepIdx, __in UINT nParaIdx = 0);

	// 수동 모드일때 검사항목 처리용 함수
	virtual LRESULT	StartOperation_InspManual		(__in UINT nParaIdx = 0);

	// 자동으로 처리되고 있는 쓰레드 강제 종료
	virtual void	StopProcess_LoadUnload			();
	virtual void	StopProcess_Test_All			();
	virtual void	StopProcess_InspManual			(__in UINT nParaIdx = 0);

	// 쓰레드 내에서 처리될 자동 작업 함수
	virtual BOOL	AutomaticProcess_LoadUnload		(__in BOOL bLoad);
	virtual void	AutomaticProcess_Test_All		(__in UINT nParaIdx = 0);
	virtual void	AutomaticProcess_Test_InspManual(__in UINT nParaIdx = 0);

	// 팔레트 로딩/언로딩 초기 세팅
	virtual void	OnInitial_LoadUnload			(__in BOOL bLoad);
	// 팔레트 로딩/언로딩 종료 세팅
	virtual void	OnFinally_LoadUnload			(__in BOOL bLoad);
	// 개별 검사 작업 시작
	virtual LRESULT	OnStart_LoadUnload				(__in BOOL bLoad);

	// 전체 테스트 초기 세팅
	virtual void	OnInitial_Test_All				(__in UINT nParaIdx = 0);
	// 전체 테스트 종료 세팅
	virtual void	OnFinally_Test_All				(__in LRESULT lResult, __in UINT nParaIdx = 0);
	// 전체 검사 작업 시작
	virtual LRESULT	OnStart_Test_All				(__in UINT nParaIdx = 0);
	virtual LRESULT	OnStart_Test_Unit				(__in UINT nUnitIdx, __in enThreadTestType nTestType, __in UINT nStepIdx);
	
	// 개별 테스트 동작항목
	virtual void	OnInitial_Test_InspManual		(__in UINT nTestItemID, __in UINT nParaIdx = 0);
	virtual void	OnFinally_Test_InspManual		(__in UINT nTestItemID, __in LRESULT lResult, __in UINT nParaIdx = 0);
	virtual LRESULT	OnStart_Test_InspManual			(__in UINT nTestItemID, __in UINT nParaIdx = 0);
	
	// 검사
	virtual LRESULT Load_Product					();
	virtual LRESULT Unload_Product					();

	// 검사기별 검사
	virtual LRESULT	StartTest_Equipment				(__in UINT nParaIdx = 0);
	virtual LRESULT	StartTest_StereoCAL				(__in BOOL bUsePresetMode = FALSE, __in UINT nPresetIndex = 0);
	virtual LRESULT	StartTest_PresetMode			(__in UINT nPresetIndex, __in UINT nParaIdx = 0);

	
	// 쓰레드 내에서 처리될 자동 작업 함수
	virtual void	AutomaticProcess_TestItem		(__in UINT nParaIdx, __in UINT nTestItemID, __in UINT nStepIdx, __in UINT nCornerStep = 0);

	virtual LRESULT	StartTest_SteCAL_TestItem		(__in UINT nTestItemID, __in UINT nStepIdx, __in UINT nCornerStep = 0, __in UINT nParaIdx = 0);

	// 공용 검사항목
	virtual	LRESULT _TI_Cm_Initialize				(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual	LRESULT _TI_Cm_Finalize					(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual	LRESULT _TI_Cm_Finalize_Error			(__in UINT nParaIdx = 0);
	virtual	LRESULT _TI_Cm_MeasurCurrent			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual	LRESULT _TI_Cm_MeasurVoltage			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual	LRESULT	_TI_Cm_CaptureImage				(__in UINT nStepIdx, __in UINT nParaIdx = 0, __in BOOL bSaveImage = FALSE, __in LPCTSTR szFileName = NULL);
	virtual	LRESULT	_TI_Cm_CameraRegisterSet		(__in UINT nStepIdx, __in UINT nRegisterType, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_Cm_CameraAlphaSet			(__in UINT nStepIdx, __in UINT nAlpha, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_Cm_CameraPowerOnOff			(__in UINT nStepIdx, __in enPowerOnOff nOnOff, __in UINT nParaIdx = 0);
	
	virtual	LRESULT _TI_SteCAL_SetParameter			(__in UINT nStepIdx);
	virtual	LRESULT	_TI_SteCAL_CaptureImage			(__in UINT nCornerStep, __in UINT nParaIdx = 0);
	virtual	LRESULT _TI_SteCAL_DetectPattern		(__in UINT nStepIdx, __in UINT nCornerStep);
	virtual	LRESULT _TI_SteCAL_Calibration			(__in UINT nStepIdx);
	virtual	LRESULT _TI_SteCAL_JudgeResult			(__in UINT nStepIdx, __in UINT nTestItem, __in UINT nParaIdx = 0);
	virtual	LRESULT _TI_SteCAL_JudgeResult_Dual		(__in UINT nStepIdx, __in UINT nTestItem);
	virtual	LRESULT _TI_SteCAL_WriteEEPROM			(__in UINT nStepIdx);
	virtual	LRESULT _TI_SteCAL_WriteEEPROM_Dual		(__in UINT nStepIdx);
	virtual	LRESULT _TI_SteCAL_WriteJSON			(__in UINT nStepIdx);

	// 임시로 Cal 결과에 대한 판정을 하는 함수
	virtual	BOOL	GetJudge_CalibrationResult		(__in UINT nStartStepIdx, __out UINT& nFailTestItem);
	// 우선순위에 따른 Cal 결과의 Fail 항목 구하는 함수
	virtual BOOL	GetFailTestItem_CalResult		(__out UINT& nOutTestItem);
	// 선택된 검사항목의 프리셋 인덱스 구하는 함수
	virtual BOOL	GetPresetIndex					(__in UINT nInTestItem, __out UINT& nOutPresetIndex);
	// Detect Pattern 좌표 로그 남기기
	virtual BOOL	WriteLog_DetectPattern			(__in UINT nCornerStep, __in BOOL bError = FALSE);


	// Library 연동 (2D CAL)
	virtual LRESULT OnLib_SteCAL_SetParameter		();
	virtual LRESULT	OnLib_SteCAL_DetectPattern		(__in UINT nCornerStep);
	virtual LRESULT	OnLib_SteCAL_ExecIntrinsic		(__in UINT nOptionFlag = 0x02);
	virtual LRESULT	OnLib_SteCAL_ExecIntrinsic_File	();
	virtual void	OnLib_SteCAL_CloseHandle		();

	// MES 통신
	virtual LRESULT	MES_CheckBarcodeValidation		(__in UINT nParaIdx = 0);
	virtual LRESULT MES_SendInspectionData			(__in UINT nParaIdx = 0);
	virtual LRESULT MES_MakeInspecData_SteCAL		(__in UINT nParaIdx = 0);

	virtual LRESULT MES_Make_SteCAL_Mono			(__in UINT nParaIdx = 0);
	virtual LRESULT MES_Make_SteCAL_Slave_Dummy		(__in UINT nParaIdx = 0);
	virtual LRESULT MES_Make_SteCAL_Stereo_Dummy	(__in UINT nParaIdx = 0);

	virtual LRESULT MES_Make_SteCAL_Master			(__in UINT nParaIdx = 0);
	virtual LRESULT MES_Make_SteCAL_Slave			(__in UINT nParaIdx = 0);
	virtual LRESULT MES_Make_SteCAL_Stereo			(__in UINT nParaIdx = 0);
	

	// 검사 결과에 따른 메세지 팝업
	virtual void	OnPopupMessageTest_All			(__in enResultCode nResultCode);

	//-----------------------------------------------------
	// Motion
	//-----------------------------------------------------	
	virtual LRESULT	OnMotion_MoveToTestPos			(__in UINT nParaIdx = 0);
	virtual	LRESULT OnMotion_MoveToStandbyPos		(__in UINT nParaIdx = 0);
	virtual LRESULT	OnMotion_MoveToUnloadPos		(__in UINT nParaIdx = 0);

	virtual LRESULT	OnMotion_Origin					();
	virtual LRESULT	OnMotion_LoadProduct			();
	virtual LRESULT	OnMotion_UnloadProduct			();
	virtual LRESULT	OnMotion_SteCal_Test			(__in double dbDistanceX, __in double dbDistanceY, __in double dbDistanceZ, __in double dbChartTX, __in double dbChartTZ, __in BOOL bUseX, __in BOOL bUseY, __in BOOL bUseZ, __in BOOL bUseTX, __in BOOL bUseTZ, __in UINT nParaIdx = 0);

	//-----------------------------------------------------
	// Digital I/O
	//-----------------------------------------------------
	// Digital I/O 신호 UI 처리
	virtual void	OnDIO_UpdateDInSignal			(__in BYTE byBitOffset, __in BOOL bOnOff){};
	virtual void	OnDIO_UpdateDOutSignal			(__in BYTE byBitOffset, __in BOOL bOnOff){};
	// I/O 신호 감지 및 신호별 기능 처리
	virtual void	OnDIn_DetectSignal				(__in BYTE byBitOffset, __in BOOL bOnOff);
	virtual void	OnDIn_DetectSignal_SteCal		(__in BYTE byBitOffset, __in BOOL bOnOff);
	// Digital I/O 초기화
	virtual void	OnDIO_InitialSignal				();

	virtual LRESULT	OnDIn_MainPower					();
	virtual LRESULT	OnDIn_EMO						();
	virtual LRESULT	OnDIn_AreaSensor				();
	virtual LRESULT	OnDIn_DoorSensor				();
	virtual LRESULT	OnDIn_JIGCoverCheck				();
	virtual LRESULT	OnDIn_Start						();
	virtual LRESULT	OnDIn_Stop						();
	virtual LRESULT	OnDIn_CheckSafetyIO				();
	virtual LRESULT	OnDIn_CheckJIGCoverStatus		();
		
	virtual LRESULT	OnDOut_BoardPower				(__in BOOL bOn, __in UINT nParaIdx = 0);
	virtual LRESULT	OnDOut_FluorescentLamp			(__in BOOL bOn);
	virtual LRESULT	OnDOut_StartLamp				(__in BOOL bOn);
	virtual LRESULT	OnDOut_StopLamp					(__in BOOL bOn);
	virtual LRESULT	OnDOut_TowerLamp				(__in enLampColor nLampColor, __in BOOL bOn);
	virtual LRESULT	OnDOut_Buzzer					(__in BOOL bOn);

	// DAQ 보드
	virtual LRESULT	OnDAQ_EEPROM_Write				(__in UINT nIdxBrd = 0);
	virtual LRESULT	OnDAQ_EEPROM_MRA2_SteCal		();
	virtual LRESULT	OnDAQ_EEPROM_MRA2_SteCal_M		(__in BOOL bMasterCamera);
	virtual LRESULT	OnDAQ_EEPROM_Verify_MRA2_Cal	(__out CString& szOutMasterBarcode, __out CString& szOutSlaveBarcode, __out SYSTEMTIME& stOutTime, __out STEREO_CALIBRATION_RESULT& stStereo);
	virtual LRESULT	OnDAQ_EEPROM_Verify_MRA2_Cal_M	(__in BOOL bMasterCamera, __in BOOL bMainPage, __out CString& szOutBarcode, __out SYSTEMTIME& stOutTime, __out STEREO_CALIBRATION_RESULT& stStereo);
	//virtual LRESULT	OnDAQ_EEPROM_Verify_MRA2_Log	(__in const LPBYTE pbyEEPROM, __in UINT nDataLeng, __in BOOL bUseResultLog);
	virtual LRESULT	OnDAQ_EEPROM_Verify_MRA2_Log_M	(__in BOOL bMasterCamera, __in BOOL bMainPage, __in const LPBYTE pbyEEPROM, __in UINT nDataLeng, __in BOOL bUseResultLog);
	
	virtual LRESULT	OnDAQ_EEPROM_IKC_SteCal			(__in UINT nIdxBrd = 0);
	virtual LRESULT	OnDAQ_EEPROM_Verify_IKC_Cal		(__out CString& szOutBarcode, __out SYSTEMTIME& stOutTime, __out SINGLE_CALIBRATION_RESULT& stMono);
	virtual LRESULT	OnDAQ_EEPROM_Verify_IKC_Log		(__in const LPBYTE pbyEEPROM, __in UINT nDataLeng, __in BOOL bUseResultLog);

	//-----------------------------------------------------
	// 카메라 보드 / 광원 제어 보드
	//-----------------------------------------------------
 	

	//-----------------------------------------------------
	// 메뉴얼 테스트
	//-----------------------------------------------------
	virtual LRESULT OnManualSequence				(__in UINT nParaID, __in enParaManual enFunc);
	virtual	LRESULT OnManualTestItem				(__in UINT nParaID, __in UINT nStepIdx);

	//-----------------------------------------------------
	// 테스트
	//-----------------------------------------------------
	// 주변기기 통신 상태 체크
	virtual LRESULT	OnCheck_DeviceComm				();
	// 기구물 안전 체크
	virtual LRESULT	OnCheck_SaftyJIG				();	
	// 모델 설정 데이터가 정상인가 판단
	virtual LRESULT	OnCheck_RecipeInfo				();	
	
	// 제품 결과 판정 및 리포트 처리
	virtual void	OnJugdement_And_Report			(__in UINT nParaIdx = 0);
	virtual void	OnJugdement_And_Report_All		();
	
	//-----------------------------------------------------
	// 타이머 
	//-----------------------------------------------------
	virtual void	CreateTimer_UpdateUI			(__in DWORD DueTime = 5000, __in DWORD Period = 250);
	virtual void	DeleteTimer_UpdateUI			();
	virtual void	OnMonitor_TimeCheck				();
	virtual void	OnMonitor_UpdateUI				();
	
	//-----------------------------------------------------
	// Worklist 처리
	//-----------------------------------------------------
	virtual void	OnSaveWorklist					();
	
	//-----------------------------------------------------
	// 멤버 변수 & 클래스
	//-----------------------------------------------------
	CCal_Stereo		m_CalStereo;

	SYSTEMTIME		m_tmEEPROMWrite;
	BYTE			arbyBufRead_Master[256];
	BYTE			arbyBufRead_Slave[256];

	// 패턴 검출 
	UINT			m_nDetectPatternCnt				= 0;

public:
	
	// 생성자 처리용 코드
	virtual void		OnInitialize				();
	// 소멸자 처리용 코드
	virtual	void		OnFinalize					();

	// 제어 권한 상태 설정
	virtual void		SetPermissionMode			(__in enPermissionMode nAcessMode);
	
	
};

#endif // TestManager_EQP_SteCal_h__

