//*****************************************************************************
// Filename	: 	Def_MTCtrl_Item.h
// Created	:	2018/2/8 - 13:21
// Modified	:	2018/2/8 - 13:21
//
// Author	:	PiRing
//	
// Purpose	:	Maintenance 개별 제어 항목
//****************************************************************************
#ifndef Def_CanCommTest_h__
#define Def_CanCommTest_h__

#include <afxwin.h>

typedef	enum enMTCtrl_TestItem
{
	MTCtrl_00,	// 
	MTCtrl_01,	// 
	MTCtrl_02,	// 
	MTCtrl_03,	// 
	MTCtrl_04,	// 
	MTCtrl_05,	// 
	MTCtrl_06,	// 
	MTCtrl_07,	// 
	MTCtrl_08,	// 
	MTCtrl_09,	// 
	MTCtrl_10,	// 
	MTCtrl_11,	// 
	MTCtrl_12,	// 
	MTCtrl_13,	// 
	MTCtrl_14,	// 
	MTCtrl_15,	// 
	MTCtrl_16,	// 
	MTCtrl_17,	// 
	MTCtrl_18,	// 
	MTCtrl_19,	// 
	MTCtrl_20,	// 
	MTCtrl_21,	// 
	MTCtrl_22,	// 
	MTCtrl_MaxEnum
};

static LPCTSTR g_szMtCtrl_TestItem[] =
{
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	NULL
};

typedef	enum enMTCtrl_Calibration
{
	MTCtrl_Cal_00,
	MTCtrl_Cal_01,
	MTCtrl_Cal_02,
	MTCtrl_Cal_03,
	MTCtrl_Cal_04,
	MTCtrl_Cal_05,
	MTCtrl_Cal_06,
	MTCtrl_Cal_07,
	MTCtrl_Cal_08,
	MTCtrl_Cal_09,
	MTCtrl_Cal_10,
	MTCtrl_Cal_11,
	MTCtrl_Cal_12,
	MTCtrl_Cal_13,
	MTCtrl_Cal_MaxNum
};

static LPCTSTR g_szMTCtrl_Calibration[] =
{
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	NULL
};

typedef	enum enMTCtrl_VsAlgo
{
	MTCtrl_VsAlgo_Se_CenterPoint,
	MTCtrl_VsAlgo_Se_FiducialMark,
	MTCtrl_VsAlgo_Se_EdgeLine,
	MTCtrl_VsAlgo_Se_SFR,
	MTCtrl_VsAlgo_Se_SNR,
	MTCtrl_VsAlgo_VCSEL_SendParam,
	MTCtrl_VsAlgo_Re_OpticalCenterX,
	MTCtrl_VsAlgo_Re_OpticalCenterY,
	MTCtrl_VsAlgo_Re_DynamicRange,
	MTCtrl_VsAlgo_Re_SNR_BW,
	MTCtrl_VsAlgo_Re_SNR_IQ,
	MTCtrl_VsAlgo_Re_FOV_Hor,
	MTCtrl_VsAlgo_Re_FOV_Ver,
	MTCtrl_VsAlgo_Re_FOV_LTRB,
	MTCtrl_VsAlgo_Re_FOV_RTLB,
	MTCtrl_VsAlgo_Re_SFR,
	MTCtrl_VsAlgo_Re_Distortion,
	MTCtrl_VsAlgo_Re_Rotation,
	MTCtrl_VsAlgo_Re_Tilt,
	MTCtrl_VsAlgo_MaxNum
};

static LPCTSTR g_szMTCtrl_VsAlgo[] =
{
	_T("Send CenterPoint	"),
	_T("Send FiducialMark	"),
	_T("Send EdgeLine		"),
	_T("Send SFR			"),
	_T("Send SNR			"),
	_T("Send VCSEL			"),
	_T("Result Center X		"),
	_T("Result Center Y		"),
	_T("Result Dynamic		"),
	_T("Result SNR BW		"),
	_T("Result SNR_IQ		"),
	_T("Result FOV_Hor		"),
	_T("Result FOV_Ver		"),
	_T("Result FOV_LTRB		"),
	_T("Result FOV_RTLB		"),
	_T("Result SFR			"),
	_T("Result Distortion	"),
	_T("Result Rotation		"),
	_T("Result Tilt			"),
	NULL
};

typedef	enum enMTCtrl_Particle
{
	MTCtrl_Particle_Sn_Alpha,
	MTCtrl_Particle_Sn_Particle,
	MTCtrl_Particle_Sn_DefectPixel,
	MTCtrl_Particle_Sn_Rtllumination,
	MTCtrl_Particle_Sn_Light_SNR,
	MTCtrl_Particle_Re_Stain,
	MTCtrl_Particle_Re_Blemish,
	MTCtrl_Particle_Re_DeadPixel,
	MTCtrl_Particle_Re_DefectPixel,
	MTCtrl_Particle_Re_Rtllumination,
	MTCtrl_Particle_Re_Light_SNR,
	MTCtrl_Particle_MaxNum
};

static LPCTSTR g_szMTCtrl_Particle[] =
{
	_T("Send Alpha								"),
	_T("Send Particle							"),
	_T("Send DefectPixel							"),
	_T("Send Rtllumination						"),
	_T("Send Light SNR							"),
	_T("Result Stain		 \r\n (Index : 0~2) "),
	_T("Result Blemish		 \r\n (Index : 0~2) "),
	_T("Result DeadPixel	 \r\n (Index : 0~2) "),
	_T("Result Hot Pixel	 \r\n (Index : 0~2) "),
	_T("Result Shading \r\n (Index : 0~2) "),
	_T("Result Light SNR	 \r\n (Index : 0~2) "),
	NULL
};



#endif // Def_CanCommTest_h__
