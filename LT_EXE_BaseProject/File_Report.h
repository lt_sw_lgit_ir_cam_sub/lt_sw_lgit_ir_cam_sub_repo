﻿//*****************************************************************************
// Filename	: 	File_Report.h
// Created	:	2016/8/5 - 15:50
// Modified	:	2016/8/5 - 15:50
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef File_Report_h__
#define File_Report_h__

#pragma once

#include "Def_DataStruct.h"

//=============================================================================
// CFile_Report
//=============================================================================

class CFile_Report
{
public:
	CFile_Report();
	~CFile_Report();


protected:
	// 최종 파일 저장
	BOOL	SaveFinalizeResult					(__in CString szFullPath, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray);

	// 장비별 Log 데이터 생성
	BOOL	Make2DCalHeadAndData				(__in CString szBarcode, __in const ST_2DCal_Result* pstResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray);
	BOOL	Make2DCalHeadAndData				(__in const ST_CamInfo* pstResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray);
	
	BOOL	MakeSteCalHeadAndData_Mono			(__in CString szBarcode, __in const SINGLE_CALIBRATION_RESULT* pstResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray);
	BOOL	MakeSteCalHeadAndData_Dual			(__in CString szBarcode, __in const STEREO_CALIBRATION_RESULT* pstResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray);
	BOOL	Make_HeadAndData_SteCal_Mono_EEPROM	(__in CString szBarcode, __in const SYSTEMTIME* pstTime, __in const SINGLE_CALIBRATION_RESULT* pstResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray);

	BOOL	Make_HeadAndData_SteCal_Ste_EEPROM	(__in BOOL bMaster, __in BOOL bMainPage, __in CString szBarcode, __in const SYSTEMTIME* pstTime, __in const STEREO_CALIBRATION_RESULT* pstResult, __in CStringArray* pszHeadArray, __in CStringArray* pszDataArray);

public:
	// 2D CAL Log 저장 --------------------------------------------------------
	BOOL	SaveFinalizeResult2DCal				(__in LPCTSTR szPath, __in UINT nParaIdx, __in CString szBarcode, __in const ST_2DCal_Result* pstResult);
	BOOL	SaveFinalizeResult2DCal				(__in LPCTSTR szPath, __in UINT nParaIdx, __in const ST_CamInfo* pstResult);

	// Stereo CAL Log 저장 ----------------------------------------------------
	BOOL	SaveFinalizeResultSteCal_Mono		(__in LPCTSTR szPath, __in CString szBarcode, __in const SINGLE_CALIBRATION_RESULT* pstResult);
	BOOL	SaveFinalizeResultSteCal_Dual		(__in LPCTSTR szPath, __in CString szBarcode, __in const STEREO_CALIBRATION_RESULT* pstResult);

	// Stereo CAL Detect Pattern Log 저장
	BOOL	Save_SteCal_DetectPattern			(__in LPCTSTR szPath, __in CString szBarcode, __in BOOL bStereo, __in const SYSTEMTIME* pInTime, __in UINT nConerStep, __in const ST_SteCal_Result* pstResult, __in BOOL bError = FALSE);

	// JSON 저장
	BOOL	SaveJSON_StereoCAL_Mono				(__in LPCTSTR szPath, __in const SINGLE_CALIBRATION_RESULT* pstResult);
	BOOL	SaveJSON_StereoCAL_Stereo			(__in LPCTSTR szPath, __in const STEREO_CALIBRATION_RESULT* pstResult);

	BOOL	SaveJSON_StereoCAL_Mono_Raw			(__in LPCTSTR szPath, __in const SINGLE_CALIBRATION_RESULT* pstResult);
	BOOL	SaveJSON_StereoCAL_Stereo_Raw		(__in LPCTSTR szPath, __in const STEREO_CALIBRATION_RESULT* pstResult);

	// IKC EEPROM Verify
	BOOL	Save_SteCal_Mono_EEPROM				(__in LPCTSTR szPath, __in LPCTSTR szBarcode, __in const SYSTEMTIME* pstTime, __in const SINGLE_CALIBRATION_RESULT* pstResult);
	BOOL	Save_SteCal_Mono_EEPROM_Bin			(__in LPCTSTR szPath, __in LPCTSTR szBarcode, __in const SYSTEMTIME* pstTime, __in const LPBYTE parbyEEPROM, __in UINT nDataLen);

	// MRA2 EEPROM Verify (Master/Slave, Mirror Page)
	BOOL	Save_SteCal_Stereo_EEPROM			(__in BOOL bMaster, __in BOOL bMainPage, __in LPCTSTR szPath, __in LPCTSTR szBarcode, __in const SYSTEMTIME* pstTime, __in const STEREO_CALIBRATION_RESULT* pstResult);
	BOOL	Save_SteCal_Stereo_EEPROM_Bin		(__in BOOL bMaster, __in BOOL bMainPage, __in LPCTSTR szPath, __in LPCTSTR szBarcode, __in const SYSTEMTIME* pstTime, __in const LPBYTE parbyEEPROM, __in UINT nDataLen);


};

#endif // File_Report_h__

