// Wnd_Graph.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_Graph.h"
#include "resource.h"

static LPCTSTR	g_szGraph_Static[] =
{
	_T("SFR GRAPH		"),				//	STI_GP_R_TITLE = 0,
	_T("SFR	[P1]		"),				//	STI_GP_R_CURRENT,
	_T("SFR	[P2]		"),				//	STI_GP_R_CURRENT,
	_T("MAX SFR	[P1]	"),				//	STI_GP_R_MAX_SFR,
	_T("MAX SFR	[P2]	"),				//	STI_GP_R_MAX_SFR,
	_T("RESULT			"),				//	STI_GP_R_RESULT,
	NULL
};

static LPCTSTR	g_szGraph_Button[] =
{
	_T("RESET"),
	_T("TEST"),
	NULL
};

// CWnd_Graph
typedef enum GraphOptID
{
	IDC_BTN_ITEM		= 1001,
	IDC_CMB_ITEM		= 2001,
	IDC_EDT_ITEM		= 3001,
	IDC_LIST_ITEM		= 4001,
};

#define IDC_STATIC_RT_GRAPH			9000

IMPLEMENT_DYNAMIC(CWnd_Graph, CWnd)

CWnd_Graph::CWnd_Graph()
{
	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Graph::~CWnd_Graph()
{
	if (NULL != m_pGraph)
	{
		delete m_pGraph;
	}

	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Graph, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
END_MESSAGE_MAP()

// CWnd_Graph 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_Graph::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdx = 0; nIdx < STI_GP_R_MAX; nIdx++)
	{
		m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);

		if (STI_GP_R_TITLE == nIdx || STI_GP_R_RESULT == nIdx)
		{
			m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
			m_st_Item[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		}
		else
		{
			m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
			m_st_Item[nIdx].SetTextAlignment(StringAlignmentNear);
			m_st_Item[nIdx].SetFont_Gdip(L"Arial", 8.0F);
		}

		m_st_Item[nIdx].Create(g_szGraph_Static[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	m_st_Item[STI_GP_R_RESULT].SetColorStyle(CVGStatic::ColorStyle_Default);

	for (UINT nIdx = 0; nIdx < BTN_GP_R_MAX; nIdx++)
	{
		m_bn_Item[nIdx].Create(g_szGraph_Button[nIdx], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_ITEM + nIdx);
		m_bn_Item[nIdx].SetFont(&m_font);
	}

	for (UINT nIdx = 0; nIdx < EDT_GP_R_MAX; nIdx++)
	{
		m_ed_Item[nIdx].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EDT_ITEM + nIdx);
		m_ed_Item[nIdx].SetWindowText(_T("0"));
		m_ed_Item[nIdx].SetValidChars(_T("0123456789.-"));
		m_ed_Item[nIdx].EnableWindow(FALSE);
	}

	for (UINT nIdx = 0; nIdx < CMB_GP_R_MAX; nIdx++)
	{
		m_cb_Item[nIdx].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CMB_ITEM + nIdx);
	}

	m_pGraph = new COScopeCtrl(2);
	m_pGraph->Create(WS_VISIBLE | WS_CHILD | WS_BORDER, CRect(0, 0, 200, 400), this, IDC_STATIC_RT_GRAPH);
	m_pGraph->SetGridColor(RGB(255, 255, 255));
	m_pGraph->SetPlotColor(RGB(255,   0,   0), 0);
	m_pGraph->SetPlotColor(RGB(255, 228,   0), 1);
	m_pGraph->SetRanges(0, 30.0);
	m_pGraph->autofitYscale = true;
	m_pGraph->SetXUnits(_T(""));
	m_pGraph->SetYUnits(_T("SFR"));
	m_pGraph->SetLegendLabel(_T("SFR"), 0);
	m_pGraph->SetLegendLabel(_T("SFR"), 1);
	m_pGraph->InvalidateCtrl();
	
	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_Graph::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iMargin  = 5;
	int iSpacing = 5;

	int iLeft	 = 0;
	int iTop	 = 0;
	int iWidth   = cx; 
	int iHeight  = cy;
	
	int iHeaderH = 40;

	int iSTWidth = iWidth / 5;
	int iSTHeight = 25;

	m_st_Item[STI_GP_R_TITLE].MoveWindow(iLeft, iTop, iWidth, iSTHeight);

	iLeft = iWidth - iSTWidth;
	iTop += iSTHeight + iMargin;
	m_st_Item[STI_GP_R_CURRENT_P1].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iTop += iSTHeight + iMargin;
	m_st_Item[STI_GP_R_MAX_SFR_P1].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iTop += iSTHeight + iMargin;
	m_st_Item[STI_GP_R_CURRENT_P2].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iTop += iSTHeight + iMargin;
	m_st_Item[STI_GP_R_MAX_SFR_P2].MoveWindow(iLeft, iTop, iSTWidth, iSTHeight);

	iTop += iSTHeight + iMargin;
	m_st_Item[STI_GP_R_RESULT].MoveWindow(iLeft, iTop, iSTWidth, iHeight - iTop);

	iTop	= iSTHeight + iMargin;
	iLeft	= 0;

	int iGraphW = iWidth - iSTWidth - iMargin;
	int iGraphH = iHeight - iTop;

	m_pGraph->MoveWindow(iLeft, iTop, iGraphW, iGraphH);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_Graph::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_Graph::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_Graph::OnRangeBtnCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_BTN_ITEM;
}

//=============================================================================
// Method		: SetUpdateMax
// Access		: public  
// Returns		: void
// Parameter	: __in ST_Focus_Data stData
// Qualifier	:
// Last Update	: 2018/8/20 - 11:11
// Desc.		:
//=============================================================================
void CWnd_Graph::SetUpdateMax(__in ST_Focus_Data stData)
{
	double dbCurrentSFR[ROI_Focus_Max] = { 0.0, };

	BOOL bMaxSFR = TRUE;

	for (UINT nROI = 0; nROI < ROI_Focus_Max; nROI++)
	{
		CString szValue;

		// UI 표시
		szValue.Format(_T("%s : %.2f"), g_szGraph_Static[STI_GP_R_CURRENT_P1 + nROI], stData.dbCurrentValue[nROI]);
		m_st_Item[STI_GP_R_CURRENT_P1 + nROI].SetText(szValue);

		szValue.Format(_T("%s : %.2f"), g_szGraph_Static[STI_GP_R_MAX_SFR_P1 + nROI], stData.dbMaxValue[nROI]);
		m_st_Item[STI_GP_R_MAX_SFR_P1 + nROI].SetText(szValue);
	
		dbCurrentSFR[nROI] = stData.dbCurrentValue[nROI];
		
		// MAX SFR 양불 확인
		if (FALSE == stData.bMaxSFR[nROI])
		{
			bMaxSFR = FALSE;
		}
	}

	// SFR 그래프 표시
	m_pGraph->AppendPoints(dbCurrentSFR);

	// 최종 MAX SFR 양불 표시
	if (TRUE == bMaxSFR)
	{
		m_st_Item[STI_GP_R_RESULT].SetColorStyle(CVGStatic::ColorStyle_Green);
		m_st_Item[STI_GP_R_RESULT].SetText(_T("OK"));
	}
	else
	{
		m_st_Item[STI_GP_R_RESULT].SetColorStyle(CVGStatic::ColorStyle_Red);
		m_st_Item[STI_GP_R_RESULT].SetText(_T("NG"));
	}
}

//=============================================================================
// Method		: SetUpdateBest
// Access		: public  
// Returns		: void
// Parameter	: __in ST_Focus_Data stData
// Qualifier	:
// Last Update	: 2018/8/20 - 11:28
// Desc.		:
//=============================================================================
void CWnd_Graph::SetUpdateBest(__in ST_Focus_Data stData)
{
	double dbCurrentSFR[ROI_Focus_Max] = { 0.0, };

	for (UINT nROI = 0; nROI < ROI_Focus_Max; nROI++)
	{
		CString szValue;

		// UI 표시
		szValue.Format(_T("%s : %.2f"), g_szGraph_Static[STI_GP_R_CURRENT_P1], stData.dbCurrentValue[nROI]);
		m_st_Item[STI_GP_R_CURRENT_P1 + nROI].SetText(szValue);

		szValue.Format(_T("%s : %.2f"), g_szGraph_Static[STI_GP_R_MAX_SFR_P1], stData.dbMaxValue[nROI]);
		m_st_Item[STI_GP_R_MAX_SFR_P1 + nROI].SetText(szValue);

		dbCurrentSFR[nROI] = stData.dbCurrentValue[nROI];
	}

	// SFR 그래프 표시
	m_pGraph->AppendPoints(dbCurrentSFR);

	// 최종 MAX SFR 양불 표시
	if (TRUE == stData.nResult)
	{
		m_st_Item[STI_GP_R_RESULT].SetColorStyle(CVGStatic::ColorStyle_Green);
		m_st_Item[STI_GP_R_RESULT].SetText(_T("OK"));
	}
	else
	{
		m_st_Item[STI_GP_R_RESULT].SetColorStyle(CVGStatic::ColorStyle_Red);
		m_st_Item[STI_GP_R_RESULT].SetText(_T("NG"));
	}
}

//=============================================================================
// Method		: SetUpdateSpec
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/7/6 - 8:12
// Desc.		:
//=============================================================================
void CWnd_Graph::SetUpdateSpec()
{
// 	for (UINT nROI = 0; nROI < ROI_Focus_Max; nROI++)
// 	{
// 		CString szValue;
// 
// 		// 그래프 명칭 표시
// 		szValue.Format(_T("ROI[%d]"), m_pstConfigInfo->nSelectROI[nROI]);
// 		m_pGraph->SetLegendLabel(szValue, nROI);
// 	}
// 
// 	// 그래프 스케일 표시
// 	m_pGraph->m_nXGrids = (int)m_pstConfigInfo->nScaleX;
// 	m_pGraph->m_nYGrids = (int)m_pstConfigInfo->nScaleY;
// 
// 	m_pGraph->SetRanges(m_pstConfigInfo->dbRangeMin, m_pstConfigInfo->dbRangeMax);
}

//=============================================================================
// Method		: SetResetData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/7/6 - 9:29
// Desc.		:
//=============================================================================
void CWnd_Graph::SetResetData()
{
	m_pGraph->Reset();
	
	for (UINT nROI = 0; nROI < ROI_Focus_Max; nROI++)
	{
		CString szValue;

		szValue.Format(_T("%s : "), g_szGraph_Static[STI_GP_R_CURRENT_P1+nROI]);
		m_st_Item[STI_GP_R_CURRENT_P1 + nROI].SetText(szValue);

		szValue.Format(_T("%s : "), g_szGraph_Static[STI_GP_R_MAX_SFR_P1 + nROI]);
		m_st_Item[STI_GP_R_MAX_SFR_P1 + nROI].SetText(szValue);

	}

	m_st_Item[STI_GP_R_RESULT].SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_Item[STI_GP_R_RESULT].SetText(_T("STAND BY"));
}
