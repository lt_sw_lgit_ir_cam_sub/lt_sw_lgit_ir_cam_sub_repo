﻿//*****************************************************************************
// Filename	: 	Overlay_Proc.cpp
// Created	:	2017/3/23 - 08:14
// Modified	:	2017/3/23 - 08:14
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "Overlay_Proc.h"


COverlay_Proc::COverlay_Proc()
{
}


COverlay_Proc::~COverlay_Proc()
{
}

//=============================================================================
// Method		: Overlay_LockingScrew
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_Torque_Opt stOption
// Parameter	: __in ST_Torque_Data stData
// Qualifier	:
// Last Update	: 2018/6/21 - 13:55
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_LockingScrew(__inout IplImage* lpImage, __in ST_Torque_Opt stOption, __in ST_Torque_Data stData)
{
	if (NULL == lpImage)
		return;

	int		iLineSize = 4;
	int		iMarkSize = 10;
	double dbFontSize = 1.3;

	CRect	rtRect;
	CPoint	ptCenter;

	CString szText;
	szText = _T("Locking The Screw!!");

	iLineSize = 6;
	dbFontSize = 2.0;

	rtRect.left = lpImage->width / 2 - 310;
	rtRect.top = 100;

	// 추후 적용 예정
	//if (TR_Pass == stData.nDetectResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 150, 0), iLineSize, dbFontSize, szText);
	}
	//else
	//{
	//	Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	//}
}

//=============================================================================
// Method		: Overlay_ReleaseScrew
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_Torque_Opt stOption
// Parameter	: __in ST_Torque_Data stData
// Qualifier	:
// Last Update	: 2018/3/12 - 19:52
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_ReleaseScrew(__inout IplImage* lpImage, __in ST_Torque_Opt stOption, __in ST_Torque_Data stData)
{
	if (NULL == lpImage)
		return;

	int		iLineSize = 4;
	int		iMarkSize = 10;
	double dbFontSize = 1.3;

	CRect	rtRect;
	CPoint	ptCenter;

	CString szText;
	szText = _T("Release The Screw!!");

	iLineSize = 6;
	dbFontSize = 2.0;

	rtRect.left = lpImage->width / 2 - 310;
	rtRect.top = 100;

	// 추후 적용 예정
	//if (TR_Pass == stData.nDetectResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 150, 0), iLineSize, dbFontSize, szText);
	}
	//else
	//{
	//	Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	//}
}

//=============================================================================
// Method		: Overlay_ProFocusing
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_OpticalCenter_Opt stOption
// Parameter	: __in ST_OpticalCenter_Data stData
// Qualifier	:
// Last Update	: 2018/3/12 - 19:27
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_PreFocusing(__inout IplImage* lpImage, __in ST_OpticalCenter_Opt stOption, __in ST_OpticalCenter_Data stData)
{
	if (NULL == lpImage)
		return;

	int		iLineSize = 4;
	int		iMarkSize = 10;
	double dbFontSize = 1.3;

	CRect	rtRect = stOption.stRegion.rtROI;
	CPoint	ptCenter;

	CString szText;
	szText = _T("Detech the Optical Center!!");

	iLineSize  = 6;
	dbFontSize = 2.0;

	rtRect.left = lpImage->width / 2 - 370;
	rtRect.top  = 100;

	if (TR_Pass == stData.nDetectResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}

	rtRect	   = stOption.stRegion.rtROI;
	iLineSize  = 3;
	dbFontSize = 1.3;

	ptCenter.x = stData.nPixX;
	ptCenter.y = stData.nPixY;

	if (0 >= ptCenter.x || 0 >= ptCenter.y)
		return;

	rtRect.left		= ptCenter.x - iMarkSize;
	rtRect.top		= ptCenter.y;
	rtRect.right	= ptCenter.x + iMarkSize;
	rtRect.bottom	= ptCenter.y;
	Overlay_Process(lpImage, OvrMode_LINE, rtRect, RGB(0, 255, 255), iLineSize);

	rtRect.left		= ptCenter.x;
	rtRect.top		= ptCenter.y - iMarkSize;
	rtRect.right	= ptCenter.x;
	rtRect.bottom	= ptCenter.y + iMarkSize;
	Overlay_Process(lpImage, OvrMode_LINE, rtRect, RGB(0, 255, 255), iLineSize);

	szText.Format(_T("X : %d Y : %d"), stData.iStandPosDevX, stData.iStandPosDevY);

	rtRect.left = stOption.stRegion.rtROI.left;
	rtRect.top  = stOption.stRegion.rtROI.bottom + 60;

	iLineSize = 3;
	dbFontSize = 1.3;

	if (TR_Pass == stData.nDetectResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}

//=============================================================================
// Method		: Overlay_OpticalCenter
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_OpticalCenter_Opt stOption
// Parameter	: __in ST_OpticalCenter_Data stData
// Qualifier	:
// Last Update	: 2018/3/4 - 15:53
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_OpticalCenter(__inout IplImage* lpImage, __in ST_OpticalCenter_Opt stOption, __in ST_OpticalCenter_Data stData, __in enOverlayItem eOverItem/*NULL*/)
{
	if (NULL == lpImage)
		return;

	int iLineSize;
	int iMarkSize;
	double dbFontSize;

	if ((m_ModelType == Model_OMS_Entry) || (m_ModelType == Model_OMS_Front) || (m_ModelType == Model_OMS_Front_Set))
	{
		iLineSize = 2;
		iMarkSize = 5;
		dbFontSize = 0.6;
	}
	else
	{
		iLineSize = 4;
		iMarkSize = 10;
		dbFontSize = 1.3;
	}

	

	CRect	rtRect = stOption.stRegion.rtROI;

	Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);

	CPoint	ptCenter;

	ptCenter.x = stData.nPixX;
	ptCenter.y = stData.nPixY;

	if (0 >= ptCenter.x || 0 >= ptCenter.y)
		return;

	rtRect.left		= ptCenter.x - iMarkSize;
	rtRect.top		= ptCenter.y;
	rtRect.right	= ptCenter.x + iMarkSize;
	rtRect.bottom	= ptCenter.y;
	Overlay_Process(lpImage, OvrMode_LINE, rtRect, RGB(0, 255, 255), iLineSize);

	rtRect.left		= ptCenter.x;
	rtRect.top		= ptCenter.y - iMarkSize;
	rtRect.right	= ptCenter.x;
	rtRect.bottom	= ptCenter.y + iMarkSize;
	Overlay_Process(lpImage, OvrMode_LINE, rtRect, RGB(0, 255, 255), iLineSize);

	CString szText;
	szText.Format(_T("X : %d Y : %d"), stData.iStandPosDevX, stData.iStandPosDevY);
	// Active Align Overlay 하단부분 따로 출력 [2/14/2019 Seongho.Lee]
	if (Ovr_ActiveAlign == eOverItem)
	{
		rtRect.left = stOption.stRegion.rtROI.left - 200;
		rtRect.top = stOption.stRegion.rtROI.bottom + 260;
	}
	else
	{
		rtRect.left = stOption.stRegion.rtROI.left;
		rtRect.top = stOption.stRegion.rtROI.bottom + 60;
	}

	if (TR_Pass == stData.nResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}

//=============================================================================
// Method		: Overlay_Current
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_ECurrent_Opt stOption
// Parameter	: __in ST_ECurrent_Data stData
// Qualifier	:
// Last Update	: 2018/3/4 - 15:56
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_Current(__inout IplImage* lpImage, __in ST_ECurrent_Opt stOption, __in ST_ECurrent_Data stData)
{
	if (NULL == lpImage)
		return;

	int iLineSize;
	int iMarkSize;
	double dbFontSize;

	CRect rtRect;
	CString szText;
	rtRect.SetRectEmpty();

	if ((m_ModelType == Model_OMS_Entry) || (m_ModelType == Model_OMS_Front) || (m_ModelType == Model_OMS_Front_Set))
	{
		iLineSize = 2;
		iMarkSize = 5;
		dbFontSize = 0.6;
		rtRect.left = 10;
		szText.Format(_T("1.8V : %.1f 3.3V : %.1f 9.0V : %.1f 14.7V : %.1f -5.7V : %.1f"), stData.dbValue[Spec_ECurrent_CH1], stData.dbValue[Spec_ECurrent_CH2], stData.dbValue[Spec_ECurrent_CH3], stData.dbValue[Spec_ECurrent_CH4], stData.dbValue[Spec_ECurrent_CH5]);
	}
	else
	{
		iLineSize = 4;
		iMarkSize = 10;
		dbFontSize = 1.2;
		rtRect.left = lpImage->width / 2 - 200;
		szText.Format(_T("1.8V : %.1f 2.8V : %.1f"), stData.dbValue[Spec_ECurrent_CH1], stData.dbValue[Spec_ECurrent_CH2]);
	}


	rtRect.top  = lpImage->height / 3 * 2;


	if (TR_Pass == stData.nResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}

//=============================================================================
// Method		: Overlay_FOV
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_FOV_Opt stOption
// Parameter	: __in ST_FOV_Data stData
// Qualifier	:
// Last Update	: 2018/3/4 - 16:01
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_FOV(__inout IplImage* lpImage, __in ST_FOV_Opt stOption, __in ST_FOV_Data stData)
{
	if (NULL == lpImage)
	return;


	int iLineSize;
	int iMarkSize;
	double dbFontSize;

	if ((m_ModelType == Model_OMS_Entry) || (m_ModelType == Model_OMS_Front) || (m_ModelType == Model_OMS_Front_Set))
	{
		iLineSize = 2;
		iMarkSize = 5;
		dbFontSize = 0.6;
	}
	else
	{
		iLineSize = 4;
		iMarkSize = 10;
		dbFontSize = 1.2;
	}

	CRect	rtRect;
	CPoint	ptCenter;
	CString szText;

	for (UINT nIdx = 0; nIdx < ROI_Fov_Max; nIdx++)
	{
		if (m_bTestmode == FALSE)
		{
			rtRect = stOption.stRegion[nIdx].rtROI;
		}
		else{
			rtRect = stData.rtTestPicROI[nIdx];

		}
		Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);

		if (m_bTestmode == FALSE)
		{
			rtRect.left = stOption.stRegion[nIdx].rtROI.left;

			if (stOption.stRegion[nIdx].rtROI.top < 100)
				rtRect.top = stOption.stRegion[nIdx].rtROI.bottom + 30;
			else
				rtRect.top = stOption.stRegion[nIdx].rtROI.top - 10;
		}
		else{
			rtRect.left = stData.rtTestPicROI[nIdx].left;

			if (stData.rtTestPicROI[nIdx].top < 100)
				rtRect.top = stData.rtTestPicROI[nIdx].bottom + 30;
			else
				rtRect.top = stData.rtTestPicROI[nIdx].top - 10;
		}	

		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 200, 0), iLineSize, dbFontSize, g_szROI_Fov[nIdx]);
	}

	for (UINT nIdx = 0; nIdx < ROI_Fov_Max; nIdx++)
	{
		ptCenter  = stData.ptCenter[nIdx];

		rtRect.left		= ptCenter.x - iMarkSize;
		rtRect.top		= ptCenter.y;
		rtRect.right	= ptCenter.x + iMarkSize;
		rtRect.bottom	= ptCenter.y;
		Overlay_Process(lpImage, OvrMode_LINE, rtRect, RGB(0, 255, 255), iLineSize);

		rtRect.left		= ptCenter.x;
		rtRect.top		= ptCenter.y - iMarkSize;
		rtRect.right	= ptCenter.x;
		rtRect.bottom	= ptCenter.y + iMarkSize;
		Overlay_Process(lpImage, OvrMode_LINE, rtRect, RGB(0, 255, 255), iLineSize);							
	}

	ptCenter		= stData.ptCenter[ROI_Fov_CT];
	rtRect.left		= ptCenter.x;
	rtRect.top		= ptCenter.y;

	ptCenter		= stData.ptCenter[ROI_Fov_CB];
	rtRect.right	= ptCenter.x;
	rtRect.bottom	= ptCenter.y;

	Overlay_Process(lpImage, OvrMode_LINE, rtRect, RGB(29, 219, 22), iLineSize);

	ptCenter		= stData.ptCenter[ROI_Fov_CL];
	rtRect.left		= ptCenter.x;
	rtRect.top		= ptCenter.y;

	ptCenter		= stData.ptCenter[ROI_Fov_CR];
	rtRect.right	= ptCenter.x;
	rtRect.bottom	= ptCenter.y;

	Overlay_Process(lpImage, OvrMode_LINE, rtRect, RGB(29, 219, 22), iLineSize);

	szText.Format(_T("Hor : %.2f / Ver : %.2f "), stData.dbValue[Spec_Fov_Hor], stData.dbValue[Spec_Fov_Ver]);

	rtRect.left		= stOption.stRegion[ROI_Fov_CC].rtROI.left;
	rtRect.top		= (stOption.stRegion[ROI_Fov_CC].rtROI.bottom + stOption.stRegion[ROI_Fov_CB].rtROI.top) / 2;
	

	if (TR_Pass == stData.nResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}

//=============================================================================
// Method		: Overlay_SFR
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_SFR_Opt stOption
// Parameter	: __in ST_SFR_Data stData
// Qualifier	:
// Last Update	: 2018/3/4 - 16:02
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_SFR(__inout IplImage* lpImage, __in ST_SFR_Opt stOption, __in ST_SFR_Data stData)
{
	if (NULL == lpImage)
		return;

	int iLineSize;
	int iMarkSize;
	double dbFontSize;

	if ((m_ModelType == Model_OMS_Entry) || (m_ModelType == Model_OMS_Front) || (m_ModelType == Model_OMS_Front_Set))
	{
		iLineSize = 2;
		iMarkSize = 5;
		dbFontSize = 0.6;
	}
	else
	{
		//iLineSize = 3;
		//iMarkSize = 10;
		//dbFontSize = 1.2;

		iLineSize = 2;
		iMarkSize = 5;
		dbFontSize = 0.6;
	}

	CRect	rtRect;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	for (UINT nIdx = 0; nIdx < ROI_SFR_Max; nIdx++)
	{

		if (TRUE == stOption.stRegion[nIdx].bEnable)
		{
			if (m_bTestmode == TRUE)
				rtRect = stData.rtTestPicROI[nIdx];
			else
				rtRect = stOption.stRegion[nIdx].rtROI;

			Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);

			rtRectText.SetRectEmpty();

			switch (stOption.stRegion[nIdx].nItemPos)
			{
			case IPos_Left:
				rtRectText.left = rtRect.left - 70;
				rtRectText.top  = (rtRect.top + rtRect.bottom) / 2;
				break;
			case IPos_Right:
				rtRectText.left = rtRect.right + 10;
				rtRectText.top  = (rtRect.top + rtRect.bottom) / 2;
				break;
			case IPos_Top:
				rtRectText.left = rtRect.left;
				rtRectText.top  = rtRect.top - 40;
				break;
			case IPos_Bottom:
				rtRectText.left = rtRect.left;
				rtRectText.top  = rtRect.bottom + 40;
				break;
			default:
				break;
			}

			szText.Format(_T("%d"), nIdx);
			Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 200, 0), iLineSize, dbFontSize, szText);

			rtRectText.top += 30;
			szText.Format(_T("%.2f"), stData.dbValue[nIdx]);

			if (TR_Pass == stData.nEachResult[nIdx])
			{
				Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
			}
			else
			{
				Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
			}
		}
	}
}

//=============================================================================
// Method		: Overlay_SFR_Group
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_SFR_Opt stOption
// Parameter	: __in ST_SFR_Data stData
// Parameter	: __in UINT nGroupCnt
// Qualifier	:
// Last Update	: 2018/6/3 - 17:37
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_SFR_Group(__inout IplImage* lpImage, __in ST_SFR_Opt stOption, __in ST_SFR_Data stData, __in UINT nGroupCnt)
{
	if (NULL == lpImage)
		return;

	int iLineSize;
	int iMarkSize;
	double dbFontSize;

	CRect rtRect;
	CString szText;
	rtRect.SetRectEmpty();

	iLineSize = 4;
	iMarkSize = 10;
	dbFontSize = 1.2;
	rtRect.left = lpImage->width / 2 - 200;
	//szText.Format(_T("Group %d Min SFR : %.2f"), nGroupCnt, stData.dbMinValue[nGroupCnt]);

	rtRect.top = lpImage->height / 3 * 2;

	if (TR_Pass == stData.nResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}

void COverlay_Proc::Overlay_SFR_Group(__inout IplImage* lpImage, __in ST_SFR_Opt stOption, __in ST_SFR_Data stData)
{
	int iLineSize;
	int iMarkSize;
	double dbFontSize;

	if ((m_ModelType == Model_OMS_Entry) || (m_ModelType == Model_OMS_Front) || (m_ModelType == Model_OMS_Front_Set))
	{
		iLineSize = 2;
		iMarkSize = 5;
		dbFontSize = 0.6;
	}
	else
	{
		iLineSize = 3;
		iMarkSize = 10;
		dbFontSize = 1.2;
	}
	//{ CRect(637, 647, 553, 563), CRect(473, 483, 97, 107), CRect(473, 483, 527, 537), CRect(774, 784, 97, 107), CRect(774, 784, 527, 537),
	CRect	rtRect[ROI_SFR_GROUP_Max] = { CRect(650, 700, 660, 710)/*CT*/, CRect(450, 150, 490, 160)/*LT0.5*/, CRect(450, 810, 490, 820)/*LB0.5*/, CRect(850, 150, 810, 160)/*RT0.5*/, CRect(850, 810, 810, 820)/*RB0.5*/,
		CRect(94, 150, 104, 160)/*LT0.7*/, CRect(94, 810, 104, 820)/*LB0.7*/, CRect(1190, 150, 1290, 160)/*RT0.7*/, CRect(1190, 810, 1290, 820) /*RB0.7*/};
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	for (UINT nIdx = 0; nIdx < ROI_SFR_GROUP_Max; nIdx++)
	{
		if (TRUE == stOption.stInput_Group[nIdx].bEnable)
		{
			//Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect[nIdx], RGB(255, 200, 0), iLineSize);

			rtRectText.SetRectEmpty();

			rtRectText.left = rtRect[nIdx].left - 70;
			rtRectText.top = (rtRect[nIdx].top + rtRect[nIdx].bottom) / 2;

			//szText.Format(_T("%d"), nIdx);
			szText = g_szSFRGroup[nIdx];
			Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(157, 240, 96), iLineSize, dbFontSize, szText);

			rtRectText.top += 35;
			szText.Format(_T("%.2f"), stData.dbEachResultGroup[nIdx]);

			if (TR_Pass == stData.nEachResult_G[nIdx])
			{
				Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
			}
			else
			{
				Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
			}
		}
	}
}
//=============================================================================
// Method		: Overlay_SFR_Tilt
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_SFR_Opt stOption
// Parameter	: __in ST_SFR_Data stData
// Parameter	: __in UINT nGroupCnt
// Qualifier	:
// Last Update	: 2018/6/3 - 17:37
// Desc.		:
//=============================================================================
//void COverlay_Proc::Overlay_SFR_Tilt(__inout IplImage* lpImage, __in ST_SFR_Opt stOption, __in ST_SFR_Data stData)
//{
//	if (NULL == lpImage)
//		return;
//
//	int iLineSize;
//	int iMarkSize;
//	double dbFontSize;
//
//	CRect rtRect;
//	CString szText;
//	rtRect.SetRectEmpty();
//
//	iLineSize = 4;
//	iMarkSize = 10;
//	dbFontSize = 1.2;
//	rtRect.left = lpImage->width / 2 - 500;//400;
//	szText.Format(_T("X1Tilt : %.2f, X2Tilt : %.2f, Y1Tilt : %.2f, Y2Tilt : %.2f"), stData.dbTiltdata[ITM_SFR_X1_Tilt_UpperLimit], stData.dbTiltdata[ITM_SFR_X2_Tilt_UpperLimit], stData.dbTiltdata[ITM_SFR_Y1_Tilt_UpperLimit], stData.dbTiltdata[ITM_SFR_Y2_Tilt_UpperLimit]);
//
//	rtRect.top = lpImage->height - 20;//250;
//
//	if (TR_Pass == stData.nTiltResult) //Tilt 값에 따라 색 띄우기
//	{
//		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
//	}
//	else
//	{
//		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
//	}
//}

void COverlay_Proc::Overlay_SFR_Tilt(__inout IplImage* lpImage, __in ST_SFR_Opt stOption, __in ST_SFR_Data stData)
{
	int iLineSize;
	int iMarkSize;
	double dbFontSize;

	if ((m_ModelType == Model_OMS_Entry) || (m_ModelType == Model_OMS_Front) || (m_ModelType == Model_OMS_Front_Set))
	{
		iLineSize = 2;
		iMarkSize = 5;
		dbFontSize = 0.6;
	}
	else
	{
		iLineSize = 3;
		iMarkSize = 10;
		dbFontSize = 1.2;
	}
	//{ CRect(637, 647, 553, 563), CRect(473, 483, 97, 107), CRect(473, 483, 527, 537), CRect(774, 784, 97, 107), CRect(774, 784, 527, 537),
	CRect	rtRect[ROI_SFR_TILT_Max] = { CRect(220, 260, 230, 270)/*Tilt0*/, CRect(1080, 260, 1090, 270)/*Tilt1*/, CRect(220, 700, 230, 710)/*Tilt2*/, CRect(1080, 700, 1090, 710)/*Tilt3*/ };
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	for (UINT nIdx = 0; nIdx < ROI_SFR_TILT_Max; nIdx++)
	{
		if (TRUE == stOption.stInput_Tilt[nIdx].bEnable)
		{
			//Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect[nIdx], RGB(255, 200, 0), iLineSize);

			rtRectText.SetRectEmpty();

			rtRectText.left = rtRect[nIdx].left - 70;
			rtRectText.top = (rtRect[nIdx].top + rtRect[nIdx].bottom) / 2;

			szText.Format(_T("Tilt %d"), nIdx);
			//szText = g_szSFRGroup[nIdx];
			Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(94, 198, 230), iLineSize, dbFontSize, szText);

			rtRectText.top += 30;
			szText.Format(_T("%.2f"), stData.dbTiltdata[nIdx]);

			if (TR_Pass == stData.nEachTiltResult[nIdx])
			{
				Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
			}
			else
			{
				Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
			}
		}
	}
}

//=============================================================================
// Method		: Overlay_Distortion
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_Distortion_Opt stOption
// Parameter	: __in ST_Distortion_Data stData
// Qualifier	:
// Last Update	: 2018/3/4 - 16:03
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_Distortion(__inout IplImage* lpImage, __in ST_Distortion_Opt stOption, __in ST_Distortion_Data stData)
{
	if (NULL == lpImage)
		return;

	int iLineSize;
	int iMarkSize;
	double dbFontSize;

	if ((m_ModelType == Model_OMS_Entry) || (m_ModelType == Model_OMS_Front) || (m_ModelType == Model_OMS_Front_Set))
	{
		iLineSize = 2;
		iMarkSize = 5;
		dbFontSize = 0.6;
	}
	else
	{
		iLineSize = 3;
		iMarkSize = 10;
		dbFontSize = 1.3;
	}

	CRect	rtRect;
	CPoint	ptCenter;

	CString szText;

	for (UINT nIdx = 0; nIdx < ROI_Dis_Max; nIdx++)
	{
		if (m_bTestmode == FALSE){
			rtRect = stOption.stRegion[nIdx].rtROI;
		}
		else{
			rtRect = stData.rtTestPicROI[nIdx];
		}
	
		Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);
		
		if (m_bTestmode == FALSE){
			rtRect.left = stOption.stRegion[nIdx].rtROI.left;

			if (stOption.stRegion[nIdx].rtROI.top < 100)
				rtRect.top = stOption.stRegion[nIdx].rtROI.bottom + 30;
			else
				rtRect.top = stOption.stRegion[nIdx].rtROI.top - 10;
		}
		else{
			rtRect.left = stData.rtTestPicROI[nIdx].left;

			if (stData.rtTestPicROI[nIdx].top < 100)
				rtRect.top = stData.rtTestPicROI[nIdx].bottom + 30;
			else
				rtRect.top = stData.rtTestPicROI[nIdx].top - 10;
		}	

		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 200, 0), iLineSize, dbFontSize, g_szROI_Distortion[nIdx]);
	}

	for (UINT nIdx = 0; nIdx < ROI_Dis_Max; nIdx++)
	{
		ptCenter = stData.ptCenter[nIdx];

		rtRect.left		= ptCenter.x - iMarkSize;
		rtRect.top		= ptCenter.y;
		rtRect.right	= ptCenter.x + iMarkSize;
		rtRect.bottom	= ptCenter.y;
		Overlay_Process(lpImage, OvrMode_LINE, rtRect, RGB(0, 255, 255), iLineSize);

		rtRect.left		= ptCenter.x;
		rtRect.top		= ptCenter.y - iMarkSize;
		rtRect.right	= ptCenter.x;
		rtRect.bottom	= ptCenter.y + iMarkSize;
		Overlay_Process(lpImage, OvrMode_LINE, rtRect, RGB(0, 255, 255), iLineSize);
	}

 	szText.Format(_T("%.2f%%"), stData.dbValue);
 
 	rtRect.left		= stOption.stRegion[ROI_Dis_CT].rtROI.left;
 	rtRect.top		= stOption.stRegion[ROI_Dis_CB].rtROI.top - 100;	
 
	if (TR_Pass == stData.nResult)
 	{
 		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
 	}
 	else
 	{
 		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
 	}
}

//=============================================================================
// Method		: Overlay_Rotation
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_Rotate_Opt stOption
// Parameter	: __in ST_Rotate_Data stData
// Qualifier	:
// Last Update	: 2018/3/4 - 16:04
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_Rotation(__inout IplImage* lpImage, __in ST_Rotate_Opt stOption, __in ST_Rotate_Data stData, __in enOverlayItem eOverItem)
{
	if (NULL == lpImage)
		return;

	int iLineSize;
	int iMarkSize;
	double dbFontSize;

	if ((m_ModelType == Model_OMS_Entry) || (m_ModelType == Model_OMS_Front) || (m_ModelType == Model_OMS_Front_Set))
	{
		iLineSize = 2;
		iMarkSize = 5;
		dbFontSize = 0.6;
	}
	else
	{
		iLineSize = 4;
		iMarkSize = 10;
		dbFontSize = 1.3;
	}

	CRect	rtRect;
	CPoint	ptCenter;
	CString szText;

	for (UINT nIdx = 0; nIdx < ROI_Rot_Max; nIdx++)
	{
		if (m_bTestmode == FALSE){
			rtRect = stOption.stRegion[nIdx].rtROI;
		}
		else{
			rtRect = stData.rtTestPicROI[nIdx];
		}
		Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);

		if (m_bTestmode == FALSE){
			rtRect.left = stOption.stRegion[nIdx].rtROI.left;

			if (stOption.stRegion[nIdx].rtROI.top < 100)
				rtRect.top = stOption.stRegion[nIdx].rtROI.bottom + 30;
			else
				rtRect.top = stOption.stRegion[nIdx].rtROI.top - 10;
		}
		else{
			rtRect.left = stData.rtTestPicROI[nIdx].left;

			if (stData.rtTestPicROI[nIdx].top < 100)
				rtRect.top = stData.rtTestPicROI[nIdx].bottom + 30;
			else
				rtRect.top = stData.rtTestPicROI[nIdx].top - 10;
		}

		
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 200, 0), iLineSize, dbFontSize, g_szROI_Rotate[nIdx]);
	}
	
	for (UINT nIdx = 0; nIdx < ROI_Rot_Max; nIdx++)
	{
		CPoint	ptCenter;

		ptCenter = stData.ptCenter[nIdx];

		rtRect.left		= ptCenter.x - iMarkSize;
		rtRect.top		= ptCenter.y;
		rtRect.right	= ptCenter.x + iMarkSize;
		rtRect.bottom	= ptCenter.y;
		Overlay_Process(lpImage, OvrMode_LINE, rtRect, RGB(0, 255, 255), iLineSize);

		rtRect.left		= ptCenter.x;
		rtRect.top		= ptCenter.y - iMarkSize;
		rtRect.right	= ptCenter.x;
		rtRect.bottom	= ptCenter.y + iMarkSize;
		Overlay_Process(lpImage, OvrMode_LINE, rtRect, RGB(0, 255, 255), iLineSize);
	}

	ptCenter		= stData.ptCenter[ROI_Rot_LT];
	rtRect.left		= ptCenter.x;
	rtRect.top		= ptCenter.y;

	ptCenter		= stData.ptCenter[ROI_Rot_RT];
	rtRect.right	= ptCenter.x;
	rtRect.bottom	= ptCenter.y;

	Overlay_Process(lpImage, OvrMode_LINE, rtRect, RGB(29, 219, 22), iLineSize);

	ptCenter		= stData.ptCenter[ROI_Rot_LT];
	rtRect.left		= ptCenter.x;
	rtRect.top		= ptCenter.y;

	ptCenter		= stData.ptCenter[ROI_Rot_LB];
	rtRect.right	= ptCenter.x;
	rtRect.bottom	= ptCenter.y;

	Overlay_Process(lpImage, OvrMode_LINE, rtRect, RGB(29, 219, 22), iLineSize);

	ptCenter		= stData.ptCenter[ROI_Rot_RT];
	rtRect.left		= ptCenter.x;
	rtRect.top		= ptCenter.y;

	ptCenter		= stData.ptCenter[ROI_Rot_RB];
	rtRect.right	= ptCenter.x;
	rtRect.bottom	= ptCenter.y;

	Overlay_Process(lpImage, OvrMode_LINE, rtRect, RGB(29, 219, 22), iLineSize);

	ptCenter		= stData.ptCenter[ROI_Rot_LB];
	rtRect.left		= ptCenter.x;
	rtRect.top		= ptCenter.y;

	ptCenter		= stData.ptCenter[ROI_Rot_RB];
	rtRect.right	= ptCenter.x;
	rtRect.bottom	= ptCenter.y;

	Overlay_Process(lpImage, OvrMode_LINE, rtRect, RGB(29, 219, 22), iLineSize);
	
	szText.Format(_T("%.2f"), stData.dbValue);
 
	if (Ovr_ActiveAlign == eOverItem)
	{
		rtRect.left = stOption.stRegion[ROI_Rot_LT].rtROI.left + 750;
		rtRect.top = stOption.stRegion[ROI_Rot_LB].rtROI.bottom + 21;
	}
	else
	{
		rtRect.left = (stOption.stRegion[ROI_Rot_LT].rtROI.right + stOption.stRegion[ROI_Rot_RT].rtROI.left) / 2;
		rtRect.top = stOption.stRegion[ROI_Rot_LB].rtROI.top - 50;
	}


	if (TR_Pass == stData.nResult)
 	{
 		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
 	}
 	else
 	{
 		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
 	}
}

//=============================================================================
// Method		: Overlay_Particle
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_Particle_Opt stOption
// Parameter	: __in ST_Particle_Data stData
// Qualifier	:
// Last Update	: 2018/3/4 - 16:06
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_Particle(__inout IplImage* lpImage, __in ST_Particle_Opt stOption, __in ST_Particle_Data stData)
{
	if (NULL == lpImage)
		return;

	int iLineSize;
	int iMarkSize;
	double dbFontSize;

	if ((m_ModelType == Model_OMS_Entry) || (m_ModelType == Model_OMS_Front) || (m_ModelType == Model_OMS_Front_Set))
	{
		iLineSize = 2;
		iMarkSize = 5;
		dbFontSize = 0.6;
	}
	else
	{
		iLineSize = 3;
		iMarkSize = 10;
		dbFontSize = 1.2;
	}

	CRect			rtRect;
	CPoint			ptCenter;
	CString			szText;
	enOverlayMode	enMode;

	enMode = OvrMode_LINE;

	// Line 1 : 위
	rtRect.left = 0;
	rtRect.top = stOption.iEdgeH;
	rtRect.right = lpImage->width;
	rtRect.bottom = stOption.iEdgeH;
	Overlay_Process(lpImage, enMode, rtRect, RGB(255, 200, 0), iLineSize);


	// Line 2 : 아래
	rtRect.left = 0;
	rtRect.top = lpImage->height - stOption.iEdgeH;
	rtRect.right = lpImage->width;
	rtRect.bottom = lpImage->height - stOption.iEdgeH;
	Overlay_Process(lpImage, enMode, rtRect, RGB(255, 200, 0), iLineSize);

	// Line 3 : 좌
	rtRect.left = stOption.iEdgeW;
	rtRect.top = 0;
	rtRect.right = stOption.iEdgeW;
	rtRect.bottom = lpImage->height;
	Overlay_Process(lpImage, enMode, rtRect, RGB(255, 200, 0), iLineSize);

	// Line 4 : 우
	rtRect.left = lpImage->width - stOption.iEdgeW;
	rtRect.top = 0;
	rtRect.right = lpImage->width - stOption.iEdgeW;
	rtRect.bottom = lpImage->height;
	Overlay_Process(lpImage, enMode, rtRect, RGB(255, 200, 0), iLineSize);

	// 불량 픽미픽미픽미업

	for (UINT nCnt = 0; nCnt < stData.nFailCount; nCnt++)
	{
		rtRect = stData.rtFailROI[nCnt];
		Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 100, 0), iLineSize);

// 		rtRect.left = stData.rtFailROI[nCnt].left + 5;
// 
// 		if (stData.rtFailROI[nCnt].top < 100)
// 			rtRect.top = stData.rtFailROI[nCnt].top + 30;
// 		else
// 			rtRect.top = stData.rtFailROI[nCnt].top - 15;
// 
// 		iLineSize	= 3;
// 		dbFontSize = 1.0;
// 		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 100, 0), iLineSize, dbFontSize, g_szParticle_Type[0]);
	}
}

//=============================================================================
// Method		: Overlay_Particle_SNR
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_Dynamic_Opt stOption
// Parameter	: __in ST_Dynamic_Data stData
// Qualifier	:
// Last Update	: 2018/3/4 - 16:06
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_Particle_SNR(__inout IplImage* lpImage, __in ST_Dynamic_Opt stOption, __in ST_Dynamic_Data stData)
{
	if (NULL == lpImage)
		return;
	
	int iLineSize;
	int iMarkSize;
	double dbFontSize;

	if ((m_ModelType == Model_OMS_Entry) || (m_ModelType == Model_OMS_Front) || (m_ModelType == Model_OMS_Front_Set))
	{
		iLineSize = 2;
		iMarkSize = 5;
		dbFontSize = 0.6;
	}
	else
	{
		iLineSize = 2;
		iMarkSize = 10;
		dbFontSize = 0.7;
	}

	CRect	rtRect;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;
	
	for (UINT nIdx = 0; nIdx < ROI_Par_SNR_Max; nIdx++)
	{
		if (TRUE == stOption.stRegion[nIdx].bEnable)
		{
			rtRect = stOption.stRegion[nIdx].rtROI;
			Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);

			rtRectText.SetRectEmpty();

			switch (stOption.stRegion[nIdx].nItemPos)
			{
			case IPos_Left:
				rtRectText.left = rtRect.left - 165;
				rtRectText.top = (rtRect.top + rtRect.bottom) / 2;
				break;
			case IPos_Right:
				rtRectText.left = rtRect.right + 10;
				rtRectText.top = (rtRect.top + rtRect.bottom) / 2;
				break;
			case IPos_Top:
				rtRectText.left = rtRect.left;
				rtRectText.top = rtRect.top - 80;
				break;
			case IPos_Bottom:
				rtRectText.left = rtRect.left;
				rtRectText.top = rtRect.bottom + 30;
				break;
			default:
				break;
			}

			szText.Format(_T("%d"), nIdx);
			Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 200, 0), iLineSize, dbFontSize, szText);

			szText.Format(_T("Dynamic : %.1f"), stData.dbValueDR[nIdx]);

			rtRectText.top  += 30;

			if (TR_Pass == stData.nEtcResultDR[nIdx])
			{
				Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
			}
			else
			{
				Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
			}

			szText.Format(_T("SNR BW : %.1f"), stData.dbValueBW[nIdx]);

			rtRectText.top  += 30;

			if (TR_Pass == stData.nEtcResultBW[nIdx])
			{
				Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
			}
			else
			{
				Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
			}
		}
	}

}

//=============================================================================
// Method		: Overlay_DefectPixel
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_DefectPixel_Opt stOption
// Parameter	: __in ST_DefectPixel_Data stData
// Qualifier	:
// Last Update	: 2018/3/4 - 16:07
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_DefectPixel(__inout IplImage* lpImage, __in ST_DefectPixel_Opt stOption, __in ST_DefectPixel_Data stData)
{
	if (NULL == lpImage)
		return;
	
	int iLineSize;
	int iMarkSize;
	double dbFontSize;

	if ((m_ModelType == Model_OMS_Entry) || (m_ModelType == Model_OMS_Front) || (m_ModelType == Model_OMS_Front_Set))
	{
		iLineSize = 2;
		iMarkSize = 5;
		dbFontSize = 0.6;
	}
	else
	{
		iLineSize = 2;
		iMarkSize = 10;
		dbFontSize = 0.7;
	}

	CRect	rtRect;
	CPoint	ptCenter;
	CString szText;
	
	for (UINT nIdx = 0; nIdx < Spec_Defect_Max; nIdx++)
	{

		for (UINT nCnt = 0; nCnt < stData.nFailCount[nIdx]; nCnt++)
		{
			if (stData.nFailType[nIdx][nCnt] == Defect_Type_RowCol)
			{
				rtRect = stData.rtFailROI[nIdx][nCnt];
				Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 100, 0), iLineSize);
			}
			else
			{
				rtRect = stData.rtFailROI[nIdx][nCnt];
				Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 100, 0), iLineSize);
			}

			rtRect.left = stData.rtFailROI[nIdx][nCnt].left + 5;

			if (stData.rtFailROI[nIdx][nCnt].top < 100)
				rtRect.top = stData.rtFailROI[nIdx][nCnt].top + 30;
			else
				rtRect.top = stData.rtFailROI[nIdx][nCnt].top - 15;

			Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 100, 0), iLineSize, dbFontSize, g_szDefectPixel_Type[stData.nFailType[nIdx][nCnt]]);
		}
	}
}

//=============================================================================
// Method		: Overlay_Intencity
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_Intensity_Opt stOption
// Parameter	: __in ST_Intensity_Data stData
// Qualifier	:
// Last Update	: 2018/3/4 - 16:07
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_Intencity(__inout IplImage* lpImage, __in ST_Intensity_Opt stOption, __in ST_Intensity_Data stData)
{
	if (NULL == lpImage)
		return;

	int iLineSize;
	int iMarkSize;
	double dbFontSize;

	if ((m_ModelType == Model_OMS_Entry) || (m_ModelType == Model_OMS_Front) || (m_ModelType == Model_OMS_Front_Set))
	{
		iLineSize = 2;
		iMarkSize = 5;
		dbFontSize = 0.6;
	}
	else
	{
		iLineSize = 3;
		iMarkSize = 10;
		dbFontSize = 0.8;
	}

	CRect	rtRect;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	for (UINT nIdx = 0; nIdx < ROI_Intencity_Max; nIdx++)
	{

		if (TRUE == stOption.stRegion[nIdx].bEnable)
		{
			rtRect = stOption.stRegion[nIdx].rtROI;

			Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);

			rtRectText.SetRectEmpty();

			switch (stOption.stRegion[nIdx].nItemPos)
			{
			case IPos_Left:
				rtRectText.left = rtRect.left - 120;
				rtRectText.top  = (rtRect.top + rtRect.bottom) / 2;
				break;
			case IPos_Right:
				rtRectText.left = rtRect.right + 10;
				rtRectText.top  = (rtRect.top + rtRect.bottom) / 2;
				break;
			case IPos_Top:
				rtRectText.left = rtRect.left;
				rtRectText.top  = rtRect.top - 40;
				break;
			case IPos_Bottom:
				rtRectText.left = rtRect.left;
				rtRectText.top  = rtRect.bottom + 40;
				break;
			default:
				break;
			}

			Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 200, 0), iLineSize, dbFontSize, g_szROI_Intencity[nIdx]);

			if (ROI_Intencity_CC == nIdx)
				szText.Format(_T("%.2f"), stData.fSignal[nIdx]);
			else
				szText.Format(_T("%.2f%%"), stData.fPercent[nIdx]);

			rtRectText.top += 30;

			if (TR_Pass == stData.nEachResult[nIdx])
			{
				Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
			}
			else
			{
				Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
			}
		}

	}
}

//=============================================================================
// Method		: Overlay_SNR_Light
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_SNR_Light_Opt stOption
// Parameter	: __in ST_SNR_Light_Data stData
// Qualifier	:
// Last Update	: 2018/3/4 - 16:08
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_SNR_Light(__inout IplImage* lpImage, __in ST_SNR_Light_Opt stOption, __in ST_SNR_Light_Data stData)
{
	if (NULL == lpImage)
		return;

	int iLineSize;
	int iMarkSize;
	double dbFontSize;

	if ((m_ModelType == Model_OMS_Entry) || (m_ModelType == Model_OMS_Front) || (m_ModelType == Model_OMS_Front_Set))
	{
		iLineSize  = 2;
		iMarkSize  = 5;
		dbFontSize = 0.6;
	}
	else
	{
		iLineSize  = 3;
		iMarkSize  = 10;
		dbFontSize = 1.2;
	}

	CRect rtRect;
	CString szText;

	szText.Format(_T("%.2f[dB]"), stData.dbFinalValue);

	rtRect.SetRectEmpty();

	rtRect.left = lpImage->width / 2;
	rtRect.top = lpImage->height / 2;

	if (TR_Pass == stData.nResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}

	

	for (UINT nIdx = 0; nIdx < ROI_SNR_Light_Max; nIdx++)
	{
		if (TRUE == stOption.bIndex[nIdx])
		{
			rtRect = stOption.st_SNR_LightRect.rt_TestSectionFullRect[nIdx];
			
			Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);

			//수치
			if (m_bTestmode == TRUE){
				 
				if (stData.nFinalIndx == nIdx)
				{
					if (stData.nResult)
					{
						Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(0, 84, 255), iLineSize);
					}
					else{
						Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 0, 0), iLineSize);

					}
				}
				szText.Format(_T("%.2f"), stData.dbEtcValue[nIdx]);

				rtRect.SetRectEmpty();

				rtRect.left = stOption.st_SNR_LightRect.rt_TestSectionFullRect[nIdx].left;
				rtRect.top = stOption.st_SNR_LightRect.rt_TestSectionFullRect[nIdx].top + stOption.st_SNR_LightRect.rt_TestSectionFullRect[nIdx].Height() / 2;

				double dbFontSize = 0.6;

				Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(86, 86, 86), iLineSize, dbFontSize, szText);
			}

		}
	}
}

//=============================================================================
// Method		: Overlay_SNR_Shading
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_Shading_Opt stOption
// Parameter	: __in ST_Shading_Data stData
// Qualifier	:
// Last Update	: 2018/5/12 - 17:27
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_SNR_Shading(__inout IplImage* lpImage, __in ST_Shading_Opt stOption, __in ST_Shading_Data stData)
{
	if (NULL == lpImage)
		return;

	int iLineSize;
	int iMarkSize;
	double dbFontSize;

	if ((m_ModelType == Model_OMS_Entry) || (m_ModelType == Model_OMS_Front) || (m_ModelType == Model_OMS_Front_Set))
	{
		iLineSize = 2;
		iMarkSize = 5;
		dbFontSize = 0.6;
	}
	else
	{
		iLineSize  = 3;
		iMarkSize  = 10;
		dbFontSize = 1.2;
	}


	CRect rtRect;
	CString szText;

	//szText.Format(_T("%.2f[dB]"), stData.dbFinalValue);

	rtRect.SetRectEmpty();

	rtRect.left = lpImage->width / 2;
	rtRect.top = lpImage->height / 2;

// 	if (TR_Pass == stData.nResult)
// 	{
// 		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
// 	}
// 	else
// 	{
// 		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
// 	}



	for (UINT nIdx = 0; nIdx < ROI_Shading_Max; nIdx++)
	{
		if (TRUE ==  stOption.bUseIndex[nIdx])
		{
			rtRect = stOption.stShading_Rect.rt_TestSectionFullRect[nIdx];

			if (Type_Shading_Standard == stOption.nType[nIdx])
			{
				Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(0, 200, 0), iLineSize);
			}
			else{
				Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);

			}

			//수치
			if (m_bTestmode == TRUE){

				if (stData.nEachResult[nIdx] == FALSE)
				{
						Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 0, 0), iLineSize);
				}
				szText.Format(_T("%.2f"), stData.fPercent[nIdx]);

				rtRect.SetRectEmpty();

				rtRect.left = stOption.stShading_Rect.rt_TestSectionFullRect[nIdx].left;
				rtRect.top = stOption.stShading_Rect.rt_TestSectionFullRect[nIdx].top + stOption.stShading_Rect.rt_TestSectionFullRect[nIdx].Height() / 2;

				double dbFontSize = 0.6;

				Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(86, 86, 86), iLineSize, dbFontSize, szText);
			}

		}
	}
}

//=============================================================================
// Method		: Overlay_HotPixel
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_HotPixel_Opt stOption
// Parameter	: __in ST_HotPixel_Data stData
// Qualifier	:
// Last Update	: 2018/5/12 - 17:24
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_HotPixel(__inout IplImage* lpImage, __in ST_HotPixel_Opt stOption, __in ST_HotPixel_Data stData)
{
	if (NULL == lpImage)
		return;

	int iLineSize;
	int iMarkSize;
	double dbFontSize;

	if ((m_ModelType == Model_OMS_Entry) || (m_ModelType == Model_OMS_Front) || (m_ModelType == Model_OMS_Front_Set))
	{
		iLineSize = 2;
		iMarkSize = 5;
		dbFontSize = 0.6;
	}
	else
	{
		iLineSize = 2;
		iMarkSize = 10;
		dbFontSize = 0.7;
	}

	CRect	rtRect;
	CPoint	ptCenter;
	CString szText;

	for (UINT nCnt = 0; nCnt < (UINT)stData.iFailCount; nCnt++)
	{
		rtRect = stData.rtFailROI[nCnt];
		Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 100, 0), iLineSize);

		rtRect.left = stData.rtFailROI[nCnt].left + 5;

		if (stData.rtFailROI[nCnt].top < 100)
			rtRect.top = stData.rtFailROI[nCnt].top + 30;
		else
			rtRect.top = stData.rtFailROI[nCnt].top - 15;

		szText.Format(_T("%d"), nCnt);

		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 100, 0), iLineSize, dbFontSize, szText);
	}
}

//=============================================================================
// Method		: Overlay_FPN
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_FPN_Opt stOption
// Parameter	: __in ST_FPN_Data stData
// Qualifier	:
// Last Update	: 2018/5/12 - 17:25
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_FPN(__inout IplImage* lpImage, __in ST_FPN_Opt stOption, __in ST_FPN_Data stData)
{
	if (NULL == lpImage)
		return;

	int iLineSize;
	int iMarkSize;
	double dbFontSize;

	CRect rtRect;
	CString szText;

	if ((m_ModelType == Model_OMS_Entry) || (m_ModelType == Model_OMS_Front) || (m_ModelType == Model_OMS_Front_Set))
	{
		iLineSize = 2;
		iMarkSize = 5;
		dbFontSize = 0.8;
		rtRect.left = 150;
	}
	else
	{
		iLineSize = 4;
		iMarkSize = 10;
		dbFontSize = 1.2;
	}


	for (UINT nIdx = 0; nIdx < ROI_FPN_Max; nIdx++)
	{
	
		rtRect = stOption.stRegion[nIdx].rtROI;
	
		
		Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);

	}

	rtRect.SetRectEmpty();

	rtRect.left = lpImage->width / 2 - 300;
	rtRect.top = lpImage->height / 3 * 2;

	szText.Format(_T("X Count : %d , Y Count : %d"), stData.iFailCount[Spec_FPN_X], stData.iFailCount[Spec_FPN_Y]);

	if (TR_Pass == stData.nResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}

//=============================================================================
// Method		: Overlay_3D_Depth
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_3D_Depth_Opt stOption
// Parameter	: __in ST_3D_Depth_Data stData
// Qualifier	:
// Last Update	: 2018/5/12 - 17:25
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_3D_Depth(__inout IplImage* lpImage, __in ST_3D_Depth_Opt stOption, __in ST_3D_Depth_Data stData)
{
	if (NULL == lpImage)
		return;

	int iLineSize;
	int iMarkSize;
	double dbFontSize;

	CRect rtRect;
	CString szText;

	if ((m_ModelType == Model_OMS_Entry) || (m_ModelType == Model_OMS_Front) || (m_ModelType == Model_OMS_Front_Set))
	{
		iLineSize = 2;
		iMarkSize = 5;
		dbFontSize = 0.8;
		rtRect.left = 50;
	}
	else
	{
		iLineSize = 4;
		iMarkSize = 10;
		dbFontSize = 1.2;
	}

	rtRect.SetRectEmpty();

	rtRect.left = lpImage->width / 2 - 200;
	rtRect.top = lpImage->height / 3 * 2;

	szText.Format(_T("Brightness Avg : %.2f, Deviation  : %.2f"), stData.dbValue[Spec_BrightnessAvg], stData.dbValue[Spec_Standard_deviation]);

	if (TR_Pass == stData.nResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}

//=============================================================================
// Method		: Overlay_EEPRAM
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_EEPROM_Verify_Opt stOption
// Parameter	: __in ST_EEPROM_Verify_Data stData
// Qualifier	:
// Last Update	: 2018/5/12 - 17:25
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_EEPRAM(__inout IplImage* lpImage, __in ST_EEPROM_Verify_Opt stOption, __in ST_EEPROM_Verify_Data stData)
{
	if (NULL == lpImage)
		return;
}

//=============================================================================
// Method		: Overlay_TemperatureSensor
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_EEPROM_Verify_Opt stOption
// Parameter	: __in ST_EEPROM_Verify_Data stData
// Qualifier	:
// Last Update	: 2018/5/12 - 17:25
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_TemperatureSensor(__inout IplImage* lpImage, __in ST_TemperatureSensor_Opt stOption, __in ST_TemperatureSensor_Data stData)
{
	if (NULL == lpImage)
		return;
	int iLineSize;
	int iMarkSize;
	double dbFontSize;

	CRect rtRect;
	CString szText;
	rtRect.SetRectEmpty();

	if ((m_ModelType == Model_OMS_Entry) || (m_ModelType == Model_OMS_Front) || (m_ModelType == Model_OMS_Front_Set))
	{
		iLineSize = 2;
		iMarkSize = 5;
		dbFontSize = 0.6;
		rtRect.left = 10;
	}
	else
	{
		iLineSize = 4;
		iMarkSize = 10;
		dbFontSize = 1.2;
		rtRect.left = lpImage->width / 2 - 200;
	}


	rtRect.top = lpImage->height / 3 * 2;
	szText.Format(_T("Temperature : %.2f"), stData.dbTemperature);

	if (TR_Pass == stData.nResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}

}
//=============================================================================
// Method		: Overlay_Process
// Access		: protected  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in enOverlayMode enMode
// Parameter	: __in CRect rtROI
// Parameter	: __in COLORREF clrLineColor
// Parameter	: __in int iLineSize
// Parameter	: __in double dbFontSize
// Parameter	: __in CString szText
// Qualifier	:
// Last Update	: 2018/3/2 - 10:56
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_Process(__inout IplImage* lpImage, __in enOverlayMode enMode, __in CRect rtROI, __in COLORREF clrLineColor /*= RGB(255, 200, 0)*/, __in int iLineSize /*= 1*/, __in double dbFontSize /*= 1.0*/, __in CString szText /*= _T("")*/)
{
	CvPoint nStartPt;
	nStartPt.x = rtROI.left;
	nStartPt.y = rtROI.top;

	CvPoint nEndPt;
	nEndPt.x = rtROI.right;
	nEndPt.y = rtROI.bottom;

	CvPoint nCenterPt;
	nCenterPt.x = nStartPt.x + ((nEndPt.x - nStartPt.x) / 2);
	nCenterPt.y = nStartPt.y + ((nEndPt.y - nStartPt.y) / 2);

	CvSize Ellipse_Axis;
	Ellipse_Axis.width = (nEndPt.x - nStartPt.x)/2;
	Ellipse_Axis.height = (nEndPt.y - nStartPt.y)/2;


	switch (enMode)
	{
	case OvrMode_LINE:
		cvLine(lpImage, nStartPt, nEndPt, CV_RGB(GetRValue(clrLineColor), GetGValue(clrLineColor), GetBValue(clrLineColor)), iLineSize);
		break;
	case OvrMode_RECTANGLE:
		cvRectangle(lpImage, nStartPt, nEndPt, CV_RGB(GetRValue(clrLineColor), GetGValue(clrLineColor), GetBValue(clrLineColor)), iLineSize);
		break;
	case OvrMode_CIRCLE:
		//cvCircle(lpImage, nStartPt, abs(nEndPt.x - nStartPt.x) / 2, CV_RGB(GetRValue(clrLineColor), GetGValue(clrLineColor), GetBValue(clrLineColor)), iLineSize);
		cvEllipse(lpImage, nCenterPt, Ellipse_Axis, 0, 0, 360, CV_RGB(GetRValue(clrLineColor), GetGValue(clrLineColor), GetBValue(clrLineColor)), iLineSize);
		break;
	case OvrMode_TXT:
	{
		CvFont* cvFont = new CvFont;
		cvInitFont(cvFont, CV_FONT_VECTOR0, dbFontSize, dbFontSize, 3, iLineSize);
		cvPutText(lpImage, CT2A(szText), nStartPt, cvFont, CV_RGB(GetRValue(clrLineColor), GetGValue(clrLineColor), GetBValue(clrLineColor))); // cvPoint 위치에 설정 색상으로 글씨 넣기

		delete cvFont; // 해제
	}
		break;
	default:
		break;
	}
}


//=============================================================================
// Method		: Overlay_LensScan
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_Torque_Opt stOption
// Parameter	: __in ST_Torque_Data stData
// Qualifier	:
// Last Update	: 2018/3/12 - 19:52
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_LensScan(__inout IplImage* lpImage, __in ST_SFR_Opt	stOption, __in ST_SFR_Data	stData)
{
	if (NULL == lpImage)
		return;

	int		iLineSize = 4;
	int		iMarkSize = 10;
	double dbFontSize = 1.3;

	CRect	rtRect;
	CPoint	ptCenter;

	CString szText;
	szText = _T("Do Lens Scan!");

	iLineSize = 6;
	dbFontSize = 2.0;

	rtRect.left = lpImage->width / 2 - 210;
	rtRect.top = 100;

	// 추후 적용 예정
	//if (TR_Pass == stData.nDetectResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 150, 0), iLineSize, dbFontSize, szText);
	}
	//else
	//{
	//	Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	//}
}

//=============================================================================
// Method		: Overlay_Bestfocus
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_Torque_Opt stOption
// Parameter	: __in ST_Torque_Data stData
// Qualifier	:
// Last Update	: 2018/3/12 - 19:52
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_Bestfocus(__inout IplImage* lpImage, __in ST_SFR_Opt	stOption, __in ST_SFR_Data	stData)
{
	if (NULL == lpImage)
		return;

	int		iLineSize = 4;
	int		iMarkSize = 10;
	double dbFontSize = 1.3;

	CRect	rtRect;
	CPoint	ptCenter;

	CString szText;
	szText = _T("Searching for best focus!");

	iLineSize = 6;
	dbFontSize = 2.0;

	rtRect.left = lpImage->width / 2 - 350;
	rtRect.top = 100;

	// 추후 적용 예정
	//if (TR_Pass == stData.nDetectResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 150, 0), iLineSize, dbFontSize, szText);
	}
	//else
	//{
	//	Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	//}
}


void COverlay_Proc::Overlay_Particle_Entry(__inout IplImage* lpImage, __in ST_Particle_Entry_Opt stOption, __in ST_Particle_Entry_Data stData)
{
	if (NULL == lpImage)
		return;

	int				iLineSize = 0;
	int				iMarkSize = 10;
	CRect			rtRect;
	CPoint			ptCenter;
	double			dbFontSize = 0.0;
	CString			szText;
	enOverlayMode	enMode;

	for (UINT nIdx = 0; nIdx < Particle_Entry_Region_MaxEnum; nIdx++)
	{
		if (TRUE == stOption.stRegion[nIdx].bEllipse)
		{
			enMode = OvrMode_CIRCLE;
		}
		else
		{
			enMode = OvrMode_RECTANGLE;
		}

		iLineSize = 3;
		rtRect = stOption.stRegion[nIdx].rtROI;
		Overlay_Process(lpImage, enMode, rtRect, RGB(255, 200, 0), iLineSize);
		rtRect.left = stOption.stRegion[nIdx].rtROI.left;

		if (stOption.stRegion[nIdx].rtROI.top < 100)
			rtRect.top = stOption.stRegion[nIdx].rtROI.top + 30;
		else
			rtRect.top = stOption.stRegion[nIdx].rtROI.top - 10;

		iLineSize = 2;
		dbFontSize = 1.0;
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 200, 0), iLineSize, dbFontSize, g_szROI_Particle[nIdx]);
	}

	iLineSize = 2;

	for (UINT nCnt = 0; nCnt < stData.nFailCount; nCnt++)
	{
		rtRect = stData.ErrRegionList[nCnt].RegionList;
		Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 100, 0), iLineSize);

		/*rtRect.left = stData.ErrRegionList[nCnt].RegionList.left + 5;

		if (stData.ErrRegionList[nCnt].RegionList.top < 100)
			rtRect.top = stData.ErrRegionList[nCnt].RegionList.top + 30;
		else
			rtRect.top = stData.ErrRegionList[nCnt].RegionList.top - 15;

		iLineSize = 3;
		dbFontSize = 1.0;
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 100, 0), iLineSize, dbFontSize, g_szParticle_Type[stData.nFailType[nCnt]]);*/
	}
}