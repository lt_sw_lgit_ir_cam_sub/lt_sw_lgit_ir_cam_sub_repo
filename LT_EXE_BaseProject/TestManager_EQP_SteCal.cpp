﻿//*****************************************************************************
// Filename	: 	TestManager_EQP_SteCal.cpp
// Created	:	2016/5/9 - 13:32
// Modified	:	2016/08/10
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "TestManager_EQP_SteCal.h"
#include "CommonFunction.h"
#include "File_Recipe.h"
#include "Def_Digital_IO.h"
#include "CRC16.h"

#include <Mmsystem.h>
#pragma comment (lib,"winmm.lib")

CTestManager_EQP_SteCal::CTestManager_EQP_SteCal()
{
	OnInitialize();
}

CTestManager_EQP_SteCal::~CTestManager_EQP_SteCal()
{
	TRACE(_T("<<< Start ~CTestManager_EQP_SteCal >>> \n"));

	this->OnFinalize();

	TRACE(_T("<<< End ~CTestManager_EQP_SteCal >>> \n"));	
}


//=============================================================================
// Method		: OnLoadOption
// Access		: virtual protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/9/28 - 20:04
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP_SteCal::OnLoadOption()
{
	BOOL bReturn = CTestManager_EQP::OnLoadOption();

	

	return bReturn;
}

//=============================================================================
// Method		: InitDevicez
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in HWND hWndOwner
// Qualifier	:
// Last Update	: 2017/11/11 - 21:37
// Desc.		:
//=============================================================================
void CTestManager_EQP_SteCal::InitDevicez(__in HWND hWndOwner /*= NULL*/)
{
	CTestManager_EQP::InitDevicez(hWndOwner);

}

//=============================================================================
// Method		: StartOperation_LoadUnload
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:19
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::StartOperation_LoadUnload(__in BOOL bLoad)
{
	LRESULT lResult = RC_OK;
	lResult = CTestManager_EQP::StartOperation_LoadUnload(bLoad);



	return lResult;
}

//=============================================================================
// Method		: StartOperation_Inspection
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:21
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::StartOperation_Inspection(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lResult = RC_OK;
	lResult = CTestManager_EQP::StartOperation_Inspection(nParaIdx);



	return lResult;
}

//=============================================================================
// Method		: StartOperation_Manual
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bLoad
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:22
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::StartOperation_Manual(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lResult = RC_OK;
	lResult = CTestManager_EQP::StartOperation_Manual(nStepIdx, nParaIdx);



	return lResult;
}

//=============================================================================
// Method		: StartOperation_InspManual
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/20 - 11:15
// Desc.		: 이미지 검사 개별 테스트용
//=============================================================================
LRESULT CTestManager_EQP_SteCal::StartOperation_InspManual(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lResult = RC_OK;
	lResult = CTestManager_EQP::StartOperation_InspManual(nParaIdx);



	return lResult;
}

//=============================================================================
// Method		: StopProcess_LoadUnload
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/19 - 19:35
// Desc.		:
//=============================================================================
void CTestManager_EQP_SteCal::StopProcess_LoadUnload()
{
	CTestManager_EQP::StopProcess_LoadUnload();

}

//=============================================================================
// Method		: StopProcess_Test_All
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/10 - 21:14
// Desc.		:
//=============================================================================
void CTestManager_EQP_SteCal::StopProcess_Test_All()
{
	CTestManager_EQP::StopProcess_Test_All();

}

//=============================================================================
// Method		: StopProcess_InspManual
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/20 - 11:15
// Desc.		:
//=============================================================================
void CTestManager_EQP_SteCal::StopProcess_InspManual(__in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::StopProcess_InspManual(nParaIdx);

}

//=============================================================================
// Method		: AutomaticProcess_LoadUnload
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:23
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP_SteCal::AutomaticProcess_LoadUnload(__in BOOL bLoad)
{
	BOOL bResult = TRUE;
	bResult = CTestManager_EQP::AutomaticProcess_LoadUnload(bLoad);



	return bResult;
}

//=============================================================================
// Method		: AutomaticProcess_Test_All
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:26
// Desc.		:
//=============================================================================
void CTestManager_EQP_SteCal::AutomaticProcess_Test_All(__in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::AutomaticProcess_Test_All(nParaIdx);

	
}

//=============================================================================
// Method		: AutomaticProcess_Test_InspManual
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/15 - 3:06
// Desc.		:
//=============================================================================
void CTestManager_EQP_SteCal::AutomaticProcess_Test_InspManual(__in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::AutomaticProcess_Test_InspManual(nParaIdx);

	
}

//=============================================================================
// Method		: OnInitial_LoadUnload
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:28
// Desc.		:
//=============================================================================
void CTestManager_EQP_SteCal::OnInitial_LoadUnload(__in BOOL bLoad)
{
	CTestManager_EQP::OnInitial_LoadUnload(bLoad);

	if (bLoad)
	{
		OnDOut_StartLamp(FALSE);
		OnDOut_StopLamp(TRUE);

		OnLightBrd_PowerOn(m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[0].fVolt, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[0].wStep);
	}
}

//=============================================================================
// Method		: OnFinally_LoadUnload
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:29
// Desc.		:
//=============================================================================
void CTestManager_EQP_SteCal::OnFinally_LoadUnload(__in BOOL bLoad)
{
	CTestManager_EQP::OnFinally_LoadUnload(bLoad);

	if (FALSE == bLoad)
	{
		OnDOut_StartLamp(TRUE);
		OnDOut_StopLamp(FALSE);
	}
}

//=============================================================================
// Method		: OnStart_LoadUnload
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:30
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnStart_LoadUnload(__in BOOL bLoad)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnStart_LoadUnload(bLoad);



	return lReturn;
}

//=============================================================================
// Method		: OnInitial_Test_All
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 19:36
// Desc.		:
//=============================================================================
void CTestManager_EQP_SteCal::OnInitial_Test_All(__in UINT nParaIdx /*= 0*/)
{
	//CTestManager_EQP::OnInitial_Test_All(nParaIdx);


	// 테스트 상태 : Run	
	m_stInspInfo.nTestPara = nParaIdx;
	OnSet_TestResult_Unit(TR_Testing, Para_Left);
	OnSet_TestResult_Unit(TR_Testing, Para_Right);
	OnSet_TestProgress_Unit(TP_Testing, Para_Left);
	OnSet_TestProgress_Unit(TP_Testing, Para_Right);

	// 검사 시작 시간 설정
	OnSet_BeginTestTime(Para_Left);
	OnSet_BeginTestTime(Para_Right);

	// 채널 카메라 데이터 초기화
	m_stInspInfo.WorklistInfo.Reset();
 
}

//=============================================================================
// Method		: OnFinally_Test_All
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in LRESULT lResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/23 - 13:05
// Desc.		:
//=============================================================================
void CTestManager_EQP_SteCal::OnFinally_Test_All(__in LRESULT lResult, __in UINT nParaIdx /*= 0*/)
{
	//CTestManager_EQP::OnFinally_Test_All(lResult, nParaIdx);

	OnSet_EndTestTime(Para_Left);
	OnSet_EndTestTime(Para_Right);

	OnJugdement_And_Report(Para_Left);
}

//=============================================================================
// Method		: OnStart_Test_All
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:30
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnStart_Test_All(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//lReturn = CTestManager_EQP::OnStart_Test_All( nParaIdx);
	// * 안전 기능 체크
	if (TR_Pass != m_stInspInfo.CamInfo[nParaIdx].nJudgeLoading)
	{
		if (RC_OK == m_stInspInfo.CamInfo[nParaIdx].ResultCode)
			lReturn = RC_LoadingFailed;
		else
			lReturn = m_stInspInfo.CamInfo[nParaIdx].ResultCode;

		// * 임시 : 안전하게 배출하기 위하여
		// KHO  이거 지워도 되는 건지? (A:지우면 언로드가 안될때 있음 )
		Sleep(3000);

		return lReturn;
	}
	//
	// * 검사 시작 위치로 제품 이동
	if (g_InspectorTable[m_InspectionType].UseMotion)
	{
		lReturn = OnMotion_MoveToTestPos(nParaIdx);
		if (RC_OK != lReturn)
		{
			// 에러 상황

			// 알람
			OnAddAlarm_F(_T("Motion : Move to Test Position [Para -> %s]"), g_szParaName[nParaIdx]);
			//return lReturn;
		}
	}

	// * Step별로 검사 진행
	enTestResult nResult = TR_Pass;
	OnSetCamerParaSelect(Para_Left);

	for (UINT nTryCnt = 0; nTryCnt <= m_stInspInfo.RecipeInfo.nTestRetryCount; nTryCnt++)
	{
		// 검사기에 맞추어 검사 시작
		lReturn = StartTest_Equipment(nParaIdx);

		// * 검사 결과 판정
		nResult = Judgment_Inspection(nParaIdx);

		if ((TR_Pass == nResult) || (TR_Check == nResult))
		{
			break;
		}
		else
		{
			// 측정 데이터, UI 초기화
#ifdef USE_PRESET_MODE
			if (m_stInspInfo.RecipeInfo.stPresetStepInfo.bUsePresetMode)
			{
				UINT nFailTestItem = 0;
				if (TR_Fail == GetFailTestItem_CalResult(nFailTestItem))
				{
					UINT nPresetIndex = 0;
					if (GetPresetIndex(nFailTestItem, nPresetIndex))
					{
						// 측정 데이터 UI 초기화, 모터 검사 위치로
						lReturn = StartTest_PresetMode(nPresetIndex);

						// * 검사 결과 판정
						nResult = Judgment_Inspection(nParaIdx);
					}
				}
			}
#endif
		}
	}

	// * 검사 결과 판정 정보 세팅 및 UI표시
	OnSet_TestResult_Unit(nResult, nParaIdx);
	OnUpdate_TestReport(nParaIdx);


	Judgment_Inspection_All();

	// * MES 검사 결과 보고
	if (MES_Online == m_stInspInfo.MESOnlineMode)
	{
		lReturn = MES_SendInspectionData(nParaIdx);
		if (RC_OK != lReturn)
		{
			// 에러 상황
		}
	}

	// * Unload Product
	lReturn = StartOperation_LoadUnload(Product_Unload);
	if (RC_OK != lReturn)
	{
		// 에러 상황
	}

	

	return lReturn;
}

//=============================================================================
// Method		: OnStart_Test_Unit
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nUnitIdx
// Parameter	: __in enThreadTestType nTestType
// Parameter	: __in UINT nStepIdx
// Qualifier	:
// Last Update	: 2018/5/30 - 17:39
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnStart_Test_Unit(__in UINT nUnitIdx, __in enThreadTestType nTestType, __in UINT nStepIdx)
{
	//return CTestManager_EQP::OnStart_Test_Unit(nUnitIdx, nTestType, nStepIdx);

	LRESULT lReturn = RC_OK;

	TRACE(_T("OnStart_Test_Unit [CH_%02d]\n"), nUnitIdx);

	switch (nTestType)
	{
	case TTT_Initialize:
		lReturn = _TI_Cm_Initialize(nStepIdx, nUnitIdx);
		break;

	case TTT_Fialize:
		lReturn = _TI_Cm_Finalize(nStepIdx, nUnitIdx);
		break;

	case TTT_CameraReboot:
		lReturn = _TI_Cm_CameraReboot(TRUE, nUnitIdx);
		break;

	case TTT_EEPROM_Write:
		if (Para_Left == nUnitIdx)
			lReturn = OnDAQ_EEPROM_MRA2_SteCal_M(TRUE);
		else
			lReturn = OnDAQ_EEPROM_MRA2_SteCal_M(FALSE);
		break;

	default:
		break;
	}

	return lReturn;
}

//=============================================================================
// Method		: OnInitial_Test_InspManual
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/8 - 9:11
// Desc.		:
//=============================================================================
void CTestManager_EQP_SteCal::OnInitial_Test_InspManual(__in UINT nTestItemID, __in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::OnInitial_Test_InspManual(nTestItemID, nParaIdx);


}

//=============================================================================
// Method		: OnFinally_Test_InspManual
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nTestItemID
// Parameter	: __in LRESULT lResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/8 - 9:12
// Desc.		:
//=============================================================================
void CTestManager_EQP_SteCal::OnFinally_Test_InspManual(__in UINT nTestItemID, __in LRESULT lResult, __in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::OnFinally_Test_InspManual(nTestItemID, lResult, nParaIdx);


}

//=============================================================================
// Method		: OnStart_Test_InspManual
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/15 - 3:05
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnStart_Test_InspManual(__in UINT nTestItemID, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnStart_Test_InspManual(nTestItemID, nParaIdx);


	return lReturn;
}

//=============================================================================
// Method		: Load_Product
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::Load_Product()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::Load_Product();

	

	return lReturn;
}

//=============================================================================
// Method		: Unload_Product
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::Unload_Product()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::Unload_Product();



	return lReturn;
}

//=============================================================================
// Method		: StartTest_Equipment
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/20 - 18:50
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::StartTest_Equipment(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = StartTest_StereoCAL();

	return lReturn;
}

//=============================================================================
// Method		: StartTest_StereoCAL
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bUsePresetMode
// Parameter	: __in UINT nPresetIndex
// Qualifier	:
// Last Update	: 2018/4/6 - 10:08
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::StartTest_StereoCAL(__in BOOL bUsePresetMode /*= FALSE*/, __in UINT nPresetIndex /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	const ST_StepInfo* pStepInfo = NULL;
	INT_PTR iStepCnt = 0;

#ifdef USE_PRESET_MODE
	if (FALSE == bUsePresetMode)
	{
		pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;
		iStepCnt = m_stInspInfo.RecipeInfo.StepInfo.GetCount();
	}
	else
	{
		pStepInfo = &m_stInspInfo.RecipeInfo.stPresetStepInfo.stStepInfo[nPresetIndex];
		iStepCnt = m_stInspInfo.RecipeInfo.stPresetStepInfo.stStepInfo[nPresetIndex].GetCount();
	}
#else
	pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;
	iStepCnt = m_stInspInfo.RecipeInfo.StepInfo.GetCount();
#endif

	DWORD dwElapTime = 0;
	UINT nCornerStep = 0;

	BOOL bError = FALSE;

	// * 설정된 스텝 진행
	for (INT nStepIdx = 0; nStepIdx < iStepCnt; nStepIdx++)
	{
		lReturn = RC_OK;

		// 스텝 진행 시간 체크
		dwElapTime = Check_TimeStepStart();

		// UI 스텝 선택
		OnSet_TestStepSelect(nStepIdx);

		// * 기구 이동		
		if ((TRUE == pStepInfo->StepList[nStepIdx].bUseChart_Move_X) || (TRUE == pStepInfo->StepList[nStepIdx].bUseMoveY) || (TRUE == pStepInfo->StepList[nStepIdx].bUseChart_Move_Z) || (TRUE == pStepInfo->StepList[nStepIdx].bUseChart_Tilt_X) || (TRUE == pStepInfo->StepList[nStepIdx].bUseChart_Tilt_Z))
		{
#ifndef MOTION_NOT_USE
			lReturn = OnMotion_SteCal_Test(pStepInfo->StepList[nStepIdx].iChart_Move_X, pStepInfo->StepList[nStepIdx].nMoveY, pStepInfo->StepList[nStepIdx].iChart_Move_Z, pStepInfo->StepList[nStepIdx].iChart_Tilt_X, pStepInfo->StepList[nStepIdx].iChart_Tilt_Z, pStepInfo->StepList[nStepIdx].bUseChart_Move_X, pStepInfo->StepList[nStepIdx].bUseMoveY, pStepInfo->StepList[nStepIdx].bUseChart_Move_Z, pStepInfo->StepList[nStepIdx].bUseChart_Tilt_X, pStepInfo->StepList[nStepIdx].bUseChart_Tilt_Z);
			if (lReturn != RC_OK)
			{
				// ERROR
				TRACE("[Motion Err] OnMotion_SteCal_Test(Chart X:%d, Chart Z:%d, Chart T1:%d, Chart T2:%d)\r\n", pStepInfo->StepList[nStepIdx].iChart_Move_X, pStepInfo->StepList[nStepIdx].iChart_Move_Z, pStepInfo->StepList[nStepIdx].iChart_Tilt_X, pStepInfo->StepList[nStepIdx].iChart_Tilt_Z);
				OnLog_Err(_T("[Motion Err] OnMotion_SteCal_Test(Chart X:%d, Chart Z:%d, Chart T1:%d, Chart T2:%d"), pStepInfo->StepList[nStepIdx].iChart_Move_X, pStepInfo->StepList[nStepIdx].iChart_Move_Z, pStepInfo->StepList[nStepIdx].iChart_Tilt_X, pStepInfo->StepList[nStepIdx].iChart_Tilt_Z);
				// ERROR
				TRACE("[Motion Err] OnActionTesting_Stereo_Y(Move Y: %d)\r\n", pStepInfo->StepList[nStepIdx].nMoveY);
				OnLog_Err(_T("[Motion Err] OnActionTesting_Stereo_Y(CMove Y: %d"), pStepInfo->StepList[nStepIdx].nMoveY);
			}

			Sleep(300);
#endif
		}

		if (lReturn != RC_OK)	// 모터 에러
		{
			if (Model_MRA2 == m_stInspInfo.RecipeInfo.ModelType)
			{
				m_stInspInfo.Set_Judgment(Para_Left, nStepIdx, TR_Check);
				m_stInspInfo.Set_Judgment(Para_Right, nStepIdx, TR_Check);
			}
			else
			{
				m_stInspInfo.Set_Judgment(Para_Left, nStepIdx, TR_Check);
			}
		}

		// * 검사 시작
		if ((TRUE == pStepInfo->StepList[nStepIdx].bTest) && (RC_OK == lReturn))
		{
			if (OpMode_DryRun != m_stInspInfo.OperateMode)
			{
				lReturn = StartTest_SteCAL_TestItem(pStepInfo->StepList[nStepIdx].nTestItem, nStepIdx, nCornerStep++);
			}
		}

		// 스텝 진행 시간 체크
		dwElapTime = Check_TimeStepStop(dwElapTime);

		// 진행 시간 설정
		m_stInspInfo.Set_ElapTime(Para_Left, nStepIdx, dwElapTime);

		// 스텝 결과 UI에 표시
		OnSet_TestStepResult(nStepIdx);

		// * 검사 프로그레스		
		OnSet_TestProgressStep((UINT)iStepCnt, nStepIdx + 1);

		// UI 갱신 딜레이
		Sleep(100);

		// 스텝 딜레이
		if (0 < pStepInfo->StepList[nStepIdx].dwDelay)
		{
			Sleep(pStepInfo->StepList[nStepIdx].dwDelay);
		}

		// * 검사 중단 판별
		if (RC_OK != lReturn)
		{
			if(TI_Ste_Initialize_Test == pStepInfo->StepList[nStepIdx].nTestItem)
			{
				bError = TRUE;
				break;
			}
			else if (TI_Ste_DetectPattern == pStepInfo->StepList[nStepIdx].nTestItem)
			{
// 				bError = TRUE;
// 				break;
			}
			else if (TI_Ste_Calibration == pStepInfo->StepList[nStepIdx].nTestItem)
			{
				// EEPROM 기록??
				bError = TRUE;
				break;
			}
		}
	} // 스텝 루프 종료


	// 검사 중단시 Finalize 수행
	if (bError)
	{
		if (Model_MRA2 == m_stInspInfo.RecipeInfo.ModelType)
		{
			// KHO 수정
// 			lReturn = _TI_Cm_Finalize_All(0);
			_TI_Cm_Finalize_Error(Para_Left);
			_TI_Cm_Finalize_Error(Para_Right);
		}
		else
		{
// 			lReturn = _TI_Cm_Finalize(0, Para_Left);
			_TI_Cm_Finalize_Error(Para_Left);
		}

		OnSet_TestResult_Unit(TR_Check, Para_Left);
	}

	return lReturn;
}

//=============================================================================
// Method		: StartTest_PresetMode
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nPresetIndex
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/4/6 - 10:12
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::StartTest_PresetMode(__in UINT nPresetIndex, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	if (MAX_TESTSTEP_PRESET <= nParaIdx)
	{
		return RC_UnknownError;
	}

	// * UI 초기화
	OnResetInfo_Measurment(nParaIdx);

	// * 검사 시작 위치로 제품 이동
	if (g_InspectorTable[m_InspectionType].UseMotion)
	{
		lReturn = OnMotion_MoveToTestPos(nParaIdx);
		if (RC_OK != lReturn)
		{
			// 에러 상황

			// 알람
			OnAddAlarm_F(_T("Motion : Move to Test Position [Para -> %s]"), g_szParaName[nParaIdx]);
			return lReturn;
		}
	}

	// * 스텝별 검사 루틴 시작 (프리셋 모드)
	lReturn = StartTest_StereoCAL(TRUE, nPresetIndex);

	return lReturn;
}

//=============================================================================
// Method		: AutomaticProcess_TestItem
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nCornerStep
// Qualifier	:
// Last Update	: 2017/12/12 - 13:31
// Desc.		:
//=============================================================================
void CTestManager_EQP_SteCal::AutomaticProcess_TestItem(__in UINT nParaIdx, __in UINT nTestItemID, __in UINT nStepIdx, __in UINT nCornerStep /*= 0*/)
{
// 	if (TI_2D_Detect_CornerExt == nTestItemID)
// 	{
// 		LRESULT lReturn = _TI_2DCAL_CornerExt(nStepIdx, nCornerStep, nParaIdx);
// 	}
}

//=============================================================================
// Method		: StartTest_SteCAL_TestItem
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nCornerStep
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/21 - 13:10
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::StartTest_SteCAL_TestItem(__in UINT nTestItemID, __in UINT nStepIdx, __in UINT nCornerStep /*= 0*/, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

 	switch (nTestItemID)
 	{
	case TI_Ste_Initialize_Test:
		if (Model_MRA2 == m_stInspInfo.RecipeInfo.ModelType)
		{
			lReturn = _TI_Cm_Initialize_All(nStepIdx);
		}
		else
		{
			lReturn = _TI_Cm_Initialize(nStepIdx, nParaIdx);
		}
		break;

	case TI_Ste_Finalize_Test:
		if (Model_MRA2 == m_stInspInfo.RecipeInfo.ModelType)
		{
			lReturn = _TI_Cm_Finalize_All(nStepIdx);
		}
		else
		{
			lReturn = _TI_Cm_Finalize(nStepIdx, nParaIdx);
		}
		break;

	case TI_Ste_SetParameter:
		lReturn = _TI_SteCAL_SetParameter(nStepIdx);
		break;

	case TI_Ste_DetectPattern:
		lReturn = _TI_SteCAL_DetectPattern(nStepIdx, nCornerStep);
		break;

	case TI_Ste_Calibration:
		lReturn = _TI_SteCAL_Calibration(nStepIdx);
		break;

	case TI_Ste_WriteEEPROM:
		if (Model_MRA2 == m_stInspInfo.RecipeInfo.ModelType)
		{
			lReturn = _TI_SteCAL_WriteEEPROM_Dual(nStepIdx);
		}
		else
		{
			lReturn = _TI_SteCAL_WriteEEPROM(nStepIdx);
		}
		break;

	case TI_Ste_Re_M_Focal_Len_X:			// Single
	case TI_Ste_Re_M_Focal_Len_Y:
	case TI_Ste_Re_M_Principle_X:
	case TI_Ste_Re_M_Principle_Y:
	case TI_Ste_Re_M_K1:
	case TI_Ste_Re_M_K2:
	case TI_Ste_Re_M_K3:
	case TI_Ste_Re_M_K4:
	case TI_Ste_Re_M_K5:
	case TI_Ste_Re_M_RMS:
	case TI_Ste_Re_S_Focal_Len_X:
	case TI_Ste_Re_S_Focal_Len_Y:
	case TI_Ste_Re_S_Principle_X:
	case TI_Ste_Re_S_Principle_Y:
	case TI_Ste_Re_S_K1:
	case TI_Ste_Re_S_K2:
	case TI_Ste_Re_S_K3:
	case TI_Ste_Re_S_K4:
	case TI_Ste_Re_S_K5:
	case TI_Ste_Re_S_RMS:
 	case TI_Ste_Re_RotationVector_1:	// Stereo
 	case TI_Ste_Re_RotationVector_2:
 	case TI_Ste_Re_RotationVector_3:
 	case TI_Ste_Re_TranslationVector_1:
 	case TI_Ste_Re_TranslationVector_2:
 	case TI_Ste_Re_TranslationVector_3:
	case TI_Ste_Re_X:
	case TI_Ste_Re_Y:
	case TI_Ste_Re_Z:
	case TI_Ste_Re_Roll:
	case TI_Ste_Re_Pitch:
	case TI_Ste_Re_Yaw:
	case TI_Ste_Re_StereoRMS:
		if (enModelType::Model_IKC == m_stInspInfo.Get_ModelType())
		{
			lReturn = _TI_SteCAL_JudgeResult(nStepIdx, nTestItemID);
		}
		else
		{
			lReturn = _TI_SteCAL_JudgeResult_Dual(nStepIdx, nTestItemID);
		}
		break;
 	
 	default:
 		break;
 	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_Initialize
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/6 - 21:56
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::_TI_Cm_Initialize(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_Initialize(nStepIdx, nParaIdx);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_Ste_Initialize_Test].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_Finalize
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/13 - 13:54
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::_TI_Cm_Finalize(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_Finalize(nStepIdx, nParaIdx);

	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_Ste_Finalize_Test].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_Finalize_Error
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/6/10 - 10:48
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::_TI_Cm_Finalize_Error(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_Finalize_Error(nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_MeasurCurrent
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/13 - 13:54
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::_TI_Cm_MeasurCurrent(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_MeasurCurrent(nStepIdx, nParaIdx);



	return lReturn;
}

LRESULT CTestManager_EQP_SteCal::_TI_Cm_MeasurVoltage(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_MeasurVoltage(nStepIdx, nParaIdx);



	return lReturn;
}

LRESULT CTestManager_EQP_SteCal::_TI_Cm_CaptureImage(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/, __in BOOL bSaveImage /*= FALSE*/, __in LPCTSTR szFileName /*= NULL*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_CaptureImage(nStepIdx, nParaIdx, bSaveImage, szFileName);



	return lReturn;
}

LRESULT CTestManager_EQP_SteCal::_TI_Cm_CameraRegisterSet(__in UINT nStepIdx, __in UINT nRegisterType, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_CameraRegisterSet(nStepIdx, nRegisterType, nParaIdx);



	return lReturn;
}

LRESULT CTestManager_EQP_SteCal::_TI_Cm_CameraAlphaSet(__in UINT nStepIdx, __in UINT nAlpha, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_CameraAlphaSet(nStepIdx, nAlpha, nParaIdx);



	return lReturn;
}

LRESULT CTestManager_EQP_SteCal::_TI_Cm_CameraPowerOnOff(__in UINT nStepIdx, __in enPowerOnOff nOnOff, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_CameraPowerOnOff(nStepIdx, nOnOff, nParaIdx);



	return lReturn;
}

//=============================================================================
// Method		: _TI_SteCAL_SetParameter
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Qualifier	:
// Last Update	: 2018/2/21 - 11:20
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::_TI_SteCAL_SetParameter(__in UINT nStepIdx)
{
	LRESULT lReturn = RC_OK;

	// *** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;

	// 보드 Init가 되어 있어야 함 (Loading 루틴에서 처리)

	// 파라미터 전송	
	for (UINT nCnt = 0; nCnt <= nRetryCnt; nCnt++)
	{
		lReturn = OnLib_SteCAL_SetParameter();

		if (RC_OK == lReturn)
			break;
	}

	// 카메라 개수
	UINT nCamCount = g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt;

	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_Ste_SetParameter].vt);

	// 스텝 결과
	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		for (UINT nParaIdx = 0; nParaIdx < nCamCount; nParaIdx++)
		{
			varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
			m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);
		}
	}
	else
	{
		// * 에러 상황
		for (UINT nParaIdx = 0; nParaIdx < nCamCount; nParaIdx++)
		{
			varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
			m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);
			m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_SteCAL_CaptureImage
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nCornerStep
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/21 - 11:20
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::_TI_SteCAL_CaptureImage(__in UINT nCornerStep, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	// 카메라 개수
	UINT nCamCount = g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt;

	// 영상 신호 체크
	if (OnDAQ_CheckVideoSignal(2000, nParaIdx))
	{
		// 이미지 캡쳐 (Request Capture)
		ST_VideoRGB* pstVideo = m_Device.DAQ_LVDS.GetRecvVideoRGB(nParaIdx);
		LPWORD lpwImage = (LPWORD)m_Device.DAQ_LVDS.GetSourceFrameData(nParaIdx);
		LPBYTE lpbyRGBImage = m_Device.DAQ_LVDS.GetRecvRGBData(nParaIdx);

		// 이미지 버퍼에 복사
		//memcpy(m_stImageBuf[nParaIdx].lpwImage_16bit, lpwImage, pstVideo->m_dwWidth * pstVideo->m_dwHeight * sizeof(WORD));
		memcpy(m_stImageBuf[nParaIdx].lpbyImage_8bit, lpbyRGBImage, pstVideo->m_dwWidth * pstVideo->m_dwHeight * 3);

		// 이미지 저장
		CString szPath;
		CString szFullPath;

		CString szFileName;
		szFileName.Format(_T("Step_%02d"), nCornerStep);
		szPath.Format(_T("%s%s\\%s"), m_stInspInfo.Path.szImage, g_szParaName[nParaIdx], m_stInspInfo.CamInfo[Para_Left].szBarcode);

		// /Left/모듈번호/
		MakeDirectory(szPath.GetBuffer(0));

// 		if (m_stOption.Inspector.bSaveImage_Gray12)
// 		{
// 			szFullPath.Format(_T("%s\\%s.png"), szPath, szFileName);
// 			OnSaveImage_Gray16(szFullPath.GetBuffer(0), m_stImageBuf[nParaIdx].lpwImage_16bit, pstVideo->m_dwWidth, pstVideo->m_dwHeight);
// 		}

		if (m_stOption.Inspector.bSaveImage_RGB)
		{
			szFullPath.Format(_T("%s\\%s.bmp"), szPath, szFileName);
			OnSaveImage_RGB(szFullPath.GetBuffer(0), m_stImageBuf[nParaIdx].lpbyImage_8bit, pstVideo->m_dwWidth, pstVideo->m_dwHeight);
		}

		if (m_stOption.Inspector.bSaveImage_RAW)
		{
			szFullPath.Format(_T("%s\\%s.raw"), szPath, szFileName);
			m_Device.DAQ_LVDS.SaveRawImage(nParaIdx, szFullPath.GetBuffer(0));
		}

		// 영상 캡쳐 이력에 추가
		CString szLabel;
		szLabel.Format(_T("%02d"), nCornerStep);
		OnImage_AddHistory(nParaIdx, szLabel);
	}
 	else
 	{
 		lReturn = RC_Grab_Err_NoSignal;
 	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_SteCAL_DetectPattern
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nCornerStep
// Qualifier	:
// Last Update	: 2018/2/21 - 11:20
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::_TI_SteCAL_DetectPattern(__in UINT nStepIdx, __in UINT nCornerStep)
{
	LRESULT lReturn = RC_OK;

	// *** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;

	// Delay Time 필요한가??

	// 카메라 개수
	UINT nCamCount = g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt;

	for (UINT nParaIdx = 0; nParaIdx < nCamCount; nParaIdx++)
	{
		// 영상 신호 체크
		if (OnDAQ_CheckVideoSignal(1000, nParaIdx))
		{
			// 이미지 캡쳐 (Request Capture)
			_TI_SteCAL_CaptureImage(nCornerStep, nParaIdx);
		}
		else
		{
			lReturn = RC_Grab_Err_NoSignal;
			break;
		}
	}

	// * 모터가 다음 스텝으로 이동가능하다는 이벤트 발생
	//SetEvent_ReqCapture();
	//Sleep(300);

	if (RC_OK == lReturn)
	{
		lReturn = OnLib_SteCAL_DetectPattern(nCornerStep);
	}

	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_Ste_DetectPattern].vt);

	// 스텝 결과
	if (RC_OK == lReturn)
	{
		// * 파일로 검출된 패턴 좌표 로그 남기기
		WriteLog_DetectPattern(nCornerStep);

		// * 검사 항목별 결과
		for (UINT nParaIdx = 0; nParaIdx < nCamCount; nParaIdx++)
		{
			varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
			m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);
		}
	}
	else
	{
		// * 파일로 검출된 패턴 좌표 로그 남기기
		WriteLog_DetectPattern(nCornerStep, TRUE);

		// * 에러 상황
		for (UINT nParaIdx = 0; nParaIdx < nCamCount; nParaIdx++)
		{
			varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
			m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);
			m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_SteCAL_Calibration
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Qualifier	:
// Last Update	: 2018/2/21 - 11:20
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::_TI_SteCAL_Calibration(__in UINT nStepIdx)
{
	LRESULT lReturn = RC_OK;

	// *** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;

	// 카메라 개수
	UINT nCamCount = g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt;

	// 라이브러리 구동
	lReturn = OnLib_SteCAL_ExecIntrinsic();

	// ** Flag 변경 재계산 기능을 사용한다면???
	// 2018.7.3 LGE협의 안되어서 주석 처리 함
// 	if (m_stInspInfo.RecipeInfo.bUseStereoCal_Flag)
// 	{
// 		// ** 임시 결과 판정
// 		UINT nFailTestItem = 0;
// 		if (FALSE == GetJudge_CalibrationResult(nStepIdx + 1, nFailTestItem))
// 		{
// 			// Fail이면 Flag를 변경하여 다시 계산한다.
// 			lReturn = OnLib_SteCAL_ExecIntrinsic(0x01);
// 		}
// 	}

	// 라이브러리 핸들 Close
	OnLib_SteCAL_CloseHandle();

	// 데이터 형식 변경
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_Ste_Calibration].vt);

	// 스텝 결과
	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		for (UINT nParaIdx = 0; nParaIdx < nCamCount; nParaIdx++)
		{
			varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
			m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);
		}
	}
	else
	{
		// * 에러 상황
		for (UINT nParaIdx = 0; nParaIdx < nCamCount; nParaIdx++)
		{
			varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
			m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);
			m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		}
	}

	// 스텝 결과
 	if (RC_OK == lReturn)
 	{
 		lReturn = _TI_SteCAL_WriteJSON(nStepIdx);
 
 		// * 검사 항목별 결과
 		for (UINT nParaIdx = 0; nParaIdx < nCamCount; nParaIdx++)
 		{
 			m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
 		}
 	}
 	else
 	{
 		// * 에러 상황
 		for (UINT nParaIdx = 0; nParaIdx < nCamCount; nParaIdx++)
 		{
 			m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
 		}
 	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_SteCAL_JudgeResult
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nTestItem
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/5 - 13:53
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::_TI_SteCAL_JudgeResult(__in UINT nStepIdx, __in UINT nTestItem, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;
	CArray <COleVariant, COleVariant&> VarArray;
	COleVariant varResult;

	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[nTestItem].vt);

 	switch (nTestItem)
 	{
	case TI_Ste_Re_M_Focal_Len_X:			// Single
	{
	 	varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stSte_Result.MonoCal.fx;
	 	VarArray.Add(varResult);
	}
	break;

	case TI_Ste_Re_M_Focal_Len_Y:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stSte_Result.MonoCal.fy;
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_M_Principle_X:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stSte_Result.MonoCal.cx;
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_M_Principle_Y:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stSte_Result.MonoCal.cy;
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_M_K1:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stSte_Result.MonoCal.distortion_cofficeints[0];
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_M_K2:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stSte_Result.MonoCal.distortion_cofficeints[1];
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_M_K3:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stSte_Result.MonoCal.distortion_cofficeints[2];
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_M_K4:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stSte_Result.MonoCal.distortion_cofficeints[3];
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_M_K5:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stSte_Result.MonoCal.distortion_cofficeints[4];
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_M_RMS:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stSte_Result.MonoCal.rms;
		VarArray.Add(varResult);
	}
		break;

 	case TI_Ste_Re_RotationVector_1:	// Stereo
 	case TI_Ste_Re_RotationVector_2:
 	case TI_Ste_Re_RotationVector_3:
 	case TI_Ste_Re_TranslationVector_1:
 	case TI_Ste_Re_TranslationVector_2:
 	case TI_Ste_Re_TranslationVector_3:
	case TI_Ste_Re_S_Focal_Len_X:			// Slave
	case TI_Ste_Re_S_Focal_Len_Y:
	case TI_Ste_Re_S_Principle_X:
	case TI_Ste_Re_S_Principle_Y:
	case TI_Ste_Re_S_K1:
	case TI_Ste_Re_S_K2:
	case TI_Ste_Re_S_K3:
	case TI_Ste_Re_S_K4:
	case TI_Ste_Re_S_K5:
	case TI_Ste_Re_S_RMS:
	case TI_Ste_Re_X:
	case TI_Ste_Re_Y:
	case TI_Ste_Re_Z:
	case TI_Ste_Re_Roll:
	case TI_Ste_Re_Pitch:
	case TI_Ste_Re_Yaw:
	case TI_Ste_Re_StereoRMS:
	default:
		break;
	}

	// 측정값 입력 및 판정
	// 카메라 개수
	UINT nCamCount = g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt;

	for (UINT nParaIdx = 0; nParaIdx < nCamCount; nParaIdx++)
	{
		m_stInspInfo.Set_Measurement(nParaIdx, nStepIdx, (UINT)VarArray.GetCount(), VarArray.GetData());
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_SteCAL_JudgeResult_Dual
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nTestItem
// Qualifier	:
// Last Update	: 2018/3/5 - 13:50
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::_TI_SteCAL_JudgeResult_Dual(__in UINT nStepIdx, __in UINT nTestItem)
{
	LRESULT lReturn = RC_OK;
	CArray <COleVariant, COleVariant&> VarArray;
	COleVariant varResult;

	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[nTestItem].vt);

 	switch (nTestItem)
 	{
	case TI_Ste_Re_M_Focal_Len_X:			// Single
	{
	 	varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.master_fx;
	 	VarArray.Add(varResult);
	}
	break;

	case TI_Ste_Re_M_Focal_Len_Y:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.master_fy;
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_M_Principle_X:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.master_cx;
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_M_Principle_Y:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.master_cy;
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_M_K1:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.master_distortion_cofficeints[0];
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_M_K2:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.master_distortion_cofficeints[1];
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_M_K3:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.master_distortion_cofficeints[2];
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_M_K4:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.master_distortion_cofficeints[3];
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_M_K5:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.master_distortion_cofficeints[4];
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_M_RMS:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.master_rms;
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_S_Focal_Len_X:			// Slave --------------------------
	{
	 	varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.slave_fx;
	 	VarArray.Add(varResult);
	}
	break;

	case TI_Ste_Re_S_Focal_Len_Y:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.slave_fy;
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_S_Principle_X:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.slave_cx;
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_S_Principle_Y:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.slave_cy;
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_S_K1:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.slave_distortion_cofficeints[0];
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_S_K2:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.slave_distortion_cofficeints[1];
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_S_K3:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.slave_distortion_cofficeints[2];
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_S_K4:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.slave_distortion_cofficeints[3];
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_S_K5:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.slave_distortion_cofficeints[4];
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_S_RMS:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.slave_rms;
		VarArray.Add(varResult);
	}
		break;

 	case TI_Ste_Re_RotationVector_1:	// Stereo
 	{
 		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.rotation_vector[0];
 		VarArray.Add(varResult);
 	}
 		break;
 
 	case TI_Ste_Re_RotationVector_2:
 	{
 		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.rotation_vector[1];
 		VarArray.Add(varResult);
 	}
 		break;
 
 	case TI_Ste_Re_RotationVector_3:
 	{
 		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.rotation_vector[2];
 		VarArray.Add(varResult);
 	}
 		break;
 
 	case TI_Ste_Re_TranslationVector_1:
 	{
 		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.translation_vector[0];
 		VarArray.Add(varResult);
 	}
 		break;
 
 	case TI_Ste_Re_TranslationVector_2:
 	{
 		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.translation_vector[1];
 		VarArray.Add(varResult);
 	}
 		break;
 
 	case TI_Ste_Re_TranslationVector_3:
 	{
 		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.translation_vector[2];
 		VarArray.Add(varResult);
 	}
 		break;

	case TI_Ste_Re_X:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.X;
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_Y:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.Y;
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_Z:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.Z;
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_Roll:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.roll;
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_Pitch:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.pitch;
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_Yaw:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.yaw;
		VarArray.Add(varResult);
	}
		break;

	case TI_Ste_Re_StereoRMS:
	{
		varResult.dblVal = m_stInspInfo.CamInfo[0].stSte_Result.StereoCal.streo_rms;
		VarArray.Add(varResult);
	}
		break;

	default:
		break;
	}

	// 측정값 입력 및 판정
	// 카메라 개수
	UINT nCamCount = g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt;

	for (UINT nParaIdx = 0; nParaIdx < nCamCount; nParaIdx++)
	{
		m_stInspInfo.Set_Measurement(nParaIdx, nStepIdx, (UINT)VarArray.GetCount(), VarArray.GetData());
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_SteCAL_WriteEEPROM
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Qualifier	:
// Last Update	: 2018/2/21 - 11:21
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::_TI_SteCAL_WriteEEPROM(__in UINT nStepIdx)
{
	LRESULT lReturn = RC_OK;

	if (OpMode_Production == m_stInspInfo.OperateMode) // 생산 모드인 경우에만 사용
	{
		// *** 추가 재시도 횟수
		UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;

		// 카메라 개수
		UINT nCamCount = g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt;

		// 현재 ICK 모델만 EEPROM 기록 (MRA2 모델 미정)
		LRESULT lReturnPara = RC_OK;


		switch (m_stInspInfo.RecipeInfo.ModelType)
		{
		case Model_IKC:// IKC 모델
		{
			for (UINT nCnt = 0; nCnt <= nRetryCnt; nCnt++)
			{
				lReturnPara = OnDAQ_EEPROM_Write(Para_Left);

				if (RC_OK == lReturnPara)
					break;
			}
		}
			break;

		case Model_MRA2:// MRA2모델
		{
			lReturnPara = _TI_SteCAL_WriteEEPROM_Dual(nStepIdx);
		}
			break;

		default:
			break;
		}

		// * 측정값 입력 (EEPROM 오류 코드)
		enResultCode_EEPROM nResultCode = Conv_ResultToEEPROMResult((enResultCode)lReturn); //g_szResultCode_EEPROM[nResultCode];

		COleVariant varResult;
		varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_Ste_WriteEEPROM].vt);
		//varResult.intVal = nResultCode;
		varResult.SetString(g_szResultCode_EEPROM[nResultCode], VT_BSTR);

		for (UINT nParaIdx = 0; nParaIdx < nCamCount; nParaIdx++)
		{
			m_stInspInfo.CamInfo[nParaIdx].nEEPROM_Result = nResultCode;
			m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

			// * 스텝 결과
			if (RC_OK == lReturnPara)
			{
				// 검사 항목별 결과
				m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
			}
			else
			{
				// 에러 상황
				lReturn = lReturnPara;
				m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
			}
		}
	}
	else
	{
		m_stInspInfo.Set_Judgment(Para_Left, nStepIdx, JUDGE_OK);
		m_stInspInfo.Set_Judgment(Para_Right, nStepIdx, JUDGE_OK);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_SteCAL_WriteEEPROM_Dual
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Qualifier	:
// Last Update	: 2018/5/31 - 14:54
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::_TI_SteCAL_WriteEEPROM_Dual(__in UINT nStepIdx)
{
	LRESULT lReturn = RC_OK;

	for (UINT nParaIdx = 0; nParaIdx < USE_CHANNEL_CNT; nParaIdx++)
	{
		if (IsTesting_Unit(nParaIdx))
		{
			TRACE(_T("_TI_SteCAL_WriteEEPROM_Dual() Error : Already Testing [%02d]\n"), nParaIdx);
			return RC_AlreadyTesting;
		}
	}

	// EEPROM Write Time 세팅
	GetLocalTime(&m_tmEEPROMWrite);

	//  개별 검사 쓰레드 실행
	HANDLE	hEventz[MAX_OPERATION_THREAD] = { NULL, };
	UINT	nThrCount = 0;

	for (UINT nParaIdx = 0; nParaIdx < USE_CHANNEL_CNT; nParaIdx++)
	{
		// 제품 초기화 상태이거나 검사 진행이 Pass 상태이면 다음 검사 계속 진행
		if (StartOperation_Test_Unit(nParaIdx, TTT_EEPROM_Write, nStepIdx))
		{
			hEventz[nThrCount++] = m_hThrTest_Unit[nParaIdx];
		}

		Sleep(150);
	}

	// 모든 쓰레드 종료 대기
	DWORD dwEvent = WaitForMultipleObjects(nThrCount, hEventz, TRUE, 60000);
	if (WAIT_TIMEOUT == dwEvent)
	{
		TRACE(_T("_TI_SteCAL_WriteEEPROM_Dual() Error : WAIT_TIMEOUT \n"));
		; // 타임 아웃 처리?
	}
	else if (WAIT_FAILED == dwEvent)
	{
		TRACE(_T("_TI_SteCAL_WriteEEPROM_Dual() Error : WAIT_FAILED \n"));
		;
	}

	// 양불 판정
// 	for (UINT nParaIdx = 0; nParaIdx < USE_CHANNEL_CNT; nParaIdx++)
// 	{
// 		if (FALSE == m_stInspInfo.CamInfo[nParaIdx].bInitialize)
// 		{
// 			lReturn = RC_UnknownError;
// 		}
// 	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_SteCAL_WriteJSON
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Qualifier	:
// Last Update	: 2018/3/5 - 14:07
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::_TI_SteCAL_WriteJSON(__in UINT nStepIdx)
{
	LRESULT lReturn = RC_OK;

	CFile_Report	fileRep;
	CString			szFileName;
	CString			szFullPath;
	SYSTEMTIME		tmLocal;
	GetLocalTime(&tmLocal);

	// single/stereo/날짜/바코드_시간.json
	if (enModelType::Model_IKC == m_stInspInfo.Get_ModelType())
	{
		szFileName.Format(_T("%s_Mono_%04d%02d%02d_%02d%02d%02d.json"), m_stInspInfo.CamInfo[Para_Left].szBarcode, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay,
													tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
	}
	else
	{
		szFileName.Format(_T("%s_Stereo_%04d%02d%02d_%02d%02d%02d.json"), m_stInspInfo.CamInfo[Para_Left].szBarcode, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay,
													tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
	}

	szFullPath.Format(_T("%s\\JSON\\%04d_%02d_%02d\\"), m_stInspInfo.Path.szReport, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay);
	MakeDirectory(szFullPath);

	szFullPath += szFileName;

	if (enModelType::Model_IKC == m_stInspInfo.Get_ModelType())
	{
		fileRep.SaveJSON_StereoCAL_Mono_Raw(szFullPath.GetBuffer(0), &m_stInspInfo.CamInfo[Para_Left].stSte_Result.MonoCal);
	}
	else
	{
		fileRep.SaveJSON_StereoCAL_Stereo_Raw(szFullPath.GetBuffer(0), &m_stInspInfo.CamInfo[Para_Left].stSte_Result.StereoCal);
	}

	return lReturn;
}

//=============================================================================
// Method		: GetJudge_CalibrationResult
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in UINT nStartStepIdx
// Qualifier	:
// Last Update	: 2018/4/4 - 19:01
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP_SteCal::GetJudge_CalibrationResult(__in UINT nStartStepIdx, __out UINT& nFailTestItem)
{
	BOOL bPass = TRUE;
	LRESULT lReturn = RC_OK;

	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;
	INT_PTR iStepCnt = m_stInspInfo.RecipeInfo.StepInfo.GetCount();

	// * 설정된 스텝 진행
	for (INT nStepIdx = nStartStepIdx; nStepIdx < iStepCnt; nStepIdx++)
	{
		lReturn = RC_OK;

		// * 검사 시작
		if (pStepInfo->StepList[nStepIdx].bTest)
		{
			if ((TI_Ste_Re_M_Focal_Len_X <= pStepInfo->StepList[nStepIdx].nTestItem) && (pStepInfo->StepList[nStepIdx].nTestItem <= TI_Ste_Re_StereoRMS))
			{
				// 
				if (enModelType::Model_IKC == m_stInspInfo.Get_ModelType())
				{
					lReturn = _TI_SteCAL_JudgeResult(nStepIdx, pStepInfo->StepList[nStepIdx].nTestItem, Para_Left);
				}
				else
				{
					lReturn = _TI_SteCAL_JudgeResult_Dual(nStepIdx, pStepInfo->StepList[nStepIdx].nTestItem);
				}

				// 스펙 판정
				if (TR_Pass != m_stInspInfo.GetTestItemMeas(nStepIdx, Para_Left)->nJudgmentAll)
				{
					bPass = FALSE;
					nFailTestItem = pStepInfo->StepList[nStepIdx].nTestItem;
					break;
				}
			}
		}
	} // 스텝 루프 종료

	return bPass;
}

//=============================================================================
// Method		: GetFailTestItem_CalResult
// Access		: virtual protected  
// Returns		: BOOL						-> 판정 : Pass/Fail
// Parameter	: __out UINT & nOutTestItem	-> 우선 순위에 따른 Fail 검사항목
// Qualifier	:
// Last Update	: 2018/4/5 - 10:54
// Desc.		: 프리셋 모드 사용 할 때에 사용하는 함수
//=============================================================================
BOOL CTestManager_EQP_SteCal::GetFailTestItem_CalResult(__out UINT& nOutTestItem)
{
	BOOL bPass = TRUE;
#ifdef USE_PRESET_MODE

	UINT				nTestItemID = 0;
	ST_PresetStepInfo*	pPreset		= &m_stInspInfo.RecipeInfo.stPresetStepInfo;
	ST_TestItemMeas*	pMeasItem	= NULL;

	UINT nMaxCount = (pPreset->nUsePresetFileCount < MAX_TESTSTEP_PRESET) ? pPreset->nUsePresetFileCount : MAX_TESTSTEP_PRESET;
	for (UINT nOrderIdx = 0; nOrderIdx < nMaxCount; nOrderIdx++)
	{
		// 우선 순위에 따른 검사 항목 ID
		nTestItemID = pPreset->stPresetLink[nOrderIdx].nPresetType;

		if ((TI_Ste_Re_M_Focal_Len_X <= nTestItemID) && (nTestItemID <= TI_Ste_Re_StereoRMS))
		{
			pMeasItem = m_stInspInfo.GetMeasurmentData(nTestItemID, Para_Left);

			if (TR_Pass != pMeasItem->nJudgmentAll)
			{
				bPass = FALSE;
				nOutTestItem = nTestItemID;
				break;
			}
		}
	}
	
#endif
	return bPass;
}

//=============================================================================
// Method		: GetPresetIndex
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in UINT nInTestItem
// Parameter	: __out UINT & nOutPresetIndex
// Qualifier	:
// Last Update	: 2018/4/6 - 10:38
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP_SteCal::GetPresetIndex(__in UINT nInTestItem, __out UINT& nOutPresetIndex)
{
	BOOL bReturn = FALSE;
#ifdef USE_PRESET_MODE
	ST_PresetStepInfo* pPreset = &m_stInspInfo.RecipeInfo.stPresetStepInfo;

	for (UINT nIdx = 0; nIdx < pPreset->nUsePresetFileCount; nIdx++)
	{
		if (nInTestItem == pPreset->stPresetLink[nIdx].nPresetType)
		{
			nOutPresetIndex = nIdx;
			bReturn = TRUE;
			break;
		}
	}
#endif
	return bReturn;
}

//=============================================================================
// Method		: WriteLog_DetectPattern
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in UINT nCornerStep
// Parameter	: __in BOOL bError
// Qualifier	:
// Last Update	: 2018/4/6 - 11:24
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP_SteCal::WriteLog_DetectPattern(__in UINT nCornerStep, __in BOOL bError /*= FALSE*/)
{
	BOOL bReturn = TRUE;

	ST_CamInfo* pCam = &m_stInspInfo.CamInfo[Para_Left];

	if (enModelType::Model_IKC == m_stInspInfo.Get_ModelType())
	{
		// Mono
		m_FileReport.Save_SteCal_DetectPattern(m_stInspInfo.Path.szReport, pCam->szBarcode, FALSE, &pCam->TestTime.tmStart_Loading, nCornerStep, &pCam->stSte_Result, bError);
	}
	else
	{
		// Stereo
		m_FileReport.Save_SteCal_DetectPattern(m_stInspInfo.Path.szReport, pCam->szBarcode, TRUE, &pCam->TestTime.tmStart_Loading, nCornerStep, &pCam->stSte_Result, bError);
	}

	return bReturn;
}

//=============================================================================
// Method		: OnLib_SteCAL_SetParameter
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/23 - 14:51
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnLib_SteCAL_SetParameter()
{
	LRESULT lReturn = RC_OK;
	BOOL bResult = TRUE;

	enModelType nModelType = m_stInspInfo.Get_ModelType();
	
	int iBoard_W = m_stInspInfo.RecipeInfo.stSte_CAL.Parameter.iBoard_Width;
	int iBoard_H = m_stInspInfo.RecipeInfo.stSte_CAL.Parameter.iBoard_Height;

#ifndef NOT_USE_STEREO_ALGORITHM
	if (1 < g_IR_ModelTable[nModelType].Camera_Cnt)
	{
		bResult = m_CalStereo.Set_Parameter_Stereo(iBoard_W, iBoard_H);
	}
	else
	{
		bResult = m_CalStereo.Set_Parameter_Mono(iBoard_W, iBoard_H);
	}
#endif

	if (bResult)
	{
		TRACE(_T("OnLib_SteCAL_SetParameter() : Succeed \n"));
	}
	else
	{
		TRACE(_T("OnLib_SteCAL_SetParameter() : Failed \n"));
		lReturn = RC_LIB_SteCAL_SetParam;
	}

	return lReturn;
}

//=============================================================================
// Method		: OnLib_SteCAL_DetectPattern
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nCornerStep
// Qualifier	:
// Last Update	: 2018/2/26 - 10:06
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnLib_SteCAL_DetectPattern(__in UINT nCornerStep)
{
	LRESULT lReturn = RC_OK;
	BOOL bResult = TRUE;

	enModelType nModelType = m_stInspInfo.Get_ModelType();

	int iWidth = (int)m_stImageBuf[Para_Left].dwWidth;
	int iHeight = (int)m_stImageBuf[Para_Left].dwHeight;
	ST_SteCal_Result*	pResult = &m_stInspInfo.CamInfo[0].stSte_Result;

#ifndef NOT_USE_STEREO_ALGORITHM
	if (1 < g_IR_ModelTable[nModelType].Camera_Cnt)
	{
		bResult = m_CalStereo.Detect_Pattern_Stereo(m_stImageBuf[Para_Left].lpbyImage_8bit, m_stImageBuf[Para_Right].lpbyImage_8bit, MRA2_IMAGE_FORMAT_24_RGB, iWidth, iHeight, &pResult->ptPattern_M[nCornerStep][0], &pResult->nPtCount_M[nCornerStep], &pResult->ptPattern_S[nCornerStep][0], &pResult->nPtCount_S[nCornerStep]);
		//bResult = m_CalStereo.Detect_Pattern_Stereo((LPBYTE)m_stImageBuf[Para_Left].lpwImage_16bit, (LPBYTE)m_stImageBuf[Para_Right].lpwImage_16bit, MRA2_IMAGE_FORMAT_16_GRAY, iWidth, iHeight, &pResult->ptPattern_M[nCornerStep], &pResult->nPtCount_M[nCornerStep], &pResult->ptPattern_S[nCornerStep], &pResult->nPtCount_S[nCornerStep]);
	}
	else
	{
		bResult = m_CalStereo.Detect_Pattern_Mono(m_stImageBuf[Para_Left].lpbyImage_8bit, MRA2_IMAGE_FORMAT_24_RGB, iWidth, iHeight, &pResult->ptPattern_M[nCornerStep][0], &pResult->nPtCount_M[nCornerStep]);
		//bResult = m_CalStereo.Detect_Pattern_Mono((LPBYTE)m_stImageBuf[Para_Left].lpwImage_16bit, MRA2_IMAGE_FORMAT_16_GRAY, iWidth, iHeight, &pResult->ptPattern_M[nCornerStep], &pResult->nPtCount_M[nCornerStep]);
	}
#endif

	if (bResult)
	{
		TRACE(_T("OnLib_SteCAL_DetectPattern() : Succeed \n"));
	}
	else
	{
		TRACE(_T("OnLib_SteCAL_DetectPattern() : Failed \n"));
		OnLog_Err(_T("Stereo CAL DLL : OnLib_SteCAL_DetectPattern() is Failed [Step %d]"), nCornerStep);
		//lReturn = RC_LIB_SteCAL_CornerExt; // 임시 주석
	}

	return lReturn;
}

//=============================================================================
// Method		: OnLib_SteCAL_ExecIntrinsic
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nOptionFlag
// Qualifier	:
// Last Update	: 2018/7/3 - 11:06
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnLib_SteCAL_ExecIntrinsic(__in UINT nOptionFlag /*= 0x02*/)
{
	LRESULT lReturn = RC_OK;
	BOOL bResult = TRUE;

	enModelType nModelType = m_stInspInfo.Get_ModelType();

#ifndef NOT_USE_STEREO_ALGORITHM
	if (1 < g_IR_ModelTable[nModelType].Camera_Cnt) // MRA2
	{
		//ZeroMemory(&stCalResult, sizeof(STEREO_CALIBRATION_RESULT));

		bResult = m_CalStereo.Exec_CalIntrinsic_Setreo((MRA2_CAMERA_FLAG)nOptionFlag, m_stInspInfo.CamInfo[Para_Left].stSte_Result.StereoCal);

		// ** 데이터 보정
		STEREO_CALIBRATION_RESULT* pstStereo = &m_stInspInfo.CamInfo[Para_Left].stSte_Result.StereoCal;

		pstStereo->master_fx	= pstStereo->master_fx * -1;
		pstStereo->master_fy	= pstStereo->master_fy * -1;
		pstStereo->master_cy	= pstStereo->master_cy;
		pstStereo->slave_fx		= pstStereo->slave_fx * -1;
		pstStereo->slave_fy		= pstStereo->slave_fy * -1;
		pstStereo->slave_cy		= pstStereo->slave_cy;

		for (UINT nIdx = 0; nIdx < 3; nIdx++)
		{
			pstStereo->translation_vector[nIdx] = pstStereo->translation_vector[nIdx] * 0.001;
		}

		pstStereo->X = abs(pstStereo->X) * -1;

		//m_CalStereo.Close_CalHandle_Stereo();
	}
	else // IKC
	{
		//ZeroMemory(&stCalResult, sizeof(SINGLE_CALIBRATION_RESULT));

		bResult = m_CalStereo.Exec_CalIntrinsic_Mono((MRA2_CAMERA_FLAG)nOptionFlag, m_stInspInfo.CamInfo[Para_Left].stSte_Result.MonoCal);

		// ** 데이터 보정
		SINGLE_CALIBRATION_RESULT* pstSingle = &m_stInspInfo.CamInfo[Para_Left].stSte_Result.MonoCal;
		pstSingle->fx = pstSingle->fx * -1;
		pstSingle->fy = pstSingle->fy * -1;
		pstSingle->cy = pstSingle->cy;

		//m_CalStereo.Close_CalHandle_Mono();
	}
#endif

	if (bResult)
	{
		TRACE(_T("OnLib_SteCAL_ExecIntrinsic() : Succeed \n"));
	}
	else
	{
		TRACE(_T("OnLib_SteCAL_ExecIntrinsic() : Failed \n"));
		lReturn = RC_LIB_SteCAL_ExecIntrinsic;
	}

	// Log 남기기

	return lReturn;
}

//=============================================================================
// Method		: OnLib_SteCAL_ExecIntrinsic_File
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/3/7 - 16:13
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnLib_SteCAL_ExecIntrinsic_File()
{
	LRESULT lReturn = RC_OK;
	BOOL bResult = TRUE;

	// 폴더명 / left or / right

// 	m_stImageBuf[Para_Left].lpbyImage_8bit;
// 	m_stImageBuf[Para_Right].lpbyImage_8bit;
// 
// 	OnLoadImage_RGB()

	return lReturn;
}

//=============================================================================
// Method		: OnLib_SteCAL_CloseHandle
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/4/6 - 18:06
// Desc.		:
//=============================================================================
void CTestManager_EQP_SteCal::OnLib_SteCAL_CloseHandle()
{
#ifndef NOT_USE_STEREO_ALGORITHM
	if (1 < g_IR_ModelTable[m_stInspInfo.Get_ModelType()].Camera_Cnt)
	{
		m_CalStereo.Close_CalHandle_Stereo();
	}
	else
	{
		m_CalStereo.Close_CalHandle_Mono();
	}
#endif
}

//=============================================================================
// Method		: MES_CheckBarcodeValidation
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/4 - 16:25
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::MES_CheckBarcodeValidation(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::MES_CheckBarcodeValidation(nParaIdx);



	return lReturn;
}

//=============================================================================
// Method		: MES_SendInspectionData
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/4 - 16:39
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::MES_SendInspectionData(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	if (enOperateMode::OpMode_Production == m_stInspInfo.OperateMode)
	{
		lReturn = MES_MakeInspecData_SteCAL(nParaIdx);

		BOOL bJudge = (TR_Pass == m_stInspInfo.Judgment_All) ? TRUE : FALSE;

		SYSTEMTIME tmLocal;
		GetLocalTime(&tmLocal);

		// MES 로그 남기기
		//m_FileMES.SaveMESFile(m_stInspInfo.CamInfo[0].szBarcode, m_stInspInfo.CamInfo[0].nMES_TryCnt, bJudge, &m_stInspInfo.CamInfo[0].TestTime.tmStart_Loading);
		m_FileMES.SaveMESFile(m_stInspInfo.CamInfo[0].szBarcode, m_stInspInfo.CamInfo[0].nMES_TryCnt, bJudge, &tmLocal);
	}

	return lReturn;
}

//=============================================================================
// Method		: MES_MakeInspecData_SteCAL
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/9 - 19:16
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::MES_MakeInspecData_SteCAL(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	// MES 데이터 만들기
	//m_FileMES.Reset_ItemData();
	m_FileMES.Reset();

	BOOL bJudge = (TR_Pass == m_stInspInfo.Judgment_All) ? TRUE : FALSE;

	m_FileMES.Set_EquipmnetID(m_stInspInfo.szEquipmentID);
	m_FileMES.Set_Path(m_stOption.MES.szPath_MESLog);
	m_FileMES.Set_Barcode(m_stInspInfo.CamInfo[nParaIdx].szBarcode, m_stInspInfo.CamInfo[nParaIdx].nMES_TryCnt, bJudge, &m_stInspInfo.CamInfo[nParaIdx].TestTime.tmStart_Loading);

	if (1 < g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt)
	{// Stereo
		MES_Make_SteCAL_Master();		// * Master
		MES_Make_SteCAL_Slave();		// * Slave
		MES_Make_SteCAL_Stereo();		// * Stereo
	}
	else
	{// Single
		MES_Make_SteCAL_Mono();		// * Master
		MES_Make_SteCAL_Slave_Dummy();	// * Slave
		MES_Make_SteCAL_Stereo_Dummy();	// * Stereo
	}

	return lReturn;
}

//=============================================================================
// Method		: MES_Make_SteCAL_Mono
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/5 - 18:53
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::MES_Make_SteCAL_Mono(__in UINT nParaIdx /*= 0*/)
{
	ST_TestItemMeas* pMeasItem = NULL;
	CString		NAME;				// 항목 명칭	
	CString		VALUE;				// 데이터
	BOOL		JUDGE = TRUE;		// 판정

	// * Single (Master)

	// * Focal_Len_X
	NAME = _T("master focal_len x");
	VALUE.Format(_T("%f"), m_stInspInfo.CamInfo[nParaIdx].stSte_Result.MonoCal.fx);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_M_Focal_Len_X, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * Focal_Len_Y
	NAME = _T("master focal_len x");
	VALUE.Format(_T("%f"), m_stInspInfo.CamInfo[nParaIdx].stSte_Result.MonoCal.fy);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_M_Focal_Len_Y, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * Principle_X
	NAME = _T("master principle x");
	VALUE.Format(_T("%f"), m_stInspInfo.CamInfo[nParaIdx].stSte_Result.MonoCal.cx);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_M_Principle_X, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * Principle_Y
	NAME = _T("master principle y");
	VALUE.Format(_T("%f"), m_stInspInfo.CamInfo[nParaIdx].stSte_Result.MonoCal.cy);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_M_Principle_Y, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * K1 ~ K5
	for (UINT nIdx = 0; nIdx < 5; nIdx++)
	{
		//NAME = _T("master K1");
		NAME.Format(_T("master K%d"), nIdx + 1);
		VALUE.Format(_T("%f"), m_stInspInfo.CamInfo[nParaIdx].stSte_Result.MonoCal.distortion_cofficeints[nIdx]);

		pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_M_K1 + nIdx, nParaIdx);
		if (NULL != pMeasItem)
		{
			JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
		}
		else
		{
			JUDGE = TRUE;
		}
		m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);
	}

	// * RMS
	NAME = _T("master rms");
	VALUE.Format(_T("%f"), m_stInspInfo.CamInfo[nParaIdx].stSte_Result.MonoCal.rms);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_M_RMS, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	return RC_OK;
}

//=============================================================================
// Method		: MES_Make_SteCAL_Slave_Dummy
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/5 - 18:49
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::MES_Make_SteCAL_Slave_Dummy(__in UINT nParaIdx /*= 0*/)
{
	ST_TestItemMeas* pMeasItem = NULL;
	CString		NAME;				// 항목 명칭	
	CString		VALUE;				// 데이터
	BOOL		JUDGE = TRUE;		// 판정

	// * Singe (Slave)

	// * Focal_Len_X
	NAME = _T("slave focal_len x");
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * Focal_Len_Y
	NAME = _T("slave focal_len x");
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * Principle_X
	NAME = _T("slave principle x");
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * Principle_Y
	NAME = _T("slave principle y");
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * K1 ~ K5
	for (UINT nIdx = 0; nIdx < 5; nIdx++)
	{
		NAME.Format(_T("slave K%d"), nIdx + 1);
		m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);
	}

	// * RMS
	NAME = _T("slave rms");
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	return RC_OK;
}

//=============================================================================
// Method		: MES_Make_SteCAL_Stereo_Dummy
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/5 - 19:03
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::MES_Make_SteCAL_Stereo_Dummy(__in UINT nParaIdx /*= 0*/)
{
	ST_TestItemMeas* pMeasItem = NULL;
	CString		NAME;				// 항목 명칭	
	CString		VALUE;				// 데이터
	BOOL		JUDGE = TRUE;		// 판정

	// * Stereo -----------------------
	// 	* Rotation Vector 1
	NAME = _T("Rotation Vector 1");
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);
	// 	* Rotation Vector 2
	NAME = _T("Rotation Vector 2");
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);
	// 	* Rotation Vector 3
	NAME = _T("Rotation Vector 3");
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);
	
	// 	* Translation Vector 1
	NAME = _T("Translation Vector 1");
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);
	// 	* Translation Vector 2
	NAME = _T("Translation Vector 2");
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);
	// 	* Translation Vector 3
	NAME = _T("Translation Vector 3");
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// 	* X
	NAME = _T("X");
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * Y
	NAME = _T("Y");
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * Z
	NAME = _T("Z");
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * Roll
	NAME = _T("roll");
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * Pitch
	NAME = _T("pitch");
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * Yaw
	NAME = _T("yaw");
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * StereoRMS
	NAME = _T("stereo rms");
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	return RC_OK;
}

//=============================================================================
// Method		: MES_Make_SteCAL_Master
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/5 - 18:45
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::MES_Make_SteCAL_Master(__in UINT nParaIdx /*= 0*/)
{
	ST_TestItemMeas* pMeasItem = NULL;
	CString		NAME;				// 항목 명칭	
	CString		VALUE;				// 데이터
	BOOL		JUDGE = TRUE;		// 판정

	// * Stereo Master
	STEREO_CALIBRATION_RESULT* pResult = &m_stInspInfo.CamInfo[nParaIdx].stSte_Result.StereoCal;

	// * Focal_Len_X
	NAME = _T("master focal_len x");
	VALUE.Format(_T("%f"), pResult->master_fx);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_M_Focal_Len_X, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * Focal_Len_Y
	NAME = _T("master focal_len x");
	VALUE.Format(_T("%f"), pResult->master_fy);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_M_Focal_Len_Y, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * Principle_X
	NAME = _T("master principle x");
	VALUE.Format(_T("%f"), pResult->master_cx);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_M_Principle_X, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * Principle_Y
	NAME = _T("master principle y");
	VALUE.Format(_T("%f"), pResult->master_cy);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_M_Principle_Y, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * K1 ~ K5
	for (UINT nIdx = 0; nIdx < 5; nIdx++)
	{
		//NAME = _T("master K1");
		NAME.Format(_T("master K%d"), nIdx + 1);
		VALUE.Format(_T("%f"), pResult->master_distortion_cofficeints[nIdx]);

		pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_M_K1 + nIdx, nParaIdx);
		if (NULL != pMeasItem)
		{
			JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
		}
		else
		{
			JUDGE = TRUE;
		}
		m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);
	}

	// * RMS
	NAME = _T("master rms");
	VALUE.Format(_T("%f"), pResult->master_rms);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_M_RMS, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	return RC_OK;
}

//=============================================================================
// Method		: MES_Make_SteCAL_Slave
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/5 - 18:45
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::MES_Make_SteCAL_Slave(__in UINT nParaIdx /*= 0*/)
{
	ST_TestItemMeas* pMeasItem = NULL;
	CString		NAME;				// 항목 명칭	
	CString		VALUE;				// 데이터
	BOOL		JUDGE = TRUE;		// 판정

	// * Stereo Slave
	STEREO_CALIBRATION_RESULT* pResult = &m_stInspInfo.CamInfo[nParaIdx].stSte_Result.StereoCal;

	// * Focal_Len_X
	NAME = _T("slave focal_len x");
	VALUE.Format(_T("%f"), pResult->slave_fx);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_S_Focal_Len_X, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * Focal_Len_Y
	NAME = _T("slave focal_len x");
	VALUE.Format(_T("%f"), pResult->slave_fy);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_S_Focal_Len_Y, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * Principle_X
	NAME = _T("slave principle x");
	VALUE.Format(_T("%f"), pResult->slave_cx);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_S_Principle_X, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * Principle_Y
	NAME = _T("slave principle y");
	VALUE.Format(_T("%f"), pResult->slave_cy);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_S_Principle_Y, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * K1 ~ K5
	for (UINT nIdx = 0; nIdx < 5; nIdx++)
	{
		NAME.Format(_T("slave K%d"), nIdx + 1);
		VALUE.Format(_T("%f"), pResult->slave_distortion_cofficeints[nIdx]);

		pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_S_K1 + nIdx, nParaIdx);
		if (NULL != pMeasItem)
		{
			JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
		}
		else
		{
			JUDGE = TRUE;
		}
		m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);
	}

	// * RMS
	NAME = _T("slave rms");
	VALUE.Format(_T("%f"), pResult->slave_rms);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_S_RMS, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	return RC_OK;
}

//=============================================================================
// Method		: MES_Make_SteCAL_Stereo
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/5 - 19:02
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::MES_Make_SteCAL_Stereo(__in UINT nParaIdx /*= 0*/)
{
	ST_TestItemMeas* pMeasItem = NULL;
	CString		NAME;				// 항목 명칭	
	CString		VALUE;				// 데이터
	BOOL		JUDGE = TRUE;		// 판정

	// * Stereo -----------------------
	STEREO_CALIBRATION_RESULT* pResult = &m_stInspInfo.CamInfo[nParaIdx].stSte_Result.StereoCal;

	// 	* Rotation Vector 1
	NAME = _T("Rotation Vector 1");
	VALUE.Format(_T("%f"), pResult->rotation_vector[0]);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_RotationVector_1, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// 	* Rotation Vector 2
	NAME = _T("Rotation Vector 2");
	VALUE.Format(_T("%f"), pResult->rotation_vector[1]);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_RotationVector_2, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// 	* Rotation Vector 3
	NAME = _T("Rotation Vector 3");
	VALUE.Format(_T("%f"), pResult->rotation_vector[2]);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_RotationVector_3, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// 	* Translation Vector 1
	NAME = _T("Translation Vector 1");
	VALUE.Format(_T("%f"), pResult->translation_vector[0]);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_TranslationVector_1, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// 	* Translation Vector 2
	NAME = _T("Translation Vector 2");
	VALUE.Format(_T("%f"), pResult->translation_vector[1]);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_TranslationVector_2, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// 	* Translation Vector 3
	NAME = _T("Translation Vector 3");
	VALUE.Format(_T("%f"), pResult->translation_vector[2]);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_TranslationVector_3, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// 	* X
	NAME = _T("X");
	VALUE.Format(_T("%f"), pResult->X);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_X, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * Y
	NAME = _T("Y");
	VALUE.Format(_T("%f"), pResult->Y);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_Y, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * Z
	NAME = _T("Z");
	VALUE.Format(_T("%f"), pResult->Z);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_Z, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * Roll
	NAME = _T("roll");
	VALUE.Format(_T("%f"), pResult->roll);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_Roll, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * Pitch
	NAME = _T("pitch");
	VALUE.Format(_T("%f"), pResult->pitch);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_Pitch, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * Yaw
	NAME = _T("yaw");
	VALUE.Format(_T("%f"), pResult->yaw);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_Yaw, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	// * StereoRMS
	NAME = _T("stereo rms");
	VALUE.Format(_T("%f"), pResult->streo_rms);

	pMeasItem = m_stInspInfo.GetMeasurmentData(TI_Ste_Re_StereoRMS, nParaIdx);
	if (NULL != pMeasItem)
	{
		JUDGE = (TR_Pass == pMeasItem->nJudgmentAll) ? TRUE : FALSE;
	}
	else
	{
		JUDGE = TRUE;
	}
	m_FileMES.Add_ItemData(NAME, VALUE, JUDGE);

	return RC_OK;
}

//=============================================================================
// Method		: OnPopupMessageTest_All
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enResultCode nResultCode
// Qualifier	:
// Last Update	: 2016/7/21 - 18:59
// Desc.		:
//=============================================================================
void CTestManager_EQP_SteCal::OnPopupMessageTest_All(__in enResultCode nResultCode)
{
	CTestManager_EQP::OnPopupMessageTest_All(nResultCode);


}

//=============================================================================
// Method		: OnMotion_MoveToTestPos
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/10/19 - 20:42
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnMotion_MoveToTestPos(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_MoveToTestPos(nParaIdx);



	return lReturn;
}

//=============================================================================
// Method		: OnMotion_MoveToStandbyPos
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/15 - 9:58
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnMotion_MoveToStandbyPos(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_MoveToStandbyPos(nParaIdx);



	return lReturn;
}

LRESULT CTestManager_EQP_SteCal::OnMotion_MoveToUnloadPos(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_MoveToUnloadPos(nParaIdx);



	return lReturn;
}

//=============================================================================
// Method		: OnMotion_Origin
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/11/15 - 3:27
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnMotion_Origin()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_Origin();



	return lReturn;
}

//=============================================================================
// Method		: OnMotion_LoadProduct
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/11/15 - 3:05
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnMotion_LoadProduct()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_LoadProduct();



	return lReturn;
}

//=============================================================================
// Method		: OnMotion_UnloadProduct
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/11/15 - 3:16
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnMotion_UnloadProduct()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_UnloadProduct();



	return lReturn;
}

LRESULT CTestManager_EQP_SteCal::OnMotion_SteCal_Test(__in double dbDistanceX, __in double dbDistanceY, __in double dbDistanceZ, __in double dbChartTX, __in double dbChartTZ, __in BOOL bUseX, __in BOOL bUseY, __in BOOL bUseZ, __in BOOL bUseTX, __in BOOL bUseTZ, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

#ifndef MOTION_NOT_USE

	// * 정해진 위치로 모터 동작
	lReturn = m_MotionSequence.OnActionTesting_Stereo(dbDistanceX, dbDistanceY, dbDistanceZ, dbChartTX, dbChartTZ, bUseX, bUseY, bUseZ, bUseTX, bUseTZ);
	
#endif

	return lReturn;
}

//=============================================================================
// Method		: OnDIn_DetectSignal
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BYTE byBitOffset
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2018/2/1 - 17:22
// Desc.		:
//=============================================================================
void CTestManager_EQP_SteCal::OnDIn_DetectSignal(__in BYTE byBitOffset, __in BOOL bOnOff)
{
	//CTestManager_EQP::OnDIn_DetectSignal(byBitOffset, bOnOff);
	
	OnDIn_DetectSignal_SteCal(byBitOffset, bOnOff);
}

 //=============================================================================
 // Method		: OnDIn_DetectSignal_SteCal
 // Access		: virtual protected  
 // Returns		: void
 // Parameter	: __in BYTE byBitOffset
 // Parameter	: __in BOOL bOnOff
 // Qualifier	:
 // Last Update	: 2018/3/31 - 11:10
 // Desc.		:
 //=============================================================================
 void CTestManager_EQP_SteCal::OnDIn_DetectSignal_SteCal(__in BYTE byBitOffset, __in BOOL bOnOff)
 {
 	enDI_Stereo nDI_Signal = (enDI_Stereo)byBitOffset;
 
 	switch (nDI_Signal)
 	{
 	case DI_St_00_MainPower:
 		if (FALSE == bOnOff)
 		{
 			OnDIn_MainPower();
 		}
 		break;
 
 	case DI_St_01_EMO:
 		if (FALSE == bOnOff)
 		{
 			OnDIn_EMO();
 		}
 		break;
 
 	case DI_St_02_AreaSensor:
 		if (FALSE == bOnOff)
 		{
 			OnDIn_AreaSensor();
 		}
 		break;
 
 	case DI_St_03_DoorSensor:
 		if (FALSE == bOnOff)
 		{
 			OnDIn_DoorSensor();
 		}
 		break;
 
 	case DI_St_04_JIG_CoverCheck:
 		if (bOnOff)
 		{
 			OnDIn_JIGCoverCheck();
 		}
 		break;
 	
 	case DI_St_06_Start:
 	{
 		if (bOnOff)
 		{
 			StartOperation_LoadUnload(Product_Load);
 		}
 		
 	}
 		break;
 
 	case DI_St_07_Stop:
 	{
 		if (bOnOff)
 		{
 			StopProcess_Test_All();
 		}
 
 	}
 		break;
 	
 	default:
 		break;
 	}
 
 }

//=============================================================================
// Method		: OnDIO_InitialSignal
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/4 - 17:33
// Desc.		:
//=============================================================================
void CTestManager_EQP_SteCal::OnDIO_InitialSignal()
{
	CTestManager_EQP::OnDIO_InitialSignal();


}

//=============================================================================
// Method		: OnDIn_MainPower
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 19:48
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnDIn_MainPower()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_MainPower();

	AfxMessageBox(_T("I/O : Main Power Down  !!"), MB_SYSTEMMODAL);

	return lReturn;
}

//=============================================================================
// Method		: OnDIn_EMO
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 20:12
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnDIn_EMO()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_EMO();

	AfxMessageBox(_T("I/O : EMO Occurred  !!"), MB_SYSTEMMODAL);

	return lReturn;
}

//=============================================================================
// Method		: OnDIn_AreaSensor
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 20:12
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnDIn_AreaSensor()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_AreaSensor();

	if (IsTesting())
		AfxMessageBox(_T("I/O : Detected Area Sesnor  !!"), MB_SYSTEMMODAL);

	return lReturn;
}

//=============================================================================
// Method		: OnDIn_DoorSensor
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 20:12
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnDIn_DoorSensor()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_DoorSensor();

	if (IsTesting())
		AfxMessageBox(_T("I/O : Door is Opened !!"), MB_SYSTEMMODAL);

	return lReturn;
}

//=============================================================================
// Method		: OnDIn_JIGCoverCheck
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 19:41
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnDIn_JIGCoverCheck()
{
	LRESULT lReturn = RC_OK;

	if (m_stOption.Inspector.bUseJIGCoverSensor)
	{
		//lReturn = CTestManager_EQP::OnDIn_JIGCoverCheck();

		// * JIG Cover 체크 (Clear : 커버 덮은 상태, Set : 커버 열린 상태)
		// * 검사 진행 중 커버 열리면 오류 처리
		if (IsTesting())
		{
			if (!m_stInspInfo.byDIO_DI[DI_2D_04_JIG_CoverCheck])
			{
				lReturn = RC_DIO_Err_JIGCorverCheck;

				OnLog_Err(_T("I/O : JIG Cover is Opened !!"));
				OnAddAlarm_F(_T("I/O : JIG Cover is Opened !!"));

				StopProcess_Test_All();

				AfxMessageBox(_T("I/O : Opened JIG Cover!!"), MB_SYSTEMMODAL);
			}
		}
	}

	return lReturn;
}

LRESULT CTestManager_EQP_SteCal::OnDIn_Start()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_Start();



	return lReturn;
}

LRESULT CTestManager_EQP_SteCal::OnDIn_Stop()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_Stop();



	return lReturn;
}

//=============================================================================
// Method		: OnDIn_CheckSafetyIO
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 18:36
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnDIn_CheckSafetyIO()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_CheckSafetyIO();



	return lReturn;
}

//=============================================================================
// Method		: OnDIn_CheckJIGCoverStatus
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/3/19 - 10:26
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnDIn_CheckJIGCoverStatus()
{
	LRESULT lReturn = RC_OK;

	if (m_stOption.Inspector.bUseJIGCoverSensor)
	{
		//lReturn = CTestManager_EQP::OnDIn_CheckJIGCoverStatus();

		// IKC 커버 센서 없음
		// 	if (Model_IKC == m_stInspInfo.RecipeInfo.ModelType)
		// 		return lReturn;

		// * JIG Cover 체크 (Clear : 커버 덮은 상태, Set : 커버 열린 상태)
		// 	if (!m_stInspInfo.byDIO_DI[DI_St_04_JIG_CoverCheck])
		// 	{
		// 		lReturn = RC_DIO_Err_JIGCorverCheck;
		// 	}
	}


	return lReturn;
}

//=============================================================================
// Method		: OnDOut_BoardPower
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bOn
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/6 - 16:55
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnDOut_BoardPower(__in BOOL bOn, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_BoardPower(bOn, nParaIdx);

	return lReturn;
}

LRESULT CTestManager_EQP_SteCal::OnDOut_FluorescentLamp(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_FluorescentLamp(bOn);

	return lReturn;
}

LRESULT CTestManager_EQP_SteCal::OnDOut_StartLamp(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_StartLamp(bOn);

	return lReturn;
}

LRESULT CTestManager_EQP_SteCal::OnDOut_StopLamp(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_StopLamp(bOn);

	return lReturn;
}

//=============================================================================
// Method		: OnDOut_TowerLamp
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in enLampColor nLampColor
// Parameter	: __in BOOL bOn
// Qualifier	:
// Last Update	: 2018/3/31 - 11:01
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnDOut_TowerLamp(__in enLampColor nLampColor, __in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	//lReturn = CTestManager_EQP::OnDOut_TowerLamp(nLampColor, bOn);

	enum_IO_SignalType enSignalType;

	if (ON == bOn)
	{
		enSignalType = IO_SignalT_SetOn;
	}
	else
	{
		enSignalType = IO_SignalT_SetOff;
	}

	switch (nLampColor)
	{
	case Lamp_Red:
		lReturn = m_Device.DigitalIOCtrl.Set_DO_Status(DO_St_12_TowerLamp_Red, enSignalType);
		break;
	case Lamp_Yellow:
		lReturn = m_Device.DigitalIOCtrl.Set_DO_Status(DO_St_13_TowerLamp_Yellow, enSignalType);
		break;
	case Lamp_Green:
		lReturn = m_Device.DigitalIOCtrl.Set_DO_Status(DO_St_14_TowerLamp_Green, enSignalType);
		break;
	case Lamp_All:
		for (UINT nIdx = 0; nIdx < 3; nIdx++)
		{
			lReturn = m_Device.DigitalIOCtrl.Set_DO_Status(DO_St_12_TowerLamp_Red + nIdx, enSignalType);

			if (RC_OK != lReturn)
				break;
		}
		break;
	default:
		break;
	}

	return lReturn;
}

//=============================================================================
// Method		: OnDOut_Buzzer
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bOn
// Qualifier	:
// Last Update	: 2018/6/11 - 13:32
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnDOut_Buzzer(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_Buzzer(bOn);

	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_EEPROM_Write
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/2/12 - 14:37
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnDAQ_EEPROM_Write(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;

#ifndef NOT_USE_STEREO_ALGORITHM
	switch (m_stInspInfo.RecipeInfo.ModelType)
	{	
	case Model_MRA2:
		lReturn = OnDAQ_EEPROM_MRA2_SteCal();
		break;

	case Model_IKC:
		lReturn = OnDAQ_EEPROM_IKC_SteCal(nIdxBrd);
		break;

	case Model_OMS_Entry:
	case Model_OMS_Front:
	case Model_OMS_Front_Set:
 	default:
		break;
 	}
#endif

	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_EEPROM_MRA2_SteCal
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/9 - 14:32
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnDAQ_EEPROM_MRA2_SteCal()
{
	LRESULT lReturn = RC_OK;
	

	// 쓰레드로 2개 동시에?
// 	OnDAQ_EEPROM_MRA2_SteCal_M(TRUE);
// 	OnDAQ_EEPROM_MRA2_SteCal_M(FALSE);

	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_EEPROM_MRA2_SteCal_M
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bMasterCamera
// Qualifier	:
// Last Update	: 2018/5/30 - 16:01
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnDAQ_EEPROM_MRA2_SteCal_M(__in BOOL bMasterCamera)
{
	LRESULT lReturn		= RC_OK;
	BOOL	bReturn		= TRUE;
	UINT	nIdxBrd		= bMasterCamera ? Para_Left : Para_Right;
	BYTE	bySlaveAddr	= 0x50 << 1;
	DWORD	dwBaseAddr	= 0x0000;

	//(slave addr : 0x50) = 0xA0 (0x50 << 1)

	// 0x0000	16 - Part Number
	// 0x0010	1 - CAM LOCATION	Camera Location (Master/Slave/IKc) IKc = I, Master = M, Slave = S	ascii value (73)
	// 0x0011	1 - EEPROM_TABLE_VERSION	EEPROM Version
	// 0x0012	2 - EEPROM_LAST_UPDATE_UTC_YEAR_0
	// 1 - EEPROM_LAST_UPDATE_UTC_MONTH
	// 1 - EEPROM_LAST_UPDATE_UTC_DAY
	// 1 - EEPROM_LAST_UPDATE_UTC_HOUR
	// 1 - EEPROM_LAST_UPDATE_UTC_MINUTE
	// 0x0018	4 - focal length x (fx)
	// 0x001C	4 - focal length y (fy)
	// 0x0020	4 - principle point x (cx)
	// 0x0024	4 - principle point y (cy)
	// 0x0028	4 - distortion k1
	// 4 - distortion k2
	// 4 - distortion k3
	// 4 - distortion k4
	// 4 - distortion k5
	// 0x003C	4 - Trans X
	// 0x0040	4 - Trans Y
	// 0x0044	4 - Trans Z
	// 0x0048	4 - Trans X
	// 0x004C	4 - Trans Y
	// 0x0050	4 - Trans Z
	// 0x0054	1 - CHECKSUM	Checksum by LGIT	Sign = Sum % 255

	CStringA szTempBarcode;

	BYTE arbyBufRead[256] = { 0, };
	BYTE arbyBufWrite[256] = { 0, };

	// * Read :  Max 256 Byte -------------------------------------------------
	if (FALSE == m_Device.DAQ_LVDS.I2C_Read_Repeat(nIdxBrd, bySlaveAddr, 2, dwBaseAddr, 0x54, arbyBufRead))
	{
		TRACE(_T("EEPROM Read Failed : First Read Failed\n"));
		return RC_EEPROM_Err_Read;
	}

	// * 데이터 세팅 -----------------------------------------------------------
	memcpy(arbyBufWrite, arbyBufRead, 256);

	// 바코드
	USES_CONVERSION;
	//if (szTempBarcode.GetLength() <= 16)
	if (m_stInspInfo.CamInfo[Para_Left].szBarcode.GetLength() <= 16)
	{
		szTempBarcode = CT2A(m_stInspInfo.CamInfo[Para_Left].szBarcode.GetBuffer(0));	// 16글자
	}
	else
	{
		CString szTemp = m_stInspInfo.CamInfo[Para_Left].szBarcode.Right(16);
		szTempBarcode = CT2A(szTemp.GetBuffer(0));	// 16글자
	}

	UINT nCount		= szTempBarcode.GetLength();
	nCount			= (16 < nCount) ? 16 : nCount;
	UINT nSN_Offset = 16 - nCount;
	//szTempBarcode += "0000000000000000";

	//memcpy(&arbyBufWrite[0x00], szTempBarcode.GetBuffer(0), 16);
	for (UINT nIdx = 0; nIdx < nSN_Offset; nIdx++)
	{
		arbyBufWrite[nIdx] = 0x00;
	}
	// * 메모리 버퍼에 데이터 세팅
	// 0x0000	Barcode
	memcpy(&arbyBufWrite[0x00 + nSN_Offset], szTempBarcode.GetBuffer(0), nCount);

	// 0x0010	1 - CAM LOCATION	Camera Location (Master/Slave/IKc) IKc = I, Master = M, Slave = S	ascii value (73)
	arbyBufWrite[0x0010] = bMasterCamera ? 'M' : 'S';

	// 0x0012	Date
	SYSTEMTIME tmLocal;
	//GetLocalTime(&tmLocal);
	memcpy(&tmLocal, &m_tmEEPROMWrite, sizeof(SYSTEMTIME));	// Master, Slave 똑같은 시간 적용
	memcpy(&arbyBufWrite[0x12], &tmLocal.wYear, 2);
	arbyBufWrite[0x14] = (BYTE)tmLocal.wMonth;
	arbyBufWrite[0x15] = (BYTE)tmLocal.wDay;
	arbyBufWrite[0x16] = (BYTE)tmLocal.wHour;
	arbyBufWrite[0x17] = (BYTE)tmLocal.wMinute;

	STEREO_CALIBRATION_RESULT* pSteCal = &m_stInspInfo.CamInfo[0].stSte_Result.StereoCal;

	// 0x0018	4 - focal length x (fx)
	float fValue = 0.0f;
	fValue = bMasterCamera ? (float)pSteCal->master_fx : (float)pSteCal->slave_fx;
	memcpy(&arbyBufWrite[0x0018], &fValue, 4);

	// 0x001C	4 - focal length y (fy)
	fValue = bMasterCamera ? (float)pSteCal->master_fy : (float)pSteCal->slave_fy;
	memcpy(&arbyBufWrite[0x001C], &fValue, 4);

	// 0x0020	4 - principle point x (cx)
	fValue = bMasterCamera ? (float)pSteCal->master_cx : (float)pSteCal->slave_cx;
	memcpy(&arbyBufWrite[0x0020], &fValue, 4);

	// 0x0024	4 - principle point y (cy)
	fValue = bMasterCamera ? (float)pSteCal->master_cy : (float)pSteCal->slave_cy;
	memcpy(&arbyBufWrite[0x0024], &fValue, 4);

	// 0x0028	4 - distortion k1
	fValue = bMasterCamera ? (float)pSteCal->master_distortion_cofficeints[0] : (float)pSteCal->slave_distortion_cofficeints[0];
	memcpy(&arbyBufWrite[0x0028], &fValue, 4);

	// 0x002C	4 - distortion k2
	fValue = bMasterCamera ? (float)pSteCal->master_distortion_cofficeints[1] : (float)pSteCal->slave_distortion_cofficeints[1];
	memcpy(&arbyBufWrite[0x002C], &fValue, 4);

	// 0x0030	4 - distortion k3
	fValue = bMasterCamera ? (float)pSteCal->master_distortion_cofficeints[2] : (float)pSteCal->slave_distortion_cofficeints[2];
	memcpy(&arbyBufWrite[0x0030], &fValue, 4);

	// 0x0034	4 - distortion k4
	fValue = bMasterCamera ? (float)pSteCal->master_distortion_cofficeints[3] : (float)pSteCal->slave_distortion_cofficeints[3];
	memcpy(&arbyBufWrite[0x0034], &fValue, 4);

	// 0x0038	4 - distortion k5
	fValue = bMasterCamera ? (float)pSteCal->master_distortion_cofficeints[4] : (float)pSteCal->slave_distortion_cofficeints[4];
	memcpy(&arbyBufWrite[0x0038], &fValue, 4);

	// 0x003C	4 - Trans X
	fValue = bMasterCamera ? 0x00000000 : (float)pSteCal->translation_vector[0];
	memcpy(&arbyBufWrite[0x003C], &fValue, 4);

	// 0x0040	4 - Trans Y
	fValue = bMasterCamera ? 0x00000000 : (float)pSteCal->translation_vector[1];
	memcpy(&arbyBufWrite[0x0040], &fValue, 4);

	// 0x0044	4 - Trans Z
	fValue = bMasterCamera ? 0x00000000 : (float)pSteCal->translation_vector[2];
	memcpy(&arbyBufWrite[0x0044], &fValue, 4);

	// 0x0048	4 - Rot X (Roll) // (임시 수정 : 2018.6.12 -> 1, 2, 0 순서에서 0, 1, 2로 바꿈... 임시) // 2018.10.16 조성기.SY 요청 0, 1, 2 -> 1, 2, 0  
	fValue = bMasterCamera ? 0x00000000 : (float)pSteCal->pitch;	//pSteCal->rotation_vector[1];
	memcpy(&arbyBufWrite[0x0048], &fValue, 4);

	// 0x004C	4 - Rot Y (Pitch)
	fValue = bMasterCamera ? 0x00000000 : (float)pSteCal->yaw;		//pSteCal->rotation_vector[2];
	memcpy(&arbyBufWrite[0x004C], &fValue, 4);

	// 0x0050	4 - Rot Z (Yaw)
	fValue = bMasterCamera ? 0x00000000 : (float)pSteCal->roll;		//pSteCal->rotation_vector[0];
	memcpy(&arbyBufWrite[0x0050], &fValue, 4);

	// * CRC 체크 
	UINT16 crc_uint16 = 0;
	UINT8 crc = 0;
	for (UINT nIdx = 0; nIdx < 0x54; nIdx++)
	{
		crc_uint16 += arbyBufWrite[nIdx];
	}
	crc = crc_uint16 % 255;
	arbyBufWrite[0x54] = crc;
	TRACE(_T("[%s] EEPROM Write CRC : %02X\n"), g_szParaName[nIdxBrd], crc);

	//=========================================================================
	// Write - Main Page
	//=========================================================================
	dwBaseAddr = 0x0000; // Main Page

	// * Write : Max 1024 Byte ------------------------------------------------
	UINT nPageCount = 0x0054 / 16;
	UINT nRemaind = 0x0054 % 16;
	UINT nOffset = dwBaseAddr + 0x0000;
	UINT nBufIndex = 0x0000;
	for (UINT nPageIdx = 0; nPageIdx < nPageCount; nPageIdx++)
	{
		bReturn &= m_Device.DAQ_LVDS.I2C_Write_Repeat(nIdxBrd, bySlaveAddr, 2, nOffset, 16, &arbyBufWrite[nBufIndex]);
		Sleep(100);

		nOffset += 16;
		nBufIndex += 16;
	}

	if (0 < nRemaind)
	{
		bReturn &= m_Device.DAQ_LVDS.I2C_Write_Repeat(nIdxBrd, bySlaveAddr, 2, nOffset, nRemaind, &arbyBufWrite[nBufIndex]);
		Sleep(100);
	}

	// 0x0054	1 - CHECKSUM	Checksum by LGIT	Sign = Sum % 255
	nOffset = dwBaseAddr + 0x0054;
	bReturn &= m_Device.DAQ_LVDS.I2C_Write_Repeat(nIdxBrd, bySlaveAddr, 2, nOffset, 1, &arbyBufWrite[0x54]);
	Sleep(100);

	if (FALSE == bReturn)
	{
		TRACE(_T("EEPROM Write Failed : \n"));
		return RC_EEPROM_Err_Write;
	}
	
	//=========================================================================
	// Write - Mirror Page
	//=========================================================================
	dwBaseAddr = 0x0800; // Mirror Page
	
	// * Write : Max 1024 Byte ------------------------------------------------
	nOffset = dwBaseAddr + 0x0000;
	nBufIndex = 0x0000;
	for (UINT nPageIdx = 0; nPageIdx < nPageCount; nPageIdx++)
	{
		bReturn &= m_Device.DAQ_LVDS.I2C_Write_Repeat(nIdxBrd, bySlaveAddr, 2, nOffset, 16, &arbyBufWrite[nBufIndex]);
		Sleep(100);

		nOffset += 16;
		nBufIndex += 16;
	}

	if (0 < nRemaind)
	{
		bReturn &= m_Device.DAQ_LVDS.I2C_Write_Repeat(nIdxBrd, bySlaveAddr, 2, nOffset, nRemaind, &arbyBufWrite[nBufIndex]);
		Sleep(100);
	}

	// 0x0054	1 - CHECKSUM	Checksum by LGIT	Sign = Sum % 255
	nOffset = dwBaseAddr + 0x0054;
	bReturn &= m_Device.DAQ_LVDS.I2C_Write_Repeat(nIdxBrd, bySlaveAddr, 2, nOffset, 1, &arbyBufWrite[0x54]);
	Sleep(100);

	if (FALSE == bReturn)
	{
		TRACE(_T("Mirror EEPROM Write Failed : \n"));
		return RC_EEPROM_Err_Write;
	}

	// * 카메라 리부팅 ---------------------------------------------------------
	lReturn = _TI_Cm_CameraReboot(TRUE, nIdxBrd);
	if (RC_OK != lReturn)
	{
		return lReturn;
	}

	//=========================================================================
	// Verify - Main Page
	//=========================================================================
	dwBaseAddr = 0x0000; // Main Page

	// * Read -----------------------------------------------------------------
	memset(arbyBufRead, 0x00, 256);
	if (FALSE == m_Device.DAQ_LVDS.I2C_Read_Repeat(nIdxBrd, bySlaveAddr, 2, dwBaseAddr, 0x55, arbyBufRead))
	{
		TRACE(_T("EEPROM Read Failed : Verify Data Read Failed\n"));
		return RC_EEPROM_Err_Read;
	}

	TRACE(_T("EEPROM Read CRC : %02X\n"), arbyBufRead[0x0054]);

	// * Verify ( 0 ~ checksum1 까지) -----------------------------------------
	BOOL bVerify = TRUE;
	for (UINT nIdx = 0; nIdx < 0x55; nIdx++)
	{
		if (arbyBufWrite[nIdx] != arbyBufRead[nIdx])
		{
			bVerify = FALSE;
			lReturn = RC_EEPROM_Err_Verify;
			TRACE(_T("EEPROM Verify Failed : Idx[%02X] - Write : %02X, Read : %02X\n"), nIdx, arbyBufWrite[nIdx], arbyBufRead[nIdx]);
			break;
		}
	}

	// ** 임시 **
	if (RC_OK == lReturn)
	{
		OnDAQ_EEPROM_Verify_MRA2_Log_M(bMasterCamera, TRUE, arbyBufRead, 0x55, TRUE);
	}
	else
	{
		OnDAQ_EEPROM_Verify_MRA2_Log_M(bMasterCamera, TRUE, arbyBufRead, 0x55, FALSE);
	}
	
	//=========================================================================
	// Verify - Mirror Page
	//=========================================================================
	dwBaseAddr = 0x0800; // Mirror Page

	// * Read -----------------------------------------------------------------
	memset(arbyBufRead, 0x00, 256);
	if (FALSE == m_Device.DAQ_LVDS.I2C_Read_Repeat(nIdxBrd, bySlaveAddr, 2, dwBaseAddr, 0x55, arbyBufRead))
	{
		TRACE(_T("Mirror EEPROM Read Failed : Verify Data Read Failed\n"));
		return RC_EEPROM_Err_Read;
	}

	TRACE(_T("Mirror EEPROM Read CRC : %02X\n"), arbyBufRead[0x0054]);

	LRESULT lReturn_Mirror = RC_OK;
	// * Verify ( 0 ~ checksum1 까지) -----------------------------------------
	bVerify = TRUE;
	for (UINT nIdx = 0; nIdx < 0x55; nIdx++)
	{
		if (arbyBufWrite[nIdx] != arbyBufRead[nIdx])
		{
			bVerify = FALSE;
			lReturn_Mirror = RC_EEPROM_Err_Verify;
			TRACE(_T("Mirror EEPROM Verify Failed : Idx[%02X] - Write : %02X, Read : %02X\n"), nIdx, arbyBufWrite[nIdx], arbyBufRead[nIdx]);
			break;
		}
	}
	//!SH _181121: Mirror 로그 안남게 막았다고 해서 풀어놓음. 확인 꼭 필요
	// ** 임시 **
	if (RC_OK == lReturn_Mirror)
	{
		OnDAQ_EEPROM_Verify_MRA2_Log_M(bMasterCamera, FALSE, arbyBufRead, 0x55, TRUE);
	}
	else
	{
		lReturn = lReturn_Mirror;
		OnDAQ_EEPROM_Verify_MRA2_Log_M(bMasterCamera, FALSE, arbyBufRead, 0x55, FALSE);
	}

	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_EEPROM_Verify_MRA2_Cal
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __out CString & szOutMasterBarcode
// Parameter	: __out CString & szOutSlaveBarcode
// Parameter	: __out SYSTEMTIME & stOutTime
// Parameter	: __out STEREO_CALIBRATION_RESULT & stStereo
// Qualifier	:
// Last Update	: 2018/6/11 - 16:22
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnDAQ_EEPROM_Verify_MRA2_Cal(__out CString& szOutMasterBarcode, __out CString& szOutSlaveBarcode, __out SYSTEMTIME& stOutTime, __out STEREO_CALIBRATION_RESULT& stStereo)
{
	LRESULT lReturn			= RC_OK;
	LRESULT lReturn_Slave	= RC_OK;

	// Master
	lReturn = OnDAQ_EEPROM_Verify_MRA2_Cal_M(TRUE, TRUE, szOutMasterBarcode, stOutTime, stStereo);

	// Slave
	lReturn_Slave = OnDAQ_EEPROM_Verify_MRA2_Cal_M(FALSE, TRUE, szOutSlaveBarcode, stOutTime, stStereo);

	if (RC_OK == lReturn)
	{
		if (RC_OK != lReturn_Slave)
		{
			lReturn = lReturn_Slave;
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_EEPROM_Verify_MRA2_Cal_M
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bMasterCamera
// Parameter	: __in BOOL bMainPage
// Parameter	: __out CString & szOutBarcode
// Parameter	: __out SYSTEMTIME & stOutTime
// Parameter	: __out STEREO_CALIBRATION_RESULT & stStereo
// Qualifier	:
// Last Update	: 2018/5/31 - 16:10
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnDAQ_EEPROM_Verify_MRA2_Cal_M(__in BOOL bMasterCamera, __in BOOL bMainPage, __out CString& szOutBarcode, __out SYSTEMTIME& stOutTime, __out STEREO_CALIBRATION_RESULT& stStereo)
{
	LRESULT lReturn = RC_OK;
	BOOL	bReturn = TRUE;
	UINT	nIdxBrd = bMasterCamera ? Para_Left : Para_Right;

	BYTE	bySlaveAddr = 0x50 << 1;
	BYTE	arbyBufRead[256] = { 0, };

	DWORD	dwBaseAddr = bMainPage ? 0x0000 : 0x0800;

	// * Read
	memset(arbyBufRead, 0x00, 256);
	if (FALSE == m_Device.DAQ_LVDS.I2C_Read_Repeat(nIdxBrd, bySlaveAddr, 2, dwBaseAddr, 0x55, arbyBufRead))
	{
		if (bMasterCamera)
			TRACE(_T("EEPROM Read Failed : Verify Data Read Failed\n"));
		else
			TRACE(_T("Slave - EEPROM Read Failed : Verify Data Read Failed\n"));
		return RC_EEPROM_Err_Read;
	}

	if (bMasterCamera)
		TRACE(_T("EEPROM Read CRC : %02X\n"), arbyBufRead[0x54]);
	else
		TRACE(_T("Slave - EEPROM Read CRC : %02X\n"), arbyBufRead[0x54]);

	// * CRC 체크
	UINT16	crc_uint16 = 0;
	UINT8	crc = 0;
	for (UINT nIdx = 0; nIdx < 0x54; nIdx++)
	{
		crc_uint16 += arbyBufRead[nIdx];
	}
	crc = crc_uint16 % 255;
	TRACE(_T("EEPROM Check CRC : %02X\n"), crc);

	// Barcode
	CStringA szBarcode;
	for (UINT nIdx = 0x00; nIdx < 16; nIdx++)
	{
		if (0x00 != arbyBufRead[nIdx])
		{
			szBarcode.AppendChar(arbyBufRead[nIdx]);
		}
	}
	szOutBarcode = szBarcode;

	// Date
	memcpy(&stOutTime.wYear, &arbyBufRead[0x12], 2);
	stOutTime.wMonth		= (WORD)arbyBufRead[0x14];
	stOutTime.wDay			= (WORD)arbyBufRead[0x15];
	stOutTime.wHour			= (WORD)arbyBufRead[0x16];
	stOutTime.wMinute		= (WORD)arbyBufRead[0x17];
	stOutTime.wSecond		= 0;
	stOutTime.wMilliseconds = 0;

	// 0x0010	1 - CAM LOCATION	Camera Location (Master/Slave/IKc) IKc = I, Master = M, Slave = S	ascii value (73)
	// 0x0011	1 - EEPROM_TABLE_VERSION	EEPROM Version

	// 0x0018	4 - focal length x (fx)
	float fValue = 0.0f;
	memcpy(&fValue, &arbyBufRead[0x0018], 4);
	if (bMasterCamera)
		stStereo.master_fx = fValue;
	else
		stStereo.slave_fx = fValue;

	// 0x001C	4 - focal length y (fy)
	memcpy(&fValue, &arbyBufRead[0x001C], 4);
	if (bMasterCamera)
		stStereo.master_fy = fValue;
	else
		stStereo.slave_fy = fValue;

	// 0x0020	4 - principle point x (cx)
	memcpy(&fValue, &arbyBufRead[0x0020], 4);
	if (bMasterCamera)
		stStereo.master_cx = fValue;
	else
		stStereo.slave_cx = fValue;

	// 0x0024	4 - principle point y (cy)
	memcpy(&fValue, &arbyBufRead[0x0024], 4);
	if (bMasterCamera)
		stStereo.master_cy = fValue;
	else
		stStereo.slave_cy = fValue;

	// 0x0028	4 - distortion k1
	memcpy(&fValue, &arbyBufRead[0x0028], 4);
	if (bMasterCamera)
		stStereo.master_distortion_cofficeints[0] = fValue;
	else
		stStereo.slave_distortion_cofficeints[0] = fValue;

	// 0x002C	4 - distortion k2
	memcpy(&fValue, &arbyBufRead[0x002C], 4);
	if (bMasterCamera)
		stStereo.master_distortion_cofficeints[1] = fValue;
	else
		stStereo.slave_distortion_cofficeints[1] = fValue;

	// 0x0030	4 - distortion k3
	memcpy(&fValue, &arbyBufRead[0x0030], 4);
	if (bMasterCamera)
		stStereo.master_distortion_cofficeints[2] = fValue;
	else
		stStereo.slave_distortion_cofficeints[2] = fValue;

	// 0x0034	4 - distortion k4
	memcpy(&fValue, &arbyBufRead[0x0034], 4);
	if (bMasterCamera)
		stStereo.master_distortion_cofficeints[3] = fValue;
	else
		stStereo.slave_distortion_cofficeints[3] = fValue;

	// 0x0038	4 - distortion k5
	memcpy(&fValue, &arbyBufRead[0x0038], 4);
	if (bMasterCamera)
		stStereo.master_distortion_cofficeints[4] = fValue;
	else
		stStereo.slave_distortion_cofficeints[4] = fValue;

	if (FALSE == bMasterCamera)
	{
		// 0x003C	4 - Trans X
		memcpy(&fValue, &arbyBufRead[0x003C], 4);
		stStereo.translation_vector[0] = fValue;

		// 0x0040	4 - Trans Y
		memcpy(&fValue, &arbyBufRead[0x0040], 4);
		stStereo.translation_vector[1] = fValue;

		// 0x0044	4 - Trans Z
		memcpy(&fValue, &arbyBufRead[0x0044], 4);
		stStereo.translation_vector[2] = fValue;

		// 0x0048	4 - Rot X (Roll) // (임시 수정 : 2018.6.12 -> 1, 2, 0 순서에서 0, 1, 2로 바꿈... 임시) // 2018.10.16 조성기.SY 요청 0, 1, 2 -> 1, 2, 0  
		memcpy(&fValue, &arbyBufRead[0x0048], 4);
		stStereo.pitch = fValue;	//stStereo.rotation_vector[1] = fValue;

		// 0x004C	4 - Rot Y
		memcpy(&fValue, &arbyBufRead[0x004C], 4);
		stStereo.yaw = fValue;		//stStereo.rotation_vector[2] = fValue;

		// 0x0050	4 - Rot Z
		memcpy(&fValue, &arbyBufRead[0x0050], 4);
		stStereo.roll = fValue;		//stStereo.rotation_vector[0] = fValue;
	}

	// 0x0054	1 - CHECKSUM	Checksum by LGIT	Sign = Sum % 255
	if (crc == arbyBufRead[0x54])
	{
		// Verify 성공
	}
	else
	{
		// Verify 실패
		lReturn = RC_EEPROM_Err_Verify;
	}

	if ((bMainPage) && (m_stOption.Inspector.bUseCreateDumpFile))
	{
		CFile_Report fileReport;
		USES_CONVERSION;
		CString szOutBarcode = CA2T(szBarcode.GetBuffer(0));
		fileReport.Save_SteCal_Stereo_EEPROM_Bin(bMasterCamera, bMainPage, m_stInspInfo.Path.szReport, szOutBarcode.GetBuffer(), &stOutTime, (LPBYTE)arbyBufRead, 0x55);
	}

	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_EEPROM_Verify_MRA2_Log_M
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bMasterCamera
// Parameter	: __in BOOL bMainPage
// Parameter	: __in const LPBYTE pbyEEPROM
// Parameter	: __in UINT nDataLeng
// Parameter	: __in BOOL bUseResultLog
// Qualifier	:
// Last Update	: 2018/5/31 - 16:01
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnDAQ_EEPROM_Verify_MRA2_Log_M(__in BOOL bMasterCamera, __in BOOL bMainPage, __in const LPBYTE pbyEEPROM, __in UINT nDataLeng, __in BOOL bUseResultLog)
{
	LRESULT lReturn = RC_OK;
	BOOL	bReturn = TRUE;
	UINT	nIdxBrd = Para_Left;
	int test = 0;
	SYSTEMTIME stOutTime;
	STEREO_CALIBRATION_RESULT stStereo;
	memset(&stStereo, 0, sizeof(STEREO_CALIBRATION_RESULT));

	// * CRC 체크
	UINT16	crc_uint16 = 0;
	UINT8	crc = 0;
	for (UINT nIdx = 0; nIdx < 0x54; nIdx++)
	{
		crc_uint16 += pbyEEPROM[nIdx];
	}
	crc = crc_uint16 % 255;
	TRACE(_T("EEPROM Check CRC : %02X\n"), crc);

	// Barcode
	CStringA szBarcode;
	for (UINT nIdx = 0x00; nIdx < 16; nIdx++)
	{
		if (0x00 != pbyEEPROM[nIdx])
		{
			szBarcode.AppendChar(pbyEEPROM[nIdx]);
		}
	}

	// Date
	memcpy(&stOutTime.wYear, &pbyEEPROM[0x12], 2);
	stOutTime.wMonth		= (WORD)pbyEEPROM[0x14];
	stOutTime.wDay			= (WORD)pbyEEPROM[0x15];
	stOutTime.wHour			= (WORD)pbyEEPROM[0x16];
	stOutTime.wMinute		= (WORD)pbyEEPROM[0x17];
	stOutTime.wSecond		= 0;
	stOutTime.wMilliseconds = 0;

	// 0x0010	1 - CAM LOCATION	Camera Location (Master/Slave/IKc) IKc = I, Master = M, Slave = S	ascii value (73)
	// 0x0011	1 - EEPROM_TABLE_VERSION	EEPROM Version

	// 0x0018	4 - focal length x (fx)
	float fValue = 0.0f;
	memcpy(&fValue, &pbyEEPROM[0x0018], 4);
	if (bMasterCamera)
		stStereo.master_fx = fValue;
	else
		stStereo.slave_fx = fValue;

	// 0x001C	4 - focal length y (fy)
	memcpy(&fValue, &pbyEEPROM[0x001C], 4);
	if (bMasterCamera)
		stStereo.master_fy = fValue;
	else
		stStereo.slave_fy = fValue;

	// 0x0020	4 - principle point x (cx)
	memcpy(&fValue, &pbyEEPROM[0x0020], 4);
	if (bMasterCamera)
		stStereo.master_cx = fValue;
	else
		stStereo.slave_cx = fValue;

	// 0x0024	4 - principle point y (cy)
	memcpy(&fValue, &pbyEEPROM[0x0024], 4);
	if (bMasterCamera)
		stStereo.master_cy = fValue;
	else
		stStereo.slave_cy = fValue;

	// 0x0028	4 - distortion k1
	memcpy(&fValue, &pbyEEPROM[0x0028], 4);
	if (bMasterCamera)
		stStereo.master_distortion_cofficeints[0] = fValue;
	else
		stStereo.slave_distortion_cofficeints[0] = fValue;

	// 0x002C	4 - distortion k2
	memcpy(&fValue, &pbyEEPROM[0x002C], 4);
	if (bMasterCamera)
		stStereo.master_distortion_cofficeints[1] = fValue;
	else
		stStereo.slave_distortion_cofficeints[1] = fValue;

	// 0x0030	4 - distortion k3
	memcpy(&fValue, &pbyEEPROM[0x0030], 4);
	if (bMasterCamera)
		stStereo.master_distortion_cofficeints[2] = fValue;
	else
		stStereo.slave_distortion_cofficeints[2] = fValue;

	// 0x0034	4 - distortion k4
	memcpy(&fValue, &pbyEEPROM[0x0034], 4);
	if (bMasterCamera)
		stStereo.master_distortion_cofficeints[3] = fValue;
	else
		stStereo.slave_distortion_cofficeints[3] = fValue;

	// 0x0038	4 - distortion k5
	memcpy(&fValue, &pbyEEPROM[0x0038], 4);
	if (bMasterCamera)
		stStereo.master_distortion_cofficeints[4] = fValue;
	else
		stStereo.slave_distortion_cofficeints[4] = fValue;

	if (FALSE == bMasterCamera)
	{
		// 0x003C	4 - Trans X
		memcpy(&fValue, &pbyEEPROM[0x003C], 4);
		stStereo.translation_vector[0] = fValue;

		// 0x0040	4 - Trans Y
		memcpy(&fValue, &pbyEEPROM[0x0040], 4);
		stStereo.translation_vector[1] = fValue;

		// 0x0044	4 - Trans Z
		memcpy(&fValue, &pbyEEPROM[0x0044], 4);
		stStereo.translation_vector[2] = fValue;

		// 0x0048	4 - Rot X (Roll) // (임시 수정 : 2018.6.12 -> 1, 2, 0 순서에서 0, 1, 2로 바꿈... 임시) // 2018.10.16 조성기.SY 요청 0, 1, 2 -> 1, 2, 0  
		memcpy(&fValue, &pbyEEPROM[0x0048], 4);
		stStereo.pitch = fValue;	//stStereo.rotation_vector[1] = fValue;

		// 0x004C	4 - Rot Y
		memcpy(&fValue, &pbyEEPROM[0x004C], 4);
		stStereo.yaw = fValue;		//stStereo.rotation_vector[2] = fValue;

		// 0x0050	4 - Rot Z
		memcpy(&fValue, &pbyEEPROM[0x0050], 4);
		stStereo.roll = fValue;		//stStereo.rotation_vector[0] = fValue;
	}

	// 0x0054	1 - CHECKSUM	Checksum by LGIT	Sign = Sum % 255
	if (crc == pbyEEPROM[0x54])
	{
		// Verify 성공
	}
	else
	{
		// Verify 실패
		lReturn = RC_EEPROM_Err_Verify;
	}

	//if (bMainPage)
	{
		CFile_Report fileReport;
		USES_CONVERSION;
		CString szOutBarcode = CA2T(szBarcode.GetBuffer(0));
		if (m_stOption.Inspector.bUseCreateDumpFile)
		{
			fileReport.Save_SteCal_Stereo_EEPROM_Bin(bMasterCamera, bMainPage, m_stInspInfo.Path.szReport, szOutBarcode.GetBuffer(), &stOutTime, (LPBYTE)pbyEEPROM, 0x55);
		}
		fileReport.Save_SteCal_Stereo_EEPROM(bMasterCamera, bMainPage, m_stInspInfo.Path.szReport, m_stInspInfo.CamInfo[nIdxBrd].szBarcode.GetBuffer(0), &stOutTime, &stStereo);
	}

	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_EEPROM_IKC_SteCal
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/2/27 - 15:54
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnDAQ_EEPROM_IKC_SteCal(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;
	BOOL bReturn = TRUE;
	BYTE bySlaveAddr = 0x50 << 1;

	//(slave addr : 0x50) = 0xA0 (0x50 << 1)

	// 0x0000	16 - Part Number
	// 0x0010	1 - CAM LOCATION	Camera Location (Master/Slave/IKc) IKc = I, Master = M, Slave = S	ascii value (73)
	// 0x0011	1 - EEPROM_TABLE_VERSION	EEPROM Version
	// 0x0012	2 - EEPROM_LAST_UPDATE_UTC_YEAR_0
	// 1 - EEPROM_LAST_UPDATE_UTC_MONTH
	// 1 - EEPROM_LAST_UPDATE_UTC_DAY
	// 1 - EEPROM_LAST_UPDATE_UTC_HOUR
	// 1 - EEPROM_LAST_UPDATE_UTC_MINUTE
	// 0x0018	4 - focal length x (fx)
	// 0x001C	4 - focal length y (fy)
	// 0x0020	4 - principle point x (cx)
	// 0x0024	4 - principle point y (cy)
	// 0x0028	4 - distortion k1
	// 4 - distortion k2
	// 4 - distortion k3
	// 4 - distortion k4
	// 4 - distortion k5
	// 0x003C	1 - CHECKSUM	Checksum by LGIT	Sign = Sum % 255

	CStringA szTempBarcode;

	BYTE arbyBufRead[256] = { 0, };
	BYTE arbyBufWrite[256] = { 0, };

	// * Read :  Max 256 Byte -------------------------------------------------
	if (FALSE == m_Device.DAQ_LVDS.I2C_Read_Repeat(nIdxBrd, bySlaveAddr, 2, 0x0000, 0x3C, arbyBufRead))
	{
		TRACE(_T("EEPROM Read Failed : First Read Failed\n"));
		return RC_EEPROM_Err_Read;
	}

	// * 데이터 세팅 -----------------------------------------------------------
	memcpy(arbyBufWrite, arbyBufRead, 256);

	// 바코드
	USES_CONVERSION;
	//if (szTempBarcode.GetLength() <= 16)
	if (m_stInspInfo.CamInfo[nIdxBrd].szBarcode.GetLength() <= 16)
	{
		szTempBarcode = CT2A(m_stInspInfo.CamInfo[nIdxBrd].szBarcode.GetBuffer(0));	// 16글자
	}
	else
	{
		CString szTemp = m_stInspInfo.CamInfo[nIdxBrd].szBarcode.Right(16);
		szTempBarcode = CT2A(szTemp.GetBuffer(0));	// 16글자
	}

	UINT nCount = szTempBarcode.GetLength();
	nCount = (16 < nCount) ? 16 : nCount;
	UINT nSN_Offset = 16 - nCount;
	//szTempBarcode += "0000000000000000";

	//memcpy(&arbyBufWrite[0x00], szTempBarcode.GetBuffer(0), 16);
	for (UINT nIdx = 0; nIdx < nSN_Offset; nIdx++)
	{
		arbyBufWrite[nIdx] = 0x00;
	}
	// * 메모리 버퍼에 데이터 세팅
	// 0x0000	Barcode
	memcpy(&arbyBufWrite[0x00 + nSN_Offset], szTempBarcode.GetBuffer(0), nCount);

	// 0x0010	1 - CAM LOCATION	Camera Location (Master/Slave/IKc) IKc = I, Master = M, Slave = S	ascii value (73)
	arbyBufWrite[0x0010] = 'I';

	// 0x0012	Date
	SYSTEMTIME tmLocal;
	GetLocalTime(&tmLocal);
	memcpy(&arbyBufWrite[0x12], &tmLocal.wYear, 2);
	arbyBufWrite[0x14] = (BYTE)tmLocal.wMonth;
	arbyBufWrite[0x15] = (BYTE)tmLocal.wDay;
	arbyBufWrite[0x16] = (BYTE)tmLocal.wHour;
	arbyBufWrite[0x17] = (BYTE)tmLocal.wMinute;

	// 0x0018	4 - focal length x (fx)
	float fValue = 0.0f;
	fValue = (float)m_stInspInfo.CamInfo[0].stSte_Result.MonoCal.fx;
	memcpy(&arbyBufWrite[0x0018], &fValue, 4);
	
	// 0x001C	4 - focal length y (fy)
	fValue = (float)m_stInspInfo.CamInfo[0].stSte_Result.MonoCal.fy;
	memcpy(&arbyBufWrite[0x001C], &fValue, 4);
	
	// 0x0020	4 - principle point x (cx)
	fValue = (float)m_stInspInfo.CamInfo[0].stSte_Result.MonoCal.cx;
	memcpy(&arbyBufWrite[0x0020], &fValue, 4);

	// 0x0024	4 - principle point y (cy)
	fValue = (float)m_stInspInfo.CamInfo[0].stSte_Result.MonoCal.cy;
	memcpy(&arbyBufWrite[0x0024], &fValue, 4);

	// 0x0028	4 - distortion k1
	fValue = (float)m_stInspInfo.CamInfo[0].stSte_Result.MonoCal.distortion_cofficeints[0];
	memcpy(&arbyBufWrite[0x0028], &fValue, 4);

	// 0x002C	4 - distortion k2
	fValue = (float)m_stInspInfo.CamInfo[0].stSte_Result.MonoCal.distortion_cofficeints[1];
	memcpy(&arbyBufWrite[0x002C], &fValue, 4);

	// 0x0030	4 - distortion k3
	fValue = (float)m_stInspInfo.CamInfo[0].stSte_Result.MonoCal.distortion_cofficeints[2];
	memcpy(&arbyBufWrite[0x0030], &fValue, 4);

	// 0x0034	4 - distortion k4
	fValue = (float)m_stInspInfo.CamInfo[0].stSte_Result.MonoCal.distortion_cofficeints[3];
	memcpy(&arbyBufWrite[0x0034], &fValue, 4);

	// 0x0038	4 - distortion k5
	fValue = (float)m_stInspInfo.CamInfo[0].stSte_Result.MonoCal.distortion_cofficeints[4];
	memcpy(&arbyBufWrite[0x0038], &fValue, 4);

	// * CRC 체크 
	UINT16 crc_uint16 = 0;
	UINT8 crc = 0;
	for (UINT nIdx = 0; nIdx < 0x3C; nIdx++)
	{
		crc_uint16 += arbyBufWrite[nIdx];
	}
	crc = crc_uint16 % 255;	
	arbyBufWrite[0x3C] = crc;
	TRACE(_T("EEPROM Write CRC : %02X\n"), crc);

	// * Write : Max 1024 Byte ------------------------------------------------
	UINT nPageCount = 0x003C / 16;
	UINT nRemaind	= 0x003C % 16;
	UINT nOffset	= 0x0000;
	for (UINT nPageIdx = 0; nPageIdx < nPageCount; nPageIdx++)
	{
		bReturn = m_Device.DAQ_LVDS.I2C_Write_Repeat(nIdxBrd, bySlaveAddr, 2, nOffset, 16, &arbyBufWrite[nOffset]);
		Sleep(100);

		nOffset += 16;
	}

	if (0 < nRemaind)
	{
		bReturn &= m_Device.DAQ_LVDS.I2C_Write_Repeat(nIdxBrd, bySlaveAddr, 2, nOffset, nRemaind, &arbyBufWrite[nOffset]);
		Sleep(100);
	}

	// 0x003C	1 - CHECKSUM	Checksum by LGIT	Sign = Sum % 255
	bReturn &= m_Device.DAQ_LVDS.I2C_Write_Repeat(nIdxBrd, bySlaveAddr, 2, 0x003C, 1, &arbyBufWrite[0x3C]);
	Sleep(100);

	if (FALSE == bReturn)
	{
		TRACE(_T("EEPROM Write Failed : "));
		return RC_EEPROM_Err_Write;
	}

	// * 카메라 리부팅 ---------------------------------------------------------
	lReturn =  _TI_Cm_CameraReboot(TRUE, Para_Left);
	if (RC_OK != lReturn)
	{
		return lReturn;
	}

	// * Read -----------------------------------------------------------------
	memset(arbyBufRead, 0x00, 256);
	if (FALSE == m_Device.DAQ_LVDS.I2C_Read_Repeat(nIdxBrd, bySlaveAddr, 2, 0x0000, 0x3D, arbyBufRead))
	{
		TRACE(_T("EEPROM Read Failed : Verify Data Read Failed\n"));
		return RC_EEPROM_Err_Read;
	}

	TRACE(_T("EEPROM Read CRC : %02X\n"), arbyBufRead[0x003C]);

	// * Verify ( 0 ~ checksum1 까지) -----------------------------------------
	BOOL bVerify = TRUE;
	for (UINT nIdx = 0; nIdx < 0x3D; nIdx++)
	{
		if (arbyBufWrite[nIdx] != arbyBufRead[nIdx])
		{
			bVerify = FALSE;
			lReturn = RC_EEPROM_Err_Verify;
			TRACE(_T("EEPROM Verify Failed : Idx[%02X] - Write : %02X, Read : %02X\n"), nIdx, arbyBufWrite[nIdx], arbyBufRead[nIdx]);
			break;
		}
	}

	// ** 임시 **
	if (RC_OK == lReturn)
	{
		OnDAQ_EEPROM_Verify_IKC_Log(arbyBufRead, 0x3D, TRUE);
	}
	else
	{
		OnDAQ_EEPROM_Verify_IKC_Log(arbyBufRead, 0x3D, FALSE);
	}

	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_EEPROM_Verify_IKC_Cal
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __out CStringA & szOutBarcode
// Parameter	: __out SYSTEMTIME & stOutTime
// Parameter	: __out SINGLE_CALIBRATION_RESULT & stMono
// Qualifier	:
// Last Update	: 2018/3/11 - 13:11
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnDAQ_EEPROM_Verify_IKC_Cal(__out CString& szOutBarcode, __out SYSTEMTIME& stOutTime, __out SINGLE_CALIBRATION_RESULT& stMono)
{
	LRESULT lReturn = RC_OK;
	BOOL	bReturn = TRUE;
	UINT	nIdxBrd = Para_Left;

	BYTE	bySlaveAddr = 0x50 << 1;

	BYTE	arbyBufRead[256] = { 0, };

	// * Read
	memset(arbyBufRead, 0x00, 256);
	if (FALSE == m_Device.DAQ_LVDS.I2C_Read_Repeat(nIdxBrd, bySlaveAddr, 2, 0x0000, 0x3D, arbyBufRead))
	{
		TRACE(_T("EEPROM Read Failed : Verify Data Read Failed\n"));
		return RC_EEPROM_Err_Read;
	}

	TRACE(_T("EEPROM Read CRC : %02X\n"), arbyBufRead[0x003C]);

	// * CRC 체크
	UINT16 crc_uint16 = 0;
	UINT8 crc = 0;
	for (UINT nIdx = 0; nIdx < 0x3C; nIdx++)
	{
		crc_uint16 += arbyBufRead[nIdx];
	}
	crc = crc_uint16 % 255;
	TRACE(_T("EEPROM Check CRC : %02X\n"), crc);

	// Barcode
	CStringA szBarcode;
	for (UINT nIdx = 0x00; nIdx < 16; nIdx++)
	{
		if (0x00 != arbyBufRead[nIdx])
		{
			szBarcode.AppendChar(arbyBufRead[nIdx]);
		}
	}

	// Date
	memcpy(&stOutTime.wYear, &arbyBufRead[0x12], 2);
	stOutTime.wMonth	= (WORD)arbyBufRead[0x14];
	stOutTime.wDay		= (WORD)arbyBufRead[0x15];
	stOutTime.wHour		= (WORD)arbyBufRead[0x16];
	stOutTime.wMinute	= (WORD)arbyBufRead[0x17];
	stOutTime.wSecond	= 0;
	stOutTime.wMilliseconds = 0;

	// 0x0010	1 - CAM LOCATION	Camera Location (Master/Slave/IKc) IKc = I, Master = M, Slave = S	ascii value (73)
	// 0x0011	1 - EEPROM_TABLE_VERSION	EEPROM Version

	// 0x0018	4 - focal length x (fx)
	float fValue = 0.0f;
	memcpy(&fValue, &arbyBufRead[0x0018], 4);
	stMono.fx = fValue;

	// 0x001C	4 - focal length y (fy)
	memcpy(&fValue, &arbyBufRead[0x001C], 4);
	stMono.fy = fValue;

	// 0x0020	4 - principle point x (cx)
	memcpy(&fValue, &arbyBufRead[0x0020], 4);
	stMono.cx = fValue;

	// 0x0024	4 - principle point y (cy)
	memcpy(&fValue, &arbyBufRead[0x0024], 4);
	stMono.cy = fValue;

	// 0x0028	4 - distortion k1
	memcpy(&fValue, &arbyBufRead[0x0028], 4);
	stMono.distortion_cofficeints[0] = fValue;

	// 0x002C	4 - distortion k2
	memcpy(&fValue, &arbyBufRead[0x002C], 4);
	stMono.distortion_cofficeints[1] = fValue;

	// 0x0030	4 - distortion k3
	memcpy(&fValue, &arbyBufRead[0x0030], 4);
	stMono.distortion_cofficeints[2] = fValue;

	// 0x0034	4 - distortion k4
	memcpy(&fValue, &arbyBufRead[0x0034], 4);
	stMono.distortion_cofficeints[3] = fValue;

	// 0x0038	4 - distortion k5
	memcpy(&fValue, &arbyBufRead[0x0038], 4);
	stMono.distortion_cofficeints[4] = fValue;


	if (crc == arbyBufRead[0x3C])
	{
		// Verify 성공
	}
	else
	{
		// Verify 실패
		lReturn = RC_EEPROM_Err_Verify;
	}

	if (m_stOption.Inspector.bUseCreateDumpFile)
	{
		CFile_Report fileReport;
		USES_CONVERSION;
		szOutBarcode = CA2T(szBarcode.GetBuffer(0));
		fileReport.Save_SteCal_Mono_EEPROM_Bin(m_stInspInfo.Path.szReport, szOutBarcode.GetBuffer(), &stOutTime, arbyBufRead, 0x3D);
	}

	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_EEPROM_Verify_IKC_Log
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in const LPBYTE pbyEEPROM
// Parameter	: __in UINT nDataLeng
// Parameter	: __in BOOL bUseResultLog
// Qualifier	:
// Last Update	: 2018/3/16 - 12:42
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnDAQ_EEPROM_Verify_IKC_Log(__in const LPBYTE pbyEEPROM, __in UINT nDataLeng, __in BOOL bUseResultLog)
{
	LRESULT lReturn = RC_OK;
	BOOL	bReturn = TRUE;
	UINT	nIdxBrd = Para_Left;

	SYSTEMTIME stOutTime;
	SINGLE_CALIBRATION_RESULT stMono;
	memset(&stMono, 0, sizeof(SINGLE_CALIBRATION_RESULT));

	// * CRC 체크
	UINT16 crc_uint16 = 0;
	UINT8 crc = 0;
	for (UINT nIdx = 0; nIdx < 0x3C; nIdx++)
	{
		crc_uint16 += pbyEEPROM[nIdx];
	}
	crc = crc_uint16 % 255;
	TRACE(_T("EEPROM Check CRC : %02X\n"), crc);

	// Barcode
	CStringA szBarcode;
	for (UINT nIdx = 0x00; nIdx < 16; nIdx++)
	{
		if (0x00 != pbyEEPROM[nIdx])
		{
			szBarcode.AppendChar(pbyEEPROM[nIdx]);
		}
	}

	// Date
	memcpy(&stOutTime.wYear, &pbyEEPROM[0x12], 2);
	stOutTime.wMonth	= (WORD)pbyEEPROM[0x14];
	stOutTime.wDay		= (WORD)pbyEEPROM[0x15];
	stOutTime.wHour		= (WORD)pbyEEPROM[0x16];
	stOutTime.wMinute	= (WORD)pbyEEPROM[0x17];
	stOutTime.wSecond	= 0;
	stOutTime.wMilliseconds = 0;

	// 0x0010	1 - CAM LOCATION	Camera Location (Master/Slave/IKc) IKc = I, Master = M, Slave = S	ascii value (73)
	// 0x0011	1 - EEPROM_TABLE_VERSION	EEPROM Version

	// 0x0018	4 - focal length x (fx)
	float fValue = 0.0f;
	memcpy(&fValue, &pbyEEPROM[0x0018], 4);
	stMono.fx = fValue;

	// 0x001C	4 - focal length y (fy)
	memcpy(&fValue, &pbyEEPROM[0x001C], 4);
	stMono.fy = fValue;

	// 0x0020	4 - principle point x (cx)
	memcpy(&fValue, &pbyEEPROM[0x0020], 4);
	stMono.cx = fValue;

	// 0x0024	4 - principle point y (cy)
	memcpy(&fValue, &pbyEEPROM[0x0024], 4);
	stMono.cy = fValue;

	// 0x0028	4 - distortion k1
	memcpy(&fValue, &pbyEEPROM[0x0028], 4);
	stMono.distortion_cofficeints[0] = fValue;

	// 0x002C	4 - distortion k2
	memcpy(&fValue, &pbyEEPROM[0x002C], 4);
	stMono.distortion_cofficeints[1] = fValue;

	// 0x0030	4 - distortion k3
	memcpy(&fValue, &pbyEEPROM[0x0030], 4);
	stMono.distortion_cofficeints[2] = fValue;

	// 0x0034	4 - distortion k4
	memcpy(&fValue, &pbyEEPROM[0x0034], 4);
	stMono.distortion_cofficeints[3] = fValue;

	// 0x0038	4 - distortion k5
	memcpy(&fValue, &pbyEEPROM[0x0038], 4);
	stMono.distortion_cofficeints[4] = fValue;

	if (crc == pbyEEPROM[0x3C])
	{
		// Verify 성공
	}
	else
	{
		// Verify 실패
		lReturn = RC_EEPROM_Err_Verify;
	}

	CFile_Report fileReport;
	USES_CONVERSION;
	CString szOutBarcode = CA2T(szBarcode.GetBuffer(0));
	if (m_stOption.Inspector.bUseCreateDumpFile)
	{
		fileReport.Save_SteCal_Mono_EEPROM_Bin(m_stInspInfo.Path.szReport, szOutBarcode.GetBuffer(), &stOutTime, (LPBYTE)pbyEEPROM, 0x3D);
	}
	fileReport.Save_SteCal_Mono_EEPROM(m_stInspInfo.Path.szReport, m_stInspInfo.CamInfo[nIdxBrd].szBarcode.GetBuffer(0), &stOutTime, &stMono);

	return lReturn;
}

//=============================================================================
// Method		: OnManualSequence
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaID
// Parameter	: __in enParaManual enFunc
// Qualifier	: 메뉴얼 시퀀스 동작 테스트
// Last Update	: 2017/10/27 - 18:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnManualSequence(__in UINT nParaID, __in enParaManual enFunc)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnManualSequence(nParaID, enFunc);



	return lReturn;
}

//=============================================================================
// Method		: OnManualTestItem
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaID
// Parameter	: __in UINT nStepIdx
// Qualifier	:
// Last Update	: 2017/10/27 - 18:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnManualTestItem(__in UINT nParaID, __in UINT nStepIdx)
{
    LRESULT lReturn = RC_OK;

	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;
	INT_PTR iStepCnt = m_stInspInfo.RecipeInfo.StepInfo.GetCount();

         
	// * 검사 시작
	if (TRUE == pStepInfo->StepList[nStepIdx].bTest)
	{
		if (OpMode_DryRun != m_stInspInfo.OperateMode)
		{
			StartTest_SteCAL_TestItem(pStepInfo->StepList[nStepIdx].nTestItem, nStepIdx, nParaID);
		}
	}

    return lReturn;
}

//=============================================================================
// Method		: OnCheck_DeviceComm
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnCheck_DeviceComm()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnCheck_DeviceComm();



	return lReturn;
}

//=============================================================================
// Method		: OnCheck_SaftyJIG
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnCheck_SaftyJIG()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnCheck_SaftyJIG();



	return lReturn;
}

//=============================================================================
// Method		: OnCheck_RecipeInfo
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_SteCal::OnCheck_RecipeInfo()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnCheck_RecipeInfo();



	return lReturn;
}

//=============================================================================
// Method		: OnMakeReport
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 14:25
// Desc.		:
//=============================================================================
void CTestManager_EQP_SteCal::OnJugdement_And_Report(__in UINT nParaIdx)
{
	CFile_Report fileReport;

	enModelType nModelType = m_stInspInfo.Get_ModelType();

	if (1 < g_IR_ModelTable[nModelType].Camera_Cnt)	// Stereo
	{
		fileReport.SaveFinalizeResultSteCal_Dual(m_stInspInfo.Path.szReport, m_stInspInfo.CamInfo[nParaIdx].szBarcode, &m_stInspInfo.CamInfo[nParaIdx].stSte_Result.StereoCal);
	}
	else // Single
	{
		fileReport.SaveFinalizeResultSteCal_Mono(m_stInspInfo.Path.szReport, m_stInspInfo.CamInfo[nParaIdx].szBarcode, &m_stInspInfo.CamInfo[nParaIdx].stSte_Result.MonoCal);
	}
}

//=============================================================================
// Method		: OnJugdement_And_Report_All
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/5 - 17:02
// Desc.		:
//=============================================================================
void CTestManager_EQP_SteCal::OnJugdement_And_Report_All()
{
	OnUpdateYield();
	OnSaveWorklist();
}

//=============================================================================
// Method		: CreateTimer_UpdateUI
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in DWORD DueTime
// Parameter	: __in DWORD Period
// Qualifier	:
// Last Update	: 2017/11/8 - 14:17
// Desc.		: 파워 서플라이 모니터링
//=============================================================================
void CTestManager_EQP_SteCal::CreateTimer_UpdateUI(__in DWORD DueTime /*= 5000*/, __in DWORD Period /*= 250*/)
{
// 		__super::CreateTimer_UpdateUI(5000, 2500);
}

void CTestManager_EQP_SteCal::DeleteTimer_UpdateUI()
{
// 		__super::DeleteTimer_UpdateUI();
}

//=============================================================================
// Method		: OnMonitor_TimeCheck
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 13:28
// Desc.		:
//=============================================================================
void CTestManager_EQP_SteCal::OnMonitor_TimeCheck()
{
	CTestManager_EQP::OnMonitor_TimeCheck();

}

//=============================================================================
// Method		: OnMonitor_UpdateUI
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 21:19
// Desc.		: Power Supply 모니터링
//=============================================================================
void CTestManager_EQP_SteCal::OnMonitor_UpdateUI()
{
	CTestManager_EQP::OnMonitor_UpdateUI();
	
}

//=============================================================================
// Method		: OnSaveWorklist
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/5/10 - 20:24
// Desc.		:
//=============================================================================
void CTestManager_EQP_SteCal::OnSaveWorklist()
{
// 	m_stInspInfo.WorklistInfo.Reset();
// 
// 	ST_CamInfo* pstCam = NULL;
// 
// 	CString szText;
// 	CString szTime;
// 	SYSTEMTIME lcTime;
// 
// 	GetLocalTime(&lcTime);
// 	szTime.Format(_T("'%04d-%02d-%02d %02d:%02d:%02d.%03d"), lcTime.wYear, lcTime.wMonth, lcTime.wDay,
// 		lcTime.wHour, lcTime.wMinute, lcTime.wSecond, lcTime.wMilliseconds);
// 
// 	for (UINT nChIdx = 0; nChIdx < USE_CHANNEL_CNT; nChIdx++)
// 	{
// 		if (FALSE == m_stInspInfo.bTestEnable[nChIdx])
// 			continue;
// 
// 		pstCam = &m_stInspInfo.CamInfo[nChIdx];
// 
// 		m_stInspInfo.WorklistInfo.Time = szTime;
// 		m_stInspInfo.WorklistInfo.EqpID = m_stInspInfo.szEquipmentID;	// g_szInsptrSysType[m_InspectionType];
// 		m_stInspInfo.WorklistInfo.SWVersion;
// 		m_stInspInfo.WorklistInfo.Model = m_stInspInfo.RecipeInfo.szModelCode;
// 		m_stInspInfo.WorklistInfo.Barcode = pstCam->szBarcode;
// 		m_stInspInfo.WorklistInfo.Result = g_TestResult[pstCam->nJudgment].szText;
// 
// 		szText.Format(_T("%d"), nChIdx + 1);
// 		m_stInspInfo.WorklistInfo.Socket = szText;
// 
// 
// 		m_stInspInfo.WorklistInfo.MakeItemz();
// 
// 		// 파일 저장
// 		m_Worklist.Save_FinalResult_List(m_stInspInfo.Path.szReport, &pstCam->TestTime.tmStart_Loading, &m_stInspInfo.WorklistInfo);
// 
// 		// UI 표시
// 		OnInsertWorklist();
// 
// 	}// End of for
}

//=============================================================================
// Method		: OnInitialize
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/26 - 6:03
// Desc.		:
//=============================================================================
void CTestManager_EQP_SteCal::OnInitialize()
{
	CTestManager_EQP::OnInitialize();

}

//=============================================================================
// Method		: OnFinalize
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/26 - 6:03
// Desc.		:
//=============================================================================
void CTestManager_EQP_SteCal::OnFinalize()
{
	CTestManager_EQP::OnFinalize();

}

//=============================================================================
// Method		: SetPermissionMode
// Access		: public  
// Returns		: void
// Parameter	: __in enPermissionMode nAcessMode
// Qualifier	:
// Last Update	: 2016/11/9 - 18:52
// Desc.		:
//=============================================================================
void CTestManager_EQP_SteCal::SetPermissionMode(__in enPermissionMode nAcessMode)
{
	CTestManager_EQP::SetPermissionMode(nAcessMode);


}
