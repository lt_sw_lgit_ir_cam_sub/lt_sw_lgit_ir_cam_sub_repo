﻿#include "stdafx.h"
#include "File_Recipe.h"
#include "Def_Enum.h"
#include "CommonFunction.h"


#define		LVDS_AppName			_T("LVDS")
#define		BoardNumber_KeyName		_T("Board Number")
#define		SensorType_KeyName		_T("Sensor Type")
#define		ConvFormat_KeyName		_T("Conv Format")
#define		ClockSelect_KeyName		_T("Clock Select")
#define		ClockUse_KeyName		_T("Clock Use")
#define		DataMode_KeyName		_T("Data Mode")
#define		DvalUse_KeyName			_T("Dval Use")
#define		HsyncPolarity_KeyName	_T("Hsync Polarity")
#define		MIPILane_KeyName		_T("MIPI Lane")
#define		PClockPolarity_KeyName	_T("PClock Polarity")
#define		VideoMode_KeyName		_T("Video Mode")
#define		Clock_KeyName			_T("Clock")
#define		WidthMultiple_KeyName	_T("Width Multiple")
#define		HeightMultiple_KeyName	_T("Height Multiple")

#define		AppName_SteCal_Para		_T("Para_SteCal")

CFile_Recipe::CFile_Recipe()
{
}


CFile_Recipe::~CFile_Recipe()
{
}

//=============================================================================
// Method		: Load_Default
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_RecipeInfo & stRecipeInfo
// Qualifier	:
// Last Update	: 2017/1/3 - 15:55
// Desc.		:
//=============================================================================
BOOL CFile_Recipe::Load_Common(__in LPCTSTR szPath, __out ST_RecipeInfo& stRecipeInfo)
{
	BOOL bReturn = TRUE;

	bReturn = __super::Load_Common(szPath, stRecipeInfo);

	//bReturn &= Load_LVDS(szPath, stRecipeInfo.stLVDSInfo);


	return bReturn;
}

//=============================================================================
// Method		: Save_Default
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_RecipeInfo * pstRecipeInfo
// Qualifier	:
// Last Update	: 2017/1/3 - 15:55
// Desc.		:
//=============================================================================
BOOL CFile_Recipe::Save_Common(__in LPCTSTR szPath, __in const ST_RecipeInfo* pstRecipeInfo)
{
	BOOL bReturn = TRUE;

	bReturn = __super::Save_Common(szPath, pstRecipeInfo);

	//bReturn &= Save_LVDS(szPath, &pstRecipeInfo->stLVDSInfo);

	return bReturn;
}

//=============================================================================
// Method		: Load_RecipeFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_RecipeInfo & stRecipeInfo
// Qualifier	:
// Last Update	: 2016/3/18 - 11:18
// Desc.		:
//=============================================================================
BOOL CFile_Recipe::Load_RecipeFile(__in LPCTSTR szPath, __out ST_RecipeInfo& stRecipeInfo)
{
	BOOL bReturn = TRUE;
	CFile_VI_Config Vl_Config;

	bReturn = __super::Load_RecipeFile(szPath, stRecipeInfo);
	bReturn &= Load_LVDS(szPath, stRecipeInfo.stLVDSInfo);
	
	switch (stRecipeInfo.nInspectionType)
	{
	case Sys_Focusing:
	{
		bReturn &= Vl_Config.Load_ECurrentFile		(szPath, stRecipeInfo.stFocus.stECurrentOpt);
		bReturn &= Vl_Config.Load_OpticalCenterFile	(szPath, stRecipeInfo.stFocus.stOpticalCenterOpt);
		bReturn &= Vl_Config.Load_RotateFile		(szPath, stRecipeInfo.stFocus.stRotateOpt);
		bReturn &= Vl_Config.Load_DefectPixelFile	(szPath, stRecipeInfo.stFocus.stDefectPixelOpt);
		bReturn &= Vl_Config.Load_SFRFile			(szPath, stRecipeInfo.stFocus.stSFROpt);
		bReturn &= Vl_Config.Load_ParticleFile		(szPath, stRecipeInfo.stFocus.stParticleOpt);
		bReturn &= Vl_Config.Load_ActiveAlignFile	(szPath, stRecipeInfo.stFocus.stActiveAlignOpt);
		bReturn &= Vl_Config.Load_TorqueFile		(szPath, stRecipeInfo.stFocus.stTorqueOpt);
		bReturn &= Vl_Config.Load_Focus				(szPath, stRecipeInfo.stFocus.stFocusOpt);
	}
	break;

	case Sys_2D_Cal:
	{
		//bReturn &= Load_2DCal_Para(szPath, stRecipeInfo.st2D_CAL.Parameter);
		bReturn &= Load_2DCal_Info(szPath, stRecipeInfo.st2D_CAL);
	}
	break;

	case Sys_Image_Test:
	{
		bReturn &= Vl_Config.Load_ECurrentFile		(szPath, stRecipeInfo.stImageQ.stECurrentOpt);
		bReturn &= Vl_Config.Load_OpticalCenterFile	(szPath, stRecipeInfo.stImageQ.stOpticalCenterOpt);
		bReturn &= Vl_Config.Load_RotateFile		(szPath, stRecipeInfo.stImageQ.stRotateOpt);
		bReturn &= Vl_Config.Load_DistortionFile	(szPath, stRecipeInfo.stImageQ.stDistortionOpt);
		bReturn &= Vl_Config.Load_FOVFile			(szPath, stRecipeInfo.stImageQ.stFovOpt);
		bReturn &= Vl_Config.Load_DynamicFile		(szPath, stRecipeInfo.stImageQ.stDynamicOpt);
		bReturn &= Vl_Config.Load_DefectPixelFile	(szPath, stRecipeInfo.stImageQ.stDefectPixelOpt);
		//bReturn &= Vl_Config.Load_SFRFile			(szPath, stRecipeInfo.stImageQ.stSFROpt);
		bReturn &= Vl_Config.Load_SFRFile_Dist		(szPath, stRecipeInfo.stImageQ.stSFROpt);
		bReturn &= Vl_Config.Load_SNR_LightFile		(szPath, stRecipeInfo.stImageQ.stSNR_LightOpt);
		bReturn &= Vl_Config.Load_IntencityFile		(szPath, stRecipeInfo.stImageQ.stIntensityOpt);
		bReturn &= Vl_Config.Load_ShadingFile		(szPath, stRecipeInfo.stImageQ.stShadingOpt);
		bReturn &= Vl_Config.Load_ParticleFile		(szPath, stRecipeInfo.stImageQ.stParticleOpt);
		bReturn &= Vl_Config.Load_HotPixelFile		(szPath, stRecipeInfo.stImageQ.stHotPixelOpt);
		bReturn &= Vl_Config.Load_FPNFile			(szPath, stRecipeInfo.stImageQ.stFPNOpt);
		bReturn &= Vl_Config.Load_3D_DepthFile		(szPath, stRecipeInfo.stImageQ.st3D_DepthOpt);
		bReturn &= Vl_Config.Load_EEPROM_VerifyFile	(szPath, stRecipeInfo.stImageQ.stEEPROM_VerifyOpt);
		bReturn &= Vl_Config.Load_TemperatureSensorFile(szPath, stRecipeInfo.stImageQ.stTemperatureSensorOpt);
		bReturn &= Vl_Config.Load_Particle_EntryFile(szPath, stRecipeInfo.stImageQ.stParticle_EntryOpt);
		

	}
	break;

	case Sys_Stereo_Cal:
	{
		bReturn &= Load_SteCal_Info(szPath, stRecipeInfo.stSte_CAL);
	}
	break;

	case Sys_3D_Cal:
	{
		//bReturn &= Load_3DCal_Info(szPath, stRecipeInfo.st3D_CAL);
	}
	break;

	default:
		break;
	}

	return bReturn;
}

//=============================================================================
// Method		: Save_RecipeFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_RecipeInfo * pstRecipeInfo
// Qualifier	:
// Last Update	: 2016/3/18 - 11:26
// Desc.		:
//=============================================================================
BOOL CFile_Recipe::Save_RecipeFile(__in LPCTSTR szPath, __in const ST_RecipeInfo* pstRecipeInfo)
{
	BOOL bReturn = TRUE;
	CFile_VI_Config Vl_Config;

	bReturn = __super::Save_RecipeFile(szPath, pstRecipeInfo);
	bReturn &= Save_LVDS(szPath, &pstRecipeInfo->stLVDSInfo);

	switch (pstRecipeInfo->nInspectionType)
	{
	case Sys_Focusing:
	{
		bReturn &= Vl_Config.Save_ECurrentFile		(szPath, &pstRecipeInfo->stFocus.stECurrentOpt);
		bReturn &= Vl_Config.Save_OpticalCenterFile	(szPath, &pstRecipeInfo->stFocus.stOpticalCenterOpt);
		bReturn &= Vl_Config.Save_RotateFile		(szPath, &pstRecipeInfo->stFocus.stRotateOpt);
		bReturn &= Vl_Config.Save_DefectPixelFile	(szPath, &pstRecipeInfo->stFocus.stDefectPixelOpt);
		bReturn &= Vl_Config.Save_SFRFile			(szPath, &pstRecipeInfo->stFocus.stSFROpt);
		bReturn &= Vl_Config.Save_ParticleFile		(szPath, &pstRecipeInfo->stFocus.stParticleOpt);
		bReturn &= Vl_Config.Save_ActiveAlignFile	(szPath, &pstRecipeInfo->stFocus.stActiveAlignOpt);
		bReturn &= Vl_Config.Save_TorqueFile		(szPath, &pstRecipeInfo->stFocus.stTorqueOpt);
		bReturn &= Vl_Config.Save_Focus				(szPath, &pstRecipeInfo->stFocus.stFocusOpt);
	}
	break;

	case Sys_2D_Cal:
	{
		//bReturn &= Save_2DCal_Para(szPath, &pstRecipeInfo->st2D_CAL.Parameter);
		bReturn &= Save_2DCal_Info(szPath, &pstRecipeInfo->st2D_CAL);
	}
	break;

	case Sys_Image_Test:
	{
		bReturn &= Vl_Config.Save_ECurrentFile		(szPath, &pstRecipeInfo->stImageQ.stECurrentOpt);
		bReturn &= Vl_Config.Save_OpticalCenterFile	(szPath, &pstRecipeInfo->stImageQ.stOpticalCenterOpt);
		bReturn &= Vl_Config.Save_RotateFile		(szPath, &pstRecipeInfo->stImageQ.stRotateOpt);
		bReturn &= Vl_Config.Save_DistortionFile	(szPath, &pstRecipeInfo->stImageQ.stDistortionOpt);
		bReturn &= Vl_Config.Save_FOVFile			(szPath, &pstRecipeInfo->stImageQ.stFovOpt);
		bReturn &= Vl_Config.Save_DynamicFile		(szPath, &pstRecipeInfo->stImageQ.stDynamicOpt);
		bReturn &= Vl_Config.Save_DefectPixelFile	(szPath, &pstRecipeInfo->stImageQ.stDefectPixelOpt);
		//bReturn &= Vl_Config.Save_SFRFile			(szPath, &pstRecipeInfo->stImageQ.stSFROpt);
		bReturn &= Vl_Config.Save_SFRFile_Dist		(szPath, pstRecipeInfo->stImageQ.stSFROpt);
		bReturn &= Vl_Config.Save_SNR_LightFile		(szPath, &pstRecipeInfo->stImageQ.stSNR_LightOpt);
		bReturn &= Vl_Config.Save_IntencityFile		(szPath, &pstRecipeInfo->stImageQ.stIntensityOpt);
		bReturn &= Vl_Config.Save_ShadingFile		(szPath, &pstRecipeInfo->stImageQ.stShadingOpt);
		bReturn &= Vl_Config.Save_ParticleFile		(szPath, &pstRecipeInfo->stImageQ.stParticleOpt);
		bReturn &= Vl_Config.Save_HotPixelFile		(szPath, &pstRecipeInfo->stImageQ.stHotPixelOpt);
		bReturn &= Vl_Config.Save_FPNFile			(szPath, &pstRecipeInfo->stImageQ.stFPNOpt);
		bReturn &= Vl_Config.Save_3D_DepthFile		(szPath, &pstRecipeInfo->stImageQ.st3D_DepthOpt);
		bReturn &= Vl_Config.Save_EEPROM_VerifyFile(szPath, &pstRecipeInfo->stImageQ.stEEPROM_VerifyOpt);
		bReturn &= Vl_Config.Save_TemperatureSensorFile(szPath, &pstRecipeInfo->stImageQ.stTemperatureSensorOpt);
		bReturn &= Vl_Config.Save_Particle_EntryFile(szPath, &pstRecipeInfo->stImageQ.stParticle_EntryOpt);
	}
	break;

	case Sys_Stereo_Cal:
	{
		bReturn &= Save_SteCal_Info(szPath, &pstRecipeInfo->stSte_CAL);
	}
	break;

	case Sys_3D_Cal:
	{
		//bReturn &= Save_2DCal_Para(szPath, &pstRecipeInfo->st2D_CAL.Parameter);
		//bReturn &= Save_3DCal_Info(szPath, &pstRecipeInfo->st3D_CAL);
	}
	break;

	default:
		break;
	}

	return bReturn;
}

//=============================================================================
// Method		: Load_LVDS
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_LVDSInfo & stLVDSInfo
// Qualifier	:
// Last Update	: 2017/8/24 - 16:45
// Desc.		:
//=============================================================================
BOOL CFile_Recipe::Load_LVDS(__in LPCTSTR szPath, __out ST_LVDSInfo& stLVDSInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };
	int		nData = 0;

	// Board Number
// 	nData = GetPrivateProfileInt(LVDS_AppName, BoardNumber_KeyName, 0, szPath);
// 	stLVDSInfo.nBoardNo[0] = nData;
// 
// 	// Sensor Type
// 	nData = GetPrivateProfileInt(LVDS_AppName, SensorType_KeyName, 0, szPath);
// 	stLVDSInfo.stLVDSOption.nSensorType = (enImgSensorType)nData;
// 
// 	// ConvFormat
// 	nData = GetPrivateProfileInt(LVDS_AppName, ConvFormat_KeyName, 0, szPath);
// 	stLVDSInfo.stLVDSOption.nConvFormat = (enConvFormat)nData;
// 
// 	// ClockSelect
// 	nData = GetPrivateProfileInt(LVDS_AppName, ClockSelect_KeyName, 0, szPath);
// 	stLVDSInfo.stLVDSOption.nClockSelect = (enDAQClockSelect)nData;
// 
// 	// ClockUse
// 	nData = GetPrivateProfileInt(LVDS_AppName, ClockUse_KeyName, 0, szPath);
// 	stLVDSInfo.stLVDSOption.nClockUse = (enDAQClockUse)nData;
// 
// 	// DataMode
// 	nData = GetPrivateProfileInt(LVDS_AppName, DataMode_KeyName, 0, szPath);
// 	stLVDSInfo.stLVDSOption.nDataMode = (enDAQDataMode)nData;
// 
// 	// DvalUse
// 	nData = GetPrivateProfileInt(LVDS_AppName, DvalUse_KeyName, 0, szPath);
// 	stLVDSInfo.stLVDSOption.nDvalUse = (enDAQDvalUse)nData;
// 
// 	// HsyncPolarity
// 	nData = GetPrivateProfileInt(LVDS_AppName, HsyncPolarity_KeyName, 0, szPath);
// 	stLVDSInfo.stLVDSOption.nHsyncPolarity = (enDAQHsyncPolarity)nData;
// 
// 	// MIPILane
// 	nData = GetPrivateProfileInt(LVDS_AppName, MIPILane_KeyName, 0, szPath);
// 	stLVDSInfo.stLVDSOption.nMIPILane = (enDAQMIPILane)nData;
// 
// 	// PClockPolarity
// 	nData = GetPrivateProfileInt(LVDS_AppName, PClockPolarity_KeyName, 0, szPath);
// 	stLVDSInfo.stLVDSOption.nPClockPolarity = (enDAQPclkPolarity)nData;
// 
// 	// VideoMode
// 	nData = GetPrivateProfileInt(LVDS_AppName, VideoMode_KeyName, 0, szPath);
// 	stLVDSInfo.stLVDSOption.nVideoMode = (enDAQVideoMode)nData;
// 
// 	// Clock
// 	GetPrivateProfileString(LVDS_AppName, Clock_KeyName, _T("10000"), inBuff, 255, szPath);
// 	stLVDSInfo.stLVDSOption.dwClock = (DWORD)_ttol(inBuff);
// 
// 	// WidthMultiple
// 	GetPrivateProfileString(LVDS_AppName, WidthMultiple_KeyName, _T("1.0"), inBuff, 255, szPath);
// 	stLVDSInfo.stLVDSOption.dWidthMultiple = _ttof(inBuff);
// 
// 	// HeightMultiple
// 	GetPrivateProfileString(LVDS_AppName, HeightMultiple_KeyName, _T("1.0"), inBuff, 255, szPath);
// 	stLVDSInfo.stLVDSOption.dHeightMultiple = _ttof(inBuff);

	// I2C File
	GetPrivateProfileString(LVDS_AppName, _T("I2C File"), _T(""), inBuff, 255, szPath);
	stLVDSInfo.strI2CFileName = inBuff;

	GetPrivateProfileString(LVDS_AppName, _T("I2C File_2"), _T(""), inBuff, 255, szPath);
	stLVDSInfo.strI2CFileName_2 = inBuff;
	return TRUE;
}

//=============================================================================
// Method		: Save_LVDS
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_LVDSInfo * pstLVDSInfo
// Qualifier	:
// Last Update	: 2018/1/10 - 20:20
// Desc.		:
//=============================================================================
BOOL CFile_Recipe::Save_LVDS(__in LPCTSTR szPath, __in const ST_LVDSInfo* pstLVDSInfo)
{
	if (NULL == szPath)
		return FALSE;

	CString strValue;

	// Board Number
// 	strValue.Format(_T("%d"), pstLVDSInfo->nBoardNo[0]);
// 	WritePrivateProfileString(LVDS_AppName, BoardNumber_KeyName, strValue, szPath);
// 
// 	// Sensor Type
// 	strValue.Format(_T("%d"), pstLVDSInfo->stLVDSOption.nSensorType);
// 	WritePrivateProfileString(LVDS_AppName, SensorType_KeyName, strValue, szPath);
// 
// 	// ConvFormat
// 	strValue.Format(_T("%d"), pstLVDSInfo->stLVDSOption.nConvFormat);
// 	WritePrivateProfileString(LVDS_AppName, ConvFormat_KeyName, strValue, szPath);
// 
// 	// ClockSelect
// 	strValue.Format(_T("%d"), pstLVDSInfo->stLVDSOption.nClockSelect);
// 	WritePrivateProfileString(LVDS_AppName, ClockSelect_KeyName, strValue, szPath);
// 
// 	// ClockUse
// 	strValue.Format(_T("%d"), pstLVDSInfo->stLVDSOption.nClockUse);
// 	WritePrivateProfileString(LVDS_AppName, ClockUse_KeyName, strValue, szPath);
// 
// 	// DataMode
// 	strValue.Format(_T("%d"), pstLVDSInfo->stLVDSOption.nDataMode);
// 	WritePrivateProfileString(LVDS_AppName, DataMode_KeyName, strValue, szPath);
// 
// 	// DvalUse
// 	strValue.Format(_T("%d"), pstLVDSInfo->stLVDSOption.nDvalUse);
// 	WritePrivateProfileString(LVDS_AppName, DvalUse_KeyName, strValue, szPath);
// 
// 	// HsyncPolarity
// 	strValue.Format(_T("%d"), pstLVDSInfo->stLVDSOption.nHsyncPolarity);
// 	WritePrivateProfileString(LVDS_AppName, HsyncPolarity_KeyName, strValue, szPath);
// 
// 	// MIPILane
// 	strValue.Format(_T("%d"), pstLVDSInfo->stLVDSOption.nMIPILane);
// 	WritePrivateProfileString(LVDS_AppName, MIPILane_KeyName, strValue, szPath);
// 
// 	// PClockPolarity
// 	strValue.Format(_T("%d"), pstLVDSInfo->stLVDSOption.nPClockPolarity);
// 	WritePrivateProfileString(LVDS_AppName, PClockPolarity_KeyName, strValue, szPath);
// 
// 	// VideoMode
// 	strValue.Format(_T("%d"), pstLVDSInfo->stLVDSOption.nVideoMode);
// 	WritePrivateProfileString(LVDS_AppName, VideoMode_KeyName, strValue, szPath);
// 
// 	// Clock
// 	strValue.Format(_T("%d"), pstLVDSInfo->stLVDSOption.dwClock);
// 	WritePrivateProfileString(LVDS_AppName, Clock_KeyName, strValue, szPath);
// 
// 	// WidthMultiple
// 	strValue.Format(_T("%1.1f"), pstLVDSInfo->stLVDSOption.dWidthMultiple);
// 	WritePrivateProfileString(LVDS_AppName, WidthMultiple_KeyName, strValue, szPath);
// 
// 	// HeightMultiple
// 	strValue.Format(_T("%1.1f"), pstLVDSInfo->stLVDSOption.dHeightMultiple);
// 	WritePrivateProfileString(LVDS_AppName, HeightMultiple_KeyName, strValue, szPath);

	// I2C File
	strValue = pstLVDSInfo->strI2CFileName;
	WritePrivateProfileString(LVDS_AppName, _T("I2C File"), strValue, szPath);

	strValue = pstLVDSInfo->strI2CFileName_2;
	WritePrivateProfileString(LVDS_AppName, _T("I2C File_2"), strValue, szPath);
	return TRUE;
}

//=============================================================================
// Method		: Load_SteCal_Info
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_SteCal_Info & stOutSteCal_Info
// Qualifier	:
// Last Update	: 2018/3/7 - 8:21
// Desc.		:
//=============================================================================
BOOL CFile_Recipe::Load_SteCal_Info(__in LPCTSTR szPath, __out ST_SteCal_Info& stOutSteCal_Info)
{
	if (NULL == szPath)
		return FALSE;

	BOOL bReturn = TRUE;

	bReturn &= Load_SteCal_Para(szPath, stOutSteCal_Info.Parameter);

	return bReturn;
}

//=============================================================================
// Method		: Save_SteCal_Info
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_SteCal_Info * pstSteCal_Info
// Qualifier	:
// Last Update	: 2018/3/7 - 8:25
// Desc.		:
//=============================================================================
BOOL CFile_Recipe::Save_SteCal_Info(__in LPCTSTR szPath, __in const ST_SteCal_Info* pstSteCal_Info)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstSteCal_Info)
		return FALSE;

	BOOL bReturn = TRUE;

	bReturn &= Save_SteCal_Para(szPath, &pstSteCal_Info->Parameter);

	return bReturn;
}

//=============================================================================
// Method		: Load_SteCal_Para
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_SteCal_Para & stOutSteCal_Para
// Qualifier	:
// Last Update	: 2018/3/7 - 8:25
// Desc.		:
//=============================================================================
BOOL CFile_Recipe::Load_SteCal_Para(__in LPCTSTR szPath, __out ST_SteCal_Para& stOutSteCal_Para)
{
	if (NULL == szPath)
		return FALSE;

	BOOL bReturn = TRUE;
	TCHAR   inBuff[255] = { 0, };
	CString strKeyName;

	GetPrivateProfileString(AppName_SteCal_Para, g_Param_2DCal[Param_SteCal_Board_W], _T("5"), inBuff, 255, szPath);
	stOutSteCal_Para.iBoard_Width = _ttoi(inBuff);

	GetPrivateProfileString(AppName_SteCal_Para, g_Param_2DCal[Param_SteCal_Board_H], _T("5"), inBuff, 255, szPath);
	stOutSteCal_Para.iBoard_Height = _ttoi(inBuff);

	return bReturn;
}

//=============================================================================
// Method		: Save_SteCal_Para
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_SteCal_Para * pstSteCal_Para
// Qualifier	:
// Last Update	: 2018/3/7 - 8:25
// Desc.		:
//=============================================================================
BOOL CFile_Recipe::Save_SteCal_Para(__in LPCTSTR szPath, __in const ST_SteCal_Para* pstSteCal_Para)
{
	if (NULL == szPath)
		return FALSE;

	BOOL bReturn = TRUE;
	CString strValue;
	CString strKeyName;

	strValue.Format(_T("%d"), pstSteCal_Para->iBoard_Width);
	WritePrivateProfileString(AppName_SteCal_Para, g_Param_2DCal[Param_SteCal_Board_W], strValue, szPath);

	strValue.Format(_T("%d"), pstSteCal_Para->iBoard_Height);
	WritePrivateProfileString(AppName_SteCal_Para, g_Param_2DCal[Param_SteCal_Board_H], strValue, szPath);

	return bReturn;
}

