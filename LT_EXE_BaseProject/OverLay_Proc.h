﻿#ifndef Overlay_Proc_h__
#define Overlay_Proc_h__

#pragma once

typedef enum enOverlayMode
{
	OvrMode_LINE,		// 직선
	OvrMode_RECTANGLE,	// 직사각형
	OvrMode_CIRCLE,		// 원
	OvrMode_TXT,		// 글씨
	OvrMode_MaxNum,
};

#include "cv.h"
#include "highgui.h"
#include "Def_DataStruct.h"

// COverlay_Proc
class COverlay_Proc
{

public:
	COverlay_Proc();
	virtual ~COverlay_Proc();

	enModelType		m_ModelType;

	void SetModelType(__in enModelType nModelType)
	{
		m_ModelType = nModelType;
	}

	BOOL	m_bTestmode;

	void	Overlay_LockingScrew		(__inout IplImage* lpImage, __in ST_Torque_Opt			stOption, __in ST_Torque_Data			stData);
	void	Overlay_ReleaseScrew		(__inout IplImage* lpImage, __in ST_Torque_Opt			stOption, __in ST_Torque_Data			stData);
	void	Overlay_PreFocusing			(__inout IplImage* lpImage, __in ST_OpticalCenter_Opt	stOption, __in ST_OpticalCenter_Data	stData);
	void	Overlay_OpticalCenter		(__inout IplImage* lpImage, __in ST_OpticalCenter_Opt	stOption, __in ST_OpticalCenter_Data	stData, __in enOverlayItem eOverItem);
	void	Overlay_Current				(__inout IplImage* lpImage, __in ST_ECurrent_Opt		stOption, __in ST_ECurrent_Data			stData);
	void	Overlay_FOV					(__inout IplImage* lpImage, __in ST_FOV_Opt				stOption, __in ST_FOV_Data				stData);
	void	Overlay_SFR					(__inout IplImage* lpImage, __in ST_SFR_Opt				stOption, __in ST_SFR_Data				stData);
	void	Overlay_SFR_Group			(__inout IplImage* lpImage, __in ST_SFR_Opt				stOption, __in ST_SFR_Data				stData, __in UINT nGroupCnt);
	void	Overlay_SFR_Tilt			(__inout IplImage* lpImage, __in ST_SFR_Opt				stOption, __in ST_SFR_Data				stData);
	void	Overlay_Distortion			(__inout IplImage* lpImage, __in ST_Distortion_Opt		stOption, __in ST_Distortion_Data		stData);
	void	Overlay_Rotation			(__inout IplImage* lpImage, __in ST_Rotate_Opt			stOption, __in ST_Rotate_Data			stData,  __in enOverlayItem eOverItem);
	void	Overlay_Particle			(__inout IplImage* lpImage, __in ST_Particle_Opt		stOption, __in ST_Particle_Data			stData);
	void	Overlay_Particle_SNR		(__inout IplImage* lpImage, __in ST_Dynamic_Opt			stOption, __in ST_Dynamic_Data			stData);
	void	Overlay_DefectPixel			(__inout IplImage* lpImage, __in ST_DefectPixel_Opt		stOption, __in ST_DefectPixel_Data		stData);
	void	Overlay_Intencity			(__inout IplImage* lpImage, __in ST_Intensity_Opt		stOption, __in ST_Intensity_Data		stData);
	void	Overlay_SNR_Light			(__inout IplImage* lpImage, __in ST_SNR_Light_Opt		stOption, __in ST_SNR_Light_Data		stData);
	void	Overlay_SNR_Shading			(__inout IplImage* lpImage, __in ST_Shading_Opt			stOption, __in ST_Shading_Data			stData);

	void	Overlay_HotPixel			(__inout IplImage* lpImage, __in ST_HotPixel_Opt		stOption, __in ST_HotPixel_Data			stData);
	void	Overlay_FPN					(__inout IplImage* lpImage, __in ST_FPN_Opt				stOption, __in ST_FPN_Data				stData);
	void	Overlay_3D_Depth			(__inout IplImage* lpImage, __in ST_3D_Depth_Opt		stOption, __in ST_3D_Depth_Data			stData);
	void	Overlay_EEPRAM				(__inout IplImage* lpImage, __in ST_EEPROM_Verify_Opt	stOption, __in ST_EEPROM_Verify_Data	stData);
	void	Overlay_TemperatureSensor	(__inout IplImage* lpImage, __in ST_TemperatureSensor_Opt	stOption, __in ST_TemperatureSensor_Data	stData);
	void	Overlay_Particle_Entry		(__inout IplImage* lpImage, __in ST_Particle_Entry_Opt		stOption, __in ST_Particle_Entry_Data			stData);

	void	Overlay_LensScan			(__inout IplImage* lpImage, __in ST_SFR_Opt				stOption, __in ST_SFR_Data				stData);
	void	Overlay_Bestfocus			(__inout IplImage* lpImage, __in ST_SFR_Opt				stOption, __in ST_SFR_Data				stData);

	void	Overlay_SFR_Group(__inout IplImage* lpImage, __in ST_SFR_Opt stOption, __in ST_SFR_Data stData);
protected:

	void	Overlay_Process(__inout IplImage* lpImage, __in enOverlayMode enMode, __in CRect rtROI, __in COLORREF clrLineColor = RGB(255, 255, 0), __in int iLineSize = 1, __in double dbFontSize = 1.0, __in CString szText = _T(""));

};


#endif // Overlay_Proc_h__
