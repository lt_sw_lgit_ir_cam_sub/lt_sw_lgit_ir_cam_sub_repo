//*****************************************************************************
// Filename	: 	Wnd_Cfg_SteCal.cpp
// Created	:	2018/2/23 - 15:28
// Modified	:	2018/2/23 - 15:28
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
// Wnd_Cfg_SteCal.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_Cfg_SteCal.h"

// CWnd_Cfg_SteCal
#define IDC_EDIT_PARAM	2000
#define IDC_EDIT_SPEC	2001


IMPLEMENT_DYNAMIC(CWnd_Cfg_SteCal, CWnd)

CWnd_Cfg_SteCal::CWnd_Cfg_SteCal()
{
	VERIFY(m_font.CreateFont(
		24,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_SteCal::~CWnd_Cfg_SteCal()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_SteCal, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

// CWnd_Cfg_SteCal 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/11/10 - 17:25
// Desc.		:
//=============================================================================
int CWnd_Cfg_SteCal::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	// 세팅 파라미터
	for (UINT nIdx = 0; nIdx < Param_SteCal_MaxNum; nIdx++)
	{
		m_st_Parameter[nIdx].SetTextAlignment(StringAlignmentNear);
		m_st_Parameter[nIdx].SetFont_Gdip(L"Arial", 9.0F, FontStyleRegular);
		m_st_Parameter[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Parameter[nIdx].SetColorStyle(CVGStatic::ColorStyle_Black);
		m_st_Parameter[nIdx].Create(g_Param_SteCal[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

		m_ed_Parameter[nIdx].Create(dwStyle | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EDIT_PARAM + nIdx);
		m_ed_Parameter[nIdx].SetFont(&m_font);
		m_ed_Parameter[nIdx].SetValidChars(_T("0123456789.-"));
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/11/10 - 17:25
// Desc.		:
//=============================================================================
void CWnd_Cfg_SteCal::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMargin		= 10;
	int iSpacing	= 5;

	int iLeft		= iMargin;
	int iTop		= iMargin;
	int iWidth		= cx - iMargin - iMargin;
	int iHeight		= cy - iMargin - iMargin;

	int iCtrlHeight = 32;
	int iTempWidth	= (iWidth - (iSpacing * 3)) / 4;
	int iNameWidth	= iTempWidth;

	int iLeftEdit	= iLeft + iNameWidth + iSpacing;
	int iLeftSub	= iLeftEdit + iTempWidth + iSpacing;
	int iLeftEditSub= iLeftSub + iNameWidth + iSpacing;

	int iListWidth	= iWidth;
	int iListHeight = 300;

	// 세팅 파라미터
	for (UINT nIdx = 0; nIdx < Param_SteCal_MaxNum; nIdx++)
	{
		if (0 == (nIdx % 2))
		{
			m_st_Parameter[nIdx].MoveWindow(iLeft, iTop, iNameWidth, iCtrlHeight);
			m_ed_Parameter[nIdx].MoveWindow(iLeftEdit, iTop, iTempWidth, iCtrlHeight);
		}
		else
		{
			m_st_Parameter[nIdx].MoveWindow(iLeftSub, iTop, iNameWidth, iCtrlHeight);
			m_ed_Parameter[nIdx].MoveWindow(iLeftEditSub, iTop, iTempWidth, iCtrlHeight);
			iTop += iCtrlHeight + iSpacing;
		}
	}

	if ( 1 == (Param_SteCal_MaxNum % 2))
	{
		iTop += iCtrlHeight + iSpacing;
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/5/29 - 19:39
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_SteCal::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: Set_TestItemInfo
// Access		: public  
// Returns		: void
// Parameter	: __in ST_TestItemInfo * pstTestItemInfo
// Qualifier	:
// Last Update	: 2017/11/10 - 20:34
// Desc.		:
//=============================================================================
void CWnd_Cfg_SteCal::Set_TestItemInfo(__in ST_TestItemInfo* pstTestItemInfo)
{

}

//=============================================================================
// Method		: Get_TestItemInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_TestItemInfo & stOutTestItemInfo
// Qualifier	:
// Last Update	: 2017/11/11 - 16:58
// Desc.		:
//=============================================================================
void CWnd_Cfg_SteCal::Get_TestItemInfo(__out ST_TestItemInfo& stOutTestItemInfo)
{

}

//=============================================================================
// Method		: Set_SteCal_Para
// Access		: public  
// Returns		: void
// Parameter	: __in ST_SteCal_Para * pstSteCal_Para
// Qualifier	:
// Last Update	: 2018/2/23 - 15:22
// Desc.		:
//=============================================================================
void CWnd_Cfg_SteCal::Set_SteCal_Para(__in ST_SteCal_Para* pstSteCal_Para)
{
	CString szText;

	UINT nIdx = Para_Left;	

	szText.Format(_T("%d"), pstSteCal_Para->iBoard_Width);
	m_ed_Parameter[Param_SteCal_Board_W].SetWindowText(szText);
	szText.Format(_T("%d"), pstSteCal_Para->iBoard_Height);
	m_ed_Parameter[Param_SteCal_Board_H].SetWindowText(szText);
	
}

//=============================================================================
// Method		: Get_SteCal_Para
// Access		: public  
// Returns		: void
// Parameter	: __out ST_SteCal_Para & stOutSteCal_Para
// Qualifier	:
// Last Update	: 2018/2/23 - 15:23
// Desc.		:
//=============================================================================
void CWnd_Cfg_SteCal::Get_SteCal_Para(__out ST_SteCal_Para& stOutSteCal_Para)
{
	CString szText;

	m_ed_Parameter[Param_SteCal_Board_W].GetWindowText(szText);
	stOutSteCal_Para.iBoard_Width = _ttoi(szText.GetBuffer(0));

	m_ed_Parameter[Param_SteCal_Board_H].GetWindowText(szText);
	stOutSteCal_Para.iBoard_Height = _ttoi(szText.GetBuffer(0));
}
