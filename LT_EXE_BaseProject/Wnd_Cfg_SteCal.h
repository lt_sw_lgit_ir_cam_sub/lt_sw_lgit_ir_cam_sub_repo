//*****************************************************************************
// Filename	: 	Wnd_Cfg_SteCal.h
// Created	:	2018/2/23 - 15:28
// Modified	:	2018/2/23 - 15:28
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
// CWnd_Cfg_SteCal

#ifndef Wnd_Cfg_SteCal_h__
#define Wnd_Cfg_SteCal_h__

#pragma once

#include "VGStatic.h"
#include "Def_Enum_Cm.h"
#include "Def_TestItem_Cm.h"
#include "Def_T_SteCal.h"

//=============================================================================
// CWnd_Cfg_SteCal
//=============================================================================
class CWnd_Cfg_SteCal : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Cfg_SteCal)

public:
	CWnd_Cfg_SteCal();
	virtual ~CWnd_Cfg_SteCal();

protected:
	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);	
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);

	DECLARE_MESSAGE_MAP()

	CFont				m_font;

	// 세팅 파라미터 옵션
	CVGStatic			m_st_Parameter[Param_SteCal_MaxNum];
	CMFCMaskedEdit		m_ed_Parameter[Param_SteCal_MaxNum];

public:
	void	Set_TestItemInfo	(__in ST_TestItemInfo* pstTestItemInfo);
	void	Get_TestItemInfo	(__out ST_TestItemInfo& stOutTestItemInfo);

	void	Set_SteCal_Para		(__in ST_SteCal_Para* pstSteCal_Para);
	void	Get_SteCal_Para		(__out ST_SteCal_Para& stOutSteCal_Para);

};

#endif // Wnd_Cfg_SteCal_h__

