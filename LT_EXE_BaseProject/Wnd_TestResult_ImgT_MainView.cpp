﻿// Wnd_TestResult_ImgT_MainView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "Wnd_TestResult_ImgT_MainView.h"


// CWnd_TestResult_ImgT_MainView
typedef enum TestResult_ImgID
{
	IDC_BTN_ITEM  = 1001,
	IDC_CMB_ITEM  = 2001,
	IDC_EDT_ITEM  = 3001,
	IDC_LIST_ITEM = 4001,
};

IMPLEMENT_DYNAMIC(CWnd_TestResult_ImgT_MainView, CWnd)

CWnd_TestResult_ImgT_MainView::CWnd_TestResult_ImgT_MainView()
{
	m_InspectionType = Sys_Focusing;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_TestResult_ImgT_MainView::~CWnd_TestResult_ImgT_MainView()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_TestResult_ImgT_MainView, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
	ON_CBN_SELENDOK(IDC_CMB_ITEM, OnLbnSelChangeTest)
END_MESSAGE_MAP()

// CWnd_TestResult_ImgT_MainView 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_TestResult_ImgT_MainView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < STI_TR_ImgT_Main_MAX; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdex].Create(g_szTestResult_ImgT_Main_Static[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	m_st_default.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_default.SetColorStyle(CVGStatic::ColorStyle_White);
	m_st_default.SetFont_Gdip(L"Arial", 9.0F);
	m_st_default.Create(_T("TEST RESULT"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);


	m_cb_TestItem.Create(dwStyle | CBS_DROPDOWNLIST | WS_VSCROLL, rectDummy, this, IDC_CMB_ITEM);
	m_cb_TestItem.SetFont(&m_font);

	UINT	nWndID = IDC_LIST_ITEM;

	
	m_Wnd_CurrentEntryData.SetOwner(GetOwner());
	m_Wnd_CurrentEntryData.Create(NULL, _T("Current"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	m_Wnd_CurrentData.SetOwner(GetOwner());
	m_Wnd_CurrentData.Create(NULL, _T("Current"), dwStyle /*| WS_BORDER*/, rectDummy,this, nWndID++);

	m_Wnd_EEPROMData.SetOwner(GetOwner());
	m_Wnd_EEPROMData.Create(NULL, _T("EEPROM"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	
	m_Wnd_DefectPixelData.SetOwner(GetOwner());
	m_Wnd_DefectPixelData.Create(NULL, _T("DefectPixel"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	m_Wnd_DistortionData.SetOwner(GetOwner());
	m_Wnd_DistortionData.Create(NULL, _T("Distortion"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	m_Wnd_DynamicRangeData.SetOwner(GetOwner());
	m_Wnd_DynamicRangeData.Create(NULL, _T("DynamicRange"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	m_Wnd_FovData.SetOwner(GetOwner());
	m_Wnd_FovData.Create(NULL, _T("Fov"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	m_Wnd_IntensityData.SetOwner(GetOwner());
	m_Wnd_IntensityData.Create(NULL, _T("Intencity"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	m_Wnd_OpticalData.SetOwner(GetOwner());
	m_Wnd_OpticalData.Create(NULL, _T("Optical"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	m_Wnd_RotateData.SetOwner(GetOwner());
	m_Wnd_RotateData.Create(NULL, _T("Rotate"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	//m_Wnd_SFRData.SetOwner(GetOwner());
	//m_Wnd_SFRData.Create(NULL, _T("SFR"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	for (int t = 0; t < SFR_TestNum_MaxEnum; t++)
	{
		m_Wnd_SFRData[t].SetOwner(GetOwner());
		m_Wnd_SFRData[t].Create(NULL, _T("SFR"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	}
	m_Wnd_ShadingData.SetOwner(GetOwner());
	m_Wnd_ShadingData.Create(NULL, _T("Shading"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	m_Wnd_SNR_LightData.SetOwner(GetOwner());
	m_Wnd_SNR_LightData.Create(NULL, _T("SNR_Light"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	m_Wnd_ParticleData.SetOwner(GetOwner());
	m_Wnd_ParticleData.Create(NULL, _T("Particle"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	m_Wnd_HotPixelData.SetOwner(GetOwner());
	m_Wnd_HotPixelData.Create(NULL, _T("HotPixel"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	m_Wnd_3DDepthData.SetOwner(GetOwner());
	m_Wnd_3DDepthData.Create(NULL, _T("3DDepth"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	m_Wnd_FPNData.SetOwner(GetOwner());
	m_Wnd_FPNData.Create(NULL, _T("FPN"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	m_Wnd_TemperatureSensorData.SetOwner(GetOwner());
	m_Wnd_TemperatureSensorData.Create(NULL, _T("TemperatureSensor"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	m_Wnd_Particle_EntryData.SetOwner(GetOwner());
	m_Wnd_Particle_EntryData.Create(NULL, _T("Particle_Entry"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	m_cb_TestItem.AddString(_T("Current"));

	m_Wnd_CurrentEntryData.ShowWindow(SW_HIDE);
	m_Wnd_CurrentData.ShowWindow(SW_SHOW);
	m_Wnd_EEPROMData.ShowWindow(SW_HIDE);
	m_Wnd_DefectPixelData.ShowWindow(SW_HIDE);
	m_Wnd_DistortionData.ShowWindow(SW_HIDE);
	m_Wnd_DynamicRangeData.ShowWindow(SW_HIDE);
	m_Wnd_FovData.ShowWindow(SW_HIDE);
	m_Wnd_IntensityData.ShowWindow(SW_HIDE);
	m_Wnd_OpticalData.ShowWindow(SW_HIDE);
	m_Wnd_RotateData.ShowWindow(SW_HIDE);
	for (int t = 0; t < SFR_TestNum_MaxEnum; t++)
	{
		m_Wnd_SFRData[t].ShowWindow(SW_HIDE);
	}
	m_Wnd_ShadingData.ShowWindow(SW_HIDE);
	m_Wnd_SNR_LightData.ShowWindow(SW_HIDE);
	m_Wnd_ParticleData.ShowWindow(SW_HIDE);
	m_Wnd_TemperatureSensorData.ShowWindow(SW_HIDE);
	m_Wnd_Particle_EntryData.ShowWindow(SW_HIDE);
	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT_MainView::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iMargin  = 10;
	int iSpacing = 5;

	int iLeft	 = 0;
	int iTop	 = 0;
	int iWidth   = cx;
	int iHeight  = cy;
	
	int iStHeight = 25;

	m_st_Item[STI_TR_ImgT_Main_Title].MoveWindow(iLeft, iTop, iWidth, iStHeight);
	
	iTop += iStHeight + 2;

	m_cb_TestItem.MoveWindow(iLeft, iTop, iWidth, iStHeight);
	iTop += iStHeight + 2;
	
	m_Wnd_CurrentEntryData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_CurrentData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_EEPROMData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_DefectPixelData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_DistortionData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_DynamicRangeData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_FovData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_IntensityData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_OpticalData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_RotateData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	for (int t = 0; t < SFR_TestNum_MaxEnum; t++)
	{
		m_Wnd_SFRData[t].MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	}
	m_Wnd_ShadingData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_SNR_LightData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_ParticleData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_HotPixelData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_3DDepthData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_FPNData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_TemperatureSensorData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Particle_EntryData.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	

}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_TestResult_ImgT_MainView::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: SetShowWindowResult
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/21 - 10:59
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT_MainView::SetShowWindowResult(int iItem)
{

	switch (m_InspectionType)
	{
	case Sys_Focusing:
		break;
	case Sys_2D_Cal:
		break;
	case Sys_Image_Test:
		break;
	case Sys_Stereo_Cal:

		break;
	case Sys_3D_Cal:
		break;
	default:
		break;
	}

}

//=============================================================================
// Method		: MoveWindow_Result
// Access		: protected  
// Returns		: void
// Parameter	: int x
// Parameter	: int y
// Parameter	: int nWidth
// Parameter	: int nHeight
// Qualifier	:
// Last Update	: 2017/10/21 - 11:16
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT_MainView::MoveWindow_Result(int x, int y, int nWidth, int nHeight)
{
	int iHeaderH = 40;
	int iListH = iHeaderH + 3 * 20;
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT_MainView::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (SW_HIDE == bShow)
	{
		SetShowWindowResult(-1);
	}
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT_MainView::OnRangeBtnCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_BTN_ITEM;
}

//=============================================================================
// Method		: SetTestItemResult
// Access		: public  
// Returns		: void
// Parameter	: __in  int iTestItem
// Qualifier	:
// Last Update	: 2017/10/21 - 10:45
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT_MainView::SetTestItem(__in int iTestItem)
{
	SetShowWindowResult(iTestItem);
}

//=============================================================================
// Method		: SetUpdateResult
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_RecipeInfo_Base * pstRecipeInfo
// Parameter	: __in int iTestItem
// Qualifier	:
// Last Update	: 2017/10/21 - 12:44
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT_MainView::SetUpdateResult(__in const ST_RecipeInfo_Base* pstRecipeInfo, __in int iTestItem)
{
	if (pstRecipeInfo == NULL)
		return;

	switch (m_InspectionType)
	{
	case Sys_Focusing:
		break;
	case Sys_2D_Cal:
		break;
	case Sys_Image_Test:
		break;
	case Sys_Stereo_Cal:
		break;
	case Sys_3D_Cal:
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: SetUpdateClear
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/21 - 14:23
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT_MainView::SetUpdateClear()
{

}

//=============================================================================
// Method		: AllDataReset
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/5/12 - 19:07
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT_MainView::AllDataReset()
{
	m_Wnd_CurrentEntryData.DataReset();
	m_Wnd_CurrentData.DataReset();
	m_Wnd_EEPROMData.DataReset();
	m_Wnd_OpticalData.DataReset();
	m_Wnd_FovData.DataReset();
	for (int t = 0; t < SFR_TestNum_MaxEnum; t++)
	{
		m_Wnd_SFRData[t].DataReset();
	}
	m_Wnd_DistortionData.DataReset();
	m_Wnd_RotateData.DataReset();
	m_Wnd_ParticleData.DataReset();

	m_Wnd_DynamicRangeData.DataReset();
	m_Wnd_DefectPixelData.DataReset();
	m_Wnd_IntensityData.DataReset();
	m_Wnd_ShadingData.DataReset();
	m_Wnd_SNR_LightData.DataReset();

	m_Wnd_HotPixelData.DataReset();
	m_Wnd_3DDepthData.DataReset();
	m_Wnd_FPNData.DataReset();
	m_Wnd_TemperatureSensorData.DataReset();
	m_Wnd_Particle_EntryData.DataReset();
}

//=============================================================================
// Method		: SetUI_Reset
// Access		: public  
// Returns		: void
// Parameter	: int nIdx
// Qualifier	:
// Last Update	: 2018/5/12 - 19:07
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT_MainView::SetUI_Reset(int nIdx)
{
	int nTypeCnt = 0;
	switch (nIdx)
	{
	case TI_ImgT_Fn_ECurrent:
	{
		m_Wnd_CurrentEntryData.DataReset();
		m_Wnd_CurrentData.DataReset();
	}
		break;

	case TI_ImgT_Fn_OpticalCenter:
	{
		m_Wnd_OpticalData.DataReset();
	}
		break;

	case TI_ImgT_Fn_FOV:
	{
		m_Wnd_FovData.DataReset();
	}
		break;

	case TI_ImgT_Fn_SFR:
	case TI_ImgT_Fn_SFR_2:
	case TI_ImgT_Fn_SFR_3:
	{
		nTypeCnt = nIdx - TI_ImgT_Fn_SFR;
		m_Wnd_SFRData[nTypeCnt].DataReset();
	}
		break;

	case TI_ImgT_Fn_Distortion:
	{
		m_Wnd_DistortionData.DataReset();
	}
		break;

	case TI_ImgT_Fn_Rotation:
	{
		m_Wnd_RotateData.DataReset();
	}
		break;

	case TI_ImgT_Fn_Stain:
	{
		m_Wnd_ParticleData.DataReset();
	}
		break;

	case TI_ImgT_Fn_Particle_SNR:
	{
		m_Wnd_DynamicRangeData.DataReset();
	}
		break;

	case TI_ImgT_Fn_DefectPixel:
	{
		m_Wnd_DefectPixelData.DataReset();
	}
		break;

	case TI_ImgT_Fn_Intensity:
	{
		m_Wnd_IntensityData.DataReset();
	}
		break;

	case TI_ImgT_Fn_Shading:
	{
		m_Wnd_ShadingData.DataReset();
	}
		break;

	case TI_ImgT_Fn_SNR_Light:
	{
		m_Wnd_SNR_LightData.DataReset();
	}
		break;

//#if (LT_EQP == SYS_GJ_IMAGE_TEST)
	case TI_ImgT_Fn_EEPROM_Verify:
	{
		m_Wnd_EEPROMData.DataReset();
	}
	break;

	case TI_ImgT_Fn_HotPixel:
	{	
		m_Wnd_HotPixelData.DataReset();
	}
		break;

	case TI_ImgT_Fn_3DDepth:
	{	
		m_Wnd_3DDepthData.DataReset();
	}
		break;

	case TI_ImgT_Fn_FPN:
	{	
		m_Wnd_FPNData.DataReset();
	}
		break;
//#endif

	case TI_ImgT_Fn_TemperatureSensor:
	{
		m_Wnd_TemperatureSensorData.DataReset();
	}
		break;
		
	case TI_ImgT_Fn_Particle_Entry:
	{
		m_Wnd_Particle_EntryData.DataReset();
	}
		break;
		

	default:
		break;
	}
}

void CWnd_TestResult_ImgT_MainView::SetUIData(int nIdx, LPVOID pParam)
{
	if (pParam == NULL)
	{
		return;
	}

	for (int t = 0; t < m_cb_TestItem.GetCount(); t++)
	{
		if (m_iSelectTest[t] == nIdx)
		{
			SelectNum(t);
			break;
		}
	}
	int nTypeCnt = 0;
	switch (nIdx)
	{
	case TI_ImgT_Fn_ECurrent:
	{
		ST_ECurrent_Data *pData = (ST_ECurrent_Data *)pParam;
		m_Wnd_CurrentEntryData.DataDisplay(*pData);
		m_Wnd_CurrentData.DataDisplay(*pData);
	}
		break;

	case TI_ImgT_Fn_OpticalCenter:
	{
		ST_OpticalCenter_Data *pData = (ST_OpticalCenter_Data *)pParam;
		m_Wnd_OpticalData.DataDisplay(*pData);
	}
		break;

	case TI_ImgT_Fn_FOV:
	{
		ST_FOV_Data *pData = (ST_FOV_Data *)pParam;
		m_Wnd_FovData.DataDisplay(*pData);
	}
		break;

	case TI_ImgT_Fn_SFR:
	case TI_ImgT_Fn_SFR_2:
	case TI_ImgT_Fn_SFR_3:
	{
		nTypeCnt = nIdx - TI_ImgT_Fn_SFR;
		ST_SFR_Data *pData = (ST_SFR_Data *)pParam;
		m_Wnd_SFRData[nTypeCnt].DataDisplay(*pData);
	}
		break;

	case TI_ImgT_Fn_Distortion:
	{
		ST_Distortion_Data *pData = (ST_Distortion_Data *)pParam;
		m_Wnd_DistortionData.DataDisplay(*pData);
	}
		break;

	case TI_ImgT_Fn_Rotation:
	{
		ST_Rotate_Data *pData = (ST_Rotate_Data *)pParam;
		m_Wnd_RotateData.DataDisplay(*pData);
	}
		break;

	case TI_ImgT_Fn_Stain:
	{
		ST_Particle_Data *pData = (ST_Particle_Data *)pParam;
		m_Wnd_ParticleData.DataDisplay(*pData);
	}
		break;

	case TI_ImgT_Fn_Particle_SNR:
	{
		ST_Dynamic_Data *pData = (ST_Dynamic_Data *)pParam;
		m_Wnd_DynamicRangeData.DataDisplay(*pData);
	}
		break;

	case TI_ImgT_Fn_DefectPixel:
	{
		ST_DefectPixel_Data *pData = (ST_DefectPixel_Data *)pParam;
		m_Wnd_DefectPixelData.DataDisplay(*pData);
	}
		break;

	case TI_ImgT_Fn_Intensity:
	{
		ST_Intensity_Data *pData = (ST_Intensity_Data *)pParam;
		m_Wnd_IntensityData.DataDisplay(*pData);
	}
		break;

	case TI_ImgT_Fn_Shading:
	{
		ST_Shading_Data *pData = (ST_Shading_Data *)pParam;
		m_Wnd_ShadingData.DataDisplay(*pData);
	}
		break;

	case TI_ImgT_Fn_SNR_Light:
	{
		ST_SNR_Light_Data *pData = (ST_SNR_Light_Data *)pParam;
		m_Wnd_SNR_LightData.DataDisplay(*pData);
	}
		break;

//#if (LT_EQP == SYS_GJ_IMAGE_TEST)
	case TI_ImgT_Fn_EEPROM_Verify:
	{
		ST_EEPROM_Verify_Data *pData = (ST_EEPROM_Verify_Data *)pParam;
		m_Wnd_EEPROMData.DataDisplay(*pData);
	}
		break;

	case TI_ImgT_Fn_HotPixel:
	{
		ST_HotPixel_Data *pData = (ST_HotPixel_Data *)pParam;
		m_Wnd_HotPixelData.DataDisplay(*pData);
	}
		break;

	case TI_ImgT_Fn_3DDepth:
	{
		ST_3D_Depth_Data *pData = (ST_3D_Depth_Data *)pParam;
		m_Wnd_3DDepthData.DataDisplay(*pData);
	}
		break;

	case TI_ImgT_Fn_FPN:
	{
		ST_FPN_Data *pData = (ST_FPN_Data *)pParam;
		m_Wnd_FPNData.DataDisplay(*pData);
	}
		break;
//#endif

	case TI_ImgT_Fn_TemperatureSensor:
	{
		ST_TemperatureSensor_Data *pData = (ST_TemperatureSensor_Data *)pParam;
		m_Wnd_TemperatureSensorData.DataDisplay(*pData);
	}
		break;
		
	case TI_ImgT_Fn_Particle_Entry:
	{
		ST_Particle_Entry_Data *pData = (ST_Particle_Entry_Data *)pParam;
		m_Wnd_Particle_EntryData.DataDisplay(*pData);
	}
		break;
		
	default:
		break;
	}
}

//=============================================================================
// Method		: SetClearTab
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/5/12 - 19:08
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT_MainView::SetClearTab()
{

	for (int t = 0; t < 20; t++)
		m_iSelectTest[t] = -1;

	if (m_cb_TestItem.GetCount() > 0)
	{
		m_cb_TestItem.ResetContent();
	}
}

//=============================================================================
// Method		: SetAddTab
// Access		: public  
// Returns		: void
// Parameter	: int nIdx
// Parameter	: int & iItemCnt
// Qualifier	:
// Last Update	: 2018/5/12 - 19:09
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT_MainView::SetAddTab(int nIdx, int &iItemCnt){

	switch (nIdx)
  	{
  	case TI_ImgT_Fn_ECurrent:
  	{
		m_iSelectTest[iItemCnt++ ] = nIdx; //들어온 순서대로 ID Input;
		m_cb_TestItem.AddString(_T("Current"));
  	}
  		break;

  	case TI_ImgT_Fn_OpticalCenter:
  	{
		m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
		m_cb_TestItem.AddString(_T("OpticalCenter"));
  	}
  		break;

  	case TI_ImgT_Fn_FOV:
  	{
		m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
		m_cb_TestItem.AddString(_T("FOV"));
  	}
  		break;

  //	case TI_ImgT_Fn_SFR:
  //	{
		//m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
		//m_cb_TestItem.AddString(_T("SFR"));
 	//}
  //		break;

	case TI_ImgT_Fn_SFR: //거리별로  보여줘야 됨 LJE
	{
			m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
			m_cb_TestItem.AddString(g_szSFR_TestNum[0]);
	}
		break;

	case TI_ImgT_Fn_SFR_2: //거리별로  보여줘야 됨 LJE
	{
			m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
			m_cb_TestItem.AddString(g_szSFR_TestNum[1]);

	}
		break;

	case TI_ImgT_Fn_SFR_3: //거리별로  보여줘야 됨 LJE
	{
			m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
			m_cb_TestItem.AddString(g_szSFR_TestNum[2]);
	}
		break;

  	case TI_ImgT_Fn_Distortion:
  	{
		m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
		m_cb_TestItem.AddString(_T("Distortion"));
  	}
  		break;

  	case TI_ImgT_Fn_Rotation:
  	{
		m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
		m_cb_TestItem.AddString(_T("Rotate"));
  	}
  		break;

  	case TI_ImgT_Fn_Stain:
  	{
		m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
		m_cb_TestItem.AddString(_T("Stain"));
  	}
  		break;

  	case TI_ImgT_Fn_Particle_SNR:
  	{
		m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
		m_cb_TestItem.AddString(_T("Dynamic"));
  	}
  		break;

  	case TI_ImgT_Fn_DefectPixel:
  	{
		m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
		m_cb_TestItem.AddString(_T("DefectPixel"));
  	}
  		break;

  	case TI_ImgT_Fn_Intensity:
  	{
		m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
		m_cb_TestItem.AddString(_T("Shading(RI)"));
  	}
  		break;

  	case TI_ImgT_Fn_Shading:
  	{
		m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
		m_cb_TestItem.AddString(_T("Shading(SNR)"));
  	}
  		break;

  	case TI_ImgT_Fn_SNR_Light:
  	{
		m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
		m_cb_TestItem.AddString(_T("SNR Light"));
  	}
		break;

//#if (LT_EQP == SYS_GJ_IMAGE_TEST)
	case TI_ImgT_Fn_EEPROM_Verify:
	{
		m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
		m_cb_TestItem.AddString(_T("EEPROM"));
	}
		break;

	case TI_ImgT_Fn_HotPixel:
	{
		m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
		m_cb_TestItem.AddString(_T("HotPixel"));
	}
		break;

	case TI_ImgT_Fn_3DDepth:
	{
		m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
		m_cb_TestItem.AddString(_T("3D Depth"));
	}
		break;

	case TI_ImgT_Fn_FPN:
	{
		m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
		m_cb_TestItem.AddString(_T("FPN"));
	}
  		break;
//#endif

	case TI_ImgT_Fn_TemperatureSensor:
	{
		m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
		m_cb_TestItem.AddString(_T("TemperatureSensor"));
	}
		break;
		
	case TI_ImgT_Fn_Particle_Entry:
	{
		m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
		m_cb_TestItem.AddString(_T("Particle"));
	}
		break;
  	default:
  		break;
  	}
}

//=============================================================================
// Method		: OnLbnSelChangeTest
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/5/12 - 19:10
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT_MainView::OnLbnSelChangeTest()
{
	m_Wnd_CurrentEntryData.ShowWindow(SW_HIDE);
	m_Wnd_CurrentData.ShowWindow(SW_HIDE);
	m_Wnd_EEPROMData.ShowWindow(SW_HIDE);
	m_Wnd_DefectPixelData.ShowWindow(SW_HIDE);
	m_Wnd_DistortionData.ShowWindow(SW_HIDE);
	m_Wnd_DynamicRangeData.ShowWindow(SW_HIDE);
	m_Wnd_FovData.ShowWindow(SW_HIDE);
	m_Wnd_IntensityData.ShowWindow(SW_HIDE);
	m_Wnd_OpticalData.ShowWindow(SW_HIDE);
	m_Wnd_RotateData.ShowWindow(SW_HIDE);
	for (int t = 0; t < SFR_TestNum_MaxEnum; t++)
	{
		m_Wnd_SFRData[t].ShowWindow(SW_HIDE);
	}
	m_Wnd_ShadingData.ShowWindow(SW_HIDE);
	m_Wnd_SNR_LightData.ShowWindow(SW_HIDE);
	m_Wnd_ParticleData.ShowWindow(SW_HIDE);
	m_Wnd_HotPixelData.ShowWindow(SW_HIDE);
	m_Wnd_3DDepthData.ShowWindow(SW_HIDE);
	m_Wnd_FPNData.ShowWindow(SW_HIDE);
	m_Wnd_TemperatureSensorData.ShowWindow(SW_HIDE);
	m_Wnd_Particle_EntryData.ShowWindow(SW_HIDE);

	int nIndex = m_cb_TestItem.GetCurSel();
	int nTypeCnt = 0;
	switch (m_iSelectTest[nIndex])
	{
	case TI_ImgT_Fn_ECurrent:
	{
		switch (m_ModelType)
		{
		case Model_OMS_Entry:
		case Model_OMS_Front:
		case Model_OMS_Front_Set:
			m_Wnd_CurrentEntryData.ShowWindow(SW_SHOW);
			break;

		case Model_MRA2:
		case Model_IKC:
			m_Wnd_CurrentData.ShowWindow(SW_SHOW);
		default:
			break;
		}
	}
		break;

	case TI_ImgT_Fn_OpticalCenter:
	{
		m_Wnd_OpticalData.ShowWindow(SW_SHOW);
	}
		break;

	case TI_ImgT_Fn_FOV:
	{
		m_Wnd_FovData.ShowWindow(SW_SHOW);				  
	}
		break;

	case TI_ImgT_Fn_SFR:
	case TI_ImgT_Fn_SFR_2:
	case TI_ImgT_Fn_SFR_3:
	{
		nTypeCnt = m_iSelectTest[nIndex] - TI_ImgT_Fn_SFR;
		m_Wnd_SFRData[nTypeCnt].ShowWindow(SW_SHOW);
	}
		break;

	case TI_ImgT_Fn_Distortion:
	{
		m_Wnd_DistortionData.ShowWindow(SW_SHOW);
	}
		break;

	case TI_ImgT_Fn_Rotation:
	{
		m_Wnd_RotateData.ShowWindow(SW_SHOW);
	}
		break;

	case TI_ImgT_Fn_Stain:
	{
		m_Wnd_ParticleData.ShowWindow(SW_SHOW);
	}
		break;

	case TI_ImgT_Fn_Particle_SNR:
	{
		m_Wnd_DynamicRangeData.ShowWindow(SW_SHOW);
	}
		break;

	case TI_ImgT_Fn_DefectPixel:
	{
		m_Wnd_DefectPixelData.ShowWindow(SW_SHOW);
	}
		break;

	case TI_ImgT_Fn_Intensity:
	{
		m_Wnd_IntensityData.ShowWindow(SW_SHOW);
	}
		break;

	case TI_ImgT_Fn_Shading:
	{
		m_Wnd_ShadingData.ShowWindow(SW_SHOW);
	}
		break;

	case TI_ImgT_Fn_SNR_Light:
	{
		m_Wnd_SNR_LightData.ShowWindow(SW_SHOW);
	}
		break;

//#if (LT_EQP == SYS_GJ_IMAGE_TEST)
	case TI_ImgT_Fn_EEPROM_Verify:
	{
		m_Wnd_EEPROMData.ShowWindow(SW_SHOW);
	}
		break;

	case TI_ImgT_Fn_HotPixel:
	{
		m_Wnd_HotPixelData.ShowWindow(SW_SHOW);
	}
		break;

	case TI_ImgT_Fn_3DDepth:
	{
		m_Wnd_3DDepthData.ShowWindow(SW_SHOW);
	}
		break;

	case TI_ImgT_Fn_FPN:
	{
		m_Wnd_FPNData.ShowWindow(SW_SHOW);
	}
		break;
//#endif
	case TI_ImgT_Fn_TemperatureSensor:
	{
		m_Wnd_TemperatureSensorData.ShowWindow(SW_SHOW);
	}
		break;

	case TI_ImgT_Fn_Particle_Entry:
	{
		m_Wnd_Particle_EntryData.ShowWindow(SW_SHOW);
	}
		break;
		
	default:
		break;
	}
}

//=============================================================================
// Method		: SelectNum
// Access		: public  
// Returns		: void
// Parameter	: UINT nSelect
// Qualifier	:
// Last Update	: 2018/5/12 - 19:10
// Desc.		:
//=============================================================================
void CWnd_TestResult_ImgT_MainView::SelectNum(UINT nSelect)
{
	m_cb_TestItem.SetCurSel(nSelect);
	OnLbnSelChangeTest();
}