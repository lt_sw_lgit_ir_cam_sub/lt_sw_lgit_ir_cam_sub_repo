﻿//*****************************************************************************
// Filename	: 	ColorSpace.h
// Created	:	2017/3/21 - 16:09
// Modified	:	2017/3/21 - 16:09
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef ColorSpace_h__
#define ColorSpace_h__

#pragma once

#include <afxwin.h>
#include "cv.h"
#include "Highgui.h"

typedef enum enChromaSampling
{
	YUV_444,	// 3 bytes per pixel     (12 bytes per 4 pixels)
	YUV_440,
	YUV_422,	// 4 bytes per 2 pixels  ( 8 bytes per 4 pixels)
	YUV_420,	// 6 bytes per 4 pixels, reordered
	YUV_411,	// 6 bytes per 4 pixels
	YUV_410,

	// ex)
	// 4 : 수평 샘플링 단위, 4 픽셀을 기준으로 한다는 뜻.
	// 2 : 첫 번째 수평 픽셀 4개에 대한 크로마(U, V) 샘플링 수, 4픽셀에서 2번 샘플링을 하니 2개씩 묶는다고 생각하자.
	// 2 : 두 번째 수평 픽셀 4개에 대한 크로마(U, V) 샘플링 수(옵션) 위 숫자와 같으면 생략 가능.
};

// 0 <= x <= 255 사이 값으로 제한하는 매크로
#define CLIP8(x) (((x) < 0) ? 0 : (((x) > 255) ? 255 : (x)))

#define CLIP16(x, bits) (((x) < 0) ? 0 : ((x) > ((1 << bits) - 1) ? ((1 << bits) - 1) : (x)))

#define RGB24_SIZE		3
#define RGB32_SIZE		4

//-----------------------------------------------------------------------------
// 
//-----------------------------------------------------------------------------
class CColorSpace
{
public:
	CColorSpace();
	~CColorSpace();

protected:

	LPBYTE			m_lpbyTemp				= NULL;

	double			m_dCPU_Frequency		= 0.0;		// CPU 클럭
	LARGE_INTEGER	m_liPC;								// 계산용 변수
	LONGLONG		m_llStart				= 0;		// 계산용 변수 (시작 시간 체크)
	LONGLONG		m_llDuration			= 10000;	// 계산용 변수 (진행 시간 체크)
	inline void		StartCounter();
	inline double	GetCounter();

public:

	static inline void	Yuv2Rgb				(__in int Y, __in int U, __in int V, __out int &R, __out int &G, __out int &B);
	static inline void	Yuv2Rgb_V2			(__in int Y, __in int U, __in int V, __out int &R, __out int &G, __out int &B);
	void		InitClip();
	void		InitDitherTable();
	//void		YUVtoRGB();

	long int		m_lTable_76309[256];
	long int		m_lTable_Cr_V[256];
	long int		m_lTable_Cb_U[256];
	long int		m_lTable_Cg_U[256];
	long int		m_lTable_Cg_V[256];

	long int		m_lLevelTable_76309[256];
	unsigned char*	m_pbyClip;

	void		convert(unsigned char *src0, unsigned char *src1, unsigned char *src2, unsigned char *dst_ori, int width, int height, int imtype);

	int			yuv2rgb(int y, int u, int v);
	
	static inline void	YUV420pToRGB32		(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst);
	static inline void	YUV420pToRGB24		(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst);
	static inline void	YUV422ToRGB32		(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in BOOL bUYVY422 = TRUE, __in UINT nYCbCrFormat = 0);
	static inline void	YUV422ToRGB24		(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in BOOL bUYVY422 = TRUE, __in UINT nYCbCrFormat = 0);
	static inline void	YUV422ToRGB24_V2	(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in UINT nYCbCrFormat = 0);

	// 회사에서 쓰던거
	BYTE		m_RtblITU[256][256];
	BYTE		m_BtblITU[256][256];

	void		MakeSpaceTableITU			();
	BOOL		ConvertYUVToRGB32			(__in const LPBYTE pbySrcData, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDstData, __in UINT nYCbCrFormat);
	BOOL		ConvertYUVToRGB24			(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in UINT nYCbCrFormat);
	
	BOOL		ConvertYUVToRGB32_YCbYCr	(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst);
	BOOL		ConvertYUVToRGB32_YCrYCb	(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst);
	BOOL		ConvertYUVToRGB32_CrYCbY	(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst);
	BOOL		ConvertYUVToRGB32_CbYCrY	(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst);

	BOOL		ConvertYUVToRGB24_YCbYCr	(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst);
	BOOL		ConvertYUVToRGB24_YCrYCb	(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst);
	BOOL		ConvertYUVToRGB24_CrYCbY	(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst);
	BOOL		ConvertYUVToRGB24_CbYCrY	(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst);

	BOOL		ConvertRGBtoYUV			(__in const LPBYTE pbyRGBData, __in UINT nWidth, __in UINT nHeight, __in enChromaSampling nChromaSampling, __out LPBYTE pbyOutYUVData, __out UINT nOutSize);
	
	static void	Grey16ToRGB24			(__in const LPWORD pbySrc, __in UINT nPixelCount, __out LPBYTE pbyDst);
	static void	Grey16ToRGB32			(__in const LPWORD pbySrc, __in UINT nPixelCount, __out LPBYTE pbyDst);
	static void Grey16ToRGB24_CV		(__in const LPWORD pbySrc, __in UINT nHeight, __in UINT nWidth, __out LPBYTE pbyDst, __in DOUBLE dAlpha = 1.0f, __in DOUBLE dBeta = 0.0f);
	static void Grey16ToRGB32_CV		(__in const LPWORD pbySrc, __in UINT nHeight, __in UINT nWidth, __out LPBYTE pbyDst, __in DOUBLE dAlpha = 1.0f, __in DOUBLE dBeta = 0.0f);

	void		ConvertBayerToGray16	(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in UINT nColorBatch, __in BYTE nBitPerPixel = 12);
	void		ConvertBayerToRGB32		(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in UINT nColorBatch, __in BYTE nBitPerPixel = 12, __in DOUBLE dAlpha = 1.0f, __in DOUBLE dBeta = 0.0f);
	void		ConvertBayerToRGB24		(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in UINT nColorBatch, __in BYTE nBitPerPixel = 12, __in DOUBLE dAlpha = 1.0f, __in DOUBLE dBeta = 0.0f);
	void		ConvertBayerToRGB24_old	(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in UINT nColorBatch, __in BYTE nBitPerPixel = 12, __in DOUBLE dAlpha = 1.0f, __in DOUBLE dBeta = 0.0f);
	

	void		ConvertToGray			(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in UINT nColorBatch, __in BYTE nBitPerPixel = 12);


	void		ConvertToRGB32			(__in UINT nWidth, __in UINT nHeight, __in const LPBYTE pbySrcData, __out LPBYTE pbyDstData, __in UINT nSrcColorFormat, __in UINT nColorBatch, __in BYTE nBitPerPixel = 12, __in DOUBLE dAlpha = 1.0f, __in DOUBLE dBeta = 0.0f);
	void		ConvertToRGB24			(__in UINT nWidth, __in UINT nHeight, __in const LPBYTE pbySrcData, __out LPBYTE pbyDstData, __in UINT nSrcColorFormat, __in UINT nColorBatch, __in BYTE nBitPerPixel = 12, __in DOUBLE dAlpha = 1.0f, __in DOUBLE dBeta = 0.0f);

	static void	Interlacing				(__in UINT nXres, __in UINT nYres, __inout LPBYTE pbySrc);

	static BOOL	FlipHorizontal			(__in LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst);
	static BOOL	FlipHorizontal			(__inout LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight);
	static BOOL	FlipVertical			(__in LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst);
	static BOOL	FlipVertical			(__inout LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight);

	static void	Shift10BitMode			(__in unsigned char* pImage, __out unsigned char* pDest, __in unsigned int nWidth, __in unsigned int nHeight);
	static void	Shift12BitMode			(__in unsigned char* pImage, __out unsigned char* pDest, __in unsigned int nWidth, __in unsigned int nHeight);
	static void	Make10BitMode			(__in unsigned char* pImage, __out unsigned short* pDest, __in unsigned int nWidth, __in unsigned int nHeight);
	static void	Make12BitMode(__in unsigned char* pImage, __out unsigned short* pDest, __in unsigned int nWidth, __in unsigned int nHeight);
	static void	Make12BitMode_RCCC(__in unsigned char* pImage, __out unsigned short* pDest, __in unsigned int nWidth, __in unsigned int nHeight);
	static void    Make12to16_CCCR_16to12(unsigned short* p12bitRaw, int nWidth, int nHeight);

	static void CropOddFrame			(__inout LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __in UINT nPixelCount = 1);
	static void CropOddFrame			(__inout LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in UINT nPixelCount = 1);
	static void CropEvenFrame			(__inout LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __in UINT nPixelCount = 1);
	static void CropEvenFrame			(__inout LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in UINT nPixelCount = 1);
	

	//BYTE m_byFrame[5120 * 2880 * 4];
};

#endif // ColorSpace_h__
