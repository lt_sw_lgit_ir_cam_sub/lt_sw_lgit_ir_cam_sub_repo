﻿//*****************************************************************************
// Filename	: 	DAQ_LG_IRCam.h
// Created	:	2018/1/10 - 16:12
// Modified	:	2018/1/10 - 16:12
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************

#ifndef DAQ_LG_IRCam_h__
#define DAQ_LG_IRCam_h__

#pragma once

#include "DAQWrapper.h"

//-----------------------------------------------------------------------------
// CDAQ_LG_IRCam
//-----------------------------------------------------------------------------
class CDAQ_LG_IRCam : public CDAQWrapper
{
public:
	CDAQ_LG_IRCam();
	virtual ~CDAQ_LG_IRCam();

	void	Reset_CaptureInfo		();

	BOOL	Send_SensorInit			(__in UINT nIdxBrd, __in UINT nRetry = 9);
	//BOOL	Check_FrameCount		(__out float& fOutFPS);

	//BOOL	Send_I2C_SelChannel		(__in UINT nChIdx);

	FLOAT	GetFPS					();
	DWORD	GetFPS_HW				();



};

#endif // DAQ_LG_IRCam_h__
