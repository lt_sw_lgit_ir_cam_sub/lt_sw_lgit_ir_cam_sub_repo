﻿///////////////////////////////////////////////////////////////////////////////////////
//	DLL import header file for USB3.0 boards
//	(c) 2011 DAQ system
///////////////////////////////////////////////////////////////////////////////////////
#define MAX_DEVICE	4						// Maximum number of board to find
											// Board level API functions
extern "C" __declspec(dllimport) int  OpenDAQDevice(void);
extern "C" __declspec(dllimport) BOOL ResetBoard(int nBoard);
extern "C" __declspec(dllimport) BOOL CloseDAQDevice(void);
extern "C" __declspec(dllimport) int  GetBoardNum(void);

extern "C" __declspec(dllimport) BOOL  LVDS_Init(int nBoard);
extern "C" __declspec(dllimport) BOOL  LVDS_Start(int nBoard);
extern "C" __declspec(dllimport) BOOL  LVDS_GetFrame(int nBoard, DWORD* nCnt, unsigned char* buf);
extern "C" __declspec(dllimport) BOOL  LVDS_Close(int nBoard);

extern "C" __declspec(dllimport) BOOL  LVDS_GetResolution(int nBoard, DWORD *xRes, DWORD *yRes);
extern "C" __declspec(dllimport) BOOL  LVDS_Stop(int nBoard);

//****************************************************************************************
//	LVDS Set Datamode
//  nMode
//		1 : 8bit, 1 : 16bit mode, 2 : 24bit, 3 : 32bit, 4 : 16bit YUV
//****************************************************************************************
extern "C" __declspec(dllimport) BOOL  LVDS_SetDataMode(int nBoard, int nMode);
extern "C" __declspec(dllimport) BOOL  LVDS_GetVersion(int nBoard, int *nFpgaVer, int *nFirmVer);
extern "C" __declspec(dllimport) BOOL  LVDS_BufferFlush(int nBoard);				//2012.09.07

//****************************************************************************************
//	LVDS Set Hsync Polarity  (2014. 03. 21)
//  bPol  FALSE:normal, TRUE: inverse
//****************************************************************************************
extern "C" __declspec(dllimport) BOOL  LVDS_HsyncPol(int nBoard, BOOL bPol);

//****************************************************************************************
//	LVDS Set Pclk Polarity  (2015. 01. 07 add)
//  bPol  FALSE:normal(rising edge), TRUE: falling edge
//****************************************************************************************
extern "C" __declspec(dllimport) BOOL  LVDS_PclkPol(int nBoard, BOOL bPol);


//****************************************************************************************
//	LVDS Set DVAL use  (2014. 03. 21)
//  nUse 	TRUE	: DVAL USE ,	FALSE	: HSYNC USE
//****************************************************************************************
extern "C" __declspec(dllimport) BOOL  LVDS_SetDeUse(int nBoard, BOOL bUse);

//****************************************************************************************
//	LVDS Video Mode (2014. 04. 02)
//  nMode
//		0 : Signal mode (Vsync, Hsync, De)
//      1 : BT656
//  others: Do not use (for future use)
//****************************************************************************************
extern "C" __declspec(dllimport) BOOL  LVDS_VideoMode(int nBoard, int nMode);

//****************************************************************************************
//	LVDS Get Hardware Frame rate (2014. 07. 30 add)
//
//****************************************************************************************
extern "C" __declspec(dllimport) BOOL  LVDS_GetFrameRate(int nBoard, DWORD *dwRate);

//****************************************************************************************
//	LVDS Video Divide (2014. 08. 26)
//****************************************************************************************
extern "C" __declspec(dllimport) BOOL  LVDS_SetDivide(	int nBoard, DWORD dwWidth, DWORD dwHeight, 
															DWORD dwX0, DWORD dwX1, DWORD dwX2, 
															DWORD dwY0, DWORD dwY1, DWORD dwY2);

extern "C" __declspec(dllimport) BOOL  LVDS_GetDivide(	int nBoard, DWORD *pdwWidth, DWORD *pdwHeight, 
															DWORD *pdwX0, DWORD *pdwX1, DWORD *pdwX2, 
															DWORD *pdwY0, DWORD *pdwY1, DWORD *pdwY2);

extern "C" __declspec(dllimport)  BOOL CLK_Init(int nBoard);
extern "C" __declspec(dllimport)  BOOL CLK_Close(int nBoard);
//****************************************************************************************
// nSelect == 0 : fixed clock
// nSelect == others : Program clock
//****************************************************************************************
extern "C" __declspec(dllimport)  BOOL  CLK_Select(int nBoard, int nSelect);
extern "C" __declspec(dllimport)  DWORD CLK_Get(int nBoard);
//****************************************************************************************
////   1039Hz to 68Mhz
//****************************************************************************************
extern "C" __declspec(dllimport)  BOOL CLK_Set(int nBoard, DWORD val);
extern "C" __declspec(dllimport)  BOOL CLK_Off(int nBoard, BOOL bOff);		// bOff TRUE : clock off, FALSE : clock on

//**************************************************************************************
//  From here it starts to define SYSTEM I2C function
//**************************************************************************************
extern "C" __declspec(dllimport) BOOL  I2C_SYS_Reset(int nBoard);
extern "C" __declspec(dllimport) BOOL  I2C_SYS_Set_Clock(int nBoard, int nClock);
extern "C" __declspec(dllimport) BOOL  I2C_SYS_Read(int nBoard, BYTE slAddr, DWORD nAddrLen, DWORD nAddr, DWORD nCnt, unsigned char* buf);
extern "C" __declspec(dllimport) BOOL  I2C_SYS_Write(int nBoard, BYTE slAddr, DWORD nAddrLen, DWORD nAddr, DWORD nCnt, unsigned char* buf);
