﻿//*****************************************************************************
// Filename	: 	ColorSpace.cpp
// Created	:	2017/3/21 - 16:10
// Modified	:	2017/3/21 - 16:10
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "ColorSpace.h"

/**
Sony DFW-X700 :
r = y + 1.4022 v got
b = y + 1.7710 u
g = y - 0.3475 u - 0.7144 v got
*/
#define YUV2RGB(y, u, v, r, g, b)\
  r = y + ( ((v << 11) + (v << 10) - (v << 8) + (v << 6) - (v << 3) ) >> 11);\
  g = y - ( ((v<<10) + (v<<9) - (v<<6) -  (v<<3) ) >> 11 ) - \
  ( ((u << 9) + (u << 7) + (u << 6) + (u << 3) + u) >> 11 );\
  b = y + ( ((u << 12) - (u << 9) + (u << 5)+(u<<4)) >> 11);\
  r = r < 0 ? 0 : r;\
  g = g < 0 ? 0 : g;\
  b = b < 0 ? 0 : b;\
  r = r > 255 ? 255 : r;\
  g = g > 255 ? 255 : g;\
  b = b > 255 ? 255 : b

enum enYcbYCr_Order
{
	Ord_Y0,
	Ord_Y1,
	Ord_Cb,
	Ord_Cr,
};


CColorSpace::CColorSpace()
{
	m_lpbyTemp = new BYTE[5120 * 2880 * 4];

	MakeSpaceTableITU();

	if (!QueryPerformanceFrequency(&m_liPC))
	{
		TRACE(_T("QueryPerformanceFrequency failed!\n"));
	}
	m_dCPU_Frequency = double(m_liPC.QuadPart) / 1000.0; // Millisecond
}


CColorSpace::~CColorSpace()
{
	if (NULL != m_lpbyTemp)
	{
		delete[] m_lpbyTemp;
	}
}

void CColorSpace::StartCounter()
{
	QueryPerformanceCounter(&m_liPC);
	m_llStart = m_liPC.QuadPart;
}

double CColorSpace::GetCounter()
{
	QueryPerformanceCounter(&m_liPC);

	return double(m_liPC.QuadPart - m_llStart) / m_dCPU_Frequency;
}

//=============================================================================
// Method		: Yuv2Rgb
// Access		: public  
// Returns		: void
// Parameter	: int Y
// Parameter	: int U
// Parameter	: int V
// Parameter	: int & R
// Parameter	: int & G
// Parameter	: int & B
// Qualifier	:
// Last Update	: 2017/3/24 - 11:11
// Desc.		:
//=============================================================================
void CColorSpace::Yuv2Rgb(int Y, int U, int V, int &R, int &G, int &B)
{
	B = (int)((1.164 * (Y - 16)) + (2.018 * (U - 128)));
	G = (int)((1.164 * (Y - 16)) - (0.813 * (V - 128)) - (0.391 * (U - 128)));
	R = (int)((1.164 * (Y - 16)) + (1.596 * (V - 128)));

	B = CLIP8(B);
	G = CLIP8(G);
	R = CLIP8(R);
}

//=============================================================================
// Method		: Yuv2Rgb_V2
// Access		: public  
// Returns		: void
// Parameter	: int Y
// Parameter	: int U
// Parameter	: int V
// Parameter	: int & R
// Parameter	: int & G
// Parameter	: int & B
// Qualifier	:
// Last Update	: 2017/3/24 - 11:11
// Desc.		:
//=============================================================================
void CColorSpace::Yuv2Rgb_V2(int Y, int U, int V, int &R, int &G, int &B)
{
	B = (int)((76284 * (Y - 16)) + (132252 * (U - 128))) >> 16;
	G = (int)((76284 * (Y - 16)) - (53281 * (V - 128)) - (25625 * (U - 128))) >> 16;
	R = (int)((76284 * (Y - 16)) + (104595 * (V - 128))) >> 16;

	B = CLIP8(B);
	G = CLIP8(G);
	R = CLIP8(R);
}

void CColorSpace::InitClip()
{
	int i;
	m_pbyClip = (unsigned char *)malloc(1024);
	m_pbyClip += 384;

	for (i = -384; i < 640; i++)
		m_pbyClip[i] = (i < 0) ? 0 : ((i>255) ? 255 : i);
}

void CColorSpace::InitDitherTable()
{
	long int crv, cbu, cgu, cgv;
	int i;

	crv = 104597; cbu = 132201;  /* fra matrise i global.h */
	cgu = 25675;  cgv = 53279;

	for (i = 0; i < 256; i++)
	{
		m_lTable_Cr_V[i] = (i - 128) * crv;
		m_lTable_Cb_U[i] = (i - 128) * cbu;
		m_lTable_Cg_U[i] = (i - 128) * cgu;
		m_lTable_Cg_V[i] = (i - 128) * cgv;
		m_lTable_76309[i] = 76309 * (i - 16);
	}

	m_lLevelTable_76309[0] = m_lTable_76309[0];
	for (i = 1; i < 256; i++)
	{
		m_lLevelTable_76309[i] = m_lTable_76309[255];
	}

}

void CColorSpace::convert(unsigned char *src0, unsigned char *src1, unsigned char *src2, unsigned char *dst_ori, int width, int height, int imtype)
{
	int i, j, k, u, v, u2g, u2b, v2g, v2r, y11, y12, y21, y22;
	int width4 = width + (4 - (width % 4)) % 4;
	unsigned char* pucY0, *pucY1, *pucU, *pucV, *pucRaster0, *pucRaster1;
	pucY0 = src0;
	pucY1 = src0 + width;
	pucU = src1;
	pucV = src2;
	pucRaster0 = dst_ori + width4 * (height - 1) * 3;
	pucRaster1 = pucRaster0 - width4 * 3;

	switch (imtype)
	{
	case 444:
		for (i = 0; i < height; i++)
		{
			for (j = 0; j < width; j++)
			{
				y11 = m_lTable_76309[*pucY0++];
				u = *pucU++;
				v = *pucV++;
				u2g = m_lTable_Cg_U[u];
				u2b = m_lTable_Cb_U[u];
				v2g = m_lTable_Cg_V[v];
				v2r = m_lTable_Cr_V[v];
				*pucRaster0++ = m_pbyClip[(y11 + u2b) >> 16];
				*pucRaster0++ = m_pbyClip[(y11 - u2g - v2g) >> 16];
				*pucRaster0++ = m_pbyClip[(y11 + v2r) >> 16];
			}

			for (; j < width4; j++)
			{
				for (k = 0; k < 3; k++)
				{
					*pucRaster0++ = 0;
				}
			}
			pucRaster0 = pucRaster0 - 2 * width4 * 3;
		}
		break;

	case 422:
		for (i = 0; i < height; i++)
		{
			for (j = 0; j < width; j += 2)
			{
				y11 = m_lTable_76309[*pucY0++];
				y12 = m_lTable_76309[*pucY0++];
				u = *pucU++;
				v = *pucV++;
				u2g = m_lTable_Cg_U[u];
				u2b = m_lTable_Cb_U[u];
				v2g = m_lTable_Cg_V[v];
				v2r = m_lTable_Cr_V[v];
				*pucRaster0++ = m_pbyClip[(y11 + u2b) >> 16];
				*pucRaster0++ = m_pbyClip[(y11 - u2g - v2g) >> 16];
				*pucRaster0++ = m_pbyClip[(y11 + v2r) >> 16];

				*pucRaster0++ = m_pbyClip[(y12 + u2b) >> 16];
				*pucRaster0++ = m_pbyClip[(y12 - u2g - v2g) >> 16];
				*pucRaster0++ = m_pbyClip[(y12 + v2r) >> 16];
			}
			for (; j < width4; j++)
			{
				for (k = 0; k < 3; k++)
				{
					*pucRaster0++ = 0;
				}
			}
			pucRaster0 = pucRaster0 - 2 * width4 * 3;
		}
		break;

	case 420:
		for (i = 0; i < height; i += 2)
		{
			for (j = 0; j < width; j += 2)
			{
				y11 = m_lTable_76309[*pucY0++];
				y12 = m_lTable_76309[*pucY0++];
				y21 = m_lTable_76309[*pucY1++];
				y22 = m_lTable_76309[*pucY1++];
				u = *pucU++;
				v = *pucV++;
				u2g = m_lTable_Cg_U[u];
				u2b = m_lTable_Cb_U[u];
				v2g = m_lTable_Cg_V[v];
				v2r = m_lTable_Cr_V[v];

				*pucRaster0++ = m_pbyClip[(y11 + u2b) >> 16];
				*pucRaster0++ = m_pbyClip[(y11 - u2g - v2g) >> 16];
				*pucRaster0++ = m_pbyClip[(y11 + v2r) >> 16];
				*pucRaster0++ = m_pbyClip[(y12 + u2b) >> 16];
				*pucRaster0++ = m_pbyClip[(y12 - u2g - v2g) >> 16];
				*pucRaster0++ = m_pbyClip[(y12 + v2r) >> 16];

				*pucRaster1++ = m_pbyClip[(y21 + u2b) >> 16];
				*pucRaster1++ = m_pbyClip[(y21 - u2g - v2g) >> 16];
				*pucRaster1++ = m_pbyClip[(y21 + v2r) >> 16];
				*pucRaster1++ = m_pbyClip[(y22 + u2b) >> 16];
				*pucRaster1++ = m_pbyClip[(y22 - u2g - v2g) >> 16];
				*pucRaster1++ = m_pbyClip[(y22 + v2r) >> 16];
			}

			for (; j < width4; j++)
			{
				for (k = 0; k < 3; k++)
				{
					*pucRaster0++ = 0;
					*pucRaster1++ = 0;
				}
			}

			pucRaster0 = pucRaster0 - 3 * width4 * 3;
			pucRaster1 = pucRaster1 - 3 * width4 * 3;
			pucY0 += width;
			pucY1 += width;
		}
		break;

	case 24: // 24-bit RGB
		for (i = 0; i < height; i++)
		{
			for (j = 0; j < width; j++)
			{
				y11 = *pucY0++;
				u = *pucU++;
				v = *pucV++;
				*pucRaster0++ = v;
				*pucRaster0++ = u;
				*pucRaster0++ = y11;
			}

			for (; j < width4; j++)
			{
				for (k = 0; k < 3; k++)
				{
					*pucRaster0++ = 0;
				}
			}
			pucRaster0 = pucRaster0 - 2 * width4 * 3;
		}
		break;

	case 256: // Gray Level
		for (i = 0; i < height; i++)
		{
			for (j = 0; j < width; j++)
			{
				y11 = m_pbyClip[m_lTable_76309[*pucY0++] >> 16];
				*pucRaster0++ = y11;
				*pucRaster0++ = y11;
				*pucRaster0++ = y11;
			}

			for (; j < width4; j++)
			{
				for (k = 0; k < 3; k++)
				{
					*pucRaster0++ = 0;
				}
			}
			pucRaster0 = pucRaster0 - 2 * width4 * 3;
		}
		break;
	}

}


int CColorSpace::yuv2rgb(int y, int u, int v)
{
	unsigned int pixel32;
	unsigned char *pixel = (unsigned char *)&pixel32;
	int r, g, b;

#if 0
/*
    One formula I found:  (not the right one)

    R = 1.164(Y - 16) + 1.596(Cr - 128)
    G = 1.164(Y - 16) - 0.813(Cr - 128) - 0.391(Cb - 128)
    B = 1.164(Y - 16)                   + 2.018(Cb - 128)
*/
	r = (1.164 * (y - 16)) + (2.018 * (v - 128));
	g = (1.164 * (y - 16)) - (0.813 * (u - 128)) - (0.391 * (v - 128));
	b = (1.164 * (y - 16)) + (1.596 * (u - 128));
#else

/*
    Another formula I found:  (seems to work)

    R = Y + 1.370705 (V-128)
    G = Y - 0.698001 (V-128) - 0.337633 (U-128)
    B = Y + 1.732446 (U-128)
*/
	r = (int)(y + (1.370705 * (v-128)));
	g = (int)(y - (0.698001 * (v-128)) - (0.337633 * (u-128)));
	b = (int)(y + (1.732446 * (u-128)));

#endif

	// Even with proper conversion, some values still need clipping.
	if (r > 255) r = 255;
	if (g > 255) g = 255;
	if (b > 255) b = 255;
	if (r < 0) r = 0;
	if (g < 0) g = 0;
	if (b < 0) b = 0;

	// Values only go from 0-220..  Why?
	pixel[0] = r * 220 / 256;
	pixel[1] = g * 220 / 256;
	pixel[2] = b * 220 / 256;
	pixel[3] = 0;

	return pixel32;
}

//=============================================================================
// Method		: YUV420pToRGB32
// Access		: public static  
// Returns		: void
// Parameter	: __in const LPBYTE pbySrc
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Parameter	: __out LPBYTE pbyDst
// Qualifier	:
// Last Update	: 2017/3/27 - 16:25
// Desc.		:
//=============================================================================
void CColorSpace::YUV420pToRGB32(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst)
{
	if ((pbyDst == pbySrc) || (NULL == pbyDst))
	{
		pbyDst = new BYTE[nWidth * nHeight * RGB32_SIZE];
	}

	float y, u = 0.0, v = 0.0;
	float r, g, b;
	UINT nOffset_U = 0;
	UINT nOffset_V = 0;
	UINT nOffset_RGB32 = 0;
	UINT iPixelCnt = nWidth * nHeight;

	for (UINT nIdx_Y = 0; nIdx_Y < nHeight; nIdx_Y++)
	{
		if (nIdx_Y % 2 == 0)
		{
			nOffset_V = iPixelCnt + (nIdx_Y / 2 * nWidth / 2);
			nOffset_U = iPixelCnt + (iPixelCnt / 4) + (nIdx_Y / 2 * nWidth / 2);
		}

		for (UINT nIdx_X = 0; nIdx_X < nWidth; nIdx_X++)
		{
			y = 1.164f * (float)(pbySrc[(nIdx_Y * nWidth) + nIdx_X] - 16);

			if (nIdx_X % 2 == 0)
			{
				v = (float)(pbySrc[nOffset_V + (nIdx_X / 2)] - 128);
				u = (float)(pbySrc[nOffset_U + (nIdx_X / 2)] - 128);
			}

			b = float(rint(y + (2.018f * v)));
			g = float(rint(y - (0.813f * u) - (0.391f * v)));
			r = float(rint(y + (1.596f * u)));

			nOffset_RGB32 = ((nIdx_Y * nWidth) + nIdx_X) * RGB32_SIZE;

			pbyDst[nOffset_RGB32]		= (BYTE)CLIP8(b);
			pbyDst[nOffset_RGB32 + 1]	= (BYTE)CLIP8(g);
			pbyDst[nOffset_RGB32 + 2]	= (BYTE)CLIP8(r);
			pbyDst[nOffset_RGB32 + 3]	= 0xFF;
		}
	}
}

void CColorSpace::YUV420pToRGB24(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst)
{
	if ((pbyDst == pbySrc) || (NULL == pbyDst))
	{
		pbyDst = new BYTE[nWidth * nHeight * RGB24_SIZE];
	}

	float y, u = 0.0, v = 0.0;
	float r, g, b;
	UINT nOffset_U = 0;
	UINT nOffset_V = 0;
	UINT nOffset_RGB24 = 0;
	UINT iPixelCnt = nWidth * nHeight;

	for (UINT nIdx_Y = 0; nIdx_Y < nHeight; nIdx_Y++)
	{
		if (nIdx_Y % 2 == 0)
		{
			nOffset_V = iPixelCnt + (nIdx_Y / 2 * nWidth / 2);
			nOffset_U = iPixelCnt + (iPixelCnt / 4) + (nIdx_Y / 2 * nWidth / 2);
		}

		for (UINT nIdx_X = 0; nIdx_X < nWidth; nIdx_X++)
		{
			y = 1.164f * (float)(pbySrc[(nIdx_Y * nWidth) + nIdx_X] - 16);

			if (nIdx_X % 2 == 0)
			{
				v = (float)(pbySrc[nOffset_V + (nIdx_X / 2)] - 128);
				u = (float)(pbySrc[nOffset_U + (nIdx_X / 2)] - 128);
			}

			b = float(rint(y + (2.018f * v)));
			g = float(rint(y - (0.813f * u) - (0.391f * v)));
			r = float(rint(y + (1.596f * u)));

			nOffset_RGB24 = ((nIdx_Y * nWidth) + nIdx_X) * RGB24_SIZE;

			pbyDst[nOffset_RGB24]		= (BYTE)CLIP8(b);
			pbyDst[nOffset_RGB24 + 1]	= (BYTE)CLIP8(g);
			pbyDst[nOffset_RGB24 + 2]	= (BYTE)CLIP8(r);
		}
	}
}

//=============================================================================
// Method		: YUV422ToRGB32
// Access		: public static  
// Returns		: void
// Parameter	: __in const LPBYTE pbySrc
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Parameter	: __out LPBYTE pbyDst
// Parameter	: __in BOOL bUYVY422
// Parameter	: __in UINT nYCbCrFormat
// Qualifier	:
// Last Update	: 2017/8/16 - 19:38
// Desc.		:
//=============================================================================
void CColorSpace::YUV422ToRGB32(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in BOOL bUYVY422 /*= TRUE*/, __in UINT nYCbCrFormat/* = 0*/)
{
	if ((pbyDst == pbySrc) || (NULL == pbyDst))
	{
		pbyDst = new BYTE[nWidth * nHeight * RGB32_SIZE];
	}

	UINT nIdx_Src, nIdx_Dst;
	register int y0, y1, u, v, r, g, b;
	UINT iPixelCnt = nWidth * nHeight;

	if (bUYVY422)
	{
		for (nIdx_Src = 0, nIdx_Dst = 0; nIdx_Src < (iPixelCnt << 1); nIdx_Src += 4, nIdx_Dst += 8)
		{
			u	= (BYTE)pbySrc[nIdx_Src + 0] - 128;
			y0	= (BYTE)pbySrc[nIdx_Src + 1];
			v	= (BYTE)pbySrc[nIdx_Src + 2] - 128;
			y1	= (BYTE)pbySrc[nIdx_Src + 3];

			YUV2RGB(y0, u, v, r, g, b);
			pbyDst[nIdx_Dst + 0] = (BYTE)(b);
			pbyDst[nIdx_Dst + 1] = (BYTE)(g);
			pbyDst[nIdx_Dst + 2] = (BYTE)(r);
			pbyDst[nIdx_Dst + 3] = 0xFF;

			YUV2RGB(y1, u, v, r, g, b);
			pbyDst[nIdx_Dst + 4] = (BYTE)(b);
			pbyDst[nIdx_Dst + 5] = (BYTE)(g);
			pbyDst[nIdx_Dst + 6] = (BYTE)(r);
			pbyDst[nIdx_Dst + 7] = 0xFF;
		}
	}
	else
	{
		for (nIdx_Src = 0, nIdx_Dst = 0; nIdx_Src < (iPixelCnt << 1); nIdx_Src += 4, nIdx_Dst += 8)
		{
			u	= (BYTE)pbySrc[nIdx_Src + 1] - 128;
			y0	= (BYTE)pbySrc[nIdx_Src + 0];
			v	= (BYTE)pbySrc[nIdx_Src + 3] - 128;
			y1	= (BYTE)pbySrc[nIdx_Src + 2];

			YUV2RGB(y0, u, v, r, g, b);
			pbyDst[nIdx_Dst + 0] = (BYTE)(b);
			pbyDst[nIdx_Dst + 1] = (BYTE)(g);
			pbyDst[nIdx_Dst + 2] = (BYTE)(r);
			pbyDst[nIdx_Dst + 3] = 0xFF;

			YUV2RGB(y1, u, v, r, g, b);
			pbyDst[nIdx_Dst + 4] = (BYTE)(b);
			pbyDst[nIdx_Dst + 5] = (BYTE)(g);
			pbyDst[nIdx_Dst + 6] = (BYTE)(r);
			pbyDst[nIdx_Dst + 7] = 0xFF;
		}
	}
}

void CColorSpace::YUV422ToRGB24(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in BOOL bUYVY422 /*= TRUE*/, __in UINT nYCbCrFormat/* = 0*/)
{
	if ((pbyDst == pbySrc) || (NULL == pbyDst))
	{
		pbyDst = new BYTE[nWidth * nHeight * RGB24_SIZE];
	}

	UINT nIdx_Src, nIdx_Dst;
	register int y0, y1, u, v, r, g, b;
	UINT iPixelCnt = nWidth * nHeight;

	if (bUYVY422)
	{
		for (nIdx_Src = 0, nIdx_Dst = 0; nIdx_Src < (iPixelCnt << 1); nIdx_Src += 4, nIdx_Dst += 6)
		{
 			u	= (int)pbySrc[nIdx_Src + 2] - 128;
 			y0	= (int)pbySrc[nIdx_Src + 1];
 			v	= (int)pbySrc[nIdx_Src + 0] - 128;
 			y1	= (int)pbySrc[nIdx_Src + 3];

			YUV2RGB(y0, u, v, r, g, b);
			pbyDst[nIdx_Dst + 0] = (BYTE)(b);
			pbyDst[nIdx_Dst + 1] = (BYTE)(g);
			pbyDst[nIdx_Dst + 2] = (BYTE)(r);

			YUV2RGB(y1, u, v, r, g, b);
			pbyDst[nIdx_Dst + 3] = (BYTE)(b);
			pbyDst[nIdx_Dst + 4] = (BYTE)(g);
			pbyDst[nIdx_Dst + 5] = (BYTE)(r);
		}
	}
	else
	{
		for (nIdx_Src = 0, nIdx_Dst = 0; nIdx_Src < (iPixelCnt << 1); nIdx_Src += 4, nIdx_Dst += 6)
		{
			u	= (BYTE)pbySrc[nIdx_Src + 1] - 128;
			y0	= (BYTE)pbySrc[nIdx_Src + 0];
			v	= (BYTE)pbySrc[nIdx_Src + 3] - 128;
			y1	= (BYTE)pbySrc[nIdx_Src + 2];

			YUV2RGB(y0, u, v, r, g, b);
			pbyDst[nIdx_Dst + 0] = (BYTE)(b);
			pbyDst[nIdx_Dst + 1] = (BYTE)(g);
			pbyDst[nIdx_Dst + 2] = (BYTE)(r);

			YUV2RGB(y1, u, v, r, g, b);
			pbyDst[nIdx_Dst + 3] = (BYTE)(b);
			pbyDst[nIdx_Dst + 4] = (BYTE)(g);
			pbyDst[nIdx_Dst + 5] = (BYTE)(r);
		}
	}
}

void CColorSpace::YUV422ToRGB24_V2(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in UINT nYCbCrFormat /*= 0*/)
{
	UINT nIdx_Src, nIdx_Dst;
	register int y0, y1, u, v, r, g, b;
	UINT iPixelCnt = (nWidth * nHeight) << 1;

	for (nIdx_Src = 0, nIdx_Dst = 0; nIdx_Src < iPixelCnt; /*nIdx_Src += 4, nIdx_Dst += 6*/)
	{
		u	= pbySrc[nIdx_Src++] - 128;
		y0	= pbySrc[nIdx_Src++];
		v	= pbySrc[nIdx_Src++] - 128;
 		y1	= pbySrc[nIdx_Src++];

// 		y0	= pbySrc[nIdx_Src++];
// 		u	= pbySrc[nIdx_Src++] - 128;
// 		y1	= pbySrc[nIdx_Src++];		
// 		v	= pbySrc[nIdx_Src++] - 128;

		YUV2RGB(y0, u, v, r, g, b);
		pbyDst[nIdx_Dst++] = (BYTE)(b);
		pbyDst[nIdx_Dst++] = (BYTE)(g);
		pbyDst[nIdx_Dst++] = (BYTE)(r);

		YUV2RGB(y1, u, v, r, g, b);
		pbyDst[nIdx_Dst++] = (BYTE)(b);
		pbyDst[nIdx_Dst++] = (BYTE)(g);
		pbyDst[nIdx_Dst++] = (BYTE)(r);
	}
}

//=============================================================================
// Method		: MakeSpaceTableITU
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/23 - 16:53
// Desc.		:
//=============================================================================
void CColorSpace::MakeSpaceTableITU()
{
	int Y, Cr, Cb;
	int iTemp;

	for (Y = 0; Y <= UCHAR_MAX; Y++)
	{			//Y
		for (Cr = 0; Cr <= UCHAR_MAX; Cr++)
		{		//Cr
			for (Cb = 0; Cb <= UCHAR_MAX; Cb++)
			{	//Cb
				iTemp = (int)(1.164 * (Y - 16) + 2.018 * (Cb - 128));
				m_BtblITU[Y][Cb] = CLIP8(iTemp);
				
				iTemp = (int)(1.164 * (Y - 16) + 1.596 * (Cr - 128));				
				m_RtblITU[Y][Cr] = CLIP8(iTemp);
			}
		}
	}
}

//=============================================================================
// Method		: ConvertYUVToRGB32
// Access		: public  
// Returns		: BOOL
// Parameter	: __in const LPBYTE pbySrc
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Parameter	: __out LPBYTE pbyDst
// Parameter	: __in UINT nYCbCrFormat
// Qualifier	:
// Last Update	: 2017/8/20 - 16:35
// Desc.		:
//=============================================================================
BOOL CColorSpace::ConvertYUVToRGB32(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in UINT nYCbCrFormat)
{
	DWORD	Y0, Y1, Cb, Cr;
	LPBYTE  pbySrcYLine = NULL;
	int		iTemp = 0;
	UINT	nOffset = 0;

	UINT nOrder[4] = { 0, 1, 2, 3 };

	switch (nYCbCrFormat)
	{
	case 0:	// CAM_FMT_YCBYCR
		nOrder[Ord_Y0] = 0;
		nOrder[Ord_Cb] = 1;
		nOrder[Ord_Y1] = 2;
		nOrder[Ord_Cr] = 3;
		break;

	case 1:	// CAM_FMT_YCRYCB
		nOrder[Ord_Y0] = 0;
		nOrder[Ord_Cr] = 1;
		nOrder[Ord_Y1] = 2;
		nOrder[Ord_Cb] = 3;
		break;

	case 2:	// CAM_FMT_CRYCBY
		nOrder[Ord_Cr] = 0;
		nOrder[Ord_Y0] = 1;
		nOrder[Ord_Cb] = 2;
		nOrder[Ord_Y1] = 3;
		break;

	case 3: // CAM_FMT_CBYCRY
		nOrder[Ord_Cb] = 0;
		nOrder[Ord_Y0] = 1;
		nOrder[Ord_Cr] = 2;
		nOrder[Ord_Y1] = 3;
		break;

	default:
		break;
	}

	for (UINT nIdx_Y = 0; nIdx_Y < nHeight; nIdx_Y++)
	{
		pbySrcYLine = &pbySrc[nOffset];

		for (UINT nIdx_X = 0; nIdx_X < nWidth; nIdx_X += 2)
		{
			Y0 = pbySrcYLine[nOrder[Ord_Y0]];
			Cb = pbySrcYLine[nOrder[Ord_Cb]];
			Y1 = pbySrcYLine[nOrder[Ord_Y1]];
			Cr = pbySrcYLine[nOrder[Ord_Cr]];

			// Blue
			*pbyDst++ = m_BtblITU[Y0][Cb];
			// Green
			iTemp = (int)(1192 * (Y0 - 16) - 832 * (Cr - 128) - 400 * (Cb - 128)) >> 10;
			//iTemp = Y0 - (((Cr << 10) + (Cr << 9) - (Cr << 6) - (Cr << 3)) >> 11) - (((Cb << 9) + (Cb << 7) + (Cb << 6) + (Cb << 3) + Cb) >> 11);
			*pbyDst++ = CLIP8(iTemp);
			// Red
			*pbyDst++ = m_RtblITU[Y0][Cr];
			// Blank
			*pbyDst++ = 0xff;

			// Blue
			*pbyDst++ = m_BtblITU[Y1][Cb];
			// Green
			iTemp = (int)(1192 * (Y1 - 16) - 832 * (Cr - 128) - 400 * (Cb - 128)) >> 10;
			//iTemp = Y1 - (((Cr << 10) + (Cr << 9) - (Cr << 6) - (Cr << 3)) >> 11) - (((Cb << 9) + (Cb << 7) + (Cb << 6) + (Cb << 3) + Cb) >> 11);
			*pbyDst++ = CLIP8(iTemp);
			// Red
			*pbyDst++ = m_RtblITU[Y1][Cr];
			// Blank
			*pbyDst++ = 0xff;

			//next  pair.
			pbySrcYLine += 4;
		}

		nOffset += nWidth * 4;
	}

	return TRUE;
}

//=============================================================================
// Method		: ConvertYUVToRGB24
// Access		: public  
// Returns		: BOOL
// Parameter	: __in const LPBYTE pbySrc
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Parameter	: __out LPBYTE pbyDst
// Parameter	: __in UINT nYCbCrFormat
// Qualifier	:
// Last Update	: 2017/8/20 - 16:35
// Desc.		:
//=============================================================================
BOOL CColorSpace::ConvertYUVToRGB24(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in UINT nYCbCrFormat)
{
	LPBYTE  pbySrcYLine = NULL;	
	register BYTE	Y0, Y1, Cb, Cr;	
	register int	iTemp = 0;
	UINT	nOffset = 0;

	register UINT nOrder[4] = {0, 1, 2, 3};

	switch (nYCbCrFormat)
	{
	case 0:	// CAM_FMT_YCBYCR
		nOrder[Ord_Y0] = 0;
		nOrder[Ord_Cb] = 1;
		nOrder[Ord_Y1] = 2;
		nOrder[Ord_Cr] = 3;
		break;

	case 1:	// CAM_FMT_YCRYCB
		nOrder[Ord_Y0] = 0;
		nOrder[Ord_Cr] = 1;
		nOrder[Ord_Y1] = 2;
		nOrder[Ord_Cb] = 3;
		break;

	case 2:	// CAM_FMT_CRYCBY
		nOrder[Ord_Cr] = 0;
		nOrder[Ord_Y0] = 1;
		nOrder[Ord_Cb] = 2;
		nOrder[Ord_Y1] = 3;
		break;

	case 3: // CAM_FMT_CBYCRY
		nOrder[Ord_Cb] = 0;
		nOrder[Ord_Y0] = 1;
		nOrder[Ord_Cr] = 2;
		nOrder[Ord_Y1] = 3;
		break;

	default:
		break;
	}

	for (UINT i = 0; i < nHeight; i += 2)
	{
		pbySrcYLine = &pbySrc[nOffset];

		for (UINT j = 0; j < nWidth; j++)
		{
			Y0 = pbySrcYLine[nOrder[Ord_Y0]];
			Cb = pbySrcYLine[nOrder[Ord_Cb]];
			Y1 = pbySrcYLine[nOrder[Ord_Y1]];
			Cr = pbySrcYLine[nOrder[Ord_Cr]];

			// Blue
			*pbyDst++ = m_BtblITU[Y0][Cb];

			// Green
			iTemp = (int)(1192 * (Y0 - 16) - 832 * (Cr - 128) - 400 * (Cb - 128)) >> 10;
			//iTemp = Y0 - (((Cr << 10) + (Cr << 9) - (Cr << 6) - (Cr << 3)) >> 11) - (((Cb << 9) + (Cb << 7) + (Cb << 6) + (Cb << 3) + Cb) >> 11);
			*pbyDst++ = CLIP8(iTemp);

			// Red
			iTemp = m_RtblITU[Y0][Cr];
			*pbyDst++ = CLIP8(iTemp);


			// Blue
			*pbyDst++ = m_BtblITU[Y1][Cb];

			// Green
			iTemp = (int)(1192 * (Y1 - 16) - 832 * (Cr - 128) - 400 * (Cb - 128)) >> 10;
			//iTemp = Y1 - (((Cr << 10) + (Cr << 9) - (Cr << 6) - (Cr << 3)) >> 11) - (((Cb << 9) + (Cb << 7) + (Cb << 6) + (Cb << 3) + Cb) >> 11);
			*pbyDst++ = CLIP8(iTemp);

			// Red
			iTemp = m_RtblITU[Y1][Cr];
			*pbyDst++ = CLIP8(iTemp);

			pbySrcYLine += 4;		//next  pair.
		}
		nOffset += nWidth * 4;
	}

	return TRUE;
}

//=============================================================================
// Method		: ConvertYUVToRGB32_YCbYCr
// Access		: public  
// Returns		: BOOL
// Parameter	: __in const LPBYTE pbySrc
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Parameter	: __out LPBYTE pbyDst
// Qualifier	:
// Last Update	: 2017/8/20 - 16:16
// Desc.		:
//=============================================================================
BOOL CColorSpace::ConvertYUVToRGB32_YCbYCr(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst)
{
	DWORD	Y0, Y1, Cb, Cr;
	LPBYTE  pbySrcYLine = NULL;
	int		iTemp = 0;
	UINT	nOffset = 0;

	for (UINT nIdx_Y = 0; nIdx_Y < nHeight; nIdx_Y++)
	{
		pbySrcYLine = &pbySrc[nOffset];

		for (UINT nIdx_X = 0; nIdx_X < nWidth; nIdx_X += 2)
		{
			// CAM_FMT_YCBYCR
			Y0 = pbySrcYLine[0];
			Cb = pbySrcYLine[1];
			Y1 = pbySrcYLine[2];
			Cr = pbySrcYLine[3];

			// Blue
			*pbyDst++ = m_BtblITU[Y0][Cb];
			// Green
			iTemp = (int)(1192 * (Y0 - 16) - 832 * (Cr - 128) - 400 * (Cb - 128)) >> 10;
			*pbyDst++ = CLIP8(iTemp);
			// Red
			*pbyDst++ = m_RtblITU[Y0][Cr];
			// Blank
			*pbyDst++ = 0xff;

			// Blue
			*pbyDst++ = m_BtblITU[Y1][Cb];
			// Green
			iTemp = (int)(1192 * (Y1 - 16) - 832 * (Cr - 128) - 400 * (Cb - 128)) >> 10;
			*pbyDst++ = CLIP8(iTemp);
			// Red
			*pbyDst++ = m_RtblITU[Y1][Cr];
			// Blank
			*pbyDst++ = 0xff;

			//next  pair.
			pbySrcYLine += 4;
		}

		nOffset += nWidth * 4;//2;
	}

	return TRUE;
}

BOOL CColorSpace::ConvertYUVToRGB32_YCrYCb(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst)
{
	DWORD	Y0, Y1, Cb, Cr;
	LPBYTE  pbySrcYLine = NULL;
	int		iTemp = 0;
	UINT	nOffset = 0;

	for (UINT nIdx_Y = 0; nIdx_Y < nHeight; nIdx_Y++)
	{
		pbySrcYLine = &pbySrc[nOffset];

		for (UINT nIdx_X = 0; nIdx_X < nWidth; nIdx_X += 2)
		{
			// CAM_FMT_YCRYCB
			Y0 = pbySrcYLine[0];
			Cr = pbySrcYLine[1];
			Y1 = pbySrcYLine[2];
			Cb = pbySrcYLine[3];

			// Blue
			*pbyDst++ = m_BtblITU[Y0][Cb];
			// Green
			iTemp = (int)(1192 * (Y0 - 16) - 832 * (Cr - 128) - 400 * (Cb - 128)) >> 10;
			*pbyDst++ = CLIP8(iTemp);
			// Red
			*pbyDst++ = m_RtblITU[Y0][Cr];
			// Blank
			*pbyDst++ = 0xff;

			// Blue
			*pbyDst++ = m_BtblITU[Y1][Cb];
			// Green
			iTemp = (int)(1192 * (Y1 - 16) - 832 * (Cr - 128) - 400 * (Cb - 128)) >> 10;
			*pbyDst++ = CLIP8(iTemp);
			// Red
			*pbyDst++ = m_RtblITU[Y1][Cr];
			// Blank
			*pbyDst++ = 0xff;

			//next  pair.
			pbySrcYLine += 4;
		}

		nOffset += nWidth * 4;//2;
	}

	return TRUE;
}

BOOL CColorSpace::ConvertYUVToRGB32_CrYCbY(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst)
{
	DWORD	Y0, Y1, Cb, Cr;
	LPBYTE  pbySrcYLine = NULL;
	int		iTemp = 0;
	UINT	nOffset = 0;

	for (UINT nIdx_Y = 0; nIdx_Y < nHeight; nIdx_Y++)
	{
		pbySrcYLine = &pbySrc[nOffset];

		for (UINT nIdx_X = 0; nIdx_X < nWidth; nIdx_X += 2)
		{
			// CAM_FMT_CRYCBY
			Cr = pbySrcYLine[0];
			Y0 = pbySrcYLine[1];
			Cb = pbySrcYLine[2];
			Y1 = pbySrcYLine[3];

			// Blue
			*pbyDst++ = m_BtblITU[Y0][Cb];
			// Green
			iTemp = (int)(1192 * (Y0 - 16) - 832 * (Cr - 128) - 400 * (Cb - 128)) >> 10;
			*pbyDst++ = CLIP8(iTemp);
			// Red
			*pbyDst++ = m_RtblITU[Y0][Cr];
			// Blank
			*pbyDst++ = 0xff;

			// Blue
			*pbyDst++ = m_BtblITU[Y1][Cb];
			// Green
			iTemp = (int)(1192 * (Y1 - 16) - 832 * (Cr - 128) - 400 * (Cb - 128)) >> 10;
			*pbyDst++ = CLIP8(iTemp);
			// Red
			*pbyDst++ = m_RtblITU[Y1][Cr];
			// Blank
			*pbyDst++ = 0xff;

			//next  pair.
			pbySrcYLine += 4;
		}

		nOffset += nWidth * 4;//2;
	}

	return TRUE;
}

BOOL CColorSpace::ConvertYUVToRGB32_CbYCrY(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst)
{
	DWORD	Y0, Y1, Cb, Cr;
	LPBYTE  pbySrcYLine = NULL;
	int		iTemp = 0;
	UINT	nOffset = 0;

	for (UINT nIdx_Y = 0; nIdx_Y < nHeight; nIdx_Y++)
	{
		pbySrcYLine = &pbySrc[nOffset];

		for (UINT nIdx_X = 0; nIdx_X < nWidth; nIdx_X += 2)
		{
			// CAM_FMT_CBYCRY
			Cb = pbySrcYLine[0];
			Y0 = pbySrcYLine[1];
			Cr = pbySrcYLine[2];
			Y1 = pbySrcYLine[3];

			// Blue
			*pbyDst++ = m_BtblITU[Y0][Cb];
			// Green
			iTemp = (int)(1192 * (Y0 - 16) - 832 * (Cr - 128) - 400 * (Cb - 128)) >> 10;
			*pbyDst++ = CLIP8(iTemp);
			// Red
			*pbyDst++ = m_RtblITU[Y0][Cr];
			// Blank
			*pbyDst++ = 0xff;

			// Blue
			*pbyDst++ = m_BtblITU[Y1][Cb];
			// Green
			iTemp = (int)(1192 * (Y1 - 16) - 832 * (Cr - 128) - 400 * (Cb - 128)) >> 10;
			*pbyDst++ = CLIP8(iTemp);
			// Red
			*pbyDst++ = m_RtblITU[Y1][Cr];
			// Blank
			*pbyDst++ = 0xff;

			//next  pair.
			pbySrcYLine += 4;
		}

		nOffset += nWidth * 4;//2;
	}

	return TRUE;
}

//=============================================================================
// Method		: ConvertYUVToRGB24_YCbYCr
// Access		: public  
// Returns		: BOOL
// Parameter	: __in const LPBYTE pbySrc
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Parameter	: __out LPBYTE pbyDst
// Qualifier	:
// Last Update	: 2017/8/20 - 15:56
// Desc.		:
//=============================================================================
BOOL CColorSpace::ConvertYUVToRGB24_YCbYCr(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst)
{
	DWORD	Y0, Y1, Cb, Cr;
	LPBYTE  pbySrcYLine = NULL;
	int		iTemp = 0;
	UINT	nOffset = 0;

	for (UINT i = 0; i < nHeight; i += 2)
	{
		pbySrcYLine = &pbySrc[nOffset];

		for (UINT j = 0; j < nWidth; j++)
		{
			// CAM_FMT_YCBYCR
			Y0 = pbySrcYLine[0];
			Cb = pbySrcYLine[1];
			Y1 = pbySrcYLine[2];
			Cr = pbySrcYLine[3];

			// Blue
			*pbyDst++ = m_BtblITU[Y0][Cb];

			// Green
			iTemp = (int)(1192 * (Y0 - 16) - 832 * (Cr - 128) - 400 * (Cb - 128)) >> 10;
			*pbyDst++ = CLIP8(iTemp);

			// Red
			iTemp = m_RtblITU[Y0][Cr];
			*pbyDst++ = CLIP8(iTemp);


			// Blue
			*pbyDst++ = m_BtblITU[Y1][Cb];

			// Green
			iTemp = (int)(1192 * (Y1 - 16) - 832 * (Cr - 128) - 400 * (Cb - 128)) >> 10;
			*pbyDst++ = CLIP8(iTemp);

			// Red
			iTemp = m_RtblITU[Y1][Cr];
			*pbyDst++ = CLIP8(iTemp);

			pbySrcYLine += 4;		//next  pair.
		}
		nOffset += nWidth * 4;//2;
	}

	return TRUE;
}

BOOL CColorSpace::ConvertYUVToRGB24_YCrYCb(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst)
{
	DWORD	Y0, Y1, Cb, Cr;
	LPBYTE  pbySrcYLine = NULL;
	int		iTemp = 0;
	UINT	nOffset = 0;

	for (UINT i = 0; i < nHeight; i += 2)
	{
		pbySrcYLine = &pbySrc[nOffset];

		for (UINT j = 0; j < nWidth; j++)
		{
			// CAM_FMT_YCRYCB
			Y0 = pbySrcYLine[0];
			Cr = pbySrcYLine[1];
			Y1 = pbySrcYLine[2];
			Cb = pbySrcYLine[3];

			// Blue
			*pbyDst++ = m_BtblITU[Y0][Cb];

			// Green
			iTemp = (int)(1192 * (Y0 - 16) - 832 * (Cr - 128) - 400 * (Cb - 128)) >> 10;
			*pbyDst++ = CLIP8(iTemp);

			// Red
			iTemp = m_RtblITU[Y0][Cr];
			*pbyDst++ = CLIP8(iTemp);


			// Blue
			*pbyDst++ = m_BtblITU[Y1][Cb];

			// Green
			iTemp = (int)(1192 * (Y1 - 16) - 832 * (Cr - 128) - 400 * (Cb - 128)) >> 10;
			*pbyDst++ = CLIP8(iTemp);

			// Red
			iTemp = m_RtblITU[Y1][Cr];
			*pbyDst++ = CLIP8(iTemp);

			pbySrcYLine += 4;		//next  pair.
		}
		nOffset += nWidth * 4;//2;
	}

	return TRUE;
}

BOOL CColorSpace::ConvertYUVToRGB24_CrYCbY(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst)
{
	DWORD	Y0, Y1, Cb, Cr;
	LPBYTE  pbySrcYLine = NULL;
	int		iTemp = 0;
	UINT	nOffset = 0;

	for (UINT i = 0; i < nHeight; i += 2)
	{
		pbySrcYLine = &pbySrc[nOffset];

		for (UINT j = 0; j < nWidth; j++)
		{
			// CAM_FMT_CRYCBY
			Cr = pbySrcYLine[0];
			Y0 = pbySrcYLine[1];
			Cb = pbySrcYLine[2];
			Y1 = pbySrcYLine[3];

			// Blue
			*pbyDst++ = m_BtblITU[Y0][Cb];

			// Green
			iTemp = (int)(1192 * (Y0 - 16) - 832 * (Cr - 128) - 400 * (Cb - 128)) >> 10;
			*pbyDst++ = CLIP8(iTemp);

			// Red
			iTemp = m_RtblITU[Y0][Cr];
			*pbyDst++ = CLIP8(iTemp);


			// Blue
			*pbyDst++ = m_BtblITU[Y1][Cb];

			// Green
			iTemp = (int)(1192 * (Y1 - 16) - 832 * (Cr - 128) - 400 * (Cb - 128)) >> 10;
			*pbyDst++ = CLIP8(iTemp);

			// Red
			iTemp = m_RtblITU[Y1][Cr];
			*pbyDst++ = CLIP8(iTemp);

			pbySrcYLine += 4;		//next  pair.
		}
		nOffset += nWidth * 4;//2;
	}

	return TRUE;
}

BOOL CColorSpace::ConvertYUVToRGB24_CbYCrY(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst)
{
	DWORD	Y0, Y1, Cb, Cr;
	LPBYTE  pbySrcYLine = NULL;
	int		iTemp = 0;
	UINT	nOffset = 0;

	for (UINT i = 0; i < nHeight; i += 2)
	{
		pbySrcYLine = &pbySrc[nOffset];

		for (UINT j = 0; j < nWidth; j++)
		{
			// CAM_FMT_CBYCRY
			Cb = pbySrcYLine[0];
			Y0 = pbySrcYLine[1];
			Cr = pbySrcYLine[2];
			Y1 = pbySrcYLine[3];
			
			// Blue
			*pbyDst++ = m_BtblITU[Y0][Cb];

			// Green
			iTemp = (int)(1192 * (Y0 - 16) - 832 * (Cr - 128) - 400 * (Cb - 128)) >> 10;
			*pbyDst++ = CLIP8(iTemp);

			// Red
			iTemp = m_RtblITU[Y0][Cr];
			*pbyDst++ = CLIP8(iTemp);


			// Blue
			*pbyDst++ = m_BtblITU[Y1][Cb];

			// Green
			iTemp = (int)(1192 * (Y1 - 16) - 832 * (Cr - 128) - 400 * (Cb - 128)) >> 10;
			*pbyDst++ = CLIP8(iTemp);

			// Red
			iTemp = m_RtblITU[Y1][Cr];
			*pbyDst++ = CLIP8(iTemp);

			pbySrcYLine += 4;		//next  pair.
		}
		nOffset += nWidth * 4;//2;
	}

	return TRUE;
}

BOOL CColorSpace::ConvertRGBtoYUV(__in LPBYTE pbyRGBData, __in UINT nWidth, __in UINT nHeight, __in enChromaSampling nChromaSampling, __out LPBYTE pbyOutYUVData, __out UINT nOutSize)
{

	return TRUE;
}

//=============================================================================
// Method		: Grey16ToRGB24
// Access		: public static  
// Returns		: void
// Parameter	: __in const LPWORD pbySrc
// Parameter	: __in UINT nPixelCount
// Parameter	: __out LPBYTE pbyDst
// Qualifier	:
// Last Update	: 2017/6/13 - 19:52
// Desc.		:	Gray16bit 표현 범위 (0 ~ 4095)
//					Gray8bit 표현 범위 (0 ~ 255)
//=============================================================================
void CColorSpace::Grey16ToRGB24(__in const LPWORD pbySrc, __in UINT nPixelCount, __out LPBYTE pbyDst)
{
	register LPWORD src_start = pbySrc;
	register const LPWORD src_stop = src_start + nPixelCount;
	register LPBYTE pDest = pbyDst;

	unsigned short value16 = 0;
	for (; src_start < src_stop; src_start++)
	{
		value16 = *src_start / 16; // 16 == (4096 / 256)

		*(pDest++) = (BYTE)(value16);
		*(pDest++) = (BYTE)(value16);
		*(pDest++) = (BYTE)(value16);
	}
}

void CColorSpace::Grey16ToRGB32(__in const LPWORD pbySrc, __in UINT nPixelCount, __out LPBYTE pbyDst)
{
	register LPWORD src_start = pbySrc;
	register const LPWORD src_stop = src_start + nPixelCount;
	register LPBYTE pDest = pbyDst;

	unsigned short value16 = 0;
	for (; src_start < src_stop; src_start++)
	{
		value16 = *src_start / 16;

		*(pDest++) = (BYTE)(value16);
		*(pDest++) = (BYTE)(value16);
		*(pDest++) = (BYTE)(value16);
		*(pDest++) = 0x00;
	}
}

//=============================================================================
// Method		: Grey16ToRGB24_CV
// Access		: public static  
// Returns		: void
// Parameter	: __in const LPWORD pbySrc
// Parameter	: __in UINT nHeight
// Parameter	: __in UINT nWidth
// Parameter	: __out LPBYTE pbyDst
// Parameter	: __in DOUBLE dAlpha
// Parameter	: __in DOUBLE dBeta
// Qualifier	:
// Last Update	: 2017/9/7 - 20:49
// Desc.		:
//=============================================================================
void CColorSpace::Grey16ToRGB24_CV(__in const LPWORD pbySrc, __in UINT nHeight, __in UINT nWidth, __out LPBYTE pbyDst, __in DOUBLE dAlpha /*= 1.0f*/, __in DOUBLE dBeta /*= 0.0f*/)
{
	cv::Mat Bit16Mat(nHeight, nWidth, CV_16UC1, pbySrc);

	// 	DOUBLE dShiftLv = pow(2.0, dShiftExp);
	// 	DOUBLE dAlpha = (0 != dShiftLv) ? (1.0f / dShiftLv) : 1.0f;
	Bit16Mat.convertTo(Bit16Mat, CV_8UC1, dAlpha, dBeta);

	cv::Mat rgb8BitMat(nHeight, nWidth, CV_8UC3, pbyDst);
	cv::cvtColor(Bit16Mat, rgb8BitMat, CV_GRAY2BGR);
}

void CColorSpace::Grey16ToRGB32_CV(__in const LPWORD pbySrc, __in UINT nHeight, __in UINT nWidth, __out LPBYTE pbyDst, __in DOUBLE dAlpha /*= 1.0f*/, __in DOUBLE dBeta /*= 0.0f*/)
{
 	cv::Mat Bit16Mat(nHeight, nWidth, CV_16UC1, pbySrc);

	// 	DOUBLE dShiftLv = pow(2.0, dShiftExp);
	// 	DOUBLE dAlpha = (0 != dShiftLv) ? (1.0f / dShiftLv) : 1.0f;
 	Bit16Mat.convertTo(Bit16Mat, CV_8UC1, dAlpha, dBeta);
 
 	cv::Mat rgb8BitMat(nHeight, nWidth, CV_8UC4, pbyDst);
 	cv::cvtColor(Bit16Mat, rgb8BitMat, CV_GRAY2BGR);
}

//=============================================================================
// Method		: ConvertBayerToGray16
// Access		: public  
// Returns		: void
// Parameter	: __in const LPBYTE pbySrc
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Parameter	: __out LPBYTE pbyDst
// Parameter	: __in UINT nColorBatch
// Parameter	: __in BYTE nBitPerPixel
// Qualifier	:
// Last Update	: 2017/9/5 - 10:53
// Desc.		:
//=============================================================================
void CColorSpace::ConvertBayerToGray16(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in UINT nColorBatch, __in BYTE nBitPerPixel /*= 12*/)
{
	LPBYTE lpbySource = pbySrc;
	DWORD dwBitWise = 0x0FFF;

	if (12 == nBitPerPixel)
	{
// 		Make12BitMode(pbySrc, (LPWORD)m_lpbyTemp, nWidth, nHeight);
// 		lpbySource = m_lpbyTemp;
		dwBitWise = 0x0FFF;
	}
	else if (10 == nBitPerPixel)
	{
// 		Make10BitMode(pbySrc, (LPWORD)m_lpbyTemp, nWidth, nHeight);
// 		lpbySource = m_lpbyTemp;
		dwBitWise = 0x03FF;
	}

	cv::Mat matBayerSrc(nHeight, nWidth, CV_16U, (LPWORD)lpbySource);
	cv::bitwise_and(matBayerSrc, dwBitWise, matBayerSrc);
	//matBayerSrc.convertTo(matBayerSrc, CV_16UC1);
	cv::Mat matGray16Dst(nHeight, nWidth, CV_16UC1, (LPWORD)pbyDst);

	switch (nColorBatch)
	{
	case 0: //Conv_YCbYCr_BGGR:
		cv::cvtColor(matBayerSrc, matGray16Dst, CV_BayerBG2GRAY);
		break;

	case 1: //Conv_YCrYCb_RGGB:
		cv::cvtColor(matBayerSrc, matGray16Dst, CV_BayerRG2GRAY);
		break;

	case 2: //Conv_CrYCbY_GBRG:
		cv::cvtColor(matBayerSrc, matGray16Dst, CV_BayerGB2GRAY);
		break;

	case 3: //Conv_CbYCrY_GRBG:
		cv::cvtColor(matBayerSrc, matGray16Dst, CV_BayerGR2GRAY);
		break;

	default:
		break;
	}

}

//=============================================================================
// Method		: ConvertBayerToRGB32
// Access		: public  
// Returns		: void
// Parameter	: __in const LPBYTE pbySrc
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Parameter	: __out LPBYTE pbyDst
// Parameter	: __in UINT nColorBatch
// Parameter	: __in BYTE nBitPerPixel
// Parameter	: __in DOUBLE dAlpha
// Parameter	: __in DOUBLE dBeta
// Qualifier	:
// Last Update	: 2017/9/7 - 20:51
// Desc.		:
//=============================================================================
void CColorSpace::ConvertBayerToRGB32(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in UINT nColorBatch, __in BYTE nBitPerPixel/* = 12*/, __in DOUBLE dAlpha /*= 1.0f*/, __in DOUBLE dBeta /*= 0.0f*/)
{
	LPBYTE lpbySource = pbySrc;
	DWORD dwBitWise = 0x0FFF;

	if (12 == nBitPerPixel)
	{
		Make12BitMode(pbySrc, (LPWORD)m_lpbyTemp, nWidth, nHeight);
		lpbySource = m_lpbyTemp;
		dwBitWise = 0x0FFF;
	}
	else if (10 == nBitPerPixel)
	{
		Make10BitMode(pbySrc, (LPWORD)m_lpbyTemp, nWidth, nHeight);
		lpbySource = m_lpbyTemp;
		dwBitWise = 0x03FF;
	}

	cv::Mat matBayerSrc(nHeight, nWidth, CV_16UC1, (LPWORD)lpbySource);
	cv::bitwise_and(matBayerSrc, dwBitWise, matBayerSrc);
	matBayerSrc.convertTo(matBayerSrc, CV_8UC1, dAlpha, dBeta);
// 	DOUBLE dShiftLv = pow(2.0, dShiftExp);
// 	DOUBLE dAlpha = (0 != dShiftLv) ? (1.0f / dShiftLv) : 1.0f;
	matBayerSrc.convertTo(matBayerSrc, CV_8UC1, dAlpha);

	cv::Mat matRGBDst(nHeight, nWidth, CV_8UC4, pbyDst);

	switch (nColorBatch)
	{
	case 0: //Conv_YCbYCr_BGGR:
		cv::cvtColor(matBayerSrc, matRGBDst, CV_BayerBG2BGR);
		break;

	case 1: //Conv_YCrYCb_RGGB:
		cv::cvtColor(matBayerSrc, matRGBDst, CV_BayerRG2BGR);
		break;

	case 2: //Conv_CrYCbY_GBRG:
		cv::cvtColor(matBayerSrc, matRGBDst, CV_BayerGB2BGR);
		break;

	case 3: //Conv_CbYCrY_GRBG:
		cv::cvtColor(matBayerSrc, matRGBDst, CV_BayerGR2BGR);
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: ConvertBayerToRGB24
// Access		: public  
// Returns		: void
// Parameter	: __in const LPBYTE pbySrc
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Parameter	: __out LPBYTE pbyDst
// Parameter	: __in UINT nColorBatch
// Parameter	: __in BYTE nBitPerPixel
// Parameter	: __in DOUBLE dAlpha
// Parameter	: __in DOUBLE dBeta
// Qualifier	:
// Last Update	: 2017/9/7 - 20:52
// Desc.		:
//=============================================================================
void CColorSpace::ConvertBayerToRGB24(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in UINT nColorBatch, __in BYTE nBitPerPixel/* = 12*/, __in DOUBLE dAlpha /*= 1.0f*/, __in DOUBLE dBeta /*= 0.0f*/)
{
	LPBYTE lpbySource = pbySrc;
	DWORD dwBitWise = 0x0FFF;

	if (12 == nBitPerPixel)
 	{
		Make12BitMode(pbySrc, (LPWORD)m_lpbyTemp, nWidth, nHeight);
		lpbySource = m_lpbyTemp;
		dwBitWise = 0x0FFF;
	}
	else if (10 == nBitPerPixel)
	{
		Make10BitMode(pbySrc, (LPWORD)m_lpbyTemp, nWidth, nHeight);
		lpbySource = m_lpbyTemp;
		dwBitWise = 0x03FF;
	}

 	cv::Mat matBayerSrc(nHeight, nWidth, CV_16UC1, (LPWORD)lpbySource);
	cv::bitwise_and(matBayerSrc, dwBitWise, matBayerSrc);
	matBayerSrc.convertTo(matBayerSrc, CV_8UC1, dAlpha, dBeta);
// 	DOUBLE dShiftLv = pow(2.0, dShiftExp);
// 	DOUBLE dAlpha = (0 != dShiftLv) ? (1.0f / dShiftLv) : 1.0f;
	matBayerSrc.convertTo(matBayerSrc, CV_8UC1, dAlpha);

	cv::Mat matRGBDst(nHeight, nWidth, CV_8UC3, pbyDst);
	
	switch (nColorBatch)
	{
	case 0: //Conv_YCbYCr_BGGR:
		cv::cvtColor(matBayerSrc, matRGBDst, CV_BayerBG2BGR);
		break;

	case 1: //Conv_YCrYCb_RGGB:
		cv::cvtColor(matBayerSrc, matRGBDst, CV_BayerRG2BGR);
		break;

	case 2: //Conv_CrYCbY_GBRG:
		cv::cvtColor(matBayerSrc, matRGBDst, CV_BayerGB2BGR);
		break;

	case 3: //Conv_CbYCrY_GRBG:
		cv::cvtColor(matBayerSrc, matRGBDst, CV_BayerGR2BGR);
		break;

	default:
		break;
	}
}

void CColorSpace::ConvertBayerToRGB24_old(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in UINT nColorBatch, __in BYTE nBitPerPixel /*= 12*/, __in DOUBLE dAlpha /*= 1.0f*/, __in DOUBLE dBeta /*= 0.0f*/)
{
	DWORD dwBitWise = 0x0FFF;

	if (12 == nBitPerPixel)
	{
		dwBitWise = 0x0FFF;
	}
	else if (10 == nBitPerPixel)
	{
		dwBitWise = 0x03FF;
	}

	cv::Mat matBayerSrc(nHeight, nWidth, CV_16U, pbySrc);
	cv::bitwise_and(matBayerSrc, dwBitWise, matBayerSrc);
	//matBayerSrc.convertTo(matBayerSrc, CV_8UC1, 1.0 / 16);
	matBayerSrc.convertTo(matBayerSrc, CV_8UC1, dAlpha, dBeta);
	cv::Mat matRGBDst(nHeight, nWidth, CV_8UC3, pbyDst);

	switch (nColorBatch)
	{
	case 0: //Conv_YCbYCr_BGGR:
		cv::cvtColor(matBayerSrc, matRGBDst, CV_BayerBG2BGR);
		break;

	case 1: //Conv_YCrYCb_RGGB:
		cv::cvtColor(matBayerSrc, matRGBDst, CV_BayerRG2BGR);
		break;

	case 2: //Conv_CrYCbY_GBRG:
		cv::cvtColor(matBayerSrc, matRGBDst, CV_BayerGB2BGR);
		break;

	case 3: //Conv_CbYCrY_GRBG:
		cv::cvtColor(matBayerSrc, matRGBDst, CV_BayerGR2BGR);
		break;

	default:
		break;
	}
}

void CColorSpace::ConvertToGray(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in UINT nColorBatch, __in BYTE nBitPerPixel /*= 12*/)
{
	int nBytes = (nBitPerPixel + sizeof(char) - 1) / sizeof(char);	// 몇 Byte 데이터인가 구분

	if (12 == nBitPerPixel)
	{
		Make12BitMode(pbySrc, (LPWORD)pbyDst, nWidth, nHeight);
	}
	else if (10 == nBitPerPixel)
	{
		Make10BitMode(pbySrc, (LPWORD)pbyDst, nWidth, nHeight);
	}
	else
	{
		memcpy(pbyDst, pbySrc, nWidth * nHeight * nBytes);
	}
}

//=============================================================================
// Method		: ConvertToRGB32
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Parameter	: __in const LPBYTE pbySrcData
// Parameter	: __out LPBYTE pbyDstData
// Parameter	: __in UINT nSrcColorFormat
// Parameter	: __in UINT nColorBatch
// Parameter	: __in BYTE nBitPerPixel
// Parameter	: __in DOUBLE dAlpha
// Parameter	: __in DOUBLE dBeta
// Qualifier	:
// Last Update	: 2017/9/7 - 20:52
// Desc.		:
//=============================================================================
void CColorSpace::ConvertToRGB32(__in UINT nWidth, __in UINT nHeight, __in const LPBYTE pbySrcData, __out LPBYTE pbyDstData, __in UINT nSrcColorFormat, __in UINT nColorBatch, __in BYTE nBitPerPixel /*= 12*/, __in DOUBLE dAlpha /*= 1.0f*/, __in DOUBLE dBeta /*= 0.0f*/)
{
	switch (nSrcColorFormat)
	{
	case 0: //enImgSensorType_YCBCR,
		//ConvertYUVToRGB32(pbySrcData, nWidth, nHeight, pbyDstData, nColorBatch);
		switch (nColorBatch)
		{
		case 0:	// CAM_FMT_YCBYCR
			ConvertYUVToRGB32_YCbYCr(pbySrcData, nWidth, nHeight, pbyDstData);
			break;

		case 1: // CAM_FMT_YCRYCB
			ConvertYUVToRGB32_YCrYCb(pbySrcData, nWidth, nHeight, pbyDstData);
			break;

		case 2: // CAM_FMT_CRYCBY
			ConvertYUVToRGB32_CrYCbY(pbySrcData, nWidth, nHeight, pbyDstData);
			break;

		case 3: // CAM_FMT_CBYCRY
			ConvertYUVToRGB32_CbYCrY(pbySrcData, nWidth, nHeight, pbyDstData);
			break;

		default:
			break;
		}
		break;

	case 1: //enImgSensorType_RGBbayer,
		ConvertBayerToRGB32(pbySrcData, nWidth, nHeight, pbyDstData, nColorBatch, 16, dAlpha, dBeta);
		break;

	case 2: //enImgSensorType_RGBbayer10,
		ConvertBayerToRGB32(pbySrcData, nWidth, nHeight, pbyDstData, nColorBatch, 10, dAlpha, dBeta);
		break;

	case 3: //enImgSensorType_RGBbayer12,
		ConvertBayerToRGB32(pbySrcData, nWidth, nHeight, pbyDstData, nColorBatch, 12, dAlpha, dBeta);
		break;

	case 4: //enImgSensorType_RGB888,
		//memcpy(pbyDstData, pbySrcData, nWidth * nHeight * RGB32_SIZE);
		break;

	case 5:	//enImgSensorType_Monochrome_12bit
		Grey16ToRGB32((LPWORD)pbySrcData, nWidth * nHeight, pbyDstData);
		//Grey16ToRGB32_CV((LPWORD)pbySrcData, nWidth, nHeight, pbyDstData, dShiftExp);
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: ConvertToRGB24
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Parameter	: __in const LPBYTE pbySrcData
// Parameter	: __out LPBYTE pbyDstData
// Parameter	: __in UINT nSrcColorFormat
// Parameter	: __in UINT nColorBatch
// Parameter	: __in BYTE nBitPerPixel
// Parameter	: __in DOUBLE dAlpha
// Parameter	: __in DOUBLE dBeta
// Qualifier	:
// Last Update	: 2017/9/7 - 20:52
// Desc.		:
//=============================================================================
void CColorSpace::ConvertToRGB24(__in UINT nWidth, __in UINT nHeight, __in const LPBYTE pbySrcData, __out LPBYTE pbyDstData, __in UINT nSrcColorFormat, __in UINT nColorBatch, __in BYTE nBitPerPixel /*= 12*/, __in DOUBLE dAlpha /*= 1.0f*/, __in DOUBLE dBeta /*= 0.0f*/)
{
#ifdef _DEBUG
	StartCounter();
#endif
	// 1. 소스영상 포맷에 따라 변환 함수 분류
	// 2. YCbCr, Bayer10, Bayer12, RGB888
	// 3. 영상 하위 옵션에 따라 분류, YCbYCr, BGGR
	switch (nSrcColorFormat)
	{
	case 0: //enImgSensorType_YCBCR,
		//ConvertYUVToRGB24(pbySrcData, nWidth, nHeight, pbyDstData, nColorBatch);
		//YUV422ToRGB24_V2(pbySrcData, nWidth, nHeight, pbyDstData, nColorBatch);

		switch (nColorBatch)
		{
		case 0:	// CAM_FMT_YCBYCR
			ConvertYUVToRGB24_YCbYCr(pbySrcData, nWidth, nHeight, pbyDstData);
			break;

		case 1: // CAM_FMT_YCRYCB
			ConvertYUVToRGB24_YCrYCb(pbySrcData, nWidth, nHeight, pbyDstData);
			break;

		case 2: // CAM_FMT_CRYCBY
			ConvertYUVToRGB24_CrYCbY(pbySrcData, nWidth, nHeight, pbyDstData);
			break;

		case 3: // CAM_FMT_CBYCRY
			ConvertYUVToRGB24_CbYCrY(pbySrcData, nWidth, nHeight, pbyDstData);
			break;

		default:
			break;
		}
		break;

	case 1: //enImgSensorType_RGBbayer
		//ConvertBayerToRGB24(pbySrcData, nWidth, nHeight, pbyDstData, nColorBatch, 16, dAlpha, dBeta);
		ConvertBayerToRGB24_old(pbySrcData, nWidth, nHeight, pbyDstData, nColorBatch, 12, dAlpha, dBeta);
		break;

	case 2: //enImgSensorType_RGBbayer10,
		ConvertBayerToRGB24(pbySrcData, nWidth, nHeight, pbyDstData, nColorBatch, 10, dAlpha, dBeta);
		break;

	case 3: //enImgSensorType_RGBbayer12,
		ConvertBayerToRGB24(pbySrcData, nWidth, nHeight, pbyDstData, nColorBatch, 12, dAlpha, dBeta);		
		break;

	case 4: //enImgSensorType_RGB888,
		memcpy(pbyDstData, pbySrcData, nWidth * nHeight * RGB24_SIZE);
		break;

	case 5:	//enImgSensorType_Monochrome_12bit
		//Grey16ToRGB24((LPWORD)pbySrcData, nWidth * nHeight, pbyDstData);
		Grey16ToRGB24_CV((LPWORD)pbySrcData, nWidth, nHeight, pbyDstData, dAlpha, dBeta);
		break;

	default:
		break;
	}

#ifdef _DEBUG
	double dDur = GetCounter();
//	TRACE(_T("ConvTime = %.4f ms\n"), dDur);
#endif
}

//=============================================================================
// Method		: Interlacing
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nXres
// Parameter	: __in UINT nYres
// Parameter	: __inout LPBYTE pbySrc
// Qualifier	:
// Last Update	: 2017/3/24 - 13:41
// Desc.		:
//=============================================================================
void CColorSpace::Interlacing(__in UINT nXres, __in UINT nYres, __inout LPBYTE pbySrc)
{
	DWORD dwLineSize	= nXres * RGB32_SIZE;
	PBYTE pOffset		= &pbySrc[0];
	PBYTE pTarget		= NULL;

	for (DWORD dwIdx = 0; dwIdx < nYres; dwIdx += 2)
	{
		pTarget = pOffset + dwLineSize;
		memcpy(pTarget, pOffset, dwLineSize);

		pOffset = pTarget + dwLineSize;
	}
}

BOOL CColorSpace::FlipHorizontal(__in LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst)
{
	// CM_Grey			: 1
	// CM_YUYV422		: packs 3 channel YUV into 2 Bytes
	// CM_RGB			: 3
	// CM_BGRA			: 4
	// CM_Bayer_GBRG	: 1 packs 3 channel RGB into 1 Byte Bayer pattern

	UINT nChannelCount = 3;
	UINT nDepth = 1; // sizeof(char)
	UINT nSize	= nWidth * nHeight * nChannelCount * nDepth; // in byte
	if (nSize <= 0)
		return FALSE;

	UINT nPixelSize = nChannelCount * nDepth;
	UINT nWidthStep = nWidth * nDepth * nChannelCount;

	register char*	pTarget = nullptr;
	register char*	pSource = nullptr;

	for (register UINT nIdx_Y = 0; nIdx_Y < nHeight; nIdx_Y++)
	{
		pTarget = (char*)pbyDst + (nIdx_Y + 1) * nWidthStep - nPixelSize;
		pSource = (char*)pbySrc + (nIdx_Y * nWidthStep);

		for (register UINT nIdx_X = 0; nIdx_X < nWidth; nIdx_X++)
		{
			memcpy(pTarget - (nIdx_X * nPixelSize), pSource + (nIdx_X * nPixelSize), nPixelSize);
		}
	}

	return TRUE;
}

BOOL CColorSpace::FlipHorizontal(__inout LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight)
{
	UINT nChannelCount = 3;
	UINT nDepth = 1;
	UINT nSize	= nWidth * nHeight * nChannelCount * nDepth; // in byte

	register LPBYTE pbyTemp = new BYTE[nSize];
	memcpy(pbyTemp, pbySrc, nSize);

	BOOL bReturn = TRUE;
	if (FALSE == FlipHorizontal(pbyTemp, nWidth, nHeight, pbySrc))
	{
		bReturn = FALSE;
	}

	delete[] pbyTemp;

	return bReturn;
}

BOOL CColorSpace::FlipVertical(__in LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst)
{
	UINT nChannelCount = 3;
	UINT nDepth = 1;
	UINT nSize	= nWidth * nHeight * nChannelCount * nDepth; // in byte
	if (nSize <= 0)
		return FALSE;

	register UINT nWidthStep = nWidth * nChannelCount * nDepth;
	register char*	pTarget = nullptr;
	register char*	pSource = nullptr;

	pTarget = (char*)pbyDst + (nSize) - nWidthStep;

	for (register unsigned int nIdx_Y = 0; nIdx_Y < nHeight; nIdx_Y++)
	{
		pSource = (char*)pbySrc + (nIdx_Y * nWidthStep);

		memcpy(pTarget, (char*)(pSource), nWidthStep);
		pTarget -= nWidthStep;
	}

	return TRUE;
}

BOOL CColorSpace::FlipVertical(__inout LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight)
{
	UINT nChannelCount = 3;
	UINT nDepth = 1;
	UINT nSize	= nWidth * nHeight * nChannelCount * nDepth; // in byte

	LPBYTE pbyTemp = new BYTE[nSize];
	memcpy(pbyTemp, pbySrc, nSize);

	BOOL bReturn = TRUE;
	if (FALSE == FlipVertical(pbyTemp, nWidth, nHeight, pbySrc))
	{
		bReturn = FALSE;
	}

	delete[] pbyTemp;

	return bReturn;
}

//=============================================================================
// Method		: Shift10BitMode
// Access		: public static  
// Returns		: void
// Parameter	: __in unsigned char * pImage
// Parameter	: __out unsigned char * pDest
// Parameter	: __in unsigned int nWidth
// Parameter	: __in unsigned int nHeight
// Qualifier	:
// Last Update	: 2017/7/18 - 18:43
// Desc.		:
//=============================================================================
void CColorSpace::Shift10BitMode(__in unsigned char* pImage, __out unsigned char* pDest, __in unsigned int nWidth, __in unsigned int nHeight)
{
	unsigned int i, j;
	unsigned int nByteWidth;

	nByteWidth = nWidth * 5 / 4;
	if (nByteWidth % 4)
	{  //if not divided by 4, increase 2 byte for consider dummy 2 byte     // D D D ... D   
		nByteWidth += 2;													// D D D     D
	}																		// D D D     X <-- here consider last dummy 2 byte if not divided by 4.

	for (i = 0; i < nHeight; i++)
	{
		for (j = 0; j < nWidth; j += 4)
		{
			memcpy(&pDest[i*nWidth + j], &pImage[i*nByteWidth + j * 5 / 4], 4);
		}
	}
}

//=============================================================================
// Method		: Shift12BitMode
// Access		: public static  
// Returns		: void
// Parameter	: __in unsigned char * pImage
// Parameter	: __out unsigned char * pDest
// Parameter	: __in unsigned int nWidth
// Parameter	: __in unsigned int nHeight
// Qualifier	:
// Last Update	: 2017/7/18 - 18:43
// Desc.		:
//=============================================================================
void CColorSpace::Shift12BitMode(__in unsigned char* pImage, __out unsigned char* pDest, __in unsigned int nWidth, __in unsigned int nHeight)
{
 	unsigned int i, j;
 	unsigned int nByteWidth;
 
 	nByteWidth = nWidth * 3 / 2;
 	if (nByteWidth % 4) 
 	{  //if not divided by 4, increase 2 byte for consider dummy 2 byte			// D D D ... D   
 		nByteWidth += 2;														// D D D     D
 	}																			// D D D     X <-- here consider last dummy 2 byte if not divided by 4.
 
 	for (i = 0; i < nHeight; i++)
 	{
 		for (j = 0; j < nWidth; j += 2)
 		{
 			memcpy(&pDest[i*nWidth + j], &pImage[i*nByteWidth + j * 3 / 2], 2);
 		}
 	}
}


void CColorSpace::Make10BitMode(__in unsigned char* pImage, __out unsigned short* pDest, __in unsigned int nWidth, __in unsigned int nHeight)
{
	unsigned int i, j;
	unsigned int nByteWidth;

	nByteWidth = nWidth * 5 / 4;
	if (nByteWidth % 4)		//if not divided by 4, increase 2 byte for consider dummy 2 byte
	{					    // D D D ... D   
		nByteWidth += 2;	// D D D     D
	}						// D D D     X <-- here consider last dummy 2 byte if not divided by 4.

	for (i = 0; i < nHeight; i++)
	{
		for (j = 0; j < nWidth; j += 4)
		{
			//LSB bit swap, so fix this bug - 2013. 10/01 OHSK
			pDest[i*nWidth + j + 0] = (WORD)((pImage[i*nByteWidth + j * 5 / 4 + 0] << 2) + (pImage[i*nByteWidth + j * 5 / 4 + 4] & 0x03));
			pDest[i*nWidth + j + 1] = (WORD)((pImage[i*nByteWidth + j * 5 / 4 + 1] << 2) + ((pImage[i*nByteWidth + j * 5 / 4 + 4] & 0x0C) >> 2));
			pDest[i*nWidth + j + 2] = (WORD)((pImage[i*nByteWidth + j * 5 / 4 + 2] << 2) + ((pImage[i*nByteWidth + j * 5 / 4 + 4] & 0x30) >> 4));
			pDest[i*nWidth + j + 3] = (WORD)((pImage[i*nByteWidth + j * 5 / 4 + 3] << 2) + ((pImage[i*nByteWidth + j * 5 / 4 + 4] & 0xC0) >> 6));
		}
	}
}

void CColorSpace::Make12BitMode(__in unsigned char* pImage, __out unsigned short* pDest, __in unsigned int nWidth, __in unsigned int nHeight)
{
	unsigned int i, j;
	unsigned int nByteWidth;

	nByteWidth = nWidth * 3 / 2;
	if (nByteWidth % 4)		//if not divided by 4, increase 2 byte for consider dummy 2 byte
	{					    // D D D ... D   
		nByteWidth += 2;	// D D D     D
	}						// D D D     X <-- here consider last dummy 2 byte if not divided by 4.

	int Count = 0;
	for (i = 0; i < nHeight; i++)
	{
		for (j = 0; j < nByteWidth; j += 3)
		{
			//LSB bit swap, so fix this bug - 2013. 10/01 OHSK
			pDest[Count++] = (WORD)((pImage[i*nByteWidth + j + 0] << 4) + (pImage[i*nByteWidth + j + 2] & 0x0F));
			pDest[Count++] = (WORD)((pImage[i*nByteWidth + j + 1] << 4) + ((pImage[i*nByteWidth + j + 2] & 0xF0) >> 4));

		}
	}

// 	for (i = 0; i < nHeight; i++)
// 	{
// 		for (j = 0; j < nWidth; j += 2)
// 		{
// 			//LSB bit swap, so fix this bug - 2013. 10/01 OHSK
// 			pDest[i*nWidth + j + 0] = (WORD)((pImage[i*nByteWidth + j * 3 / 2 + 0] << 4) + (pImage[i*nByteWidth + j * 3 / 2 + 2] & 0x0F));
// 			pDest[i*nWidth + j + 1] = (WORD)((pImage[i*nByteWidth + j * 3 / 2 + 1] << 4) + ((pImage[i*nByteWidth + j * 3 / 2 + 2] & 0xF0) >> 4));
// 		}
// 	}
}

void CColorSpace::Make12BitMode_RCCC(__in unsigned char* pImage, __out unsigned short* pDest, __in unsigned int nWidth, __in unsigned int nHeight)
{
	unsigned int i, j;
	unsigned int nByteWidth;

	nByteWidth = nWidth * 2;

	int Count = 0;
	for (i = 0; i < nHeight; i++)
	{
		for (j = 0; j < nByteWidth; j += 2)
		{
#if 1
			pDest[Count++] = (WORD)(((pImage[i*nByteWidth + j + 1] & 0x0F) << 8) + (pImage[i*nByteWidth + j + 0]));
#else
			pDest[Count++] = (WORD)((pImage[i*nByteWidth + j + 0] & 0x0F) << 8 + pImage[i*nByteWidth + j + 1]);
#endif
		}
	}
}

void CColorSpace::Make12to16_CCCR_16to12(unsigned short* p12bitRaw, int nWidth, int nHeight)
{
	//-Shift12to16BitMode
	LPBYTE			p16bitRaw;

	p16bitRaw = new BYTE[nWidth*nHeight*2];

// 	int k = 0;
// 	int n12bitRawSize = nWidth * nHeight * 1.5;
// 
// 	for (int i = 0; i < n12bitRawSize; i += 3)
// 	{
// 		p16bitRaw[k + 1] = (p12bitRaw[i] & 0xF0) >> 4;
// 		p16bitRaw[k + 0] = ((p12bitRaw[i] & 0x0F) << 4) + ((p12bitRaw[i + 2] & 0x0F));
// 
// 		p16bitRaw[k + 3] = (p12bitRaw[i + 1] & 0xF0) >> 4;
// 		p16bitRaw[k + 2] = ((p12bitRaw[i + 1] & 0x0F) << 4) + ((p12bitRaw[i + 2] & 0xF0) >> 4);
// 
// 		k += 4;
// 	}
// 	
	//-CCCR_Interpolation
	WORD nNewValue = 0;
	int start_x = 3;
	int start_y =1;

// 	if (m_RCCC_move_x) start_x = 2;		//	Bit Shift에 따른 Interpolation 영향 계단현상 해소
// 	if (m_RCCC_move_y) start_y = 2;

	for (int p = start_y; p < nHeight - 1; p += 2)
	{
		for (int k = start_x; k < nWidth - 1; k += 2)
		{
			//nNewValue =  pData[(p - 1) * nWidth + (k + 0)] + pData[(p + 1) * nWidth + (k + 0)] + pData[(p + 0) * nWidth + (k - 1)]  + pData[(p + 0) * nWidth + (k - 1)] ;
			nNewValue = (WORD)p12bitRaw[(p - 1) * nWidth + (k + 0)] + (WORD)p12bitRaw[(p + 1) * nWidth + (k + 0)] + (WORD)p12bitRaw[(p + 0) * nWidth + (k - 1)] + (WORD)p12bitRaw[(p + 0) * nWidth + (k + 1)];

			nNewValue = nNewValue / 4;
			p12bitRaw[p * nWidth + k] = (WORD)nNewValue;

		}
	}

	//-Shift16to12BitMode

// 	unsigned int i, j;
// 	unsigned int nScrWidth, nDestWidth;
// 	unsigned char *pcDest;
// 
// 	nDestWidth = nWidth * 3 / 2;
// 	nScrWidth = nWidth * 2;
// 
// 	pcDest = (unsigned char *)p12bitRaw;
// 
// 	for (i = 0; i < (nHeight); i++)
// 	{
// 		for (j = 0, k = 0; j < nDestWidth; j += 6, k += 8)
// 		{
// 			pcDest[j + 0] = ((p16bitRaw[k] & 0xF0) >> 4) + ((p16bitRaw[k + 1] & 0x0f) << 4);
// 			pcDest[j + 1] = ((p16bitRaw[k + 2] & 0xF0) >> 4) + ((p16bitRaw[k + 3] & 0x0f) << 4);
// 			pcDest[j + 2] = (p16bitRaw[k] & 0x0F) + ((p16bitRaw[k + 2] & 0x0f) << 4);
// 
// 			pcDest[j + 3] = ((p16bitRaw[k + 4] & 0xF0) >> 4) + ((p16bitRaw[k + 5] & 0x0f) << 4);
// 			pcDest[j + 4] = ((p16bitRaw[k + 6] & 0xF0) >> 4) + ((p16bitRaw[k + 7] & 0x0f) << 4);
// 			pcDest[j + 5] = (p16bitRaw[k + 4] & 0x0F) + ((p16bitRaw[k + 6] & 0x0f) << 4);
// 		}
// 		pcDest += nDestWidth;
// 		p16bitRaw += nScrWidth;
// 	}

	delete[]p16bitRaw;
}

//=============================================================================
// Method		: CropOddFrame
// Access		: public static  
// Returns		: void
// Parameter	: __inout LPBYTE pbySrc
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Parameter	: __in UINT nPixelCount
// Qualifier	:
// Last Update	: 2017/9/5 - 13:20
// Desc.		:
//=============================================================================
void CColorSpace::CropOddFrame(__inout LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __in UINT nPixelCount /*= 1*/)
{
	UINT nDstOffset		= 0;
	UINT nSrcOffset		= 0;
	UINT nWidthPexelSize= nWidth * nPixelCount;
	UINT nSrcIndex		= 2;

	for (UINT nDstIndex = 1; nDstIndex < (nHeight / 2); nDstIndex++)
	{
		nDstOffset	= nDstIndex * nWidthPexelSize;
		nSrcOffset	= nSrcIndex * nWidthPexelSize;

		memcpy(&pbySrc[nDstOffset], &pbySrc[nSrcOffset], nWidthPexelSize);

		nSrcIndex += 2;
	}
}

void CColorSpace::CropOddFrame(__inout LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in UINT nPixelCount /*= 1*/)
{
	UINT nDstOffset = 0;
	UINT nSrcOffset = 0;
	UINT nWidthPexelSize = nWidth * nPixelCount;
	UINT nSrcIndex = 2;

	for (UINT nDstIndex = 1; nDstIndex < (nHeight / 2); nDstIndex++)
	{
		nDstOffset = nDstIndex * nWidthPexelSize;
		nSrcOffset = nSrcIndex * nWidthPexelSize;

		memcpy(&pbyDst[nDstOffset], &pbySrc[nSrcOffset], nWidthPexelSize);

		nSrcIndex += 2;
	}
}

//=============================================================================
// Method		: CropEvenFrame
// Access		: public static  
// Returns		: void
// Parameter	: __inout const LPBYTE pbySrc
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Parameter	: __in UINT nPixelCount
// Qualifier	:
// Last Update	: 2017/9/5 - 13:20
// Desc.		:
//=============================================================================
void CColorSpace::CropEvenFrame(__inout const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __in UINT nPixelCount /*= 1*/)
{
	UINT nDstOffset = 0;
	UINT nSrcOffset = 0;
	UINT nWidthPexelSize = nWidth * nPixelCount;
	UINT nSrcIndex = 1;

	for (UINT nDstIndex = 0; nDstIndex < (nHeight / 2); nDstIndex++)
	{
		nDstOffset = nDstIndex * nWidthPexelSize;
		nSrcOffset = nSrcIndex * nWidthPexelSize;

		memcpy(&pbySrc[nDstOffset], &pbySrc[nSrcOffset], nWidthPexelSize);

		nSrcIndex += 2;
	}
}

void CColorSpace::CropEvenFrame(__inout LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in UINT nPixelCount /*= 1*/)
{
	UINT nDstOffset = 0;
	UINT nSrcOffset = 0;
	UINT nWidthPexelSize = nWidth * nPixelCount;
	UINT nSrcIndex = 1;

	for (UINT nDstIndex = 0; nDstIndex < (nHeight / 2); nDstIndex++)
	{
		nDstOffset = nDstIndex * nWidthPexelSize;
		nSrcOffset = nSrcIndex * nWidthPexelSize;

		memcpy(&pbyDst[nDstOffset], &pbySrc[nSrcOffset], nWidthPexelSize);

		nSrcIndex += 2;
	}
}
