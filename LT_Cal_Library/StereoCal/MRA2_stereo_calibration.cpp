#include "MRA2_stereo_calibration.h"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>     // cout
#include <algorithm>    // std::sort

#define _USE_MATH_DEFINES // for C  
#include <math.h>  

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
//#include <opencv2/legacy/legacy.hpp>
#include <opencv2/calib3d/calib3d.hpp>

using namespace cv;
using namespace std;

using std::vector;
using std::string;

#define DEBUG_DISPLAY

#define USE_STEREO_INTRINSIC	// Stereo CAL인경우 Stereo CAL 처리후에 변경된 Intrinsic Parameter 사용 (Use Intrinsic Guess 플래그를 사용 할 경우) modify.2018.7.3
//#define USE_EulerAngles			// Rot 값 이상현상 문제로 Rodrigues 함수 사용 (2018.6.12)

/*
단일 카메라의 캘리브레이션 함수

objPoint		캘리브레이션 패틴의 위치 정보
imgPoint		찾은 패턴의 정보
image_size		영상의 크기
camera_matrix	camera intrinsics matrix
dist_coef		distortion matrix
return			rms 값
*/
__declspec(dllexport) double MRA2_calibrate_camera(
	vector<vector<Point3f> > objPoint,
	vector<vector<Point2f> > imgPoint,
	MRA2_CAMERA_FLAG mra2_camera_flag,
	Size image_size,
	Mat& camera_matrix,
	Mat& dist_coef);

/*
스테레오 카메라의 캘리브레이션 함수

objPoints			캘리브레이션 패틴의 위치 정보
imagePointsLeft		왼쪽 찾은 패턴의 정보
imagePointsRight	오른쪽 찾은 패턴의 정보
image_size			영상의 크기
cameraMmatrixLeft	왼쪽 camera intrinsics matrix
distCoeffsLeft		왼쪽 distortion matrix
cameraMmatrixRight	오른쪽 camera intrinsics matrix
distCoeffsRight		오른쪽 distortion matrix
R					rotation matrix
R_VEC				1x3 rotation vector
T					translation matrix
POS					pose matrix
return				rms 값
*/
__declspec(dllexport) double MRA2_calibrate_stereo(
	vector<vector<Point3f> > objectPoints,
	vector<vector<Point2f> > imagePointsLeft, vector<vector<Point2f> > imagePointsRight,
	MRA2_CAMERA_FLAG mra2_camera_flag,
	Size imageSize,
	Mat& cameraMatrixLeft, Mat& distCoeffsLeft,
	Mat& cameraMatrixRight, Mat& distCoeffsRight,
	Mat& R, Mat& R_VEC, Mat& T, Mat& POS);

// Circle Detect 부분
/*
Size patternsize; // = Size(board_w, board_h); // Chart의 Circle 갯수 (10x10등)
Mat img; //Capture 된 이미지들
vector<Point2f> centers; //Circle 중심 위치 찾은 후 저장 변수
vector<vector<Point2f> > imgPoint;	//	Center를 누적시킬 Image
Size image_size;
*/

typedef struct MRA2_signle_cameara_processing_info
{
	Size patternsize;
	vector<Point2f>* centers;
	vector<vector<Point2f>>* imgPoint;
	Size image_size;

	Mat* camera_matrix;
	Mat* dist_coef;
} MRA2_SINGLE_CAMERA_PROCESSING_INFO, *PMRA2_SINGLE_CAMERA_PROCESSING_INFO;

typedef struct MRA2_stereo_cameara_processing_info
{
	Size patternsize;

	vector<Point3f>* boardModel;
	vector<vector<Point3f> >* objectPoints;

	MRA2_HANDLE mra2_handle_left;
	MRA2_HANDLE mra2_handle_right;

} MRA2_STEREO_CAMERA_PROCESSING_INFO, *PMRA2_STEREO_CAMERA_PROCESSING_INFO;

MRA2_HANDLE MRA2_single_camera_start(int board_w, int board_h)
{
	PMRA2_SINGLE_CAMERA_PROCESSING_INFO pMra2SingleCameraProcInfo = (PMRA2_SINGLE_CAMERA_PROCESSING_INFO)malloc(sizeof(MRA2_SINGLE_CAMERA_PROCESSING_INFO));

	if (pMra2SingleCameraProcInfo == (MRA2_HANDLE)NULL)
		return (MRA2_HANDLE)NULL;

	pMra2SingleCameraProcInfo->patternsize = Size(board_w, board_h);

	pMra2SingleCameraProcInfo->centers = new vector<Point2f>;
	pMra2SingleCameraProcInfo->imgPoint = new vector<vector<Point2f>>;

	pMra2SingleCameraProcInfo->camera_matrix = new Mat;
	pMra2SingleCameraProcInfo->dist_coef = new Mat;

	return (MRA2_HANDLE)pMra2SingleCameraProcInfo;
}

void MRA2_single_camera_stop(MRA2_HANDLE mra2_handle)
{
	PMRA2_SINGLE_CAMERA_PROCESSING_INFO pMra2SingleCameraProcInfo = (PMRA2_SINGLE_CAMERA_PROCESSING_INFO)mra2_handle;

	if (pMra2SingleCameraProcInfo == (MRA2_HANDLE)NULL)
		return;

	delete pMra2SingleCameraProcInfo->centers;
	delete pMra2SingleCameraProcInfo->imgPoint;

	delete pMra2SingleCameraProcInfo->camera_matrix;
	delete pMra2SingleCameraProcInfo->dist_coef;

	free(pMra2SingleCameraProcInfo);
}

bool MRA2_find_single_camera_pattern(
	MRA2_HANDLE mra2_handle, 
	void* imageData, 
	MRA2_IMAGE_FORMAT imageFormat, 
	int width, int height, 
	bool isCircle, 
	MRA2_POINT* ceneters,
	int* pcenters_num)
{
	PMRA2_SINGLE_CAMERA_PROCESSING_INFO pMra2SingleCameraProcInfo = (PMRA2_SINGLE_CAMERA_PROCESSING_INFO)mra2_handle;

	bool found;
	Mat img;
#ifdef DEBUG_DISPLAY
	bool displayCorners = true;
#else
	bool displayCorners = false;
#endif
	
	if (pMra2SingleCameraProcInfo == (MRA2_HANDLE)NULL)
		return false;

	switch (imageFormat)
	{
	case MRA2_IMAGE_FORMAT_8_GRAY:
		img.create(Size(width, height), CV_8UC1);
		memcpy((void*)img.data, (void*)imageData,  width*height);
		break;
	case MRA2_IMAGE_FORMAT_16_GRAY:
		img.create(Size(width, height), CV_16UC1);
		memcpy((void*)img.data, (void*)imageData, 2*width*height);
		break;
	case MRA2_IMAGE_FORMAT_24_RGB:
		img.create(Size(width, height), CV_8UC3);
		memcpy((void*)img.data, (void*)imageData, 3 * width*height);
		break;
	}

	pMra2SingleCameraProcInfo->image_size = img.size();

	if (isCircle)
	{
		found = findCirclesGrid(img, pMra2SingleCameraProcInfo->patternsize, *(pMra2SingleCameraProcInfo->centers), CALIB_CB_SYMMETRIC_GRID);		//	Symmetric = 정원
	}else
	{
		found = findChessboardCorners(img, pMra2SingleCameraProcInfo->patternsize, *(pMra2SingleCameraProcInfo->centers));
	}

	if(pcenters_num != NULL)
		*pcenters_num = 0;

	if (found)		//	Circle Pattern 검출이 정상적일 때,
	{
		pMra2SingleCameraProcInfo->imgPoint->push_back(*(pMra2SingleCameraProcInfo->centers));		//	Center 누적

		if (pcenters_num != NULL && ceneters != NULL)
		{
			*pcenters_num = pMra2SingleCameraProcInfo->centers->size();
			for (int i = 0; i < *pcenters_num; i++)
			{
				ceneters[i].x = pMra2SingleCameraProcInfo->centers->at(i).x;
				ceneters[i].y = pMra2SingleCameraProcInfo->centers->at(i).y;
			}
		}

	}

	if (displayCorners)
	{
		drawChessboardCorners(img, pMra2SingleCameraProcInfo->patternsize, *(pMra2SingleCameraProcInfo->centers), found);
		namedWindow("image", WINDOW_NORMAL);
		resizeWindow("image", 3*1024 / 4, 3*768 / 4);
		imshow("image", img);
		waitKey(1);
	}

	return found;
}

bool MRA2_find_single_camera_pattern2(MRA2_HANDLE mra2_handle, char* imageFileName, bool isCircle)
{
	PMRA2_SINGLE_CAMERA_PROCESSING_INFO pMra2SingleCameraProcInfo = (PMRA2_SINGLE_CAMERA_PROCESSING_INFO)mra2_handle;

	bool found;
	Mat img;
#ifdef DEBUG_DISPLAY
	bool displayCorners = true;
#else
	bool displayCorners = false;
#endif


	if (pMra2SingleCameraProcInfo == (MRA2_HANDLE)NULL)
		return false;

	img = imread(imageFileName, 0);

	if (img.empty())
	{
		printf("bad file information...\n");
		return false;
	}

	pMra2SingleCameraProcInfo->image_size = img.size();

	if (isCircle)
	{
		found = findCirclesGrid(img, pMra2SingleCameraProcInfo->patternsize, *(pMra2SingleCameraProcInfo->centers), CALIB_CB_SYMMETRIC_GRID);		//	Symmetric = 정원
	}
	else
	{
		found = findChessboardCorners(img, pMra2SingleCameraProcInfo->patternsize, *(pMra2SingleCameraProcInfo->centers));
	}

	if (found)		//	Circle Pattern 검출이 정상적일 때,
	{
		pMra2SingleCameraProcInfo->imgPoint->push_back(*(pMra2SingleCameraProcInfo->centers));		//	Center 누적
	}
	else
	{
		printf("pattern not found: %d\n", pMra2SingleCameraProcInfo->centers->size());
	}

	if (displayCorners)
	{
		drawChessboardCorners(img, pMra2SingleCameraProcInfo->patternsize, *(pMra2SingleCameraProcInfo->centers), found);
		namedWindow("image", WINDOW_NORMAL);
		resizeWindow("image", 3*1024 / 4, 3*768 / 4);
		imshow("image", img);
		waitKey(-1);
	}

	return found;
}

bool MRA2_find_single_camera_parameter(
	MRA2_HANDLE mra2_handle,
	MRA2_CAMERA_FLAG mra2_camera_flag,
	PSINGLE_CALIBRATION_RESULT single_calibration_result
	)
{
	PMRA2_SINGLE_CAMERA_PROCESSING_INFO pMra2SingleCameraProcInfo = (PMRA2_SINGLE_CAMERA_PROCESSING_INFO)mra2_handle;

	// Image Point 사이즈에 맞춰서 Object Point 매트릭스를 Resize
	vector<vector<Point3f> > objPoint(1);

	if (pMra2SingleCameraProcInfo == (MRA2_HANDLE)NULL)
		return false;

	for (int i = 0; i < pMra2SingleCameraProcInfo->patternsize.height; i++) {			//		10 = Grid 갯수 (row)
		for (int j = 0; j < pMra2SingleCameraProcInfo->patternsize.width; j++) {		//		10 = Grid 갯수 (col)
			objPoint[0].push_back(Point3f(j * SQUARE_SIZE, i * SQUARE_SIZE, 0.f));		//	30 = Chart Circle 크기
		}
	}

	objPoint.resize(pMra2SingleCameraProcInfo->imgPoint->size(), objPoint[0]);

	// MRA2 Intrinsic Init 기본값 초기화
	Mat camera_matrix = (Mat_<double>(3, 3) << 1470.0, 0, 640.0, 0, 1470.0, 482.0, 0, 0, 1);
	Mat dist_coef = (Mat_<double>(1, 5) << -0.1, -0.2, 0, 0, 0);

	/*
	초기값
	fx, fy = 1470.0
	cx	   =  640.0
	cy     =  482.0
	k1     =-   0.1
	k2     =-   0.2
	*/

	double rms = MRA2_calibrate_camera(
		objPoint, 
		*(pMra2SingleCameraProcInfo->imgPoint), 
		mra2_camera_flag,
		pMra2SingleCameraProcInfo->image_size, 
		camera_matrix, dist_coef);

	*(pMra2SingleCameraProcInfo->camera_matrix) = camera_matrix;
	*(pMra2SingleCameraProcInfo->dist_coef) = dist_coef;

	//cout << "camera_matrix: " << *(pMra2SingleCameraProcInfo->camera_matrix) << endl;
	//cout << "dist_coeff: " << *(pMra2SingleCameraProcInfo->dist_coef) << endl;

	single_calibration_result->fx = camera_matrix.at<double>(0, 0);
	single_calibration_result->fy = camera_matrix.at<double>(1, 1);
	single_calibration_result->cx = camera_matrix.at<double>(0, 2);
	single_calibration_result->cy = camera_matrix.at<double>(1, 2);

	single_calibration_result->distortion_cofficeints[0] = dist_coef.at<double>(0, 0);
	single_calibration_result->distortion_cofficeints[1] = dist_coef.at<double>(0, 1);
	single_calibration_result->distortion_cofficeints[2] = dist_coef.at<double>(0, 2);
	single_calibration_result->distortion_cofficeints[3] = dist_coef.at<double>(0, 3);
	single_calibration_result->distortion_cofficeints[4] = dist_coef.at<double>(0, 4);

	single_calibration_result->calib_res_width = pMra2SingleCameraProcInfo->image_size.width;
	single_calibration_result->calib_res_height = pMra2SingleCameraProcInfo->image_size.height;

	single_calibration_result->rms = rms;

	return true;
}

double MRA2_calibrate_camera(
	vector<vector<Point3f> > objPoint,
	vector<vector<Point2f> > imgPoint,
	MRA2_CAMERA_FLAG mra2_camera_flag,
	Size image_size,
	Mat& camera_matrix,
	Mat& dist_coef)
{
	//	Calibration Flags 설정
	int flags = 0;

	if (mra2_camera_flag == MRA2_CAMERA_FLAG_1)
		flags |= CALIB_FIX_INTRINSIC;
	else if (mra2_camera_flag == MRA2_CAMERA_FLAG_2)
		flags |= CALIB_USE_INTRINSIC_GUESS;

	flags |= CALIB_FIX_K3;					//	p1, p2, K3 값 초기값에서 고정
	flags |= CALIB_FIX_K4;
	flags |= CALIB_FIX_K5;
	flags |= CALIB_ZERO_TANGENT_DIST;		//	p1, p2 값 0으로 고정
	flags |= CALIB_FIX_ASPECT_RATIO;


	//	OpenCV 버젼에 따라 매개변수 순서가 달라질 수 있음
	//	Image Size는 Capture Image 의 Size
	//  NULL은 Rotation / Translation Matrix임
	double rms = cv::calibrateCamera(
		objPoint, imgPoint,
		image_size,
		camera_matrix, dist_coef,
		cv::noArray(), cv::noArray(),
		flags);

	return rms;
}

MRA2_HANDLE MRA2_stereo_camera_start(int board_w, int board_h)
{
	PMRA2_STEREO_CAMERA_PROCESSING_INFO pMra2StereoCameraProcInfo = (PMRA2_STEREO_CAMERA_PROCESSING_INFO)malloc(sizeof(MRA2_STEREO_CAMERA_PROCESSING_INFO));
	int i, j;

	if (pMra2StereoCameraProcInfo == (MRA2_HANDLE)NULL)
		return (MRA2_HANDLE)NULL;

	pMra2StereoCameraProcInfo->patternsize = Size(board_w, board_h);

	pMra2StereoCameraProcInfo->boardModel = new vector<Point3f>;
	pMra2StereoCameraProcInfo->objectPoints = new vector<vector<Point3f>>;

	pMra2StereoCameraProcInfo->mra2_handle_left = MRA2_single_camera_start(board_w, board_h);
	pMra2StereoCameraProcInfo->mra2_handle_right = MRA2_single_camera_start(board_w, board_h);

	for (i = 0; i < board_h; i++)
	{
		for (j = 0; j < board_w; j++)
			pMra2StereoCameraProcInfo->boardModel->push_back(
			Point3f((float)(i * SQUARE_SIZE), (float)(j * SQUARE_SIZE), 0.f));
	}

	return (MRA2_HANDLE)pMra2StereoCameraProcInfo;
}

void MRA2_stereo_camera_stop(MRA2_HANDLE mra2_handle)
{
	PMRA2_STEREO_CAMERA_PROCESSING_INFO pMra2StereoCameraProcInfo = (PMRA2_STEREO_CAMERA_PROCESSING_INFO)mra2_handle;

	if (pMra2StereoCameraProcInfo == (MRA2_HANDLE)NULL)
		return;

	delete pMra2StereoCameraProcInfo->objectPoints;
	delete pMra2StereoCameraProcInfo->boardModel;

	MRA2_single_camera_stop(pMra2StereoCameraProcInfo->mra2_handle_left);
	MRA2_single_camera_stop(pMra2StereoCameraProcInfo->mra2_handle_right);

	free(pMra2StereoCameraProcInfo);
}

bool MRA2_find_stereo_camera_pattern(
	MRA2_HANDLE mra2_handle, 
	void* imageDataLeft, void* imageDataRight,
	MRA2_IMAGE_FORMAT imageFormat, 
	int width, int height, 
	bool isCircle,
	MRA2_POINT* left_ceneters, int* pleft_centers_num,
	MRA2_POINT* right_ceneters, int* pright_centers_num)
{
	PMRA2_STEREO_CAMERA_PROCESSING_INFO pMra2StereoCameraProcInfo = (PMRA2_STEREO_CAMERA_PROCESSING_INFO)mra2_handle;

	bool foundLeft;
	bool foundRight;
#ifdef DEBUG_DISPLAY
	bool displayCorners = true;
#else
	bool displayCorners = false;
#endif


	if (pMra2StereoCameraProcInfo == (MRA2_HANDLE)NULL)
		return false;

	foundLeft = MRA2_find_single_camera_pattern(
		pMra2StereoCameraProcInfo->mra2_handle_left,
		imageDataLeft, 
		imageFormat, width, height, isCircle,NULL,NULL);

	foundRight = MRA2_find_single_camera_pattern(
		pMra2StereoCameraProcInfo->mra2_handle_right,
		imageDataRight,
		imageFormat, width, height, isCircle,NULL,NULL);
	
	*pleft_centers_num = 0;
	*pright_centers_num = 0;

	if (foundLeft)
	{
		PMRA2_SINGLE_CAMERA_PROCESSING_INFO pMra2SingleCameraProcInfo = (PMRA2_SINGLE_CAMERA_PROCESSING_INFO)pMra2StereoCameraProcInfo->mra2_handle_left;
		*pleft_centers_num = pMra2SingleCameraProcInfo->centers->size();
		for (int i = 0; i < *pleft_centers_num; i++)
		{
			left_ceneters[i].x = pMra2SingleCameraProcInfo->centers->at(i).x;
			left_ceneters[i].y = pMra2SingleCameraProcInfo->centers->at(i).y;
		}
	}

	if (foundRight)
	{
		PMRA2_SINGLE_CAMERA_PROCESSING_INFO pMra2SingleCameraProcInfo = (PMRA2_SINGLE_CAMERA_PROCESSING_INFO)pMra2StereoCameraProcInfo->mra2_handle_right;
		*pright_centers_num = pMra2SingleCameraProcInfo->centers->size();
		for (int i = 0; i < *pright_centers_num; i++)
		{
			right_ceneters[i].x = pMra2SingleCameraProcInfo->centers->at(i).x;
			right_ceneters[i].y = pMra2SingleCameraProcInfo->centers->at(i).y;
		}
	}

	if (foundLeft && foundRight) {
		pMra2StereoCameraProcInfo->objectPoints->push_back(*(pMra2StereoCameraProcInfo->boardModel));
	}

	return (foundLeft && foundRight);
}

double MRA2_find_stereo_camera_parameter(
	MRA2_HANDLE mra2_handle,
	MRA2_CAMERA_FLAG mra2_camera_flag,
	PSTEREO_CALIBRATION_RESULT stereo_calibration_result
	)
{
	PMRA2_STEREO_CAMERA_PROCESSING_INFO pMra2StereoCameraProcInfo = (PMRA2_STEREO_CAMERA_PROCESSING_INFO)mra2_handle;

	SINGLE_CALIBRATION_RESULT single_calibration_result_left;
	SINGLE_CALIBRATION_RESULT single_calibration_result_right;
	// CALIBRATE THE STEREO CAMERAS
	Mat cameraMatrixLeft = Mat::eye(3, 3, CV_64F);
	Mat cameraMatrixRight = Mat::eye(3, 3, CV_64F);
	Mat distCoeffsLeft, distCoeffsRight;

	if (pMra2StereoCameraProcInfo == (MRA2_HANDLE)NULL)
		return false;

	PMRA2_SINGLE_CAMERA_PROCESSING_INFO pMra2SingleCameraProcInfoLeft = (PMRA2_SINGLE_CAMERA_PROCESSING_INFO)pMra2StereoCameraProcInfo->mra2_handle_left;
	PMRA2_SINGLE_CAMERA_PROCESSING_INFO pMra2SingleCameraProcInfoRight = (PMRA2_SINGLE_CAMERA_PROCESSING_INFO)pMra2StereoCameraProcInfo->mra2_handle_right;

	MRA2_find_single_camera_parameter(pMra2StereoCameraProcInfo->mra2_handle_left, mra2_camera_flag, &single_calibration_result_left);
	MRA2_find_single_camera_parameter(pMra2StereoCameraProcInfo->mra2_handle_right, mra2_camera_flag, &single_calibration_result_right);

	// master camera
	stereo_calibration_result->single_master_fx = single_calibration_result_left.fx;
	stereo_calibration_result->single_master_fy = single_calibration_result_left.fy;
	stereo_calibration_result->single_master_cx = single_calibration_result_left.cx;
	stereo_calibration_result->single_master_cy = single_calibration_result_left.cy;

	stereo_calibration_result->single_master_distortion_cofficeints[0] = single_calibration_result_left.distortion_cofficeints[0]; // k1-k5: distortion_cofficeints[0] - distortion_cofficeints[4]
	stereo_calibration_result->single_master_distortion_cofficeints[1] = single_calibration_result_left.distortion_cofficeints[1]; // k1-k5: distortion_cofficeints[0] - distortion_cofficeints[4]
	stereo_calibration_result->single_master_distortion_cofficeints[2] = single_calibration_result_left.distortion_cofficeints[2]; // k1-k5: distortion_cofficeints[0] - distortion_cofficeints[4]
	stereo_calibration_result->single_master_distortion_cofficeints[3] = single_calibration_result_left.distortion_cofficeints[3]; // k1-k5: distortion_cofficeints[0] - distortion_cofficeints[4]
	stereo_calibration_result->single_master_distortion_cofficeints[4] = single_calibration_result_left.distortion_cofficeints[4]; // k1-k5: distortion_cofficeints[0] - distortion_cofficeints[4]
	
	stereo_calibration_result->single_master_rms = single_calibration_result_left.rms;

	// slave camera
	stereo_calibration_result->single_slave_fx = single_calibration_result_right.fx;
	stereo_calibration_result->single_slave_fy = single_calibration_result_right.fy;
	stereo_calibration_result->single_slave_cx = single_calibration_result_right.cx;
	stereo_calibration_result->single_slave_cy = single_calibration_result_right.cy;

	stereo_calibration_result->single_slave_distortion_cofficeints[0] = single_calibration_result_right.distortion_cofficeints[0]; // k1-k5: distortion_cofficeints[0] - distortion_cofficeints[4]
	stereo_calibration_result->single_slave_distortion_cofficeints[1] = single_calibration_result_right.distortion_cofficeints[1]; // k1-k5: distortion_cofficeints[0] - distortion_cofficeints[4]
	stereo_calibration_result->single_slave_distortion_cofficeints[2] = single_calibration_result_right.distortion_cofficeints[2]; // k1-k5: distortion_cofficeints[0] - distortion_cofficeints[4]
	stereo_calibration_result->single_slave_distortion_cofficeints[3] = single_calibration_result_right.distortion_cofficeints[3]; // k1-k5: distortion_cofficeints[0] - distortion_cofficeints[4]
	stereo_calibration_result->single_slave_distortion_cofficeints[4] = single_calibration_result_right.distortion_cofficeints[4]; // k1-k5: distortion_cofficeints[0] - distortion_cofficeints[4]
	
	stereo_calibration_result->single_slave_rms = single_calibration_result_right.rms;


	Mat R, R_VEC, T, POS;

	double stereo_rms = MRA2_calibrate_stereo(
		*(pMra2StereoCameraProcInfo->objectPoints),
		*(pMra2SingleCameraProcInfoLeft->imgPoint), 
		*(pMra2SingleCameraProcInfoRight->imgPoint),
		mra2_camera_flag,
		pMra2SingleCameraProcInfoLeft->image_size,
		*(pMra2SingleCameraProcInfoLeft->camera_matrix), 
		*(pMra2SingleCameraProcInfoLeft->dist_coef),
		*(pMra2SingleCameraProcInfoRight->camera_matrix),
		*(pMra2SingleCameraProcInfoRight->dist_coef),
		R, R_VEC, T, POS);


	// master camera
#ifdef USE_STEREO_INTRINSIC
	stereo_calibration_result->master_fx = (*(pMra2SingleCameraProcInfoLeft->camera_matrix)).at<double>(0, 0);
	stereo_calibration_result->master_fy = (*(pMra2SingleCameraProcInfoLeft->camera_matrix)).at<double>(1, 1);
	stereo_calibration_result->master_cx = (*(pMra2SingleCameraProcInfoLeft->camera_matrix)).at<double>(0, 2);
	stereo_calibration_result->master_cy = (*(pMra2SingleCameraProcInfoLeft->camera_matrix)).at<double>(1, 2);

	stereo_calibration_result->master_distortion_cofficeints[0] = (*(pMra2SingleCameraProcInfoLeft->dist_coef)).at<double>(0, 0);
	stereo_calibration_result->master_distortion_cofficeints[1] = (*(pMra2SingleCameraProcInfoLeft->dist_coef)).at<double>(0, 1);
	stereo_calibration_result->master_distortion_cofficeints[2] = (*(pMra2SingleCameraProcInfoLeft->dist_coef)).at<double>(0, 2);
	stereo_calibration_result->master_distortion_cofficeints[3] = (*(pMra2SingleCameraProcInfoLeft->dist_coef)).at<double>(0, 3);
	stereo_calibration_result->master_distortion_cofficeints[4] = (*(pMra2SingleCameraProcInfoLeft->dist_coef)).at<double>(0, 4);
#else
 	stereo_calibration_result->master_fx = single_calibration_result_left.fx;
 	stereo_calibration_result->master_fy = single_calibration_result_left.fy;
 	stereo_calibration_result->master_cx = single_calibration_result_left.cx;
 	stereo_calibration_result->master_cy = single_calibration_result_left.cy;

 	stereo_calibration_result->master_distortion_cofficeints[0] = single_calibration_result_left.distortion_cofficeints[0]; // k1-k5: distortion_cofficeints[0] - distortion_cofficeints[4]
 	stereo_calibration_result->master_distortion_cofficeints[1] = single_calibration_result_left.distortion_cofficeints[1]; // k1-k5: distortion_cofficeints[0] - distortion_cofficeints[4]
 	stereo_calibration_result->master_distortion_cofficeints[2] = single_calibration_result_left.distortion_cofficeints[2]; // k1-k5: distortion_cofficeints[0] - distortion_cofficeints[4]
 	stereo_calibration_result->master_distortion_cofficeints[3] = single_calibration_result_left.distortion_cofficeints[3]; // k1-k5: distortion_cofficeints[0] - distortion_cofficeints[4]
 	stereo_calibration_result->master_distortion_cofficeints[4] = single_calibration_result_left.distortion_cofficeints[4]; // k1-k5: distortion_cofficeints[0] - distortion_cofficeints[4]
#endif

	stereo_calibration_result->master_rms = single_calibration_result_left.rms;

	stereo_calibration_result->master_calib_res_width = single_calibration_result_left.calib_res_width;
	stereo_calibration_result->master_calib_res_height = single_calibration_result_left.calib_res_height;

	// slave camera
#ifdef USE_STEREO_INTRINSIC
	stereo_calibration_result->slave_fx = (*(pMra2SingleCameraProcInfoRight->camera_matrix)).at<double>(0, 0);
	stereo_calibration_result->slave_fy = (*(pMra2SingleCameraProcInfoRight->camera_matrix)).at<double>(1, 1);
	stereo_calibration_result->slave_cx = (*(pMra2SingleCameraProcInfoRight->camera_matrix)).at<double>(0, 2);
	stereo_calibration_result->slave_cy = (*(pMra2SingleCameraProcInfoRight->camera_matrix)).at<double>(1, 2);

	stereo_calibration_result->slave_distortion_cofficeints[0] = (*(pMra2SingleCameraProcInfoRight->dist_coef)).at<double>(0, 0);
	stereo_calibration_result->slave_distortion_cofficeints[1] = (*(pMra2SingleCameraProcInfoRight->dist_coef)).at<double>(0, 1);
	stereo_calibration_result->slave_distortion_cofficeints[2] = (*(pMra2SingleCameraProcInfoRight->dist_coef)).at<double>(0, 2);
	stereo_calibration_result->slave_distortion_cofficeints[3] = (*(pMra2SingleCameraProcInfoRight->dist_coef)).at<double>(0, 3);
	stereo_calibration_result->slave_distortion_cofficeints[4] = (*(pMra2SingleCameraProcInfoRight->dist_coef)).at<double>(0, 4);
#else
	stereo_calibration_result->slave_fx = single_calibration_result_right.fx;
	stereo_calibration_result->slave_fy = single_calibration_result_right.fy;
	stereo_calibration_result->slave_cx = single_calibration_result_right.cx;
	stereo_calibration_result->slave_cy = single_calibration_result_right.cy;

	stereo_calibration_result->slave_distortion_cofficeints[0] = single_calibration_result_right.distortion_cofficeints[0]; // k1-k5: distortion_cofficeints[0] - distortion_cofficeints[4]
	stereo_calibration_result->slave_distortion_cofficeints[1] = single_calibration_result_right.distortion_cofficeints[1]; // k1-k5: distortion_cofficeints[0] - distortion_cofficeints[4]
	stereo_calibration_result->slave_distortion_cofficeints[2] = single_calibration_result_right.distortion_cofficeints[2]; // k1-k5: distortion_cofficeints[0] - distortion_cofficeints[4]
	stereo_calibration_result->slave_distortion_cofficeints[3] = single_calibration_result_right.distortion_cofficeints[3]; // k1-k5: distortion_cofficeints[0] - distortion_cofficeints[4]
	stereo_calibration_result->slave_distortion_cofficeints[4] = single_calibration_result_right.distortion_cofficeints[4]; // k1-k5: distortion_cofficeints[0] - distortion_cofficeints[4]
#endif

	stereo_calibration_result->slave_rms = single_calibration_result_right.rms;

	stereo_calibration_result->slave_calib_res_width = single_calibration_result_right.calib_res_width;
	stereo_calibration_result->slave_calib_res_height = single_calibration_result_right.calib_res_height;

	//cout << "\n";
	//cout << "T" << T << endl;
	//cout << "R" << R << endl;
	//cout << "R_VEC" << R_VEC << endl;
	//cout << "POS =" << POS << endl;
	//cout << "\n";

	stereo_calibration_result->translation_vector[0] = T.at<double>(0);
	stereo_calibration_result->translation_vector[1] = T.at<double>(1);
	stereo_calibration_result->translation_vector[2] = T.at<double>(2);

	stereo_calibration_result->rotation_vector[0] = R_VEC.at<double>(0);
	stereo_calibration_result->rotation_vector[1] = R_VEC.at<double>(1);
	stereo_calibration_result->rotation_vector[2] = R_VEC.at<double>(2);

// 	double roll = atan2(R.at<double>(1,2), R.at<double>(2,2))*180./M_PI;
// 	double pitch = atan2(-R.at<double>(0, 2), sqrt(pow(R.at<double>(1, 2), 2) + pow(R.at<double>(2, 2), 2)))*180. / M_PI;
// 	double yaw = atan2(R.at<double>(0, 1), R.at<double>(0, 0))*180. / M_PI;
// 
// 	roll	= atan2(R.at<double>(2, 1), R.at<double>(2, 2))*180. / M_PI;
// 	pitch	= atan2(-R.at<double>(2, 0), sqrt(pow(R.at<double>(2, 1), 2) + pow(R.at<double>(2, 2), 2)))*180. / M_PI;
// 	yaw		= atan2(R.at<double>(1, 0), R.at<double>(0, 0))*180. / M_PI;
// 
// 	stereo_calibration_result->roll = roll;
// 	stereo_calibration_result->pitch = pitch;
// 	stereo_calibration_result->yaw = yaw;

	//stereo_calibration_result->roll = atan2(R.at<double>(1,2), R.at<double>(2,2))*180./M_PI;
	//stereo_calibration_result->pitch = atan2(-R.at<double>(0, 2), sqrt(pow(R.at<double>(1, 2), 2) + pow(R.at<double>(2, 2), 2)))*180. / M_PI;
	//stereo_calibration_result->yaw = atan2(R.at<double>(0, 1), R.at<double>(0, 0))*180. / M_PI;

	stereo_calibration_result->roll		= atan2(R.at<double>(2, 1), R.at<double>(2, 2))*180. / M_PI;
	stereo_calibration_result->pitch	= atan2(-R.at<double>(2, 0), sqrt(pow(R.at<double>(2, 1), 2) + pow(R.at<double>(2, 2), 2)))*180. / M_PI;
	stereo_calibration_result->yaw		= atan2(R.at<double>(1, 0), R.at<double>(0, 0))*180. / M_PI;
	

	stereo_calibration_result->X = POS.at<double>(0);
	stereo_calibration_result->Y = POS.at<double>(1);
	stereo_calibration_result->Z = POS.at<double>(2);

	return stereo_rms;
}

void rotationMatrixToEulerAngles(Mat R, Mat& R_VEC)
{

	R_VEC.create(Size(3, 1), CV_32FC1);

	R = R.t();

	float sy = sqrt(R.at<float>(0, 0)*R.at<float>(0, 0) + R.at<float>(0, 1)*R.at<float>(0, 1));

	bool isSigular = sy < 1e-6;

	float x, y, z;

	if (!isSigular)
	{
		x = atan2f(R.at<float>(1, 2), R.at<float>(2, 2));
		y = atan2f(R.at<float>(0, 2), sy);
		z = atan2f(R.at<float>(0, 1), R.at<float>(0, 0));
	}
	else
	{
		x = atan2f(-R.at<float>(2, 1), R.at<float>(1, 1));
		y = atan2f(-R.at<float>(0, 2), sy);
		z = 0;
	}

	R_VEC.at<float>(0) = x;
	R_VEC.at<float>(1) = y;
	R_VEC.at<float>(2) = z;
}

double MRA2_calibrate_stereo(
	vector<vector<Point3f> > objectPoints,
	vector<vector<Point2f> > imagePointsLeft, vector<vector<Point2f> > imagePointsRight,
	MRA2_CAMERA_FLAG mra2_camera_flag,
	Size imageSize,
	Mat& cameraMatrixLeft, Mat& distCoeffsLeft,
	Mat& cameraMatrixRight, Mat& distCoeffsRight,
	Mat& R, Mat& R_VEC, Mat& T, Mat& POS)
{
	Mat E, F;	//	Stereo 에 필요한 변수 설정
	TermCriteria term_crit = TermCriteria(TermCriteria::MAX_ITER | TermCriteria::EPS, 30, 0.01);
	// Stereo Calibration Flag 설정
	int stereoFlags = 0;

	Mat INV_R, Trans, dummy;


	if (mra2_camera_flag == MRA2_CAMERA_FLAG_1)
		stereoFlags |= CALIB_FIX_INTRINSIC;
	else if (mra2_camera_flag == MRA2_CAMERA_FLAG_2)
		stereoFlags |= CALIB_USE_INTRINSIC_GUESS;

	stereoFlags |= CALIB_FIX_K3;
	stereoFlags |= CALIB_FIX_K4;
	stereoFlags |= CALIB_FIX_K5;
	stereoFlags |= CALIB_ZERO_TANGENT_DIST;
	stereoFlags |= CALIB_FIX_ASPECT_RATIO;

	// 위의 Single_Calibration() 시 Left / Right (Master / Slave로 표현) 의 각각 ImagePoint와 Object, Intrinsic Parameter를 적용한다.
	// 추가적으로 캡쳐이미지의 마지막 5장 or 처음 5장으로만 Extrinsic 계산을 하기 위해 objPoint와 imagePoints를 각각 5로 resize 해야한다.
	// Resize 구현 필요.

	double stereo_rms = stereoCalibrate(
		objectPoints,
		imagePointsLeft, imagePointsRight,
		cameraMatrixLeft, distCoeffsLeft,
		cameraMatrixRight, distCoeffsRight,
		imageSize,
		R, T, E, F,
		stereoFlags,
		term_crit);

	// Stereo 후 Position 계산 필요.
	// Rotation Matrix 3x3 -> Roation Vector 1x3  
	// (Rodrigues 함수 참고 -> Rodrigues함수를 사용하면 3x3 Matrix가 euler Angle 화된 Vector인 1x3으로 변경됨)


	// ** Rot 값 이상현상 문제로 Rodrigues 함수로 원복 (2018.6.12)
#ifndef USE_EulerAngles
	Rodrigues(R, R_VEC);
#else
	rotationMatrixToEulerAngles(R, R_VEC);
#endif

	// Translation Matrix 1x3 -> Translation Vector 1x3  (아래 수식 참고)

	/*
	Translation Matrix to Vector
	Invert_Rotation_Vector = invert(Rotation_Vector)
	Translation_Vector = -Invert_Rotation_Vector * Translation_Matrix      //  Vector * Matrix = Vector 내적(1x3 * 1x3) 곱
	*/

	invert(R, INV_R);

	gemm(-INV_R, T, 1.0, dummy, 0.0, POS);

	return stereo_rms;
}
