#ifndef __MRA2_STEREO_CALIBRATION_H__
#define __MRA2_STEREO_CALIBRATION_H__

#define SQUARE_SIZE 30

typedef	void*	MRA2_HANDLE;

typedef enum
{
	MRA2_IMAGE_FORMAT_8_GRAY = 0x00,
	MRA2_IMAGE_FORMAT_16_GRAY = 0x01,
	MRA2_IMAGE_FORMAT_24_RGB = 0x02,
}MRA2_IMAGE_FORMAT;

typedef enum
{
	MRA2_CAMERA_FLAG_1 = 0x01,	//CALIB_FIX_INTRINSIC
	MRA2_CAMERA_FLAG_2 = 0x02,	//CALIB_USE_INTRINSIC_GUESS
}MRA2_CAMERA_FLAG;

typedef struct __mra2_point
{
	float x;
	float y;
}MRA2_POINT, *PMRA2_POINT;

typedef struct __single_calibration_result
{
	// master camera
	double fx, fy;
	double cx, cy;
	double distortion_cofficeints[5]; // k1-k5: distortion_cofficeints[0] - distortion_cofficeints[4]
	double rms;
	int calib_res_width, calib_res_height;
} SINGLE_CALIBRATION_RESULT, *PSINGLE_CALIBRATION_RESULT;

typedef struct __stereo_calibration_result
{
	// single camera calibration

	// master camera
	double single_master_fx, single_master_fy;
	double single_master_cx, single_master_cy;
	double single_master_distortion_cofficeints[5]; // k1-k5: distortion_cofficeints[0] - distortion_cofficeints[4]
	double single_master_rms;

	// slave camera
	double single_slave_fx, single_slave_fy;
	double single_slave_cx, single_slave_cy;
	double single_slave_distortion_cofficeints[5]; // k1-k5: distortion_cofficeints[0] - distortion_cofficeints[4]
	double single_slave_rms;

	// stereo camera calibration

	// master camera
	double master_fx, master_fy;
	double master_cx, master_cy;
	double master_distortion_cofficeints[5]; // k1-k5: distortion_cofficeints[0] - distortion_cofficeints[4]
	double master_rms;
	int master_calib_res_width, master_calib_res_height;

	// slave camera
	double slave_fx, slave_fy;
	double slave_cx, slave_cy;
	double slave_distortion_cofficeints[5]; // k1-k5: distortion_cofficeints[0] - distortion_cofficeints[4]
	double slave_rms;
	int slave_calib_res_width, slave_calib_res_height;

	// streo camera
	double rotation_vector[3];
	double translation_vector[3];
	double X, Y, Z;
	double roll, pitch, yaw;
	double streo_rms;

} STEREO_CALIBRATION_RESULT, *PSTEREO_CALIBRATION_RESULT;

/*
single camrea 용 영상처리 루틴을 이용하기전에 초기화 하는 함수

board_w			패턴의 폭
board_h			패턴의 높이
return			영상처리 루틴을 사용하기 위한 handle
*/
__declspec(dllexport) MRA2_HANDLE MRA2_single_camera_start(int board_w, int board_h);

/*
single camrea 용 영상처리 루틴의 이용이 끝나면 호출하는 함수

mra2_handle		영상처리 루틴을 이용하기 위한 핸들
*/
__declspec(dllexport) void MRA2_single_camera_stop(MRA2_HANDLE mra2_handle);

/*
단일 카메라의 패턴을 찾는 함수

mra2_handle			영상처리 루틴을 이용하기 위한 핸들
imageData			패턴을 포함하는 영상
width				영상의 폭
height				영상의 높이
isCircle			true: circle 패턴, false: chessboard 패턴
displayCorners		true: 찾은 circle 및 코너를 영상에 출력
return				true: 패턴 찾기 성공, false: 패턴 찾기 실패
*/
__declspec(dllexport) bool MRA2_find_single_camera_pattern(
	MRA2_HANDLE mra2_handle,
	void* imageData,
	MRA2_IMAGE_FORMAT imageFormat,
	int width,
	int height,
	bool isCircle,
	MRA2_POINT* ceneters,
	int* pcenters_num);

/*
단일 카메라의 패턴을 찾는 함수

mra2_handle			영상처리 루틴을 이용하기 위한 핸들
imageFileName		패턴을 포함하는 영상의 파일경로 및 이름
isCircle			true: circle 패턴, false: chessboard 패턴
displayCorners		true: 찾은 circle 및 코너를 영상에 출력
return				true: 패턴 찾기 성공, false: 패턴 찾기 실패
*/
__declspec(dllexport) bool MRA2_find_single_camera_pattern2(
	MRA2_HANDLE mra2_handle,
	char* imageFileName,
	bool isCircle);

/*
단일 카메라의 파라미터를 찾는 함수

mra2_handle					영상처리 루틴을 이용하기 위한 핸들
mra2_camera_flag			함수 flag
single_calibration_result	캘리브레이션을 저장할 변수의 포인터
return						true: 처리 성공, false: 처리 실패
*/
__declspec(dllexport) bool MRA2_find_single_camera_parameter(
	MRA2_HANDLE mra2_handle,
	MRA2_CAMERA_FLAG mra2_camera_flag,
	PSINGLE_CALIBRATION_RESULT single_calibration_result
	);

/*
stereo camrea 용 영상처리 루틴을 이용하기전에 초기화 하는 함수

board_w			패턴의 폭
board_h			패턴의 높이
return			영상처리 루틴을 사용하기 위한 handle
*/
__declspec(dllexport) MRA2_HANDLE MRA2_stereo_camera_start(int board_w, int board_h);

/*
stereo camrea 용 영상처리 루틴의 이용이 끝나면 호출하는 함수

mra2_handle		영상처리 루틴을 이용하기 위한 핸들
*/
__declspec(dllexport) void MRA2_stereo_camera_stop(MRA2_HANDLE mra2_handle);

/*
스테레오 카메라의 패턴을 찾는 함수

mra2_handle			영상처리 루틴을 이용하기 위한 핸들
imageDataLeft		패턴을 포함하는 왼쪽 영상
imageDataRight		패턴을 포함하는 오른쪽 영상
width				영상의 폭
height				영상의 높이
isCircle			true: circle 패턴, false: chessboard 패턴
displayCorners		true: 찾은 circle 및 코너를 영상에 출력
isCircle		true 이면 circle 패턴이고 false 이면 chessboard 패턴
displayCorners 	찾은 circle 및 코너를 영상에 출력할지 여부
return			true : 처리 성공, false : 처리 실패
*/
__declspec(dllexport) bool MRA2_find_stereo_camera_pattern(
	MRA2_HANDLE mra2_handle,
	void* imageDataLeft, void* imageDataRight,
	MRA2_IMAGE_FORMAT imageFormat,
	int width, int height,
	bool isCircle,
	MRA2_POINT* left_ceneters, int* pleft_centers_num,
	MRA2_POINT* right_ceneters, int* pright_centers_num);

/*
단일 카메라의 패턴을 찾는 함수

mra2_handle			영상처리 루틴을 이용하기 위한 핸들
imageData			패턴을 포함하는 영상
width				영상의 폭
height				영상의 높이
isCircle			true: circle 패턴, false: chessboard 패턴
displayCorners		true: 찾은 circle 및 코너를 영상에 출력
return				true: 패턴 찾기 성공, false: 패턴 찾기 실패
*/
__declspec(dllexport) bool MRA2_find_single_camera_pattern(
	MRA2_HANDLE mra2_handle,
	void* imageData,
	MRA2_IMAGE_FORMAT imageFormat,
	int width,
	int height,
	bool isCircle,
	MRA2_POINT* ceneters,
	int* pcenters_num);

/*
스테레요 카메라의 파라미터를 찾는 함수

mra2_handle					영상처리 루틴을 이용하기 위한 핸들
mra2_camera_flag			함수 flag
stereo_calibration_result	캘리브레이션을 저장할 변수의 포인터
return						true: 처리 성공, false: 처리 실패
*/
__declspec(dllexport) double MRA2_find_stereo_camera_parameter(
	MRA2_HANDLE mra2_handle,
	MRA2_CAMERA_FLAG mra2_camera_flag,
	PSTEREO_CALIBRATION_RESULT stereo_calibration_result);

#endif