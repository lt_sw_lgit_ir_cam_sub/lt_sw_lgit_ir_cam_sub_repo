/**
@file	LGCalibIntrParam.h
@date   2017.10.11
@author
Yongji Yun(yongji.yun@lge.com)
@brief
Factory Intrinsic Calibration Parameter for OMS Camera
@remark
@warning
Copyright(C) 2012 LG Electronics Co., LTD., Seoul, Korea
All Rights are Reserved.
*/


#pragma once

#define OMSCAL_INTRINSIC_IRGAIN_SEARCH_INIT		(0.045f)
#define OMSCAL_INTRINSIC_IRGAIN_SEARCH_STEP		(0.02f)
#define OMSCAL_INTRINSIC_IRGAIN_SEARCH_MAX		(2.0f)
#define OMSCAL_INTRINSIC_IRGAIN_SEARCH_HALF		(1.0f)
/**
@brief default parameter
*/
typedef struct
{
	float arfKK[4]; /*!< 0: Fu, 1: Fv, 2: Pu, 3: Pv */
	float arfKc[5]; /*!< Distortion Coef */
	float fR2Max;
	float fOrgOffset;
}LGCalibIntrInfo;

typedef struct
{
	/* default param */
	LGCalibIntrInfo stCalibInfo_default;

	/* Image */
	int nImgWidth;
	int nImgHeight;

	/* Chart Spec */
	int nNumofCornerX;	/*!< cols of chess corner */
	int nNumofCornerY;	/*!< rows of chess corner */
	float fPatternSizeX;
	float fPatternSizeY;

	/* Threshold */
	float fRepThres;	/*!< Threshold of reprojection error, outliers will be excepted to calculate intrinsic, default 1.5 */
	int nMinNumImages;  /*!< Minimum number of corner images, default 13 */
	int nMinNumValidPnts; /*!< threshold of number of inlier points, default 300 */

	/* SearchRange */
	float fMaxOriginOffset;			/*!< maximum range of origin difference between real production stage and theoretical, default 4.0 */
	float fMinOriginOffset;			/*!< minimum range of origin difference between real production stage and theoretical, default -4.0 */
	float fMaxFocalLength;			/*!< maximum range of focal length, default 314.0 */
	float fMinFocalLength;			/*!< minimum range of focal length, default 311.0 */

	/* Version */
	float fVersion;					/*!< Intrinsic algorithm version */

}LGCalibIntrParam;

typedef struct
{
	int nU;					/*!< Start point x of ROI */
	int nV;					/*!< Start point y of ROI */
	int nWidth;				/*!< ROI width */
	int nHeight;			/*!< ROI height */
}LGCalibRoiParam;