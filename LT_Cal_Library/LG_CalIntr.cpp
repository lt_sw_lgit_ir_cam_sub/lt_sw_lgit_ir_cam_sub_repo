//*****************************************************************************
// Filename	: 	LG_CalIntr.cpp
// Created	:	2017/10/16 - 14:50
// Modified	:	2017/10/16 - 14:50
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "LG_CalIntr.h"

#ifdef _WIN32
	#ifdef _WIN64	// 64bit
		#ifdef _DEBUG
			#pragma comment (lib,"../LT_Cal_Library/LGIT_OMS_Entry_2DCal/x64/Debug/LGCalibIntrinsic_LGITd.lib")
		#else
			#pragma comment (lib,"../LT_Cal_Library/LGIT_OMS_Entry_2DCal/x64/Release/LGCalibIntrinsic_LGIT.lib")
		#endif
	#else			// 32bit

	#endif
#endif


CLG_CalIntr::CLG_CalIntr()
{
	m_pnLoadIdx = NULL;
	memset(m_arfCornerPt, 0x00, sizeof(float)* MAX_2DCAL_Corner_Points * 2 * MAX_2DCAL_Positions);
}

CLG_CalIntr::~CLG_CalIntr()
{
}

//=============================================================================
// Method		: OnTracePrint
// Access		: protected  
// Returns		: void
// Parameter	: __in eLGCalibIntrMsg eRet
// Qualifier	:
// Last Update	: 2018/2/7 - 21:10
// Desc.		:
//=============================================================================
void CLG_CalIntr::OnTracePrint(__in eLGCalibIntrMsg eRet)
{
	switch (eRet)
	{
	case eLGINTRMSG_ERR:
		TRACE(_T("2D Cal SendResult ErrorName : eLGINTRMSG_ERR \r\n"));
		break;

	case eLGINTRMSG_ERR_FILENAME:
		TRACE(_T("2D Cal SendResult ErrorName : eLGINTRMSG_ERR_FILENAME \r\n"));
		break;

	case eLGINTRMSG_ERR_DATE:
		TRACE(_T("2D Cal SendResult ErrorName : eLGINTRMSG_ERR_DATE \r\n"));
		break;

	case eLGINTRMSG_ERR_IMAGESIZE:
		TRACE(_T("2D Cal SendResult ErrorName : eLGINTRMSG_ERR_IMAGESIZE \r\n"));
		break;

	case eLGINTRMSG_ERR_CHARTSPEC:
		TRACE(_T("2D Cal SendResult ErrorName : eLGINTRMSG_ERR_CHARTSPEC \r\n"));
		break;

	case eLGINTRMSG_ERR_THRES:
		TRACE(_T("2D Cal SendResult ErrorName : eLGINTRMSG_ERR_THRES \r\n"));
		break;

	case eLGINTRMSG_ERR_SEARCHRANGE:
		TRACE(_T("2D Cal SendResult ErrorName : eLGINTRMSG_ERR_SEARCHRANGE \r\n"));
		break;

	case eLGINTRMSG_ERR_VERSION:
		TRACE(_T("2D Cal SendResult ErrorName : eLGINTRMSG_ERR_VERSION \r\n"));
		break;

	case eLGINTRMSG_ERR_CORNERPOINTS:
		TRACE(_T("2D Cal SendResult ErrorName : eLGINTRMSG_ERR_CORNERPOINTS \r\n"));
		break;

	case eLGINTRMSG_ERR_EVALDISTANCE:
		TRACE(_T("2D Cal SendResult ErrorName : eLGINTRMSG_ERR_EVALDISTANCE \r\n"));
		break;

	case eLGINTRMSG_ERR_NEEDMOREIMAGES:
		TRACE(_T("2D Cal SendResult ErrorName : eLGINTRMSG_ERR_NEEDMOREIMAGES \r\n"));
		break;

	case eLGINTRMSG_ERR_NEEDMOREPOINTS:
		TRACE(_T("2D Cal SendResult ErrorName : eLGINTRMSG_ERR_NEEDMOREPOINTS \r\n"));
		break;

	case eLGINTRMSG_ERR_NOTEXISTFILE:
		TRACE(_T("2D Cal SendResult ErrorName : eLGINTRMSG_ERR_NOTEXISTFILE \r\n"));
		break;

	case eLGINTRMSG_ERR_INVALIDFILE:
		TRACE(_T("2D Cal SendResult ErrorName : eLGINTRMSG_ERR_INVALIDFILE \r\n"));
		break;

	case eLGINTRMSG_ERR_FILEWRITEFAILURE:
		TRACE(_T("2D Cal SendResult ErrorName : eLGINTRMSG_ERR_FILEWRITEFAILURE \r\n"));
		break;

	case eLGINTRMSG_SIZE:
		TRACE(_T("2D Cal SendResult ErrorName : eLGINTRMSG_SIZE \r\n"));
		break;

	default:
		break;
	}
}


//=============================================================================
// Method		: SetCalResultExecute
// Access		: public  
// Returns		: eLGCalibIntrMsg
// Parameter	: __in UINT nLoadFileCnt
// Qualifier	:
// Last Update	: 2018/2/7 - 21:10
// Desc.		:
//=============================================================================
eLGCalibIntrMsg CLG_CalIntr::SetCalResultExecute(__in UINT nLoadFileCnt)
{
	eLGCalibIntrMsg eRet = eLGINTRMSG_NOERR;

	memset(&m_stLG_CalResult, 0x00, sizeof(LGCalibIntrResult));
	
	// initalize
	m_stLG_CalParam.nImgWidth						= 640;
	m_stLG_CalParam.nImgHeight						= 480;
	m_stLG_CalParam.nNumofCornerX					= 4;
	m_stLG_CalParam.nNumofCornerY					= 6;
	m_stLG_CalParam.fPatternSizeX					= 60.f;
	m_stLG_CalParam.fPatternSizeY					= 60.f;
	m_stLG_CalParam.fRepThres						= 2.f;
	m_stLG_CalParam.nMinNumImages					= 13;
	m_stLG_CalParam.nMinNumValidPnts				= 260;
	m_stLG_CalParam.fMinOriginOffset				= -3.0f;
	m_stLG_CalParam.fMaxOriginOffset				= 14.5f;
	m_stLG_CalParam.fMinFocalLength					= 311.5f;
	m_stLG_CalParam.fMaxFocalLength					= 314.0f;
	m_stLG_CalParam.stCalibInfo_default.arfKK[0]	= 312.f;
	m_stLG_CalParam.stCalibInfo_default.arfKK[1]	= 312.f;
	m_stLG_CalParam.stCalibInfo_default.arfKK[2]	= 320.f;
	m_stLG_CalParam.stCalibInfo_default.arfKK[3]	= 240.f;
	m_stLG_CalParam.stCalibInfo_default.arfKc[0]	= -0.2f;
	m_stLG_CalParam.stCalibInfo_default.arfKc[1]	= 0.045f;
	m_stLG_CalParam.stCalibInfo_default.arfKc[2]	= 0.f;
	m_stLG_CalParam.stCalibInfo_default.arfKc[3]	= 0.f;
	m_stLG_CalParam.stCalibInfo_default.arfKc[4]	= -0.0045f;
	m_stLG_CalParam.stCalibInfo_default.fR2Max		= 4.4f;
	m_stLG_CalParam.fVersion						= 1.0f;

	eRet = m_LG_CalIntr.setParam(&m_stLG_CalParam);
	if (eRet != eLGINTRMSG_NOERR)
	{
		OnTracePrint(eRet);
		return eRet;
	}

	eRet = m_LG_CalIntr.setOutputFileName("./omscal_intrinsic.bin");
	if (eRet != eLGINTRMSG_NOERR)
	{
		OnTracePrint(eRet);
		return eRet;
	}

	eRet = m_LG_CalIntr.setDate("171011");
	if (eRet != eLGINTRMSG_NOERR)
	{
		OnTracePrint(eRet);
		return eRet;
	}

	// execute
	m_LG_CalIntr.clearAll();
	const int nNumofEval = 4;
	const int arnEvalIdx[nNumofEval] = { 1, 6, 9, 12 };
	const float arfEvalDist[nNumofEval] = { 200.f, 300.f, 400.f, 500.f };

	for (UINT nIdx0 = 0; nIdx0 < nLoadFileCnt; nIdx0++)
	{
		if (m_pnLoadIdx[nIdx0]== 0)
			continue;		

		// receive corner points
		eRet = m_LG_CalIntr.addPointsCalib(m_arfCornerPt[nIdx0]);
		if (eRet != eLGINTRMSG_NOERR)
		{
			OnTracePrint(eRet);
			return eRet;
		}

		for (int nEvalIdx = 0; nEvalIdx < nNumofEval; nEvalIdx++)
		{
			int nIdx1 = arnEvalIdx[nEvalIdx];
			if (nIdx1 > (int)nIdx0)
			{
				break;
			}
			else if (nIdx0 == nIdx1)
			{
				eRet = m_LG_CalIntr.addPointsEval(m_arfCornerPt[nIdx0], arfEvalDist[nEvalIdx]);
				if (eRet != eLGINTRMSG_NOERR)
				{
					OnTracePrint(eRet);
					return eRet;
				}
			}
		}
	}

	eRet = m_LG_CalIntr.execIntrinsic(&m_stLG_CalResult);
	if (eRet != eLGINTRMSG_NOERR)
	{
		OnTracePrint(eRet);
		return eRet;
	}


	memset(&m_stLG_CalResult.arcDate, 0x00, sizeof(m_stLG_CalResult));

	SYSTEMTIME tm;
	GetLocalTime(&tm);

	CStringA szDate;
	szDate.Format("%02d%02d%02d", (tm.wYear - 2000), tm.wMonth, tm.wDay);

	memcpy(m_stLG_CalResult.arcDate, szDate.GetBuffer(0), 6);

	// print result to console
	m_LG_CalIntr.printResult("./omscal_intrinsic.bin");
	TRACE(_T("2D Cal SendResult ==== FileWrite Path: ./omscal_intrinsic.bin \r\n"));

	return eRet;
}

//=============================================================================
// Method		: Exec_CalIntrinsic
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LGCalibIntrParam * pstInParameter
// Parameter	: __in const float * * ppInCornerPt
// Parameter	: __in UINT nConerPtStepCount
// Parameter	: __out LGCalibIntrResult & stOutResult
// Qualifier	:
// Last Update	: 2018/2/7 - 21:09
// Desc.		:
//=============================================================================
BOOL CLG_CalIntr::Exec_CalIntrinsic(__in LGCalibIntrParam* pstInParameter, __in const float** ppInCornerPt, __in UINT nConerPtStepCount, __out LGCalibIntrResult& stOutResult)
{
	//MAX_2DCAL_Positions
	for (UINT nIdx = 0; nIdx < nConerPtStepCount; nIdx++)
	{
	 	memcpy(&m_arfCornerPt[nIdx], ppInCornerPt, sizeof(float) * MAX_2DCAL_Corner_Points * 2);
	}
	 
	Set_Parameter(pstInParameter);
	 
	if (eLGINTRMSG_NOERR != SetCalResultExecute(nConerPtStepCount))
	{
	 	//lReturn = RC_2DCal_Err_Execute;
	 	return FALSE;
	}
	 
	GetCalResultParam(&stOutResult);

	return TRUE;
}

//=============================================================================
// Method		: Get_ConerPoint
// Access		: public  
// Returns		: BOOL
// Parameter	: __in const unsigned short * pwInImage
// Parameter	: __in LGCalibRoiParam * pstInROI
// Parameter	: __out float * pfOutResult
// Parameter	: __in_opt bool bImageShow	-> 코너점 위치 표시창 팝업 사용여부
// Qualifier	:
// Last Update	: 2018/2/20 - 15:04
// Desc.		:
//=============================================================================
BOOL CLG_CalIntr::Get_ConerPoint(__in const unsigned short* pwInImage, __in LGCalibRoiParam* pstInROI, __out float* pfOutResult, __in_opt bool bImageShow /*= false*/)
{
	//DLLAPI eLGCalibIntrMsg findChessBoard(const unsigned short *imgData, float* fcbResult, LGCalibRoiParam* calibRoi = NULL);
	m_nLG_CalMessage = m_LG_CalIntr.findChessBoard(pwInImage, pfOutResult, pstInROI, bImageShow);

	if (eLGINTRMSG_NOERR == m_nLG_CalMessage)
		return TRUE;
	else
		return FALSE;
}

//=============================================================================
// Method		: Set_Parameter
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LGCalibIntrParam * pstParam_ext
// Qualifier	:
// Last Update	: 2018/2/7 - 21:05
// Desc.		:
//=============================================================================
BOOL CLG_CalIntr::Set_Parameter(__in LGCalibIntrParam *pstParam_ext)
{
	memcpy(&m_stLG_CalParam, pstParam_ext, sizeof(LGCalibIntrParam));

	m_nLG_CalMessage = m_LG_CalIntr.setParam(pstParam_ext);

	if (eLGINTRMSG_NOERR == m_nLG_CalMessage)
		return TRUE;
	else
		return FALSE;
}

