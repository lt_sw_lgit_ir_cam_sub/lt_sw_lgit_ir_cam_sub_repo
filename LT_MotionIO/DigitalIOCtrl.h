﻿//*****************************************************************************
// Filename	: 	DigitalIOCtrl.h
// Created	:	2017/02/05 - 15:59
// Modified	:	2017/02/05 - 15:59
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#ifndef DigitalIOCtrl_h__
#define DigitalIOCtrl_h__

#pragma once

#include "AXD.h"
#include "AXL.h"
#include "Def_DigitiIO.h"
#include <TimeAPI.h>

class CDigitalIOCtrl
{
public:
	CDigitalIOCtrl();
	~CDigitalIOCtrl();

	enum enReadIndex
	{
		ReadIdx_DI,
		ReadIdx_DO,
		ReadIdx_MaxEnum,
	};

protected:

	HWND				m_hOwnerWnd					= NULL;		// 오너의 윈도우 핸들
	HANDLE				m_hInternalExitEvent		= NULL;		// 내부의 통신 접속 쓰레드 종료 이벤트
	HANDLE				m_hExternalExitEvent		= NULL;		// 외부의 통신 접속 쓰레드 종료 이벤트	

	// 베이스 보드 및 모듈 처리에 사용하는 구조체
	ST_AxdBoardInfo		m_stAxdBoardInfo;

	// DI, DO 모듈 수량
	long				m_lDIO_ModuleCnt			= 0;
	long				m_lDI_PortCnt				= 0;
	long				m_lDO_PortCnt				= 0;
	
	// 플래그
	BOOL				m_bStored_DI				= FALSE;
	BOOL				m_bStored_DO				= FALSE;

	// DI Bit 데이터 (Read)
	DWORD64				m_dwDI_ReadData				= 0;
	DWORD64				m_dwDO_ReadData				= 0;

	// DO Bit 데이터 (Write)
	DWORD				m_dwDO_WriteData			= 0;

	// 윈도우 메세지
	UINT				m_nWMBitChanged				= NULL;		// 비트 단위의 변화 감지시 메세지 처리 용도
	UINT				m_nWMFirstRead				= NULL;		// 처음으로 읽은 데이터

	BOOL				m_bFlag_Mon					= FALSE;
	HANDLE				m_hThr_Mon					= NULL;		// 모니터링 쓰레드 핸들	
	DWORD				m_dwMonCycle				= 100;		// 모니터링 주기
	
	long				m_lOutPulseDelay			= 1500;		// 1 -> 0 으로 bit 처리하는데 필요한 지연시간 (ms)
	long				m_lToggleCount				= -1;		// 토글 횟수, -1의 경우 무한 토글
	long				m_lToggleOnTimeDelay		= 200;		// 토글 기능 On Time 시간 간격 (1 ~ 30000 ms)
	long				m_lToggleOffTimeDelay		= 200;		// 토글 기능 Off Time 시간 간격 (1 ~ 30000 ms)

	static UINT WINAPI	Thread_Monitoring			(__in LPVOID lParam);
	void				Monitoring_Status			();

	// DI/DO 상태 정보 구하기
	virtual DWORD		Read_DI_Status				();
	virtual DWORD		Read_DO_Status				();

	DWORD				Get_DIO_Assign				();
	long				Get_DI_PortCnt				();
	long				Get_DO_PortCnt				();

public:

	// Lib 초기화 및 IN & OUT SETTING
	BOOL				AXTInit						();
	BOOL				AXTState					();

	void				SetExitEvent				(__in HANDLE hExitEvent);

	// DI/DO bit 모니터링 시작/종료
	BOOL				Start_Monitoring			();
	BOOL				Stop_Monitoring				();
	void				Set_MonitoringCycle			(__in DWORD dwMiliseconds);

	// 윈도우 메세지
	void				SetOwnerHwnd				(__in HWND hOwnerWnd);
	void				Set_WM_BitChanged			(__in UINT nWindowMsg);
	void				Set_WM_FirstRead			(__in UINT nWindowMsg);

	// 현재 DI/DO 상태 가져오기
	virtual void		Get_DI_Status_All			(__out DWORD64 dwValue);
	virtual BOOL		Get_DI_Status				(__in  long   lOffset);
	virtual void		Get_DO_Status_All			(__out DWORD64 dwValue);
	virtual BOOL		Get_DO_Status				(__in  long   lOffset);

	// DO Bit 설정
	virtual DWORD		Set_DO_Status				(__in long lOffset, __in enum_IO_SignalType SignalType = IO_SignalT_SetOn);
};

#endif // DigitalIOCtrl_h__
