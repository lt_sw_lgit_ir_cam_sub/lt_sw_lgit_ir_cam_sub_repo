//*****************************************************************************
// Filename	: PageOpt_PLC.h
// Created	: 2010/9/16
// Modified	: 2010/9/16 - 18:05
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#ifndef PageOpt_PLC_h__
#define PageOpt_PLC_h__

#pragma once
#include "PageOption.h"
#include "Define_Option.h"

using namespace Luritech_Option;

//=============================================================================
//
//=============================================================================
class CPageOpt_PLC : public CPageOption
{
	DECLARE_DYNAMIC(CPageOpt_PLC)

public:
	CPageOpt_PLC						(void);
	CPageOpt_PLC						(UINT nIDTemplate, UINT nIDCaption = 0);
	virtual ~CPageOpt_PLC				(void);

protected:

	DECLARE_MESSAGE_MAP()

	virtual void			AdjustLayout		();
	virtual void			SetPropListFont		();
	virtual void			InitPropList		();

	stOpt_PLC			m_stOption;

	stOpt_PLC			GetOption			();
	void					SetOption			(stOpt_PLC stOption);

public:

	virtual void			SaveOption			();
	virtual void			LoadOption			();
};

#endif // PageOpt_PLC_h__
