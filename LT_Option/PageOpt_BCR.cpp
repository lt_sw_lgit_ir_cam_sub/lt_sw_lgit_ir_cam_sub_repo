﻿//*****************************************************************************
// Filename	: PageOpt_BCR.cpp
// Created	: 2010/9/16
// Modified	: 2010/9/16 - 15:33
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#include "StdAfx.h"
#include "PageOpt_BCR.h"
#include "Define_OptionItem.h"
#include "Define_OptionDescription.h"
#include <memory>
#include "MFCPropertyGridProperties.h"

#define ID_PROPGRID_IPADDR (21)

IMPLEMENT_DYNAMIC(CPageOpt_BCR, CPageOption)

//=============================================================================
// 생성자
//=============================================================================
CPageOpt_BCR::CPageOpt_BCR(void)
{
}

CPageOpt_BCR::CPageOpt_BCR(UINT nIDTemplate, UINT nIDCaption /*= 0*/) : CPageOption(nIDTemplate, nIDCaption)
{

}

//=============================================================================
// 소멸자
//=============================================================================
CPageOpt_BCR::~CPageOpt_BCR(void)
{
}

BEGIN_MESSAGE_MAP(CPageOpt_BCR, CPageOption)	
END_MESSAGE_MAP()

//=============================================================================
// CPageOpt_BCR 메시지 처리기입니다.
//=============================================================================
//=============================================================================
// Method		: CPageOpt_BCR::AdjustLayout
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/8/30 - 11:12
// Desc.		:
//=============================================================================
void CPageOpt_BCR::AdjustLayout()
{
	CPageOption::AdjustLayout();
}

//=============================================================================
// Method		: CPageOpt_BCR::SetPropListFont
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/8/30 - 11:12
// Desc.		:
//=============================================================================
void CPageOpt_BCR::SetPropListFont()
{
	CPageOption::SetPropListFont();
}

//=============================================================================
// Method		: CPageOpt_BCR::InitPropList
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/8/30 - 11:12
// Desc.		:
//=============================================================================
void CPageOpt_BCR::InitPropList()
{
	CPageOption::InitPropList();

	//--------------------------------------------------------
	// 통신 포트 설정
	//--------------------------------------------------------
	// Handy BCR
	std::auto_ptr<CMFCPropertyGridProperty> apHandy_ComPort(new CMFCPropertyGridProperty(_T("Handy Barcode Reader COM Port")));
		
	InitPropList_Comport(apHandy_ComPort);

	m_wndPropList.AddProperty(apHandy_ComPort.release());


// 	if (SYS_FOCUSING == g_InspectorTable[m_InsptrType].SysType)
// 	{
// 		// Fixed BCR
// 		std::auto_ptr<CMFCPropertyGridProperty> apFixed_ComPort(new CMFCPropertyGridProperty(_T("Fixed Barcode Reader COM Port")));
// 
// 		InitPropList_Comport(apFixed_ComPort);
// 
// 		m_wndPropList.AddProperty(apFixed_ComPort.release());
// 	}
}

//=============================================================================
// Method		: CPageOpt_BCR::SaveOption
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/9/6 - 15:20
// Desc.		:
//=============================================================================
void CPageOpt_BCR::SaveOption()
{
	CPageOption::SaveOption();

	m_stOption	= GetOption ();

	m_pLT_Option->SaveOption_BCR(m_stOption);
}

//=============================================================================
// Method		: CPageOpt_BCR::LoadOption
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/9/6 - 15:20
// Desc.		:
//=============================================================================
void CPageOpt_BCR::LoadOption()
{
	CPageOption::LoadOption();

	if (m_pLT_Option->LoadOption_BCR(m_stOption))
		SetOption(m_stOption);
}

//=============================================================================
// Method		: CPageOpt_BCR::GetOption
// Access		: protected 
// Returns		: Luritech_Option::stOption_DigIO
// Qualifier	:
// Last Update	: 2010/9/10 - 16:07
// Desc.		:
//=============================================================================
Luritech_Option::stOpt_BCR CPageOpt_BCR::GetOption()
{
 	UINT nGroupIndex	= 0;
 	UINT nSubItemIndex	= 0;
 	UINT nIndex			= 0;

	COleVariant rVariant;
	//VARIANT		varData;
	CString		strValue;

	//---------------------------------------------------------------
	// 통신 포트 설정
	//---------------------------------------------------------------
	int iCount = m_wndPropList.GetPropertyCount();
	CMFCPropertyGridProperty* pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	int iSubItemCount = 0;

	// Handy BCR
	GetOption_ComPort(pPropertyGroup, m_stOption.Handy_ComPort);

// 	if (SYS_FOCUSING == g_InspectorTable[m_InsptrType].SysType)
// 	{
// 		// Fixed BCR
// 		pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
// 		GetOption_ComPort(pPropertyGroup, m_stOption.Fixed_ComPort);
// 	}
	
	return m_stOption;
}

//=============================================================================
// Method		: CPageOpt_BCR::SetOption
// Access		: protected 
// Returns		: void
// Parameter	: stOption_DigIO stOption
// Qualifier	:
// Last Update	: 2010/9/10 - 16:07
// Desc.		:
//=============================================================================
void CPageOpt_BCR::SetOption( stOpt_BCR stOption )
{
	UINT nGroupIndex	= 0;
	UINT nSubItemIndex = 0;

	//---------------------------------------------------------------
	// 그룹 1 통신 종류
	//---------------------------------------------------------------
	int iCount = m_wndPropList.GetPropertyCount();
	CMFCPropertyGridProperty* pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	int iSubItemCount = 0;
	
	// Handy BCR
	SetOption_ComPort(pPropertyGroup, m_stOption.Handy_ComPort);
 
// 	if (SYS_FOCUSING == g_InspectorTable[m_InsptrType].SysType)
// 	{
// 		// Fixed BCR
// 		pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
// 		SetOption_ComPort(pPropertyGroup, m_stOption.Fixed_ComPort);
// 	}
}
