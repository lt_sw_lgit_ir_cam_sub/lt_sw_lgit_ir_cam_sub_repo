﻿//*****************************************************************************
// Filename	: 	PageOpt_Torque.cpp
// Created	:	2015/12/16 - 15:23
// Modified	:	2015/12/16 - 15:23
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "StdAfx.h"
#include "PageOpt_Torque.h"
#include "Define_OptionItem.h"
#include "Define_OptionDescription.h"

#include <memory>
#include "CustomProperties.h"

IMPLEMENT_DYNAMIC(CPageOpt_Torque, CPageOption)

//=============================================================================
//
//=============================================================================
CPageOpt_Torque::CPageOpt_Torque(LPCTSTR lpszCaption /*= NULL*/) : CPageOption(lpszCaption)
{
}

CPageOpt_Torque::CPageOpt_Torque(UINT nIDTemplate, UINT nIDCaption /*= 0*/) : CPageOption(nIDTemplate, nIDCaption)
{

}

//=============================================================================
//
//=============================================================================
CPageOpt_Torque::~CPageOpt_Torque(void)
{
}

BEGIN_MESSAGE_MAP(CPageOpt_Torque, CPageOption)
END_MESSAGE_MAP()

// CPageOpt_Torque 메시지 처리기입니다.

//=============================================================================
// Method		: CPageOpt_Torque::AdjustLayout
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/16 - 15:23
// Desc.		:
//=============================================================================
void CPageOpt_Torque::AdjustLayout()
{
	CPageOption::AdjustLayout();
}

//=============================================================================
// Method		: CPageOpt_Torque::SetPropListFont
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/16 - 15:23
// Desc.		:
//=============================================================================
void CPageOpt_Torque::SetPropListFont()
{
	CPageOption::SetPropListFont();
}

//=============================================================================
// Method		: CPageOpt_Torque::InitPropList
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/16 - 15:23
// Desc.		:
//=============================================================================
void CPageOpt_Torque::InitPropList()
{
	CPageOption::InitPropList();

	//--------------------------------------------------------
	// 통신 포트 설정
	//--------------------------------------------------------
	CString szGroupName;
	szGroupName.Format(_T("Torque Driver : COM Port"));
	std::auto_ptr<CMFCPropertyGridProperty> apGroup_ComPort_1(new CMFCPropertyGridProperty(szGroupName));
	InitPropList_Comport(apGroup_ComPort_1);
	m_wndPropList.AddProperty(apGroup_ComPort_1.release());
}

//=============================================================================
// Method		: CPageOpt_Torque::SaveOption
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/16 - 15:23
// Desc.		:
//=============================================================================
void CPageOpt_Torque::SaveOption()
{
	CPageOption::SaveOption();

	m_stOption = GetOption();

	m_pLT_Option->SaveOption_Torque(m_stOption);
}

//=============================================================================
// Method		: CPageOpt_Torque::LoadOption
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/16 - 15:23
// Desc.		:
//=============================================================================
void CPageOpt_Torque::LoadOption()
{
	CPageOption::LoadOption();

	if (m_pLT_Option->LoadOption_Torque(m_stOption))
		SetOption(m_stOption);
}

//=============================================================================
// Method		: GetOption
// Access		: protected  
// Returns		: Luritech_Option::stOpt_Torque
// Qualifier	:
// Last Update	: 2018/3/5 - 23:33
// Desc.		:
//=============================================================================
Luritech_Option::stOpt_Torque CPageOpt_Torque::GetOption()
{
	UINT nGroupIndex = 0;
	UINT nSubItemIndex = 0;
	UINT nIndex = 0;

	//---------------------------------------------------------------
	// 그룹 1 
	//---------------------------------------------------------------
	int iCount = m_wndPropList.GetPropertyCount();
	CMFCPropertyGridProperty* pPropertyGroup = NULL;
	int iSubItemCount = 0;
	
	pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	iSubItemCount = pPropertyGroup->GetSubItemsCount();
	nSubItemIndex = 0;

	GetOption_ComPort(pPropertyGroup, m_stOption.ComPort_Torque);

	return m_stOption;
}

//=============================================================================
// Method		: CPageOpt_Torque::SetOption
// Access		: protected 
// Returns		: void
// Parameter	: stOpt_Torque stOption
// Qualifier	:
// Last Update	: 2015/12/16 - 15:23
// Desc.		:
//=============================================================================
void CPageOpt_Torque::SetOption(stOpt_Torque stOption)
{
 	UINT nGroupIndex = 0;
	UINT nSubItemIndex	= 0;

 	//---------------------------------------------------------------
 	// 그룹 1
 	//---------------------------------------------------------------
	int iCount = m_wndPropList.GetPropertyCount();
	CMFCPropertyGridProperty* pPropertyGroup = NULL;
	int iSubItemCount = 0;

	pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	iSubItemCount = pPropertyGroup->GetSubItemsCount();
	nSubItemIndex = 0;

	SetOption_ComPort(pPropertyGroup, m_stOption.ComPort_Torque);
}