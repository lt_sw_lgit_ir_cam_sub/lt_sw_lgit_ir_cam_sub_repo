﻿//*****************************************************************************
// Filename	: 	PageOpt_Torque.h
// Created	:	2015/12/16 - 15:23
// Modified	:	2015/12/16 - 15:23
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef PageOpt_Torque_h__
#define PageOpt_Torque_h__

#pragma once
#include "PageOption.h"
#include "Define_Option.h"

using namespace Luritech_Option;

class CPageOpt_Torque : public CPageOption
{
	DECLARE_DYNAMIC(CPageOpt_Torque)

public:
	CPageOpt_Torque(LPCTSTR lpszCaption = NULL);
	CPageOpt_Torque(UINT nIDTemplate, UINT nIDCaption = 0);
	virtual ~CPageOpt_Torque(void);

protected:

	DECLARE_MESSAGE_MAP()

	virtual void		AdjustLayout();
	virtual void		SetPropListFont();
	virtual void		InitPropList();

	stOpt_Torque	m_stOption;

	stOpt_Torque	GetOption();
	void			SetOption(stOpt_Torque stOption);

public:

	virtual void		SaveOption();
	virtual void		LoadOption();
};

#endif // PageOpt_Torque_h__
