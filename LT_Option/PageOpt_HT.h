﻿//*****************************************************************************
// Filename	: PageOpt_HT.h
// Created	: 2010/9/16
// Modified	: 2010/9/16 - 18:05
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#ifndef PageOpt_HT_h__
#define PageOpt_HT_h__

#pragma once
#include "PageOption.h"
#include "Define_Option.h"

using namespace Luritech_Option;

//=============================================================================
//
//=============================================================================
class CPageOpt_HT : public CPageOption
{
	DECLARE_DYNAMIC(CPageOpt_HT)

public:
	CPageOpt_HT						(void);
	CPageOpt_HT						(UINT nIDTemplate, UINT nIDCaption = 0);
	virtual ~CPageOpt_HT			(void);

protected:

	DECLARE_MESSAGE_MAP()

	virtual void			AdjustLayout		();
	virtual void			SetPropListFont		();
	virtual void			InitPropList		();

	stOpt_HT				m_stOption;

	stOpt_HT				GetOption			();
	void					SetOption			(stOpt_HT stOption);

public:

	virtual void			SaveOption			();
	virtual void			LoadOption			();
};

#endif // PageOpt_HT_h__
